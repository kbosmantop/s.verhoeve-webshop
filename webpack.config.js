'use strict';

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const Encore = require('@symfony/webpack-encore');
const path = require('path');

Encore
    // the project directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    .enableSingleRuntimeChunk()

    .createSharedEntry('vendor', './assets/js/vendor.js')

    .addEntry('common', './assets/js/common.js')
    .addEntry('css-vars', './assets/js/css-vars.js')
    .addEntry('reset', './assets/js/reset.js')
    .addEntry('app', './assets/js/app.js')
    .addEntry('home', './assets/js/home.js')
    .addEntry('assortment', './assets/js/assortment.js')
    .addEntry('product', './assets/js/product.js')
    .addEntry('product-combination', './assets/js/product-combination.js')
    .addEntry('designer', './assets/js/designer.js')
    .addEntry('designer.extensions', './assets/js/designer.extensions.js')
    .addEntry('designer.extensions.front', './assets/js/designer.extensions.front.js')
    .addEntry('preview', './assets/js/preview.js')
    .addEntry('checkout-delivery', './assets/js/checkout-delivery.js')
    .addEntry('checkout-additional-product', './assets/js/checkout-additional-product.js')
    .addEntry('checkout-sender', './assets/js/checkout-sender.js')
    .addEntry('checkout-success', './assets/js/checkout-success.js')
    .addEntry('checkout-cart', './assets/js/checkout-cart.js')
    .addEntry('payment-pending', './assets/js/payment-pending.js')
    .addEntry('customer-register', './assets/js/customer-register.js')
    .addEntry('customer-account', './assets/js/customer-account.js')
    .addEntry('customer-order', './assets/js/customer-order.js')
    .addEntry('customer-settings', './assets/js/customer-settings.js')
    .addEntry('customer-account-users', './assets/js/customer-account-users.js')
    .addEntry('customer-addressbook', './assets/js/customer-addressbook.js')
    .addEntry('truncate.jquery', './assets/js/includes/truncate.jquery.js')
    .addEntry('cart-invalid-combination', './assets/js/includes/cart-invalid-combination.js')
    .addEntry('cookie-consent', './assets/js/cookie-consent.js')
    .addEntry('employee-bar', './assets/js/employee-bar.js')
    .addEntry('page', './assets/js/page.js')
    .addEntry('reviews', './assets/js/reviews.js')
    .addEntry('customer-choose-password', './assets/js/customer-choose-password.js')
    .addEntry('customer-reset-password', './assets/js/customer-reset-password.js')

    // allow sass/scss files to be processed
    .enableSassLoader()

    .enablePostCssLoader((options) => {
         options.config = {
             path: 'postcss.config.js'
         };
     })

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .addLoader({
        test: /\.twig$/,
        include: [
            path.resolve(__dirname, 'node_modules', 'topgeschenken_designer', 'src', 'twig')
        ],
        loader: "raw-loader",
        options: {
            name: '[path]twig/[name].[ext]',
        }
    })

    .autoProvideVariables({
        Promise: 'es6-promise-promise',
        Popper: ['popper.js'],
        "window.Tether": ['tether'],
        'window.store': 'store2',
        'store': 'store2',
        'swal': ['sweetalert2'],
        Tether: ['tether'],
        'Cookies': ['js-cookie'],
        'SVG': 'svg.js',
        'Twig': 'twig',
        'emojione': 'emojione',
        'WebFont': 'webfontloader'
    })

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
;

if(!Encore.isProduction()) {
    Encore.addPlugin(new BundleAnalyzerPlugin({
        analyzerMode: 'disabled',
        generateStatsFile: true
    }));
}

const webpackConfig = Encore.getWebpackConfig();

// Include Designer to compile to ES5
webpackConfig.module.rules[0].exclude = /bower_components|node_modules\/(?!(topgeschenken_designer)\/).*/;

// export the final configuration
module.exports = webpackConfig;
