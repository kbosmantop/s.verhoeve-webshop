### Werkwijze git
- Ontwikkel **NOOIT** direct in de `master` branch of in een versie-branch (bijv. `1.5`)
- Voor _bug fixes_ op de release in productie:
  - Tak een branch af van `pr-branchpoint`
  - Maak de fix in de nieuwe branch
  - Wanneer de fix gereed is maak je een PR (pull request) naar de versie-branch die op dat moment in productie staat.
- Voor het ontwikkelen van een _nieuwe feature_:
  - Tak een branch af van `master`
  - Implementeer de feature in de nieuwe branch
  - Wanneer de feature gereed is maak je een PR naar master.
- Elke PR wordt beoordeeld door de applicatiebeheerder, of door de manager IT samen met een andere developer dan die de PR heeft gemaakt. Wanneer de wijziging is goedgekeurd wordt deze online gemerged.
- Een merge conflict is altijd reden voor afkeur. Los dit op door de branch te [rebasen](https://github.com/edx/edx-platform/wiki/How-to-Rebase-a-Pull-Request).

### Versienummering
- Versienummering is geen planning mechanisme! Een eventuele target voor oplevering van nieuwe features wordt dan ook gezet in de vorm van een datum, niet in de vorm van een versienummer.
- Op het moment een release wordt gemaakt, wordt hier een versienummer voor gekozen op basis van de [SemVer 2.0](http://semver.org/#semantic-versioning-200) richtlijn. Kort samengevat:
  - Versienummer heeft vorm _x_._y_._z_
  - Een release die ten opzichte van de vorige alléén bugfixes bevat: _z_ wordt verhoogd.
  - Een release die nieuwe features bevat, maar backwards compatible is met de vorige release: _y_ wordt verhoogd, _z_ wordt `0`.
  - Een release die niet backwards compatible is met de vorige release: _x_ wordt verhoogd, _y_ en _z_ worden `0`.
  
  Hierin is natuurlijk wel van belang om te definiëren wat _backwards compatible_ dan inhoudt. Vooralsnog houden we aan dat _x_ = 1 bij
  alle versies vóór de komende refactoring. In het kader van de refactoring zullen we ook backwards compatability helder definiëren.

### Commands

###### enter a machine with ssh
```docker exec -it tg_php /bin/bash```

###### 
```find . -maxdepth 1 -type d -print0 | xargs -0 -I {} sh -c 'echo -e $(find {} | wc -l) {}' | sort -n```


# Docker

### Docker WSL
https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly
alias fle="sed -i 's/\r$//' "

### Docker Sync
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up
docker-compose -f docker-compose.yml -f docker-compose-$(uname -s).yml up

### Node Setup
sudo curl -sL https://deb.nodesource.com/setup_10.x | (which bash - \\) && apt-get install -y nodejs
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

### Ruby / Docker sync
sudo apt-get install -y ruby
sudo apt-get install -y ruby`ruby -e 'puts RUBY_VERSION[/\d+\.\d+/]'`-dev
sudo gem install docker-sync