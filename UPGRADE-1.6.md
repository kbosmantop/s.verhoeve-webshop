# Upgrade from 1.5 to 1.6

Voorbereiding voor doorsturen geschenkbezorgen.
-----
- Referencielijst staat op netwerk.
- Bedrijf aanmaken voor Geschenkbezorgen.nl (k.v.k. 34108906).
- Besteller aanmaken en koppelen aan het bedrijf.
- Factuur adres instellen.
- TG Producten voorzien van referencies naar GB producten (zie leveranciers tab).
- Zie ook bezorgkosten: GB_DELIVERY
- Zie ook personalisatie: GB_DESIGN
- Zie ook brief: GB_LETTER
- Zie ook kaartjes: GB CARD aanmaken en voorzien van GB_CARD + GB ProductID bijv. "GB_CARD_127". --> GB_CARD = Neutraal.

# Paramaters
- Toevoegen  
  - `admin_protocol`: 'https'   
  - `admin_port`: null  