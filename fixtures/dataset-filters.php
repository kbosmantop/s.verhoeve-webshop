<?php

use AdminBundle\Form\Fields\PriceRange;
use AppBundle\Entity\Common\DataSet\DataSetFilterField;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use RuleBundle\Entity\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

$entity = $GLOBALS['kernel']->getContainer()->get("doctrine")->getManager()->getRepository(Entity::class)
    ->findOneBy([
        'fullyQualifiedName' => GenericProduct::class
    ]);

$return = [
    DataSetFilterField::class => [],
];

$return[DataSetFilterField::class]['dataset_filter_product_price'] = [
    'entity' => $entity,
    'path' => '__result.variations.price',
    'label' => 'Prijs Excl. BTW',
    'formType' => PriceRange::class,
    'position' => 1
];

$return[DataSetFilterField::class]['dataset_filter_product_assortment'] = [
    'entity' => $entity,
    'path' => '__result.assortmentProducts.assortment.id',
    'label' => 'Assortiment',
    'placeholder' => 'Maak een keuze',
    'formType' => EntityType::class,
    'multiple' => true,
    'position' => 2
];

$return[DataSetFilterField::class]['dataset_filter_product_productgroup'] = [
    'entity' => $entity,
    'path' => '__result.productgroup.id',
    'label' => 'Productgroep',
    'placeholder' => 'Maak een keuze',
    'formType' => EntityType::class,
    'multiple' => true,
    'position' => 3
];

$return[DataSetFilterField::class]['dataset_filter_product_stock'] = [
    'entity' => $entity,
    'path' => '__result.variations.stock.physicalStock',
    'label' => 'Voorraadbeheer',
    'placeholder' => 'Maak een keuze',
    'formType' => ChoiceType::class,
    'multiple' => false,
    'position' => 4
];

return $return;
