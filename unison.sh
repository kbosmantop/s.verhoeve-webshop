unison C:/phpstorm/topbloemen/ \
    ssh://vagrant@127.0.0.1/./../../vagrant \
    -prefer newer \
    -terse \
    -repeat 1 \
    -ignore "Name {.git,vendor,node_modules,provisioning,var/cache,var/logs,app/logs,app/cache,web/bundles,web/assets,web/js,web/images,web/css,web/media,web/components,web/build,bin/doctrine,bin/doctrine-dbal,bin/doctrine-migrations,bin/doctrine.php,bin/php-parse,bin/phpunit,bin/.phpunit,bin/security-checker,.idea,.DS_Store,unison.sh}" \
    -sshargs "-p 2222  -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -i C:\phpstorm\topbloemen\.vagrant\machines\default\virtualbox\private_key"

##    -fastcheck \
##    -copyonconflict \