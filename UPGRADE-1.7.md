# Upgrade from 1.6 to 1.7
---

# Parameters
- Toevoegen  
  - connector_gb_api_url: `https://www.geschenkbezorgen.nl/api/`
  
- Wijzigen
  - bakery_sync_api_url => connector_bakker_api_url
  
## Connectors
 - getGuzzleClient vereist nu een `$url` parameter (zodat api urls gebruikt kunnen worden (nu parameter))
 
  