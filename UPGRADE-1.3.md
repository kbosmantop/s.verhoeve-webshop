# Upgrade from 1.2 to 1.3

Openstack
-----
Op alle stacks moeten bij de instellingen op de public en private container de juiste domeinnamen worden
toegevoegd aan de CORS headers. Anders werkt de designer niet!

Adyen
-----

* Instellingen voor Adyen dienen vanaf deze versie bij de site ingesteld te horen, waar deze voorheen in de parameters.yml stonden en deels hardcoded waren.

Registratie
-----

Vanaf heden wordt er bij de Customer Entity opgeslagen op welke website zij zich hebben geregistreerd.
Bij het live zetten van deze functionaliteit moeten alle huidig geregistreerde klanten voorzien worden van de juiste site id.
Zodra een Customer entity een ingevulde username/password kolom heeft kan er vanuit gegaan worden dat het een registratie betreft.

Assets
-----

Vanaf heden worden alle JS/CSS assets afgehandeld door Bower, playbook zal dus ook Bower moeten runnen

MySQL Delete rechten
-----

Vanaf heden is het mogelijk om via de command-line alle tabellen in de database op te vragen. Door middel van een parameter
 
 **Toon alle tabellen**
  
  `php bin/console topbloemen:base:list-database-tables`
   
 **Toon tabellen met de soft deletable kolom**
 
 `php bin/console topbloemen:base:list-database-tables --soft-deletable=1`
  
 **Toon tabellen zonder de soft deletable kolom**
 
 `php bin/console topbloemen:base:list-database-tables --soft-deletable=0`
 
Betaalmethode kosten
-----
Het is mogelijk om betaalkosten in te stellen voor betaalmethodes, alle producten met de SKU opgebouwd als:
 
PAY123456
 
Worden uitgelezen op de betaalmethode beheerpagina en kunnen op die manier gekoppeld worden aan een betaalmethode.
 
Teamleader
-----
Bedrijven, contacten en producten worden automatisch gesyncroniseerd naar Teamleader. 
  
De onderstaande commando's dienen éénmalig gedraaid te worden.
  
`php bin/console teamleader:sync-company --all -v`
  
`php bin/console teamleader:sync-customer --all -v`
  
`php bin/console teamleader:sync-product --all -v`
