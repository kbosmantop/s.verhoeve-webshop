let Util = new function () {

};

Util.checkPostcodeAndNumber = function (postcode, number, numberAddition, country, container, callback) {
    if (!postcode || !number || !country || !callback) {
        return;
    }

    if (!$(container).hasClass('loading')) {
        $(container).addClass('loading')

        $.ajax({
            url: '/data/postcode',
            method: 'GET',
            data: {
                postcode: postcode,
                number: number,
                numberAddition: numberAddition,
                country: country
            }
        }).done(function (result) {
            callback(result, container);
        }).fail(function () {
            callback(false, container);
        }).always(function () {
            $(container).removeClass('loading');
        });
    }
};

Util.getPostcodeAndCity = function (q, limit, callback) {
    if (!q) {
        return;
    }

    $.ajax({
        url: '/data/city-and-postcode',
        method: 'GET',
        data: {
            q: q,
            limit: limit
        },
        success: function (result) {
            callback(result);
        },
        error: function () {
            callback(false);
        }
    });
};

export default Util;