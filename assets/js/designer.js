global.Cookies = require('js-cookie');
global.Twig = require('twig');
global.WebFont = require('webfontloader');
global.emojione = require('emojione');

require('fabric');
require('svgxuse');
require('spectrum-colorpicker');
require('spectrum-colorpicker/spectrum.css');
require('svg.js');

require('jquery-ui/themes/base/all.css');
require('jquery-ui/ui/widgets/slider');

require('jquery-ui-touch-punch');

require('emojione/assets/css/emojione.css');

require('topgeschenken_designer/src/js/designer');
require('topgeschenken_designer/src/scss/designer.scss');

$(function () {

    designer_data.extensions = [];

    if(typeof window.DesignerTemplateExtension === 'function') {
        designer_data.extensions.push(new DesignerTemplateExtension());
    }

    if(typeof window.DesignerLockElementExtension === 'function') {
        designer_data.extensions.push(new DesignerLockElementExtension());
    }

    if(typeof window.DesignerEditElementExtension === 'function') {
        designer_data.extensions.push(new DesignerEditElementExtension());
    }

    global.designer = $('#product-designer').designer(designer_data);
});