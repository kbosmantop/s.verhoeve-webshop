let checkoutSuccess = new function () {

};

checkoutSuccess.init = function () {
    var showEmployeeModal = $('.show-employee-modal');

    if (showEmployeeModal && showEmployeeModal.length === 1) {
        this._showEmployeeModal();
    }
};

checkoutSuccess._showEmployeeModal = function () {
    var timer = 10, // timer in seconds
        countdown;

    swal({
        type: 'warning',
        title: trans('pay_attention'),
        html: trans('login_as.message.automatic.logout').replace('%timer%', `<span>${timer}</span>`).replace('%order_collection_number%', `<strong>${orderCollectionNumber}</strong>`),
        showCancelButton: true,
        showConfirmButton: true,
        cancelButtonText: trans('modal.go_on'),
        confirmButtonText: trans('login_as.logout'),
        cancelButtonColor: 'red',
        customClass: 'employee-modal',
        allowOutsideClick: false,
        allowEscapeKey: false,
        useRejections: true // swal7fix
    }).then(() => {
        window.location.href = '/account/logout';
    }, () => {
        clearInterval(countdown);

        swal({
            type: 'info',
            title: trans('please_wait'),
            text: trans('login_as.message.automatic.forwarded'),
            showConfirmButton: false,
            allowOutsideClick: false,
            allowEscapeKey: false,
            timer: 3000,
        }).then(() => {
            window.location.href = '/';
        });
    });

    countdown = setInterval(function () {
        timer--;

        if (timer < 0) {
            clearInterval(countdown);

            swal.close(function () {
                window.location.href = '/account/logout';
            });
            return;
        }

        $('.swal2-content span', '.employee-modal').html(timer);
    }, 1000);
};

checkoutSuccess.init();
