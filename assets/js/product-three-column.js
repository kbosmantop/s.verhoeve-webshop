const timer = require('./includes/timer');

let productThreeColumn = new function () {

};

productThreeColumn.init = function () {
    timer.setTimer();

    this._bindEvents();
};

productThreeColumn._bindEvents = function () {
    var self = productThreeColumn;

    $('.formaat-tab').on('swipeleft', function () {
        self.tabSwipe('right');
    });
    $('.formaat-tab').on('swiperight', function () {
        self.tabSwipe('left');
    });

    $('#formaten .formaat-item').on('click', function (e) {
        var index = $(this).attr('data-index');

        window.location.hash = '#' + index;

        self.selectVariation();
    });

    if (window.location.hash) {
        self.selectVariation();
    }

    $('#product_variation').on('change', function (e) {
        var index = $(this).find('option:selected').index() + 1;

        window.location.hash = '#' + index;

        self.selectVariation();
    });
};
productThreeColumn.scrollToProductContainer = function () {
    $('#product-info-container').fadeIn('slow');

    $.scrollTo('#product-info-container', {
        duration: 800,
        axis: 'y',
        interrupt: true,
        offset: -125,
        easing: 'easeInQuint'
    });
};
productThreeColumn.selectVariation = function (index) {
    if (!index) {
        var index = window.location.hash.substr(1);
    }

    if (index < 0 || index > $('#product_variation option').length) {
        return;
    }

    $('#formaten').addClass('variation-selected');
    $('#formaten .formaat-item').removeClass('selected');
    $('#formaten .formaat-item').eq(index - 1).addClass('selected');

    productThreeColumn.scrollToProductContainer();

    $('#product_variation').find('option').eq(index - 1).prop('selected', true);
    $('#product_price').html($('#product_variation').find('option').eq(index - 1).attr('data-price'));
};

productThreeColumn.tabSwipe = function (direction) {
    var currentPosition = 2;

    if (direction == 'left') {
        if (currentPosition != 1) {
            currentPosition--;
        }
    } else {
        if (currentPosition != 3) {
            currentPosition++;
        }
    }
    switch (currentPosition) {
        case 1:
            $('.formaat-tab .tab').animate({left: '12.5%'}, 300);
            $('#formaat-1').animate({marginTop: '0%',}, 300);
            $('#formaat-1 .foto img').animate({width: '100%',}, 300);
            $('#formaat-2').animate({marginTop: '2.5%',}, 300);
            $('#formaat-2 .foto img').animate({width: '80%',}, 300);
            $('.go-basis').hide();
            $('.go-groot-left').show();
            break
        case 2:
            $('.formaat-tab .tab').animate({left: '-52.5%'}, 300);
            $('#formaat-2').animate({marginTop: '0%',}, 300);
            $('#formaat-2 .foto img').animate({width: '100%',}, 300);
            $('#formaat-1').animate({marginTop: '2.5%',}, 300);
            $('#formaat-1 .foto img').animate({width: '80%',}, 300);
            $('#formaat-3').animate({marginTop: '2.5%',}, 300);
            $('#formaat-3 .foto img').animate({width: '80%',}, 300);
            $('.go-basis').show();
            $('.go-xl').show();
            $('.go-groot-left').hide();
            $('.go-groot').hide();
            break
        case 3:
            $('.formaat-tab .tab').animate({left: '-122.5%'}, 300);
            $('#formaat-3').animate({marginTop: '0%',}, 300);
            $('#formaat-3 .foto img').animate({width: '100%',}, 300);
            $('#formaat-2').animate({marginTop: '2.5%',}, 300);
            $('#formaat-2 .foto img').animate({width: '80%',}, 300);
            $('.go-xl').hide();
            $('.go-groot').show();
            break
    }
};

productThreeColumn.init();
