const productVariables = new function () {
    this.selector = null;
    this.variations = [];
    this.filters = [];
    this.debug = false;
    this.variationProperties = null;
};

productVariables.setVariationProperties = function () {
    if (productVariables.selector.data('properties')) {
        productVariables.variationProperties = productVariables.selector.data('properties');
    }
    return productVariables;
};

productVariables.getVariationProperties = function () {
    return productVariables.variationProperties;
};

productVariables.getVariationProperty = function (id) {
    var properties = productVariables.getVariationProperties();
    for (var key in properties) {
        var property = properties[key];
        if (property.id === id) {
            return property;
        }
    }
    return false;
};

productVariables.setVariations = function () {

    if (productVariables.selector.find('option')) {
        productVariables.selector.find('option').each(function () {
            var elm = $(this);
            var properties = productVariables.getVariationProperties();
            var value = elm.val();

            var variation = elm.data();

            // Fix for issue with data property elements
            for (var key in properties) {
                var property = properties[key];
                variation['property-' + property.id] = elm.attr('data-property-' + property.id);
            }

            if (value.length > 0) {
                variation.id = parseInt(value);
                productVariables.variations.push(variation);
            }
        });
    }

    return productVariables;
};

productVariables.getVariations = function () {
    return productVariables.variations;
};

productVariables.inArray = function (needle, array) {
    for (var key in array) {
        var item = array[key];
        if (typeof item === 'object') {
            item = item.value;
        }
        if (item === needle) {
            return true;
        }
    }
    return false;
};


productVariables.propertyOptionIsOrderable = function (property, option) {
    var filteredVariations = productVariables.getFilteredVariations();
    for (var key in filteredVariations) {
        var variation = filteredVariations[key];
        var orderable = variation['orderable'];
        var propOpt = variation['property-' + property.id];
        if (propOpt === option && orderable) {
            return true;
        }
    }
    return false;
};

/**
 * Get options for the property selector
 * @param property
 * @returns {Array}
 */
productVariables.getPropertyOptions = function (property) {

    var options = [];
    var filteredVariations = productVariables.getFilteredVariations();
    for (var key in filteredVariations) {

        var variation = filteredVariations[key];
        var option = variation['property-' + property.id];

        if (!productVariables.inArray(option, options)) {
            options.push({
                value: option,
                orderable: productVariables.propertyOptionIsOrderable(property, option)
            });
        }
    }

    return options;
};

productVariables.getPropertySelectorOptions = function (property) {
    var html = '';
    var options = productVariables.getPropertyOptions(property);
    html += '<option value="">' + productVariables.selector.find('option:first-child').text() + '</option>';
    for (var key in options) {

        var option = options[key];
        var selected = '';
        var disabled = '';

        var label = option.value;
        if (options.length == 1) {
            selected = ' selected="selected"';
        }

        if (!option.orderable) {
            disabled = ' disabled="disabled"';
            label += ' (' + selector.data('label-out-of-stock') + ')';
        }

        html += '<option value="' + option.value + '"' + selected + disabled + '>' + label + '</option>';
    }
    return html;
};

/**
 * Render selectors for each property as html.
 * @param property
 * @returns {string}
 */
productVariables.createPropertySelector = function (property, level) {

    var klass = '';
    if (level > 1) {
        klass += 'display: none;';
    }

    var html = '<div data-property-id="' + property.id + '" data-level="' + level + '" class="option option-' + property.id + ' ' + property.type + '" style="' + klass + '">';

    html += '<p class="bold">' + property.description + '</p>';

    if (property.type === 'select') {
        html += '<span class="form-select-wrapper">';
        html += '<select class="form-control">';
        html += productVariables.getPropertySelectorOptions(property);
        html += '</select>';
        html += '</span>';
    }

    html += '</div>';

    return html;
};

productVariables.createVariationPropertySelectors = function () {
    var html = '';
    var properties = productVariables.getVariationProperties();
    var level = 0;
    for (var key in properties) {
        level++;
        var property = properties[key];
        html += productVariables.createPropertySelector(property, level);
    }
    return html;
}

productVariables.render = function () {
    var html = productVariables.createVariationPropertySelectors();
    productVariables.selector.closest('.field').after('<div class="product-variables">' + html + '</div>');

    if (!productVariables.debug) {
        productVariables.selector.closest('.field').hide();
    }
};

productVariables.setFilter = function (filter) {

    if (filter.value.length > 0) {
        productVariables.filters['property-' + filter.id] = filter;
    }

    return productVariables;
};

productVariables.getFilters = function () {
    return productVariables.filters;
};

productVariables.clearFilters = function () {
    productVariables.filters = [];

    return productVariables;
};

productVariables.getFilteredVariations = function () {

    var variationFilterIndex = productVariables.getVariations();
    var variationFilter = productVariables.getFilters();
    var variationProperties = productVariables.getVariationProperties();
    var filteredVariations = [];

    if (Object.keys(variationFilter).length === 0) {
        return variationFilterIndex;
    }

    for (var k1 in variationFilterIndex) {

        var variation = variationFilterIndex[k1];
        var inFilter = true;

        for (var k2 in variationFilter) {
            var filter = variationFilter[k2];
            if (!variation['property-' + filter.id] || (variation['property-' + filter.id] !== filter.value)) {
                inFilter = false;
            }
        }
        if (inFilter) {
            filteredVariations.push(variation);
        }

    }

    return filteredVariations;
};

productVariables.setFilters = function (level) {

    var curLevel = parseInt(level);
    var nextLevel = curLevel + 1;
    var fieldHolder = productVariables.selector.closest('.field').parent();

    if (fieldHolder.find('.product-variables .option')) {
        fieldHolder.find('.product-variables .option').each(function () {

            var elm = $(this);
            var elmLevel = elm.data('level');
            var propertyId = parseInt(elm.attr('data-property-id'));
            var value = elm.find('select').val();

            if (elmLevel < nextLevel) {
                productVariables.setFilter({
                    id: propertyId,
                    value: value
                });
            }
        });
    }

    productVariables.toggleFilters(level);
};

productVariables.toggleFilters = function (level) {

    var curLevel = parseInt(level);
    var nextLevel = curLevel + 1;
    var fieldHolder = productVariables.selector.closest('.field').parent();

    if (fieldHolder.find('.product-variables .option')) {

        fieldHolder.find('.product-variables .option').each(function () {

            var elm = $(this);
            var prevElm = elm.prev();
            var elmLevel = elm.data('level');
            var propertyId = parseInt(elm.attr('data-property-id'));
            var value = elm.find('select').val();
            var property = productVariables.getVariationProperty(propertyId);
            var html = productVariables.getPropertySelectorOptions(property);

            if (elmLevel >= nextLevel) {
                elm.find('select').html(html).trigger('change');
                elm.hide();
            }
            if (elmLevel <= nextLevel && (elmLevel === 1 || prevElm.find('select').val() !== '')) {
                elm.show();
            }
        });
    }
};


productVariables.applyFilters = function (level) {

    var curLevel = parseInt(level);
    var nextLevel = curLevel + 1;

    productVariables.clearFilters();
    productVariables.setFilters(level);
    productVariables.selectVariation();
};

productVariables.selectVariation = function () {
    var filteredVariations = productVariables.getFilteredVariations();

    productVariables.selector.find('option').prop('selected', false);
    productVariables.selector.closest('form').find('#form_submit').prop('disabled', true);

    var filtersCount = Object.keys(productVariables.getFilters()).length;
    var propertiesCount = productVariables.getVariationProperties().length;

    if (filteredVariations.length === 1 && (filtersCount === propertiesCount)) {
        var value = filteredVariations[0].id;

        productVariables.selector.closest('form').find('#form_submit').prop('disabled', false);
        productVariables.selector.find('option[value=' + value + ']').prop('selected', true);
    }

    productVariables.selector.trigger('change');
};

productVariables.bindEvents = function () {
    const productVariablesSelect = $('.product-variables .option select');
    productVariablesSelect.on('change', function () {
        var elm = $(this);
        var id = elm.closest('.option').attr('data-property-id');
        var level = elm.closest('.option').attr('data-level');

        productVariables.applyFilters(level);
    });

    // Trigger selects
    productVariablesSelect.trigger('change');
};

productVariables.init = function () {
    var productVariation = $('#product_variation');

    // Check if there are variation
    if (productVariation && productVariation.data('property-select') === true) {
        // Set selector
        productVariables.selector = productVariation;

        // Get variation properties of selector
        productVariables.setVariationProperties();

        // Get variation properties of selector
        productVariables.setVariations();

        // Create selectors for users
        productVariables.render();

        // Set events on selectors.
        productVariables.bindEvents();
    }
};

productVariables.init();
