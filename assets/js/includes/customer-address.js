const Util = require('./../Util').default;

let CustomerAddress = new function () {

};

CustomerAddress.init = function () {
    this._bindEvents();
};

CustomerAddress._bindEvents = function () {
    let self = CustomerAddress;

    $('.country-choice :radio').on('change', function (e) {
        let container = $(this).closest('.address-container');
        let address_container = container;

        if ($(container).attr('data-address-container')) {
            address_container = $('#' + $(container).attr('data-address-container'));
        }

        let value = $('.country-choice :checked', $(container)).val();

        if (value && $(this).is(':checked')) {
            self._triggerCountryAddressElement(address_container, value);

            if (value !== 'other') {
                $('.country .country-list-other', $(container)).val('');
                $('.country .country-list', $(container)).val(value);
            } else {
                $('.country .country-list', $(container)).val($('.country .country-list-other', $(container)).val());
            }
        }
    }).trigger('change');

    if ($('.country-choice').length === 0) {
        $('.country select', '.address-container').on('change', function (e) {
            let container = $(this).closest('.address-container');

            if ($(container).attr('data-address-container')) {
                container = $('#' + $(container).attr('data-address-container'));
            }

            let value = $('.country select', $(container)).val();

            self._triggerCountryAddressElement(container, value);
        }).trigger('change');
    }

    $('.country .country-list-other').on('change', function (e) {
        const elm = $(this);
        const value = elm.val();
        const rowCountryInputs = elm.closest('.row-country-inputs');
        const countryList = rowCountryInputs.find('.country select.country-list');

        if (countryList.length > 0) {
            countryList.val(value);
        } else {
            let container = $(this).closest('.address-container');
            if ($(container).attr('data-address-container')) {
                container = $('#' + $(container).attr('data-address-container'));
            }
            $('.country .country-list', $(container)).val(value);
        }
    });

    let searchAddressDelay = null;
    let searchAddressDelayTime = 0;

    $('.postcode input, .number input, .number-addition', '.address-container').on('change', function (e) {
        let container = $(this).closest('.address-container');
        let country_container = container;
        let country;

        if ($(container).attr('data-country-container')) {
            country_container = $('#' + $(container).attr('data-country-container'));
        }

        if ($('.country-choice').length === 0) {
            country = $('.country select', $(country_container)).val();
        } else {
            let countryRadioActive = $('.country-choice', $(country_container)).find('.radio').length;

            if (countryRadioActive) {
                country = $('.country-choice', $(country_container)).find(':checked').val();
            } else {
                country = $('.country-choice', $(country_container)).val();
            }
        }

        if (country && country.toLowerCase() !== 'nl') {
            return;
        }

        const postcode = $('.postcode input', $(container)).val();
        const number = $('.number input', $(container)).val();
        const numberAddition = $('.number-addition input', $(container)).val();

        if (!e.isTrigger) {
            $('.street input', $(container)).val('');
            $('.city input', $(container)).val('');
        }

        $('.number-addition input', $(container)).closest('.form-group').removeClass('has-danger');

        $('#number_addition_select').select2('destroy').parent().remove();
        $('.number-addition input', $(container)).show();

        searchAddressDelayTime = 0;

        if (e.type === 'keyup') {
            searchAddressDelayTime = 2000;
        }

        clearTimeout(searchAddressDelay);

        container.attr('data-address-searching', 1);

        searchAddressDelay = setTimeout(function () {
            clearTimeout(searchAddressDelay);

            Util.checkPostcodeAndNumber(postcode, number, numberAddition, country, container, self.processPostcodeAndNumber);
        }, searchAddressDelayTime);
    }).trigger('change');

    $('.postcode input', '.address-container').on('keyup', function (e) {
        let value = $(this).val();

        let start = $(this).get(0).selectionStart,
            end = $(this).get(0).selectionEnd;

        value = value.toString().toUpperCase()
        .replace(/\s+/g, '')                   // Replace spaces with -
        .replace(/&/g, '')                  // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')                   // Remove all non-word chars
        .replace(/\-\-+/g, '')                 // Replace multiple - with single -
        .trim();

        $(this).val(value);

        $(this).get(0).setSelectionRange(start, end);
    });

    $('.row-postcode-and-city select').select2({
        // containerCssClass: 'postcode-and-city-select-container',
        // dropdownCssClass: 'postcode-and-city-select-dropdown',
        ajax: {
            data: function (params) {
                return {
                    q: params.term
                };
            },
            url: '/data/city-and-postcode',
            delay: 250,
            dataType: 'json',
            processResults: function (data) {
                let items = [];

                $.each(data.items, function (i, item) {
                    items.push({
                        id: item,
                        text: item
                    });
                });

                return {
                    results: items
                };
            }
        },
        minimumInputLength: 2
    })
    .on('change', function (e) {
        let container = $(this).closest('.address-container');

        $('.postcode input', $(container)).val('');
        $('.city input', $(container)).val('');

        let value = $(this).val();

        if (!value) {
            return;
        }

        let matches = value.match(/([0-9]{4}) ([a-zA-Z]+)/);

        if (matches && matches.length < 2) {
            return;
        }

        $('.postcode input', $(container)).val(matches[1]);
        $('.city input', $(container)).val(matches[2]);
    });
};

CustomerAddress.processPostcodeAndNumber = function (result, container) {
    const postcodeElement = $('.postcode input', $(container));
    const numberElement = $('.number input', $(container));
    const numberAdditionElement = $('.number-addition input', $(container));
    const streetElement = $('.street input', $(container));
    const cityElement = $('.city input', $(container));

    $(container).attr('data-address-searching', 0);
    $(container).attr('data-address-found', 0);

    if (!result || result.length === 0) {
        $('.row-street-and-city input', $(container));
        $('.row-street-and-city .street input', $(container)).focus();
        $(container).trigger('getDeliveryDate');

        return;
    }

    const currentNumberAddition = numberAdditionElement.val();
    const houseNumberAdditionsLenght = result.houseNumberAdditions.length;

    postcodeElement.val(result.postcode).addClass('filled');
    numberElement.val(result.houseNumber).addClass('filled');

    streetElement.val(result.street).addClass('filled').trigger('change');
    cityElement.val(result.city).addClass('filled').trigger('change');

    if (currentNumberAddition && result.houseNumberAddition) {
        numberAdditionElement.val(result.houseNumberAddition).addClass('filled');
    }

    if (houseNumberAdditionsLenght > 1 || (houseNumberAdditionsLenght === 1 && result.houseNumberAdditions[0].length > 0)) {
        let numberAdditionOptions = [];

        for (let i = 0; i < houseNumberAdditionsLenght; i++) {
            numberAdditionOptions.push('<option value="' + result.houseNumberAdditions[i] + '"' + ((result.houseNumberAdditions[i].toLowerCase() === currentNumberAddition.toLowerCase()) ? ' selected="selected"' : '') + '>' + result.houseNumberAdditions[i] + '</option>');
        }

        const numberAdditionSelect = $('<span class="form-select-wrapper"><select name="number-addition-select" id="number_addition_select" class="form-control">' + numberAdditionOptions.join('') + '</select></span>');

        numberAdditionElement.hide();
        numberAdditionSelect.insertBefore(numberAdditionElement);
        numberAdditionSelect.find('select').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            placeholder: '-'
        });

        $('#number_addition_select').on('change', function (e) {
            numberAdditionElement.val($(this).val());
        });
    }

    $(container)
    .attr('data-address-found', 1)
    .attr('data-address-searched-postcode', result.postcode)
    .attr('data-address-searched-houseNumber', result.houseNumber)
    .attr('data-address-searched-street', result.street)
    .attr('data-address-searched-city', result.city)
    ;

    $(container).trigger('getDeliveryDate');
};

CustomerAddress._triggerCountryAddressElement = function (container, choice) {
    let country_container = container;

    if ($(container).attr('data-country-container')) {
        country_container = $('#' + $(container).attr('data-country-container'));
    }

    switch (choice.toLowerCase()) {
        case 'nl':
            $('.row-postcode-and-city', $(container)).hide();
            $('.row-country .country .country-list-other', $(country_container)).hide();
            $('.row-country .country', $(country_container)).hide();
            break;
        case 'be':
            $('.row-postcode-and-city', $(container)).show();
            $('.row-country .country', $(country_container)).hide();
            break;
        case 'other':
            $('.row-postcode-and-city', $(container)).hide();
            $('.row-country .country .country-list-other', $(country_container)).show();
            $('.row-country .country', $(country_container)).show();
            break;
    }
};

CustomerAddress._movePostcodeToCity = function (container) {
    let postcode = $('.row .postcode', $(container)).clone(true);

    $('.row .postcode:first', $(container)).remove();

    $(postcode).insertBefore($('.city', $(container)));
};

CustomerAddress._movePostcodeToNumber = function (container) {
    let postcode = $('.row .postcode', $(container)).clone(true);

    $('.row .postcode:first', $(container)).remove();

    $(postcode).insertBefore($('.street', $(container)));
};

CustomerAddress._moveStreetToPostcode = function (container) {
    let street = $('.row .street', $(container)).clone(true);

    $('.row .street:first', $(container)).remove();

    $(street).insertBefore($('.number', $(container)));

    $('.city, .street', $(container)).removeClass('col-md-6').addClass('col-md-8');
    $('.street', $(container)).hide();

    $('.city', $(container)).closest('.col-md-12').removeClass('col-md-12').addClass('col-md-8');
    $('.city', $(container)).hide();
};

CustomerAddress._moveStreetToCity = function (container, invoice) {
    let street = $('.street', $(container)).clone(true);

    $('.street:first', $(container)).remove();

    if (invoice) {
        $(street).appendTo($('.row-invoice-street', $(container)));

        $('.city, .street', $(container)).removeClass('col-md-8').addClass('col-md-12').show();
        $('.city, .street', $(container)).show();
        $('.row-invoice-city, .row-invoice-street', $(container)).show();
    } else {
        $(street).insertBefore($('.city', $(container)));

        $('.city, .street', $(container)).removeClass('col-md-8').addClass('col-md-6').show();
        $('.city', $(container)).closest('.col-md-8').removeClass('col-md-8').addClass('col-md-12');
    }
};

module.exports = CustomerAddress;
