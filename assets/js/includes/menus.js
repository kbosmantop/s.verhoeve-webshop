let topmenu = new function () {

};

topmenu.init = function () {
    topmenu.bindEvents();
};

topmenu.bindEvents = function () {
    $('.btn-klantenservice').on('click', function(e) {
        if($(this).hasClass('active')) {
            topmenu.hideDropdown();
        } else {
            topmenu.showDropdown();
        }
    });

    $('.klantenservice-dropdown').on('mouseenter mouseleave', function (e) {
        switch (e.type) {
            case 'mouseenter':
                topmenu.showDropdown();

                break;
            case 'mouseleave':
                topmenu.hideDropdown();

                break;
        }
    });
};

topmenu.showDropdown = function () {
    $('.btn-klantenservice').addClass('active');
    $('.btn-klantenservice .up').toggle();
    $('.btn-klantenservice .down').toggle();
    $('.klantenservice-dropdown').show();
};

topmenu.hideDropdown = function () {
    $('.btn-klantenservice .up').toggle();
    $('.btn-klantenservice .down').toggle();
    $('.btn-klantenservice').removeClass('active');
    $('.klantenservice-dropdown').hide();
};

let menu = new function () {

};

let submenu = new function () {

};

module.exports = {
    topmenu: topmenu,
    menu: menu,
    submenu: submenu,
};