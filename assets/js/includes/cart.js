require('./../../scss/_components/_cart.scss');
require('./jquery.qty.plugin');

let Cart = new function () {

};

Cart.init = function () {
    Cart.applyQuantityPlugin();
};

Cart.applyQuantityPlugin = function (parent) {
    if (!parent) {
        parent = $('body');
    }

    $('.qty-selector', parent).qty();
};

Cart.updateOrderLine = function (url, data) {
    return $.ajax({
        url: url,
        method: 'POST',
        data: data
    });
};

Cart.removeOrderLine = function (url) {
    return $.ajax({
        url: url,
        method: 'DELETE',
        data: {
            quantity: 0
        }
    });
};

Cart.showEditCardModal = function (url, callback) {
    swal({
        title: trans('cart.order_edit_card.title')
    });

    swal.showLoading();

    $.get(url).then(function (html) {
        swal({
            title: trans('cart.order_edit_card.title'),
            html: html,
            showCancelButton: true,
            confirmButtonText: trans('cart.order_edit_card.button'),
            cancelButtonText: trans('modal.cancel'),
            customClass: 'cart-modal-edit-order-card',
            useRejections: true, // swal7fix
            width: '41em',
            preConfirm: function () {
                return new Promise((resolve, reject) => {
                    const cardId = $('input.radio-card:checked', cartEditCardModal).val();
                    const cardText = $('textarea', cartEditCardModal).val();

                    if (!cardId) {
                        // reject(trans('cart.order_edit_card.no_card_selected'));
                        swal.showValidationError(trans('cart.order_edit_card.no_card_selected')); // swal7fix
                        swal.hideLoading();
                        return;
                    }

                    if (!cardText) {
                        // reject(trans('cart.order_edit_card.empty_card_text'));
                        swal.showValidationError(trans('cart.order_edit_card.empty_card_text')); // swal7fix
                        swal.hideLoading()

                        return;
                    }

                    let data = {
                        cardId: cardId,
                        cardText: cardText
                    };

                    Cart.updateOrderLine(url, data).done(function () {
                        resolve();
                    }).fail(function () {
                        // reject(trans('modal.error'));
                        swal.showValidationError(trans('modal.error')); // swal7fix
                    });
                });
            },
            showLoaderOnConfirm: true
        }).then(function () {
            if (callback) {
                callback();
            }
        }, function () {
        });

        let cartEditCardModal = $('.cart-modal-edit-order-card');

        cartEditCardModal.on('keydown keyup', 'textarea', function (e) {
            switch (e.type) {
                case 'keydown':
                    if (parseInt($(this).get(0).scrollHeight) <= parseInt($(this).outerHeight())) {
                        $.data(this, 'value', $(this).val());
                    }

                    break;
                case 'keyup':
                    if (parseInt($(this).get(0).scrollHeight) > parseInt($(this).outerHeight())) {
                        if ($.data(this, 'value').length > 0) {
                            $(this).val($.data(this, 'value'));
                        } else {
                            $(this).val($(this).val().substr(0, -1)).trigger('keyup');
                        }
                    }

                    break;
            }
        });

        cartEditCardModal.on('change', 'input.radio-card', function () {
            const cardImage = $('input.radio-card:checked').attr('data-image');

            let cardImageContainer = $('.card-preview .image', cartEditCardModal);

            if (cardImage) {
                cardImageContainer.css({
                    'background-image': 'url(\'' + cardImage + '\')'
                });
            } else {
                cardImageContainer.css({
                    'background-image': ''
                });
            }
        });

        $('input.radio-card', cartEditCardModal).trigger('change');
    });
};

Cart.showEditDesignerModal = function (url) {
    swal({
        title: trans('cart.order_edit_designer.title')
    });

    swal.showLoading();

    $.ajax({
        url: url
    }).then(function (response) {
        swal({
            title: trans('cart.order_edit_designer.title'),
            html: '<img src="' + response.previewImageUrl + '" />',
            showCancelButton: true,
            confirmButtonText: trans('cart.order_edit_designer.button'),
            cancelButtonText: trans('modal.cancel'),
            customClass: 'cart-modal-show-designer',
            useRejections: true // swal7fix
        }).then(function () {
            window.location.href = response.designerUrl;
        }, function () {
        });
    });
};

Cart.showEditLetterModal = function (url) {
    swal({
        title: trans('cart.order_edit_letter_title')
    });

    swal.showLoading();

    $.get(url).then(function (html) {
        swal({
            title: trans('cart.order_edit_letter_title'),
            html: html,
            showCancelButton: true,
            confirmButtonText: trans('cart.order_edit_letter.button'),
            cancelButtonText: trans('modal.cancel'),
            customClass: 'cart-modal-edit-order-card',
            reverseButtons: true,
            useRejections: true, // swal7fix
            preConfirm: function () {
                return new Promise((resolve, reject) => {
                    let letter = $('input#letter_file');
                    let files = letter.prop('files');

                    if (files.length === 0) {
                        // reject(trans('cart.order_edit_letter.error'));
                        swal.showValidationError(trans('cart.order_edit_letter.error')); // swal7fix
                        return false
                    }

                    const reader = new FileReader;
                    reader.onload = (e) => {
                        let data = {};
                        let formToken = $('#letter__token');

                        data[formToken.attr('name')] =  formToken.val();
                        data[letter.attr('name')] =  e.target.result;

                        Cart.updateOrderLine(url, data).done(function (data) {
                            if (data.status === 'success') {
                                resolve(trans('cart.order_edit_letter.success'));
                            } else {
                                // reject(data.message);
                                swal.showValidationError(data.message); // swal7fix
                            }
                        }).fail(function () {
                            // reject(trans('modal.error'));
                            swal.showValidationError(trans('modal.error')); // swal7fix
                        });
                    };

                    reader.readAsDataURL(files[0]);
                });
            },
            showLoaderOnConfirm: true
        }).then(function () {
            window.location.reload();
        }, function () {
        });
    });
};

Cart.showRemoveProductModal = function (url) {
    return swal({
        type: 'question',
        text: trans('checkout.delivery.modal_removal_confirm.text'),
        showCancelButton: true,
        confirmButtonText: trans('modal.confirm_removal'),
        cancelButtonText: trans('modal.cancel'),
        showLoaderOnConfirm: true,
        useRejections: true, // swal7fix
        preConfirm: function () {
            return new Promise((resolve, reject) => {
                Cart.removeOrderLine(url).done(function (response) {
                    resolve(response);
                }).fail(function () {
                    // reject(trans('modal.error'));
                    swal.showValidationError(trans('modal.error')) // swal7fix
                });
            });
        }
    });
};

Cart.showRemovePersonalizationProductModal = function (url) {
    return swal({
        type: 'question',
        text: trans('checkout.delivery.modal_removal_confirm.text'),
        showCancelButton: true,
        confirmButtonText: trans('modal.confirm_removal'),
        cancelButtonText: trans('modal.cancel'),
        showLoaderOnConfirm: true,
        useRejections: true, // swal7fix
        preConfirm: function () {
            return new Promise((resolve, reject) => {
                Cart.removeOrderLine(url).done(function () {
                    resolve();
                }).fail(function () {
                    // reject(trans('modal.error'));
                    swal.showValidationError(trans('modal.error')) // swal7fix
                });
            });
        }
    });
};

module.exports = Cart;
