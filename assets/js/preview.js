require('./../scss/preview.scss');
require('./../img/preview_bg.jpg');

$(function () {
    // Create the iFrame
    var iframe = document.createElement('iframe');

    // Get the current document height and width
    var docHeight = $(document).height() - $('#header').height();
    var docWidth = $(document).width();

    // iFrame attributes
    iframe.src = $('body').attr('data-preview-url');

    iframe.className = 'viewport';
    iframe.height = docHeight;
    iframe.width = docWidth;

    // Append to body
    document.body.appendChild(iframe);

    // Scrollbar test
    var scrollBars = document.createElement('div');
    scrollBars.className = 'scroll-test';
    document.body.appendChild(scrollBars);

    // Find the scrollbar width
    var scrollbarWidth = scrollBars.offsetWidth - scrollBars.clientWidth;
    document.body.removeChild(scrollBars);

    // Using jQuery on for dynamic element click events
    $('#viewports').on('click', 'label', function () {
        // For any list item add/remove 'active' class
        $(this).addClass('active').siblings().removeClass('active');

        // If the class is the reset button, reset the width
        // Else, animate the viewport and add any scrollbar widths
        if ($(this).hasClass('reset')) {
            $(iframe).css({'display': 'block'}).animate({width: docWidth}, 'medium');
        } else {
            var dataWidth = this.getAttribute('data-width');
            var fullWidth = +dataWidth + +scrollbarWidth;

            $(iframe).css({'display': 'block'}).animate({width: fullWidth}, 'medium');
        }
    });

    $('[data-action="close"]', '#header').on('click', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        if (!href) {
            return;
        }

        swal({
            type: 'question',
            title: trans('modal.title.are_you_sure'),
            text: trans('modal.preview.message.close'),
            showCancelButton: true,
            confirmButtonText: trans('modal.close'),
            cancelButtonText: trans('modal.preview.message.back'),
        }).then(() => {
            window.parent.location.href = href;
        });
    });

    // $('[data-tooltip="true"]').tooltip({
    //     container: 'body',
    //     trigger: 'hover'
    // });
    //
    // $('.btn', '#viewports').on('click', function () {
    //     $(this).tooltip('hide');
    // });
});
