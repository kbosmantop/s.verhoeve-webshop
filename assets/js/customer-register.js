require('./../scss/customer-register.scss');

const CustomerAddress = require('./includes/customer-address');

let customerRegister = new function () {

};

customerRegister.init = function () {
    this._bindEvents();
};

customerRegister._bindEvents = function () {
    CustomerAddress.init();

    $('.invoice-address-container input, .invoice-address-container select').on('change', function(e){
        const field = $(this).data('invoice-input');
        const value = (field !== 'country')? e.currentTarget.value : e.currentTarget[e.currentTarget.selectedIndex].innerText;
        $('.invoice-address-' + field).text(value);
    });

    if ($('#invoice_address_edit').length == 1) {
        $('#invoice_address_edit').on('click', function (e) {
            e.preventDefault();

            $('.invoice-address-container').toggle();
        });
    }

    $('form[name=company]').on('submit', function (e) {
        if ($('#company_chamberOfCommerceNumber').val()) {
            $.blockUI({
                message: $('#block-loading-message').html(),
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.4,
                    zIndex: 9999,
                    cursor: 'wait',
                    textAlign: 'center'
                },
                css: {
                    width: 200,
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    padding: 10,
                    border: 0,
                    backgroundColor: '#ddd',
                    color: '#373a3c',
                    zIndex: 10000,
                    left: '50%',
                    marginLeft: '-100px'
                }
            });
        }
    });

    if ($('#content-account-company-register[data-auto-submit]').length == 1) {
        $('form[name=company]').submit();
    }
};

customerRegister.init();