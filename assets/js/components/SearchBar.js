export default class SearchBar {
    constructor() {
        this.elm = $('.search-bar');
        this.formElm = $('form', this.elm);
        this.inputElm = $('input[name="q"]', this.elm);
        this.minInputValueLength = 3;

        this.bindEvents();
    }

    bindEvents() {
        this.elm.find('input').on('keyup', this.onInputKeyUp.bind(this));
        this.elm.on('submit', this.onFormSubmit.bind(this));
    }

    onFormSubmit(e) {
        const formElm = $(e.currentTarget);
        const value = $('input', formElm).val();

        if (value.length <= (this.minInputValueLength - 1)) {
            e.preventDefault();

            this.showFormHelper(trans('error.search.min_length'))
        }
    }

    onInputKeyUp(e) {
        const value = $(e.currentTarget).val();

        if (value.length > (this.minInputValueLength - 1)) {
            this.removeFormHelper();
        }
    }

    removeFormHelper() {
        $('.form-helper', this.elm).remove();
    }

    showFormHelper(message) {
        const helperElm = Object.assign(document.createElement('div'), {
            'className': 'form-helper form-control',
            'innerText': message
        });

        this.removeFormHelper();
        this.formElm.after(helperElm);
    }
}