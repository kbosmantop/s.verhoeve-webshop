export default class ContentBlock {
    constructor() {
        this.elm = $('.content-block');
        this.productCollectionElm = $('.product-collection');
        this.firstSmallProductItemElm = $('.product-item-small', this.productCollectionElm).eq(0);
        this.firstSmallProductItemChildElm = $('.product-item', this.firstSmallProductItemElm);

        this.contentWidth = this.elm.data('content-width');
        this.contentHeight = this.elm.data('content-height');

        this.calcMarginRight = 15;

        this.bindEvents();
    }

    bindEvents() {
        this.calcSize();

        $(window).resize(this.onWindowResize.bind(this));
    }

    onWindowResize() {
        this.calcSize();
    }

    calcSize() {
        if(this.elm.length === 0) {
            return false;
        }

        if(this.firstSmallProductItemElm.length === 0) {
            this.elm.hide(); // Can't define content block sizes.
            return false;
        }

        this.windowWidth = $(window).width();
        this.calcHeight = this.firstSmallProductItemChildElm.outerHeight();
        this.calcMarginBottom = this.firstSmallProductItemElm.outerHeight() - this.firstSmallProductItemChildElm.outerHeight();
        this.calcRowItems = Math.round(this.productCollectionElm.innerWidth() / this.firstSmallProductItemElm.outerWidth());

        if(this.calcRowItems >= 2 && this.windowWidth >= 768) {
            const contentBlockHeight = (this.calcHeight * this.contentHeight) + ((this.contentHeight * this.calcMarginBottom) - this.calcMarginBottom);

            this.elm.height(contentBlockHeight);
        } else {
            this.elm.height('auto');
        }
    }
}