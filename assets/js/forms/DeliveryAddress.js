export default class DeliveryAddress {
    constructor(selector) {
        this.elm = $(selector);
        this.type = 1;

        const defaultIndex = [
            'cart_order_deliveryAddressAttn',
            'cart_order_deliveryAddressPostcode',
            'cart_order_deliveryAddressNumber',
            'cart_order_deliveryAddressNumberAddition',
            'cart_order_deliveryAddressStreet',
            'cart_order_deliveryAddressCity',
            'cart_order_deliveryAddressPhoneNumber',
            'cart_order_deliveryRemark',
        ];

        const companyIndex = ['cart_order_deliveryAddressCompanyName', ...defaultIndex];
        const hospitalIndex = ['cart_order_deliveryAddressCompanyName', ...defaultIndex];

        this.tabIndexes = {
            1: defaultIndex,
            2: companyIndex,
            3: hospitalIndex
        };

        this.bindEvents();
    }

    onChangeType(e) {
        this.type = e.currentTarget.value;
        this.updateTabIndexes();
    }

    bindEvents() {
        const typeField = $('#cart_order_deliveryAddressType', this.elm);

        this.updateTabIndexes();

        typeField.on('change', this.onChangeType.bind(this));
    }

    focusFirstField() {
        if(this.tabIndexes[this.type][0]){
            $(`#${this.tabIndexes[this.type][0]}`).focus();
        }
    }

    updateTabIndexes() {
        this.elm.find('input, select').each( (i, elm) => {
           const field = $(elm);
           const fieldId = field.attr('id');

           field.removeAttr('tab-index');

           if(this.tabIndexes[this.type] && this.tabIndexes[this.type].hasOwnProperty(fieldId)) {
               field.attr('tab-index', this.tabIndexes[this.type].indexOf(fieldId) + 1);
           }
        });

        this.focusFirstField();
    }
};
