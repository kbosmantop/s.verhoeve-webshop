require('./../scss/checkout-cart.scss');

const Cart = require('./includes/cart');

const AddressBook = require('./address-book');

let checkoutCart = function () {
    new AddressBook();
    this.container = $('#checkout_cart');

    this.lazy = new LazyLoad({
        elements_selector: ".lazy"
    });

    this.init();
};

checkoutCart.prototype.init = function () {
    this.bindEvents();
    this.initializeQtySelectors();
};

checkoutCart.prototype.refresh = function (callback) {
    const self = this;

    $(this.container).load(window.location.href + ' #checkout_cart > div', {}, function() {
        self.initializePopovers();
        self.initializeQtySelectors();

        self.hideOrderLoading();

        self.lazy = new LazyLoad({
            elements_selector: ".lazy"
        });

        if(typeof callback === 'function') {
            callback()
        }
    });
};

checkoutCart.prototype.refreshTotals = function (html) {
    $(this.container).find('.totals').replaceWith(html);
};

checkoutCart.prototype.initializeQtySelectors = function () {
    const self = this;

    $('.qty-selector', self.container).qty();

    $('.qty-selector :input', self.container).on('qty-update-started',function () {
        self.showOrderLoading()
    });

    $('.qty-selector :input', self.container).on('qty-updated',function () {
        const orderId = $(this).closest('.order').attr('data-order-id');

        self.refresh(() => {
            self.openOrder($(`.order[data-order-id="${orderId}"]`));
        });
    });
}

checkoutCart.prototype.initializePopovers = function () {
    $('[data-toggle="popover"]', self.container).each(function (i, elm) {
        new Popover(elm, {
            trigger: 'hover',
            placement: 'top'
        });
    });
};

checkoutCart.prototype.showOrderLoading = function () {
    $('.order.active', this.container).toggleClass('loading', true)
};

checkoutCart.prototype.hideOrderLoading = function () {
    $('.order.loading.active', this.container).removeClass('loading')
};

checkoutCart.prototype.bindEvents = function () {
    const self = this;
    const container = $(self.container);

    self.openOrder(container.find('.order:last-child'))

    $('.qty-selector').qty();
    this.initializePopovers();


    container.on('click', '[data-action="add-voucher"]', (e) => {
        e.preventDefault();

        const item = $(e.currentTarget);
        const html = `
            <form method="POST" id="cart_voucher_form" action="${item.attr('href')}">
                <input type="text" name="cart_voucher" id="cart_voucher" placeholder="${trans('cart.coupon_add_placeholder')}" class="form-control">
            </form>
        `;

        swal({
            title: trans('cart.modal.coupon_add.title'),
            html: html,
            showCancelButton: true,
            confirmButtonText: trans('cart.modal.coupon_add.submit'),
            cancelButtonText: trans('modal.cancel'),
            showLoaderOnConfirm: true,
            reverseButtons: true,
            useRejections: true, // swal7fix
            preConfirm: () => {
                return new Promise(function (resolve, reject) {
                    if (!$('#cart_voucher').val()) {
                        // reject(trans('cart.modal.coupon_add.blank'))
                        swal.showValidationError(trans('cart.modal.coupon_add.blank')); // swal7fix
                    } else {
                        $('#cart_voucher_form').submit();

                        resolve();
                    }
                });
            },
        }).then(() => {
            self.refresh()
        });
    });

    container.on('click', '.orders .order-heading', function (e) {
        const order = $(e.currentTarget).closest('.order');
        const count = $(self.container).find('.orders').find('.order').length;

        if (count > 1 || (count === 1 && !order.hasClass('active'))) {
            self.openOrder(order);
        }
    });

    // Remove product
    container.on('click', 'span[data-action="remove-product"]', function (e) {
        e.stopPropagation();
        e.preventDefault();

        const url = $(this).attr('data-url');
        const element = $(this);

        Cart.showRemoveProductModal(url).then(function (response) {
            if (false === response.hasOwnProperty('dismiss')) {
                if (!response.count || parseInt(response.count) === 0) {
                    window.location.href = '/';

                    return;
                } else {
                    const orderId = element.closest('.order').attr('data-order-id');

                    self.refresh(() => {
                        self.openOrder($(`.order[data-order-id="${orderId}"]`));
                    });

                    swal.close();
                }
            }
        });
    });

    // Remove card
    container.on('click', '[data-action="remove-card"], [data-action="remove-personalization"], [data-action="remove-letter"]', function (e) {
        e.stopPropagation();
        e.preventDefault();

        const url = $(this).attr('data-url');
        const element = $(this);

        Cart.showRemovePersonalizationProductModal(url).then(function (response) {
            if (false === response.hasOwnProperty('dismiss')) {
                const orderId = element.closest('.order').attr('data-order-id');

                self.refresh(() => {
                    self.openOrder($(`.order[data-order-id="${orderId}"]`));
                });

                swal.close();
            }
        });
    });

    // Edit card
    container.on('click', 'a[data-action="edit-card"]', function (e) {
        e.stopPropagation();
        e.preventDefault();

        const url = $(this).attr('href');
        const element = $(this);

        Cart.showEditCardModal(url, function () {
            const orderId = element.closest('.order').attr('data-order-id');

            self.refresh(() => {
                self.openOrder($(`.order[data-order-id="${orderId}"]`));
            });

            swal.close();
        });
    });

    // Edit personalization
    container.on('click', 'a[data-action="edit-personalization"]', function (e) {
        e.stopPropagation();
        e.preventDefault();

        const url = $(this).attr('href');

        Cart.showEditDesignerModal(url);
    });

    // Edit letter
    container.on('click', 'a[data-action="edit-letter"]', function (e) {
        e.stopPropagation();
        e.preventDefault();

        const url = $(this).attr('href');

        Cart.showEditLetterModal(url);
    });

    container.on('click', '[data-action="duplicate"]', function (e) {
        e.preventDefault();

        const href = $(this).attr('data-url');

        swal.setDefaults({
            cancelButtonText: trans('modal.cancel')
        });

        swal({
            title: trans('cart.order_duplicate_confirm.title'),
            html: `<span class="fa-stack fa-3x">\n
                    <i class="fa fa-circle fa-stack-2x"></i>\n
                    <i class="fa fa-refresh fa-stack-1x fa-inverse"></i>\n
                    </span><br/>
                    ${trans('cart.order_duplicate_confirm.text.html')}`,
            showLoaderOnConfirm: true,
            confirmButtonText: trans('cart.order_duplicate_confirm.button'),
            showCancelButton: true,
            preConfirm: () => {
                $('.swal2-cancel').hide();

                window.location.href = href;
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    });
};

checkoutCart.prototype.closeOrders = function (element) {
    $(this.container).find('.order').not(element).removeClass('active');
    $(this.container).find('.order-heading').not(element.find('.order-heading')).removeClass('theme-background-color');
};

checkoutCart.prototype.openOrder = function (element) {
    this.closeOrders(element);

    $(element).toggleClass('active', true);
    $(element).find('.order-heading').toggleClass('theme-background-color', true);

    this.lazy.update();
};

new checkoutCart();
