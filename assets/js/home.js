import '../scss/home.scss';

$(document).ready(function () {
    let slickOptions = {
        infinite: true,
        dots: false,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1000,
                settings:
                    {
                        slidesToShow: 2
                    }

            },
            {
                breakpoint: 600,
                settings:
                    {
                        slidesToShow: 2
                    }

            },
            {
                breakpoint: 480,
                settings:
                    {
                        slidesToShow: 1
                    }

            }
        ],
        prevArrow: '<span class="fa fa-chevron-left slick-prev"></span>',
        nextArrow: '<span class="fa fa-chevron-right slick-next"></span>'
    };

    $('.product-slider-wrapper').slick(slickOptions);
    $('.review-slider').slick(slickOptions);
});
