require('./../js/checkout-delivery.js');

require('./../scss/_components/_cart.scss');
require('./../scss/checkout-additional-product.scss');

const Cart = require('./includes/cart');

let checkoutAdditionalProducts = new function () {
    this.additionalProductsContainer = $('.additional-products');
};

checkoutAdditionalProducts.init = function () {
    this.bindEvents();
};

checkoutAdditionalProducts.bindEvents = function () {
    const self = checkoutAdditionalProducts;

    Cart.init();

    self.additionalProductsContainer.on('click', '[data-action="add-to-cart"]', function (e) {
        e.preventDefault();

        $(this).blur();

        const item = $(e.currentTarget).closest('.additional-product');

        const dataVariations = item.attr('data-variations');
        let variations = [];
        if (dataVariations) {
            variations = JSON.parse(dataVariations);
        }

        if(variations.length === 1) {
            self.addToCart(variations[0].cart_url);
            self.process(item);
            return;
        }

        item.find('a[data-action="show-description"]').trigger('click');
    });

    self.additionalProductsContainer.on('click', '.image', function (e) {
        e.preventDefault();

        $(this).closest('.additional-product').find('[data-action="show-description"]').trigger('click');
    });

    self.additionalProductsContainer.on('click', '[data-action="show-description"]', function (e) {
        e.preventDefault();

        const item = $(e.currentTarget).closest('.additional-product');

        $(this).blur();

        const dataVariations = item.attr('data-variations');

        let variations = [];
        if (dataVariations) {
            variations = JSON.parse(dataVariations);
        }

        const variationOptions = {};
        for (let i = 0; i < variations.length; i++) {
            variationOptions[variations[i].id] = variations[i].name;
        }

        swal({
            customClass: 'additional-product-modal',
            title: item.find('.title').text(),
            html: `<div class="additional-product-modal-text-wrapper"><span class="variation-text-wrapper theme-color pull-left">${trans('cart.modal.choose_one_option_below')}</span></div>`,
            showLoaderOnConfirm: true,
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: trans('modal.add'),
            cancelButtonText: trans('modal.close'),
            reverseButtons: true,
            input: 'select',
            inputOptions: variationOptions,
            inputClass: 'form-control additional-product-modal-variations',
            useRejections: true, // swal7fix
            preConfirm: (variationId) => {
                $('.additional-product-modal-text-wrapper, .additional-product-modal-variation-wrapper').hide();

                const html = `<span class="additional-product-modal-variation-loading-text">
                        <strong>${trans('please_wait')}</strong><br />
                        ${trans('cart.product.adding')}
                    </span>`;

                $(html).appendTo($('.swal2-content', '.additional-product-modal'));

                $('.swal2-cancel', '.additional-product-modal').hide();

                const selectedItem = $('option[value=' + variationId + ']', '.additional-product-modal-variations-placeholder');

                return self.addToCart(selectedItem.attr('data-cart-url'));
            },
            onOpen: function (dialog) {
                const origSelect = $('.additional-product-modal-variations:visible', dialog);
                const variationWrapper = $('<div class="additional-product-modal-variation-wrapper"></div>').insertAfter(origSelect);
                const selectWrapper = $('<span class="form-select-wrapper pull-left"></span>').appendTo(variationWrapper);
                const priceWrapper = $('<span class="price-wrapper pull-right"></span>').insertAfter(selectWrapper);
                const newSelect = origSelect.clone().addClass('additional-product-modal-variations-placeholder');
                const options = [];

                const imageContainer = $('<div class="additional-product-variation-image"><img /></div>').prependTo($('.swal2-content', dialog));

                if (item.attr('data-description')) {
                    $('<div>' + item.attr('data-description') + '</div>').insertAfter(imageContainer);
                }

                for (let i = 0; i < variations.length; i++) {
                    options.push(
                        '<option value="' + variations[i].id + '" data-cart-url="' + variations[i].cart_url + '" data-image="' + variations[i].image + '" data-price="' + variations[i].price + '">' + variations[i].name + '</option>'
                    );
                }

                newSelect.html(options.join(''));

                var isSingleVariation = variations.length === 1;
                $('.additional-product-modal-variation-wrapper').toggleClass('single-additional-product', isSingleVariation);
                $('.variation-text-wrapper').toggle(!isSingleVariation);

                newSelect.appendTo(selectWrapper).on('change', function () {
                    const option = $(this).find('option:selected');

                    origSelect.val($(this).val());

                    imageContainer.find('img').attr('src', option.attr('data-image'));

                    priceWrapper.html('à ' + option.attr('data-price'));
                }).trigger('change');

                origSelect.hide();
            },
            allowOutsideClick: false
        }).then((response) => {
            $('.additional-product-modal-text-wrapper, .additional-product-modal-variation-wrapper').hide();

            self.process(item, response);
        }, (dismiss) => {
            self.dismiss(item, dismiss);
        });
    });

    $('#additional_products_load_trigger').on('click', function (e) {
        e.preventDefault();

        const elm = $(this);

        // Show loading indicator
        $(elm)
        .toggleClass('loading', true)
        .attr('disabled', true);

        $.ajax({
            url: $(this).attr('href'),
            method: 'POST',
            dataType: 'json'
        }).done(function (response) {
            if (response.hasOwnProperty('paginationUrl')) {
                $(elm).attr('href', response.paginationUrl);
            } else {
                $(elm).remove();
            }

            if (response.hasOwnProperty('html')) {
                $(response.html).appendTo($('#extra-items'));
            }
        }).fail(function (response) {

        }).always(function () {
            // Remove loading indicator
            $(elm)
            .toggleClass('loading', false)
            .removeAttr('disabled')
            .blur();
        });
    });
};

checkoutAdditionalProducts.addToCart = function (url) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            method: 'POST'
        }).done(function (response) {
            resolve(response);

            $('.buttons:last').hide();
        }).fail(function () {
            $('.buttons:last').show();

            reject(trans('cart.order_edit_additional.add.error'));

            $('.swal2-cancel', '.additional-product-modal').show();
            $('.swal2-confirm', '.additional-product-modal').remove();
        });
    });
};

checkoutAdditionalProducts.dismiss = function (item, dismiss) {

};

checkoutAdditionalProducts.process = () => {
    checkoutAdditionalProducts.refresh();

    swal({
        type: 'success',
        title: trans('cart.order.product.added'),
        text: trans('app.checkout.additional_product_message.success'),
        showConfirmButton: true,
        showCancelButton: false, // Enable when multiple products are allowed.
        confirmButtonText: trans('cart.modal.add_to_cart.continue_checkout'),
        cancelButtonText: trans('cart.continue_shopping'),
        reverseButtons: true,
    }).then(function () {
        const submitBtn = $('#content-shoppingcart .btn-submit');
        window.location.href = submitBtn.attr('href');
    });
};

checkoutAdditionalProducts.refresh = function () {
    let additionalProductLinesContainer = $('.column-additional-products .product-lines');
    const href = $('.column-additional-products').attr('data-additional-products-url');

    additionalProductLinesContainer.css('height', additionalProductLinesContainer.height()).empty().addClass('loading');

    $.get(href, {
        max: 3
    }).then(function (html) {
        additionalProductLinesContainer.css('height', 'auto').removeClass('loading').replaceWith(html);

        Cart.applyQuantityPlugin($('.column-additional-products'));
    });
};

checkoutAdditionalProducts.init();
