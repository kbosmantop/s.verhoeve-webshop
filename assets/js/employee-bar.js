require('./../scss/employee-bar.scss')

$(document).ready(function () {
    $('[data-action="cancel-cart"]').on('click', (e) => {
        e.preventDefault()

        const item = $(e.currentTarget)

        swal({
            type: 'question',
            title: 'Weet je het zeker',
            html: 'dat de winkelwagen geleegd moet worden?<br />Deze actie kan niet ongedaan gemaakt worden.',
            showCancelButton: true,
            cancelButtonText: 'Annuleren',
            showConfirmButton: true,
            confirmButtonText: 'Winkelwagen legen',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return $.ajax({
                    url: item.attr('href')
                })
            },
            allowOutsideClick: false
        }).then((result) => {
            if (result && result.value.success === true) {
                swal({
                    type: 'success',
                    title: 'Gelukt',
                    html: 'De winkelwagen is succesvol geleegd, de pagina wordt nu vernieuwd',
                    timer: 2500,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.reload()
                })
            } else {
                swal({
                    type: 'error',
                    title: 'Niet gelukt',
                    html: 'De winkelwagen is niet geleegd',
                })
            }
        })
    })
});