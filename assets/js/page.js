import './../../assets/scss/page.scss';

import FaqList from './components/FaqList';

class Page {
    constructor() {
        Page.bindEvents();
    }

    static bindEvents() {
        for(const listElm of document.getElementsByClassName('faq-list')) {
            new FaqList(listElm);
        }
    }
}

$(function() {
    new Page();
});