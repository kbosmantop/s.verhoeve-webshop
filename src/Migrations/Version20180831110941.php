<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180831110941 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // remove commission_invoice_supplier_order_line.order_id_line
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line DROP FOREIGN KEY FK_BB089323BB01DC09');
        $this->addSql('DROP INDEX IDX_BB089323BB01DC09 ON commission_invoice_supplier_order_line');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line DROP order_line_id');

        // remove commission_invoice_supplier_order.order_id
        $this->addSql('ALTER TABLE commission_invoice_supplier_order DROP FOREIGN KEY FK_BA7AC2398D9F6D38');
        $this->addSql('DROP INDEX IDX_BA7AC2398D9F6D38 ON commission_invoice_supplier_order');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order DROP order_id');

        // remove order_line.forward_price
        $this->addSql('ALTER TABLE order_line DROP forward_price');

        // remove 
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E7652ADD6D8C');
        $this->addSql('DROP INDEX IDX_F52993982ADD6D8C ON `order`');
        $this->addSql('ALTER TABLE `order` DROP supplier_id, DROP commissionable, DROP recommission, DROP supplier_connector');
    }

    /**
     * @param Schema $schema
     * @throws IrreversibleMigrationException
     */
    public function down(Schema $schema)
    {
        throw new IrreversibleMigrationException('');
    }
}
