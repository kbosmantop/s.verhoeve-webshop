<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190104121107 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');
        $parameter = [
            'key' => 'catalog_cards_explicit_assignment',
            'form_type' => YesNoType::class,
            'entity' => Company::class,
            'label' => 'Standaardkaartjes verbergen',
            'value' => null,
        ];

        $parameterService->createViaArray($parameter);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
