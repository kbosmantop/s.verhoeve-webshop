<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911133142 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_registration (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, country_id VARCHAR(2) DEFAULT NULL, main_address_id INT DEFAULT NULL, post_address_id INT DEFAULT NULL, registration_number VARCHAR(64) DEFAULT NULL, establishment_number VARCHAR(64) DEFAULT NULL, vat_number VARCHAR(64) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9C656D69F92F3E70 (country_id), INDEX IDX_9C656D69CD4FDB16 (main_address_id), INDEX IDX_9C656D69D6CFA6B4 (post_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_registration ADD CONSTRAINT FK_9C656D69F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE company_registration ADD CONSTRAINT FK_9C656D69CD4FDB16 FOREIGN KEY (main_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE company_registration ADD CONSTRAINT FK_9C656D69D6CFA6B4 FOREIGN KEY (post_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE company ADD company_registration_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FCCCCB53E FOREIGN KEY (company_registration_id) REFERENCES company_registration (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094FCCCCB53E ON company (company_registration_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FCCCCB53E');
        $this->addSql('DROP TABLE company_registration');
        $this->addSql('DROP INDEX IDX_4FBF094FCCCCB53E ON company');
        $this->addSql('ALTER TABLE company DROP company_registration_id');
    }
}
