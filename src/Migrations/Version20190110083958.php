<?php

namespace DoctrineMigrations;

use AppBundle\Form\Type\YesNoType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\Relation\Company;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190110083958 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameter = [
            'key' => 'show_product_sku',
            'entity' => Company::class,
            'label' => 'Artikelnrs. tonen in shop',
            'value' => '',
            'form_type' => YesNoType::class,
            'required' => false,
        ];

        $parameterService->createViaArray($parameter);
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'show_product_sku',
        ];

        foreach ($parameters as $parameter) {
            $parameterService->deleteByKey($parameter);
        }
    }
}
