<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190129121518 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pickup_location (id INT AUTO_INCREMENT NOT NULL, company_establishment_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_F696749826ADA4EA (company_establishment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pickup_location ADD CONSTRAINT FK_F696749826ADA4EA FOREIGN KEY (company_establishment_id) REFERENCES company_establishment (id)');

        /* Fixture query
        INSERT INTO pickup_location(company_establishment_id)
        SELECT coes.id
        FROM company_establishment AS coes
        INNER JOIN company AS comp ON comp.id = coes.company_id
        WHERE comp.is_supplier = 1
        */
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE pickup_location');
    }
}
