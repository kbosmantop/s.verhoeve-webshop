<?php
namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190620115328 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     * @throws AbortMigrationException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_transport_type ADD product_group_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_transport_type ADD CONSTRAINT FK_2677E8035E4B3D0 FOREIGN KEY (product_group_id) REFERENCES productgroup (id)');
        $this->addSql('CREATE INDEX IDX_2677E8035E4B3D0 ON product_transport_type (product_group_id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     * @throws AbortMigrationException
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_transport_type DROP FOREIGN KEY FK_2677E8035E4B3D0');
        $this->addSql('DROP INDEX IDX_2677E8035E4B3D0 ON product_transport_type');
        $this->addSql('ALTER TABLE product_transport_type DROP product_group_id, CHANGE product_id product_id INT NOT NULL');
    }
}
