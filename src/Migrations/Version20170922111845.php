<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\Order;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170922111845 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order ADD supplier_connector VARCHAR (35) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $qb = $this->container->get("doctrine")->getRepository(Order::class)->createQueryBuilder('o');
            $qb->andWhere('o.supplier is not null');

            $orders = $qb->getQuery()->getResult();
            if ($orders) {
                foreach ($orders as $order) {
                    if ($order->getSupplier()->getSupplierConnector()) {
                        $connector = $order->getSupplier()->getSupplierConnector();

                        if (strtolower($connector) == 'bakker' && !$order->getCommissionable()) {
                            $connector = 'BakkerMail';
                        }

                        $order->setSupplierConnector($connector);
                    }
                }
            }

            $this->container->get('doctrine')->getManager()->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order DROP supplier_connector');
    }
}
