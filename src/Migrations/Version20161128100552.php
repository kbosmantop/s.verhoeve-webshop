<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161128100552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE supplier_product ADD id INT AUTO_INCREMENT NOT NULL, CHANGE supplier_id supplier_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('CREATE TABLE supplier_product_product (supplier_product_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_6B90CF692475ABB3 (supplier_product_id), INDEX IDX_6B90CF694584665A (product_id), PRIMARY KEY(supplier_product_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_product_product ADD CONSTRAINT FK_6B90CF692475ABB3 FOREIGN KEY (supplier_product_id) REFERENCES supplier_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_product_product ADD CONSTRAINT FK_6B90CF694584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplier_product_product');
        $this->addSql('ALTER TABLE supplier_product MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE supplier_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE supplier_product DROP id, CHANGE supplier_id supplier_id INT NOT NULL, CHANGE product_id product_id INT NOT NULL');
    }
}
