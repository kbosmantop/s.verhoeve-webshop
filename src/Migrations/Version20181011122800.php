<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Site\Site;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181011122800 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'default_company_site',
            'entity' => null,
            'label' => 'Standaard site voor bedrijven',
            'value' => null,
            'form_type' => EntityType::class,
            'form_type_class' => Site::class,
            'required' => true,
        ]);
    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     * @throws DBALException
     */
    public function preDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');
        $parameterService->deleteByKey('default_company_site');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
