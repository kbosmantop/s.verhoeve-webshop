<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170208115627 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE seo_redirect (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, old_url VARCHAR(100) NOT NULL, new_url VARCHAR(100) DEFAULT NULL, entity_class VARCHAR(100) DEFAULT NULL, entity_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seo_redirect_domain (redirect_id INT NOT NULL, domain_id INT NOT NULL, INDEX IDX_33E2C011B42D874D (redirect_id), INDEX IDX_33E2C011115F0EE5 (domain_id), PRIMARY KEY(redirect_id, domain_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE seo_redirect_domain ADD CONSTRAINT FK_33E2C011B42D874D FOREIGN KEY (redirect_id) REFERENCES seo_redirect (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seo_redirect_domain ADD CONSTRAINT FK_33E2C011115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE seo_redirect_domain DROP FOREIGN KEY FK_33E2C011B42D874D');
        $this->addSql('DROP TABLE seo_redirect');
        $this->addSql('DROP TABLE seo_redirect_domain');
    }
}
