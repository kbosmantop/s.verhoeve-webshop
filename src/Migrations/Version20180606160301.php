<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180606160301 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @description Sets the parameters
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
              'voicedata_api_account_id' => 'Voicedata API Account ID',
              'voicedata_api_password' => 'Voicedata API Password',
        ];

        foreach ($parameters as $key => $label) {
            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $label,
                'value' => null,
            ];

            $parameterService->createViaArray($parameter);
        }
    }
}
