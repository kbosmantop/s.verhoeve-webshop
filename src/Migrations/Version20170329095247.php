<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170329095247 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order CHANGE delivery_address_street delivery_address_street VARCHAR(100) DEFAULT NULL, CHANGE delivery_address_number delivery_address_number VARCHAR(20) DEFAULT NULL, CHANGE delivery_address_postcode delivery_address_postcode VARCHAR(10) DEFAULT NULL, CHANGE delivery_address_city delivery_address_city VARCHAR(100) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order CHANGE delivery_address_street delivery_address_street VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, CHANGE delivery_address_number delivery_address_number VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, CHANGE delivery_address_postcode delivery_address_postcode VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, CHANGE delivery_address_city delivery_address_city VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci');
    }
}
