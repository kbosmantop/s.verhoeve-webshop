<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170809135505 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE assortment_type (id INT AUTO_INCREMENT NOT NULL, `key` VARCHAR(30) DEFAULT NULL, name VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assortment ADD assortment_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD CONSTRAINT FK_2A5E6D8BE8C18765 FOREIGN KEY (assortment_type_id) REFERENCES assortment_type (id)');
        $this->addSql('CREATE INDEX IDX_2A5E6D8BE8C18765 ON assortment (assortment_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->entityManager = $this->container->get("doctrine")->getManager();

            $this->createAssortmentTypes();
            $this->updateExistingAssortments();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment DROP FOREIGN KEY FK_2A5E6D8BE8C18765');
        $this->addSql('DROP TABLE assortment_type');
        $this->addSql('DROP INDEX IDX_2A5E6D8BE8C18765 ON assortment');
        $this->addSql('ALTER TABLE assortment DROP assortment_type_id');
    }

    /**
     * Create new assortment types
     */
    private function createAssortmentTypes()
    {
        $types = [
            'additional_products' => 'Extra producten',
            'cards' => 'Kaartjes',
        ];

        foreach ($types as $key => $name) {
            $assortmentType = new AssortmentType();
            $assortmentType->setKey($key);
            $assortmentType->setName($name);

            $this->entityManager->persist($assortmentType);
            $this->entityManager->flush();
        }
    }

    /**
     * Migrate current assortments with "Kaartjes" in the name to the appropriate assortmentType
     */
    private function updateExistingAssortments()
    {
        $assortmentType = $this->entityManager->getRepository(AssortmentType::class)->findOneBy([
            'key' => AssortmentType::TYPE_CARDS,
        ]);

        if ($assortmentType) {
            $qb = $this->entityManager
                ->createQueryBuilder('a')
                ->update(Assortment::class, 'a')
                ->set('a.assortmentType', $assortmentType->getId())
                ->andWhere("a.name LIKE '%Kaartjes%'")
                ->getQuery();

            $qb->execute();
        }
    }
}
