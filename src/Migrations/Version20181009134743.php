<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20181009134743
 * @package Application\Migrations
 */
class Version20181009134743 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site_usp DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE site_usp ADD id INT AUTO_INCREMENT NOT NULL FIRST, ADD PRIMARY KEY (id), CHANGE site_id site_id INT DEFAULT NULL, CHANGE usp_id usp_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX usp_unique ON site_usp (site_id, usp_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX usp_unique ON site_usp');

        $this->addSql('ALTER TABLE site_usp DROP FOREIGN KEY FK_DF11980F6BD1646');
        $this->addSql('ALTER TABLE site_usp DROP FOREIGN KEY FK_DF11980A6601B70');

        $this->addSql('ALTER TABLE site_usp CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE site_usp DROP PRIMARY KEY');

        $this->addSql('ALTER TABLE site_usp DROP id, CHANGE site_id site_id INT NOT NULL, CHANGE usp_id usp_id INT NOT NULL');
        $this->addSql('ALTER TABLE site_usp ADD PRIMARY KEY (site_id, usp_id)');

        $this->addSql('ALTER TABLE site_usp ADD CONSTRAINT FK_DF11980F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site_usp ADD CONSTRAINT FK_DF11980A6601B70 FOREIGN KEY (usp_id) REFERENCES usp (id)');
    }
}
