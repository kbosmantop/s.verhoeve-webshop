<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704140915 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $results = $qb->select('oc.id as order_collection_id, oc.company_id, a.*')
            ->from('order_collection', 'oc')
            ->leftJoin('oc', 'company', 'c', 'c.id = oc.company_id')
            ->leftJoin('c', 'address', 'a', 'a.id = c.invoice_address_id')
            ->andWhere('oc.company_id IS NOT NULL')
            ->andWhere('c.invoice_address_id IS NOT NULL')
            ->andWhere('oc.`invoice_address_street` != a.street')
            ->andWhere('oc.created_at > "2018-07-03 17:00:00"')
            ->execute()->fetchAll()
        ;

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();
        $updateQuery = $qb->update('order_collection', 'oc')
            ->set('invoice_address_company_name', ':invoice_address_company_name')
            ->set('invoice_address_attn', ':invoice_address_attn')
            ->set('invoice_address_street', ':invoice_address_street')
            ->set('invoice_address_number', ':invoice_address_number')
            ->set('invoice_address_postcode', ':invoice_address_postcode')
            ->set('invoice_address_city', ':invoice_address_city')
            ->set('invoice_address_phone_number', ':invoice_address_phone_number')
            ->where('id = :order_collection_id');

        foreach($results as $result) {
            $updateQuery->setParameters([
                'invoice_address_company_name' => $result['company_name'],
                'invoice_address_attn' => $result['attn'],
                'invoice_address_street' => $result['street'],
                'invoice_address_number' => $result['number'],
                'invoice_address_postcode' => $result['postcode'],
                'invoice_address_city' => $result['city'],
                'invoice_address_phone_number' => $result['phone_number'],
                'order_collection_id' => $result['order_collection_id']
            ])->execute();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
