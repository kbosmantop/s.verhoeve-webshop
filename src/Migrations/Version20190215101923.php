<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190215101923 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_entity ADD default_value VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter_entity RENAME INDEX idx_ea564e993d8e604f TO IDX_EA564E99727ACA70');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_entity DROP default_value');
        $this->addSql('ALTER TABLE parameter_entity RENAME INDEX idx_ea564e99727aca70 TO IDX_EA564E993D8E604F');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'supplier_product_preparation_time',
            'entity' => SupplierProduct::class,
            'label' => 'Productie tijd',
            'default_value' => '1',
            'form_type' => IntegerType::class,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'supplier_group_product_preparation_time',
            'entity' => SupplierGroupProduct::class,
            'label' => 'Productie tijd',
            'default_value' => '1',
            'form_type' => IntegerType::class,
            'required' => false,
        ]);
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'supplier_product_preparation_time',
            'supplier_group_product_preparation_time',
        ];

        foreach ($parameters as $parameter) {
            $parameterService->deleteByKey($parameter);
        }
    }
}

