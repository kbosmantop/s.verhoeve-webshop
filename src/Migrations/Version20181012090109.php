<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181012090109 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->addSql('INSERT INTO vat (id, `description`) VALUES(4, "Export binnen EU")');
        }
    }

    /**
     * @param Schema $schema
     */
    public function preDown(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->addSql('DELETE FROM vat WHERE id = 4');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
