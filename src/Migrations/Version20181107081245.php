<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181107081245 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE product ADD name VARCHAR(100) DEFAULT NULL AFTER parent_id');
        $this->addSql('UPDATE product AS prod 
                            LEFT JOIN (
                                SELECT LEFT(title, 100) AS title, translatable_id
                                FROM product_translation
                                WHERE locale = \'nl_NL\'
                            ) AS tran ON tran.translatable_id = prod.id
                            
                            SET prod.name = tran.title');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP name');
    }
}
