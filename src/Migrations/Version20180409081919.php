<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180409081919 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("UPDATE `customer` SET customer_group_id = 1  WHERE  `customer_group_id`  IS NULL AND username IS NOT NULL  AND company_id IS NULL");
        $this->connection->executeQuery("UPDATE `customer` SET customer_group_id = 2 WHERE `customer_group_id` IS NULL AND username IS NOT NULL AND company_id IS NOT NULL");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
