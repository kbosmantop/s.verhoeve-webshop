<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171107131409 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE assortment_product ADD id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, ADD diff LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', CHANGE assortment_id assortment_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery('UPDATE assortment_product SET diff = \'N;\' WHERE diff = \'\'');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE assortment_product DROP id, DROP diff, CHANGE assortment_id assortment_id INT NOT NULL, CHANGE product_id product_id INT NOT NULL');
        $this->addSql('ALTER TABLE assortment_product ADD PRIMARY KEY (assortment_id, product_id)');
    }
}
