<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180613091812
 * @package Application\Migrations
 */
class Version20180613091812 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE product_image_product');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('CREATE TABLE product_image_product (product_image_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_9CB74F83F6154FFA (product_image_id), INDEX IDX_9CB74F834584665A (product_id), PRIMARY KEY(product_image_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE product_image_product ADD CONSTRAINT FK_9CB74F83F6154FFA FOREIGN KEY (product_image_id) REFERENCES product_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_image_product ADD CONSTRAINT FK_9CB74F834584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }
}
