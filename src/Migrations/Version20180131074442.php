<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180131074442 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE auto_order_job (order_id INT NOT NULL, job_id BIGINT UNSIGNED DEFAULT NULL, updated_at DATETIME NOT NULL, forward_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_95476689BE04EA9 (job_id), PRIMARY KEY(order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE auto_order_job ADD CONSTRAINT FK_954766898D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id)');
        $this->addSql('ALTER TABLE auto_order_job ADD CONSTRAINT FK_95476689BE04EA9 FOREIGN KEY (job_id) REFERENCES jms_jobs (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE auto_order_job');
    }
}
