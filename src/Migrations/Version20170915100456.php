<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170915100456 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order ADD recommission TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE commission_invoice_order DROP INDEX UNIQ_BA7AC2398D9F6D38, ADD INDEX IDX_BA7AC2398D9F6D38 (order_id)');
        $this->addSql('ALTER TABLE commission_invoice_order ADD recommissioned TINYINT(1) DEFAULT \'0\' NOT NULL');

        $this->addSql('ALTER TABLE commission_invoice_line DROP FOREIGN KEY FK_5DB747555A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB747555A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commission_invoice DROP FOREIGN KEY FK_39B0F19475E4B948');
        $this->addSql('ALTER TABLE commission_invoice ADD CONSTRAINT FK_39B0F19475E4B948 FOREIGN KEY (commission_batch_id) REFERENCES commission_batch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commission_invoice_order DROP FOREIGN KEY FK_BA7AC2395A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_order ADD CONSTRAINT FK_BA7AC2395A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commission_invoice_order_line DROP FOREIGN KEY FK_BB0893234E7367B');
        $this->addSql('ALTER TABLE commission_invoice_order_line ADD CONSTRAINT FK_BB0893234E7367B FOREIGN KEY (commission_invoice_order_id) REFERENCES commission_invoice_order (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commission_invoice DROP FOREIGN KEY FK_39B0F19475E4B948');
        $this->addSql('ALTER TABLE commission_invoice ADD CONSTRAINT FK_39B0F19475E4B948 FOREIGN KEY (commission_batch_id) REFERENCES commission_batch (id)');
        $this->addSql('ALTER TABLE commission_invoice_line DROP FOREIGN KEY FK_5DB747555A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB747555A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id)');
        $this->addSql('ALTER TABLE commission_invoice_order DROP FOREIGN KEY FK_BA7AC2395A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_order ADD CONSTRAINT FK_BA7AC2395A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id)');
        $this->addSql('ALTER TABLE commission_invoice_order_line DROP FOREIGN KEY FK_BB0893234E7367B');
        $this->addSql('ALTER TABLE commission_invoice_order_line ADD CONSTRAINT FK_BB0893234E7367B FOREIGN KEY (commission_invoice_order_id) REFERENCES commission_invoice_order (id)');

        $this->addSql('ALTER TABLE commission_invoice_order DROP recommissioned');
        $this->addSql('ALTER TABLE commission_invoice_order DROP INDEX IDX_BA7AC2398D9F6D38, ADD UNIQUE INDEX UNIQ_BA7AC2398D9F6D38 (order_id)');
        $this->addSql('ALTER TABLE order_order DROP recommission');
    }
}
