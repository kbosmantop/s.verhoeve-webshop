<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424131524 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE page_accessible_customer_group (page_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_D8574FA8C4663E4 (page_id), INDEX IDX_D8574FA8D2919A68 (customer_group_id), PRIMARY KEY(page_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_item_accessible_customer_group (menu_item_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_44D8593F9AB44FE0 (menu_item_id), INDEX IDX_44D8593FD2919A68 (customer_group_id), PRIMARY KEY(menu_item_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_accessible_customer_group (product_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_997CAD4D4584665A (product_id), INDEX IDX_997CAD4DD2919A68 (customer_group_id), PRIMARY KEY(product_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE assortment_accessible_customer_group (assortment_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_2B9AFFC5D7A15862 (assortment_id), INDEX IDX_2B9AFFC5D2919A68 (customer_group_id), PRIMARY KEY(assortment_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE page_accessible_customer_group ADD CONSTRAINT FK_D8574FA8C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE page_accessible_customer_group ADD CONSTRAINT FK_D8574FA8D2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_item_accessible_customer_group ADD CONSTRAINT FK_44D8593F9AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_item_accessible_customer_group ADD CONSTRAINT FK_44D8593FD2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_accessible_customer_group ADD CONSTRAINT FK_997CAD4D4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_accessible_customer_group ADD CONSTRAINT FK_997CAD4DD2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_accessible_customer_group ADD CONSTRAINT FK_2B9AFFC5D7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_accessible_customer_group ADD CONSTRAINT FK_2B9AFFC5D2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE page ADD login_required TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_item ADD login_required TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD login_required TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD login_required TINYINT(1) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE page_accessible_customer_group');
        $this->addSql('DROP TABLE menu_item_accessible_customer_group');
        $this->addSql('DROP TABLE product_accessible_customer_group');
        $this->addSql('DROP TABLE assortment_accessible_customer_group');
        $this->addSql('ALTER TABLE assortment DROP login_required');
        $this->addSql('ALTER TABLE menu_item DROP login_required');
        $this->addSql('ALTER TABLE page DROP login_required');
        $this->addSql('ALTER TABLE product DROP login_required');
    }
}
