<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180906085723 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->connection->insert('paymentmethod', [
                'code' => 'freeofcharge',
                'position' => 7,
                'max_amount' => 0,
            ]);

            $paymentMethodId = $this->connection->lastInsertId();

            $this->connection->insert('paymentmethod_translation', [
                'translatable_id' => $paymentMethodId,
                'description' => 'Reeds voldaan',
                'locale' => 'nl_NL',
            ]);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->addSql("DELETE FROM `paymentmethod_translation` WHERE `description` = 'Reeds voldaan';");
            $this->addSql("DELETE FROM `paymentmethod` WHERE `code` = 'freeofcharge';");
        }
    }
}
