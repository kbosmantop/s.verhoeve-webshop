<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171218112208 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parameter_entity (id INT AUTO_INCREMENT NOT NULL, parameter_id VARCHAR(32) DEFAULT NULL, entity VARCHAR(255) DEFAULT NULL, required TINYINT(1) NOT NULL, label VARCHAR(255) NOT NULL, INDEX IDX_EA564E997C56DBD6 (parameter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter_entity_value (id INT AUTO_INCREMENT NOT NULL, parameter_entity_id INT DEFAULT NULL, entity INT DEFAULT NULL, value LONGTEXT NOT NULL, INDEX IDX_F610037871D0EF (parameter_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter (`key` VARCHAR(32) NOT NULL, form_type VARCHAR(255) NOT NULL, multiple TINYINT(1) NOT NULL, PRIMARY KEY(`key`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parameter_entity ADD CONSTRAINT FK_EA564E997C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (`key`)');
        $this->addSql('ALTER TABLE parameter_entity_value ADD CONSTRAINT FK_F610037871D0EF FOREIGN KEY (parameter_entity_id) REFERENCES parameter_entity (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_entity_value DROP FOREIGN KEY FK_F610037871D0EF');
        $this->addSql('ALTER TABLE parameter_entity DROP FOREIGN KEY FK_EA564E997C56DBD6');
        $this->addSql('DROP TABLE parameter_entity');
        $this->addSql('DROP TABLE parameter_entity_value');
        $this->addSql('DROP TABLE parameter');
    }
}
