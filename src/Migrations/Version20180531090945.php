<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180531090945 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Connection $conn */
    private $conn;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $this->conn = $this->container->get('doctrine')->getConnection();

        $products = [];
        $supplierProducts = $this->conn->createQueryBuilder()->select('*')->from('supplier_product')->execute()->fetchAll();
        $supplierGroupProducts = $this->conn->createQueryBuilder()->select('*')->from('supplier_group_product')->execute()->fetchAll();

        $this->conn->beginTransaction();

        if (!empty($supplierProducts) || !empty($supplierGroupProducts)) {
            $products = $this->conn->createQueryBuilder()->select('id, parent_id')->from('product')->execute()->fetchAll();
        }

        if ($supplierProducts) {
            $supplierProductProducts = $this->conn->createQueryBuilder()->select('*')->from('supplier_product_product')->execute()->fetchAll();

            $this->migrateProducts($products, $supplierProducts, $supplierProductProducts, 'supplier');
        }

        if ($supplierGroupProducts) {
            $supplierGroupProductProducts = $this->conn->createQueryBuilder()->select('*')->from('supplier_group_product_product')->execute()->fetchAll();

            $this->migrateProducts($products, $supplierGroupProducts, $supplierGroupProductProducts, 'supplier_group');
        }

        $this->conn->commit();
    }

    /**
     * @param $productsArray
     * @param $supplierProductsArray
     * @param $supplierProductProductsArray
     * @param $type
     * @return array
     */
    private function migrateProducts(
        $productsArray,
        $supplierProductsArray,
        $supplierProductProductsArray,
        $type
    ): array {
        //loop through all supplier products and remove products that already are migrated correctly
        foreach ($productsArray as $product) {
            foreach ($supplierProductsArray as $key => $supplierProduct) {
                if (null !== $product['parent_id'] && $product['id'] === $supplierProduct['product_id']) {
                    unset($supplierProductsArray[$key]);
                }
            }
        }

        //set table depending on type
        $table = $type === 'supplier' ? 'supplier_product' : 'supplier_group_product';

        $translationTable = $type === 'supplier' ? 'supplier_product_translation' : 'supplier_group_product_translation';

        //set supplierField depending on type
        $supplierFieldId = $type === 'supplier' ? 'supplier_id' : 'supplier_group_id';
        $newSupplierProducts = [];
        $newSupplierProductsTranslations = [];

        //prepare querties
        /** @var QueryBuilder $deleteQuery */
        $deleteQuery = $this->conn->createQueryBuilder();
        $deleteQuery->delete($table)
            ->where('product_id = :productID')
            ->andWhere($supplierFieldId . ' = :supplierGroupID');

        /** @var QueryBuilder $insertQuery */
        $insertQuery = $this->conn->createQueryBuilder();
        $insertQuery->insert($table);

        $insertTranslationQuery = $this->conn->createQueryBuilder();
        $insertTranslationQuery->insert($translationTable);

        $selectTranslationQuery = $this->conn->createQueryBuilder();
        $translations = $selectTranslationQuery->select('*')->from($translationTable)->execute()->fetchAll();

        //loop through all supplier product products and rewrite them to supplier_products
        foreach ($supplierProductProductsArray as $supplierProductProduct) {
            $key = $type === 'supplier' ? 'supplier_product_id' : 'supplier_group_product_id';

            foreach ($supplierProductsArray as $supplierProductKey => $supplierProduct) {
                if ($supplierProductProduct[$key] === $supplierProduct['id']) {

                    //find all translations regarding supplier product
                    foreach($translations as $translation) {
                        if($translation['translatable_id'] === $supplierProduct['id']) {
                            unset($translation['id']);
                            unset($translation['translatable_id']);

                            $newSupplierProductsTranslations[$supplierProductProduct['product_id']][$translation['locale']] = $translation;
                        }
                    }

                    unset($supplierProduct['id']);
                    $supplierProduct['sku'] = $this->conn->quote($supplierProduct['sku']);

                    //delete old supplier product
                    $deleteQuery
                        ->setParameter('productID', $supplierProduct['product_id'])
                        ->setParameter('supplierGroupID', $supplierProduct[$supplierFieldId])
                        ->execute();

                    $supplierProduct['product_id'] = $supplierProductProduct['product_id'];
                    $newSupplierProducts[$supplierProduct['product_id']] = array_filter($supplierProduct);

                    if(isset($supplierProductsArray[$supplierProductKey])) {
                        unset($supplierProductsArray[$supplierProductKey]);
                    }
                }
            }
        }

        //insert all new supplier products
        foreach ($newSupplierProducts as $newSupplierProduct) {
            $insertQuery->values($newSupplierProduct)->execute();

            $supplierProductId = $this->conn->lastInsertId();

            //continue if no translation is available
            if(!isset($newSupplierProductsTranslations[$newSupplierProduct['product_id']])) {
                continue;
            }

            foreach($newSupplierProductsTranslations[$newSupplierProduct['product_id']] as $translation) {
                $translation['translatable_id'] = $supplierProductId;

                foreach($translation as $key => $value) {
                    $translation[$key] = $this->conn->quote($value);
                }

                try {
                    $insertTranslationQuery->values($translation)->execute();
                } catch (\Exception $e) {
                    echo $e->getMessage().PHP_EOL;
                }
            }
        }

        //loop through old supplier products and check if they have variations to be added
        foreach ($supplierProductsArray as $supplierProduct) {
            $newSupplierProduct = array_filter($supplierProduct);
            $parentId = $supplierProduct['product_id'];

            unset($newSupplierProduct['id']);
            $newSupplierProduct['sku'] = $this->conn->quote($supplierProduct['sku']);

            foreach ($productsArray as $product) {
                if ($product['parent_id'] === $parentId) {
                    $newSupplierProduct['product_id'] = $product['id'];

                    //insert variations
                    $insertQuery->values($newSupplierProduct)->execute();

                    $supplierProductId = $this->conn->lastInsertId();
                    foreach($translations as $translation) {
                        if($supplierProduct['id'] === $translation['translatable_id']) {
                            unset($translation['id']);
                            $translation['translatable_id'] = $supplierProductId;
                            foreach($translation as $key => $value) {
                                $translation[$key] = $this->conn->quote($value);
                            }

                            try {
                                $insertTranslationQuery->values($translation)->execute();
                            } catch (\Exception $e) {
                                echo $e->getMessage().PHP_EOL;
                            }
                        }
                    }

                    //delete old supplier product
                    $deleteQuery
                        ->setParameter('productID', $supplierProduct['product_id'])
                        ->setParameter('supplierGroupID', $supplierProduct[$supplierFieldId])
                        ->execute();
                }
            }
        }

        return $newSupplierProducts;
    }

    /**
     * @param Schema $schema
     * @throws IrreversibleMigrationException
     */
    public function down(Schema $schema)
    {
        throw new IrreversibleMigrationException("This migration cannot be reversed.");
    }
}
