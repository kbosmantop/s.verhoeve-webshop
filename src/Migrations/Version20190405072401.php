<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Autogenerated Migration: Please modify to your needs!
 */
final class Version20190405072401 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_custom_order_field_value (id INT AUTO_INCREMENT NOT NULL, company_custom_field_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, order_collection_id INT DEFAULT NULL, order_id INT DEFAULT NULL, value LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_F6CB3727EE14DAA9 (company_custom_field_id), INDEX IDX_F6CB37279395C3F3 (customer_id), INDEX IDX_F6CB3727FC2A1E2F (order_collection_id), INDEX IDX_F6CB37278D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD CONSTRAINT FK_F6CB3727EE14DAA9 FOREIGN KEY (company_custom_field_id) REFERENCES company_custom_field (id)');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD CONSTRAINT FK_F6CB37279395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD CONSTRAINT FK_F6CB3727FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD CONSTRAINT FK_F6CB37278D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE company_custom_field ADD applies_to ENUM(\'order_collection\', \'order\', \'\') DEFAULT \'order_collection\' NOT NULL COMMENT \'(DC2Type:AppliesToOrderType)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_custom_order_field_value');
        $this->addSql('ALTER TABLE company_custom_field DROP applies_to');
    }
}