<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\ColumnType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180525132413 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function preDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('cookie_consent_categories_file');
        $parameterService->deleteByKey('cookie_consent_translation_file');
        $parameterService->deleteByKey('cookie_consent_page_about_cookies');
        $parameterService->deleteByKey('cookie_consent');
    }

    /**
     * @description Sets the parameters for WICS for use in the WICS service
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'cookie_consent',
            'entity' => Site::class,
            'label' => 'Cookie Consent',
            'value' => null,
            'form_type' => ColumnType::class
        ]);

        $parameterService->createViaArray([
            'key' => 'cookie_consent_page_about_cookies',
            'entity' => Site::class,
            'label' => 'Pagina over cookies',
            'value' => null,
            'parent' => 'cookie_consent',
            'form_type' => EntityType::class,
            'form_type_class' => Page::class,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'cookie_consent_categories_file',
            'entity' => Site::class,
            'label' => 'Bestand categorieen',
            'parent' => 'cookie_consent',
            'value' => null,
            'form_type' => TextType::class,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'cookie_consent_translation_file',
            'entity' => Site::class,
            'label' => 'Bestand vertalingen',
            'parent' => 'cookie_consent',
            'value' => null,
            'form_type' => TextType::class,
            'required' => false,
        ]);

    }

}
