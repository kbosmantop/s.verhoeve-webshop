<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614071736 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order ADD supplier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E7652ADD6D8C FOREIGN KEY (supplier_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_76B7E7652ADD6D8C ON order_order (supplier_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E7652ADD6D8C');
        $this->addSql('DROP INDEX IDX_76B7E7652ADD6D8C ON order_order');
        $this->addSql('ALTER TABLE order_order DROP supplier_id');
    }
}
