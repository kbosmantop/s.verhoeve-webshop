<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180403093606 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE adiuvo_image_format CHANGE extension extension ENUM(\'png\', \'jpg\') DEFAULT NULL');
        $this->addSql('ALTER TABLE adiuvo_image DROP FOREIGN KEY FK_56EF47E9EAB2D51');
        $this->addSql('DROP TABLE adiuvo_image');
        $this->addSql('DROP TABLE adiuvo_image_format');

    }


    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adiuvo_image_format CHANGE extension extension ENUM(\'png\', \'jpg\') DEFAULT NULL  COMMENT \'(DC2Type:ImageExtensionType)\'');
        $this->addSql('CREATE TABLE adiuvo_image (id INT AUTO_INCREMENT NOT NULL, image_format_id INT DEFAULT NULL, entity_name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, entity_id INT NOT NULL, path VARCHAR(200) NOT NULL COLLATE utf8_unicode_ci, rendered_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_56EF47E9B548B0F (path), INDEX IDX_56EF47E9EAB2D51 (image_format_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adiuvo_image_format (id INT AUTO_INCREMENT NOT NULL, entity LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, slug VARCHAR(24) NOT NULL COLLATE utf8_unicode_ci, description VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, method LONGTEXT NOT NULL COLLATE utf8_unicode_ci, extension VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, quality INT NOT NULL, parameters LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adiuvo_image ADD CONSTRAINT FK_56EF47E9EAB2D51 FOREIGN KEY (image_format_id) REFERENCES adiuvo_image_format (id)');
    }
}
