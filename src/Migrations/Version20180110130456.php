<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180110130456 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_combination (id INT AUTO_INCREMENT NOT NULL, combination_product_id INT DEFAULT NULL, product_id INT DEFAULT NULL, main TINYINT(1) DEFAULT NULL, stock TINYINT(1) DEFAULT NULL, quantity INT NOT NULL, metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_722684B0CBF21BF8 (combination_product_id), INDEX IDX_722684B04584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_combination ADD CONSTRAINT FK_722684B0CBF21BF8 FOREIGN KEY (combination_product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_combination ADD CONSTRAINT FK_722684B04584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product_combination');
    }
}
