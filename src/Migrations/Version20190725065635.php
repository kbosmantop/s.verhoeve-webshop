<?php declare(strict_types=1);

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190725065635 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameter = [
            'key' => 'card_productgroup',
            'form_type' => EntityType::class,
            'form_type_class' => Productgroup::class,
            'entity' => null,
            'label' => 'Productgroep voor kaartjes',
            'value' => null,
        ];

        $parameterService->createViaArray($parameter);
    }

    public function preDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('card_productgroup');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
