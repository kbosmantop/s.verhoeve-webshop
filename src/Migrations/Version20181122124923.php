<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181122124923 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_vat_group (product_id INT NOT NULL, vat_group_id INT NOT NULL, INDEX IDX_594C459B4584665A (product_id), INDEX IDX_594C459BE8C706E8 (vat_group_id), PRIMARY KEY(product_id, vat_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vat_group (id INT AUTO_INCREMENT NOT NULL, country_id VARCHAR(2) DEFAULT NULL, level_id INT DEFAULT NULL, INDEX IDX_37ECF316F92F3E70 (country_id), INDEX IDX_37ECF3165FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vat_level (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, special TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vat_rate (id INT AUTO_INCREMENT NOT NULL, group_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, purchase TINYINT(1) DEFAULT NULL, title VARCHAR(255) NOT NULL, valid_from DATETIME NOT NULL, valid_until DATETIME NOT NULL, factor NUMERIC(10, 0) NOT NULL, INDEX IDX_F684F7C7FE54D947 (group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_vat_group ADD CONSTRAINT FK_594C459B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_vat_group ADD CONSTRAINT FK_594C459BE8C706E8 FOREIGN KEY (vat_group_id) REFERENCES vat_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vat_group ADD CONSTRAINT FK_37ECF316F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE vat_group ADD CONSTRAINT FK_37ECF3165FB14BA7 FOREIGN KEY (level_id) REFERENCES vat_level (id)');
        $this->addSql('ALTER TABLE vat_rate ADD CONSTRAINT FK_F684F7C7FE54D947 FOREIGN KEY (group_id) REFERENCES vat_group (id)');
        $this->addSql('ALTER TABLE vat_rate CHANGE factor factor DOUBLE PRECISION NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vat_rate CHANGE factor factor NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE product_vat_group DROP FOREIGN KEY FK_594C459BE8C706E8');
        $this->addSql('ALTER TABLE vat_rate DROP FOREIGN KEY FK_F684F7C7FE54D947');
        $this->addSql('ALTER TABLE vat_group DROP FOREIGN KEY FK_37ECF3165FB14BA7');
        $this->addSql('DROP TABLE product_vat_group');
        $this->addSql('DROP TABLE vat_group');
        $this->addSql('DROP TABLE vat_level');
        $this->addSql('DROP TABLE vat_rate');
    }
}
