<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180423071721 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_site (id INT AUTO_INCREMENT NOT NULL, default_site_id INT DEFAULT NULL, logo_id INT DEFAULT NULL, order_confirmation_user_id INT DEFAULT NULL, order_confirmation_ccuser_id INT DEFAULT NULL, delivery_status_user_id INT DEFAULT NULL, delivery_status_ccuser_id INT DEFAULT NULL, confirm_order_user_id INT DEFAULT NULL, confirm_user_user_id INT DEFAULT NULL, locale VARCHAR(255) DEFAULT NULL, custom_style LONGTEXT DEFAULT NULL, registration_enabled TINYINT(1) NOT NULL, allowed_extensions LONGTEXT DEFAULT NULL, domain_name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, login_text LONGTEXT DEFAULT NULL, registration_text LONGTEXT DEFAULT NULL, order_success_text LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_2A2E130A4384703 (default_site_id), INDEX IDX_2A2E130AF98F144A (logo_id), INDEX IDX_2A2E130A9FA6A8EE (order_confirmation_user_id), INDEX IDX_2A2E130AEC9D323D (order_confirmation_ccuser_id), INDEX IDX_2A2E130AFB3B42F (delivery_status_user_id), INDEX IDX_2A2E130A2A7D1868 (delivery_status_ccuser_id), INDEX IDX_2A2E130AC7B46673 (confirm_order_user_id), INDEX IDX_2A2E130A56D7EED5 (confirm_user_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_site_custom_field (id INT AUTO_INCREMENT NOT NULL, company_site_id INT NOT NULL, type VARCHAR(30) NOT NULL, required TINYINT(1) NOT NULL, multiple TINYINT(1) NOT NULL, position INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_FEFB17AD6EF0C91 (company_site_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_site_custom_field_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, label VARCHAR(60) DEFAULT NULL, `values` LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A3FCA66A2C2AC5D3 (translatable_id), UNIQUE INDEX company_site_custom_field_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_site_logo (id INT AUTO_INCREMENT NOT NULL, path LONGTEXT NOT NULL, description VARCHAR(128) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_logo (id INT AUTO_INCREMENT NOT NULL, path LONGTEXT NOT NULL, description VARCHAR(128) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130A4384703 FOREIGN KEY (default_site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130AF98F144A FOREIGN KEY (logo_id) REFERENCES site_logo (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130A9FA6A8EE FOREIGN KEY (order_confirmation_user_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130AEC9D323D FOREIGN KEY (order_confirmation_ccuser_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130AFB3B42F FOREIGN KEY (delivery_status_user_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130A2A7D1868 FOREIGN KEY (delivery_status_ccuser_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130AC7B46673 FOREIGN KEY (confirm_order_user_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site ADD CONSTRAINT FK_2A2E130A56D7EED5 FOREIGN KEY (confirm_user_user_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_site_custom_field ADD CONSTRAINT FK_FEFB17AD6EF0C91 FOREIGN KEY (company_site_id) REFERENCES company_site (id)');
        $this->addSql('ALTER TABLE company_site_custom_field_translation ADD CONSTRAINT FK_A3FCA66A2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES company_site_custom_field (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD site_id INT DEFAULT NULL, DROP custom_shop');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF6BD1646 FOREIGN KEY (site_id) REFERENCES company_site (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094FF6BD1646 ON company (site_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF6BD1646');
        $this->addSql('ALTER TABLE company_site_custom_field DROP FOREIGN KEY FK_FEFB17AD6EF0C91');
        $this->addSql('ALTER TABLE company_site_custom_field_translation DROP FOREIGN KEY FK_A3FCA66A2C2AC5D3');
        $this->addSql('ALTER TABLE company_site DROP FOREIGN KEY FK_2A2E130AF98F144A');
        $this->addSql('DROP TABLE company_site');
        $this->addSql('DROP TABLE company_site_custom_field');
        $this->addSql('DROP TABLE company_site_custom_field_translation');
        $this->addSql('DROP TABLE company_site_logo');
        $this->addSql('DROP TABLE site_logo');
        $this->addSql('DROP INDEX IDX_4FBF094FF6BD1646 ON company');
        $this->addSql('ALTER TABLE company ADD custom_shop TINYINT(1) DEFAULT NULL, DROP site_id');
    }
}
