<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903115915 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_report_customer_history (id INT AUTO_INCREMENT NOT NULL, company_report_customer_id INT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_41BD177891B78F44 (company_report_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_report_customer_history ADD CONSTRAINT FK_41BD177891B78F44 FOREIGN KEY (company_report_customer_id) REFERENCES company_report_customer (id)');
        $this->addSql('ALTER TABLE company_report_customer ADD start_date DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_report_customer_history');
        $this->addSql('ALTER TABLE company_report_customer DROP start_date');
    }
}
