<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618081109 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_paymentmethod (company_id INT NOT NULL, paymentmethod_id INT NOT NULL, INDEX IDX_FF0AC233979B1AD6 (company_id), INDEX IDX_FF0AC233778E3E6F (paymentmethod_id), PRIMARY KEY(company_id, paymentmethod_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_paymentmethod ADD CONSTRAINT FK_FF0AC233979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_paymentmethod ADD CONSTRAINT FK_FF0AC233778E3E6F FOREIGN KEY (paymentmethod_id) REFERENCES paymentmethod (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_paymentmethod');
    }
}
