<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170419145312 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_group_product DROP FOREIGN KEY FK_4797D0662ADD6D8C');
        $this->addSql('DROP INDEX IDX_4797D0662ADD6D8C ON supplier_group_product');
        $this->addSql('ALTER TABLE supplier_group_product CHANGE supplier_id supplier_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_group_product ADD CONSTRAINT FK_4797D06673DDB1C6 FOREIGN KEY (supplier_group_id) REFERENCES supplier_group (id)');
        $this->addSql('CREATE INDEX IDX_4797D06673DDB1C6 ON supplier_group_product (supplier_group_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_group_product DROP FOREIGN KEY FK_4797D06673DDB1C6');
        $this->addSql('DROP INDEX IDX_4797D06673DDB1C6 ON supplier_group_product');
        $this->addSql('ALTER TABLE supplier_group_product CHANGE supplier_group_id supplier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_group_product ADD CONSTRAINT FK_4797D0662ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier_group (id)');
        $this->addSql('CREATE INDEX IDX_4797D0662ADD6D8C ON supplier_group_product (supplier_id)');
    }
}
