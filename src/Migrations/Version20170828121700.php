<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170828121700 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE commission_invoice_line (id INT AUTO_INCREMENT NOT NULL, commission_invoice_id INT DEFAULT NULL, vat_id INT DEFAULT NULL, description TINYTEXT NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_5DB747555A9DB655 (commission_invoice_id), INDEX IDX_5DB74755B5B63A6B (vat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commission_invoice (id INT AUTO_INCREMENT NOT NULL, commission_batch_id INT DEFAULT NULL, company_id INT DEFAULT NULL, date DATE NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_39B0F19475E4B948 (commission_batch_id), INDEX IDX_39B0F194979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commission_invoice_order (id INT AUTO_INCREMENT NOT NULL, commission_invoice_id INT DEFAULT NULL, order_id INT DEFAULT NULL, INDEX IDX_BA7AC2395A9DB655 (commission_invoice_id), UNIQUE INDEX UNIQ_BA7AC2398D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commission_batch (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commission_invoice_order_line (id INT AUTO_INCREMENT NOT NULL, commission_invoice_order_id INT DEFAULT NULL, order_line_id INT DEFAULT NULL, vat_id INT DEFAULT NULL, percentage INT NOT NULL, order_line_total_price DOUBLE PRECISION NOT NULL, INDEX IDX_BB0893234E7367B (commission_invoice_order_id), INDEX IDX_BB089323BB01DC09 (order_line_id), INDEX IDX_BB089323B5B63A6B (vat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_commission (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, product_group_id INT NOT NULL, percentage INT NOT NULL, INDEX IDX_A1EBE8CE979B1AD6 (company_id), INDEX IDX_A1EBE8CE35E4B3D0 (product_group_id), UNIQUE INDEX company_productgroup (company_id, product_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier_group_commission (id INT AUTO_INCREMENT NOT NULL, supplier_group_id INT DEFAULT NULL, product_group_id INT NOT NULL, percentage INT NOT NULL, INDEX IDX_9CA9D61F73DDB1C6 (supplier_group_id), INDEX IDX_9CA9D61F35E4B3D0 (product_group_id), UNIQUE INDEX supplier_productgroup (supplier_group_id, product_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB747555A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id)');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB74755B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE commission_invoice ADD CONSTRAINT FK_39B0F19475E4B948 FOREIGN KEY (commission_batch_id) REFERENCES commission_batch (id)');
        $this->addSql('ALTER TABLE commission_invoice ADD CONSTRAINT FK_39B0F194979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE commission_invoice_order ADD CONSTRAINT FK_BA7AC2395A9DB655 FOREIGN KEY (commission_invoice_id) REFERENCES commission_invoice (id)');
        $this->addSql('ALTER TABLE commission_invoice_order ADD CONSTRAINT FK_BA7AC2398D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id)');
        $this->addSql('ALTER TABLE commission_invoice_order_line ADD CONSTRAINT FK_BB0893234E7367B FOREIGN KEY (commission_invoice_order_id) REFERENCES commission_invoice_order (id)');
        $this->addSql('ALTER TABLE commission_invoice_order_line ADD CONSTRAINT FK_BB089323BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_order_line (id)');
        $this->addSql('ALTER TABLE commission_invoice_order_line ADD CONSTRAINT FK_BB089323B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE company_commission ADD CONSTRAINT FK_A1EBE8CE979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company_commission ADD CONSTRAINT FK_A1EBE8CE35E4B3D0 FOREIGN KEY (product_group_id) REFERENCES productgroup (id)');
        $this->addSql('ALTER TABLE supplier_group_commission ADD CONSTRAINT FK_9CA9D61F73DDB1C6 FOREIGN KEY (supplier_group_id) REFERENCES supplier_group (id)');
        $this->addSql('ALTER TABLE supplier_group_commission ADD CONSTRAINT FK_9CA9D61F35E4B3D0 FOREIGN KEY (product_group_id) REFERENCES productgroup (id)');

        $this->addSql('ALTER TABLE order_status ADD invoiceable TINYINT(1) NOT NULL, CHANGE description description VARCHAR(100) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        $em = $this->container->get("doctrine")->getManager();

        foreach (["processed", "complete", "sent", "archived"] as $status) {
            $orderStatus = $em->find(OrderStatus::class, $status);

            if ($orderStatus) {
                $orderStatus->setCommissionable(true);
            }
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_status DROP invoiceable, CHANGE description description VARCHAR(25) NOT NULL COLLATE utf8_unicode_ci');

        $this->addSql('ALTER TABLE commission_invoice_line DROP FOREIGN KEY FK_5DB747555A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_order DROP FOREIGN KEY FK_BA7AC2395A9DB655');
        $this->addSql('ALTER TABLE commission_invoice_order_line DROP FOREIGN KEY FK_BB0893234E7367B');
        $this->addSql('ALTER TABLE commission_invoice DROP FOREIGN KEY FK_39B0F19475E4B948');
        $this->addSql('DROP TABLE commission_invoice_line');
        $this->addSql('DROP TABLE commission_invoice');
        $this->addSql('DROP TABLE commission_invoice_order');
        $this->addSql('DROP TABLE commission_batch');
        $this->addSql('DROP TABLE commission_invoice_order_line');
        $this->addSql('DROP TABLE company_commission');
        $this->addSql('DROP TABLE supplier_group_commission');
    }
}
