<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181018081601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE newsletter_preferences (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, email VARCHAR(100) NOT NULL, name VARCHAR(100) DEFAULT NULL, type_no_mail TINYINT(1) DEFAULT \'0\' NOT NULL, type_newsletters TINYINT(1) DEFAULT \'0\' NOT NULL, type_discount_actions TINYINT(1) DEFAULT \'0\' NOT NULL, type_research TINYINT(1) DEFAULT \'0\' NOT NULL, site_topbloemen TINYINT(1) DEFAULT \'0\' NOT NULL, site_topgeschenken TINYINT(1) DEFAULT \'0\' NOT NULL, site_toptaart TINYINT(1) DEFAULT \'0\' NOT NULL, site_topfruit TINYINT(1) DEFAULT \'0\' NOT NULL, site_suppliers TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_DFDFF93AE7927C74 (email), UNIQUE INDEX UNIQ_DFDFF93A9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE newsletter_preferences ADD CONSTRAINT FK_DFDFF93A9395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE newsletter_preferences');
    }
}
