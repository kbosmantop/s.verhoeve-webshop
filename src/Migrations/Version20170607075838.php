<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170607075838 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplier_country');
        $this->addSql('DROP TABLE supplier_florist');
        $this->addSql('ALTER TABLE supplier_product DROP FOREIGN KEY FK_522F70B22ADD6D8C');
        $this->addSql('DROP INDEX IDX_522F70B22ADD6D8C ON supplier_product');
        $this->addSql('ALTER TABLE supplier_product CHANGE supplier_id supplier_id_old INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD supplier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD CONSTRAINT FK_522F70B22ADD6D8C FOREIGN KEY (supplier_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_522F70B22ADD6D8C ON supplier_product (supplier_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplier_country (supplier_id INT NOT NULL, country_id VARCHAR(2) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_D216BD792ADD6D8C (supplier_id), INDEX IDX_D216BD79F92F3E70 (country_id), PRIMARY KEY(supplier_id, country_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier_florist (id INT NOT NULL, partner_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_country ADD CONSTRAINT FK_D216BD792ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('ALTER TABLE supplier_country ADD CONSTRAINT FK_D216BD79F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE supplier_florist ADD CONSTRAINT FK_4571FAABBF396750 FOREIGN KEY (id) REFERENCES supplier (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_product CHANGE supplier_id_old supplier_id INT NOT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD CONSTRAINT FK_522F70B22ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('CREATE INDEX IDX_522F70B22ADD6D8C ON supplier_product (supplier_id)');
        $this->addSql('ALTER TABLE supplier_product DROP FOREIGN KEY FK_522F70B22ADD6D8C');
        $this->addSql('DROP INDEX IDX_522F70B22ADD6D8C ON supplier_product');
        $this->addSql('ALTER TABLE supplier_product DROP supplier_id');
    }
}
