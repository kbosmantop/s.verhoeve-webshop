<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180716064758 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'wics_incomplete_order_from' => [
                'label' => 'WICS | Incomplete order van',
                'value' => 'magazijn@topgeschenken.nl',
            ],
            'wics_incomplete_order_to' => [
                'label' => 'WICS | Incomplete order naar',
                'value' => 'klantenservice@topgeschenken.nl',
            ],
        ];

        foreach ($parameters as $key => $parameter) {
            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $parameter['label'],
                'value' => $parameter['value'],
            ];

            $parameterService->createViaArray($parameter);
        }
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'wics_incomplete_order_from',
            'wics_incomplete_order_to',
        ];

        foreach ($parameters as $parameter) {
            $parameterService->deleteByKey($parameter);
        }
    }
}
