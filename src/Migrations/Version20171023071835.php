<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023071835 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `designer_layout` RENAME TO `designer_canvas`');

        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D38C22AA1A');
        $this->addSql('DROP INDEX IDX_81C553D38C22AA1A ON designer_template');
        $this->addSql('ALTER TABLE designer_template CHANGE layout_id canvas_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3DD8C9D23 FOREIGN KEY (canvas_id) REFERENCES designer_canvas (id)');
        $this->addSql('CREATE INDEX IDX_81C553D3DD8C9D23 ON designer_template (canvas_id)');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `designer_canvas` RENAME TO `designer_layout`');

        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3DD8C9D23');
        $this->addSql('DROP INDEX IDX_81C553D3DD8C9D23 ON designer_template');
        $this->addSql('ALTER TABLE designer_template CHANGE canvas_id layout_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D38C22AA1A FOREIGN KEY (layout_id) REFERENCES designer_canvas (id)');
        $this->addSql('CREATE INDEX IDX_81C553D38C22AA1A ON designer_template (layout_id)');


    }

}
