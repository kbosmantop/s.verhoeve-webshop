<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171130153112 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rule_expression (id INT AUTO_INCREMENT NOT NULL, left_operand_id INT NOT NULL, right_operand_id INT DEFAULT NULL, operator VARCHAR(16) NOT NULL, INDEX IDX_E381DADA4127021E (left_operand_id), INDEX IDX_E381DADAA86AA40 (right_operand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rule (id INT AUTO_INCREMENT NOT NULL, expression_id INT NOT NULL, description VARCHAR(64) NOT NULL, start VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_46D8ACCCADBB65A1 (expression_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rule_operand (id INT AUTO_INCREMENT NOT NULL, expression_id INT DEFAULT NULL, entity_relation_id INT DEFAULT NULL, variable VARCHAR(64) DEFAULT NULL, field VARCHAR(64) DEFAULT NULL, value VARCHAR(64) DEFAULT NULL, INDEX IDX_73740552ADBB65A1 (expression_id), INDEX IDX_73740552B06CCB88 (entity_relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity_property (id INT AUTO_INCREMENT NOT NULL, entity_id INT NOT NULL, name VARCHAR(64) NOT NULL, type VARCHAR(64) NOT NULL, description VARCHAR(64) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_2906A09E81257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity_relation (id INT AUTO_INCREMENT NOT NULL, start_entity_id INT DEFAULT NULL, end_entity_id INT DEFAULT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_C07DFB0949ED069B (start_entity_id), INDEX IDX_C07DFB0912F5C155 (end_entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity_relation_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, label VARCHAR(64) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_66EF20482C2AC5D3 (translatable_id), UNIQUE INDEX entity_relation_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(64) NOT NULL, rule_start_point TINYINT(1) NOT NULL, fully_qualified_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rule_expression ADD CONSTRAINT FK_E381DADA4127021E FOREIGN KEY (left_operand_id) REFERENCES rule_operand (id)');
        $this->addSql('ALTER TABLE rule_expression ADD CONSTRAINT FK_E381DADAA86AA40 FOREIGN KEY (right_operand_id) REFERENCES rule_operand (id)');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT FK_46D8ACCCADBB65A1 FOREIGN KEY (expression_id) REFERENCES rule_expression (id)');
        $this->addSql('ALTER TABLE rule_operand ADD CONSTRAINT FK_73740552ADBB65A1 FOREIGN KEY (expression_id) REFERENCES rule_expression (id)');
        $this->addSql('ALTER TABLE rule_operand ADD CONSTRAINT FK_73740552B06CCB88 FOREIGN KEY (entity_relation_id) REFERENCES entity_relation (id)');
        $this->addSql('ALTER TABLE entity_property ADD CONSTRAINT FK_2906A09E81257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE entity_relation ADD CONSTRAINT FK_C07DFB0949ED069B FOREIGN KEY (start_entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE entity_relation ADD CONSTRAINT FK_C07DFB0912F5C155 FOREIGN KEY (end_entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE entity_relation_translation ADD CONSTRAINT FK_66EF20482C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES entity_relation (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rule DROP FOREIGN KEY FK_46D8ACCCADBB65A1');
        $this->addSql('ALTER TABLE rule_operand DROP FOREIGN KEY FK_73740552ADBB65A1');
        $this->addSql('ALTER TABLE rule_expression DROP FOREIGN KEY FK_E381DADA4127021E');
        $this->addSql('ALTER TABLE rule_expression DROP FOREIGN KEY FK_E381DADAA86AA40');
        $this->addSql('ALTER TABLE rule_operand DROP FOREIGN KEY FK_73740552B06CCB88');
        $this->addSql('ALTER TABLE entity_relation_translation DROP FOREIGN KEY FK_66EF20482C2AC5D3');
        $this->addSql('ALTER TABLE entity_property DROP FOREIGN KEY FK_2906A09E81257D5D');
        $this->addSql('ALTER TABLE entity_relation DROP FOREIGN KEY FK_C07DFB0949ED069B');
        $this->addSql('ALTER TABLE entity_relation DROP FOREIGN KEY FK_C07DFB0912F5C155');
        $this->addSql('DROP TABLE rule_expression');
        $this->addSql('DROP TABLE rule');
        $this->addSql('DROP TABLE rule_operand');
        $this->addSql('DROP TABLE entity_property');
        $this->addSql('DROP TABLE entity_relation');
        $this->addSql('DROP TABLE entity_relation_translation');
        $this->addSql('DROP TABLE entity');
    }
}
