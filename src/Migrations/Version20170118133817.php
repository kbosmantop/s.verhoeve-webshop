<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170118133817 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer ADD registered_on_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09716C8353 FOREIGN KEY (registered_on_site_id) REFERENCES site (id)');
        $this->addSql('CREATE INDEX IDX_81398E09716C8353 ON customer (registered_on_site_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `customer` DROP FOREIGN KEY FK_81398E09716C8353');
        $this->addSql('DROP INDEX IDX_81398E09716C8353 ON `customer`');
        $this->addSql('ALTER TABLE `customer` DROP registered_on_site_id');
    }
}
