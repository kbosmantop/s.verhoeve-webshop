<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171102083931 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        return;
        /**
         * @var EntityManagerInterface $em
         */
        $em = $this->container->get('doctrine')->getManager();
        $em->getFilters()->disable('softdeleteable');
        $em->beginTransaction();

        $result = $em->createQueryBuilder()
            ->update(Customer::class, 'c')
            ->set('c.username', ':username')
            ->setParameter('username', null)
            ->set('c.usernameCanonical', ':canonical')
            ->setParameter('canonical', null)
            ->andWhere('c.deletedAt IS NOT NULL')
            ->getQuery()
            ->execute();

        $em->commit();

        echo "Rows affected: " . $result;
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
