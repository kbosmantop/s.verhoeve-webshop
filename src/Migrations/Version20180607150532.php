<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180607150532 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function postUp(Schema $schema)
    {
        $conn = $this->connection;

        $insertableEntries = $conn->executeQuery('
            SELECT 
                NULL AS id,
                product_variations.id AS product_id,
                NULL AS product_variation_id,
                product_property.productgroup_property_id,
                product_property.productgroup_property_option_id,
                product_property.value,
                product_property.created_at,
                product_property.updated_at,
                product_property.deleted_at
            FROM `product_property` 
            LEFT JOIN product ON product.id = product_property.product_id
            LEFT JOIN product AS product_variations ON product.id = product_variations.parent_id
            WHERE product.parent_id IS NULL  
         ')->fetchAll();

        $insertQuery = $conn->createQueryBuilder()->insert('product_property');

        foreach ($insertableEntries as $entry) {
            $entry = array_filter($entry, function ($value) {
                return null !== $value;
            });

            $entry = array_map(function ($value) use ($conn) {
                return $conn->quote($value);
            }, $entry);

            try {
                $insertQuery->values($entry)->execute();
            } catch (\Exception $exception) {
                //get output of possible duplicate entries to review for manual addition
                echo $exception->getMessage() . PHP_EOL;
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
