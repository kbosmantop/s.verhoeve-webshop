<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180905094316 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var string */
    private const OPTION_KEY = 'bouquet_format';

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {

            $conn = $this->connection;

            $propertySetId = $this->getPropertySetId();

            $succeeded = $conn->createQueryBuilder()
                ->insert('productgroup_property')
                ->values([
                    'form_type' => $conn->quote('choice'),
                    'form_type_options' => $conn->quote('required'),
                    'configurable' => 1,
                    'filterable' => 0,
                    '`key`' => $conn->quote(self::OPTION_KEY),
                    'created_at' => 'NOW()',
                    'updated_at' => 'NOW()',
                ])
                ->execute();

            if ($succeeded) {
                $this->write('<info>Productgroup property created</info>');
            }

            $propertyId = $conn->lastInsertId();

            $succeeded = $conn->createQueryBuilder()
                ->insert('productgroup_property_set_productgroup_property')
                ->values([
                    'productgroup_property_set_id' => $propertySetId,
                    'productgroup_property_id' => $propertyId,
                ])
                ->execute();

            if ($succeeded) {
                $this->write('<info>Assign productgroup property to property set</info>');
            }

            $succeeded = $conn->createQueryBuilder()
                ->insert('productgroup_property_translation')
                ->values([
                    'translatable_id' => $propertyId,
                    'description' => $conn->quote('Boeket formaat'),
                    'slug' => $conn->quote('boeket-formaat'),
                    'locale' => $conn->quote('nl_NL'),
                ])
                ->execute();

            if ($succeeded) {
                $this->write('<info>Translation productgroup property created</info>');
            }

            $this->insertProductgroupPropertyOptions($propertyId, ['Standaard', 'Groot', 'XL']);
        }
    }

    /**
     * @param Schema $schema
     */
    public function preDown(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {

            $propertyId = $this->getPropertyId();

            $succeeded = $this->connection->createQueryBuilder()
                ->delete('product_property')
                ->andWhere('productgroup_property_id = :id')
                ->setParameter('id', $propertyId)
                ->execute();

            if ($succeeded) {
                $this->write('<info>Product property options values removed</info>');
            }

            $succeeded = $this->connection->createQueryBuilder()
                ->delete('productgroup_property_option')
                ->andWhere('productgroup_property_id = :id')
                ->setParameter('id', $propertyId)
                ->execute();

            if ($succeeded) {
                $this->write('<info>Productgroup property options removed</info>');
            }

            $succeeded = $this->connection->createQueryBuilder()
                ->delete('productgroup_property_translation')
                ->andWhere('translatable_id = :translatableId')
                ->setParameter('translatableId', $propertyId)
                ->execute();

            if ($succeeded) {
                $this->write('<info>Translation productgroup property removed</info>');
            }

            $succeeded = $this->connection->createQueryBuilder()
                ->delete('productgroup_property')
                ->andWhere('`key` = :key')
                ->setParameter('key', self::OPTION_KEY)
                ->execute();

            if ($succeeded) {
                $this->write('<info>Translation productgroup property removed</info>');
            }
        }
    }

    /**
     * @return bool|string
     */
    private function getPropertyId()
    {
        return $this->connection->createQueryBuilder()
            ->select('id')
            ->from('productgroup_property')
            ->where('`key` = :key')
            ->setParameter('key', self::OPTION_KEY)
            ->execute()
            ->fetchColumn();
    }

    /**
     * @return bool|string
     */
    private function getPropertySetId()
    {
        return $this->connection->createQueryBuilder()
            ->select('id')
            ->from('productgroup_property_set')
            ->where('name LIKE :name')
            ->setParameter('name', 'Bloemen%')
            ->execute()
            ->fetchColumn();
    }

    /**
     * @param int   $propertyId
     * @param array $values
     */
    private function insertProductgroupPropertyOptions(int $propertyId, array $values)
    {
        $conn = $this->connection;

        foreach ($values as $key => $value) {
            $succeeded = $conn->createQueryBuilder()
                ->insert('productgroup_property_option')
                ->values([
                    'productgroup_property_id' => $propertyId,
                    'position' => $key + 1,
                    'value' => $conn->quote($value),
                    'created_at' => 'NOW()',
                    'updated_at' => 'NOW()',
                ])
                ->execute();

            if ($succeeded) {
                $this->write('<info>' . sprintf("Productgroup property option '%s' created", $value) . '</info>');
            }
        }
    }
}
