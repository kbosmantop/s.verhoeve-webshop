<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170720124236 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_status ADD position INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $em = $this->container->get("doctrine")->getManager();

            if ($em->find(OrderStatus::class, "cancelled")) {
                $em->find(OrderStatus::class, "cancelled")->setPosition(1);
            }

            if ($em->find(OrderStatus::class, "on_hold")) {
                $em->find(OrderStatus::class, "on_hold")->setPosition(2);
            }

            if ($em->find(OrderStatus::class, "payment_pending")) {
                $em->find(OrderStatus::class, "payment_pending")->setPosition(3);
            }

            if ($em->find(OrderStatus::class, "pending")) {
                $em->find(OrderStatus::class, "pending")->setPosition(5);
            }

            if ($em->find(OrderStatus::class, "processed")) {
                $em->find(OrderStatus::class, "processed")->setPosition(6);
            }

            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_status DROP position');
    }
}
