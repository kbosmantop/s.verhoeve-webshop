<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use PDO;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Version20180612140521
 * @package Application\Migrations
 */
class Version20180612140521 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function postUp(Schema $schema)
    {
        $filesystem = $this->container->get('filesystem_public');

        $productImages = $this->getProductImages();

        if ($productImages) {

            /** @var Connection $conn */
            $conn = $this->container->get('doctrine')->getConnection();
            $qb = $conn->createQueryBuilder();

            $qb->insert('product_image');
            $qb->values([
                'product_id' => ':productId',
                'path' => ':path',
                'description' => ':description',
                'main' => ':main',
                'position' => 1,
                'created_at' => 'NOW()',
                'updated_at' => 'NOW()',
            ]);

            $index = [];

            foreach ($productImages as $productImage) {

                $productId = $productImage['product_id'];
                $orgProductId = $productImage['org_product_id'];
                $entityType = $productImage['entity_type'];
                $path = $productImage['path'];
                $description = $productImage['description'];

                $newFileExist = false;
                $newPath = null;
                if ($filesystem->has($productImage['path'])) {

                    $filePathParts = \explode('/', $path);

                    $newPath = 'images/product/' . $productId . '/' . end($filePathParts);

                    if (!$filesystem->has($newPath)) {
                        $newFileExist = $filesystem->copy($path, $newPath);
                    } else {
                        $newFileExist = true;
                    }
                }

                if (!$newFileExist || null === $newPath) {
                    continue;
                }

                $qb->setParameter('productId', $productId);
                $qb->setParameter('path', $newPath);
                $qb->setParameter('description', $description);

                $isMain = (($entityType === 'product_personalization' AND !\in_array($productId, $index, true))  ? 1 : 0);

                $qb->setParameter('main', $isMain);

                if ($qb->execute()) {
                    $msg = 'Copied image from product (id: %s) to variation product (id: %s)';
                    $msg = sprintf($msg, $orgProductId, $productId);

                    $this->write('<info>' . $msg . '</info>');
                }

                $index[] = $productId;
            }

            $qb = $conn->createQueryBuilder();

            $qb
                ->select('`id`')
                ->from('product_image', 'pi')
                ->leftJoin('pi', 'product_image_product', 'pip', 'pip.product_image_id = pi.id')
                ->andWhere('`pip`.`product_image_id` IS NOT NULL')
                ->groupBy('`id`')
            ;

            $ids = $qb->execute()->fetchAll(PDO::FETCH_COLUMN);

            if($ids) {
                // Delete images existing in `product_image_product`
                $qb = $conn->createQueryBuilder();

                $qb
                    ->delete('product_image')
                    ->andWhere('id IN(:ids)')
                    ->setParameter('ids', $ids, Connection::PARAM_INT_ARRAY);

                if ($qb->execute()) {
                    $msg = 'Remove rows (ids: %s) in `product_image` that exist in `product_image_product`';
                    $msg = sprintf($msg, implode(',', $ids));
                    $this->write('<info>' . $msg . '</info>');
                }
            }
        }
    }

    /**
     * @return array
     */
    private function getProductImages(): array
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();

        $qb->select('`pip`.*, `pi`.`path`, `pi`.`description`, `pi`.`product_id` `org_product_id`, `p`.`entity_type`, `pi`.`main`, `pi`.`category`');
        $qb->from('`product_image_product`', '`pip`');
        $qb->leftJoin('`pip`', '`product_image`', '`pi`', '`pi`.`id` = `pip`.`product_image_id`');
        $qb->leftJoin('`pip`', '`product`', '`p`', '`p`.`id` = `pip`.`product_id`');
        $qb->where('`pi`.`deleted_at` IS NULL');
        $qb->orderBy('`pi`.`main`', 'DESC');
        $qb->addOrderBy('`pi`.`category`', 'DESC');

        return $qb->execute()->fetchAll();
    }

}
