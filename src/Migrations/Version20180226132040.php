<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180226132040 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address ADD number_addition VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE cart ADD invoice_address_number_addition VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE cart_order ADD delivery_address_number_addition VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD delivery_address_number_addition VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD invoice_address_number_addition VARCHAR(20) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address DROP number_addition');
        $this->addSql('ALTER TABLE cart DROP invoice_address_number_addition');
        $this->addSql('ALTER TABLE cart_order DROP delivery_address_number_addition');
        $this->addSql('ALTER TABLE order_order DROP delivery_address_number_addition');
        $this->addSql('ALTER TABLE `order` DROP invoice_address_number_addition');
    }
}
