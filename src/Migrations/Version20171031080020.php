<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Supplier\DeliveryArea;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031080020 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

    }

    public function postUp(Schema $schema)
    {

        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();

        $em->beginTransaction();

        try {
            $em->createQueryBuilder()
                ->update(DeliveryArea::class, 'da')
                ->set("da.deliveryInterval", ':deliveryInterval')
                ->andWhere("da.deliveryInterval = 'P1DT17H'")
                ->setParameter('deliveryInterval', 'P1D7H')
                ->getQuery()
                ->execute();


            $em->commit();

            $this->write('Succeed to update `P1DT17H` into `P1D7H` in `DeliveryArea`.');

        } catch (\Exception $exception) {
            $em->rollback();
            $this->write($exception->getMessage());
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
