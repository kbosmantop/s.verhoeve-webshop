<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190312084019 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_establishment_timeslot (company_establishment_id INT NOT NULL, timeslot_id INT NOT NULL, INDEX IDX_EEE0056326ADA4EA (company_establishment_id), INDEX IDX_EEE00563F920B9E9 (timeslot_id), PRIMARY KEY(company_establishment_id, timeslot_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_establishment_timeslot ADD CONSTRAINT FK_EEE0056326ADA4EA FOREIGN KEY (company_establishment_id) REFERENCES company_establishment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_establishment_timeslot ADD CONSTRAINT FK_EEE00563F920B9E9 FOREIGN KEY (timeslot_id) REFERENCES timeslot (id) ON DELETE CASCADE');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_establishment_timeslot');
    }
}
