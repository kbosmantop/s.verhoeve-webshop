<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116112920 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE designer_template (id INT AUTO_INCREMENT NOT NULL, layout_id INT DEFAULT NULL, background_image_id INT DEFAULT NULL, INDEX IDX_81C553D38C22AA1A (layout_id), INDEX IDX_81C553D3E6DA28AA (background_image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_background_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_F3B7F9DC2C2AC5D3 (translatable_id), UNIQUE INDEX designer_background_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_layout (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, width DOUBLE PRECISION NOT NULL, height DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_font (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, family VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1269E8A2A5E6215B (family), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_background (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_template_object (id INT AUTO_INCREMENT NOT NULL, template_id INT DEFAULT NULL, type VARCHAR(10) NOT NULL, x DOUBLE PRECISION NOT NULL, y DOUBLE PRECISION NOT NULL, width DOUBLE PRECISION NOT NULL, height DOUBLE PRECISION NOT NULL, editable TINYINT(1) NOT NULL, selectable TINYINT(1) NOT NULL, INDEX IDX_35BF991B5DA0FB8 (template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D38C22AA1A FOREIGN KEY (layout_id) REFERENCES designer_layout (id)');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3E6DA28AA FOREIGN KEY (background_image_id) REFERENCES designer_background (id)');
        $this->addSql('ALTER TABLE designer_background_translation ADD CONSTRAINT FK_F3B7F9DC2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES designer_background (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE designer_template_object ADD CONSTRAINT FK_35BF991B5DA0FB8 FOREIGN KEY (template_id) REFERENCES designer_template (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE designer_template_object DROP FOREIGN KEY FK_35BF991B5DA0FB8');
        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D38C22AA1A');
        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3E6DA28AA');
        $this->addSql('ALTER TABLE designer_background_translation DROP FOREIGN KEY FK_F3B7F9DC2C2AC5D3');
        $this->addSql('DROP TABLE designer_template');
        $this->addSql('DROP TABLE designer_background_translation');
        $this->addSql('DROP TABLE designer_layout');
        $this->addSql('DROP TABLE designer_font');
        $this->addSql('DROP TABLE designer_background');
        $this->addSql('DROP TABLE designer_template_object');
    }
}
