<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180316160601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX `IDX_F3B7F9DC2C2AC5D3` ON `designer_background_translation`');
        $this->addSql('DROP TABLE designer_background_translation');

        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3E6DA28AA');
        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3DD8C9D23');
        $this->addSql('DROP TABLE designer_background');

        $this->addSql('ALTER TABLE `designer_template` DROP `background_image_id`, DROP `uuid`, DROP canvas_id');

        $this->addSql('RENAME TABLE `designer_template` TO `designer_collage`');

        // Create new designer template setup.
        $this->addSql('CREATE TABLE `designer_template` (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, canvas_id INT DEFAULT NULL, uuid VARCHAR(36) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3DD8C9D23 FOREIGN KEY (canvas_id) REFERENCES designer_canvas (id)');

        $this->addSql('CREATE INDEX IDX_81C553D3DD8C9D23 ON designer_template (canvas_id)');

        // Rename designer template_object into collage_object
        $this->addSql('RENAME TABLE `designer_template_object` TO `designer_collage_object`');

        $this->addSql('ALTER TABLE `designer_collage_object` CHANGE `template_id` `collage_id` INT(11), DROP `editable`, DROP `selectable`');

        $this->addSql('ALTER TABLE designer_collage_object ADD CONSTRAINT FK_DE5FCC8FEC9066A4 FOREIGN KEY (collage_id) REFERENCES designer_collage (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3DD8C9D23');
        $this->addSql('DROP INDEX `IDX_81C553D3DD8C9D23` ON `designer_template`');
        $this->addSql('DROP TABLE `designer_template`');

        $this->addSql('ALTER TABLE designer_collage_object DROP FOREIGN KEY FK_DE5FCC8FEC9066A4');

        $this->addSql('RENAME TABLE `designer_collage` TO `designer_template`');
        $this->addSql('RENAME TABLE `designer_collage_object` TO `designer_template_object`');
        $this->addSql('ALTER TABLE `designer_template_object` CHANGE `collage_id` `template_id` INT(11), ADD `editable` TINYINT(1) NOT NULL, ADD `selectable` TINYINT(1) NOT NULL');

        $this->addSql('ALTER TABLE `designer_template` ADD `background_image_id` INT(11), ADD `uuid` VARCHAR(36), ADD canvas_id INT(11)');

        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3DD8C9D23 FOREIGN KEY (canvas_id) REFERENCES designer_canvas (id)');

        $this->addSql('CREATE TABLE designer_background (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_background_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX designer_background_translation_unique_translation (translatable_id, locale), INDEX IDX_F3B7F9DC2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE designer_background_translation ADD CONSTRAINT FK_F3B7F9DC2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES designer_background (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3E6DA28AA FOREIGN KEY (background_image_id) REFERENCES designer_background (id)');

    }

}
