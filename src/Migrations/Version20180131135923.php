<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\Company;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180131135923 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD invoice_email VARCHAR(200) DEFAULT NULL');
    }

    public function postUp(Schema $schema)
    {
        return;
        /** @var Company[] $companies */
        $companies = $this->container->get("doctrine")->getRepository(Company::class)->findAll();

        foreach ($companies as $company) {
            if ($company->getInvoiceAddress()) {
                $company->setInvoiceEmail($company->getInvoiceAddress()->getEmail());
            }
        }

        $this->container->get("doctrine")->getManager()->flush();
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP invoice_email');
    }
}
