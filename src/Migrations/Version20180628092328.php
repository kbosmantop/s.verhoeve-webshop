<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180628092328 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parents = [];

        $products = $this->connection->createQueryBuilder()
            ->select('p.id, p.parent_id')
            ->from('product', 'p')
            ->leftJoin('p', 'product', 'parent', 'p.parent_id = parent.id')
            ->andWhere('parent.parent_id IS NOT NULL')
            ->andWhere('p.deleted_at IS NULL')
            ->andWhere('parent.deleted_at IS NULL')
            ->execute()
            ->fetchAll()
        ;

        $updateQuery = $this->connection->createQueryBuilder()
            ->update('product')
            ->set('parent_id', ':parent')
            ->where('id = :product')
        ;

        $this->connection->beginTransaction();

        foreach($products as $product) {
            if(!isset($parents[$product['parent_id']])) {
                $qb = $this->connection->createQueryBuilder();

                $parent = $qb->select('*')->from('product')->where('id = :id')->setParameter('id', $product['parent_id'])->execute()->fetch();
                unset($parent['id'], $parent['uuid'], $parent['sku'], $parent['parent_id']);

                $parent = array_filter($parent, function ($f) {
                    return null !== $f;
                });

                $parent = array_map(function ($value) {
                    return $this->connection->quote($value);
                }, $parent);

                $newParent = $parent;
                $newParent['uuid'] = $this->connection->quote(Uuid::uuid4());

                $qb = $this->connection->createQueryBuilder();
                $qb->insert('product')->values($newParent)->execute();
                $newParentId = $this->connection->lastInsertId();

                $parents[$product['parent_id']] = $newParentId;

                $qb = $this->connection->createQueryBuilder();
                //set translation
                $translations = $qb->select('*')
                    ->from('product_translation')
                    ->where('translatable_id = :transid')
                    ->setParameter('transid', $product['parent_id'])
                    ->execute()
                    ->fetchAll();

                foreach ($translations as $key => $translation) {
                    $translation = array_filter($translation, function ($f) {
                        return null !== $f;
                    });

                    $translation = array_map(function ($value) {
                        return $this->connection->quote($value);
                    }, $translation);

                    $translation['translatable_id'] = $newParentId;
                    $translation['title'] = $this->connection->quote('[hersteld] ' . $translation['title']);

                    unset($translation['id'], $translation['slug']);

                    $qb->insert('product_translation')->values($translation)->execute();
                }

            } else {
                $newParentId = $parents[$product['parent_id']];
            }

            $updateQuery->setParameters([
                'parent' => $newParentId,
                'product' => $product['id']
            ])->execute();
        }

        $this->connection->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
