<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180403095038 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE report_chart (id INT AUTO_INCREMENT NOT NULL, report_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type ENUM(\'bar\', \'line\', \'pie\') DEFAULT NULL COMMENT \'(DC2Type:ReportChartType)\', date_filter VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CF4A2D534BD2A4C0 (report_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report_chart ADD CONSTRAINT FK_CF4A2D534BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id)');
        $this->addSql('CREATE TABLE report_chart_data (id INT AUTO_INCREMENT NOT NULL, report_chart_id INT DEFAULT NULL, from_date DATETIME DEFAULT NULL, till_date DATETIME DEFAULT NULL, sort_by_path VARCHAR(255) NOT NULL, data_path VARCHAR(255) NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, data_label VARCHAR(255) NOT NULL, INDEX IDX_EC528217BE98A777 (report_chart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report_chart_data ADD CONSTRAINT FK_EC528217BE98A777 FOREIGN KEY (report_chart_id) REFERENCES report_chart (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE report_chart_data');
        $this->addSql('DROP TABLE report_chart');
    }
}
