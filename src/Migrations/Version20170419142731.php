<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170419142731 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplier_group_product (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, product_id INT DEFAULT NULL, sku VARCHAR(20) NOT NULL, pickup_enabled TINYINT(1) DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_4797D0662ADD6D8C (supplier_id), INDEX IDX_4797D0664584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier_group_product_product (supplier_group_product_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_F0F5ED0BC548B11D (supplier_group_product_id), INDEX IDX_F0F5ED0B4584665A (product_id), PRIMARY KEY(supplier_group_product_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_group_product ADD CONSTRAINT FK_4797D0662ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier_group (id)');
        $this->addSql('ALTER TABLE supplier_group_product ADD CONSTRAINT FK_4797D0664584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE supplier_group_product_product ADD CONSTRAINT FK_F0F5ED0BC548B11D FOREIGN KEY (supplier_group_product_id) REFERENCES supplier_group_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_group_product_product ADD CONSTRAINT FK_F0F5ED0B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_group_product_product DROP FOREIGN KEY FK_F0F5ED0BC548B11D');
        $this->addSql('DROP TABLE supplier_group_product');
        $this->addSql('DROP TABLE supplier_group_product_product');
    }
}
