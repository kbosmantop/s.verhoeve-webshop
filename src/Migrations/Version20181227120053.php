<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181227120053 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplier_group_vat_group (supplier_group_id INT NOT NULL, vat_group_id INT NOT NULL, INDEX IDX_9E5C7F8373DDB1C6 (supplier_group_id), INDEX IDX_9E5C7F83E8C706E8 (vat_group_id), PRIMARY KEY(supplier_group_id, vat_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_group_vat_group ADD CONSTRAINT FK_9E5C7F8373DDB1C6 FOREIGN KEY (supplier_group_id) REFERENCES supplier_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_group_vat_group ADD CONSTRAINT FK_9E5C7F83E8C706E8 FOREIGN KEY (vat_group_id) REFERENCES vat_group (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplier_group_vat_group');
    }
}
