<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180709121049
 * @package Application\Migrations
 */
class Version20180709121049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site ADD parent_id INT DEFAULT NULL, ADD slug VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE site ADD CONSTRAINT FK_694309E4727ACA70 FOREIGN KEY (parent_id) REFERENCES site (id)');
        $this->addSql('CREATE INDEX IDX_694309E4727ACA70 ON site (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_694309E4989D9B62 ON site (slug)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site DROP FOREIGN KEY FK_694309E4727ACA70');
        $this->addSql('DROP INDEX IDX_694309E4727ACA70 ON site');
        $this->addSql('DROP INDEX UNIQ_694309E4989D9B62 ON site');
        $this->addSql('ALTER TABLE site DROP parent_id, DROP slug');
    }
}
