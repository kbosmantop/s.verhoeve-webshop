<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Version20180613063959
 * @package Application\Migrations
 */
class Version20180613063959 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();

        $qb->select('pi.id image_id, p.parent_id');
        $qb->from('product_image', 'pi');
        $qb->leftJoin('pi', 'product', 'p', 'p.id = pi.product_id');
        $qb->where('main = 0');
        $qb->andWhere('category = 0');
        $qb->andWhere('parent_id IS NOT NULL');
        $qb->andWhere('entity_type <> :entityType');
        $qb->setParameter('entityType', 'product_personalization');

        $productImages = $qb->execute()->fetchAll();

        if(null !== $productImages) {

            $qb = $conn->createQueryBuilder();
            $qb->update('product_image');
            $qb->set('product_id', ':parentId');
            $qb->andWhere('id = :id');

            foreach ($productImages as $productImage) {

                if(!isset($productImage['parent_id'], $productImage['image_id'])) {
                    continue;
                }

                $qb->setParameter('id', $productImage['image_id']);
                $qb->setParameter('parentId', $productImage['parent_id']);

                if($qb->execute()) {
                    $this->write('<info>' . sprintf('Assign image (id: %s) to parent product (id: %s)', $productImage['image_id'],
                            $productImage['parent_id']) . '</info>');
                }
            }
        }
    }
}
