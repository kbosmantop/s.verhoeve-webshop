<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171215083216 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_packaging_unit (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, packaging_unit_id VARCHAR(2) DEFAULT NULL, quantity INT NOT NULL, dimensions_length INT NOT NULL, dimensions_width INT NOT NULL, dimensions_height INT NOT NULL, INDEX IDX_D7A6A34D4584665A (product_id), INDEX IDX_D7A6A34D101900D0 (packaging_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE packaging_unit (iso_code VARCHAR(2) NOT NULL, name VARCHAR(50) NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(iso_code)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_packaging_unit ADD CONSTRAINT FK_D7A6A34D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_packaging_unit ADD CONSTRAINT FK_D7A6A34D101900D0 FOREIGN KEY (packaging_unit_id) REFERENCES packaging_unit (iso_code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_packaging_unit DROP FOREIGN KEY FK_D7A6A34D101900D0');
        $this->addSql('DROP TABLE product_packaging_unit');
        $this->addSql('DROP TABLE packaging_unit');
    }
}
