<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180307112705 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE promotion_cart_company DROP FOREIGN KEY FK_D2B2044B42A83B85');
        $this->addSql('ALTER TABLE promotion_cart_customer DROP FOREIGN KEY FK_86C9471D42A83B85');
        $this->addSql('ALTER TABLE promotion_cart_customergroup DROP FOREIGN KEY FK_D4E57042A83B85');
        $this->addSql('ALTER TABLE promotion_cart_product DROP FOREIGN KEY FK_4E4709A942A83B85');
        $this->addSql('ALTER TABLE promotion_cart_site DROP FOREIGN KEY FK_F7D1D8F042A83B85');
        $this->addSql('ALTER TABLE promotion_catalog_company DROP FOREIGN KEY FK_EC712E2089924D58');
        $this->addSql('ALTER TABLE promotion_catalog_customer_group DROP FOREIGN KEY FK_CD59D82D89924D58');
        $this->addSql('ALTER TABLE promotion_catalog_product DROP FOREIGN KEY FK_708423C289924D58');
        $this->addSql('ALTER TABLE promotion_catalog_productgroup DROP FOREIGN KEY FK_8591350489924D58');
        $this->addSql('ALTER TABLE promotion_catalog_site DROP FOREIGN KEY FK_7A217C9189924D58');
        $this->addSql('DROP TABLE promotion_cart');
        $this->addSql('DROP TABLE promotion_cart_company');
        $this->addSql('DROP TABLE promotion_cart_customer');
        $this->addSql('DROP TABLE promotion_cart_customergroup');
        $this->addSql('DROP TABLE promotion_cart_product');
        $this->addSql('DROP TABLE promotion_cart_site');
        $this->addSql('DROP TABLE promotion_catalog');
        $this->addSql('DROP TABLE promotion_catalog_company');
        $this->addSql('DROP TABLE promotion_catalog_customer_group');
        $this->addSql('DROP TABLE promotion_catalog_product');
        $this->addSql('DROP TABLE promotion_catalog_productgroup');
        $this->addSql('DROP TABLE promotion_catalog_site');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promotion_cart (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_cart_company (promotion_cart_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_D2B2044B42A83B85 (promotion_cart_id), INDEX IDX_D2B2044B979B1AD6 (company_id), PRIMARY KEY(promotion_cart_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_cart_customer (promotion_cart_id INT NOT NULL, customer_id INT NOT NULL, INDEX IDX_86C9471D42A83B85 (promotion_cart_id), INDEX IDX_86C9471D9395C3F3 (customer_id), PRIMARY KEY(promotion_cart_id, customer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_cart_customergroup (promotion_cart_id INT NOT NULL, customergroup_id INT NOT NULL, INDEX IDX_D4E57042A83B85 (promotion_cart_id), INDEX IDX_D4E57096C19C84 (customergroup_id), PRIMARY KEY(promotion_cart_id, customergroup_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_cart_product (promotion_cart_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_4E4709A942A83B85 (promotion_cart_id), INDEX IDX_4E4709A94584665A (product_id), PRIMARY KEY(promotion_cart_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_cart_site (promotion_cart_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_F7D1D8F042A83B85 (promotion_cart_id), INDEX IDX_F7D1D8F0F6BD1646 (site_id), PRIMARY KEY(promotion_cart_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog_company (promotion_catalog_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_EC712E2089924D58 (promotion_catalog_id), INDEX IDX_EC712E20979B1AD6 (company_id), PRIMARY KEY(promotion_catalog_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog_customer_group (promotion_catalog_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_CD59D82D89924D58 (promotion_catalog_id), INDEX IDX_CD59D82DD2919A68 (customer_group_id), PRIMARY KEY(promotion_catalog_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog_product (promotion_catalog_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_708423C289924D58 (promotion_catalog_id), INDEX IDX_708423C24584665A (product_id), PRIMARY KEY(promotion_catalog_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog_productgroup (promotion_catalog_id INT NOT NULL, productgroup_id INT NOT NULL, INDEX IDX_8591350489924D58 (promotion_catalog_id), INDEX IDX_859135045BC5238A (productgroup_id), PRIMARY KEY(promotion_catalog_id, productgroup_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promotion_catalog_site (promotion_catalog_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_7A217C9189924D58 (promotion_catalog_id), INDEX IDX_7A217C91F6BD1646 (site_id), PRIMARY KEY(promotion_catalog_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promotion_cart_company ADD CONSTRAINT FK_D2B2044B42A83B85 FOREIGN KEY (promotion_cart_id) REFERENCES promotion_cart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_company ADD CONSTRAINT FK_D2B2044B979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_customer ADD CONSTRAINT FK_86C9471D42A83B85 FOREIGN KEY (promotion_cart_id) REFERENCES promotion_cart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_customer ADD CONSTRAINT FK_86C9471D9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_customergroup ADD CONSTRAINT FK_D4E57042A83B85 FOREIGN KEY (promotion_cart_id) REFERENCES promotion_cart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_customergroup ADD CONSTRAINT FK_D4E57096C19C84 FOREIGN KEY (customergroup_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_product ADD CONSTRAINT FK_4E4709A942A83B85 FOREIGN KEY (promotion_cart_id) REFERENCES promotion_cart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_product ADD CONSTRAINT FK_4E4709A94584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_site ADD CONSTRAINT FK_F7D1D8F042A83B85 FOREIGN KEY (promotion_cart_id) REFERENCES promotion_cart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_cart_site ADD CONSTRAINT FK_F7D1D8F0F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_company ADD CONSTRAINT FK_EC712E2089924D58 FOREIGN KEY (promotion_catalog_id) REFERENCES promotion_catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_company ADD CONSTRAINT FK_EC712E20979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_customer_group ADD CONSTRAINT FK_CD59D82D89924D58 FOREIGN KEY (promotion_catalog_id) REFERENCES promotion_catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_customer_group ADD CONSTRAINT FK_CD59D82DD2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_product ADD CONSTRAINT FK_708423C24584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_product ADD CONSTRAINT FK_708423C289924D58 FOREIGN KEY (promotion_catalog_id) REFERENCES promotion_catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_productgroup ADD CONSTRAINT FK_859135045BC5238A FOREIGN KEY (productgroup_id) REFERENCES productgroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_productgroup ADD CONSTRAINT FK_8591350489924D58 FOREIGN KEY (promotion_catalog_id) REFERENCES promotion_catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_site ADD CONSTRAINT FK_7A217C9189924D58 FOREIGN KEY (promotion_catalog_id) REFERENCES promotion_catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE promotion_catalog_site ADD CONSTRAINT FK_7A217C91F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
    }
}
