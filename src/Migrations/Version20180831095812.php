<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Entity\Relation\Company as Supplier;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Services\SupplierManagerService;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180831095812 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // supplier_order_line
        $this->addSql('CREATE TABLE supplier_order_line (id INT AUTO_INCREMENT NOT NULL, supplier_order_id INT DEFAULT NULL, order_line_id INT DEFAULT NULL, supplier_product_id INT DEFAULT NULL, supplier_group_product_id INT DEFAULT NULL, vat_id INT DEFAULT NULL, quantity INT NOT NULL, price DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_FD1443081605B9 (supplier_order_id), INDEX IDX_FD144308BB01DC09 (order_line_id), INDEX IDX_FD1443082475ABB3 (supplier_product_id), INDEX IDX_FD144308C548B11D (supplier_group_product_id), INDEX IDX_FD144308B5B63A6B (vat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier_order (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, order_id INT DEFAULT NULL, reference VARCHAR(40) DEFAULT NULL, status VARCHAR(20) NOT NULL, commissionable TINYINT(1) NOT NULL, recommission TINYINT(1) DEFAULT \'0\' NOT NULL, connector VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_2C3291B22ADD6D8C (supplier_id), INDEX IDX_2C3291B28D9F6D38 (order_id), INDEX IDX_2C3291B27B00651C (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD1443081605B9 FOREIGN KEY (supplier_order_id) REFERENCES supplier_order (id)');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD144308BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id)');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD1443082475ABB3 FOREIGN KEY (supplier_product_id) REFERENCES supplier_product (id)');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD144308C548B11D FOREIGN KEY (supplier_group_product_id) REFERENCES supplier_group_product (id)');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD144308B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');

        // supplier_order
        $this->addSql('ALTER TABLE supplier_order ADD CONSTRAINT FK_2C3291B22ADD6D8C FOREIGN KEY (supplier_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE supplier_order ADD CONSTRAINT FK_2C3291B28D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE `order` ADD supplier_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993981605B9 FOREIGN KEY (supplier_order_id) REFERENCES supplier_order (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F52993981605B9 ON `order` (supplier_order_id)');

        // rename commission_invoice_order(_line) -> commission_invoice_supplier_order(_line)
        $this->addSql('ALTER TABLE commission_invoice_order_line DROP FOREIGN KEY FK_BB0893234E7367B');
        $this->addSql('DROP INDEX IDX_BB0893234E7367B ON commission_invoice_order_line');
        $this->addSql('RENAME TABLE commission_invoice_order TO commission_invoice_supplier_order');
        $this->addSql('RENAME TABLE commission_invoice_order_line TO commission_invoice_supplier_order_line');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line CHANGE commission_invoice_order_id commission_invoice_supplier_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line RENAME INDEX idx_bb089323b5b63a6b TO IDX_F445BE65B5B63A6B');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order RENAME INDEX idx_BA7AC2395A9DB655 TO IDX_AA52BFD65A9DB655');
        $this->addSql('CREATE INDEX IDX_F445BE6597D0EB7 ON commission_invoice_supplier_order_line (commission_invoice_supplier_order_id)');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line ADD CONSTRAINT FK_F445BE6597D0EB7 FOREIGN KEY (commission_invoice_supplier_order_id) REFERENCES commission_invoice_supplier_order (id) ON DELETE CASCADE');

        // add column commission_invoice_supplier_order_line.supplier_order_line_id
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line ADD supplier_order_line_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line ADD CONSTRAINT FK_F445BE65A9DC5CCE FOREIGN KEY (supplier_order_line_id) REFERENCES supplier_order_line (id)');
        $this->addSql('CREATE INDEX IDX_F445BE65A9DC5CCE ON commission_invoice_supplier_order_line (supplier_order_line_id)');

        // add column commission_invoice_supplier_order.supplier_order_id
        $this->addSql('ALTER TABLE commission_invoice_supplier_order ADD supplier_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order ADD CONSTRAINT FK_AA52BFD61605B9 FOREIGN KEY (supplier_order_id) REFERENCES supplier_order (id)');
        $this->addSql('CREATE INDEX IDX_AA52BFD61605B9 ON commission_invoice_supplier_order (supplier_order_id)');
    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $this->connection->beginTransaction();

        $orders = $this->connection->createQueryBuilder()
            ->select([
                'id',
                'status_id',
                'supplier_id',
                'commissionable',
                'recommission',
                'supplier_connector',
                'created_at',
                'updated_at',
            ])
            ->from('`order`')
            ->andWhere('supplier_id IS NOT NULL')
            ->execute()
            ->fetchAll();

        foreach ($orders as $i => $order) {
            if ($i % 25 === 0) {
                print '.';
            }

            $this->handleOrder($order);
        }

        $this->connection->commit();
    }

    /**
     * @param array $order
     */
    private function handleOrder(array $order)
    {
        $connector = $this->guessConnector($order);

        $this->connection->createQueryBuilder()
            ->insert('supplier_order')
            ->values([
                'status' => ':status',
                'supplier_id' => ':supplier_id',
                'order_id' => ':order_id',
                'commissionable' => ':commissionable',
                'recommission' => ':recommission',
                'connector' => ':connector',
                'created_at' => ':created_at',
                'updated_at' => ':updated_at',
            ])
            ->setParameters([
                'status' => $this->determineSupplierOrderStatus($order),
                'supplier_id' => $order['supplier_id'],
                'order_id' => $order['id'],
                'commissionable' => $order['commissionable'],
                'recommission' => $order['recommission'],
                'connector' => $connector,
                'created_at' => $order['created_at'],
                'updated_at' => $order['updated_at'],
            ])
            ->execute();

        $supplierOrderId = (int)$this->connection->lastInsertId();

        $this->connection->update('`order`', [
            'supplier_order_id' => $supplierOrderId,
        ], [
            'id' => $order['id'],
        ]);

        $this->connection->createQueryBuilder()
            ->update('commission_invoice_supplier_order')
            ->set('supplier_order_id', $supplierOrderId)
            ->where('order_id = :order_id')
            ->setParameters([
                'order_id' => (int)$order['id'],
            ])
            ->execute();

        $orderLines = $this->connection->createQueryBuilder()
            ->select([
                'id',
                'product_id',
                'vat_id',
                'quantity',
                'forward_price',
                'created_at',
                'updated_at',
            ])
            ->from('order_line')
            ->andWhere('order_id = :order_id')
            ->setParameter('order_id', $order['id'])
            ->execute()
            ->fetchAll();

        foreach ($orderLines as $orderLine) {
            $this->handleOrderLine($orderLine, $supplierOrderId, $order['supplier_id']);
        }
    }

    /**
     * Try to get the SupplierProduct (or SupplierGroupProduct) as far is possible
     *
     * @param int $productId
     * @param int $supplierId
     * @return SupplierGroupProduct|SupplierProduct|null
     */
    private function getSupplierProduct(int $productId, int $supplierId)
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Supplier $supplier */
        $supplier = $doctrine->getRepository(Supplier::class)->find($supplierId);

        /** @var Product $product */
        $product = $doctrine->getRepository(Product::class)->find($productId);

        if (!$supplier || !$product) {
            return null;
        }

        try {
            $supplierProduct = $this->container->get(SupplierManagerService::class)->getSupplierProduct($product, $supplier);
        } catch (\RuntimeException $e) {
            return null;
        }

        return $supplierProduct;
    }

    /**
     * @param array $orderLine
     * @param int   $supplierOrderId
     * @param int   $supplierId
     */
    private function handleOrderLine(array $orderLine, int $supplierOrderId, int $supplierId)
    {
        $supplierProduct = $this->getSupplierProduct($orderLine['product_id'], $supplierId);

        $this->connection->createQueryBuilder()
            ->insert('supplier_order_line')
            ->values([
                'supplier_order_id' => ':supplier_order_id',
                'order_line_id' => ':order_line_id',
                'supplier_product_id' => ':supplier_product_id',
                'supplier_group_product_id' => ':supplier_group_product_id',
                'vat_id' => ':vat_id',
                'quantity' => ':quantity',
                'price' => ':price',
                'created_at' => ':created_at',
                'updated_at' => ':updated_at',
            ])
            ->setParameters([
                'supplier_order_id' => $supplierOrderId,
                'order_line_id' => $orderLine['id'],
                'supplier_product_id' => $supplierProduct instanceof SupplierProduct ? $supplierProduct->getId() : null,
                'supplier_group_product_id' => $supplierProduct instanceof SupplierGroupProduct ? $supplierProduct->getId() : null,
                'vat_id' => $orderLine['vat_id'],
                'quantity' => $orderLine['quantity'],
                'price' => $orderLine['forward_price'],
                'created_at' => $orderLine['created_at'],
                'updated_at' => $orderLine['updated_at'],
            ])
            ->execute();

        $supplierOrderLineId = (int)$this->connection->lastInsertId();

        $this->connection->createQueryBuilder()
            ->update('commission_invoice_supplier_order_line')
            ->set('supplier_order_line_id', $supplierOrderLineId)
            ->where('order_line_id = :order_line_id')
            ->setParameters([
                'order_line_id' => (int)$orderLine['id'],
            ])
            ->execute();
    }

    /**
     * @param array $order
     * @return mixed
     */
    private function guessConnector(array $order)
    {
        if ($order['supplier_connector'] !== null) {
            return $order['supplier_connector'];
        }

        if ((int)$order['commissionable'] === 1) {
            return 'Bakker';
        }

        $connector = $this->connection->createQueryBuilder()
            ->select([
                'supplier_connector',
            ])
            ->from('company')
            ->andWhere('id = :companyId')
            ->andWhere('deleted_at IS NULL')
            ->setParameter('companyId', $order['supplier_id'])
            ->execute()
            ->fetchColumn();

        if ($connector === 'Bakker' && (int)$order['commissionable'] === 0) {
            $connector = 'BakkerMail';
        }

        if ($connector === null) {
            $connector = 'Topgeschenken';
        }

        return $connector;
    }

    /**
     * @param array $order
     * @return string
     */
    private function determineSupplierOrderStatus(array $order): string
    {
        switch ($order['status_id']) {
            case 'on_hold':
            case 'payment_pending':
            case 'new':
            case 'cancelled':
                return 'cancelled';
            case 'pending':
                return 'new';
            case 'processed':
                return 'new';
            case 'sent':
                return 'sent';
            case 'complete':
                return 'complete';
            case 'archived':
                // When this migration has been written, no order has been archived yet
                throw new \RuntimeException("Order is archived, we can't reliable determine the supplier order status");
            default:
                throw new \RuntimeException(sprintf("Unknown status '%s'", $order['status_id']));
        }
    }

    /**
     * @param Schema $schema
     * @throws IrreversibleMigrationException
     */
    public function down(Schema $schema)
    {
        throw new IrreversibleMigrationException('');
    }
}
