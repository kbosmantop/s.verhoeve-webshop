<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190319101442 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE carrier (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, short_name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_4739F11C979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carrier_delivery_method (carrier_id INT NOT NULL, delivery_method_id INT NOT NULL, INDEX IDX_48C814AC21DFC797 (carrier_id), INDEX IDX_48C814AC5DED75F5 (delivery_method_id), PRIMARY KEY(carrier_id, delivery_method_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_method (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE carrier ADD CONSTRAINT FK_4739F11C979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE carrier_delivery_method ADD CONSTRAINT FK_48C814AC21DFC797 FOREIGN KEY (carrier_id) REFERENCES carrier (id)');
        $this->addSql('ALTER TABLE carrier_delivery_method ADD CONSTRAINT FK_48C814AC5DED75F5 FOREIGN KEY (delivery_method_id) REFERENCES delivery_method (id)');
        $this->addSql('ALTER TABLE product ADD preferred_carrier_id INT DEFAULT NULL, ADD preferred_delivery_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADECD5DABE FOREIGN KEY (preferred_carrier_id) REFERENCES carrier (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADDC3FE8A FOREIGN KEY (preferred_delivery_method_id) REFERENCES delivery_method (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADECD5DABE ON product (preferred_carrier_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADDC3FE8A ON product (preferred_delivery_method_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE carrier_delivery_method DROP FOREIGN KEY FK_48C814AC21DFC797');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADECD5DABE');
        $this->addSql('ALTER TABLE carrier_delivery_method DROP FOREIGN KEY FK_48C814AC5DED75F5');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADDC3FE8A');
        $this->addSql('DROP TABLE carrier');
        $this->addSql('DROP TABLE carrier_delivery_method');
        $this->addSql('DROP TABLE delivery_method');
        $this->addSql('DROP INDEX IDX_D34A04ADECD5DABE ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADDC3FE8A ON product');
        $this->addSql('ALTER TABLE product DROP preferred_carrier_id, DROP preferred_delivery_method_id');
    }
}
