<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413115456 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE expiration_meta (id INT AUTO_INCREMENT NOT NULL, period INT NOT NULL, guarantee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serial_number_meta (id INT AUTO_INCREMENT NOT NULL, level INT NOT NULL, fix_ser VARCHAR(255) NOT NULL, mask VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD expiration_meta_id INT DEFAULT NULL, ADD serial_number_meta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD45CEFDA1 FOREIGN KEY (expiration_meta_id) REFERENCES expiration_meta (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD2A0F64D3 FOREIGN KEY (serial_number_meta_id) REFERENCES serial_number_meta (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD45CEFDA1 ON product (expiration_meta_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD2A0F64D3 ON product (serial_number_meta_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD45CEFDA1');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD2A0F64D3');
        $this->addSql('DROP INDEX IDX_D34A04AD45CEFDA1 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD2A0F64D3 ON product');
        $this->addSql('ALTER TABLE product DROP expiration_meta_id, DROP serial_number_meta_id');
    }
}
