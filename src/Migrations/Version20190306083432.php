<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20190306083432
 * @package DoctrineMigrations
 */
class Version20190306083432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address ADD uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\' AFTER id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4E6F81D17F50A6 ON address (uuid)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $qb->update('address', 'a')
            ->set('a.uuid', 'UUID()')
            ->where('a.uuid IS NULL')
            ->execute();
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_D4E6F81D17F50A6 ON address');
        $this->addSql('ALTER TABLE address DROP uuid');
    }
}
