<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180509062902 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @description Sets the parameters for WICS for use in the WICS service
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
              'wics_ftp_host' => 'WICS ftp host',
              'wics_ftp_username' => 'WICS ftp gebruikersnaam',
              'wics_ftp_password' => 'WICS ftp wachtwoord',
        ];

        foreach ($parameters as $key => $label) {
            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $label,
                'value' => null,
            ];

            $parameterService->createViaArray($parameter);
        }
    }
}
