<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911121732 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE authentication_log (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, type VARCHAR(32) NOT NULL, event VARCHAR(128) NOT NULL, reason LONGTEXT DEFAULT NULL, ip VARCHAR(64) NOT NULL, user_agent LONGTEXT NOT NULL, created_at DATETIME NOT NULL, username VARCHAR(128) NOT NULL, INDEX IDX_F28AA62D8C03F15C (employee_id), INDEX IDX_F28AA62D9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE authentication_log ADD CONSTRAINT FK_F28AA62D8C03F15C FOREIGN KEY (employee_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE authentication_log ADD CONSTRAINT FK_F28AA62D9395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE authentication_log');
    }
}
