<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170828100108 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment ADD banner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD CONSTRAINT FK_2A5E6D8B684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id)');
        $this->addSql('CREATE INDEX IDX_2A5E6D8B684EC833 ON assortment (banner_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment DROP FOREIGN KEY FK_2A5E6D8B684EC833');
        $this->addSql('DROP INDEX IDX_2A5E6D8B684EC833 ON assortment');
        $this->addSql('ALTER TABLE assortment DROP banner_id');
    }
}
