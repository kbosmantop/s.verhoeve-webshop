<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170306102346 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE site_customer_group (site_id INT NOT NULL, customer_group_id INT NOT NULL, INDEX IDX_E2A3E8F6F6BD1646 (site_id), INDEX IDX_E2A3E8F6D2919A68 (customer_group_id), PRIMARY KEY(site_id, customer_group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_customer_group ADD CONSTRAINT FK_E2A3E8F6F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_customer_group ADD CONSTRAINT FK_E2A3E8F6D2919A68 FOREIGN KEY (customer_group_id) REFERENCES customer_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site ADD disable_customer_login TINYINT(1) NOT NULL, ADD disable_customer_registration TINYINT(1) NOT NULL, ADD authentication_required TINYINT(1) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE site_customer_group');
        $this->addSql('ALTER TABLE site ADD login_required TINYINT(1) NOT NULL, DROP authentication_required, DROP disable_customer_login, DROP disable_customer_registration');
    }
}
