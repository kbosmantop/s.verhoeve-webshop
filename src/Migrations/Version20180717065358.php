<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180717065358 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF6BD1646');
        $this->addSql('DROP INDEX IDX_4FBF094FF6BD1646 ON company');
        $this->addSql('ALTER TABLE company CHANGE site_id menu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FCCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094FCCD7E912 ON company (menu_id)');
        $this->addSql('ALTER TABLE site ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE site ADD CONSTRAINT FK_694309E4979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_694309E4979B1AD6 ON site (company_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FCCD7E912');
        $this->addSql('DROP INDEX IDX_4FBF094FCCD7E912 ON company');
        $this->addSql('ALTER TABLE company CHANGE menu_id site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF6BD1646 FOREIGN KEY (site_id) REFERENCES company_site (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094FF6BD1646 ON company (site_id)');
        $this->addSql('ALTER TABLE site DROP FOREIGN KEY FK_694309E4979B1AD6');
        $this->addSql('DROP INDEX IDX_694309E4979B1AD6 ON site');
        $this->addSql('ALTER TABLE site DROP company_id');
    }
}
