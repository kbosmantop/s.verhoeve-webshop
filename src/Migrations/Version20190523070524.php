<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523070524 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE product_stock DROP FOREIGN KEY FK_EA6A2D3C4584665A');
        $this->addSql('ALTER TABLE product_stock DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE product_stock ADD id INT NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY(id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EA6A2D3C4584665A ON product_stock (product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_stock MODIFY id INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_EA6A2D3C4584665A ON product_stock');
        $this->addSql('ALTER TABLE product_stock DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE product_stock DROP id');
        $this->addSql('ALTER TABLE product_stock ADD PRIMARY KEY (product_id)');
    }
}
