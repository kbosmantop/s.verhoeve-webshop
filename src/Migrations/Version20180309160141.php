<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180309160141 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property_set DROP FOREIGN KEY FK_B04EBF855BC5238A');
        $this->addSql('DROP INDEX fk_b04ebf855bc5238a ON productgroup_property_set');
        $this->addSql('CREATE INDEX IDX_B04EBF855BC5238A ON productgroup_property_set (productgroup_id)');
        $this->addSql('ALTER TABLE productgroup_property_set ADD CONSTRAINT FK_B04EBF855BC5238A FOREIGN KEY (productgroup_id) REFERENCES productgroup (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property_set DROP FOREIGN KEY FK_B04EBF855BC5238A');
        $this->addSql('DROP INDEX idx_b04ebf855bc5238a ON productgroup_property_set');
        $this->addSql('CREATE INDEX FK_B04EBF855BC5238A ON productgroup_property_set (productgroup_id)');
        $this->addSql('ALTER TABLE productgroup_property_set ADD CONSTRAINT FK_B04EBF855BC5238A FOREIGN KEY (productgroup_id) REFERENCES productgroup (id)');
    }
}
