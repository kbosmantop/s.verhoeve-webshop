<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180309154712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adyen_notification DROP FOREIGN KEY FK_B1F60FDA8D9F6D38');
        $this->addSql('DROP INDEX IDX_B1F60FDA8D9F6D38 ON adyen_notification');
        $this->addSql('ALTER TABLE adyen_notification CHANGE order_id order_collection_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adyen_notification ADD CONSTRAINT FK_B1F60FDAFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_B1F60FDAFC2A1E2F ON adyen_notification (order_collection_id)');

        $this->addSql('ALTER TABLE cart DROP FOREIGN KEY FK_BA388B78D9F6D38');
        $this->addSql('DROP INDEX UNIQ_BA388B78D9F6D38 ON cart');
        $this->addSql('ALTER TABLE cart CHANGE order_id order_collection_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart ADD CONSTRAINT FK_BA388B7FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BA388B7FC2A1E2F ON cart (order_collection_id)');

        $this->addSql('ALTER TABLE collo DROP FOREIGN KEY FK_2E7D77F36C678F89');
        $this->addSql('DROP INDEX IDX_2E7D77F36C678F89 ON collo');
        $this->addSql('ALTER TABLE collo CHANGE order_order_id order_id INT NOT NULL');
        $this->addSql('ALTER TABLE collo ADD CONSTRAINT FK_2E7D77F38D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2E7D77F38D9F6D38 ON collo (order_id)');

        $this->addSql('ALTER TABLE twinfield_export_order DROP FOREIGN KEY FK_CB7392AC6C678F89');
        $this->addSql('DROP INDEX IDX_CB7392AC6C678F89 ON twinfield_export_order');
        $this->addSql('ALTER TABLE twinfield_export_order DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE twinfield_export_order CHANGE order_order_id order_id INT NOT NULL');
        $this->addSql('ALTER TABLE twinfield_export_order ADD CONSTRAINT FK_CB7392AC8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_CB7392AC8D9F6D38 ON twinfield_export_order (order_id)');
        $this->addSql('ALTER TABLE twinfield_export_order ADD PRIMARY KEY (export_id, order_id)');

        $this->addSql('ALTER TABLE order_internal_remark DROP FOREIGN KEY FK_85C55AB26C678F89');
        $this->addSql('DROP INDEX IDX_85C55AB26C678F89 ON order_internal_remark');
        $this->addSql('ALTER TABLE order_internal_remark CHANGE order_order_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_internal_remark ADD CONSTRAINT FK_85C55AB28D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id)');
        $this->addSql('CREATE INDEX IDX_85C55AB28D9F6D38 ON order_internal_remark (order_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adyen_notification DROP FOREIGN KEY FK_B1F60FDAFC2A1E2F');
        $this->addSql('DROP INDEX IDX_B1F60FDAFC2A1E2F ON adyen_notification');
        $this->addSql('ALTER TABLE adyen_notification CHANGE order_collection_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adyen_notification ADD CONSTRAINT FK_B1F60FDA8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_B1F60FDA8D9F6D38 ON adyen_notification (order_id)');

        $this->addSql('ALTER TABLE cart DROP FOREIGN KEY FK_BA388B7FC2A1E2F');
        $this->addSql('DROP INDEX UNIQ_BA388B7FC2A1E2F ON cart');
        $this->addSql('ALTER TABLE cart CHANGE order_collection_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart ADD CONSTRAINT FK_BA388B78D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BA388B78D9F6D38 ON cart (order_id)');

        $this->addSql('ALTER TABLE collo DROP FOREIGN KEY FK_2E7D77F38D9F6D38');
        $this->addSql('DROP INDEX IDX_2E7D77F38D9F6D38 ON collo');
        $this->addSql('ALTER TABLE collo CHANGE order_id order_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE collo ADD CONSTRAINT FK_2E7D77F36C678F89 FOREIGN KEY (order_order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2E7D77F36C678F89 ON collo (order_order_id)');

        $this->addSql('ALTER TABLE twinfield_export_order DROP FOREIGN KEY FK_CB7392AC8D9F6D38');
        $this->addSql('DROP INDEX IDX_CB7392AC8D9F6D38 ON twinfield_export_order');
        $this->addSql('ALTER TABLE twinfield_export_order DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE twinfield_export_order CHANGE order_id order_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE twinfield_export_order ADD CONSTRAINT FK_CB7392AC6C678F89 FOREIGN KEY (order_order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_CB7392AC6C678F89 ON twinfield_export_order (order_order_id)');
        $this->addSql('ALTER TABLE twinfield_export_order ADD PRIMARY KEY (export_id, order_order_id)');

        $this->addSql('ALTER TABLE order_internal_remark DROP FOREIGN KEY FK_85C55AB28D9F6D38');
        $this->addSql('DROP INDEX IDX_85C55AB28D9F6D38 ON order_internal_remark');
        $this->addSql('ALTER TABLE order_internal_remark CHANGE order_id order_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_internal_remark ADD CONSTRAINT FK_85C55AB26C678F89 FOREIGN KEY (order_order_id) REFERENCES order_order (id)');
        $this->addSql('CREATE INDEX IDX_85C55AB26C678F89 ON order_internal_remark (order_order_id)');
    }
}
