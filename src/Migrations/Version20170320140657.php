<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170320140657 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_order ADD pickup_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart_order ADD CONSTRAINT FK_AAC3FE90A72D874B FOREIGN KEY (pickup_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_AAC3FE90A72D874B ON cart_order (pickup_address_id)');

        $this->addSql('ALTER TABLE order_order ADD pickup_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E765A72D874B FOREIGN KEY (pickup_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_76B7E765A72D874B ON order_order (pickup_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_order DROP FOREIGN KEY FK_AAC3FE90A72D874B');
        $this->addSql('DROP INDEX IDX_AAC3FE90A72D874B ON cart_order');

        $this->addSql('ALTER TABLE cart_order DROP pickup_address_id');
        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E765A72D874B');
        $this->addSql('DROP INDEX IDX_76B7E765A72D874B ON order_order');
        $this->addSql('ALTER TABLE order_order DROP pickup_address_id');
    }
}
