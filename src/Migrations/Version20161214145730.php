<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214145730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer ADD preferred_paymentmethod_id INT DEFAULT NULL, ADD preferred_ideal_issuer VARCHAR(4) DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E093997DCD5 FOREIGN KEY (preferred_paymentmethod_id) REFERENCES paymentmethod (id)');
        $this->addSql('CREATE INDEX IDX_81398E093997DCD5 ON customer (preferred_paymentmethod_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `customer` DROP FOREIGN KEY FK_81398E093997DCD5');
        $this->addSql('DROP INDEX IDX_81398E093997DCD5 ON `customer`');
        $this->addSql('ALTER TABLE `customer` DROP preferred_paymentmethod_id, DROP preferred_ideal_issuer');
    }
}
