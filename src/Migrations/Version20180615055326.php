<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\CompanyConnectorParameter;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180615055326 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @description Remove all $test connector parameters
     * @param Schema $schema
     */
    public function postDown(Schema $schema): void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $connectorParameters = $em->getRepository()->findBy(['name' => 'test']);

        /** @var CompanyConnectorParameter $parameter */
        foreach ($connectorParameters as $parameter) {
            $em->remove($parameter);
        }
    }
}
