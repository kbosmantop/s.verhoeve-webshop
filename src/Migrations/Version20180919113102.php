<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180919113102 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E7656BF700BD');
        $this->addSql('DROP INDEX IDX_F52993986BF700BD ON `order`');
        $this->addSql('ALTER TABLE `order` CHANGE COLUMN `status_id` `status` VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993986BF700BD');
        $this->addSql('DROP INDEX IDX_4F9798676BF700BD ON order_collection');
        $this->addSql('ALTER TABLE order_collection DROP status_id');
        $this->addSql('DROP TABLE IF EXISTS order_status;');
    }

    /**
     * @param Schema $schema
     * @throws IrreversibleMigrationException
     */
    public function down(Schema $schema)
    {
        throw new IrreversibleMigrationException('Migration is irreversible due to the dropping of order status table in favor of a status column in the order table');
    }
}
