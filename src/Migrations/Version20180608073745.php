<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180608073745 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_usp DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE product_usp ADD id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id), CHANGE product_id product_id INT DEFAULT NULL, CHANGE usp_id usp_id INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_usp MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE product_usp DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE product_usp DROP id, CHANGE product_id product_id INT NOT NULL, CHANGE usp_id usp_id INT NOT NULL');
        $this->addSql('ALTER TABLE product_usp ADD PRIMARY KEY (product_id, usp_id)');
    }
}
