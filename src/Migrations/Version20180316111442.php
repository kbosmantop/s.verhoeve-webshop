<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180316111442 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('RENAME TABLE `order` TO order_collection');
        $this->addSql('RENAME TABLE order_order TO `order`');
        $this->addSql('RENAME TABLE order_order_line TO order_line');

        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_2F3F73B44584665A');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_2F3F73B4727ACA70');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_2F3F73B48D9F6D38');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_2F3F73B4A7B913DD');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_2F3F73B4B5B63A6B');
        $this->addSql('DROP INDEX idx_2f3f73b48d9f6d38 ON order_line');
        $this->addSql('CREATE INDEX IDX_9CE58EE18D9F6D38 ON order_line (order_id)');
        $this->addSql('DROP INDEX idx_2f3f73b4727aca70 ON order_line');
        $this->addSql('CREATE INDEX IDX_9CE58EE1727ACA70 ON order_line (parent_id)');
        $this->addSql('DROP INDEX idx_2f3f73b44584665a ON order_line');
        $this->addSql('CREATE INDEX IDX_9CE58EE14584665A ON order_line (product_id)');
        $this->addSql('DROP INDEX idx_2f3f73b4b5b63a6b ON order_line');
        $this->addSql('CREATE INDEX IDX_9CE58EE1B5B63A6B ON order_line (vat_id)');
        $this->addSql('DROP INDEX idx_2f3f73b4a7b913dd ON order_line');
        $this->addSql('CREATE INDEX IDX_9CE58EE1A7B913DD ON order_line (ledger_id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_2F3F73B44584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_2F3F73B4727ACA70 FOREIGN KEY (parent_id) REFERENCES order_line (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_2F3F73B48D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_2F3F73B4A7B913DD FOREIGN KEY (ledger_id) REFERENCES ledger (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_2F3F73B4B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993984C7291B3');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993986BF700BD');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993988C03F15C');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993989395C3F3');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F5299398979B1AD6');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F52993989A1887DC');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F5299398C6BDFEB');
        $this->addSql('ALTER TABLE order_collection DROP FOREIGN KEY FK_F5299398F6BD1646');
        $this->addSql('DROP INDEX idx_f52993986bf700bd ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F9798676BF700BD ON order_collection (status_id)');
        $this->addSql('DROP INDEX idx_f5299398f6bd1646 ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F979867F6BD1646 ON order_collection (site_id)');
        $this->addSql('DROP INDEX idx_f5299398979b1ad6 ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F979867979B1AD6 ON order_collection (company_id)');
        $this->addSql('DROP INDEX idx_f52993989395c3f3 ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F9798679395C3F3 ON order_collection (customer_id)');
        $this->addSql('DROP INDEX idx_f52993988c03f15c ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F9798678C03F15C ON order_collection (employee_id)');
        $this->addSql('DROP INDEX idx_f5299398c6bdfeb ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F979867C6BDFEB ON order_collection (invoice_address_id)');
        $this->addSql('DROP INDEX idx_f52993984c7291b3 ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F9798674C7291B3 ON order_collection (invoice_address_country)');
        $this->addSql('DROP INDEX idx_f52993989a1887dc ON order_collection');
        $this->addSql('CREATE INDEX IDX_4F9798679A1887DC ON order_collection (subscription_id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F52993984C7291B3 FOREIGN KEY (invoice_address_country) REFERENCES country (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F52993986BF700BD FOREIGN KEY (status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F52993988C03F15C FOREIGN KEY (employee_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F52993989395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F5299398979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F52993989A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F5299398C6BDFEB FOREIGN KEY (invoice_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE order_collection ADD CONSTRAINT FK_F5299398F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E765286F5B26');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E7652ADD6D8C');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E7652F924C2F');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E7656BF700BD');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E765A72D874B');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E765BE501ED3');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E765EBF23851');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_76B7E765FC2A1E2F');
        $this->addSql('DROP INDEX idx_76b7e765fc2a1e2f ON `order`');
        $this->addSql('CREATE INDEX IDX_F5299398FC2A1E2F ON `order` (order_collection_id)');
        $this->addSql('DROP INDEX idx_76b7e7656bf700bd ON `order`');
        $this->addSql('CREATE INDEX IDX_F52993986BF700BD ON `order` (status_id)');
        $this->addSql('DROP INDEX idx_76b7e7652f924c2f ON `order`');
        $this->addSql('CREATE INDEX IDX_F52993982F924C2F ON `order` (delivery_status_id)');
        $this->addSql('DROP INDEX idx_76b7e765286f5b26 ON `order`');
        $this->addSql('CREATE INDEX IDX_F5299398286F5B26 ON `order` (delivery_address_type_id)');
        $this->addSql('DROP INDEX idx_76b7e7652add6d8c ON `order`');
        $this->addSql('CREATE INDEX IDX_F52993982ADD6D8C ON `order` (supplier_id)');
        $this->addSql('DROP INDEX idx_76b7e765ebf23851 ON `order`');
        $this->addSql('CREATE INDEX IDX_F5299398EBF23851 ON `order` (delivery_address_id)');
        $this->addSql('DROP INDEX idx_76b7e765a72d874b ON `order`');
        $this->addSql('CREATE INDEX IDX_F5299398A72D874B ON `order` (pickup_address_id)');
        $this->addSql('DROP INDEX idx_76b7e765be501ed3 ON `order`');
        $this->addSql('CREATE INDEX IDX_F5299398BE501ED3 ON `order` (delivery_address_country)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E765286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E7652ADD6D8C FOREIGN KEY (supplier_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E7652F924C2F FOREIGN KEY (delivery_status_id) REFERENCES order_delivery_status (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E7656BF700BD FOREIGN KEY (status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E765A72D874B FOREIGN KEY (pickup_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E765BE501ED3 FOREIGN KEY (delivery_address_country) REFERENCES country (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E765EBF23851 FOREIGN KEY (delivery_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_76B7E765FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     * @throws IrreversibleMigrationException
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        throw new IrreversibleMigrationException();
    }
}
