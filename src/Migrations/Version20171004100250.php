<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Payment\Paymentmethod;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Version20171004100250 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;


    public function up(Schema $schema)
    {
        // TODO: Implement up() method.
    }

    public function down(Schema $schema)
    {
        // TODO: Implement up() method.
    }

    /**
     * @return Paymentmethod|null
     */
    private function getPaymentMethod()
    {
        return $this->container->get("doctrine")
            ->getRepository(Paymentmethod::class)
            ->findOneBy([
                    'code' => 'internal',
                ]
            );
    }

    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $paymentMethod = $this->getPaymentMethod();

            if (!$paymentMethod) {

                $paymentMethod = new Paymentmethod();
                $paymentMethod->setCode('internal');
                $paymentMethod->translate('nl_NL')->setDescription('Interne verrekening');
                $paymentMethod->mergeNewTranslations();

                $em = $this->container->get("doctrine")->getManager();
                $em->persist($paymentMethod);
                $em->flush();
            }
        }
    }

    public function postDown(Schema $schema)
    {
        $em = $this->container->get("doctrine")->getManager();
        $paymentMethod = $this->getPaymentMethod();
        $em->remove($paymentMethod);
        $em->flush();
    }


}
