<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180302142536 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E7658D9F6D38');
        $this->addSql('DROP INDEX IDX_76B7E7658D9F6D38 ON order_order');
        $this->addSql('DROP INDEX number ON order_order');
        $this->addSql('ALTER TABLE order_order CHANGE order_id order_collection_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E765FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_76B7E765FC2A1E2F ON order_order (order_collection_id)');
        $this->addSql('CREATE UNIQUE INDEX number ON order_order (order_collection_id, number)');

        $this->addSql('ALTER TABLE order_order_line DROP FOREIGN KEY FK_2F3F73B46C678F89');
        $this->addSql('DROP INDEX IDX_2F3F73B46C678F89 ON order_order_line');
        $this->addSql('ALTER TABLE order_order_line CHANGE order_order_id order_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_order_line ADD CONSTRAINT FK_2F3F73B48D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2F3F73B48D9F6D38 ON order_order_line (order_id)');

        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D8D9F6D38');
        $this->addSql('DROP INDEX IDX_6D28840D8D9F6D38 ON payment');
        $this->addSql('ALTER TABLE payment CHANGE order_id order_collection_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840DFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_6D28840DFC2A1E2F ON payment (order_collection_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840DFC2A1E2F');
        $this->addSql('DROP INDEX IDX_6D28840DFC2A1E2F ON payment');
        $this->addSql('ALTER TABLE payment CHANGE order_collection_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_6D28840D8D9F6D38 ON payment (order_id)');

        $this->addSql('ALTER TABLE order_order_line DROP FOREIGN KEY FK_2F3F73B48D9F6D38');
        $this->addSql('DROP INDEX IDX_2F3F73B48D9F6D38 ON order_order_line');
        $this->addSql('ALTER TABLE order_order_line CHANGE order_id order_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_order_line ADD CONSTRAINT FK_2F3F73B46C678F89 FOREIGN KEY (order_order_id) REFERENCES order_order (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2F3F73B46C678F89 ON order_order_line (order_order_id)');

        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E765FC2A1E2F');
        $this->addSql('DROP INDEX IDX_76B7E765FC2A1E2F ON order_order');
        $this->addSql('DROP INDEX number ON order_order');
        $this->addSql('ALTER TABLE order_order CHANGE order_collection_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E7658D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_76B7E7658D9F6D38 ON order_order (order_id)');
        $this->addSql('CREATE UNIQUE INDEX number ON order_order (order_id, number)');
    }
}
