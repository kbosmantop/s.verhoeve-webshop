<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180612073921 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;


    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();

        $key = 'personalization';

        $qb->update('`productgroup_property`');
        $qb->set('`form_type_options`', 'CONCAT_WS(\',\', `form_type_options`, \'read_only\')');
        $qb->where('`key` = :key');
        $qb->andWhere('`form_type_options` NOT LIKE (:formTypeOptions)');

        $qb->setParameter('key', $key);
        $qb->setParameter('formTypeOptions', '%read_only%');

        if ($qb->execute()) {
            $this->write('<info>' . sprintf("Set `productgroup_property` option for '%s' to 'read_only'.", $key) . '</info>');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
