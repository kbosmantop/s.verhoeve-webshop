<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171115135911 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE productgroup_property_set (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, productgroup_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE productgroup_property_set_productgroup_property (productgroup_property_set_id INT NOT NULL, productgroup_property_id INT NOT NULL, INDEX IDX_8448E4597386C20B (productgroup_property_set_id), INDEX IDX_8448E45956671D65 (productgroup_property_id), PRIMARY KEY(productgroup_property_set_id, productgroup_property_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE productgroup_property_set_productgroup_property ADD CONSTRAINT FK_8448E4597386C20B FOREIGN KEY (productgroup_property_set_id) REFERENCES productgroup_property_set (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE productgroup_property_set_productgroup_property ADD CONSTRAINT FK_8448E45956671D65 FOREIGN KEY (productgroup_property_id) REFERENCES productgroup_property (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE productgroup_property_set ADD CONSTRAINT FK_B04EBF855BC5238A FOREIGN KEY (productgroup_id) REFERENCES productgroup (id)');

        $this->addSql('ALTER TABLE product ADD property_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6E8C6264 FOREIGN KEY (property_set_id) REFERENCES productgroup_property_set (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD6E8C6264 ON product (property_set_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6E8C6264');
        $this->addSql('DROP INDEX IDX_D34A04AD6E8C6264 ON product');
        $this->addSql('ALTER TABLE product DROP property_set_id');

        $this->addSql('DROP INDEX IDX_B04EBF855BC5238A ON productgroup_property_set');
        $this->addSql('DROP TABLE productgroup_property_set_productgroup_property');
        $this->addSql('DROP TABLE productgroup_property_set');
    }
}
