<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171106110926 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE assortment_company (assortment_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_623202B9D7A15862 (assortment_id), INDEX IDX_623202B9979B1AD6 (company_id), PRIMARY KEY(assortment_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assortment_company ADD CONSTRAINT FK_623202B9D7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_company ADD CONSTRAINT FK_623202B9979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD CONSTRAINT FK_2A5E6D8B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_2A5E6D8B7E3C61F9 ON assortment (owner_id)');
        $this->addSql('ALTER TABLE assortment ADD card_assortment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD CONSTRAINT FK_2A5E6D8BA7DB9143 FOREIGN KEY (card_assortment_id) REFERENCES assortment (id)');
        $this->addSql('CREATE INDEX IDX_2A5E6D8BA7DB9143 ON assortment (card_assortment_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment DROP FOREIGN KEY FK_2A5E6D8BA7DB9143');
        $this->addSql('DROP INDEX IDX_2A5E6D8BA7DB9143 ON assortment');
        $this->addSql('ALTER TABLE assortment DROP card_assortment_id');
        $this->addSql('DROP TABLE assortment_company');
        $this->addSql('ALTER TABLE assortment DROP FOREIGN KEY FK_2A5E6D8B7E3C61F9');
        $this->addSql('DROP INDEX IDX_2A5E6D8B7E3C61F9 ON assortment');
        $this->addSql('ALTER TABLE assortment DROP owner_id');
    }
}
