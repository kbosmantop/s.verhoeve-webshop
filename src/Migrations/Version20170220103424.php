<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170220103424 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE twinfield_export (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_BF329B03B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE twinfield_export_company (export_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_8392F47464CDAF82 (export_id), INDEX IDX_8392F474979B1AD6 (company_id), PRIMARY KEY(export_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE twinfield_export_order (export_id INT NOT NULL, order_order_id INT NOT NULL, INDEX IDX_CB7392AC64CDAF82 (export_id), INDEX IDX_CB7392AC6C678F89 (order_order_id), PRIMARY KEY(export_id, order_order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE twinfield_export ADD CONSTRAINT FK_BF329B03B03A8386 FOREIGN KEY (created_by_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE twinfield_export_company ADD CONSTRAINT FK_8392F47464CDAF82 FOREIGN KEY (export_id) REFERENCES twinfield_export (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE twinfield_export_company ADD CONSTRAINT FK_8392F474979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE twinfield_export_order ADD CONSTRAINT FK_CB7392AC64CDAF82 FOREIGN KEY (export_id) REFERENCES twinfield_export (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE twinfield_export_order ADD CONSTRAINT FK_CB7392AC6C678F89 FOREIGN KEY (order_order_id) REFERENCES order_order (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE twinfield_export_company DROP FOREIGN KEY FK_8392F47464CDAF82');
        $this->addSql('ALTER TABLE twinfield_export_order DROP FOREIGN KEY FK_CB7392AC64CDAF82');
        $this->addSql('DROP TABLE twinfield_export');
        $this->addSql('DROP TABLE twinfield_export_company');
        $this->addSql('DROP TABLE twinfield_export_order');
    }
}
