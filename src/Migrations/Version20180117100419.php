<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180117100419 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_preferred_supplier (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, delivery_area_id INT NOT NULL, position INT NOT NULL, INDEX IDX_3ED6B7AE979B1AD6 (company_id), INDEX IDX_3ED6B7AEB711FF42 (delivery_area_id), UNIQUE INDEX UNIQ_3ED6B7AE979B1AD6B711FF42462CE4F5 (company_id, delivery_area_id, position), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_preferred_supplier ADD CONSTRAINT FK_3ED6B7AE979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company_preferred_supplier ADD CONSTRAINT FK_3ED6B7AEB711FF42 FOREIGN KEY (delivery_area_id) REFERENCES supplier_delivery_area (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_preferred_supplier');
    }
}
