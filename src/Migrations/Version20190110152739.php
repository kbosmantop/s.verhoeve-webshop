<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190110152739 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_establishment ADD verified_on DATETIME DEFAULT NULL');
        $this->addSql('
            INSERT INTO company_establishment (address_id, company_id, name, establishment_number, created_at, updated_at, deleted_at, verified_on)

            SELECT 			core.main_address_id
                        ,	comp.id
                        ,	core.name
                        ,	core.establishment_number
                        ,	NOW()
                        ,	NOW()
                        ,	NULL
                        ,	core.created_at
            
            FROM 		company AS comp
            INNER JOIN	company_registration AS core ON core.id = comp.company_registration_id
            LEFT JOIN	company_establishment AS coes ON coes.company_id = comp.id AND coes.establishment_number = comp.chamber_of_commerce_establishment_number
            
            WHERE 		coes.id IS NULL

        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_establishment DROP verified_on');
    }
}
