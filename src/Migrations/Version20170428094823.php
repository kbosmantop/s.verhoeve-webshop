<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170428094823 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE postcode_geometry (id INT AUTO_INCREMENT NOT NULL, point POINT DEFAULT NULL COMMENT \'(DC2Type:point)\', multipolygon MULTIPOLYGON DEFAULT NULL COMMENT \'(DC2Type:multipolygon)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE postcode ADD geometry_id INT DEFAULT NULL, DROP point, DROP multipolygon');
        $this->addSql('ALTER TABLE postcode ADD CONSTRAINT FK_6339A411A88A6A72 FOREIGN KEY (geometry_id) REFERENCES postcode_geometry (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6339A411A88A6A72 ON postcode (geometry_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE postcode DROP FOREIGN KEY FK_6339A411A88A6A72');
        $this->addSql('DROP TABLE postcode_geometry');
        $this->addSql('DROP INDEX UNIQ_6339A411A88A6A72 ON postcode');
        $this->addSql('ALTER TABLE postcode ADD point POINT DEFAULT NULL COMMENT \'(DC2Type:point)\', ADD multipolygon MULTIPOLYGON DEFAULT NULL COMMENT \'(DC2Type:multipolygon)\', DROP geometry_id');
    }
}
