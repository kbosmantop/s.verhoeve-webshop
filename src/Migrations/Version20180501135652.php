<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180501135652 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_packaging_unit DROP FOREIGN KEY FK_D7A6A34D101900D0');
        $this->addSql('ALTER TABLE product_packaging_unit CHANGE packaging_unit_id packaging_unit_id VARCHAR(3) DEFAULT NULL');

        $this->addSql('ALTER TABLE packaging_unit DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE packaging_unit CHANGE iso_code code VARCHAR(3) NOT NULL');
        $this->addSql('ALTER TABLE packaging_unit ADD PRIMARY KEY (code)');

        $this->addSql('ALTER TABLE product_packaging_unit ADD CONSTRAINT FK_D7A6A34D101900D0 FOREIGN KEY (packaging_unit_id) REFERENCES packaging_unit (code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_packaging_unit DROP FOREIGN KEY FK_D7A6A34D101900D0');
        $this->addSql('ALTER TABLE product_packaging_unit CHANGE packaging_unit_id packaging_unit_id VARCHAR(2) DEFAULT NULL');

        $this->addSql('ALTER TABLE packaging_unit DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE packaging_unit CHANGE code iso_code VARCHAR(2) NOT NULL');
        $this->addSql('ALTER TABLE packaging_unit ADD PRIMARY KEY (iso_code)');

        $this->addSql('ALTER TABLE product_packaging_unit ADD CONSTRAINT FK_D7A6A34D101900D0 FOREIGN KEY (packaging_unit_id) REFERENCES packaging_unit (iso_code)');
    }
}
