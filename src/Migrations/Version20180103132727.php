<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180103132727 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fraud_detection_report (id INT AUTO_INCREMENT NOT NULL, order_collection_id INT DEFAULT NULL, fraud_detection_rule_id INT DEFAULT NULL, INDEX IDX_9E097ACCFC2A1E2F (order_collection_id), INDEX IDX_9E097ACCC13ECFE (fraud_detection_rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fraud_detection_rule (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, score INT NOT NULL, publish TINYINT(1) DEFAULT NULL, publish_start DATETIME DEFAULT NULL, publish_end DATETIME DEFAULT NULL, INDEX IDX_4DECED3B744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCC13ECFE FOREIGN KEY (fraud_detection_rule_id) REFERENCES fraud_detection_rule (id)');
        $this->addSql('ALTER TABLE fraud_detection_rule ADD CONSTRAINT FK_4DECED3B744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fraud_detection_report DROP FOREIGN KEY FK_9E097ACCC13ECFE');
        $this->addSql('DROP TABLE fraud_detection_report');
        $this->addSql('DROP TABLE fraud_detection_rule');
    }
}
