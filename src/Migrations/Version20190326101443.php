<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190326101443 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {

        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();
        $conn->beginTransaction();

        /** @var QueryBuilder $qb */
        $qb = $conn->createQueryBuilder();

        $values = [
            '`key`'     => $qb->createNamedParameter('wics_document_type'),
            'form_type' => $qb->createNamedParameter(TextType::class),
            'multiple'  => $qb->createNamedParameter(0),
        ];

        $qb->insert('parameter')
            ->values($values)
            ->execute();

        $entities = [
            Letter::class,
            ProductCard::class,
            Personalization::class,
            Productgroup::class
        ];

        foreach($entities as $entity) {
            $parameterEntityValues = [
                'parameter_id' => $qb->createNamedParameter('wics_document_type'),
                'required' => $qb->createNamedParameter(0),
                'label' => $qb->createNamedParameter('WICS document type'),
                'entity' => $qb->createNamedParameter($entity),
            ];

            $qb->insert('parameter_entity')
                ->values($parameterEntityValues)
                ->execute();
        }

        $conn->commit();
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('wics_document_type');
    }
}
