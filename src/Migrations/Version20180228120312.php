<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180228120312 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE auto_order_job ADD supplier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE auto_order_job ADD CONSTRAINT FK_954766892ADD6D8C FOREIGN KEY (supplier_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_954766892ADD6D8C ON auto_order_job (supplier_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE auto_order_job DROP FOREIGN KEY FK_954766892ADD6D8C');
        $this->addSql('DROP INDEX IDX_954766892ADD6D8C ON auto_order_job');
        $this->addSql('ALTER TABLE auto_order_job DROP supplier_id');
    }
}
