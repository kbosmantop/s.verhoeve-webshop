<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627120248 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplier_product_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, description LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_6ED153B42C2AC5D3 (translatable_id), UNIQUE INDEX supplier_product_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier_group_product_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, description LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_BB602B9C2C2AC5D3 (translatable_id), UNIQUE INDEX supplier_group_product_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_product_translation ADD CONSTRAINT FK_6ED153B42C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES supplier_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_group_product_translation ADD CONSTRAINT FK_BB602B9C2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES supplier_group_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_group_product ADD forward_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD forward_price DOUBLE PRECISION DEFAULT NULL');
    }

    public function postUp(Schema $schema)
    {
        return;
        $this->connection->beginTransaction();

        $supplierProducts = $this->container->get("doctrine")->getRepository(SupplierProduct::class)->findAll();

        foreach ($supplierProducts as $supplierProduct) {
            $this->updateSupplierProduct($supplierProduct);
        }

        $supplierProducts = $this->container->get("doctrine")->getRepository(SupplierGroupProduct::class)->findAll();

        foreach ($supplierProducts as $supplierProduct) {
            $this->updateSupplierProduct($supplierProduct);
        }

        $this->connection->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplier_product_translation');
        $this->addSql('DROP TABLE supplier_group_product_translation');
        $this->addSql('ALTER TABLE supplier_group_product DROP forward_price');
        $this->addSql('ALTER TABLE supplier_product DROP forward_price');
    }

    /**
     * @param $entity SupplierProduct|SupplierGroupProduct
     */
    protected function updateSupplierProduct($entity)
    {
        $product = null;

        if ($entity->getProductVariations()->count() == 0) {
            $product = $entity->getProduct();
        } elseif ($entity->getProductVariations()->count() == 1) {
            $product = $entity->getProductVariations()->current();
        }

        if (!$product) {
            return;
        }

        if (!$entity->translate("nl_NL")->getTitle()) {
            $entity->translate("nl_NL")->setTitle($product->translate()->getTitle());
        }

        if (!$entity->translate("nl_NL")->getDescription()) {
            $mainProduct = $product;

            if ($mainProduct->getParent()) {
                $mainProduct = $mainProduct->getParent();
            }

            $entity->translate("nl_NL")->setDescription($mainProduct->translate()->getShortDescription());
        }

        $entity->mergeNewTranslations();

        $this->container->get("doctrine")->getManager()->flush();
    }
}
