<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180716081913
 * @package Application\Migrations
 */
class Version20180716081913 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment ADD image LONGTEXT');
        $this->addSql('ALTER TABLE assortment_translation ADD homepage_slogan VARCHAR(120) DEFAULT NULL');
        $this->addSql('CREATE TABLE site_homepage_assortment (id INT AUTO_INCREMENT NOT NULL, site_id INT DEFAULT NULL, assortment_id INT DEFAULT NULL, position INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_97C04805F6BD1646 (site_id), INDEX IDX_97C04805D7A15862 (assortment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE site_homepage_assortment ADD CONSTRAINT FK_97C04805F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site_homepage_assortment ADD CONSTRAINT FK_97C04805D7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment DROP image');
        $this->addSql('ALTER TABLE assortment_translation DROP homepage_slogan');
        $this->addSql('DROP TABLE site_homepage_assortment');
    }
}
