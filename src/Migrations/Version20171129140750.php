<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Site\Site;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171129140750 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE site CHANGE theme theme ENUM('topgeschenken','topbloemen','topfruit','toptaarten', 'bloemisten', 'suppliers') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '(DC2Type:ThemeType)'");
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        $em = $this->container->get('doctrine')->getManager();
        $site = $em->getRepository(Site::class)->findOneBy(['theme' => 'bloemisten']);
        if ($site) {
            $site->setTheme('suppliers');
            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE site CHANGE theme theme ENUM('topgeschenken','topbloemen','topfruit','toptaarten','bloemisten') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '(DC2Type:ThemeType)'");
    }

}
