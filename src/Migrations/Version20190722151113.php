<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190722151113 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     *
     * @throws ConnectionException
     * @throws DBALException
     */
    public function preDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('use_default_address_as_delivery_address');
        $parameterService->deleteByKey('site_other_settings');
    }

    /**
     * @description Sets the parameters for WICS for use in the WICS service
     *
     * @param Schema $schema
     *
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'site_other_settings',
            'entity' => Site::class,
            'label' => 'Overige instellingen',
            'value' => null,
            'form_type' => ColumnType::class,
        ]);

        $parameterService->createViaArray([
            'key' => 'use_default_address_as_delivery_address',
            'entity' => Site::class,
            'label' => 'Bezorgadres vullen met gegevens besteller',
            'value' => null,
            'parent' => 'site_other_settings',
            'form_type' => YesNoType::class,
            'form_type_class' => Site::class,
            'required' => true,
            'default_value' => 0,
        ]);
    }
}
