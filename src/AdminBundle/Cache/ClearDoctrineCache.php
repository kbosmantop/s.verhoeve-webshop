<?php

namespace AdminBundle\Cache;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;
use Symfony\Component\Process\Process;

/**
 * Class ClearDoctrineCache
 * @package App\AdminBundle\Cache
 */
class ClearDoctrineCache implements CacheClearerInterface
{
    use ContainerAwareTrait;

    /**
     * @param string $cacheDirectory
     * @throws \Exception
     */
    public function clear($cacheDirectory): void
    {
        $input = new ArgvInput();
        $output = new ConsoleOutput();
        $io = new SymfonyStyle($input, $output);

        $io->section(' -> Clears all metadata cache');
        $process = new Process('php bin/console doctrine:cache:clear-metadata');
        $process->setTimeout(null);
        $process->start();

        while ($process->isRunning()) {
            $process->wait(function ($type, $buffer) use ($io) {
                if (Process::ERR === $type) {
                    $io->error('Error: ' . $buffer);
                } else {
                    $io->text($buffer);
                }
            });
        }

        $io->section(' -> Clears all query cache');
        $process = new Process('php bin/console doctrine:cache:clear-query');
        $process->setTimeout(null);
        $process->start();

        while ($process->isRunning()) {
            $process->wait(function ($type, $buffer) use ($io) {
                if (Process::ERR === $type) {
                    $io->error('Error: ' . $buffer);
                } else {
                    $io->text($buffer);
                }
            });
        }
    }
}
