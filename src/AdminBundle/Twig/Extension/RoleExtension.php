<?php

namespace AdminBundle\Twig\Extension;

use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class RoleExtension
 * @package AdminBundle\Twig\Extension
 */
class RoleExtension extends \Twig_Extension
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * RoleExtension constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Get al filters
     *
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('user_has_role', [$this, 'hasRole']),
        ];
    }

    /**
     * @param null $role
     *
     * @return bool
     */
    public function hasRole($role = null): bool
    {
        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        return $this->getUser()->hasRole($role);
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            throw new \RuntimeException('No token received');
        }

        $user = $token->getUser();

        if($user instanceof User) {
            return $user;
        }

        throw new \RuntimeException('User not found');
    }
}
