<?php

namespace AdminBundle\Twig\Extension;

class IconExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    protected $iconSet;

    /**
     * @var string
     */
    protected $shortcut;

    /**
     * @var \Twig_Template
     */
    protected $iconTemplate;

    /**
     * Constructor.
     *
     * @param string $iconSet
     * @param string $shortcut
     */
    public function __construct($iconSet, $shortcut = null)
    {
        $this->iconSet = $iconSet;
        $this->shortcut = $shortcut;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('admin_icon', [$this, 'renderIcon'], $options),
        ];

        if ($this->shortcut) {
            $functions[] = new \Twig_SimpleFunction($this->shortcut, [$this, 'renderIcon'], $options);
        }

        return $functions;
    }

    /**
     * Renders the icon.
     *
     * @param \Twig_Environment $env
     * @param string            $icon
     * @param boolean           $inverted
     * @param array             $attr
     * @param string            $iconSet
     * @return string
     */
    public function renderIcon(\Twig_Environment $env, $icon, $inverted = false, $attr = [], $iconSet = null)
    {
        $useIconSet = $this->iconSet;

        if ($iconSet !== null) {
            $useIconSet = $iconSet;
        }

        $template = $this->getIconTemplate($env);
        $context = [
            'icon' => $icon,
            'inverted' => $inverted,
            'attr' => $attr,
        ];

        return $template->renderBlock($useIconSet, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_icon';
    }

    /**
     * @param \Twig_Environment $env
     * @return \Twig_Template
     */
    protected function getIconTemplate(\Twig_Environment $env)
    {
        if ($this->iconTemplate === null) {
            $this->iconTemplate = $env->loadTemplate('@Admin/Base/icons.html.twig');
        }

        return $this->iconTemplate;
    }
}
