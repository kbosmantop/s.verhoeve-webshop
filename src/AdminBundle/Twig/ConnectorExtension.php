<?php

namespace AdminBundle\Twig;

use AppBundle\Entity\Relation\Company;
use AppBundle\Interfaces\ConnectorInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Services\ConnectorHelper;

class ConnectorExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('connector', [$this, 'connector']),
        ];
    }

    /**
     * @param Company $supplier
     * @return null|ConnectorInterface
     */
    public function connector(Company $supplier)
    {
        return $this->container->get(ConnectorHelper::class)->getConnector($supplier);
    }
}
