<?php

namespace AdminBundle\Components\Crud;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CrudActionResponse extends JsonResponse
{
    public function __construct($data = [], $status = 200, array $headers = [], $json = false)
    {
        $data = $this->resolveDataOptions($data);

        parent::__construct($data, $status, $headers, $json);
    }

    private function resolveDataOptions($options)
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setRequired([
            'message',
        ]);

        $optionsResolver->setAllowedTypes('message', 'array');

        $options = $optionsResolver->resolve($options);

        $options['message'] = $this->resolveMessageOptions($options['message']);

        return $options;
    }

    private function resolveMessageOptions($options)
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setRequired([
            'type',
            'text',
            'message_type',
        ]);

        $optionsResolver->setDefined([
            'title',
            'disable_timer',
            'callback',
            'redirect',
        ]);

        $optionsResolver->setDefaults([
            'message_type' => 'flash',
            'callback' => null,
        ]);

        $optionsResolver->setAllowedTypes('message_type', 'string');
        $optionsResolver->setAllowedTypes('type', 'string');
        $optionsResolver->setAllowedTypes('title', ['null', 'string']);
        $optionsResolver->setAllowedTypes('text', 'string');
        $optionsResolver->setAllowedTypes('disable_timer', ['null', 'boolean']);
        $optionsResolver->setAllowedTypes('callback', ['null', 'string']);

        $optionsResolver->setAllowedValues('message_type', ['flash', 'modal']);
        $optionsResolver->setAllowedValues('type', ['info', 'question', 'error', 'success', 'warning']);

        $optionsResolver->setNormalizer('type', function (Options $options, $value) {
            void($options);

            return strtolower($value);
        });

        $optionsResolver->setNormalizer('message_type', function (Options $options, $value) {
            $value = strtolower($value);

            if ($value == 'modal') {
                if (empty($options['title'])) {
                    throw new MissingOptionsException('
                    The required option "title" is missing for option "message", it is required when the option "message_type" is set to "modal"
                    ');
                }
            }

            return $value;
        });

        return $optionsResolver->resolve($options);
    }
}
