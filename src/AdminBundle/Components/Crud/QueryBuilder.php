<?php

namespace AdminBundle\Components\Crud;

use AdminBundle\Components\Datatable\AbstractColumn;
use AdminBundle\Components\Datatable\Datatable;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata as PersistenceClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder as OrmQueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QueryBuilder
 * @package AdminBundle\Components\Crud
 */
class QueryBuilder
{
    use ContainerAwareTrait;

    /**
     * @var OrmQueryBuilder
     */
    private $qb;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \object
     */
    private $entity;

    /**
     * @var Datatable
     */
    private $dataTable;

    /**
     * @var string
     */
    private $entityClass;

    /**
     * @var ClassMetadata
     */
    private $entityMetadata;

    /**
     * @var array
     */
    private $joinedRelationAliases;

    /** @var bool */
    private $gotSearchField = false;

    /** @var null|string $alias */
    private $alias = null;

    /**
     * @param Request     $request
     * @param string      $entity
     * @param Datatable   $dataTable
     * @param null|string $alias
     */
    public function initialize(Request $request, string $entity, Datatable $dataTable, $alias = null): void
    {
        $this->request = $request;
        $this->entity = new $entity;
        $this->dataTable = $dataTable;
        $this->alias = $alias;

        $this->entityClass = \get_class($this->entity);
        $this->entityMetadata = $this->getMetadata($this->entityClass);

        $this->qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $this->build();
    }

    public function build(): void
    {
        $this->select();
        $this->joins();

        $this->order();
        $this->search();
    }

    private function select(): void
    {
        $alias = $this->alias ?? $this->entityMetadata->getTableName();

        $this->qb
            ->select($alias)
            ->from($this->entityClass, $alias);

        if ($this->entityMetadata->hasAssociation('translations')) {
            $this->qb->addSelect($this->entityMetadata->getTableName() . '_translations')->join($alias . '.translations',
                $this->entityMetadata->getTableName() . '_translations');
        }

        if ($this->entityMetadata->hasAssociation('parent')) {
            $this->qb->leftJoin($alias . '.parent', $alias . '_parent');
        }
    }

    private function joins(): void
    {
        foreach ($this->dataTable as $column) {
            if (null === $column->getOption('proxy')) {
                $this->join($column);
            }
        }

        foreach ($this->dataTable as $column) {
            if (null !== $column->getOption('proxy')) {
                $this->join($column);
            }
        }
    }

    /**
     * @param AbstractColumn $column
     */
    private function join(AbstractColumn $column): void
    {
        $metadata = $this->entityMetadata;

        $rootAlias = $this->alias ?? $this->entityMetadata->getTableName();

        if (null !== $column->getOption('proxy')) {
            $metadata = $this->getMetadata($metadata->getAssociationTargetClass($column->getOption('proxy')));
        }

        $columnName = $column->getName();

        if ($relationAlias = strstr($columnName, '.', true)) {
            if (isset($this->joinedRelationAliases[$relationAlias])) {
                return;
            }

            $this->joinedRelationAliases[$relationAlias] = 1;

            /** @var ClassMetadata $relationMetadata */
            $relationMetadata = $this->getMetadata($metadata->getAssociationTargetClass($relationAlias));

            $this->qb->addSelect($relationAlias)->leftJoin($rootAlias . '.' . $relationAlias,
                $relationAlias);

            if ($relationMetadata->hasAssociation('translations')) {
                $this->qb->addSelect($relationAlias . '_translations')->leftJoin($relationAlias . '.translations',
                    $relationAlias . '_translations');
            }

        } elseif ($metadata->hasAssociation($columnName)) {
            /** @var ClassMetadata $relationMetadata */
            $relationMetadata = $this->getMetadata($metadata->getAssociationTargetClass($columnName));

            $relationAlias = $relationMetadata->getTableName();

            $this->qb->addSelect($relationAlias)->leftJoin($rootAlias . '.' . $columnName,
                $relationAlias);

            if ($relationMetadata->hasAssociation('translations')) {
                $this->qb->addSelect($relationAlias . '_translations')->leftJoin($relationAlias . '.translations',
                    $relationAlias . '_translations');
            }
        }
    }

    private function order(): void
    {
        if (null === $this->request->get('order') || !\is_array($this->request->get('order'))) {
            return;
        }

        $orderColumnFallback = [
            'name',
            'description',
        ];

        foreach ($this->request->get('order') as $order) {
            $column = $this->dataTable[$order['column']];

            $columnName = $column->getName();
            if (strpos($columnName, '.') !== false) {
                [$entity, $field] = explode('.', $columnName);
                $this->qb->addOrderBy($entity . '.' . $field, strtoupper($order['dir']));
            } elseif ($this->entityMetadata->hasField($columnName)) {
                $alias = $this->alias ?? $this->entityMetadata->getTableName();

                if ($this->entityMetadata->hasAssociation('parent')) {
                    $this->qb->addSelect('(COALESCE(' . $alias . '_parent.' . $columnName . ', ' . $alias . '.' . $columnName . ')) AS HIDDEN tree_order');
                    $this->qb->addOrderBy('tree_order', strtoupper($order['dir']));

                    $this->qb->addOrderBy($alias . '.parent', 'ASC');
                }

                $this->qb->addOrderBy($alias . '.' . $columnName, strtoupper($order['dir']));
            } elseif ($this->entityMetadata->hasAssociation($columnName)) {
                /** @var ClassMetadata $relationMetadata */
                $relationMetadata = $this->getMetadata($this->entityMetadata->getAssociationTargetClass($columnName));
                $relationAlias = $relationMetadata->getTableName();

                if ($relationMetadata->hasField($columnName)) {
                    $this->qb->addOrderBy($relationAlias . '.' . $columnName, strtoupper($order['dir']));
                } else {
                    foreach ($orderColumnFallback as $orderColumn) {
                        if ($relationMetadata->hasField($orderColumn)) {
                            $this->qb->addOrderBy($relationAlias . '.' . $orderColumn, strtoupper($order['dir']));
                            break;
                        }
                    }
                }
            }

            if ($this->entityMetadata->hasAssociation('translations')) {
                $translationsMetadata = $this->getMetadata($this->entityMetadata->getAssociationTargetClass('translations'));

                if ($translationsMetadata->hasField($columnName)) {
                    $this->qb->addOrderBy($this->entityMetadata->getTableName() . '_translations.' . $columnName,
                        strtoupper($order['dir']));
                }
            }
        }
    }

    private function search(): void
    {
        if (!$this->request->get('search') ||
            !\is_array($this->request->get('search')) ||
            empty($this->request->get('search')['value']) ||
            \strlen($this->request->get('search')['value']) < 3 ||
            \in_array($this->request->get('search')['value'],
                $this->container->getParameter('search_ignored_search_strings'), true)) {
            return;
        }

        $alias = $this->alias ?? $this->entityMetadata->getTableName();

        $criteria = [];

        foreach ($this->dataTable as $column) {
            if (!$column->getOptions()['searchable']) {
                continue;
            }

            $columnName = $column->getName();

            if (strpos($columnName, '.') !== false) {
                [$entity, $field] = explode('.', $columnName);
                $criteria[] = $this->qb->expr()->like($entity . '.' . $field, ':search');
            } elseif ($this->entityMetadata->hasField($columnName)) {
                $criteria[] = $this->qb->expr()->like($alias . '.' . $columnName, ':search');
            } elseif ($this->entityMetadata->hasAssociation($columnName)) {
                /** @var ClassMetadata $relationMetadata */
                $relationMetadata = $this->getMetadata($this->entityMetadata->getAssociationTargetClass($columnName));
                $relationAlias = $relationMetadata->getTableName();

                foreach ($relationMetadata->fieldMappings as $fieldMapping) {
                    if ($fieldMapping['type'] === 'string') {
                        $criteria[] = $this->qb->expr()->like($relationAlias . '.' . $fieldMapping['fieldName'],
                            ':search');
                    }
                }
            }

            if ($this->entityMetadata->hasAssociation('translations') && $this->getMetadata($this->entityMetadata->getAssociationTargetClass('translations'))->hasField($columnName)) {
                $criteria[] = $this->qb->expr()->like($alias . '_translations.' . $columnName,
                    ':search');
            }
        }

        if ($this->entityMetadata->hasAssociation('translations')) {
            $this->qb->andWhere($alias . "_translations.locale = 'nl_NL'");
        }

        if (!empty($criteria)) {

            $this->gotSearchField = true;

            $this->qb->andWhere(
                \call_user_func_array([$this->qb->expr(), 'orX'], $criteria)
            );
        }
    }

    /**
     * @param $className
     * @return PersistenceClassMetadata
     */
    private function getMetadata($className): PersistenceClassMetadata
    {
        $cmf = $this->getDoctrine()->getManager()->getMetadataFactory();

        return $cmf->getMetadataFor($className);
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        $query = $this->qb->getQuery();

        $this->bindParameters($query);

        return $query;
    }

    /**
     * @return OrmQueryBuilder
     */
    public function getQueryBuilder(): OrmQueryBuilder
    {
        return $this->qb;
    }

    /**
     * @return null|string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param Query $query
     */
    private function bindParameters(Query $query): void
    {
        if ($this->gotSearchField &&
            $this->request->get('search') &&
            \is_array($this->request->get('search')) &&
            !empty($this->request->get('search')['value']) &&
            ($searchString = $this->request->get('search')['value']) &&
            !\in_array($searchString, $this->container->getParameter('search_ignored_search_strings'), true) &&
            \strlen($this->request->get('search')['value']) >= 3) {
            $query->setParameter('search', '%' . $searchString . '%');
        }
    }

    /**
     * @return Registry|\object
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

}
