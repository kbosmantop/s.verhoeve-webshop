<?php

namespace AdminBundle\Components\Datatable\Column;

use AppBundle\Entity\Security\Employee\User;

class UserColumn extends TextColumn
{
    public function value($entity)
    {
        /** @var User $user */
        $user = parent::value($entity);

        $displayName = $user->getDisplayName();

        if (!$displayName) {
            return '<i>' . $user->getUsername() . '</i>';
        }

        return $displayName;
    }
}