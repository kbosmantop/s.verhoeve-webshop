<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Knp\Bundle\TimeBundle\DateTimeFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class TimeAgoColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        global $kernel;

        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            $value = $accessor->getValue($entity, $this->name);

            if (!$value) {
                return $this->options['empty_data'];
            }

            $translator = $kernel->getContainer()->get("translator");

            $formatter = new DateTimeFormatter($translator);

            return $translator->trans($formatter->formatDiff($value, new \Datetime()));
        }

        return false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => false,
                "searchable" => false,
            ]
        );

        return $resolver;
    }
}
