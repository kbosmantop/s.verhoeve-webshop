<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use AppBundle\Traits\NonDeletableEntity;
use AppBundle\Traits\NonEditableEntity;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class ActionColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class ActionColumn extends AbstractColumn
{
    private $entity;

    /**
     * @param $entity
     * @return array
     */
    public function value($entity)
    {
        $this->entity = $entity;

        return $this->buttons($this->getOptions()['buttons']);
    }

    /**
     * @param $buttons
     * @return array
     */
    private function buttons($buttons)
    {
        $qb = [];

        $entityTraits = class_uses($this->entity);
        foreach ($buttons as $button) {
            if (property_exists($button, 'type')) {
                if ($button->type === 'edit' && (\in_array(NonEditableEntity::class, $entityTraits,
                            true) && !$this->entity->isEditable())) {
                    continue;
                }

                if ($button->type === 'delete' && (\in_array(NonDeletableEntity::class, $entityTraits,
                            true) && !$this->entity->isDeletable())) {
                    continue;
                }
            }

            $qb[] = $this->createButton($button);
        }

        return $qb;
    }

    /**
     * @param $properties
     * @return \stdClass
     */
    private function createButton($properties)
    {
        $button = new \stdClass();
        $accessor = PropertyAccess::createPropertyAccessor();
        $idField = 'id';

        if (isset($properties->identifier)) {
            if (\is_array($properties->identifier)) {
                $idField = $properties->identifier['field'];
                $button->id = $accessor->getValue($this->entity, $properties->identifier['id'] ?? 'id');
            } else {
                $button->id = $accessor->getValue($this->entity, $properties->identifier);
            }
        } else {
            $button->id = $accessor->getValue($this->entity, 'id');
        }

        foreach ($properties as $key => $value) {
            switch ($key) {
                case 'buttons':
                    $button->buttons = $this->buttons($value);
                    break;

                case 'url':
                    $button->url = $value($this->entity);
                    break;

                case 'route':
                    $button->url = $properties->router->generate($value,
                        $properties->route_parameters ?? [$idField => $button->id]);
                    break;

                default:
                    $button->{$key} = $value;
                    break;
            }
        }

        return $button;
    }

    /**
     * @param OptionsResolver $resolver
     * @return mixed|OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(
            [
                'buttons',
            ]
        );

        return $resolver;
    }
}
