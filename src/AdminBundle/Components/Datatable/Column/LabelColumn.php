<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class LabelColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class LabelColumn extends TextColumn
{
    /**
     * @param $entity
     * @return mixed|null
     * @throws \Exception
     */
    public function value($entity)
    {
        $class = $this->getOption('class');
        $attrs = $this->getOption('attr');
        $callback = $this->getOption('callback');
        $propertyAccessor = new PropertyAccessor();

        if($callback !== null){
            $callback = $callback(parent::value($entity), $entity);
        }

        $html = '<span class="' . $class . '" ';
        foreach($attrs as $attr => $value){
            try {
                $value = $propertyAccessor->getValue($entity, $value);
            } catch (NoSuchPropertyException $e){
                continue;
            }

            $html .= $attr . '="' . $value . '" ';
        }
        $html .= '>' . $callback . '</span>';
        return $html;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver|mixed
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'class',
            'attr',
            'callback',
        ]);

        $resolver->setAllowedTypes('class', ['string']);
        $resolver->setAllowedTypes('attr', ['array']);
        $resolver->setAllowedTypes('callback', ['closure', 'null']);

        $resolver->setDefaults([
            'class' => '',
            'attr' => [],
            'callback' => null
        ]);

        return $resolver;
    }
}