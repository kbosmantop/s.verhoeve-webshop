<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Recurr;
use Recurr\Transformer\TextTransformer;
use Recurr\Transformer\Translator;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RruleColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            $rule = new Recurr\Rule($accessor->getValue($entity, $this->name));

            $textTransformer = new TextTransformer(new Translator('nl'));
            return ucfirst($textTransformer->transform($rule));
        }

        return false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => false,
                "searchable" => false,
            ]
        );

        return $resolver;
    }
}
