<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class ClassNameColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class ClassNameColumn extends TextColumn
{
    /**
     * @param $value
     * @return string
     */
    public function value($value): string
    {
        try {
            $propertyAccessor = new PropertyAccessor();
            $className = $propertyAccessor->getValue($value, $this->getName());
            $className = (new \ReflectionClass($className))->getShortName();
            $className = preg_replace('/(?<=\\w)(?=[A-Z])/', ' $1', $className);
            return trim($className);
        } catch(\Exception $e){
            return $value;
        }
    }
}
