<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeColumn extends DateTimeColumn
{
    public function value($entity)
    {
        $this->options['datetype'] = \IntlDateFormatter::NONE;

        return parent::value($entity);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
                "timetype" => \IntlDateFormatter::SHORT,
            ]
        );

        return $resolver;
    }
}
