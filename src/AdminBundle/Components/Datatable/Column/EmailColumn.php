<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EmailColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class EmailColumn extends AbstractColumn
{

    /**
     * @param $entity
     * @return mixed|null|string
     * @throws \Exception
     */
    public function value($entity)
    {
        $value = parent::value($entity);

        if ($value) {
            return '<a href="mailto:' . $value . '">' . $value . '</a>';
        }

        return null;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'orderable' => true,
                'searchable' => true,
            ]
        );

        return $resolver;
    }
}