<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class EntityColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class EntityColumn extends AbstractColumn
{
    /**
     * @param $entity
     * @return bool|string
     */
    public function value($entity)
    {
        global $kernel;

        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            $values = [];

            if ($accessor->getValue($entity, $this->name) instanceof \Traversable) {
                $entities = $accessor->getValue($entity, $this->name);
            } else {
                $entities = [$accessor->getValue($entity, $this->name)];
            }

            if (is_array($entities)) {
                $entities = array_filter($entities, function ($entity) {
                    return $entity !== null;
                });
            }

            foreach ($entities as $subEntity) {
                if ($this->hasOption('value')) {
                    $value = call_user_func_array($this->getOption('value'), [$subEntity]);
                } else {
                    $value = (string)$accessor->getValue($entity, $this->name);
                }

                if ($this->hasOption('path')) {
                    $url = $kernel->getContainer()->get('router')->generate($this->getOption('path'), [
                        'id' => $subEntity->getId(),
                    ]);

                    $value = '<a href="' . $url . '">' . $value . '</a>';
                }

                array_push($values, $value);
            }

            if (count($values) > 3) {
                return implode(", ", array_slice($values, 0,
                        3)) . " en nog " . (count($values) - 3) . " " . strtolower($this->getOption('label'));
            } else {
                return implode(", ", $values);
            }
        }

        return false;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            "proxy",
        ]);

        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
            ]
        );

        $resolver->setDefined([
            "value",
            "path",
        ]);

        return $resolver;
    }
}
