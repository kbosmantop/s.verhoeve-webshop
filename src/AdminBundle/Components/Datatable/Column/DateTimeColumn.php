<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class DateTimeColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            if (!$accessor->getValue($entity, $this->name)) {
                return "<i>Geen</i>";
            }

            if ($this->options['datetype'] === \IntlDateFormatter::NONE && $this->options['timetype'] === \IntlDateFormatter::NONE) {
                return $accessor->getValue($entity, $this->name)->format("Y-m-d H:i:s");
            }

            $formattedDate = null;
            $formattedTime = null;

            if ($this->options['datetype'] !== \IntlDateFormatter::NONE) {
                $dateFormatter = new \IntlDateFormatter('nl_NL', $this->options['datetype'], \IntlDateFormatter::NONE);

                $formattedDate = $dateFormatter->format($accessor->getValue($entity, $this->name));
            }

            if ($this->options['timetype'] !== \IntlDateFormatter::NONE) {
                $timeFormatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::NONE, $this->options['timetype']);

                $formattedTime = $timeFormatter->format($accessor->getValue($entity, $this->name));
            }

            if ($formattedDate && $formattedTime) {
                return $formattedDate . " " . $this->options['separator'] . " " . $formattedTime;
            } elseif ($formattedDate) {
                return $formattedDate;
            } else {
                return $formattedTime;
            }
        }

        return false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
                "datetype" => \IntlDateFormatter::FULL,
                "timetype" => \IntlDateFormatter::SHORT,
                "separator" => " om ",
            ]
        );

        return $resolver;
    }
}
