<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class IdColumn extends AbstractColumn
{

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver;
    }

    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            return $accessor->getValue($entity, $this->name);
        }

        throw new Exception("Can't find a value for '" . $this->name . "'");
    }
}
