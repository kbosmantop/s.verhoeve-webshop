<?php

namespace AdminBundle\Components\Datatable;

/**
 * Class DatatableBuilder
 * @package AdminBundle\Components\Datatable
 */
class DatatableBuilder
{
    /**
     * @var array
     */
    private $children = [];

    public function __construct()
    {
    }

    /**
     * @param $name
     * @return string|null
     */
    private function findChildKey($name)
    {
        foreach ($this->children as $key => $content) {
            if ($content['keyName'] === $name) {
                return $key;
            }
        }

        return null;
    }

    /**
     * @param           $child
     * @param null      $column
     * @param array     $options
     * @param int|null  $insertOnPostion
     * @return $this
     */
    public function add($child, $column = null, array $options = [], int $insertOnPostion = null)
    {
        $insertOnPostion = ($insertOnPostion) ? $insertOnPostion : \count($this->children);
        $this->arrayInsert($this->children, $insertOnPostion, [
            $child => [
                'column' => $column,
                'options' => $options,
                'keyName' => $child,
            ],
        ]);

        return $this;
    }

    /**
     * @param array      $array
     * @param int|string $position
     * @param mixed      $insert
     */
    private function arrayInsert(&$array, $position, $insert)
    {
        if (\is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos = array_search($position, array_keys($array), true);
            $array = array_merge(
                \array_slice($array, 0, $pos),
                $insert,
                \array_slice($array, $pos)
            );
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        if (($childKey = $this->findChildKey($name)) !== null) {
            return $this->children[$childKey];
        }

        throw new \InvalidArgumentException(sprintf('The child with the name "%s" does not exist.', $name));
    }

    /**
     * @param $name
     * @return $this
     */
    public function remove($name)
    {
        if (($childKey = $this->findChildKey($name)) !== null) {
            unset($this->children[$childKey]);
        }

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        if ($this->findChildKey($name) !== null) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function all()
    {
        $children = [];
        foreach ($this->children as $key => $content) {
            $children[$this->children[$key]['keyName']] = $content;
        }

        return $children;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->all());
    }

    /**
     * @return Datatable
     */
    public function getDatatable()
    {
        $datatable = new Datatable();

        foreach ($this->children as $key => $child) {
            $options = $child['options'];
            $keyName = $child['keyName'];

            $datatable->add(new $child['column']($keyName, $options));
        }

        return $datatable;
    }
}
