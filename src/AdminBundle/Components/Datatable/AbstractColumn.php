<?php

namespace AdminBundle\Components\Datatable;

use Exception;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class AbstractColumn
 * @package AdminBundle\Components\Datatable
 */
abstract class AbstractColumn
{
    /** @var string $name */
    protected $name;

    /** @var array $options */
    protected $options;

    /**
     * AbstractColumn constructor.
     * @param       $name
     * @param array $options
     */
    public function __construct($name, array $options)
    {
        $this->name = $name;

        $resolver = new OptionsResolver();
        $resolver = $this->setDefaultOptions($resolver);

        $this->options = $resolver->resolve($options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasOption($key)
    {
        return array_key_exists($key, $this->options);
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getOption($key)
    {
        if (!$this->hasOption($key)) {
            return null;
        }

        return $this->options[$key];
    }

    /**
     * @param $entity
     * @return mixed|null
     * @throws Exception
     */
    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if (method_exists($entity, 'getTranslations') && $accessor->isReadable($entity->translate('nl'),
                $this->name)) {
            return $accessor->getValue($entity->translate('nl_NL'), $this->name);
        }

        if ($accessor->isReadable($entity, $this->name)) {
            return $accessor->getValue($entity, $this->name);
        }

        if ($this->hasOption('empty_data')) {
            return $this->getOption('empty_data');
        }

        throw new \RuntimeException("Can't find a value for '" . $this->name . "'");
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(
            [
                'label',
            ]
        );

        $resolver->setDefaults(
            [
                'searchable' => false,
                'orderable' => false,
                'width' => null,
                'empty_data' => null,
                'type' => 'string'
            ]
        );

        $resolver->setAllowedTypes('searchable', ['boolean']);
        $resolver->setAllowedTypes('orderable', ['boolean']);
        $resolver->setAllowedTypes('type', ['string']);
        $resolver->setAllowedTypes('width', ['null', 'integer', 'string']);

        $resolver->setAllowedValues('width', function ($value) {
            return !(\is_string($value) && (strpos($value, ' ') !== false || strpos($value, '%') === false));
        });

        $resolver->setAllowedValues('type', ['string', 'date', 'num', 'num-fmt', 'html-num', 'html-num-fmt', 'html']);

        $resolver = $this->configureOptions($resolver);

        return $resolver;
    }

    /**
     * @param OptionsResolver $resolver
     * @return mixed
     */
    abstract public function configureOptions(OptionsResolver $resolver);
}
