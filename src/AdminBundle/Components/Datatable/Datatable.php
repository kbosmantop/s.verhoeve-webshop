<?php

namespace AdminBundle\Components\Datatable;

class Datatable implements \IteratorAggregate, \ArrayAccess
{

    private $children = [];

    public function add($child, $type = null, array $options = [])
    {
        void($type, $options);

        $this->children[] = $child;
    }

    /**
     * Returns the iterator
     *
     * @return \Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children);
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->children[] = $value;
        } else {
            $this->children[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->children[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->children[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->children[$offset]) ? $this->children[$offset] : null;
    }
}
