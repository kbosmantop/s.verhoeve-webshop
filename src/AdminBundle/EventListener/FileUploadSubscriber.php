<?php


namespace AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use League\Flysystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class FileUploadSubscriber
 * @package AdminBundle\EventListener
 */
class FileUploadSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    public function postPersist(LifecycleEventArgs $args)
    {
        if (is_object($args)) {
            $this->update($args, true);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        if (is_object($args)) {
            $this->update($args, true);
        }
    }

    private function update(LifecycleEventArgs $args, $upload = false)
    {
        $entity = $args->getEntity();

        if (!\in_array(ImageInterface::class, class_implements($entity), true)) {
            return;
        }

        $moveToPath = str_replace('tmp/uploads', $entity->getStoragePath(), $entity->getPath());

        if (empty($moveToPath)) {
            return;
        }

        if ($upload) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($entity->getPath(), $moveToPath);
            }
        }

        $em = $args->getEntityManager();

        $entity->setPath(str_replace('tmp/uploads', $entity->getStoragePath(), $entity->getPath()));

        $changeset = $em->getUnitOfWork()->getEntityChangeSet($entity);

        /**
         * Only add a transformation job if the image path changed
         **/
        if (!empty($changeset[$entity->getPathColumn()])) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($entity->getPath(), $moveToPath);
            }

            $this->addTransformation($args->getEntity());
        }
    }

    private function addTransformation($entity)
    {
        $this->container->get('cms.media_image')->process($entity);
    }

    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }
}
