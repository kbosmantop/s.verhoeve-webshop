<?php

namespace AdminBundle\EventListener;

use AppBundle\Entity\Catalog\Product\ProductImage;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductImageSubscriber
 * @package AdminBundle\EventListener
 */
class ProductImageSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * @param LifecycleEventArgs $args
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        if (is_object($args)) {
            $this->update($args, true);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        if (is_object($args)) {
            $this->update($args, false);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param bool $upload
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    private function update(LifecycleEventArgs $args, $upload = false): void
    {
        $productImage = $args->getEntity();

        // only act on some "ProductImage" entity
        if (!$productImage instanceof ProductImage) {
            return;
        }

        $moveToPath = str_replace('tmp/images/product', 'images/product/' . $productImage->getProduct()->getId(),
            $productImage->getPath());

        if ($upload) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($productImage->getPath(), $moveToPath);
            }
        }

        $em = $args->getEntityManager();

        $productImage->setPath(str_replace('tmp/images/product',
            'images/product/' . $productImage->getProduct()->getId(), $productImage->getPath()));

        $em->flush();

        $changeset = $em->getUnitOfWork()->getEntityChangeSet($productImage);

        /**
         * Only add a transformation job if the image path changed
         **/
        if (!empty($changeset['path'])) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($productImage->getPath(), $moveToPath);
            }
        }
    }

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'postPersist',
//            'postUpdate',
        ];
    }
}
