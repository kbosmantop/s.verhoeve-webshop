<?php

namespace AdminBundle\Locale;

use AdminBundle\Interfaces\LocaleDetectorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestStackDetector implements LocaleDetectorInterface
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @param RequestStack $requestStack
     * @param string       $defaultLocale
     */
    public function __construct(RequestStack $requestStack, $defaultLocale)
    {
        $this->requestStack = $requestStack;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
        if ($request = $this->requestStack->getCurrentRequest()) {
            return $request->getLocale();
        }

        return $this->defaultLocale;
    }
}
