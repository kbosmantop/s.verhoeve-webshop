<?php


namespace AdminBundle\Filter;

use AdminBundle\EventListener\PublishableListener;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Symfony\Bridge\Doctrine\ContainerAwareEventManager;

/**
 * Class PublishableFilter
 * @package AdminBundle\Filter
 */
class PublishableFilter extends SQLFilter
{
    protected $listener = null;
    protected $entityManager;
    protected $disabled = [];

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string        $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     * @throws \ReflectionException
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $class = $targetEntity->getName();

        if (!\in_array('AppBundle\Traits\PublishableEntity', class_uses($class), true)) {
            return '';
        }

        $isAdmin = false;

        if (null !== $this->getListener() && null !== $this->getListener()->getTokenStorage()->getToken() && $this->getListener()->getTokenStorage()->getToken()->getUser() instanceof User) {
            $isAdmin = (bool)array_intersect(['ROLE_ADMIN', 'ROLE_SUPER_ADMIN'],
                $this->getListener()->getTokenStorage()->getToken()->getUser()->getRoles(false));
        }

        if (
            $isAdmin
            || (array_key_exists($class, $this->disabled) && $this->disabled[$class] === true)
            || (array_key_exists($targetEntity->rootEntityName,
                    $this->disabled) && $this->disabled[$targetEntity->rootEntityName] === true)
        ) {
            return '';
        }

        /** @var Connection $conn */
        $conn = $this->getEntityManager()->getConnection();
        $platform = $conn->getDatabasePlatform();

        $column = 'publish';
        $columnStart = 'publish_start';
        $columnEnd = 'publish_end';

        $now = $conn->quote(date($platform->getDateTimeFormatString())); // should use UTC in database and PHP

        $columnStartNull = $platform->getIsNullExpression($targetTableAlias . '.' . $columnStart);
        $columnEndNull = $platform->getIsNullExpression($targetTableAlias . '.' . $columnEnd);

        $addCondSql = "(
            {$targetTableAlias}.{$column} = 1
            AND (
                {$columnStartNull} OR {$targetTableAlias}.{$columnStart} <= {$now}
            )
            AND (
                {$columnEndNull} OR {$targetTableAlias}.{$columnEnd} >= {$now}
            )
        )";

        return $addCondSql;
    }

    /**
     * @param $class
     */
    public function disableForEntity($class)
    {
        $this->disabled[$class] = true;
    }

    /**
     * @param $class
     */
    public function enableForEntity($class)
    {
        $this->disabled[$class] = false;
    }

    /**
     * @return PublishableListener|null
     * @throws \ReflectionException
     */
    public function getListener()
    {
        if ($this->listener === null) {
            $em = $this->getEntityManager();
            /** @var ContainerAwareEventManager $evm */
            $evm = $em->getEventManager();

            foreach ($evm->getListeners() as $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof PublishableListener) {
                        $this->listener = $listener;

                        break 2;
                    }
                }
            }

            if ($this->listener === null) {
                throw new \RuntimeException('Listener "PublishableListener" was not added to the EventManager!');
            }
        }

        return $this->listener;
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    protected function getEntityManager()
    {
        if ($this->entityManager === null) {
            $refl = new \ReflectionProperty(SQLFilter::class, 'em');
            $refl->setAccessible(true);
            $this->entityManager = $refl->getValue($this);
        }

        return $this->entityManager;
    }
}
