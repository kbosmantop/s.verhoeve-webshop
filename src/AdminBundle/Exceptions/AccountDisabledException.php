<?php

namespace AdminBundle\Exceptions;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * Class AccountDisabledException
 */
class AccountDisabledException extends AccountStatusException
{
    /**
     * @return string
     */
    public function getMessageKey(): string
    {
        return 'Account is disabled.';
    }
}