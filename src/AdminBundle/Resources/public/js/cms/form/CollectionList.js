function CollectionList(elm, index) {
    this.elm = elm;
    this.index = index;

    this.elmCount = 0;

    this.onClickItemAdd = function (e) {
        e.preventDefault();

        var elmCount = this.getElmCount();
        var newElmCount = elmCount + 1;
        var buttonElm = $(e.target);
        var collectionListElm = buttonElm.closest('.collection-list');
        var newItemHtml = collectionListElm.attr('data-prototype');
        var newItem = $(this.transformHtml(newItemHtml));
        var newItemId = 'new_collection_list_item' + newElmCount;

        newItem.attr('id', newItemId);
        newItem.toggleClass('show');

        // Add new item to collection list.
        collectionListElm.find('> .collection-list-items').append(newItem);

        this.bindItemEvents(newItem);

        this.initFormElements();

        $.uniform.update();

        if (typeof(setupFormValidation) !== 'undefined') {
            setupFormValidation({}, true);
        }

        // Hide position field if exist.
        newItem.find('input.position').closest('.field-container').hide();

        var event = new CustomEvent('add_list_collection_item', { detail: {
            item: newItem
        }});
        this.elm[0].dispatchEvent(event);

        return newItemId;
    };

    this.onClickItemRemove = function (e) {
        e.preventDefault();

        var elm = $(e.currentTarget);
        var collectionListItem = elm.closest('.collection-list-item');

        collectionListItem.remove();

        var event = new Event('remove_list_collection_item');
        this.elm[0].dispatchEvent(event);
    };

    this.onClickItemSettings = function (e) {
        e.preventDefault();

        var elm = $(e.currentTarget);
        var collectionListItem = elm.closest('.collection-list-item');

        collectionListItem.toggleClass('show');
    };

    this.bindItemEvents = function (itemElm) {
        // bind change events...
        var itemLabelKeys = itemElm.data('label-field-keys').split(',');
        var formFullName = itemElm.data('form-full-name');

        this.updateItemLabel(itemElm, itemLabelKeys, formFullName);

        $('> .list-heading button[data-action=delete]', itemElm).on('click', this.onClickItemRemove.bind(this));
        $('> .list-heading button[data-action=settings]', itemElm).on('click', this.onClickItemSettings.bind(this));

        this.bindFieldEvents(itemElm, itemLabelKeys, formFullName);
    };

    this.bindFieldEvents = function (itemElm, itemLabelKeys, formFullName) {
        for (var key in itemLabelKeys) {
            if (itemLabelKeys.hasOwnProperty(key)) {
                var labelKey = itemLabelKeys[key];
                var fieldSelector = `${formFullName}[${labelKey}]`;
                var field = $(`[name="${fieldSelector}"]`, this.elm);

                field.on('change keyup bind', function (e) {
                    this.updateItemLabel(itemElm, itemLabelKeys, formFullName);
                }.bind(this));
            }
        }

        return this;
    };

    this.updateItemLabel = function (itemElm, itemLabelKeys, formFullName) {
        var values = [];
        for (var key in itemLabelKeys) {

            if (itemLabelKeys.hasOwnProperty(key)) {
                var name = itemLabelKeys[key];
                var selector = `[name="${formFullName}[${name}]"]`;
                var fieldElm = $(selector, itemElm);

                values.push(fieldElm.val());
            }
        }

        var labelElm = itemElm.find('> .list-heading > .labels');

        if(values.length > 0) {
            labelElm.html(values.join(' '));
        } else {
            labelElm.html('');
        }

        return this;
    };

    this.transformHtml = function (html) {
        var elmCount = this.getElmCount();

        var protoLabel = this.elm.data('prototype-label');
        var protoName = this.elm.data('prototype-name');
        var protoLabelRegex = new RegExp(protoLabel, "g");
        var protoNameRegex = new RegExp(protoName, "g");

        html = html.replace(protoLabelRegex, '');
        html = html.replace(protoNameRegex, elmCount);

        // Remove empty labels tags.
        html = html.replace('<label></label>', '');

        return html;
    };

    this.initFormElements = function () {
        var elements = cms.getElements();

        elements.initElementCheckboxAndRadio();
        elements.initElementSelect2();
        elements.initElementMultiSelect();
        elements.initElementDate();
        elements.initElementDateTime();
        elements.initElementTime();
        elements.initElementCollectionList(this.elm);
    };

    this.bindEvents = function () {
        this.elm.on('click', '> .actions button[data-action=add]', this.onClickItemAdd.bind(this));

        var self = this;
        var childs = this.elm.find('> .collection-list-items > .collection-list-item');

        childs.each(function () {
            var itemElm = $(this);
            self.bindItemEvents(itemElm);
        });

        this.initSortable();
    };

    this.initSortable = function () {
        var sortable = this.elm.find('> .collection-list-items');

        sortable.sortable({
            placeholder: 'collection-list-item-placeholder list-item',
            handle: '> .list-heading > .drag-handle',
            classes: {
                'ui-sortable': 'highlight'
            },
            start: function (event, ui) {
                ui.item.removeClass('show')
            },
            stop: function (e, ui) {
                var sortCount = 1;
                sortable.find('> div').each(function () {
                    var listItemElm = $(this);
                    var formFullName = listItemElm.data('form-full-name');
                    var fieldElm = listItemElm.find(`[name="${formFullName}[position]"]`);

                    fieldElm.val(sortCount);
                    sortCount++;
                });
            }
        });
    };

    this.calcElmCount = function () {
        return this.elmCount = this.elm.find('> .collection-list-items > div').length;
    };

    /**
     * @returns {number}
     */
    this.getElmCount = function () {
        return this.calcElmCount();
    };

    this.setDefaults = function () {
        this.calcElmCount();
    };

    this.init = function () {
        this.setDefaults();
        this.bindEvents();
    };

    this.init();
}