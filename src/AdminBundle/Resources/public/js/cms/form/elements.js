function CmsFormElements(relativeElm) {

    this.relativeElm = relativeElm;

    this.init();
}

CmsFormElements.prototype.init = function () {
    this.initElements();
};

CmsFormElements.prototype.initElements = function (context) {
    this.initElementSelect2();
    this.initElementRange(context);
    this.initElementMultiSelect();
    this.initElementCheckboxAndRadio();
    this.initElementDate();
    this.initElementTime();
    this.initElementDateTime();
    this.initElementCKEditor();
    this.initElementColor();
    this.initElementCollection();
    this.initElementCollectionList(context);
    this.initElementSlug();
    this.initTranslationElements();
};

CmsFormElements.prototype.initElementSelect2 = function () {
    var self = this;

    $.each($('select.select2'), function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).data());

        if(manualOptions.hasOwnProperty('noSearch') && manualOptions.noSearch === true){
            manualOptions = {};
            manualOptions.minimumResultsForSearch = window.Infinity;
        }

        if (manualOptions.ajax) {
            manualOptions.ajax = {
                processResults: function (data) {
                    return {
                        results: data.items
                    };
                },
                data: function (params) {
                    return {
                        name: $(elm).data('name'),
                        search: params.term
                    };
                }
            }
        }
        $(elm).select2(manualOptions);
    });
};

CmsFormElements.prototype.initElementRange = function (context) {
    var elms = $('.range-slider', context);

    if (elms.length > 0) {
        elms.each(function () {
            var elm = $(this);

            if (!elm.data('bind-events')) {
                elm.attr('data-bind-events', true);

                elm.wrap(`<div class="range-slider-container"></div>`);
                elm.before(`<b>${elm.data('slider-min')}</b>`);
                elm.after(`<b>${elm.data('slider-max')}</b>`);

                elm.slider();
            }
        });
    }
};

CmsFormElements.prototype.initElementMultiSelect = function () {
    var self = this;
    var elements = $('select.multiselect');

    var settings = {
        enableClickableOptGroups: true,
        nonSelectedText: 'Geen selectie',
        selectAllText: 'Selecteer alles',
        allSelectedText: 'Alles geselecteerd',
        filterPlaceholder: 'Typ om te zoeken',
        numberDisplayed: 1,
        buttonClass: 'btn btn-xs btn-default',
        templates: {
            divider: '<div class="divider" data-role="divider"></div>',
            filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        }
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        options.onChange = function (option, checked, select) {
            $(elm).validate();

            $('input[value=\'multiselect-all\']', $(elm).parents('.multi-select-full'))
            .attr('checked', false)
            .prop('checked', false);

            $.uniform.update();
        };

        if (options.includeSelectAllOption) {
            options.onSelectAll = function () {
                $.uniform.update();
            };
        }

        if (options.enableFiltering) {
            options.enableCaseInsensitiveFiltering = true;
        }

        if (options.includeSelectAllOption && options.enableFiltering) {
            options.selectAllJustVisible = true;
        }

        $(elm).multiselect(options);
    });

    // Styled checkboxes and radios
    $('.multiselect-container input').uniform({radioClass: 'choice'});
};

CmsFormElements.prototype.initElementCheckboxAndRadio = function () {
    var self = this;
    var elements = $('input.styled');

    var settings = {
        radioClass: 'choice',
        wrapperClass: 'bg-white'
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        $(elm).uniform(options);
    });
};

CmsFormElements.prototype.initElementDate = function () {
    var self = this;
    var elements = $('.date_picker input.form-control');

    var settings = {
        selectYears: true,
        selectMonths: true,
        formatSubmit: 'yyyy-mm-dd',
        hiddenPrefix: '',
        hiddenSuffix: '',
        clear: 'wissen',
        close: false
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).parent('.date_picker').data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        $(elm).pickadate(options);
    });
};

CmsFormElements.prototype.initElementTime = function () {
    var self = this;
    var elements = $('.time_picker input.form-control');

    var settings = {
        format: 'HH:i',
        formatSubmit: 'HH:i',
        hiddenPrefix: '',
        hiddenSuffix: '',
        clear: 'wissen'
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).parent('.time_picker').data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        $(elm).pickatime(options);
    });
};


CmsFormElements.prototype.initElementDateTime = function () {
    var self = this;
    var container = $('.datetime_picker');
    var elements = ['date', 'time'];

    var settings = {
        date: {
            selectYears: true,
            selectMonths: true,
            formatSubmit: 'yyyy-mm-dd',
            hiddenPrefix: '',
            hiddenSuffix: '',
            clear: 'wissen',
            close: false,
            onSet: function () {
                var time_id = this.get('id').replace('_date', '_time');
                var inputTimeId = $('input[id="' + time_id + '"]');
                var time_picker = inputTimeId.pickatime('picker');

                var element_id = inputTimeId.attr('data-element-id');

                if (!this.get('select', 'yyyy-mm-dd')) {
                    time_picker.set('clear');
                }

                setDateTime(element_id, this, time_picker);
            }
        },
        time: {
            format: 'HH:i',
            formatSubmit: 'HH:i',
            hiddenPrefix: '',
            hiddenSuffix: '',
            clear: 'wissen',
            interval: 5,
            onSet: function () {
                var date_id = this.get('id').replace('_time', '_date');
                var inputDateId = $('input[id="' + date_id + '"]');
                var date_picker = inputDateId.pickadate('picker');
                var element_id = inputDateId.attr('data-element-id');

                setDateTime(element_id, date_picker, this);
            }
        }
    };

    for (element in elements) {
        $.each($(container).find('input.' + elements[element]), function (i, elm) {
            var manualOptions = self.convertAttributesToObject($(elm).data());
            var options = {};

            $.extend(true, options, settings[elements[element]], manualOptions);

            if (elements[element] === 'date') {
                $(elm).pickadate(options);
            } else {
                $(elm).pickatime(options);
            }
        });
    }

    function setDateTime(element_id, date_picker, time_picker) {
        var datetime_picker = $('input[name="' + element_id + '"]');
        var value = '';

        var date = date_picker.get('select', 'yyyy-mm-dd');
        var time = time_picker.get('select', 'HH:i');

        if (date && !time) {
            time = '00:00';
        }

        if (time) {
            time += ':00';
        }

        if (date && time) {
            value = date + ' ' + time;
        }

        $(datetime_picker).val(value);
    }
};

CmsFormElements.prototype.initElementCKEditor = function () {
    var self = this;
    var elements = null;

    elements = $('.ckeditor textarea.form-control', this.relativeElm);

    elements.unbind();

    var settings = {
        height: '200px'
    };

    var custom_toolbars = {
        'basic': [
            {
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat']
            },
            {
                name: 'paragraph',
                items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'NumberedList', 'BulletedList']

            },
            {
                name: 'links',
                items: ['Link', 'Unlink', 'Anchor']
            }
        ]
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).parent('.ckeditor').data());
        var options = {};

        if (manualOptions.custom_toolbar) {
            options.toolbar = custom_toolbars[manualOptions.custom_toolbar];
            options.removeButtons = false;
        }

        $.extend(true, options, settings, manualOptions);

        var id = $(elm).attr('id');
        if (!CKEDITOR.instances[id]) {
            $(elm).data('ckeditor', CKEDITOR.replace(id, options));
        }
    });
};

CmsFormElements.prototype.initElementColor = function () {
    var self = this;
    var elements = $('.color_picker input.form-control');

    var default_color_palette = [
        '#8C914F', '#755426',
        '#b5251a', '#d77710',
        '#919b16', '#44493e'
    ];

    var settings = {
        showPalette: true,
        showInitial: true,
        showInput: true,
        showAlpha: true,
        allowEmpty: true,
        preferredFormat: 'rgb',
        palette: default_color_palette
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).parent('.color_picker').data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        $(elm).spectrum(options);
    });
};

CmsFormElements.prototype.initElementCollection = function () {
    var self = this;
    var elements = $('.collection-items');

    var settings = {};

    $.each(elements, function (i, elm) {
        $(elm).data('item-count', $(elm).find('.collection-item').length);

        var isSortable = $(elm).find('.table').attr('data-sortable');

        if (isSortable) {
            var sortableTable = $('.collection-items-container table[data-sortable=true]', $(elm));

            $(sortableTable).rowSorter({
                handler: '.drag-handle .handle i',
                onDragStart: function (tbody, row, index) {

                },
                onDrop: function (tbody, row, new_index, old_index) {
                    collectionCalculatePosition(elm);
                }
            });

            var sortableColumnIndex = $(sortableTable).find('input[name*=\'position\']').parents('td').index();

            $(sortableTable).data('sortable-column-index', sortableColumnIndex);

            collectionHideSortableColumn($(sortableTable));
        }

        $(elm).on('click', '.btn-add', function (e) {
            e.preventDefault();

            collectionAddRow(elm);
        });

        $(elm).on('click', '.btn-remove', function (e) {
            e.preventDefault();

            collectionRemoveRow($(this).closest('.collection-item'));
        });

        $(elm).css('visibility', 'visible');
    });
};

CmsFormElements.prototype.initElementCollectionList = function (context) {
    var elements = $('.collection-list', context);

    elements.each(function (index) {
        new CollectionList($(this), index);
    });
};

CmsFormElements.prototype.initElementSlug = function () {
    var self = this;
    var elements = $('.slugify input.form-control');

    var settings = {
        readonly: true,
        methods: ['keyup', 'change']
    };

    $.each(elements, function (i, elm) {
        var manualOptions = self.convertAttributesToObject($(elm).parent('.slugify').data());
        var options = {};

        $.extend(true, options, settings, manualOptions);

        $(elm).on(options.methods.join(' '), function (e) {
            // store current positions in variables
            var start = $(this).get(0).selectionStart,
                end = $(this).get(0).selectionEnd;

            if ([37, 39, 16, 17].indexOf(e.keyCode) === -1) {
                $(this).val($(this).val().slugify());
            }

            $(this).get(0).setSelectionRange(start, end);
        });

        var slugContainer = $(elm).closest('.slugify');

        if (slugContainer.attr('data-base-on')) {
            var slugBaseOn = $(slugContainer.attr('data-base-on'), slugContainer.closest('.tab-pane'));

            if (slugBaseOn) {
                slugBaseOn.on('change', function (e) {
                    if ($(elm).val() && confirm('Er is al een slug aanwezig, wil je deze overschrijven met de nieuwe data')) {
                        $(elm).val(slugBaseOn.val().slugify());
                    } else if (!$(elm).val()) {
                        $(elm).val(slugBaseOn.val().slugify());
                    }
                });
            }
        }
    });
};

CmsFormElements.prototype.initTranslationElements = function () {
    var timer;
    var delay = 350;

    $('body').on('mouseenter mouseleave', '.translation-item-container', function (e) {
        var elm = $(this);

        if (e.type === 'mouseenter') {
            timer = setTimeout(function () {
                $(elm).toggleClass('active', true);
            }, delay);
        } else if (e.type === 'mouseleave') {
            clearTimeout(timer);

            $(elm).toggleClass('active', false);
        }
    });
};

CmsFormElements.prototype.convertAttributesToObject = function (attributes) {
    var self = this;

    var options = {};

    for (var key in attributes) {
        options[self.camelCase(key)] = attributes[key];
    }

    return options;
};

CmsFormElements.prototype.camelCase = function (string) {
    return jQuery.camelCase(string);
};

function collectionCalculatePosition(elm) {
    $(elm).find('input[name*=\'position\']').each(function (i, position) {
        $(position).val(i + 1);
    });
}

function collectionAddRow(elm) {

    var newItem = $(elm).attr('data-prototype');
    newItem = newItem.replace(/__name__/g, $(elm).data('item-count'));

    $(elm).data('item-count', $(elm).data('item-count') + 1);

    var row_id = 'collection_row_inserted_' + $(elm).data('item-count');
    $(newItem).appendTo($(elm).find('.collection-item-list')).attr('id', row_id);

    var isSortable = $(elm).find('.table').attr('data-sortable');

    if (isSortable) {
        collectionCalculatePosition($(elm));

        collectionHideSortableColumn($(elm).find('.table'));
    }

    cms.getElements().initElementCheckboxAndRadio();
    cms.getElements().initElementSelect2();
    cms.getElements().initElementMultiSelect();
    cms.getElements().initElementDate();
    cms.getElements().initElementDateTime();
    cms.getElements().initElementTime();

    $.uniform.update();

    if (typeof(setupFormValidation) != 'undefined') {
        setupFormValidation({}, true);
    }

    return row_id;
}

function collectionRemoveRow(row) {
    var container = $(row).closest('.collection-items');

    $(row).remove();

    var isSortable = $(container).find('.table').eq(0).attr('data-sortable');

    if (isSortable) {
        collectionCalculatePosition(container);
    }
}

function collectionHideSortableColumn(table) {
    $.each($(table).find('tr'), function (i, tr) {
        $(tr).children().eq(1).addClass('sortable-column');
    });
}
