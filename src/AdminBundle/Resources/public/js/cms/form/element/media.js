;(function ($, window, document) {
    // Create the defaults once
    var pluginName = 'Media',
        defaults = {
            test: true
        };


    // The actual plugin constructor
    function Media(element, options) {
        this.options = $.extend(true, {}, options, defaults);

        this.element = element;

        this._defaults = defaults;
        this._name = pluginName;
        this.id = $(element).find('[data-type="elfinder-input-field"]').attr('id');

        this.element_upload = $('.media-widget-uploader', this.element);
        this.uploadUrl = $(this.element).attr('data-upload-url');
        this.mimeTypes = [];

        this.queueErrors = [];
        this.minimumImageDimensions = null;

        this.init();
    }

    Media.prototype = {
        init: function () {
            this.render(this.options);

            if (this.uploadUrl) {
                this.initUploader();
            }

            this.bindEvents(this.options);
        },

        render: function () {
            var input_element = $(this.element).find('[data-type="elfinder-input-field"]');

            $(this.element).attr('data-media-widget-id', $(input_element).attr('id'));
        },
        bindEvents: function () {
            var self = this;

            var input_element = $(this.element).find('[data-type="elfinder-input-field"]');
            var thumbnail_element = $(this.element).find('.media-widget-thumbnail-container');
            var thumbnail_selectlink_element = $(this.element).find('[data-action="media[select]"]');
            var thumbnail_uploadlink_element = $(this.element).find('[data-action="media[upload]"]');
            var thumbnail_removelink_element = $(this.element).find('[data-action="media[remove]"]');

            $(thumbnail_element).on('click', function () {
                self.open();
            });

            $(thumbnail_selectlink_element).on('click', function (e) {
                e.preventDefault();

                self.open();
            });

            $(thumbnail_uploadlink_element).on('click', function (e) {
                e.preventDefault();

                if (self.uploadUrl) {
                    self.showUploader();
                }
            });

            $(thumbnail_removelink_element).on('click', function (e) {
                e.preventDefault();

                self.remove();
            });

            $(input_element).on('change', function () {

                var container = $(this).closest('.media-widget');
                var baseUrl = $(container).attr('data-media-base-url');

                if (baseUrl.substr(-1) !== '/') {
                    baseUrl += '/';
                }

                if ($(this).val()) {
                    $('.media-widget-thumbnail-image img', container).attr('src', baseUrl + $(this).val());
                    $('.media-widget-thumbnail-container', container).show();
                    $('a[data-action="media[remove]"]', container).show();
                } else {
                    $('.media-widget-thumbnail-image img', container).removeAttr('src');
                    $('.media-widget-thumbnail-container', container).hide();
                    $('a[data-action="media[remove]"]', container).hide();
                }
            });

            $(document).on('dragover', function (e) {
                var dt = e.originalEvent.dataTransfer;
                if (dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') !== -1 && dt.types.indexOf('Files') !== null : dt.types.contains('application/x-moz-file'))) {
                    self.showUploader();

                    window.clearTimeout(self.fileDragTimer);
                }
            });

            $(document).on('dragleave', function () {
                self.fileDragTimer = window.setTimeout(function () {
                    self.hideUploader();
                }, 25);
            });
        },
        open: function () {
            var self = this;

            var html = '<iframe src="/admin/elfinder/form?id=' + self.id + '" frameborder="0" width="100%" height="602" scrolling="no"></iframe>';

            self.modal = bootbox.dialog({
                animate: false,
                message: html,
                title: 'Kies een bestand',
                className: 'modal-filemanager',
                show: true
            }).find('.modal-dialog').addClass('modal-full');
        },
        close: function () {
            bootbox.hideAll();
        },
        setPath: function (path) {
            var input_element = $(this.element).find('[data-type="elfinder-input-field"]');
            var mediaWidgetFilename = $(this.element).find('.media-widget-filename');

            $(input_element).val(path).trigger('change');
            mediaWidgetFilename.html(path);
        },
        setThumbnailPath: function (path) {
            var thumbnail_element = $(this.element).find('.media-widget-thumbnail-image img');

            if (thumbnail_element.length === 0) {
                thumbnail_element = $('<img />').appendTo($(this.element).find('.media-widget-thumbnail-image .centered'));
            }

            $(thumbnail_element).attr('src', path);
        },
        initUploader: function () {
            var self = this;

            if ($(self.element).attr('data-type') === 'image') {
                self.mimeTypes.push({
                    title: 'Afbeelding',
                    extensions: 'jpg,gif,png'
                });
            } else if ($(self.element).attr('data-type') === 'video') {
                self.mimeTypes.push({
                    title: 'Video',
                    extensions: 'mp4,ogg'
                });
            } else {
                self.mimeTypes.push({
                    title: 'Bestand',
                    extensions: 'json'
                });
            }

            var uploaderOptions = {
                // General settings
                runtimes: 'html5',
                url: self.uploadUrl,
                unique_names: true,
                multi_selection: false,
                filters: {
                    mime_types: self.mimeTypes
                }
            };

            if ($(self.element).attr('data-dimensions')) {
                var dimensions = JSON.parse($('.media-widget').attr('data-dimensions'));

                if (dimensions.min) {
                    self.minimumImageDimensions = dimensions.min;

                    uploaderOptions.filters.min_dimensions = [self.minimumImageDimensions[0], self.minimumImageDimensions[1]];
                }
            }

            $(self.element_upload).html('').pluploadQueue(uploaderOptions);

            $(self.element_upload).pluploadQueue().bind('Error', function (uploader, error) {
                if (error.code === plupload.IMAGE_DIMENSIONS_ERROR) {
                    self.queueErrors['' + error.file.id + ''] = error.message;
                }
            });

            $(self.element_upload).pluploadQueue().bind('FilesAdded', function (uploader, files) {
                for (var i = 0; i < files.length; i++) {
                    if (self.queueErrors[files[i].id]) {
                        $('<span class="file-filter-error icon-warning" data-popup="popover" data-trigger="hover" title="Waarschuwing" data-content="Afbeelding voldoet niet aan de minimum afmetingen: ' + self.minimumImageDimensions.join(' x ') + '"></span>').insertBefore($('.plupload_file_name span', $('#' + files[i].id)));
                    }
                }

                $('[data-popup="popover"]', $('.plupload_file_name')).popover();
            });

            $(self.element_upload).pluploadQueue().bind('FileUploaded', function (uploader, file, response) {
                var result = JSON.parse(response.response);

                self.setPath(result.file_path);
                self.setThumbnailPath(result.thumbnail_path);
            });

            $(self.element_upload).pluploadQueue().bind('UploadComplete', function (uploader) {
                if (uploader.total.uploaded === uploader.files.length) {
                    $('.plupload_buttons').css('display', 'inline');
                    $('.plupload_upload_status').css('display', 'none');
                    $('.plupload_start').addClass('plupload_disabled');

                    setTimeout(function () {
                        uploader.splice();
                        uploader.destroy();

                        self.hideUploader();
                    }, 200);
                }
            });
        },
        showUploader: function () {
            $(this.element_upload).fadeIn();
            $('.media-widget-thumbnail-container', this.element).fadeOut();
        },
        hideUploader: function () {
            $(this.element_upload).html('').fadeOut();
            $('.media-widget-thumbnail-container', this.element).show();
        },
        remove: function () {
            var input_element = $(this.element).find('[data-type="elfinder-input-field"]');
            var mediaWidgetFilename = $(this.element).find('.media-widget-filename');

            $(input_element).val('').trigger('change');
            mediaWidgetFilename.html('');
        }
    };

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName,
                    new Media(this, options));
            }
        });
    };

})(jQuery, window, document);
