;(function ($, window, document, undefined) {
    // Create the defaults once
    var pluginName = 'Crud',
        select2Defaults = {
            placeholder: 'Selecteer een actie...',
            minimumResultsForSearch: '-1',
            escapeMarkup: function (m) {
                return m;
            },
            templateResult: function (data) {
                var element_data = $(data.element).data();
                var result = '';

                if (typeof(element_data) !== 'undefined') {
                    if ('icon' in element_data) {
                        result += '<span class="position-left ' + element_data.icon + '"></span>';
                    }
                }

                result += data.text;

                return result;
            }
        },
        confirm_delete_modal_options = {
            type: 'question',
            title: 'Let op!',
            showCancelButton: true,
            confirmButtonText: 'Ja',
            cancelButtonText: 'Annuleren',
            confirmButtonColor: '#ef5350'
        },
        defaults = {
            showCheckboxColumn: true,
            dataTable: {
                searchDelay: 0,
                autoWidth: false,
                stateSave: true,
                dataSrc: 0,
                rowReorder: {
                    selector: 'span.reorder',
                    update: false
                },
                columnDefs: [
                    {
                        orderable: false,
                        width: '20px',
                        targets: 0,
                        className: 'row-checkbox',
                        render: function (data, type, full, meta) {
                            if (!parseInt(data)) {
                                return data;
                            }

                            return '<input type="checkbox"  name="id[]" value="' + $('<div/>').text(data).html() + '" class="form-styled" />';
                        }
                    }, {
                        orderable: false,
                        targets: -1,
                        className: 'row-actions',
                        render: function (data, type, full, meta) {

                            if (!Array.isArray(data)) {
                                return data;
                            }

                            var buttonsList = $('<ul class="icons-list"></ul>');

                            $.each(data, function (i, button) {
                                if (button.buttons) {
                                    var buttonWrapper = $('<li class="dropdown"></li>');
                                    buttonWrapper.append('<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i>' + (button.text) ? button.text : '' + '</a>');

                                    $.each(button.buttons, function (i, button) {
                                        var b = $('<li></li>');
                                        var a = $('<a></a>');

                                        $(a).attr({
                                            'href': button.url,
                                            'data-type': button.type,
                                            'data-id': button.id
                                        });

                                        if (button.icon) {
                                            $(a).append('<i class="icon-' + button.icon + '"></i>');
                                        }

                                        if (button.text) {
                                            $(a).append(button.text);
                                        }

                                        b.append(a);
                                        buttonWrapper.append(b);
                                    });

                                    buttonsList.append(buttonWrapper);
                                } else {
                                    var b = $('<li></li>');
                                    var a = $('<a></a>');

                                    $(a).attr({
                                        'href': button.url,
                                        'data-type': button.type,
                                        'data-id': button.id
                                    });

                                    if (button.hasOwnProperty('override_propagation')) {
                                        $(a).attr({
                                            'data-override-propagation': button.override_propagation
                                        });
                                    }

                                    if (button.hasOwnProperty('label')) {
                                        $(a).attr({
                                            'title': button.label,
                                            'data-popup': 'tooltip'
                                        });
                                    }

                                    if (button.hasOwnProperty('refresh_panel')) {
                                        $(a).attr({
                                            'data-refresh-panel': button.refresh_panel
                                        });
                                    }

                                    if (button.icon) {
                                        $(a).append('<i class="' + button.icon + '"></i>');
                                    }

                                    if (button.attr) {
                                        $.each(button.attr, function (key, value) {
                                            $(a).attr(key, value);
                                        })
                                    }

                                    if (button.text) {
                                        $(a).append(button.text);
                                    }

                                    b.append(a);
                                    buttonsList.append(b);
                                }
                            });

                            return $(buttonsList)[0].outerHTML;
                        }
                    }
                ],
                lengthMenu: [25, 50, 75, 100, 500, 750],
                displayLength: 50,
                dom: '<"datatable-scroll"tr><"datatable-footer"lip>',
                ajax: {
                    'type': 'POST'
                },
                pagingType: 'simple',
                serverSide: true,
                processing: false,
                language: {
                    'lengthMenu': 'Items per pagina: _MENU_',
                    'info': '_START_-_END_ van _TOTAL_',
                    'infoEmpty': '0 - 0 van 0',
                    'paginate': {
                        'next': '<i class="mdi mdi-chevron-right"></i>',
                        'previous': '<i class="mdi mdi-chevron-left"></i>'
                    }
                },
                drawCallback: function () {
                    var api = this.api();
                    var rows = api.rows({page: 'current'}).nodes();
                    var ajaxData = api.ajax.json();
                    var groups = ajaxData.groups;
                    var totalGroups = Object.keys(groups).length;

                    // Check if we received groups
                    if (totalGroups > 0) {
                        var dataTable = this.api().table().node();
                        var lastGroups = [];
                        var firstGroup = Object.keys(groups)[0];

                        $(dataTable).addClass('got_groups total_groups_' + totalGroups);

                        // Hide headers
                        for (var g in groups) {
                            lastGroups[g] = '';
                            $(this.api().column(groups[g]).header()).hide();
                        }

                        var buttonColumn = ($(rows).eq(0).find('td').length - 1);

                        rows.each(function (row, i) {
                            var columns = $(rows).eq(i).find('td');

                            var group_key = 1;
                            for (var g in groups) {
                                var groupElement = $(row).find('td').eq(groups[g]);
                                var groupText = groupElement.text().trim();

                                groupElement.hide();

                                // Check if current row group text is different then last saved group text
                                if (lastGroups[groups[g]] !== groupText) {

                                    // If current group is the same as first group
                                    if (g === firstGroup) {
                                        for (var lg in lastGroups) {
                                            lastGroups[lg] = '';
                                        }
                                    }

                                    // Append group row before current row
                                    groupElement.closest('tr').before(
                                        '<tr class="group group-start group_' + g + ' group_id_' + groups[g] + ' group_key_' + group_key + '"><td colspan="50">  ' + groupText + '</td></tr>'
                                    );

                                    // Set current group text as last group
                                    lastGroups[groups[g]] = groupText;
                                    lastGroups[group_key] = '';
                                }

                                group_key++;
                            }

                            if (typeof ajaxData.data[i][buttonColumn] === 'object' && ajaxData.data[i][buttonColumn].length > 0) {

                                for (var button = 0; button < ajaxData.data[i][buttonColumn].length; ++button) {

                                    if (typeof ajaxData.data[i][buttonColumn][button]['javascript_callback'] === 'string') {
                                        columns.eq(buttonColumn).find('a').eq(button).attr('data-button-nr', button).attr('data-row-nr', i);
                                    }
                                }
                            }
                        });

                        $(dataTable).find('tbody tr:not(.group)').addClass('normal_row');

                        $(dataTable).find('a[data-button-nr]').on('click', function (e) {
                            e.preventDefault();

                            var buttonNr = $(this).data('button-nr');
                            var rowNr = $(this).data('row-nr');

                            var jsCallback = ajaxData.data[rowNr][buttonColumn][buttonNr]['javascript_callback'].split('.');

                            var jsClass = jsCallback[0];
                            var jsFunction = jsCallback[1];
                            window[jsClass][jsFunction](this, ajaxData.data[rowNr], ajaxData.data[rowNr][buttonColumn][buttonNr]);
                        });
                    }

                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');

                    $('.form-styled').uniform({
                        wrapperClass: 'bg-white'
                    });

                    // Enable Select2 select for the length option
                    $('.dataTables_length select').select2({
                        minimumResultsForSearch: '-1'
                    });

                    $('[data-popup="tooltip"]').tooltip();
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            }
        };


    // The actual plugin constructor
    function Crud(element, options) {
        this.options = $.extend(true, {}, options, defaults);

        if (!this.options.dataTable.ajax.url && !this.options.dataTable.data) {
            console.error('Missing ajax parameter or json data');

            return false;
        }

        this.element = element;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Crud.prototype = {
        init: function () {
            this.render(this.element, this.options);
            this.bindEvents(this.element, this.options);

            $.fn.dataTableExt.sErrMode = 'throw';
        },
        removeSelectedItems: function () {
            $(this.element).find('tbody td input[type=checkbox]:checked').closest('tr[role="row"]').remove();
            $('.datatable-header-bulk-actions').removeClass('active');
        },
        render: function (element, options) {
            var self = this;

            var reorderable = !!$(element).attr('data-reorder-url');

            if(false !== reorderable) {
                options.dataTable.columnDefs.push({
                    orderable: false,
                    width: '10px',
                    targets: 1,
                    className: 'row-reorder',
                    render: function () {
                        return '<span class="reorder"><i class="mdi mdi-reorder-horizontal"></i></span>';
                    }
                });
            }

            var dataTable = $(element).DataTable(options.dataTable).on('init.dt', function () {
                self.parseState(element);
            }).on('processing.dt', function (e, settings, processing) {
                self.processing(element, processing);
            });

            this.setDataTable(element, dataTable);
        },
        processing: function (element, state) {
            $(element).closest('.datatable').find('.datatable-header-processing').toggleClass('active', state);
        },
        parseState: function (element) {
            var dataTable = this.getDataTable(element);
            var state = dataTable.state();

            // Set datatable save state in the custom search input
            $(element).closest('.datatable').find('input[name="datatable[search]"]').val(state.search.search);
        },
        bindEvents: function (element, options) {
            var self = this;
            var dataTable = this.getDataTable(element);
            var dataTableWrapper = $(element).closest('.datatable');

            if ($('.tabbable').length) {
                $(element).on('click', 'a[data-type="edit"]', function (e) {
                    e.preventDefault();

                    new Content($(this).attr('href'));
                });
            }

            $(element).on('click', 'a[data-trigger="delete"]', function (e) {
                e.preventDefault();

                var elm = $(this);

                confirm_delete_modal_options.html = 'Weet je het zeker dat dit item verwijderd mag worden?';

                swal(confirm_delete_modal_options).then(function () {
                    e.preventDefault();

                    $.ajax({
                        url: $(elm).attr('href'),
                        success: function (data) {
                            if (data.type === 'success') {
                                $(element).dataTable().api().ajax.reload();
                            } else {
                                swal({
                                    title: 'Error',
                                    text: data.response,
                                    type: 'warning',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Sluiten'
                                });
                            }
                        }
                    });
                }, function (dismiss) {
                });
            });

            // Handle click on "Select all" control
            $(element).on('click', '#select_all', function () {
                // Get all rows with search applied
                var rows = dataTable.rows({'search': 'applied'}).nodes();

                $(rows).toggleClass('selected');

                // Check/uncheck checkboxes for all rows in the table
                $('input[type="checkbox"]', rows).prop('checked', this.checked).trigger('change');
            });

            $(element).on('dblclick', 'tbody tr', function (e) {
                var target = e.target.tagName;
                var targetParent = e.target.parentElement.tagName;

                if ([target.toLowerCase(), targetParent.toLowerCase()].indexOf('a') === -1) {

                    var viewButton = $(e.target.parentElement).find('td.row-actions a[data-type="view"]');

                    if (viewButton.length === 1) {
                        self.simulateClick(viewButton);
                    } else {
                        var editButton = $(e.target.parentElement).find('td.row-actions a[data-type="edit"]');
                        if (editButton.length === 1) {
                            self.simulateClick(editButton);
                        }

                    }
                }
            });

            $(element).on('change', 'tbody td input[type=checkbox]', function () {
                $(this).parents('tr').toggleClass('selected', $(this).is(':checked'));

                $.uniform.update();

                var totalCheckboxesChecked = $('tbody td input[type=checkbox]:checked', $(this).closest('.datatable')).length;
                var totalCheckboxesCountLabel = totalCheckboxesChecked + ' item' + ((totalCheckboxesChecked !== 1) ? 's' : '') + ' geselecteerd';
                var dataTables_actions_list = $('#dataTables_actions_list');

                $('.datatable-header-bulk-actions')
                .toggleClass('active', (totalCheckboxesChecked > 0))
                .find('.count-selected-items').html(totalCheckboxesCountLabel);

                // Enable action list when one ore more items are checked
                dataTables_actions_list.attr('disabled', !($('tbody td input[type=checkbox]:checked', $(element)).length > 0));

                // Add disabled property to options which do not support multiple items being checked
                var disabledState = ($('tbody td input[type=checkbox]:checked', $(element)).length > 1);

                $('#dataTables_actions_list option:not([data-multiple])').attr('disabled', disabledState).prop('disabled', disabledState);

                /**
                 * @todo re-init select 2: temp work-a-round due to bug in select2 4.0.0
                 */
                dataTables_actions_list.select2(select2Defaults);
            });

            $(element).on('click', '> tbody > tr > td.row-actions a', function (e) {
                if ($(this).attr('data-type') !== 'preview') {
                    if (typeof $(this).closest('.panel').attr('data-override-propagation') === 'undefined'
                        && typeof $(this).attr('data-override-propagation') === 'undefined' && $(this).attr('data-type') !== 'manually-process-order') {
                        e.stopPropagation();
                    } else {
                        e.preventDefault();
                    }

                    if ($(this).attr('data-type') === 'manually-process-order') {
                        new ManuallyProcessOrder($(this).attr('href'));
                    } else if ($(this).attr('data-type') === 'get-order') {
                        e.stopPropagation();
                        e.preventDefault();

                        var href = $(this).attr('href');
                        var orderId = $(this).data('id');

                        new Order(href, orderId);

                    } else if ($(this).closest('.view-action').length && $(this).attr('data-type') !== 'delete' && $(this).attr('data-type') !== 'view') {
                        e.preventDefault();

                        var options = [];
                        if ($(this).attr('data-refresh-panel')) {
                            var self = $(this);

                            options['hideCallbackOnSubmit'] = function () {
                                loadPanel($('.panel[data-panel-id="' + self.attr('data-refresh-panel') + '"]'));
                            };
                        }

                        new Content($(this).attr('href'), null, options);
                    }
                }
            });

            $(element).on('click', 'tbody tr a[data-type="delete"]', function (e) {
                e.preventDefault();

                var elm = $(this);

                confirm_delete_modal_options.html = 'Weet je het zeker dat dit item verwijderd mag worden?';

                swal(confirm_delete_modal_options).then(function () {
                    e.preventDefault();

                    $.ajax({
                        url: $(elm).attr('href'),
                        type: 'POST',
                        data: {
                            '_method': 'DELETE'
                        },
                        success: function (result) {
                            document.location.reload();
                        }
                    });

                    return false;
                }, function (dismiss) {

                });
            });

            $('.datatable-header-bulk-actions a[data-bulk-action="true"], .datatable-header-button a[data-action="pre_confirm"]').on('click', function (e) {
                e.preventDefault();

                var action = self.convertOptionToAction($(this));

                self.triggerAction(element, action);
            });

            // Lightbox
            $('[data-popup="lightbox"]', $(element)).fancybox({
                padding: 3
            });


            var searchBox = $(dataTableWrapper).find('[name="datatable[search]"]');
            var searchDelay;

            $(searchBox).on('keyup', function (e) {
                if (searchDelay) {
                    if (typeof(dataTable.settings()[0].jqXHR) !== 'undefined') {
                        dataTable.settings()[0].jqXHR.abort();
                    }

                    clearTimeout(searchDelay);
                }

                var searchValue = $(this).val();
                var defaultDelay = 750;

                // Remove delay when user hits the "enter" key
                if (e.keyCode === 13) {
                    defaultDelay = 0;
                }

                searchDelay = setTimeout(function () {
                    dataTable.search(searchValue).draw();
                }, defaultDelay);
            });

            $(window).on('scroll', function () {
                $('#datatable_fab_menu').toggle(!$('.datatable-header-button').inView());
            });
        },
        simulateClick: function (elem) {
            var evt = document.createEvent('MouseEvents');
            evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            elem[0].dispatchEvent(evt);
        },
        triggerAction: function (element, action) {
            var self = this;

            var ids = [];
            var sw_modal;
            var route = action.route_generated;

            $('input:checkbox[name="id[]"]:checked').each(function () {
                ids.push($(this).val());
            });

            Cms.prototype.initNotifications();

            if (('pre_confirm' in action)) {
                var modal_options = $.extend({}, confirm_delete_modal_options, action.pre_confirm);

                sw_modal = swal(modal_options).then(function (additionalPreConfirmData) {
                    self.executeAction(element, action, route, ids, additionalPreConfirmData);
                }, function (dismiss) {
                    self.clearBulkAction(element);

                    // Sweet Alert verbergen
                    swal.close();
                });
            } else {
                this.executeAction(element, action, route, ids);
            }
        },
        executeAction: function (element, action, route, ids, additionalPreConfirmData) {
            var self = this;

            switch ('xhr' in action) {
                case true:
                    $.ajax({
                        url: route,
                        method: action.method,
                        beforeSend: function () {
                            var text = `De geselecteerde actie "${action.label}", wordt uitgevoerd voor ${ids.length} item${((ids.length !== 1) ? 's' : '')}.`;
                            if(ids.length === 0){
                                text = 'De geselecteerde actie wordt uitgevoerd';
                            }
                            sw_modal = swal({
                                type: 'info',
                                title: 'Een moment geduld',
                                text: text,
                                allowEscapeKey: false,
                                showConfirmButton: false
                            });
                        },
                        data: {
                            ids: ids,
                            data: additionalPreConfirmData
                        }
                    }).done(function (response) {


                    }).error(function () {
                        Cms.prototype.showNotification({
                            type: 'error',
                            text: 'Er is een fout opgetreden'
                        });
                    }).always(function (response) {
                        // Sweet Alert verbergen
                        swal.close();

                        if (response && 'message' in response) {
                            if(typeof(response.message.callback) !== 'undefined' && response.message.callback === 'CrudRefresh'){
                                $(self.element).DataTable().draw();
                            }
                            if ('message_type' in response.message && response.message.message_type === 'modal') {
                                var modal_options = {
                                    type: response.message.type,
                                    html: response.message.text,
                                    title: response.message.title,
                                    allowEscapeKey: false,
                                    allowOutsideClick: ('disable_timer' in response.message),
                                    showConfirmButton: ('disable_timer' in response.message)
                                };

                                if (!('disable_timer' in response.message)) {
                                    modal_options.timer = 2000;
                                }

                                swal(modal_options).catch(function (result) {
                                    if (('callback' in response.message)) {
                                        if (typeof (window[response.message.callback]) === 'function') {
                                            window['CrudRemoveSelectedItems']();
                                        }
                                    }
                                });
                            } else {
                                Cms.prototype.showNotification(response.message);

                                if (('callback' in response.message)) {
                                    if (typeof (window[response.message.callback]) === 'function') {
                                        window['CrudRemoveSelectedItems']();
                                    }
                                }
                            }

                            if(typeof(response.message.redirect) !== 'undefined'){
                                location.href = response.message.redirect;
                            }
                        }

                        self.clearBulkAction(element);
                    });

                    break;
                case false:
                    var qs = [];

                    for (var i = 0; i < ids.length; i++) {
                        qs.push('ids[]=' + encodeURIComponent(ids[i]));
                    }

                    qs = qs.join('&');

                    window.location.href = route + '?' + qs;

                    break;
            }
        },
        clearBulkAction: function (element) {
            // Actie dropdown weer terugzetten op niets geselecteerd.
            $(element).find('#dataTables_actions_list').select2('val', '');
        },
        convertOptionToAction: function (option) {
            var action = {};

            var data = $(option).data();

            for (var key in data) {
                if (key !== 'data') {
                    action[key] = data[key];
                }
            }

            action.route_generated = $(option).attr('href');

            return action;
        },
        getDataTable: function (element) {
            return $(element).data('DataTable');
        },

        setDataTable: function (element, dataTable) {
            $(element).data('DataTable', dataTable);
        }
    };

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName,
                    new Crud(this, options));
            }
        });
    };

})(jQuery, window, document);

$(document).ready(function () {
    initView();
});

$(document).on('click', '[data-action="open-content"]', function (e) {
    e.preventDefault();

    openContent($(this));
});

function initView() {
    var query_string = '';

    if (window.location.search && window.location.search.length > 0) {
        query_string = window.location.search;
    }

    initializePanels();

    initializeDatatables();

    $('.view-action')
    .on('click', 'a[data-type="add"]', function (e) {
        e.preventDefault();

        var view = $(this).closest('.view-action')
        var entityId = view.attr('data-entity-id');
        var entityName = view.attr('data-entity-class') ? view.attr('data-entity-class') : 'entity';
        var url = $(this).attr('href') + '?entity=' + entityId + '&entityClass=' + entityName;

        $(this).attr('data-url', url);

        openContent($(this));
    });

    $('body')
        .on('click', '.datatable a[data-type="reorder"]', function (e) {
            e.preventDefault();

            var elm = $(this);
            var wrapperElm = $(e.currentTarget).closest('.datatable');
            var table = wrapperElm.find('table.dataTable');

            wrapperElm.addClass('datatable-reorder');

            if (table.length > 0) {
                var dataTable = table.DataTable();

                // Reorder table on position
                dataTable.clear().order([1, 'asc']).draw();
            }

            elm
            .attr('data-type', 'reorder-save')
            .text(elm.data('save-text'))
            ;
        })
        .on('click', '.datatable a[data-type="reorder-save"]', function (e) {
            e.preventDefault();

            var elm = $(this);
            var wrapperElm = $(e.currentTarget).closest('.datatable');
            var table = wrapperElm.find('table.dataTable');

            if (table.length > 0) {
                var orderIndexes = {};
                table.find('tbody tr').each(function (index) {
                    var id = $(this).find('.row-checkbox input').val();

                    if(typeof id !== "undefined") {
                        orderIndexes[id] = parseInt(index);
                    }
                });

                if(Object.keys(orderIndexes).length === 0) {
                    swal({
                        'title': 'Geen items om te verwerken'
                    });
                    return;
                }

                elm.prop('disabled', true);

                $.ajax({
                    'url': table.data('reorder-url'),
                    'method': 'POST',
                    'data': {
                        reorder: orderIndexes
                    }
                })
                .done(function () {
                    elm
                    .attr('data-type', 'reorder')
                    .text(elm.data('default-text'))
                    ;

                    wrapperElm.removeClass('datatable-reorder');
                })
                .fail(function () {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Bijwerken van volgorde mislukt.',
                    });
                })
                .always(function () {
                    elm.prop('disabled', false);
                })
                ;
            }
        })
    ;
}

function initializeDatatables() {
    var datatableSelector = $('table[data-dataTables]');

    datatableSelector.each(function () {
        $(this).Crud({
            dataTable: {
                ajax: {
                    url: $(this).data('data-url'),
                    type: 'POST',
                    data: function ( d ) {
                        var datatable = datatableSelector.closest('.datatable');
                        var filterForm = datatable.find('.datatable-filters form');

                        filterForm.on('submit', function(e) {
                            e.preventDefault();
                        });

                        var filterActive = datatable.find('> .datatable-header [data-action="open-filter"]').hasClass('active');

                        if(filterActive) {
                            d.filters = filterForm.serialize();
                        }
                    }
                },
                order: JSON.parse($(this).attr('data-order'))
            }
        });
    });

    $('body')
        .on('click', 'a[data-action="open-filter"]', function (e) {
            e.preventDefault();

            var elm = $(this);
            var panelElm = elm.closest('.panel');

            elm.toggleClass('active');
            panelElm.toggleClass('filter-expanded');

            var searchFieldInputElm = elm.closest('.datatable').find('input[name="datatable[search]"]');

            // Simulate search request.
            searchFieldInputElm.trigger('keyup');
        })
        .on('click', 'a[data-action="open-filter-settings"]', function (e) {
            e.preventDefault();

            var buttonElm = $(this);

            new DataSetFilterSettingsModal({url: buttonElm.attr('href')}).open();
        })
        .on('blur change keyup', '.datatable-filters input, .datatable-filters select', function() {
            var searchFieldInputElm = datatableSelector.closest('.datatable').find('input[name="datatable[search]"]');

            // Simulate search request.
            searchFieldInputElm.trigger('keyup');
        })
        .on('click', '.datatable-filters .clear-filters a', function(e) {
            e.preventDefault();

            var elm = $(this);
            var form = elm.closest('.datatable-filters').find('form');

            form.trigger('reset');
            form.find('.select2').trigger('change');
            form.find('.range-slider').slider('refresh');
        })
        .on('click', '.datatable-filters .collapse-filter', function(e) {
            e.preventDefault();

            var elm = $(this);
            var datatable = elm.closest('.datatable');

            datatable.find('[data-action="open-filter"]').trigger('click');
        })
    ;
}

function loadPanel(panel) {
    panel.find('.panel-body').html('');
    panel.addClass('loading');

    $.ajax({
        url: panel.attr('data-href')
    }).done(function (response) {
        panel.find('.panel-body').html(response);

        var tabs = $('.tabbable', '.panel[data-panel-id]');
        var firstTab = tabs.find('.panel-tab:first');

        if (window.location.hash !== '') {
            var hash = window.location.hash;
            var tab = $('a[href="' + hash + '"]');

            if (!tab) {
                history.pushState(null, null, document.location.pathname);
                firstTab.trigger('click');
            } else {
                tab.trigger('click');
            }
        } else {
            firstTab.trigger('click');
        }

        initializeDatatables();
    }).error(function () {
        swal('Foutmelding', 'Er is iets foutgegaan met het laden van het paneel', 'error');
    }).always(function () {
        panel.removeClass('loading');
    });
}

function initializePanels() {
    $('.panel[data-panel-id]').each(function () {
        var $panel = $(this);

        loadPanel($panel);
    });

    $('.panel').on('click', '.tabbable a.panel-tab', function () {
        var $this = $(this);
        var $tabs = $this.closest('.tabbable');
        var $panelTabContent = $('.panel-tab-content', $tabs);

        history.pushState(null, null, $(this).attr('href'));

        $panelTabContent
        .removeClass('template')
        .removeClass('datatable')
        .addClass('loading')
        .empty();

        $.ajax({
            url: $this.attr('data-href'),
            method: 'GET'
        }).done(function (response) {
            $panelTabContent.addClass($this.attr('data-tab-type')).html(response);

            initializeDatatables();
        }).always(function () {
            $panelTabContent.removeClass('loading');
        });
    });

    // navigate to a tab when the history changes
    window.addEventListener('popstate', function (e) {

        var activeTab = $('[href="' + location.hash + '"]');

        if (activeTab.length) {
            activeTab.trigger('click');
        }
    });
}

function openContent(obj) {
    var scripts = [];
    var options = {};
    var scriptsAttr = obj.attr('data-scripts');

    if (scriptsAttr) {
        scripts = JSON.parse(scriptsAttr);
    }

    var url = typeof obj.attr('data-url') !== typeof undefined ? obj.attr('data-url') : obj.attr('href');

    if (obj.attr('data-refresh-panel')) {
        options['hideCallbackOnSubmit'] = function () {
            loadPanel($('.panel[data-panel-id="' + obj.attr('data-refresh-panel') + '"]'));
        };
    }

    if (obj.attr('data-hide-callback')) {
        if (typeof window[obj.attr('data-hide-callback')] !== 'undefined') {
            options['hideCallback'] = window[obj.attr('data-hide-callback')];
        }
    }

    if(obj.attr('data-contentwindow-width')) {
        options['width'] = parseInt(obj.attr('data-contentwindow-width')) + 'px';
    }

    new Content(url, scripts, options);
}

function Content(url, scripts, options) {

    var contentOptions = {
        width: window.innerWidth * 0.9 + 'px',
        toolbarButtons: [
            {
                position: 'right',
                action: 'submit-content-form',
                label: 'Opslaan',
                icon: 'fa fa-save',
                classNames: 'btn-primary'
            }
        ],
        scripts: scripts
    };

    if (options) {
        contentOptions = Object.assign(contentOptions, options);
    }

    this.contentWindow = new ContentWindow(contentOptions);

    this.open(url, scripts);
}

Content.prototype.open = function (url, scripts) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('scripts', scripts);
    this.contentWindow.loading();

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);

        self.init(url);
    }).fail(function () {

    });
};

Content.prototype.init = function (url) {
    var contentWindow = this.contentWindow.elm;
    $(contentWindow).find('form').attr('action', url);

    if (typeof initializeUpload !== 'undefined') {
        initializeUpload();
    }

    this.initTooltips();

    $('a[data-action="submit-content-form"]').on('click', function () {
        for (var instanceName in CKEDITOR.instances)
            CKEDITOR.instances[instanceName].updateElement();
        $(contentWindow).find('form').submit();
    });

    initializeDatatables();
};

Content.prototype.initTooltips = function () {
    var contentWindow = this.contentWindow.elm;
    $('[data-toggle="tooltip"]', contentWindow).tooltip();
};

function CrudRemoveSelectedItems() {
    $('.datatable').find('table.dataTable').data('Crud').removeSelectedItems();
}
