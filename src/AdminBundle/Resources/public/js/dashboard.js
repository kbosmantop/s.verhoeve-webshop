Dashboard = function() {
    var self = this;
    this.myChart = {};

    this.bindEvents = function() {
        $('#recentOrderPanel').on('click', 'button[data-event="get-order"]', function(e) {
            e.stopPropagation();
            e.preventDefault();

            var href = $(this).data('href');
            var orderId = $(this).data('order-id');

            new Order(href, orderId);
        });

        $('.dashboard-statistics select').on('change', function (e) {
            var elm = $(e.currentTarget);
            var panel = elm.closest('.panel');
            var id = elm.attr('id');

            $.ajax({
                url: elm.data('url'),
                method: 'POST',
                data: {
                    siteId: elm.val()
                }
            }).done(function (response) {
                this.buildChart(id, panel, response);
            }.bind(this));
        }.bind(this)).trigger('change');
    };

    this.buildChart = function( id, panel, data ) {
        if(panel.find('.panel-body canvas')[0]){
            var ctx = panel.find('.panel-body canvas')[0].getContext('2d');

            if(this.myChart[id]) {
                this.myChart[id].destroy();
                delete this.myChart[id];
            }

            this.myChart[id] = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: data.chartLabels,
                    datasets: [{
                        label: 'Omzet in euro\'s',
                        data: data.chartValues
                    }]
                },
                options: {
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem) {
                                return ['Omzet in euro\'s: ' + tooltipItem.yLabel.toFixed(2), 'Aantal orders: ' + data.tooltips[tooltipItem.index]];
                            }
                        },
                        displayColors: false,
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            var price = '' + data.total;
            var max = '' + data.max;

            panel.find('.revenue-total').text(price.replace('.', ','));
            panel.find('.revenue-count').text(data.count);
            panel.find('.revenue-max').text(max.replace('.', ','));
        }
    };

    this.init = function() {
        this.bindEvents();
    };

    this.init();
};

$(function() {
    new Dashboard();
});