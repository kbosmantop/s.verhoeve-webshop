function CollectionCalcVat() {
    this.collectionElm = null;
    this.priceVatClass = '.price-vat';
    this.priceInclClass = '.price-incl';
    this.priceExclClass = '.price-excl';

    this.init = function (collectionElm) {
        this.collectionElm = collectionElm;
        this.bindEvents();
    };

    /**
     * Calculate excl. price based on incl. price and vat.
     *
     * @param priceIncl
     * @param vat
     * @returns {number}
     */
    this.calcPriceExcl = function(priceIncl, vat) {
        return this.formatPrice((priceIncl / (100 + vat)) * 100, 3);
    };

    /**
     * Format price into a fixed format of N decimals.
     * @param price
     * @param decimals
     * @returns {number}
     */
    this.formatPrice = function(price, decimals, asFloat) {
        if (isNaN(price)) {
            price = 0.000;
        }

        price = price.toFixed(decimals);

        if(!!asFloat) {
            return parseFloat(price);
        }

        return price.replace('.', ',');
    };

    /**
     * Hide column based on field class inside the column.
     *
     * @param fieldElmClass
     */
    this.hideColumn = function(fieldElmClass) {
        var colIndex = $('.collection-item ' + fieldElmClass, this.collectionElm).closest('td').index();

        // Hide column with priceExl and Vat.
        $('th:nth-child(' + (colIndex + 1) + '), td:nth-child(' + (colIndex + 1) + ')', this.collectionElm).hide();
    };

    this.bindPriceFieldEvents = function() {
        this.collectionElm
            .off('keyup change', '.collection-item ' + this.priceInclClass)
            .on('keyup change', '.collection-item ' + this.priceInclClass, function (e) {
                var priceInclFieldElm = $(e.currentTarget);
                var collectionItemElm = priceInclFieldElm.closest('.collection-item');
                var priceExclFieldElm = collectionItemElm.find(this.priceExclClass);
                var priceVatFieldElm = collectionItemElm.find(this.priceVatClass);
                var priceIncl = parseFloat(priceInclFieldElm.val().replace(',', '.'));
                var vat = parseFloat(priceVatFieldElm.val());

                priceExclFieldElm.val(this.calcPriceExcl(priceIncl, vat));
            }.bind(this))
            .off('blur', '.collection-item ' + this.priceInclClass)
            .on('blur', '.collection-item ' + this.priceInclClass, function (e) {
                var priceInclFieldElm = $(e.currentTarget);
                var priceIncl = parseFloat(priceInclFieldElm.val().replace(',', '.'));

                priceInclFieldElm.val(this.formatPrice(priceIncl, 2));
            }.bind(this))
        ;
    };

    this.bindEvents = function () {
        this.hideColumn(this.priceExclClass);
        this.hideColumn(this.priceVatClass);

        this.bindPriceFieldEvents();
    };
}