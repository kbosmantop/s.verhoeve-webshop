firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

requestPermission();

function requestPermission() {
    messaging.requestPermission()
    .then(function () {
        messaging.getToken().then(function (token) {
            $.get('/admin/firebase', {
                token: token
            });
        });
    })
    .catch(function (err) {
        console.log(err);
    })
    ;
}

messaging.onMessage(function (payload) {
    payload.data.target = JSON.parse(CryptoJS.AES.decrypt(payload.data.target, 'QU^MMWmR4I5KusYmtAo7NQJxejHRqKr9', {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));

    var options = {
        body: payload.data.body,
        icon: payload.data.icon,
    }

    var notification = new Notification(payload.data.title, options);

    notification.onclick = function (event) {
        event.preventDefault();

        window.open(payload.data.target, '_blank');

        event.srcElement.close();
    }
});
