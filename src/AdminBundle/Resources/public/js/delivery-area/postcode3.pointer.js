function PostcodePointer(parent, data) {
    this.parent = parent;
    this.clickTimeout = null;
    this.marker = null;
    this.infoWindow = null;
    this.supplierRowHtml = '';

    for (var key in data) {
        this[key] = data[key];
    }
}

PostcodePointer.prototype.show = function () {
    if (!this.polygon) {
        this.render();
    }
};

PostcodePointer.prototype.toggleInfoWindow = function (show) {
    if (show) {
        if (this.infoWindow !== null) {
            return;
        }

        this.infoWindow = new google.maps.InfoWindow({
            content: this.name
        });

        this.infoWindow.open(this.parent.map, this.marker);

        this.parent.currentInfoWindow = this.infoWindow;

    } else {
        // Remove existing info window
        if (this.parent.currentInfoWindow !== null) {
            google.maps.event.clearInstanceListeners(this.parent.currentInfoWindow);  // just in case handlers continue to stick around
            this.parent.currentInfoWindow.close();
            this.parent.currentInfoWindow = null;
            this.infoWindow = null;
        }
    }
};

PostcodePointer.prototype.handleClickEvent = function (eventName) {
    var self = this;

    clearTimeout(this.clickTimeout);
    this.clickTimeout = setTimeout(function () {

        self.clickTimeout = null;

        self.parent.map.setZoom(12);

        if (self.marker) {
            self.parent.map.panTo(self.marker.position);
        }

        if (eventName === 'click') {
            self.showDeliveryArea(false);

            if (self.parent.viewModus === 'select-supplier') {
                self.renderSupplierRow().then(function (html) {
                    self.supplierRowHtml = html;
                    html = $(html);

                    self.selectSupplier();

                    // Remove unused fields
                    $('.col-internal-remark', html).remove();
                    $('.col-delivery-costs', html).remove();
                    $('.col-actions', html).remove();
                    $('.col-internal-company-remarks button', html).remove();

                    $('.selected-supplier tbody').html(html);

                });
            }
        } else if (eventName === 'dblclick') {
            if (self.parent.viewModus === 'default') {
                self.showDeliveryArea(true, true);
                self.open();
            }
        }
    }, 250);
};

PostcodePointer.prototype.selectSupplier = function () {
    var self = this;
    var parentWindow = window.parent;

    if (parentWindow && typeof parentWindow.alternativeSupplierModal !== 'undefined') {
        parentWindow.alternativeSupplierModal.selectedSupplier = self.company;
        parentWindow.alternativeSupplierModal.supplierRowHtml = self.supplierRowHtml;
    }
};

PostcodePointer.prototype.clear = function () {
    if (this.marker) {
        this.marker.setMap(null);
        this.marker = null;
    }
};

PostcodePointer.prototype.render = function () {
    var self = this;

    this.marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.lat, this.lng),
        map: this.parent.map,
        title: this.name,
        icon: {
            url: this.icon,
            anchor: new google.maps.Point(35, 35),
            scaledSize: new google.maps.Size(35, 35),
        },
        opacity: 1,
        optimized: false,
        zIndex: ((this.zIndex) ? this.zIndex : 1000)
    });

    google.maps.event.addListener(this.marker, 'click', function (e) {
        self.handleClickEvent('click');
    });

    google.maps.event.addListener(this.marker, 'dblclick', function (e) {
        self.handleClickEvent('dblclick');
    });

    google.maps.event.addListener(this.marker, 'mouseover', function (e) {
        self.toggleInfoWindow(true);
    });

    google.maps.event.addListener(this.marker, 'mouseout', function (e) {
        self.toggleInfoWindow(false);
    });
};

PostcodePointer.prototype.showDeliveryArea = function (flatten, force) {
    var self = this;
    var dataset = this.parent.getDataset();

    if (this.id) {
        self.parent.suppliers = [this.id];
        self.parent.forceSupplierFilterUpdate = true;

        self.parent.setDataset(dataset, flatten, !!force);
    }
};

PostcodePointer.prototype.open = function () {
    var self = this;

    if (this.parent.postcodeSelector) {
        this.parent.postcodeSelector.close();
    }

    this.parent.currentPointer = this;

    this.infoWindow = new SupplierInfoWindow({
        width: '50%',
        parent: this.parent,
        pointer: this,
        entity: self
    });

    if (this.id) {
        var url = this.parent.pageUrl + 'get-supplier-info/' + this.id;

        this.infoWindow.open(url, {
            supplierGroup: self.parent.supplierGroup
        });
    } else {
        this.infoWindow.setHtml('\
		    <b>' + this.name + '</b><br />\
		    ' + this.address + '<br />\
		    ' + this.postcode + ' ' + this.city + '<br />\
		    <br />\
		    ' + this.phone + '<br />\
		');
    }
};

PostcodePointer.prototype.renderSupplierRow = function () {
    var self = this;
    var url = '/admin/bestelling-order/%order%/%uuid%/ophalen-alternatieve-leverancier';
    var orderId = parseInt(getQueryParam('order'));
    var hasOrerId = !isNaN(orderId) && orderId > 0;

    return new Promise(function(resolve, reject) {
        if(self.company === null) {
            reject('Geen leverancier geselecteerd');
        }

        if(!hasOrerId) {
            reject('Order niet gevonden');
        }

        url = url.replace('%order%', orderId);
        url = url.replace('%uuid%', self.company.uuid);

        $.ajax({
            url: url,
            method: 'GET'
        }).then(resolve, reject);
    });
};