function RegionInfoWindow(options) {

    var self = this;

    if (typeof options !== 'undefined' && typeof options.parent !== 'undefined') {
        this.parent = options.parent;
    }

    if (typeof options !== 'undefined' && typeof options.entity !== 'undefined') {
        this.entity = options.entity;
    }

    options = $.extend(true, {}, options, {
        width: '600px',
        zIndex: 3000,
        closeButtonLabel: 'Venster sluiten',
        hideBackground: true,
        hideCallback: function () {

            // Reset current content window state.
            if(self.parent.currentContentWindow && self.parent.currentContentWindow === self.contentWindow) {
                self.parent.currentContentWindow = null;
            }

            // Update data when modals fully closed...
            self.parent.setDataset(self.parent.dataset, null, true);
        },
        afterErrorCallback: function() {
            self.bindEvents();
        },
        toolbarButtons: [
            {
                id: 'save_edit_region',
                position: 'right',
                label: 'Opslaan en sluiten',
                order: 1,
                classNames: 'btn btn-primary',
                action: 'save-edit-regions'
            }
        ]
    });

    // Don't send this parent to the ContentWindow.
    options.parent = null;

    var currentContentWindow = this.parent.currentContentWindow;

    // Check if there is already a content window.
    if(currentContentWindow) {
        if(
            typeof currentContentWindow.referenceId !== 'undefined'
            && currentContentWindow.referenceId === 'RegionInfoWindow'
        ) {
            this.contentWindow = currentContentWindow;
        } else {
            currentContentWindow.hide();
        }
    }

    if(!this.contentWindow) {
        this.contentWindow = new ContentWindow(options);
        this.contentWindow.referenceId = 'RegionInfoWindow';
    }
}

RegionInfoWindow.prototype.open = function (url, data) {

    var self = this;

    this.url = url;
    this.data = data;

    if (typeof self.contentWindow === 'undefined') {
        return false;
    }

    this.parent.currentContentWindow = self.contentWindow;

    self.contentWindow.empty().show();
    self.contentWindow.loading();

    if(this.parent.xhr) {
        this.parent.xhr.abort();
    }

    this.parent.xhr = $.ajax({
        url: url,
        data: data,
        success: function (res) {
            self.contentWindow.setHtml(res);
            self.bindEvents();
        },
        dataType: 'html'
    });
};

RegionInfoWindow.prototype.bindSelect2 = function (elm) {

    var self = this;

    var formatLabel = function (label) {

        var pattern = '%%(.*?)%%';
        var regex = new RegExp(pattern, 'g');
        var match = regex.exec(label);

        // @todo: optimize color assign
        var color = '#000000';
        if (match && typeof match[1] !== 'undefined') {
            if (match[1] === 'Bakker' || match[1] === 'Topbloemen') {
                color = self.parent.connectorColors.bakker;
            } else if (match[1] === 'BakkerMail') {
                color = self.parent.connectorColors.bakkerMail;
            }
        }

        label = label.replace(regex, '<i class="fa fa-home" style="color: ' + color + '"></i>');

        return label;
    };

    elm.select2({
        'ajax': {
            url: self.url,
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            data: function (params) {
                return {
                    name: elm.data('name'),
                    search: params.term,
                    supplierGroup: self.parent.supplierGroup
                };
            }
        },
        escapeMarkup: function (markup) {
            return formatLabel(markup);
        },
        templateResult: function (data) {

            if (typeof data === 'object') {
                data.text = formatLabel(data.text);
                return data.text;
            }

            return data.text;
        }
    });

};

RegionInfoWindow.prototype.close = function (removeElm) {
    var self = this;

    self.contentWindow.hide(removeElm);
};

RegionInfoWindow.prototype.bindEvents = function () {
    var self = this;
    var elm = $(self.contentWindow.elm);

    var regionCollectionCalcVat = new RegionCollectionCalcVat();
    regionCollectionCalcVat.init(elm.find('form'));

    $(elm).on('click', '.btn-add', function (e) {

        $('select.select2', $(self.contentWindow.elm)).each(function () {
            var selectField = $(this);
            self.bindSelect2(selectField);
        });

        regionCollectionCalcVat.bindEvents();
    });

    elm.find('select.select2').each(function () {
        var selectField = $(this);
        self.bindSelect2(selectField);
    });

    $(elm).off('click', '#save_edit_region').on('click', '#save_edit_region', function (e) {
        elm.find('form').submit();
    });
};
