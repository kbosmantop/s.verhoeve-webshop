function PostcodeRegion(parent, data) {
    this.parent = parent;

    this.color = '#FFFFFF';

    this.label = null;
    this.polygon = null;
    this.bounds = [];

    for (var key in data) {
        this[key] = data[key];
    }
};

PostcodeRegion.prototype.getCountry = function () {
    return this.country;
};

PostcodeRegion.prototype.getPostcode = function () {
    return this.postcode;
};

PostcodeRegion.prototype.clear = function (resetColor) {
    if(this.polygon) {
        this.polygon.setMap(null);
        this.polygon = null;
    }

    if(resetColor === true) {
        this.color = '#FFFFFF';
    }

    if(this.label) {
        this.label.close();
    }
};

PostcodeRegion.prototype.setColor = function (color) {
    this.color = color;

    if (this.polygon) {
        this.polygon.setOptions({
            fillColor: color,
        });
    }
};

PostcodeRegion.prototype.show = function () {
    if (!this.polygon) {
        this.render();
    }
};

PostcodeRegion.prototype.render = function () {
    var self = this;
    var coords = self.getPolygonCoords();

    this.label = this.renderLabel();

    this.polygon = new google.maps.Polygon({
        paths: coords,
        strokeColor: '#000000',
        strokeOpacity: 0.3,
        strokeWeight: 2,
        fillColor: this.color,
        fillOpacity: 0.25, //0.35
        data: {
            country: this.country,
            postcode: this.postcode,
            label: this.label
        }
    });

    if(self.parent.viewModus === 'default') {
        google.maps.event.addListener(this.polygon, 'click', function (event) {
            if (self.parent.postcodeSelector) {
                self.parent.postcodeSelector.toggle(self);
            } else {
                self.highlight();
                self.open();
            }
        });
    }

    this.polygon.setMap(this.parent.map);
};

PostcodeRegion.prototype.getPolygonCoords = function() {
    var coords = [];
    var polygons = [];

    if (this.geometry.substring(0, 7) === 'POLYGON') {
        polygons.push(this.geometry.slice(7));
    } else if (this.geometry.substring(0, 12) === 'MULTIPOLYGON') {
        var geometry = this.geometry.slice(15, -3);
        var splits = geometry.split(')),((');

        for (var k in splits) {
            polygons.push('((' + splits[k] + '))');
        }
    }

    for (var k in polygons) {
        var parts = polygons[k].slice(2, -2).split('),(');

        for (var p in parts) {
            coords.push(convertPolygon(parts[p]));
        }
    }

    return coords;
};

PostcodeRegion.prototype.getBounds = function() {

    if(this.bounds.length > 0) {
        return this.bounds;
    }

    var polygonCoords = this.getPolygonCoords();
    var minLat = null, maxLat = null, minLng = null, maxLng = null;

    polygonCoords.forEach(function(coords) {
        coords.forEach(function(coord) {
            if(minLat == null || coord.lat() < minLat) {
                minLat = coord.lat();
            }
            if(maxLat == null || coord.lat() > maxLat) {
                maxLat = coord.lat();
            }
            if(minLng == null || coord.lng() < minLng) {
                minLng = coord.lng();
            }
            if(maxLng == null || coord.lng() > maxLng) {
                maxLng = coord.lng();
            }
        });
    });

    this.bounds = {
        'NorthEast': {
            'lat': maxLat,
            'lng': maxLng,
        },
        'SouthWest': {
            'lat': minLat,
            'lng': minLng,
        }
    };

    return this.bounds;
};

PostcodeRegion.prototype.highlight = function () {
    if (this.parent.map.currentRegion) {
        this.parent.map.currentRegion.reset();
    }

    this.parent.map.currentRegion = this;

    this.polygon.setOptions({
        strokeOpacity: 1
    });
};

PostcodeRegion.prototype.reset = function () {
    if(this.polygon !== null) {
        this.polygon.setOptions({
            strokeOpacity: 0.3,
        });
    }
};

PostcodeRegion.prototype.open = function () {
    var self = this;

    if (typeof self.parent.supplierGroup === 'undefined') {
        return false;
    }

    this.parent.currentRegion = this;

    var options = {
        parent: this.parent
    };

    var url = this.parent.pageUrl + 'get-region/' + this.postcode + '/' + this.country + '/';

    this.infoWindow = new RegionInfoWindow(options);
    this.infoWindow.open(url, {
        'supplierGroup': self.parent.supplierGroup
    });
};

PostcodeRegion.prototype.renderLabel = function () {
    var label = new InfoBox({
        content: this.postcode,
        boxClass: 'regionLabel',
        disableAutoPan: true,
        pixelOffset: new google.maps.Size(-21, -4),
        position: new google.maps.LatLng(this.lat, this.lng),
        closeBoxURL: '',
        isHidden: false,
        pane: 'mapPane',
        enableEventPropagation: true,
        visible: ((this.parent.map.getZoom() <= 11) ? false : true)
    });

    label.open(this.parent.map);

    return label;
};

PostcodeRegion.prototype.toggleLabel = function () {
    /**
     * region has not been rendered yet, skip
     */
    if (!this.label) {
        return;
    }

    if (this.parent.map.getZoom() <= 11) {
        this.label.hide();
    } else {
        this.label.show();
    }
};

PostcodeRegion.prototype.getDataset = function () {
    return $('#dataset option[value=\'' + $('#dataset').val() + '\']').data('event');
};

convertPolygon = function (polygon) {
    var splits = polygon.split(',');
    var coords = [];

    for (var key in splits) {
        var latLng = splits[key].split(' ');

        coords.push(new google.maps.LatLng(latLng[1], latLng[0]));
    }

    return coords;
};
