$(function () {

});

function SpecialRole(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.8 + 'px',
        zIndex: 2010,
        toolbarButtons: [
            {
                position: 'right',
                label: 'Opslaan',
                classNames: 'btn btn-primary',
                disabled: false,
                action: 'save-special-roles'
            }
        ]
    });

    this.open(url);
};

SpecialRole.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);
        self.initialize(url);
    }).fail(function () {

    });
};

SpecialRole.prototype.initialize = function (url) {
    var self = this;

    $('#specialRoles').attr('data-url', url);

    $('a[data-action="save-special-roles"]').on('click', function () {
        var roles = {}
        $('#specialRoles input[type="checkbox"]').each(function () {
            if (!$(this).is(':disabled')) {
                roles[$(this).attr('data-role-id')] = $(this).is(':checked');
            }
        });

        $.ajax({
            url: $('#specialRoles').attr('data-url'),
            method: 'POST',
            data: {
                roles: JSON.stringify(roles)
            }
        }).done(function () {
            swal.setDefaults({
                onOpen: function () {
                    $('.swal2-container').css({
                        'z-index': 3000
                    })
                }
            });

            self.contentWindow.refresh();
            swal('Opgeslagen', 'De rollen zijn succesvol aangepast', 'success');
        })
    });

    $('button[data-action="add-role-field"]').on('click', function () {
        var swalCheckboxes = $('<table style="margin-left: auto; margin-right: auto;"></table>');
        swalCheckboxes.append('<tr><td class="row-checkbox" style="text-align: left;"><label class="checker bg-white" id="uniform-select_all"><span class="checked"><input type="checkbox" id="swal-role-checkbox-read" class="form-styled" checked></span></label> Lezen</td></tr>');
        swalCheckboxes.append('<tr><td class="row-checkbox" style="text-align: left;"><label class="checker bg-white" id="uniform-select_all"><span class="checked"><input type="checkbox" id="swal-role-checkbox-write" class="form-styled" checked></span></label> Schrijven</td></tr>');

        var $this = $(this);
        swal.setDefaults({
            confirmButtonText: 'Volgende',
            progressSteps: ['1', '2'],
            showCancelButton: true,
            type: 'question',
            onOpen: function () {
                $('.swal2-container').css({
                    'z-index': 3000
                })
            }
        });

        swal.queue([
            {
                title: 'Kolom toevoegen',
                text: 'Welke kolom wil je toevoegen?',
                input: 'text',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value) {
                            resolve()
                        } else {
                            reject('Er moet een kolom opgegeven worden.')
                        }
                    })
                }
            },
            {
                title: 'Rollen',
                text: 'Welke rollen wil je op dit veld toepassen?',
                html: swalCheckboxes[0].outerHTML,
                focusConfirm: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-role-checkbox-read').is(':checked'),
                            $('#swal-role-checkbox-write').is(':checked')
                        ])
                    })
                }
            }
        ]).then(function (result) {
            var column = result[0];
            var roles = {
                read: result[1][0],
                write: result[1][1]
            };

            $.ajax({
                method: 'POST',
                url: $this.attr('data-add-column-path'),
                data: {
                    column: column,
                    roles: JSON.stringify(roles)
                }
            }).done(function (result) {
                var row = $('<tr></tr>');
                row.append('<td>' + result.name + '</td>');

                var columnRowTable = $('.columnRoleTable tbody');

                if (columnRowTable.length === 0) {
                    self.contentWindow.refresh();
                    return;
                }

                if (result.read > 0) {
                    row.append(' <td class="row-checkbox"><label class="checker bg-white" id="uniform-select_all"><span class="checked">' +
                        '<input type="checkbox" data-role-id="' + result.read + '" class="form-styled" checked></span></label></td>');
                } else {
                    row.append(' <td class="row-checkbox"><label class="checker bg-white disabled" id="uniform-select_all"><span class="checked disabled">' +
                        '<input type="checkbox" class="form-styled" checked disabled></span></label></td>');
                }

                if (result.write > 0) {
                    row.append(' <td class="row-checkbox"><label class="checker bg-white" id="uniform-select_all"><span class="checked">' +
                        '<input type="checkbox" data-role-id="' + result.id + '" class="form-styled" checked></span></label></td>');
                } else {
                    row.append(' <td class="row-checkbox"><label class="checker bg-white disabled" id="uniform-select_all"><span class="checked disabled">' +
                        '<input type="checkbox" class="form-styled" checked disabled></span></label></td>');
                }

                row.append('<td></td>');

                columnRowTable.append(row);
            });
        });
    });

    $('button[data-action="delete-column"]').on('click', function (e) {
       var $this = $(this);

        swal.setDefaults({
            showCancelButton: true,
            onOpen: function () {
                $('.swal2-container').css({
                    'z-index': 2100
                })
            }
        });

        swal('Let op', 'Weet je zeker dat je deze kolom wil verwijderen? Deze kolom wordt ook voor de andere groepen en gebruikers verwijderd.', 'warning')
        .then(function () {
            $.ajax({
                url: $this.attr('data-url'),
                method: 'DELETE'
            }).done(function () {
                $this.closest('tr').remove();
                swal('Kolom verwijderd', 'De kolom is succesvol verwijderd', 'success');
            });
        });
    });
};
