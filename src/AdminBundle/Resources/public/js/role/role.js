$(function () {
    $('table').on('click', '.row-actions a[data-type="view-role"]', function (e) {
        e.preventDefault();
        new Role($(this).attr('href'));
    });
});

function Role(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.9 + 'px',
        zIndex: 2000,
        toolbarButtons: [
            {
                position: 'right',
                label: 'Opslaan',
                classNames: 'btn btn-primary',
                disabled: false,
                action: 'persist_roles'
            }
        ]
    });

    this.open(url);
}

Role.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);
        self.initialize();

        $('#persist-roles-form').attr('action', url);
    }).fail(function () {

    });
};

Role.prototype.initialize = function () {
    var self = this;

    $('button[data-action="role-add-user"]').on('click', function () {
        var ids = [];
        var users = [];
        var roleOverlay = $(this).closest('.role-overlay');

        $.ajax({
            url: roleOverlay.attr('data-getuserlist-url'),
            method: 'GET'
        }).done(function (data) {
            swal.setDefaults({
                onOpen: function () {
                    $('.swal2-container').css({
                        'z-index': 2001
                    })
                }
            });

            if (data.users.length <= 0) {
                swal('Let op', 'Voor deze categorie zijn alle geregistreerde gebruikers al toegewezen', 'warning');
            } else {
                $.each(data.users, function (key, value) {
                    ids.push(value.id);
                    users.push(value.firstname === null && value.lastname === null ? value.username : value.firstname + ' ' + value.lastname);
                });

                swal({
                    html: 'Welke gebruiker wil je toewijzen aan deze rol?',
                    type: 'question',
                    input: 'select',
                    inputOptions: users
                }).then(function (result) {
                    var userTableBody = $('.user-table-body');

                    if (userTableBody.length === 0) {
                        self.contentWindow.refresh();
                        return;
                    }

                    var roles = JSON.parse(roleOverlay.attr('data-roles'));
                    var row = $('<tr></tr>');
                    row.append('<td>' + users[result] + '</td>');
                    $.each(roles, function (key, value) {
                        row.append(' <td class="row-checkbox">\n' +
                            '<label class="checker bg-white" id="uniform-select_all">\n' +
                            '<span class="checked">\n' +
                            '<input type="checkbox" data-user-id="' + ids[result] + '" data-role-id="' + value.id + '" class="form-styled" checked>\n' +
                            '</span>\n' +
                            '</label>\n' +
                            '</td>');
                    });

                    row.append('<td><a data-action="open-special-access" ' +
                        'data-path="categorie/' + roleOverlay.attr('data-category-id') + '/speciale-toegang/gebruiker/' + ids[result] + '">Speciale toegang</a></td>');

                    userTableBody.append(row);
                });
            }
        }).fail(function () {
            self.showFailAlert();
        })
    });

    $('button[data-action="role-add-group"]').on('click', function () {
        var ids = [];
        var groups = [];
        var roleOverlay = $(this).closest('.role-overlay');

        $.ajax({
            url: roleOverlay.attr('data-getgrouplist-url'),
            method: 'GET'
        }).done(function (data) {
            swal.setDefaults({
                onOpen: function () {
                    $('.swal2-container').css({
                        'z-index': 2001
                    })
                }
            });

            if (data.groups.length <= 0) {
                swal('Let op', 'Voor deze categorie zijn alle geregistreerde groepen al toegewezen', 'warning');
            } else {
                $.each(data.groups, function (key, value) {
                    ids.push(value.id);
                    groups.push(value.name);
                });

                swal({
                    html: 'Welke groep wil je toewijzen aan deze rol?',
                    type: 'question',
                    input: 'select',
                    inputOptions: groups
                }).then(function (result) {
                    var groupTableBody = $('.group-table-body');

                    if (groupTableBody.length === 0) {
                        self.contentWindow.refresh();
                        return;
                    }

                    var roles = JSON.parse(roleOverlay.attr('data-roles'));
                    var row = $('<tr></tr>');

                    row.append('<td>' + groups[result] + '</td>');

                    $.each(roles, function (key, value) {
                        row.append(' <td class="row-checkbox">\n' +
                            '<label class="checker bg-white" id="uniform-select_all">\n' +
                            '<span class="checked">\n' +
                            '<input type="checkbox" data-group-id="' + ids[result] + '" data-role-id="' + value.id + '" class="form-styled" checked>\n' +
                            '</span>\n' +
                            '</label>\n' +
                            '</td>');
                    });

                    row.append('<td><a data-action="open-special-access" ' +
                        'data-path="categorie/' + roleOverlay.attr('data-category-id') + '/speciale-toegang/groep/' + ids[result] + '">Speciale toegang</a></td>');

                    groupTableBody.append(row);
                });
            }
        }).fail(function () {
            self.showFailAlert();
        })
    });

    $('a[data-action="persist_roles"]').on('click', function () {
        var userRoles = {};
        var groupRoles = {};
        $('.user-table-body input').each(function () {
            if (typeof userRoles[$(this).attr('data-user-id')] === 'undefined') {
                userRoles[$(this).attr('data-user-id')] = {};
            }
            userRoles[$(this).attr('data-user-id')][$(this).attr('data-role-id')] = $(this).is(':checked');
        });

        $('.group-table-body input').each(function () {
            if (typeof groupRoles[$(this).attr('data-group-id')] === 'undefined') {
                groupRoles[$(this).attr('data-group-id')] = {};
            }
            groupRoles[$(this).attr('data-group-id')][$(this).attr('data-role-id')] = $(this).is(':checked');
        });

        swal.setDefaults({
            onOpen: function () {
                $('.swal2-container').css({
                    'z-index': 2500
                })
            }
        });

        swal('Wijzigingen opgeslagen', 'De wijzigingen zijn succesvol opgeslagen', 'success');

        if (Object.keys(userRoles).length === 0 && Object.keys(groupRoles).length === 0) {
            self.contentWindow.hide();
            return false;
        }

        $('#user-roles').val(JSON.stringify(userRoles));
        $('#group-roles').val(JSON.stringify(groupRoles));
        $('#persist-roles-form').submit();
    });

    $('.role-overlay').on('click', 'a[data-action="open-special-access"]', function (e) {
        e.preventDefault();

        new SpecialRole($(this).attr('data-path'));
    });

    $('body').on('click', 'button[data-action="delete-role"]', function () {
        var $this = $(this);
        $.ajax({
            url: $this.attr('data-url')
        }).then(() => {
            swal('Rol verwijderd', 'De rol is succesvol verwijderd', 'success');
            $this.closest('tr').remove();
        });
    })
    .on('click', 'button[data-action="add-role"]', function () {
        var $this = $(this);
        swal.setDefaults({
            type: 'question',
            onOpen: function () {
                $('.swal2-container').css({
                    'z-index': 2500
                })
            },
            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        resolve();
                    } else {
                        reject('Het veld mag niet leeg zijn.');
                    }
                });
            }
        });

        swal.queue([
            {
                title: 'Welke naam heeft de rol?',
                text: 'Rol moet beginnen met ROLE_',
                input: 'text',
                inputValidator: (value) => {
                    return new Promise((resolve, reject) => {
                        if (value.startsWith('ROLE_')) {
                            resolve();
                        } else {
                            reject('Rol moet beginnen met ROLE_');
                        }
                    });
                }
            },
            {
                title: 'Hoe omschrijf je deze rol?',
                text: 'Leesbare benaming voor de rol',
                input: 'text'
            }
        ]).then(function (result) {
            var roleName = result[0].toUpperCase();
            $.ajax({
                url: $this.attr('data-add-role-url'),
                method: 'POST',
                data: {
                    roleName: roleName,
                    roleDescription: result[1]
                }
            }).done(function (data) {
                var row = $('<tr></tr>');
                row.append('<td>' + roleName + '</td>');
                row.append('<td class="row-checkbox"><label class="checker bg-white" id="uniform-select_all">' +
                    '<span class="checked"><input type="checkbox" data-role-id="' + data + '" class="form-styled" checked></span></label></td>');

                $('.specialRoleTable').append(row);
            }).error(function () {
                swal('Rol kan niet worden toegevoegd', 'Er bestaat al een rol met deze naam', 'error');
            });
        });
    })
    .on('click', 'td.row-checkbox input[type="checkbox"]', function () {
        $(this).closest('span').toggleClass('checked', $(this).is(':checked'));
    });
};

Role.prototype.showFailAlert = function () {
    swal('Er is iets fout gegaan', 'De lijst kan niet worden opgehaald.', 'error');
};
