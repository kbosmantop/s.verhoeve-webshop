$('a[data-action="create-rule"]').on('click', function (e) {
    e.preventDefault();

    var elm = $(this);
    new RuleCreate(elm.attr('href'), function () {
        window.location.reload();
    });
});