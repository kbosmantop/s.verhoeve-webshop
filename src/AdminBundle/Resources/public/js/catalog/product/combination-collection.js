function CombinationProductSelection(combinationId, viewUrl) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.4 + 'px',
        zIndex: 2000
    });
    this.init(combinationId, viewUrl);
}

CombinationProductSelection.prototype.init = function (combinationId, viewUrl) {
    var self = this;

    self.view = viewUrl;
    self.contentWindow.empty().show();
    self.contentWindow.setOption('refreshUrl', self.view);

    $.get(viewUrl)
    .done(function (data) {
        self.contentWindow.setHtml(data);
        self.bindEvents(combinationId);
    });
};

CombinationProductSelection.prototype.bindEvents = function (combinationId) {
    var selectProductsPanel = $('.select-product-panel');
    var getCombinationsUrl = selectProductsPanel.data('get-combinations');

    $('#searchQuery').on('change', function () {
        var val = $(this).val();

        $.ajax({
            url: getCombinationsUrl,
            method: 'POST',
            data: {
                combination_id: combinationId,
                query: val
            }
        })
        .then(function (data) {
            var products = $('.products');
            products.empty();
            $.each(data, function (pr) {
                var html = '<div class="col-product-item" data-product-id="' + data[pr].id + '" style="width: 24%;">'
                  + '<div class="product-item">'
                  + '<div class="product-item-image"><img src="' + data[pr].image + '"/></div>'
                  + '<div class="product-item-description">' + data[pr].title + '</div>'
                  + '<div class="product-item-price">&euro; ' + data[pr].price + '</div>'
                  + '</div>';
                products.append(html);
            });
        });
    });

    // Select combination product
    $(document).on('click', '.col-product-item', function () {
        var product = $(this);
        var productId = product.data('product-id');
        var productDescription = product.find('.product-item-description').html();

        swal({
            type: 'question',
            title: 'Toevoegen aan de combinatie?',
            showCancelButton: true,
            cancelButtonText: 'Annuleren',
            text: 'Weet je zeker dat je "' + productDescription + '" aan de combinatie wil toevoegen?'
        })
        .then(function () {
            var extendCombinationUrl = selectProductsPanel.data('extend-combination');
            $.ajax({
                url: extendCombinationUrl,
                method: 'POST',
                data: {
                    combination_id: combinationId,
                    product_id: productId
                }
            })
            .then(function () {
                swal({
                    type: 'success',
                    title: 'Success!',
                    text: 'De combinatie is uitgebreid.'
                });
                window.location.reload();
            }, function () {
                swal({
                    type: 'error',
                    title: 'Foutmelding',
                    text: 'Het geselecteerde product kan niet worden gecombineerd met de huidige producten.'
                });
            });
        });
    });
};

function CombinationCollection () {
    this.init = function() {
        this.bindEvents();
    };

    this.onClickAdd = function(e) {
        e.preventDefault();

        var self = $(e.currentTarget);
        var combinationId = self.data('combination-id');
        var viewUrl = self.data('select-view');

        new CombinationProductSelection(combinationId, viewUrl);
    };

    this.onClickRemove = function(e) {
        e.preventDefault();
        var url = e.currentTarget.href;

        swal({
            type: 'question',
            title: 'Verwijderen uit de combinatie?',
            showCancelButton: true,
            cancelButtonText: 'Annuleren',
            confirmButtonText: 'Verwijderen',
            text: 'Weet je zeker dat je dit item uit de combinatie wil verwijderen?'
        })
        .then(function() {
            $.ajax({
                url: url,
                method: 'DELETE'
            })
            .error(function(data) {
                swal('Mislukt!', data.responseJSON.error, 'error');
            })
            .success(function(data) {
                swal('Success!', data.success, 'success')
                .then(function() {
                    window.location.reload();
                });
            });
        });
    };

    this.bindEvents = function() {
        $('.combination-product-holder')
        .on('click', '.combination-add-item', this.onClickAdd.bind(this))
        .on('click', '.combination-remove-item', this.onClickRemove.bind(this))
        ;
    };

    this.init();
}

$(function () {
    new CombinationCollection();
});
