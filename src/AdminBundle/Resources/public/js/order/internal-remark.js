function InternalRemark(url, options) {

    this.url = url;

    options.successCallback = function(data, contentWindow) {

        contentWindow.formChange = false;
        contentWindow.empty().hide();

        var user = 'Gegenereerd door systeem';
        if (data.hasOwnProperty('user')) {
            user = data.user;
        }

        if (data.hasOwnProperty('remark') && data.remark) {
            var internalRemarksElm = $('.internal-remarks');

            internalRemarksElm.prepend(
                '<div class="remark">' +
                '<div class="row">' +
                '<div class="col-xs-6">' +
                '<strong>' + user + '</strong>' +
                '</div>' +
                '<div class="col-xs-6 text-right">'
                + data.createdAt +
                '</div>' +
                '</div>' +
                '<div>'
                + data.remark +
                '</div>' +
                '</div>'
            );
        }
    };

    this.contentWindow = new ContentWindow(options);

    this.open();
}

InternalRemark.prototype.open = function() {

    var self = this;

    this.contentWindow.empty().show();
    this.contentWindow.loading();

    $.get(this.url).done(function (html) {
        self.contentWindow.setHtml(html);

        var textarea = $('textarea', self.contentWindow.elm);

        autosize(textarea, self.contentWindow.elm);

        textarea.focus();
        textarea.get(0).selectionStart = textarea.get(0).selectionEnd = textarea.val().length;
    });
};

$(function () {
    var panel = $('.panel');

    panel.on('click', 'button.delete-remark', function(e) {
        var elm = $(this);
        swal({
            type: 'warning',
            title: 'Opmerking verwijderen',
            text: 'Weet je zeker dat je deze opmerking wilt verwijderen?',
            showCancelButton: true
        }).then((result) => {
            if(result) {
                $.ajax({
                    url: elm.attr('data-href'),
                    method: 'DELETE'
                }).then(() => {
                    elm.closest('.internal-remark').remove();

                if($('.internal-remark', panel).length === 0) {
                    $('.internal-remarks').html('<em>Er zijn geen interne opmerkingen geplaatst.</em>');
                }

            });
            }
        });
    });

    $('body').on('click', '[data-action="remark"]', function () {
        var elm = $(this);
        var url = elm.attr('data-url');

        new InternalRemark(url, {
            width: '600px',
            zIndex: 2012
        });
    });
});