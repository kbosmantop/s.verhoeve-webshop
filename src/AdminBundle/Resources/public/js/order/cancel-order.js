function CancelOrder(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth / 2 + 'px',
        zIndex: 2020
    });

    this.open(url);
}

CancelOrder.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);
    });
};

CancelOrder.prototype.initialize = function () {
    this.bindEvents();
    // this.initializePopovers();
    // this.initializeTooltips();
    // this.initializeSwitchery();
    //
    // this.hideUnrelatedOrders();
    // this.scrollTo();
};

CancelOrder.prototype.bindEvents = function () {
    var self = this;

    $(this.contentWindow.elm).unbind().on('click', 'button[data-action=select-supplier]', function (e) {
        e.preventDefault();

        $('#order_process_supplier', self.contentWindow.elm).val($(this).data('id'));

        $('form', self.contentWindow.elm).trigger('submit');
    });
};
