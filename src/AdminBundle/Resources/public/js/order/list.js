var orderList = null;

function OrderList() {
    this.elm = $('#order_list');

    this.init = function() {
        this.bindEvents();
    };

    this.initDataTable = function() {
        this.elm.DataTable({
            'sDom': '',
            'paging': false,
            'ordering': false,
            'info': false,
            'columns': [
                {'width': '100px'},
                {'width': '150px'},
                null,
                null,
                null,
                null,
                {'width': '200px'},
                {'width': '150px'}
            ]
        });
    };

    this.bindEvents = function() {
        this.initDataTable();

        this.elm
            .on('click', 'button[data-event="get-order"]', function (e) {
                e.preventDefault();

                var href = $(this).data('href');
                var orderId = $(this).data('order-id');

                new Order(href, orderId);
            })
            .on('click', 'button[data-event="process-order"]', function (e) {
                e.preventDefault();

                new ProcessOrderModal($(this).data('href'));
            })
            .on('click', 'button[data-event="cancel-order"]', function (e) {
                e.preventDefault();

                new CancelOrder($(this).data('href'));
            })
        ;
    };

    this.init();
}

$(function () {
    var orderList = new OrderList();
});



