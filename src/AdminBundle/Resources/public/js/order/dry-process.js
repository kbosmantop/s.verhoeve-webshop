function DryProcess(orderId) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.3 + 'px',
        zIndex: 2012,
        closeButtonLabel: 'Venster sluiten',
    });

    this.url = '/admin/bestellingen/' + orderId + '/analyseren';

    this.open();
}

DryProcess.prototype.open = function () {
    var self = this;

    this.contentWindow.empty().show();

    $.get(self.url).done(function (data) {
        self.contentWindow.setHtml(data);
    });
};
