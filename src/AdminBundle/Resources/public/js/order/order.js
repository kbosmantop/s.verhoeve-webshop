function Order(url, orderId) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.9 + 'px',
        zIndex: 2000
    });

    this.open(url, orderId);
}

Order.prototype.open = function (url, orderId) {
    var self = this;

    this.url = url;
    this.orderId = orderId;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    this.orderXhr = null;

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);

        self.bindEvents();
        self.initialize();
    }).fail(function () {

    });
};

Order.prototype.initialize = function () {

    this.initializePopovers();
    this.initializeTooltips();
    this.initializeSwitchery();

    this.showOrder();
    this.checkDrafts();
    this.scrollTo();

    var orderCount = $('#order-orders').find('.panel');
    if(orderCount.length === 1){
        $('#order-orders .panel .heading-elements').hide();
    }
};

Order.prototype.bindEvents = function () {
    var self = this;
    var elm = self.contentWindow.elm;

    var pdfViewerParameters = {
        toolbarButtons: [
            {
                position: 'right',
                action: 'pdf-print',
                label: 'Afdrukken',
                icon: 'icon-printer',
                order: 5,
                classNames: 'btn-primary',
                disabled: true
            },
            {
                position: 'right',
                action: 'pdf-download',
                label: 'Opslaan',
                icon: 'icon-file-download',
                order: 10,
                classNames: 'btn-primary',
                disabled: true
            }
        ]
    };

    elm.on('click', '#payment_request', function(e) {
        e.preventDefault();

        openContent($(this));
    });

    elm.on('click', 'i[data-action="show-activity-popup"]', function() {
        var elm = $(this);

        swal({
            title: elm.attr('data-title'),
            type: 'info',
            html: elm.attr('data-content')
        });
    });

    elm.on('click', 'button[data-action="show-activity"]', function () {
        var activityPath = $(this).closest('.view').attr('data-ordercollection-activity-path');
        self.loadActivities(activityPath);
    });

    elm.on('click', 'a[data-action="view-pdf"]', function (e) {
        e.preventDefault();

        new ViewPDF($(this).attr('href'), pdfViewerParameters);
    });

    elm.on('click', '[data-action="review-fraud"]', function (e) {
        e.preventDefault();

        var fraudFields = JSON.parse($('#fraudFields').attr('data-fields'));

        var checkboxes = $('<div class="checkboxes"></div>');
        $.each(fraudFields, function (key, field) {
            var checkboxHolder = $('<div class="checkbox" style="text-align: left;"></div>');

            var checkbox = $('<input id="fraud-' + key + '" class="fraudField" type="checkbox"/>');
            var label = $('<label for="fraud-' + key + '">' + field.description + ' (score: ' + field.score + ')</label>');

            checkboxHolder.append(checkbox).append(label);
            checkboxes.append(checkboxHolder);
        });

        swal({
            title: 'Mogelijke fraude gedetecteerd',
            text: 'Vink de onderstaande punten af om de order te kunnen verwerken.',
            html: checkboxes.html(),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Deze order is in orde',
            cancelButtonText: 'Terug naar order',
            focusConfirm: false,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    var checked = true;
                    $('.fraudField').each(function () {
                        if (!$(this).is(':checked')) {
                            checked = false;
                        }
                    });

                    if (checked) {
                        resolve();
                    } else {
                        swal.showValidationError('Alle fraude mogelijkheden moeten zijn afgevinkt voordat de order kan worden doorgestuurd.');
                        $('.swal2-buttonswrapper').find('button').attr('disabled', false);
                    }

                });
            }
        }).then(function (data) {
            $('[data-action="process-order"]').attr('disabled', false);
        });
    });

    elm.on('click', 'a[data-action="edit-status"]', function () {
        var self = $(this);
        var currentStatus = $(this).attr('data-current-status');

        var availableStatuses;

        $.ajax({
            url: $(this).attr('data-status-path'),
            method: 'GET'
        }).done(function (statuses) {
            availableStatuses = statuses;

            if (!availableStatuses) {
                swal({
                    title: 'Let op',
                    html: 'De huidige orderstatus kan niet handmatig worden aangepast',
                    type: 'warning',
                    onOpen: function () {
                        $('.swal2-container').css({
                            'z-index': 2001
                        })
                    }
                });
            } else {
                swal.setDefaults({
                    type: 'question',
                    title: 'Bevestiging',
                    onOpen: function () {
                        $('.swal2-container').css({
                            'z-index': 2001
                        })
                    },
                    showCancelButton: true
                });
                swal.queue([
                    {
                        html: 'Weet je zeker dat je de status wilt wijzigen?',
                        input: 'select',
                        inputOptions: availableStatuses,
                        inputValue: currentStatus
                    },
                    {
                        html: 'Wat is de reden van de aanpassing?',
                        input: 'text',
                        inputValidator: function (value) {
                            return new Promise(function (resolve, reject) {
                                if (!value) {
                                    reject('Er moet een reden van wijziging opgegeven worden.');
                                } else {
                                    resolve();
                                }
                            })
                        }
                    }
                ]).then(function (result) {
                    var status = result[0];
                    var reason = result[1];

                    $.ajax({
                        url: self.attr('data-href'),
                        method: 'POST',
                        data: {
                            status: status,
                            reason: reason
                        }
                    }).done(function (response) {
                        swal('', 'De status is succesvol geupdate naar ' + availableStatuses[status], 'success');

                        new Order(self.attr('data-order-href'));
                    }).fail(function () {
                        swal('', 'Er is iets fout gegaan met het updaten van de status', 'error');
                    });
                });
            }
        });
    });

    elm.on('click', 'a[data-action="view-invoice-pdf"]', function (e) {
        e.preventDefault();

        var buttons = JSON.parse(JSON.stringify(pdfViewerParameters.toolbarButtons));

        buttons.push({
            position: 'right',
            action: 'send-invoice',
            label: 'Verstuur per e-mail',
            icon: 'icon-mail5',
            order: 0,
            classNames: 'btn-primary',
            disabled: true,
            href: $('a[data-action=send-invoice]', self.contentWindow.elm).attr('href'),
            dataAttributes: {
                email: $('a[data-action=send-invoice]', self.contentWindow.elm).attr('data-email')
            }
        });

        new ViewPDF($(this).attr('href'), {
            toolbarButtons: buttons
        });
    });

    elm.on('click', 'a[data-action=edit-order]', function (e) {
        e.preventDefault();

        if ($(this).attr('disabled')) {
            return;
        }

        new EditOrder($(this).attr('href'), self.contentWindow, self);
    });

    elm.on('click', 'a[data-action=process-order]', function (e) {
        e.preventDefault();

        if ($(this).attr('disabled')) {
            return;
        }

        new ProcessOrderModal($(this).attr('href'), self);
    });

    elm.on('click', 'a[data-action=cancel-order]', function (e) {
        e.preventDefault();

        new CancelOrder($(this).attr('href'));
    });

    elm.on('click', 'a[data-event="edit-process"]', function (e) {
        e.preventDefault();

        new EditProcess($(this).data('href'));
    });

    elm.on('click', '.order-line-card-edit, .order-line-inputfields-edit', function (e) {
        e.preventDefault();
        var btnElm = $(this);
        new EditOrderLineCard(btnElm.attr('href'), {
            parent: self.contentWindow,
            refreshUrl: self.url
        });
    });

    elm.on('click', '.order-line-design-edit', function (e) {
        e.preventDefault();
        var btnElm = $(this);
        new ViewDesign(btnElm.attr('href'), {
            parent: self.contentWindow,
            refreshUrl: self.url
        });
    });

    elm.on('click', '.order-line-letter-edit', function (e) {
        e.preventDefault();
        var btnElm = $(this);

        new EditLetter(btnElm.attr('href'), {
            parent: self.contentWindow,
            refreshUrl: self.url
        });
    });

    $('body').on('click', 'a[data-action=send-invoice]', function (e) {
        e.preventDefault();

        new SendInvoice({
            url: $(this).attr('href'),
            email: $(this).attr('data-email')
        });
    });

    elm.on('click', '.order-order.panel .panel-heading', function (e) {
        e.preventDefault();

        if ($(this).closest('.order-order').find('.panel-body').length) {
            $(this).find('.panel-body').remove();
            $(this).find('.panel-footer').remove();
            $('a[data-action="collapse"]').show();
        }

        self.showOrder($(this).closest('.order-order').attr('data-order-id'));
    });

    $('i.glyphicon').on('mouseover', function (e) {
        e.stopPropagation();

        $(this).tooltip('show');
        $(this).parent().parent().popover('hide');
    });

    $('[data-popup="tooltip"]').tooltip();

    elm.on('click', 'button[data-action=cancel-ajax-order]', function (e) {
        e.preventDefault();

        var btn = $(this);
        var url = btn.data('url');
        var type = btn.data('type');
        var number = btn.data('order-number');

        var message = '<div>Weet je zeker dat je order ' + number + '<br> wilt annuleren?<br><br></div><small>Reden:</small>';

        if (type === 'supplier') {
            message = '<div>Weet je zeker dat je deze order wilt weghalen bij de leverancier?<br><br></div><small>Reden:</small>';
        }

        self.swal(message, url, type, number)
    });

    elm.on('click', 'a[data-action=payment-invoice-cancellation]', function (e) {
        e.preventDefault();

        var btn = $(this);
        var cancellationUrl = btn.data('cancellation');
        var url = btn.data('url');

        var message = 'Dit verzoek annuleert de factuurbetalingsmethode en opent een nieuw betalingsverzoek.';

        swal({
            type: 'question',
            title: 'Factuurbetaling annuleren?',
            showCancelButton: true,
            confirmButtonText: 'Factuurbetaling annuleren',
            confirmButtonColor: '#ef5350',
            reverseButtons: true,
            text: message
        }).then(function () {
            $.ajax({
                url: cancellationUrl,
                method: 'GET'
            }).done(function () {
                self.contentWindow.refresh();
                swal({
                    type: 'success',
                    title: 'Gelukt',
                    text: 'De factuurbetaling is succesvol geannuleerd',
                    allowOutsideClick: false
                }).then(function () {
                    openContent($('#payment_request'));
                })
            }).fail(function () {
                swal({
                    type: 'error',
                    title: 'Mislukt',
                    text: 'Factuurbetaling kan niet geannuleerd worden',
                    allowOutsideClick: true
                });
            });
        }).catch(swal.noop);
    });

    elm.on('click', 'button[data-action="restitute-ajax-order"]', function (e) {
        e.preventDefault();

        const href = $(this).attr('data-url');
        const self = this;

        self.contentWindow = new ContentWindow({
            width: window.innerWidth * 0.85 + 'px',
            zIndex: 2001
        });
        self.contentWindow.empty().show();
        self.contentWindow.setOption('refreshUrl', href);
        self.contentWindow.setOption('refreshCallback', function () {

        });

        orderXhr = null;

        $.get(href).done(function (data) {
            self.contentWindow.setHtml(data);
        }).fail(function () {

        });
    });

    elm.on('click', '.order-collection-line.panel .panel-heading', function(e){
        e.preventDefault();
        $('.order-order').each(function(){
            $(this).find('.panel-body').remove();
            $(this).find('.panel-footer').remove();
            $('a[data-action="collapse"]').show();
        });
        $('.order-collection-line.hidden-content').show();
    });

    elm.on('click', 'a[data-action="apply-voucher"]', function(e) {
        e.preventDefault();

        const url = $(this).attr('href');

        swal({
            type: 'info',
            title: 'Korting toepassen',
            text: 'Voer de kortingscode in',
            input: 'text',
            showCancelButton: true,
            inputValidator: function (text) {
                return new Promise(function (resolve, reject) {
                    if (text) {
                        resolve()
                    } else {
                        reject('Er moet een voucher code ingevoerd worden.');
                    }
                })
            }
        }).then(function(code) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    code: code
                },
                error: function(errorMessage) {
                    swal('Fout', 'De toegepaste korting is niet geldig.', 'error');
                }
            }).then(function(response) {
                swal('Korting toegepast', 'De korting is succesvol toegepast.', 'success');

                self.contentWindow.refresh();
            });
        })
    })
};

Order.prototype.swal = function (message, url, type, number) {
    var self = this;
    swal({
        type: 'question',
        title: 'Bevestiging',
        html: message,
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Bevestigen',
        onOpen: function () {
            $('.swal2-container').css({
                'z-index': 2001
            })
        },
        inputValidator: function (text) {
            return new Promise(function (resolve, reject) {
                if (text) {
                    resolve()
                } else {
                    reject('Reden veld mag niet leeg zijn!');
                }
            })
        }
    }).then(function (text) {
        $.ajax({
            url: url,
            method: 'POST',
            data: {reason: text, type: type}
        }).done(function () {
            swal({
                type: 'success',
                title: 'Gelukt',
                text: 'Order ' + number + ' is succesvol geannuleerd',
                allowOutsideClick: false
            }).then(function () {
                self.contentWindow.refresh();
                if(typeof(orderSearch) !== 'undefined'){
                    orderSearch.search();
                }
                var orderSiteSelectionOnChangeTriggerElement = $('#orderSiteSelection');
                if(orderSiteSelectionOnChangeTriggerElement){
                    orderSiteSelectionOnChangeTriggerElement.trigger('change');
                }
            }, function () {
                window.location.reload();
            });
        }).fail(function () {
            swal({
                type: 'error',
                title: 'Mislukt',
                text: 'Order ' + number + ' kan niet geannuleerd worden',
                allowOutsideClick: true
            });
        });
    }, function (dismiss) {
    });

    $('button[data-action="show-activity"], a[data-action="show-activity"]').unbind().on('click', function () {
        var value = $(this).data('value');

        $('.panel[data-order-id="' + value + '"]').find('a[data-action="collapse"]').trigger('click');

        self.showOrderActivity(value);
    });
};

Order.prototype.checkDrafts = function() {
    var self = this;

    $('#order_finalize_drafts').remove();

    $.ajax({
        url: self.url,
        data: {
            check_drafts: true
        }
    }).done(function(response) {
        var saveButtonHtml = '<a id="order_finalize_drafts" data-action="order-finalize-drafts" class="btn btn-xs btn bg-green order-save">Maak gewijzigde order(s) definitief</a>';

        if (
            response.hasOwnProperty('hasDrafts')
            && response.hasOwnProperty('finalizeUrl')
            && response.hasDrafts === true
        ) {
            var existingBtn = $('#order_finalize_drafts');
            if(existingBtn.length > 0) {
                existingBtn.remove();
            }

            $(saveButtonHtml).appendTo($('.pull-right', '.content-window-toolbar'));
            self.bindFinalizeEvent(response.finalizeUrl);
        }
    });
};

Order.prototype.bindFinalizeEvent = function(url) {
    var self = this;

    $('#order_finalize_drafts').on('click', function (e) {
        e.preventDefault();

        swal({
            title: 'Opletten',
            type: 'warning',
            html: `<p>Let op: Na het definitief maken van de orders ontvangt de klant een bevestigings bericht met hierin (eventueel) een betaallink.</p>
                    <p>Reden: <small><i>(Wordt ook met de email meegezonden naar de klant...)</i></small></p>`,
            confirmButtonText: 'Maak gewijzigde order(s) definitief',
            confirmButtonColor: '#EF5350',
            showCancelButton: true,
            cancelButtonText: 'Annuleren',
            showLoaderOnConfirm: true,
            input: 'textarea',
            inputAutoTrim: true,
            inputValidator: function (finalize_message) {
                return new Promise(function (resolve, reject) {
                    if (finalize_message) {
                        resolve()
                    } else {
                        reject('Reden veld mag niet leeg zijn!');
                    }
                })
            },
            preConfirm: function (finalize_message) {
                return new Promise(function (resolve) {
                    $.ajax({
                        url: url,
                        method: 'post',
                        data: {
                            finalize_message: finalize_message
                        }
                    }).done(function(response) {
                        self.contentWindow.refresh();

                        var html = '<p>De order is met success definitief gemaakt</p>',
                            textInvoice = '<p>- Op rekekening!: Bevestigingsemail <strong>zonder</strong> betalingsverzoek verstuurd.</p>',
                            textEmail = '<p>- Bevestigingsemail is verstuurd.</p>',
                            textPaymentRequestSent = '<p>- Een betalingsverzoek is verstuurd.</p>',

                            hasInvoice = response.hasOwnProperty('invoice') && response.invoice === true,
                            emailSent = response.hasOwnProperty('emailSent') && response.emailSent === true,
                            paymentRequestSent = response.hasOwnProperty('paymentRequestSent')
                                && response.paymentRequestSent === true;

                        html = (hasInvoice) ? html + textInvoice : html;
                        html = (emailSent) ? html + textEmail : html;
                        html = (paymentRequestSent) ? html + textPaymentRequestSent : html;

                        swal({
                            type: 'success',
                            title: 'Gelukt',
                            html: html
                        });
                    }).fail(function() {
                        swal({
                            type: 'error',
                            title: 'Helaas is er iets misgegaan...',
                            text: 'De gewijzigde orders zijn niet (helemaal) definitief gemaakt.'
                        });
                    });
                    resolve();
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
        .catch(swal.noop);
    });
};

Order.prototype.initializePopovers = function () {
    $('[data-toggle="popover"]', this.contentWindow.elm).popover()
};

Order.prototype.initializeTooltips = function () {
    $(this.contentWindow.elm).tooltip({
        selector: '[data-popup=tooltip]'
    });
};

Order.prototype.initializeSwitchery = function () {
    var input = $('input.switchery', this.contentWindow.elm);

    input.on('change', function () {
        if ($(this).is(':checked')) {
            $('.excl-vat').hide();
            $('.incl-vat').show();
        } else {
            $('.excl-vat').show();
            $('.incl-vat').hide();
        }
    }).trigger('change');

    new Switchery(input.get(0));
};

Order.prototype.triggerInclExclSwitch = function () {
    $('input.switchery', this.contentWindow.elm).trigger('change');
};

Order.prototype.showOrder = function (orderId) {
    var self = this;
    var contentWindow = this.contentWindow.elm;

    $('a[data-action="collapse"]').show();

    if (!orderId) {
        orderId = this.orderId ? this.orderId : $('#order-orders').find('.order-order').last().attr('data-order-id');;
    }

    $('.panel-collapsed').children('.panel-heading').nextAll().hide();

    if (orderId) {

        if (this.orderXhr && typeof this.orderXhr.abort === 'function') {
            this.orderXhr.abort();
        }

        this.orderXhr = $.ajax({
            url: $('#order-orders').attr('data-order-order-url'),
            data: {
                order: orderId
            },
            method: 'POST',
        }).then(function (response) {
            $('.order-order .panel-body, .order-order .panel-footer').remove();
            $('.order-order[data-order-id="' + orderId + '"]').append(response);

            self.showOrderActivity(orderId);
            self.triggerInclExclSwitch();
        });
    }
};

Order.prototype.showOrderActivity = function (orderId) {
    var activityPath = $('.order-order[data-order-id="' + orderId + '"]').find('.panel-body').attr('data-activity-url');
    this.loadActivities(activityPath);
};

Order.prototype.loadActivities = function (url) {
    var orderActivityElm = $('.order-activity');

    $.ajax({
        url: url,
        method: 'GET',
    }).then(function (response) {
        orderActivityElm.find('table').html(response);
        $('body [data-toggle="popover"]').popover()
    });
};

Order.prototype.scrollTo = function () {
    if (this.url.match(/[0-9]*$/)) {
        $(' > div', this.contentWindow.elm).scrollTo($('a[name="' + this.url.match(/[0-9]*$/) + '"]'), {
            offset: -57
        });
    }
};

$(function () {
    $('body').on('click', '[data-action="view-order"]', function (e) {
        e.preventDefault();

        new Order($(this).attr('data-href'));
    });
});
