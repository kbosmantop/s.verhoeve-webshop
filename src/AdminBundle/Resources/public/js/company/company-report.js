$(function () {
    initializeDatatables();

    var panel = $('#companycustomer-panel');

    panel.find('a[data-type="add"]').on('click', function (e) {
        openWindow($(this), e);
    });

    function openWindow(obj, e) {
        e.preventDefault();

        var url = obj.attr('href') + '?companyReport=' + obj.closest('#companycustomer-panel').attr('data-company-report-id');
        obj.attr('href', url);
        obj.attr('data-hide-callback', 'reloadTables');

        openContent(obj);
    }
});

function reloadTables() {
    var panel = $('#companycustomer-panel');
    var datatable = panel.find('.companyReportDatatablePanel').find('table').attr('id');

    $('#' + datatable).dataTable().api().ajax.reload();
}