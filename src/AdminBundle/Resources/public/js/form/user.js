$(document).ready(function () {
    $('#user_container_user_super_admin').on('change', function (e) {
        var state = $(this).is(':checked');

        $('#container_roles').toggle(!state);
    }).trigger('change');

    $('#user_groups').on('change', 'input[type=checkbox]', function (e) {
        processGroups();
    });

    processGroups();

    function processGroups() {
        var groups = [];

        $('#user_groups :checked').each(function (i, elm) {
            groups.push($(elm).val());
        });

        $.each($('.role-inherited-by-group input[type=checkbox]'), function (i, checkbox) {
            if (!$(checkbox).parents('.checkbox').hasClass('role-user')) {
                $(checkbox)
                .attr('checked', false)
                .prop('checked', false);
            }
        });

        $('.checkbox').removeClass('role-inherited-by-group');

        $.uniform.update();

        $.ajax({
            url: '/admin/instellingen/authenticatie/groepen/privileges',
            method: 'POST',
            dataType: 'json',
            data: {
                groups: groups
            }
        }).done(function (response) {
            if (response) {
                $.each(response, function (group, roles) {

                    $.each(roles, function (i, role) {
                        var checkbox = $('input[type=checkbox][value=' + role + ']', $('.roles-widget'))

                        if ($(checkbox).is(':checked')) {
                            $(checkbox).parents('.checkbox').addClass('role-user');
                        }

                        $(checkbox)
                        .attr('checked', true)
                        .prop('checked', true);

                        $(checkbox).parents('.checkbox').addClass('role-inherited-by-group');
                    });
                });
            }

            $.uniform.update();
        });
    }
});
