$(function () {
    var selectedProductsInput = $('#selectedProducts');
    var selectedProducts = JSON.parse(selectedProductsInput.val());

    $('form').on('submit', function(e) {
        $('input[type="submit"]').attr('disabled', 'disabled');
    });

    $('.col-product-item').on('click', function () {
        var elm = $(this);
        var productId = $(this).attr('data-product-id');

        $.ajax({
            url: $('.configurationHolder').attr('data-variation-url'),
            method: 'POST',
            data: {
                product: productId
            }
        }).then(function (data) {
            if (data.length) {
                var variations = $('<div class="variations"></div>');

                $.each(data, function (index) {
                    var variationPrice = "" + data[index].price.toFixed(2);
                    var variation = $('<div class="variation" data-variation-id="' + data[index].id + '">' +
                        '<div class="variation-title">' + data[index].title + '</div>' +
                        '<div class="variation-price">' + variationPrice.replace('.', ',') + '</div>' +
                        '</div>');

                    variations.append(variation);
                });

                swal({
                    type: 'question',
                    title: 'Selecteer een variatie',
                    html: variations,
                    showCancelButton: true,
                });

                $('.variation').on('click', function () {
                    productId = $(this).attr('data-variation-id');
                    swal.close();

                    var product = {};

                    product.id = $(this).attr('data-variation-id');
                    product.title = $(this).find('.variation-title').html();
                    product.price = parseFloat($(this).find('.variation-price').html());

                    if(isNaN(product.price)) {
                        product.price = 0.00;
                    }

                    selectedProducts.push(product.id);
                    selectedProductsInput.val(JSON.stringify(selectedProducts));

                    var productPrice = "" + product.price.toFixed(2);

                    var variation = $('<div class="variation-holder" data-variation-id="' + product.id + '">' +
                        '<div class="variation-title">' + product.title + '</div>' +
                        '<div class="variation-price">' + productPrice.replace('.', ',') + '</div>' +
                        '</div>');

                    $('.selected-variations').append(variation);
                });
            } else {
                var product = {};

                product.id = productId;
                product.title = elm.find('.product-item-description').text();
                product.price = parseFloat(elm.find('.product-item-price').html());

                if(isNaN(product.price)) {
                    product.price = 0.00;
                }

                selectedProducts.push(productId);
                selectedProductsInput.val(JSON.stringify(selectedProducts));

                var productPrice = "" + product.price.toFixed(2);
                var variation = $('<div class="variation-holder" data-variation-id="' + product.id + '">' +
                    '<div class="variation-title">' + product.title + '</div>' +
                    '<div class="variation-price">' + productPrice.replace('.', ',') + '</div>' +
                    '</div>');

                $('.selected-variations').append(variation);
            }
        });

    })
});