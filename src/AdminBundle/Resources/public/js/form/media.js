$(document).ready(function () {
    $('#image_format_container_general_column_left_method').on('change', function (e) {
        $('.transformer-identifier-toggle').closest('.form-group').hide();

        var value = $(this).val();

        var transformerIdentifier = value.split('\\').pop().toLowerCase();
        var transformerIdentifierClass = 'transformer-identifier-' + transformerIdentifier;

        var form = $(this).closest('form');

        $(form).find('.' + transformerIdentifierClass).closest('.form-group').show();
    }).trigger('change');

    $('#image_format_container_general_column_left_extension').on('change', function (e) {
        $('#image_format_container_general_column_left_quality').closest('.form-group').hide();

        var value = $(this).val();

        if (value == 'jpg') {
            var form = $(this).closest('form');

            $(form).find('#image_format_container_general_column_left_quality').closest('.form-group').show();
        }
    }).trigger('change');
});
