function ProductWizard() {
    this.init();
    this.step = 1;
}

ProductWizard.prototype.init = function () {
    var self = this;

    $('#products').multiselect({
        enableCaseInsensitiveFiltering: true,
        enableClickableOptGroups: true,
        nonSelectedText: 'Geen selectie',
        selectAllText: 'Selecteer alles',
        allSelectedText: 'Alles geselecteerd',
        filterPlaceholder: 'Typ om te zoeken',
        numberDisplayed: 1,
        buttonClass: 'btn btn-xs btn-default',
        templates: {
            divider: '<div class="divider" data-role="divider"></div>',
            filter: '<li class="multiselect-itfrem multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        }
    });

    $('#select-products').on('click', function () {
        new ProductSelection($(this).data('product-url'));
    });

    $('#producttype').on('change', function (e) {
        var productGroupPanel = $('#productgroup_panel');
        var productsPanel = $('#products_panel');
        var state = $('#producttype').val() === 'combination';

        if(!$(this).val()) {
            productGroupPanel.hide();
            productsPanel.hide();
        } else {
            productGroupPanel.toggle(!state);
            productsPanel.toggle(state);
        }
    });

    $('#productgroup').select2();

    $('#productgroup').on('change', function () {
        var productgroup = $(this).val();
        var url = $(this).closest('form').attr('data-set-url');
        $.ajax({
            data: {
                productgroup: productgroup
            },
            url: url,
            method: 'POST'
        }).then(function (response) {
            $('#product_set').html('');
            $('#product_info_step').show();

            if (response['sets'].length > 0) {
                for (var propertySet in response['sets']) {
                    $('#product_set').append('<option value="' + response['sets'][propertySet]['id'] + '">' + response['sets'][propertySet]['name'] + '</option>')
                }
            } else {
                $('#product_set').append('<option value="">Geen sets beschikbaar</option>')
            }
        });
    });

    $('#products').on('change', function () {
        $('#productgroup_panel').show();
    });

    $('#product_name').on('change', function () {
        $('#wizard_submit').removeAttr('disabled');
    });

    $('#selectedProducts').on('change', function () {
        var products = JSON.parse($(this).val());
        var state = products.length > 1;

        $('#productgroup_panel').toggle(state);
    });

};

new ProductWizard();
