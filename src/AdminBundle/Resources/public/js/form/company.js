function Company() {
    this.init();
}

Company.prototype.init = function () {
    this.bindEvents();
};

// @todo: move to leveranciers content window callback
Company.prototype.bindEvents = function () {
    var viewAction = $('.view-action');

    viewAction.on('click', 'button[data-event="get-order"]', function (e) {
        e.preventDefault();

        var href = $(this).data('href');
        var orderId = $(this).data('order-id');

        new Order(href, orderId);
    });

    viewAction.on('click', 'a[data-type="create-business-report"]', function (e) {
        e.preventDefault();

        new RuleCreate($(this).attr('href'), function () {
            $('.content-window').hide();
            $('[data-tab-action="reports"]').trigger('click');
        });
    });

    viewAction.on('click', 'a[data-type="link-business-report"]', function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var companyId = $(this).closest('.view-action').attr('data-entity-id');

        $.ajax({
            url: url,
            method: 'GET'
        }).then((result) => {
            var options = {};
            $.map(result, function (r) {
                options[r.id] = r.name;
            });

            swal({
                type: 'question',
                input: 'select',
                inputOptions: options,
                title: 'Welk profiel wil je overnemen?'
            }).then((result) => {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        company: companyId,
                        report: result
                    }
                }).then(() => {
                    swal('Gelukt!', 'Profiel succesvol gekoppeld.', 'success')
                    $('[data-tab-action="reports"]').trigger('click');
                }).error(() => {
                    swal('Foutmelding!', 'Er is iets misgegaan met het koppelen van het profiel', 'error')
                });
            });
        })
    });

    viewAction.on('click', 'a[data-action="add-business-rule"]', function (e) {
        e.preventDefault();

        new RuleCreate($(this).attr('href'), function () {
            $('.content-window').hide();
            $('[data-tab-action="catalog"]').trigger('click');
        });
    });

    viewAction.on('click', 'a[data-action="create-discount-existing-rule"]', function (e) {
        e.preventDefault();

        var url = $(this).attr('href');
        var companyId = $(this).closest('.view-action').attr('data-entity-id');
        $(this).attr('href', url + '?company=' + companyId);

        openContent($(this));
    });

    viewAction.on('click', 'a[data-action="company-report-delete"]', function (e) {
        e.preventDefault();

        swal({
            type: 'warning',
            title: 'Let op!',
            text: 'Weet je zeker dat je deze regel wilt verwijderen?',
            showCancelButton: true
        }).then(() => {
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                method: 'POST',
            }).then(() => {
                swal('Gelukt!', 'Regel succesvol verwijderd', 'success');
                $('[data-tab-action="catalog"]').trigger('click');
            })
        });
    });

    viewAction.on('click', 'a[data-action="create-discount-rule"]', function (e) {
        e.preventDefault();

        var self = $(this);

        var selectedDiscount = '';
        var discountName = '';
        swal.queue([
            {
                type: 'question',
                text: 'Naam van de korting (zichtbaar voor klant)',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Volgende',
                preConfirm: (value) => {
                    return new Promise((resolve) => {
                        discountName = value;
                        resolve(value);
                    });
                }
            },
            {
                type: 'question',
                text: 'Waar wil je korting op geven?',
                input: 'select',
                inputOptions: {
                    'product': 'Product',
                    'cart': 'Winkelwagen'
                },
                showCancelButton: true,
                confirmButtonText: 'Volgende',
                preConfirm: (value) => {
                    return new Promise((resolve) => {
                        selectedDiscount = value;
                        resolve(value);
                    });
                }
            },
            {
                type: 'question',
                html: `Hoeveel korting wil je geven?<br/><br/>
                        <input type="text" id="discount_amount"/>
                        <select id="discount_type">
                            <option value="percentage">Procent</option>
                            <option value="amount">Euro</option>
                        </select>`,
                showCancelButton: true,
                confirmButtonText: 'Volgende',
                onOpen: function() {
                    if(selectedDiscount == "product") {
                        $('#discount_type').find('option[value="amount"]').attr('disabled', 'disabled');
                    }
                },
                preConfirm: () => {
                    return new Promise((resolve, reject) => {
                        var amount = $('#discount_amount');
                        if(!amount || !$.isNumeric(amount.val())) {
                            reject('De ingevoerde waarde is ongeldig');
                        } else {
                            resolve([
                                $('#discount_amount').val(),
                                $('#discount_type').val()
                            ]);
                        }
                    });
                }
            },
            {
                type: 'question',
                html: 'Tot wanneer geldt de korting?<br/>' +
                    'Als de einddatum niet is ingevoerd geldt de korting tot deze verwijderd wordt.<br/><br/><input id="discount_datepicker">',
                customClass: 'swal2-overflow',
                showCancelButton: true,
                onOpen: function() {
                    $('#discount_datepicker').datepicker({
                        beforeShow: function() {
                            setTimeout(function(){
                                $('.ui-datepicker')[0].style.setProperty('z-index', '9000', 'important');
                            }, 0);
                        }
                    });
                },
                preConfirm: () => {
                    return new Promise((resolve, reject) => {
                        var amount = $('#discount_datepicker');
                        if(amount.val()) {
                            resolve(amount.val());
                        } else {
                            resolve();
                        }
                    });
                }
            }
        ]).then((result) => {
            $.ajax({
                url: self.attr('href'),
                method: 'POST',
                data: {
                    start: result[1],
                    type: result[2][1],
                    value: result[2][0],
                    date: typeof result[3] === 'string' ? result[3] : 0,
                    company: viewAction.attr('data-entity-id'),
                    name: result[0]
                },
            }).done((result) => {
                new RuleCreate(result.url, function() {
                    window.location.reload();
                }, function() {
                    $.ajax({
                        method: 'DELETE',
                        url: result.cancelUrl
                    });
                });
            });
        });
    })
};


new Company();