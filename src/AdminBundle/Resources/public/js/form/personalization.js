var productImageDragTimer;
var productImageQueueErrors = [];
var productImageMinDimensions = null;

$(function () {
    $('.slugify').find('input').on('change', function () {
        let slug = $(this).val();
        let self = $(this);
        $.ajax({
            url: '/admin/slug/is-uniek',
            method: 'POST',
            data: {
                slug: slug
            }
        }).then((result) => {

            $('#product_submit, #product_submit_close').attr('disabled', false);
            self.closest('.form-group').removeClass('has-error');
            if (result.message === 'error') {
                self.closest('.form-group').addClass('has-error');
                $('#product_submit, #product_submit_close').attr('disabled', true);
            }
        })
    });

    $('.slugifyTitle').on('change', function () {
        $(this).closest('.row').find('.slugify').find('input').trigger('change');
    });
});

function toggleCollectionAndUploader(type) {
    if (type === 'showUploader') {
        $('.product-image-container .collection-items').hide();
        $('.product-image-container .product-image-uploader').fadeIn();
    } else {
        $('.product-image-container .product-image-uploader').fadeOut();
        $('.product-image-container .collection-items').show();
    }
}

function initializeUpload() {
    $('.product-image-container .btn-upload-image').on('click', function (e) {
        e.preventDefault();

        toggleCollectionAndUploader('showUploader');
    });

    $('a[href="#product_container_product_tab_product_images"]').on('shown.bs.tab', function (e) {
        if ($('.collection-item', '.product-image-container').length === 0) {
            $('.product-image-container .btn-upload-image').trigger('click');
        }
    })

    var uploaderOptions = {
        // General settings
        runtimes: 'html5',
        url: $('.product-image-uploader').attr('data-url'),
        unique_names: true,
        filters: {
            mime_types: [
                {
                    title: 'Image files',
                    extensions: 'jpg,gif,png'
                }
            ]
        }
    };

    if ($('.media-widget').attr('data-dimensions')) {
        var dimensions = JSON.parse($('.media-widget').attr('data-dimensions'));

        if (dimensions.min) {
            productImageMinDimensions = dimensions.min;

            uploaderOptions.filters.min_dimensions = [productImageMinDimensions[0], productImageMinDimensions[1]];
        }
    }

    if ($('.product-image-uploader').length > 0) {
        $('.product-image-uploader').pluploadQueue(uploaderOptions);

        $('.product-image-uploader').pluploadQueue().bind('Error', function (uploader, error) {
            var plupload;
            if (error.code === plupload.IMAGE_DIMENSIONS_ERROR) {
                productImageQueueErrors['' + error.file.id + ''] = error.message;
            }
        });

        $('.product-image-uploader').pluploadQueue().bind('FilesAdded', function (uploader, files) {
            for (var i = 0; i < files.length; i++) {
                if (productImageQueueErrors[files[i].id]) {
                    $('<span class="file-filter-error icon-warning" data-popup="popover" data-trigger="hover" title="Waarschuwing" data-content="Afbeelding voldoet niet aan de minimum afmetingen: ' + productImageMinDimensions.join(' x ') + '"></span>').insertBefore($('.plupload_file_name span', $('#' + files[i].id)));
                }
            }

            $('[data-popup="popover"]', $('.plupload_file_name')).popover();
        });

        $('.product-image-uploader').pluploadQueue().bind('FileUploaded', function (uploader, file, response) {
            // add collection row behind the scenes
            var row_id = collectionAddRow($('.product-image-container').find('.collection-items'));

            var result = JSON.parse(response.response);

            var row = $('#' + row_id);

            row.find('input[name*=\'path\']').val(result.file_path);

            row.find('.media-widget-thumbnail-image .centered').append('<img />');
            row.find('.media-widget-thumbnail-image img').attr('src', result.thumbnail_path);

            row.find('.media-widget-thumbnail-container').show();
        });

        $('.product-image-uploader').pluploadQueue().bind('UploadComplete', function (uploader, file, response) {
            // hide the uploader and show the collection rows
            toggleCollectionAndUploader('hideUploader');

            // Remove all files from queue
            uploader.splice();

            if (uploader.total.uploaded === uploader.files.length) {
                $('.plupload_buttons').css('display', 'inline');
                $('.plupload_upload_status').css('display', 'none');
                $('.plupload_start').addClass('plupload_disabled');
            }

            uploader.refresh();

            if ($('.collection-item', '.product-image-container').length === 1) {
                $('.image-application-main', $('.collection-item', '.product-image-container')).attr('checked', true).prop('checked', true);

                $.uniform.update();
            }
        });

        $(document).on('dragover', function (e) {
            var dt = e.originalEvent.dataTransfer;
            if (dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') !== -1 : dt.types.contains('application/x-moz-file'))) {
                toggleCollectionAndUploader('showUploader');

                window.clearTimeout(productImageDragTimer);
            }
        });
        $(document).on('dragleave', function (e) {
            productImageDragTimer = window.setTimeout(function () {
                toggleCollectionAndUploader('hideUploader');
            }, 25);
        });

        $('.product-image-container').on('change', '.image-application-main, select[data-name="productVariations"]', function (e) {
            var container = $('.product-image-container');
            var currentRow = $(this).closest('.collection-item').index();
            var variations = [];

            if ($(this).hasClass('image-application-main')) {
                var select = $(this).parents('.collection-item').find('select[data-name="productVariations"]');
                var checkbox = $(this);
            } else {
                var select = $(this);
                var checkbox = $(this).parents('.collection-item').find('.image-application-main');
            }

            if ($(select).find('option:selected').length === 0) {
                $('.collection-item', '.product-image-container').find('.image-application-main').not(checkbox).attr('checked', false).prop('checked', false);

                $.uniform.update();

                return;
            } else {
                $.each($('.collection-item', '.product-image-container').find('select[data-name="productVariations"]').not(select), function (i, elm) {
                    if ($(elm).find('option:selected').length === 0) {
                        $(elm).parents('.collection-item').find('.image-application-main').attr('checked', false).prop('checked', false);
                    }
                });

                $.uniform.update();

                return;
            }

            $.each($('.collection-item', '.product-image-container').find('select[data-name="productVariations"]'), function (i, elm) {
                $.each($(elm).find('option:selected'), function (y, option) {
                    if (!variations[$(option).attr('value')]) {
                        variations[$(option).attr('value')] = [];
                    }

                    variations[$(option).attr('value')].push(i);
                });
            });

            var variationsToDeselect = [];

            $.each($(this).closest('.collection-item', '.product-image-container').find('select[data-name="productVariations"]').find('option:selected'), function (i, elm) {
                variationsToDeselect.push($(this).attr('value'));
            });

            $.each(variationsToDeselect, function (i, variationId) {
                if (variations[variationId]) {
                    $.each(variations[variationId], function (i, row) {

                        if (row !== currentRow) {
                            $('.collection-item', '.product-image-container').eq(row).find('.image-application-main').attr('checked', false).prop('checked', false);
                        }
                    });
                }
            });

            $.uniform.update();
        });

        $('.product-image-container').on('change', '.image-application-category', function (e) {
            var container = $('.product-image-container');

            var state = $(this).is(':checked');

            $.each($(container).find('.image-application-category'), function (i, elm) {
                $(elm).attr('checked', false).prop('checked', false);
            });

            if (state) {
                $(this).attr('checked', true).prop('checked', true);
            }

            $.uniform.update();
        });

        $('.btn-remove', '#product_container_product_tab_product_variations .collection-item').on('click', function (e) {
            var images_container = $('#product_container_product_tab_product_images');
            var suppliers_container = $('#product_container_product_tab_product_supplier');
            var row = $(this).closest('.collection-item');
            var variation_id = $(row).attr('data-id');
            var variation_images_found = [];
            var variation_suppliers_found = [];

            $('select[data-name="productVariations"]', images_container).each(function (i, elm) {
                $(elm).find('option').each(function (y, option) {
                    if ($(option).is(':selected') && $(option).val() === variation_id) {
                        variation_images_found.push(option);
                    }
                });
            });

            $('select[data-name="productVariations"]', suppliers_container).each(function (i, elm) {
                $(elm).find('option').each(function (y, option) {
                    if ($(option).is(':selected') && $(option).val() === variation_id) {
                        variation_suppliers_found.push(option);
                    }
                });
            });

            if (variation_images_found.length === 0 && variation_suppliers_found.length === 0) {
                $('a[href="#product_container_product_tab_product_images"]').find('.label-info').remove();
                $('a[href="#product_container_product_tab_product_supplier"]').find('.label-info').remove();

                return true;
            }

            var modalHtml = '<p>De conflicten moeten opgelost worden voordat de variatie verwijderd kan worden.</p>';

            swal({
                title: 'Conflict gevonden',
                type: 'info',
                html: modalHtml,
            }).then(function () {
                $('a[href="#product_container_product_tab_product_images"]').find('.label-info').remove();
                $('a[href="#product_container_product_tab_product_supplier"]').find('.label-info').remove();
                $('select[data-name="productVariations"]', images_container).toggleClass('border-info', false);
                $('select[data-name="productVariations"]', suppliers_container).toggleClass('border-info', false);

                if (variation_images_found.length > 0) {
                    var badge = '<span class="label label-info pull-right">' + variation_images_found.length + ' actie(s)</span>';

                    $(badge).prependTo($('a[href="#product_container_product_tab_product_images"]'));

                    for (var i = 0; i < variation_images_found.length; i++) {
                        $(variation_images_found[i]).closest('.multi-select-full').find('button.multiselect').toggleClass('border-info', true);
                    }
                }

                if (variation_suppliers_found.length > 0) {
                    var badge = '<span class="label label-info pull-right">' + variation_suppliers_found.length + ' actie(s)</span>';

                    $(badge).prependTo($('a[href="#product_container_product_tab_product_supplier"]'));

                    for (var i = 0; i < variation_suppliers_found.length; i++) {
                        $(variation_suppliers_found[i]).closest('.multi-select-full').find('button.multiselect').toggleClass('border-info', true);
                    }
                }
            });

            return false;
        });
    }
}

initializeUpload();

$(function () {
    $('.panel').on('mouseover', '.thumb', function () {
        $('.foto-groot').find('img').attr('src', $(this).find('img').attr('src'));
        $('.fa-caret-up.active').removeClass('active');
        $(this).find('span').addClass('active');
    });

    $('.view-action').on('click', 'a[data-type="add"]', function (e) {
        e.preventDefault();

        var productId = $(this).closest('.view-action').find('.product-container').attr('data-product');

        $(this).attr('href', $(this).attr('href') + '?product=' + productId);

        new Content($(this).attr('href'));
    });

    $('.view-action').on('click', 'a[data-type="edit"]', function (e) {
        e.preventDefault();
        new Content($this.attr('href'));
    });

    $('body').on('change', '[data-unique]', function () {
        var self = $(this);
        self.parent().removeClass('has-error');
        $.ajax({
            url: self.attr('data-unique-action'),
            method: 'POST',
            data: {
                productgroupPropertyId: self.attr('id').split('_').pop(),
                value: self.val()
            }
        }).then(function (response) {
            if (!response.unique) {
                swal('Waarde moet uniek zijn', 'De opgegeven waarde is niet toegestaan, dit veld moet uniek zijn ten opzichte van andere producten', 'error');
                setTimeout(function () {
                    self.parent().addClass('has-error');
                }, 100);
            }
        });
    });
});



