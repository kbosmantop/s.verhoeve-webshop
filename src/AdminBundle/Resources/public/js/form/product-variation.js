$(function() {
    var bbe = $('#_bbeChoice');
    var serial = $('#_serialnumberChoice');

    $('[id*="_expirationMeta"]').each(function() {
        if($(this).val().length) {
            bbe.val(1);
            bbe.trigger('change');
        }
    })

    $('[id*="_serialNumberMeta"]').each(function() {
        if($(this).val().length) {
            serial.val(1);
            bbe.trigger('change');
        }
    })

    bbe.trigger('change');
    serial.trigger('change');

    bbe.on('change', function() {
        toggleRequiredFields($(this), '_expirationMeta')
    });

    serial.on('change', function() {
        toggleRequiredFields($(this), '_serialNumberMeta')
    });

    function toggleRequiredFields(elm, id) {
        var state = parseInt(elm.val());
        var fields = $('[id*='+id+']');

        if(state) {
            fields.attr('required', true);
        } else {
            fields.removeAttr('required');
        }
    }
});