$(function () {
    var optionContainer = $('.productgroup_property_container_details_column3_options_form_group');
    var productContainer = $('#productgroup_property_container_details_column3_product').closest('.form-group');
    var assortmentContainer = $('#productgroup_property_container_details_column3_assortment').closest('.form-group');


    $('#productgroup_property_container_details_column2_form_type_options').select2({
        placeholder: 'Selecteer een optie'
    });

    $('#productgroup_property_container_details_column2_form_type').on('change', function () {
        productContainer.hide();
        assortmentContainer.hide();
        optionContainer.hide();

        if ($(this).val() === 'choice') {
            optionContainer.show();
        } else if ($(this).val() === 'entity') {
            productContainer.show();
        } else if ($(this).val() === 'assortment') {
            assortmentContainer.show();
        }
    });
});


