$(function () {
    var prefix = 'company_establishment_container_establishment_container_container_establishment_';


    if($('#' + prefix + 'general_column_name').length) {
        var form = $('form');
        var establishmentNameInput = $('#' + prefix + 'general_column_name');

        establishmentNameInput.on('change', function () {
            if ($(this).val()) {
                $('input[id^=' + prefix + ']').attr('readonly', false);
                $('.establishmentNumberInput').attr('readonly', 'readonly').val('').closest('.form-group').removeClass('has-error');
            } else {
                setReadOnly();
            }
        });
        var form = $('form');

        if ($('.establishmentNumberInput').val()) {
            setReadOnly();
        }

        $('.view-action ').on('click', 'a[data-action="admin_company_companyestablishment_add"]', function (e) {
            e.preventDefault();

            new Content($(this).attr('href') + '?company=' + $(this).closest('.view-action').attr('data-entity-id'));
        });

        $('.establishmentNumberInput').on('change', function () {
            if (!$('.establishmentNumberInput').val()) {
                $('input[id^=' + prefix + ']').attr('readonly', false);
                return;
            }

            var self = $(this);

            $.ajax({
                url: self.data('establishment-url'),
                data: {
                    company: $('#company_establishment_container_establishment_container_container_establishment_general_column_company').val(),
                    establishmentNumber: self.val()
                }
            }).done(function (data) {
                $('#' + prefix + 'general_column_name').val(data.name);
                $('#' + prefix + 'address_column_address_street').val(data.address.street);
                $('#' + prefix + 'address_column_address_number').val(data.address.houseNumber);
                $('#' + prefix + 'address_column_address_postcode').val(data.address.postcode);
                $('#' + prefix + 'address_column_address_city').val(data.address.city);
                $('#' + prefix + 'address_column_address_country option[value="NL"]').attr('selected', 'selected');

                setReadOnly();

                self.closest('.form-group').removeClass('has-error has-success').addClass('has-success')
            }).fail(function () {
                $('input[id^=' + prefix + ']').attr('readonly', false);
                self.closest('.form-group').removeClass('has-error has-success').addClass('has-error');
            });
        });

        var searchParams = new URLSearchParams(window.location.search);
        form.attr('id', 'company_establishment');

        $.validate();

        //prevent default form handler, handle with ajax instead to redirect back to company edit screen
        form.submit(function (e) {
            e.preventDefault();
            if (!form.isValid()) {
                return false;
            }

            var redirectUrl = '/admin/klanten/bedrijven/' + $('#' + prefix + 'general_column_company').val() + '/bewerken#company_company_container_company_establishments_tab';

            $.ajax({
                url: $(this).attr('action'),
                method: 'post',
                data: $(this).serialize()
            }).done(function () {
                window.location.href = redirectUrl;
            }).fail(function () {

            });
        });

        if (searchParams.has('id')) {
            var id = searchParams.get('id');
            $('#' + prefix + 'general_column_company').val(id).trigger('change');
        }

        function setReadOnly() {
            $('input[id^=' + prefix + ']').attr('readonly', 'readonly');
            $('#company_establishment_container_establishment_container_container_establishment_general_column_establishmentNumber, #company_establishment_container_establishment_container_container_establishment_address_column_address_description, #company_establishment_container_establishment_container_container_establishment_address_column_address_attn').attr('readonly', false);
        }
    }
});

