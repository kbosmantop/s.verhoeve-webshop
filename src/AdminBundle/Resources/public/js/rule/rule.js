$(function () {
    $(document).on('click', 'a[data-type="add-rule"]', function (e) {
        e.preventDefault();

        new RuleCreate($(this).attr('href'));
    });
});