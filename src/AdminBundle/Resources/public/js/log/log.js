var Log = function (url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.9 + 'px',
        zIndex: 2000
    });

    this.open(url);
};

Log.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);
        self.bindEvents();
    });
};

Log.prototype.bindEvents = function () {
    $('a[data-action="collapse"]', this.elm).on('click', function () {
        $(this).closest('.panel').toggleClass('panel-collapsed').find('.panel-body:first.collapsable').stop().slideToggle();
    });
};
