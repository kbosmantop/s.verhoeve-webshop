/**
 * @todo implement http://demo.interface.club/limitless/layout_4/LTR/default/extension_blockui.html
 */

var ContentWindowCollection = [];

function ContentWindow(options) {
    var self = this;
    var tooltipContainers = $('.tooltip[role="tooltip"]:visible');

    ContentWindowCollection.push(self);

    if (tooltipContainers.length >= 1) {
        tooltipContainers.remove();
    }

    var zIndex = 1000;

    this.cancelButtonClicked = false;

    $('.content-window').each(function () {
        if ($(this).css('zIndex') >= zIndex) {
            zIndex = $(this).css('zIndex') + 1;
        }
    });

    var optionsParams = {
        id: null,
        width: '750px',
        zIndex: zIndex,
        refreshUrl: null,
        refreshCallback: null,
        hideCallback: null,
        hideCallbackOnSubmit: null,
        afterErrorCallback: null,
        successCallback: null,
        hideBackground: false,
        toolbar: true,
        toolbarButtons: [],
        disableCloseButton: false,
        closeButtonLabel: 'Venster sluiten',
        disableCloseAllButton: false,
        scripts: []
    };

    this.formChange = false;
    this.options = $.extend(true, {}, optionsParams, options);

    var className = 'content-window';
    if (this.options.hideBackground) {
        className += ' without-background';
    }

    this.elm = $('\
        <div' + (this.options.id ? ' id="' + this.options.id + '"' : '') + ' class="' + className + '" style="z-index: ' + this.options.zIndex + ';">\
            <span class="content-window-loading">\
                <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>\
            </span>\
            <div class="content-window-body"></div>\
        </div>\
    ');

    this.elm.prependTo('body');

    this.elm.on('click', function (e) {
        if (e.delegateTarget !== e.target) {
            return true;
        }

        if (!self.isDirty()) {
            self.hide();
        }
    });
}

ContentWindow.prototype.buildToolbarButton = function (button) {
    var buttonAttributes = [];

    if (button.id) {
        buttonAttributes.push('id="' + button.id + '"');
    }

    if (button.action) {
        buttonAttributes.push('data-action="' + button.action + '"');
    }

    if (button.href) {
        buttonAttributes.push('href="' + button.href + '"');
    }

    if (button.disabled) {
        buttonAttributes.push('disabled="' + button.disabled + '"');
    }

    if (button.classNames) {
        buttonAttributes.push('class="btn btn-xs' + ((button.classNames) ? ' ' + button.classNames : '') + '"');
    }

    if (button.dataAttributes) {
        var dataAttributes = [];

        $.each(button.dataAttributes, function (key, value) {
            dataAttributes.push('data-' + key + '="' + value + '"');
        });

        dataAttributes = dataAttributes.join(' ');

        buttonAttributes.push(dataAttributes);
    }

    var build = '<a ' + buttonAttributes.join(' ') + '>' +
        '' + ((button.icon) ? '<span class="' + button.icon + ((button.label) ? ' position-left' : '') + '"></span>' : '') + ((button.label) ? button.label : '') +
        '</a>';

    return build;
};

ContentWindow.prototype.buildToolbarCloseButton = function () {
    var self = this;

    var html = '\
    <div class="btn-group dropup">\
        <a data-action="cancel" class="btn btn-xs btn-default"><i class="icon-cross3 position-left"></i>' + this.options.closeButtonLabel + '</a>\
        ' + ((!self.options.disableCloseAllButton && ContentWindowCollection.length > 0) ? '\
        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>\
        <ul class="dropdown-menu dropdown-menu-right">\
            <li><a data-action="cancel-all"><i class="icon-stack-cancel position-left"></i> Alle vensters sluiten</a></li>\
        </ul>' : '') + '\
    </div>';

    return html;
};

ContentWindow.prototype.setOption = function (name, value) {
    this.options[name] = value;
};

ContentWindow.prototype.refresh = function () {
    var self = this;

    if (!this.options.refreshUrl) {
        return;
    }

    this.loading();

    $.get(this.options.refreshUrl).done(function (data) {
        self.setHtml(data);

        if (self.options.refreshCallback) {
            self.options.refreshCallback(self);
        }
    });
};

ContentWindow.prototype.show = function () {
    this.elm.fadeIn('fast');

    this.elm.find('.content-window-loading').attr('style', `overflow-y: scroll; width: ${this.options.width}`);
    this.elm.find('.content-window-body').attr('style', `overflow-y: scroll; width: ${this.options.width}`);

    this.renderToolbar();
    this.bindEvents();

    $('body').css('overflow', 'hidden');

    return this;
};

ContentWindow.prototype.renderToolbar = function () {
    var self = this;

    if ($('.content-window-toolbar', this.elm).length === 1) {
        $('.content-window-toolbar', this.elm).remove();
    }

    if (this.options.toolbar && (this.options.toolbarButtons.length > 0 || !this.options.disableCloseButton)) {
        var toolbarButtonsLeft = [];
        var toolbarButtonsRight = [];

        $('' +
            '<div class="content-window-toolbar" style="width: ' + this.options.width + '">\
            <div class="row">\
            <div class="col-md-12">\
            <div class="pull-left">' + this.buildToolbarCloseButton() + '</div>\
            <div class="pull-right"></div>\
            </div>\
            </div>\
            </div>\
        ').appendTo(this.elm);

        this.elm.toggleClass('has-toolbar', true);

        this.options.toolbarButtons.sort(function (a, b) {
            a.order = parseInt(a.order);
            b.order = parseInt(b.order);

            if (a.order < b.order) {
                return -1;
            }

            if (a.order > b.order) {
                return 1;
            }

            return 0;
        });

        $.each(this.options.toolbarButtons, function (i, button) {
            if (button.position === 'left') {
                toolbarButtonsLeft.push(self.buildToolbarButton(button));
            }

            if (button.position === 'right') {
                toolbarButtonsRight.push(self.buildToolbarButton(button));
            }
        });

        if (toolbarButtonsLeft && toolbarButtonsLeft.length > 0) {
            toolbarButtonsLeft = toolbarButtonsLeft.join('');

            $(toolbarButtonsLeft).appendTo($('.content-window-toolbar .pull-left', this.elm));
        }

        if (toolbarButtonsRight && toolbarButtonsRight.length > 0) {
            toolbarButtonsRight = toolbarButtonsRight.join('');

            $(toolbarButtonsRight).appendTo($('.content-window-toolbar .pull-right', this.elm));
        }
    }
};


ContentWindow.prototype.bindEvents = function () {
    var self = this;

    $('a[data-action=cancel]', this.elm).on('click', function (e) {
        e.preventDefault();

        self.cancelButtonClicked = true;
        self.hide();
    });

    $('a[data-action=cancel-all]', this.elm).on('click', function (e) {
        e.preventDefault();

        self.cancelButtonClicked = true;
        self.hideAll();
    });
};

ContentWindow.prototype.fadeOut = function (executeCallback, removeElm) {

    var self = this;
    this.elm.fadeOut('fast', function () {
        if (!$('.content-window:visible').length) {
            $('body').css('overflow', 'auto');
        }

        if (typeof self.options.hideCallback === 'function' && executeCallback !== false) {
            self.options.hideCallback(self);
        }

        if (typeof self.options.hideCallbackOnSubmit === 'function' && executeCallback !== false && !self.cancelButtonClicked) {
            self.options.hideCallbackOnSubmit(self);
        }

        self.cancelButtonClicked = false;

        if (false !== removeElm) {
            $(this).remove();
        }
    });

    return this;
};

ContentWindow.prototype.hide = function (executeCallback, removeElm) {

    var self = this;
    if (this.formChange) {
        swal({
            type: 'question',
            title: 'Wanneer u het venster sluit gaan de wijzigingen verloren',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sluiten',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText: 'Annuleren',
        }).then((result) => {
            if (result) {
                self.fadeOut(executeCallback, removeElm);
            }

            return true;
        }).catch(() => {
            return false;
        });

        return this;
    }

    self.fadeOut(executeCallback, removeElm);

    return this;
};

ContentWindow.prototype.reopen = function () {
    this.elm.fadeIn('fast', function () {
        if (!$('.content-window:visible').length) {
            $('body').css('overflow', 'scroll');
        }
    });

    return this;
};

ContentWindow.prototype.loading = function () {
    $('.content-window-body', this.elm).html('');
    $('.content-window-loading', this.elm).show();
};

ContentWindow.prototype.hideloader = function() {
    $('.content-window-loading', this.elm)
        .removeClass('content-window-loading-with-overlay')
        .hide();
};

ContentWindow.prototype.setHtml = function (html) {
    var self = this;

    var loadingContainer = $('.content-window-loading', this.elm);

    loadingContainer.hide();

    $('.content-window-body', this.elm).html(html);

    if (typeof cms.initMediaElements === "function") {
        cms.initMediaElements();
    }

    var isXhr = (typeof this.elm.find('form').attr('disable-xhr') === typeof undefined);

    if (this.elm.find('form').length > 0) {

        if (typeof(setupFormValidation) !== 'undefined') {
            setupFormValidation({}, true);
        }

        $('form', this.elm).on('change', function () {
            self.formChange = true;
        });

        if (isXhr) {
            self.applyAjaxForm();
        }

        if (self.options.scripts) {
            for (var i = 0; i < self.options.scripts.length; i++) {
                var scriptCb;

                if (self.options.scripts[i].hasOwnProperty('callback')) {
                    scriptCb = self.options.scripts[i].callback;
                }

                var scriptLoaded = $.getScriptOnce(self.options.scripts[i].src, function () {
                    var fnCb = window[scriptCb];

                    if (typeof fnCb === 'function') {
                        fnCb();
                    }
                });

                // If script is already loaded only execute the callback method
                if (scriptLoaded === true) {
                    var fnCb = window[scriptCb];

                    if (typeof fnCb === 'function') {
                        fnCb();
                    }
                }
            }
        }
    }

    return this;
};

ContentWindow.prototype.applyAjaxForm = function() {
    var self = this;
    var loadingContainer = $('.content-window-loading', this.elm);

    var formElm = $('form', this.elm);

    $('#form_errors').remove();

    formElm.ajaxForm({
        beforeSubmit: function (arr, form, options) {

            if(typeof (form.attr('novalidate')) === 'undefined') {

                loadingContainer
                .addClass('content-window-loading-with-overlay')
                .show();

                form.validate();

                var isValid = form.isValid();

                form.find('.multi-select-full:not(.has-error) select[required]').each(function (k, v) {
                    var el = $(v);
                    if (el.val() === null) {
                        el.closest('.multi-select-full').addClass('has-error');
                        el.css({'border-color': 'rgb(185, 74, 72)'});
                        isValid = false;
                    }
                });

                form.find('.ckeditor textarea').each(function (k, v) {
                    var el = $(v);
                    if (typeof (el.attr('data-validation')) !== 'undefined') {
                        var containingDiv = el.closest('.ckeditor');
                        if (el.val().length === 0) {
                            containingDiv.addClass('has-error').css({'border': '1px solid rgb(185, 74, 72)'});
                            isValid = false;
                        } else {
                            containingDiv.removeClass('has-error').css({'border': ''});
                        }
                    }
                });

                if (!isValid) {
                    var elmErrors = self.elm.find(".has-error");

                    if (elmErrors.length > 0) {
                        self.elm.find('.content-window-body').stop().animate({
                            scrollTop: elmErrors.first().offset().top
                        }, 500);
                    }

                    self.hideloader();

                    return false;
                }
            }
        },
        success: function (data) {
            // @todo: server errors (also SF form errors) move to error method (422 status, json reponse)

            if (typeof self.options.successCallback === 'function') {
                self.options.successCallback(data, self);
            } else if (!data) {
                self.formChange = false;

                self.empty().hide();
                if (self.options.parent) {
                    self.options.parent.refresh();
                }
            } else {
                var dataCheck = $('<div></div>').append(data);

                $('.content-window-body form', self.elm).html(data);

                if (typeof cms.initMediaElements === "function") {
                    cms.initMediaElements();
                }

                self.hideloader();

                self.applyFormControl();

                if ($('.has-error', dataCheck).length > 0 || $('.alert:not(.alert-success)', dataCheck).length > 0) {
                    if (typeof self.options.afterErrorCallback === 'function') {
                        self.options.afterErrorCallback(data, self);
                    }
                } else {
                    self.formChange = false;

                    if($('.alert-success', dataCheck).length === 0) {
                        self.empty().hide();
                    }

                    if (self.options.parent) {
                        self.options.parent.refresh();
                    }
                }
            }
        },
        error: function(data){
            if (data.hasOwnProperty('responseJSON')) {
                if(data.responseJSON.hasOwnProperty('formError')) {
                    var errors = data.responseJSON;
                    self.hideloader()

                    for (var errorIndex in errors) {
                        cms.showNotification({
                            type: 'error',
                            text: errors[errorIndex]
                        });
                    }
                } else if(data.responseJSON.hasOwnProperty('errors')) {
                    var errors = data.responseJSON.errors;

                    self.displayFormErrors(formElm, errors);
                }
            }

            var toolbarButtons = $('.pull-right .btn', self.elm);

            toolbarButtons.each(function() {
                var elm = $(this);

                elm.removeAttr('disabled');
                elm.find('.fa-spinner.fa-spin').remove();
            });
        }
    });

    self.applyFormControl();
};

ContentWindow.prototype.displayFormErrors = function(formElm, errors) {
    if(!errors.length) {
        return false;
    }

    $('#form_errors', this.elm).remove();

    var errorHtml = `<li>${errors.join('</li><li>')}</li>`;

    var formErrorElm = $(`<div id="form_errors" class="alert alert-danger"><ul>${errorHtml}</ul></div>`);

    formElm.prepend(formErrorElm);
};

ContentWindow.prototype.applyFormControl = function() {

    if(!this.elm) {
        return false;
    }

    var form = $('form', this.elm);

    if (!form.attr('id')) {
        form.attr('id', form.attr('name'));
    }

    setupFormValidation({
        form: '#' + form.attr('id'),
        lang : 'nl'
    });

    new CmsFormElements(this.elm);
};

ContentWindow.prototype.empty = function () {
    this.elm.find('.content-window-body').empty();

    return this;
};

ContentWindow.prototype.isDirty = function () {
    var isDirty = false;

    $.each($(':input', this.elm), function (i, elm) {
        if (!elm.name) {
            return true;
        }

        switch (elm.type) {
            case 'checkbox':
            case 'radio':
                if (elm.checked != element.defaultChecked) {
                    isDirty = true;
                }
                break;
            case 'select-one':
            case 'select-multiple':
                for (var j = 0; j < elm.options.length; j++) {
                    if (elm.options[j].selected != elm.options[j].defaultSelected) {
                        isDirty = true;
                    }
                }
                break;
            case 'submit':
                break;
            default:
                if (elm.value != elm.defaultValue) {
                    isDirty = true;
                }
        }

        if (isDirty) {
            return false;
        }
    });

    return isDirty;
};

ContentWindow.prototype.hideAll = function () {
    if (ContentWindowCollection.length > 0) {
        for (var i = 0; i < ContentWindowCollection.length; i++) {
            ContentWindowCollection[i].cancelButtonClicked = true;
            ContentWindowCollection[i].hide();
        }
    }
};
