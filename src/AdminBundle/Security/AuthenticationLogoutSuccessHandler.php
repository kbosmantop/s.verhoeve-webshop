<?php

namespace AdminBundle\Security;

use AdminBundle\Services\AuthenticationLogger;
use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

class AuthenticationLogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    /**
     * @var AuthenticationLogger
     */
    private $authenticationLogHandler;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param AuthenticationLogger $authenticationLogHandler
     */
    public function setAuthenticationLogger(AuthenticationLogger $authenticationLogHandler)
    {
        $this->authenticationLogHandler = $authenticationLogHandler;
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     *
     * @param Request $request
     *
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {
        $username = null;
        $user = null;

        if ($this->tokenStorage->getToken()) {
            if ($this->tokenStorage->getToken()->getUser() instanceof User) {
                $user = $this->tokenStorage->getToken()->getUser();
            }

            if ($this->tokenStorage->getToken()->getUsername()) {
                $username = $this->tokenStorage->getToken()->getUsername();
            }
        }

        $this->authenticationLogHandler->success('logout', $username, $user);

        return parent::onLogoutSuccess($request);
    }
}