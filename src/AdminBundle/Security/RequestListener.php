<?php namespace AdminBundle\Security;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        // Skip if it's not the master request
        if (!$event->isMasterRequest()) {
            return;
        }

        $event->getKernel();
        $event->getRequest();
        $this->container;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        void($event);
    }
}