<?php

namespace AdminBundle\Form\Site\Navigation;

use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MenuType
 * @package AdminBundle\Form\Site\Navigation
 */
class MenuType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $container->add('site', EntityType::class, [
            'label' => "Site",
            "class" => Site::class,
            'placeholder' => '',
            'required' => true,
        ]);

        $container->add('name', TextType::class, [
            'label' => 'Naam',
            'required' => true,
        ]);

        $container->add('location', ChoiceType::class, [
            'label' => 'Locatie',
            'placeholder' => 'Geen toegewezen locatie',
            'choices' => MenuLocationType::getChoices(),
            'group_by' => function ($val, $desc, $index) {
                void($desc, $index);
                if (stripos($val, 'email_') !== false) {
                    return 'E-mail';
                } else {
                    return 'Website';
                }
            },
            'choice_label' => function ($val, $desc, $index) {
                void($index);
                if (stripos($val, 'email_') !== false) {
                    return str_ireplace('E-mail: ', '', $desc);
                } else {
                    return $desc;
                }
            },
        ]);

        $container->add('maxLevels', IntegerType::class, [
            'label' => 'Aantal niveau\'s',
            'required' => false,
            'attr' => [
                'placeholder' => 'Leeglaten indien geen restricties',
            ],
        ]);

        $container->add('maxRootItems', IntegerType::class, [
            'label' => 'Aantal items op hoofdniveau',
            'required' => false,
            'attr' => [
                'placeholder' => 'Leeglaten indien geen restricties',
            ],
        ]);

        $container->add('maxChildItems', IntegerType::class, [
            'label' => 'Aantal items in dropdown menu',
            'required' => false,
            'attr' => [
                'placeholder' => 'Leeglaten indien geen restricties',
            ],
        ]);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
