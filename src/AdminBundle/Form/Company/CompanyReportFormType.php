<?php

namespace AdminBundle\Form\Company;

use AdminBundle\Form\Report\ReportEditFormType;
use AdminBundle\Form\Report\ReportFormType;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyReportFormType
 * @package AdminBundle\Form\Company
 */
class CompanyReportFormType extends AbstractEntityFormType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('report', ReportEditFormType::class, [
            'label' => 'Rapportage'
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyReport::class
        ]);
    }

}