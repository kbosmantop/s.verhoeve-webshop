<?php

namespace AdminBundle\Form\Company;

use AppBundle\DBAL\Types\CompanyReportCustomerFrequencyType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AbstractEntityFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateTimeType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyReportFormType
 * @package AdminBundle\Form\Company
 */
class CompanyReportCustomerFormType extends AbstractEntityFormType
{
    /** @var RequestStack */
    private $requestStack;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param RequestStack  $requestStack
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $companyReportId = $this->requestStack->getMasterRequest()->get('companyReport');
        $company = null;
        $companyReport = null;

        if ($companyReportId) {
            /** @var CompanyReport $companyReport */
            $companyReport = $this->entityManager->getRepository(CompanyReport::class)->find($companyReportId);

            $company = $companyReport->getCompany();
        }

        $container = $builder->create('container', ContainerType::class, []);

        $column = $builder->create('column', ColumnType::class, [
            'label' => 'Rapportage gebruiker',
        ]);

        if ($company) {
            $column
                ->add('customer', EntityType::class, [
                    'class' => Customer::class,
                    'label' => 'Klant',
                    'query_builder' => function (EntityRepository $er) use ($company) {
                        $qb = $er->createQueryBuilder('c')
                            ->select('c')
                            ->andWhere('c.company = :company')
                            ->setParameter('company', $company->getId());

                        return $qb;
                    },
                ]);
        }

        $column
            ->add('startDate', DateTimeType::class, [
                'label' => 'Vanaf',
            ])
            ->add('frequency', ChoiceType::class, [
                'label' => 'Interval',
                'choices' => CompanyReportCustomerFrequencyType::getChoices(),
            ]);

        $container->add($column);
        $builder->add($container);

        if ($companyReport) {
            $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $formEvent) use ($companyReport) {
                /** @var CompanyReportCustomer $companyReportCustomer */
                $companyReportCustomer = $formEvent->getData();
                $companyReportCustomer->setCompanyReport($companyReport);
            });
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyReportCustomer::class,
        ]);
    }

}