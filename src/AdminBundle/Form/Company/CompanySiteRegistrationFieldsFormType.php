<?php

namespace AdminBundle\Form\Company;

use AppBundle\Entity\Relation\CompanyCustomField;
use AppBundle\Form\Type\TranslationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanySiteRegistrationFieldsFormType
 * @package AdminBundle\Form\Company
 */
class CompanySiteRegistrationFieldsFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('position', IntegerType::class, [
            'attr' => [
                'min' => 0,
                'class' => 'text-right',
            ],
        ]);
        $builder->add('label', TranslationType::class, [
            'label' => false,
        ]);
        $builder->add('type', ChoiceType::class, [
            'required' => false,
            'label' => false,
            'choices' => [
                'Tekstveld' => 'text',
                'Selectielijst' => 'choice',
            ],
        ]);
        $builder->add('values', TranslationType::class, [
            'label' => false,
            'field_type' => TextareaType::class,
            'attr' => [
                'placeholder' => 'La La Land',
            ],
        ]);
        $builder->add('multiple', CheckboxType::class, [
            'required' => false,
            'label' => false,
        ]);
        $builder->add('required', CheckboxType::class, [
            'required' => false,
            'label' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyCustomField::class,
        ]);
    }
}