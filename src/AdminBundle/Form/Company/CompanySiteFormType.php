<?php

namespace AdminBundle\Form\Company;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanySite;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanySiteFormType
 * @package AdminBundle\Form\Company
 */
class CompanySiteFormType extends AbstractType
{

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * CompanySiteFormType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {

        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_general', ContainerType::class, []);

        $columnLeft = $builder->create('column_left', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);

        $columnStyle = $builder->create('columnStyle', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $columnStyle
            ->add('locale', ChoiceType::class, [
                'label' => 'Voorkeurstaal',
                'choices' => [
                    'Nederlands' => 'nl_NL',
                    'Engels' => 'en_US',
                ],
            ]);

        $columnLeft->add($columnStyle);

        $columnUrl = $builder->create('column_url', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Url',
        ])
            ->add('defaultSite', EntityType::class, [
                'class' => Site::class,
                'label' => 'Standaard site',
                'required' => true
            ])
            ->add('registrationEnabled', ChoiceType::class, [
                'label' => 'Registratie toestaan',
                'choices' => [
                    'Ingeschakeld' => true,
                    'Uitgeschakeld' => false,
                ],
                'required' => false,
            ])
            ->add('allowedExtensions', TextareaType::class, [
                'label' => 'Email extensies',
                'help_block' => 'Email extensies die toegestaan zijn voor het aanmaken van een account.'
            ])
            ->add('domainName', TextType::class, [
                'label' => 'Eigen domein',
                'required' => false,
            ])
            ->add('slug', TextType::class, [
                'label' => 'Slug',
                'required' => false,
            ]);

        $columnLeft->add($columnUrl);

        $columnCommunication = $builder->create('column_communication', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Communicatie momenten',
        ]);

        $fields = [
            'orderConfirmationUser' => 'Orderbevestiging',
            'orderConfirmationCCUser' => 'Orderbevestiging via CC',
            'deliveryStatusUser' => 'Bezorgstatus',
            'deliveryStatusCCUser' => 'Bezorgstatus via CC',
            'confirmOrderUser' => 'Bestellingen goedkeuren',
            'confirmUserUser' => 'Gebruikers goedkeuren',
        ];

        foreach ($fields as $field => $label) {
            $columnCommunication->add($field, EntityType::class, [
                'placeholder' => 'Selecteer een gebruiker',
                'class' => Customer::class,
                'label' => $label,
                'query_builder' => function (EntityRepository $repository) {
                    return $this->getUsersByCompany($repository);
                },
                'required' => false,
            ]);
        }

        $columnLeft->add($columnCommunication);

        $columnRight = $builder->create('column_right', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);

        $column_text_login = $builder->create('tcr_column_text_login', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Tekst voor bedanktpagina',
        ]);

        $column_text_login->add('orderSuccessText', CKEditorType::class, [
            'label' => false,
            'required' => false,
            'ckeditor' => [
                'custom_toolbar' => 'basic',
            ],
        ]);

        $column_text_register = $builder->create('tcr_column_text_register', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Tekst voor registratiepagina',
        ]);

        $column_text_register->add('registrationText', CKEditorType::class, [
            'label' => false,
            'required' => false,
            'ckeditor' => [
                'custom_toolbar' => 'basic',
            ],
        ]);

        $column_custom_style = $builder->create('tcr_column_custom_style', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Aangepaste stijl',
        ]);

        $column_custom_style->add('customStyle', TextareaType::class, [
           'label' => 'Aangepaste stijl',
        ]);

        $columnRight
            ->add($column_text_login)
            ->add($column_text_register)
            ->add($column_custom_style);

        /**
         * Additional field column
         */
        $column_custom_fields = $builder->create('custom_fields_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Aangepaste registratie velden',
        ]);

        $column_custom_fields->add('customFields', CollectionType::class, [
            'entry_type' => CompanySiteRegistrationFieldsFormType::class,
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        'label' => 'Positie *',
                        'width' => '80px',
                    ],
                    [
                        'label' => 'Label',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Type',
                        'width' => '125px',
                    ],
                    [
                        'label' => 'Waardes',
                        'width' => '300px',
                    ],
                    [
                        'label' => 'Multiple',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Verplicht',
                        'width' => '100px',
                    ],
                ],
            ],
        ]);

        $columnRight->add($column_custom_fields);

        $container
            ->add($columnLeft)
            ->add($columnRight);

        $builder->add($container);
    }

    /**
     * @param EntityRepository $er
     * @return QueryBuilder|null
     */
    private function getUsersByCompany(EntityRepository $er)
    {
        if (!is_null($this->getCompany())) {
            return $er->createQueryBuilder('c')
                ->andWhere('c.company = :company')
                ->setParameter('company', $this->getCompany()->getId());
        }

        return null;
    }

    /**
     * @return null|object
     */
    public function getCompany()
    {
        $id = $this->requestStack->getCurrentRequest()->get('company');

        if ($id) {
            $company = $this->entityManager->getRepository(Company::class)->find($id);

            return $company;
        }

        return null;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanySite::class,
        ]);
    }

}