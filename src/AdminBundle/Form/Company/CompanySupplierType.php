<?php

namespace AdminBundle\Form\Company;

use AdminBundle\Form\App\CompanyCommissionType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Form\Type\YesNoType;
use AppBundle\Interfaces\ConnectorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanySupplierType
 * @package AdminBundle\Form\Company
 */
class CompanySupplierType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ConnectorHelper
     */
    private $connectorHelper;

    /**
     * CompanySupplierType constructor.
     * @param EntityManagerInterface $entityManager
     * @param ConnectorHelper        $connectorHelper
     */
    public function __construct(EntityManagerInterface $entityManager, ConnectorHelper $connectorHelper)
    {
        $this->entityManager = $entityManager;
        $this->connectorHelper = $connectorHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $connectors = $this->connectorHelper->getConnectors();
        $connectorChoices = [];

        foreach ($connectors as $connector) {
            $shortName = (new \ReflectionClass($connector))->getShortName();

            $connectorChoices[$shortName] = $shortName;
        }

        $container = $builder->create('company_suppliers_container', ContainerType::class, []);

        $column = $builder->create('company_suppliers_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Leveranciergegevens',
            'icon' => '',
        ]);

        $column->add('supplierGroups', EntityType::class, [
            'label' => 'Leveranciers groep',
            'placeholder' => 'Selecteer een leveranciersgroep',
            'class' => SupplierGroup::class,
            'required' => false,
            'multiple' => true,
            'choice_label' => 'name',
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
            'attr' => [
                'data-supplier-group-list' => true,
            ],
        ])->add('supplierConnector', ChoiceType::class, [
            'label' => 'Connector',
            'choices' => $connectorChoices,
            'placeholder' => '',
            'required' => false,
            'attr' => [
                'data-supplier-group-list' => true,
            ],
        ]);

        $container->add($column);

        $column = $builder->create('company_suppliers_column_commission', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Commissiepercentages',
            'icon' => '',
        ]);

        $column
            ->add('supplierCommissions', CollectionType::class, [
                'entry_type' => CompanyCommissionType::class,
                'label' => false,
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Productgroep',
                            'width' => '150px',
                        ],
                        [
                            'label' => 'Percentage',
                            'width' => '50px',
                        ],
                    ],
                ],
            ]);

        $container->add($column);

        $connectors = $this->connectorHelper->getConnectors();

        foreach ($connectors as $connector) {
            $reflectionClass = new \ReflectionClass($connector);

            $connector = $this->connectorHelper->findConnector($connector, new Company());
            if($connector === null){
                continue;
            }

            $formBuilder = $connector->getConnectorParametersFormBuilder();

            if (!$formBuilder || !($formBuilder instanceof FormBuilderInterface)) {
                continue;
            }

            $column = $builder->create('company_suppliers_connector_' . strtolower($reflectionClass->getShortName()) . '_column',
                ColumnType::class, [
                    'column_layout' => ColumnType::COLUMN_WIDTH_6,
                    'label' => 'Connector ' . $reflectionClass->getShortName() . ' parameters',
                    'icon' => '',
                ]);

            $column->add($formBuilder);

            $container->add($column);
        }

        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $connectors = $this->connectorHelper->getConnectors();

            $company = $event->getData();

            $connectorForms = $event->getForm()->get('company_suppliers_container');

            $parameters = new ArrayCollection($this->entityManager->getRepository(CompanyConnectorParameter::class)->findBy([
                'company' => $company,
            ]));

            foreach ($connectors as $connector) {
                $reflectionClass = new ReflectionClass($connector);

                $connectorFormName = 'company_suppliers_connector_' . strtolower($reflectionClass->getShortName()) . '_column';

                if (!$connectorForms->has($connectorFormName)) {
                    continue;
                }

                foreach ($connectorForms->get($connectorFormName)->all() as $forms) {
                    foreach ($forms->all() as $form) {
                        $parameter = $parameters->filter(function (CompanyConnectorParameter $companyConnectorParameter
                        ) use ($reflectionClass, $form) {
                            if ($companyConnectorParameter->getConnector() !== $reflectionClass->getShortName()) {
                                return false;
                            }

                            if ($companyConnectorParameter->getName() !== $form->getName()) {
                                return false;
                            }

                            return true;
                        })->current();

                        if(!$parameter && $form->getConfig()->getType()->getInnerType() instanceof YesNoType) {
                            $form->setData(true);
                        }

                        if (!$parameter) {
                            continue;
                        }

                        if($form->getConfig()->getType()->getInnerType() instanceof CheckboxType) {
                            $value = (boolean) $parameter->getValue();
                        } else {
                            $value = $parameter->getValue();
                        }

                        $form->setData($value);
                    }
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
