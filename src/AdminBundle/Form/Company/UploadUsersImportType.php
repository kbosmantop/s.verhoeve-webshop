<?php

namespace AdminBundle\Form\Company;

use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class UploadUsersImportType
 * @package AdminBundle\Form\Company
 */
class UploadUsersImportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $container = $builder->create('upload_users_container', ContainerType::class, []);

        $column = $builder->create('upload_users_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Gebruikers importeren',
            'icon' => 'profile',
        ]);

        $column
            ->add('userTemplate', FileType::class, [
                'mapped' => false,
                'label' => 'Gebruikers upload',
            ])
        ;

        $container->add($column);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
