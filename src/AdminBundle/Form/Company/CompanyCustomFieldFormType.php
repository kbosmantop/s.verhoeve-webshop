<?php

namespace AdminBundle\Form\Company;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\DBAL\Types\AppliesToOrderType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyCustomField;
use AppBundle\Entity\Relation\CompanyCustomFieldOption;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyCustomFieldFormType
 * @package AdminBundle\Form\Company
 */
class CompanyCustomFieldFormType extends AbstractFormType
{
    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'label',
    ];

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * CompanyCustomFieldFormType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack  $requestStack
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('translations', ContainerType::class);

        $container
            ->add('fieldKey', TextType::class, [
                'label' => 'Technische naam',
                'help_label' => 'Toegestane tekens: a-z A-Z 0-9 - _'
            ])
            ->add('translations', TranslationsType::class, $this->getA2lixTranslatableFieldsOptions([
                'label' => [
                    'label' => 'Naam veld',
                ],
            ])
            )
            ->add('type', ChoiceType::class, [
                'label' => 'Type veld',
                'choices' => [
                    'Tekst' => TextType::class,
                    'Dropdown' => ChoiceType::class,
                ],
            ])
            ->add('options', CollectionType::class, [
                'entry_type' => CompanyCustomFieldOptionFormType::class,
                'by_reference' => false,
                'label' => 'Opties',
                'collection' => [
                    'sortable' => true,
                    'columns' => [
                        [
                            'label' => 'Positie *',
                            'width' => '80px',
                        ],
                        [
                            'label' => 'Label *',
                            'width' => '300px',
                        ],
                        [
                            'label' => 'Waarde *',
                            'width' => '300px',
                        ],
                    ],
                ],

            ])
            ->add('required', YesNoType::class, [
                'label' => 'Verplicht veld',
            ])
            ->add('multiple', YesNoType::class, [
                'label' => 'Meerdere waarden',
            ])
            ->add('appliesTo', ChoiceType::class, [
                'label' => 'Wordt gevraagd bij',
                'required' => true,
                'choices' => AppliesToOrderType::getChoices(),
            ])
            ->add('showOnInvoice', YesNoType::class, [
                'label' => 'Toon op factuur',
            ])
            ->add('invoiceGroup', YesNoType::class, [
                'label' => 'Aparte factuur per waarde in dit veld',
            ]);

        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var CompanyCustomField $companyField */
            $companyField = $event->getData();
            $request = $this->requestStack->getMasterRequest();

            if (null === $companyField->getCompany()
                && null !== $request
                && $request->get('entity')) {
                //get company from request

                $companyId = $request->get('entity');
                /** @var Company $company */
                $company = $this->entityManager->getRepository(Company::class)->find($companyId);

                $companyField->setCompany($company);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyCustomField::class,
        ]);
    }

}