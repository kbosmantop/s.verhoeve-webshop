<?php

namespace AdminBundle\Form\Company;

use AdminBundle\Form\Address\CompanyEstablishmentAddressType;
use AdminBundle\Form\CollectionListType;
use AdminBundle\Form\Supplier\PickupLocationType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Form\Common\EntityHiddenTransformer;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TimeslotType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CompanyEstablishmentType
 * @package AdminBundle\Form\Company
 */
class CompanyEstablishmentType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * CustomerType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $requestStack
     * @param RouterInterface        $router
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_establishment_container', ContainerType::class, []);

        $column = $builder->create('container_establishment_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Vestiging',
            'icon' => 'key',
        ]);

        $column
            ->add('establishmentNumber', TextType::class, [
                'label' => 'Vestiging nummer',
                'attr' => [
                    'class' => 'establishmentNumberInput',
                    'data-establishment-url' => $this->router->generate("topgeschenken.company.establishment.details"),
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Naam',
            ]);

        $pickupLocation = $this->entityManager->getRepository(PickupLocation::class)->findOneBy(['companyEstablishment' => $builder->getData()->getId()]);
        $column->add('isPickupLocation', CheckboxType::class, [
            'label' => 'Deze vestiging is een afhaallocatie',
            'mapped' => false,
            'required' => false,
            'data' => null !== $pickupLocation,
        ]);

        $column->get('isPickupLocation')->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event){
            $companyEstablishment = null;
            if(!((null !== ($parent = $event->getForm()->getRoot())) && null !== ($companyEstablishment = $parent->getData()) && $companyEstablishment instanceof CompanyEstablishment)) {
                return;
            }

            $isPickupLocation = $event->getData() === '1';
            $pickupLocationRepository = $this->entityManager->getRepository(PickupLocation::class);
            if($isPickupLocation === true){
                $pickupLocationRepository->findOneOrCreate(['companyEstablishment' => $companyEstablishment]);
            } else {
                $pickupLocation = $pickupLocationRepository->findOneBy(['companyEstablishment' => $companyEstablishment->getId()]);
                if($pickupLocation){
                    $this->entityManager->remove($pickupLocation);
                }
            }
        });

        $container->add($column);

        $column = $builder->create('container_establishment_address_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Adresgegevens',
            'icon' => 'key',
        ]);

        $column->add('address', CompanyEstablishmentAddressType::class, [
            'label' => false,
        ]);

        $container->add($column);
        $builder->add($container);

        $builder->add('company', HiddenType::class, [
            'empty_data' => $this->getCompany(),
        ]);

        $builder->get('company')->addModelTransformer(new EntityHiddenTransformer(
            $this->entityManager,
            Company::class,
            'id'
        ));


        $hoursContainer = $builder->create('opening_hours_container', ContainerType::class, []);
        $hoursColumn = $hoursContainer->create('opening_hours_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Openingstijden voor vestiging',
            'icon' => 'key',
        ]);

        $hoursColumn->add('timeslots', CollectionListType::class, [
            'entry_type' => TimeslotType::class,
            'by_reference' => false,
        ]);

        $hoursContainer->add($hoursColumn);
        $builder->add($hoursContainer);

        if(null !== $pickupLocation){
            $builder->add('pickupLocation', PickupLocationType::class, [
                'mapped' => false,
                'data' => $pickupLocation,
                'label' => false,
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyEstablishment::class,
        ]);
    }

    /**
     * @return null|object
     */
    public function getCompany()
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if (null !== $masterRequest && ($id = $masterRequest->get('entity'))) {
            return $this->entityManager->getRepository(Company::class)->find($id);
        }

        return null;
    }
}
