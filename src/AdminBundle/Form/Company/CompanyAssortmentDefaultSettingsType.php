<?php

namespace AdminBundle\Form\Company;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyAssortmentDefaultSettingsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('assortment', ChoiceType::class, [
            'choices' => $options['data']['assortments'],
            'label' => 'Zoek een catalogus van een ander bedrijf',
        ])->add('copy', ChoiceType::class, [
            'choices' => [
                'Gebruiken als (geen eigen producten)' => false,
                'Kopieren van (eigen producten mogelijk)' => true,
            ],
            'label' => 'Wijze van gebruik geselecteerde catalogus',
        ]);
    }

}
