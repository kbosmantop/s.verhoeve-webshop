<?php

namespace AdminBundle\Form\Authentication;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class IpWhitelistType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('username', TextType::class, [
                'label' => 'Gebruikersnaam',
            ])
            ->add('email', TextType::class, [
                'label' => 'E-mailadres',
            ])
            ->add('plainpassword', PasswordType::class, [
                'label' => 'Wachtwoord',
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }
}
