<?php

namespace AdminBundle\Form\Discount;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DiscountFormType
 * @package AdminBundle\Form\Discount
 */
class DiscountFormType extends AbstractFormType
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * DiscountFormType constructor.
     *
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $column = $builder->create('category_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Korting',
        ]);

        $column->add('description', TextType::class, [
            'label' => 'Beschrijving'
        ]);

        $column->add('type', ChoiceType::class, [
            'label' => 'Type',
            'placeholder' => 'Maak een keuze',
            'choices' => [
                'Bedrag' => 'amount',
                'Percentage' => 'percentage',
            ],
        ]);

        $column->add('value', NumberType::class, [
            'label' => 'Waarde',
        ]);

        $column->add('rule', EntityType::class, [
            'label' => 'Regel',
            'placeholder' => 'Selecteer een regel',
            'class' => Rule::class,
            'choice_label' => 'description',
            'select2' => [],
        ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Discount $discount */
            $discount = $event->getData();

            if (!$discount->getCompanies()->isEmpty()) {
                $description = $event->getForm()->get('container')->get('category_column')->get('voucher')->getData();

                /** @var Voucher $voucher */
                foreach($discount->getVouchers() as $voucher) {
                    $voucher->setDescription($description);
                }
            }
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Discount $discount */
            $discount = $event->getData();

            if (null !== $discount && !$discount->getCompanies()->isEmpty()) {
                /** @var Discount $discount */
                $discount = $event->getData();

                $event->getForm()->get('container')->get('category_column')->add('voucher', TextType::class, [
                    'label' => 'Naam',
                    'help_label' => 'Zichtbaar voor klant',
                    'data' => $discount->getVouchers()->current()->getDescription(),
                    'mapped' => false,
                ]);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Discount::class
        ]);
    }
}