<?php

namespace AdminBundle\Form\Discount;

use AppBundle\Entity\Discount\DiscountProperty;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Interfaces\Sales\OrderInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Property;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DiscountPropertyFormType
 * @package AdminBundle\Form\Discount
 */
class DiscountPropertyFormType extends AbstractFormType {

    /** @var EntityManagerInterface $entityManager */
    protected $entityManager;

    /** @var RuleTransformerService $ruleTransformer */
    protected $ruleTransformer;

    /**
     * DiscountPropertyFormType constructor.
     * @param RuleTransformerService $ruleTransformerService
     * @param EntityManagerInterface          $entityManager
     */
    public function __construct(RuleTransformerService $ruleTransformerService, EntityManagerInterface $entityManager)
    {
        $this->ruleTransformer = $ruleTransformerService;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('discount_property_container', ContainerType::class, []);

        $column = $builder->create('discount_property_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Kortingsveld'
        ]);

        $column->add('description', TextType::class, [
            'label' => 'Beschrijving'
        ]);

        //get choices and add path field
        $choices = [];

        $inputs = [
            ResolverService::QB_ALIAS => $this->entityManager->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => OrderLineInterface::class,
            ]),
        ];

        $filters = $this->ruleTransformer->getFilters($inputs);

        foreach($filters as $filter) {
            $choices[$filter['optgroup']][$filter['label']] = $filter['id'];
        }

        $column->add('path', ChoiceType::class, [
            'label' => 'Veld',
            'mapped' => false,
            'choices' => $choices
        ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            /** @var DiscountProperty $discountProperty */
            $discountProperty = $event->getData();
            $form = $event->getForm();

            $path = str_replace(ResolverService::QB_ALIAS.'.', null, $form->get('discount_property_container')->get('discount_property_column')->get('path')->getData());
            $discountProperty->setPath($path);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DiscountProperty::class
        ]);
    }

}