<?php

namespace AdminBundle\Form\Discount;

use AppBundle\DBAL\Types\VoucherUsageType;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateType;
use AppBundle\Services\Discount\VoucherService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VoucherType
 * @package AdminBundle\Form\Discount
 */
class VoucherFormType extends AbstractFormType
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * VoucherFormType constructor.
     * @param RequestStack $requestStack
     * @param EntityManagerInterface $entityManager
     * @param VoucherService $voucherService
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        VoucherService $voucherService
    ) {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->voucherService = $voucherService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);
        $container = $builder->create('container', ContainerType::class);
        $column = $builder->create('category_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Kortingsbon',
        ]);
        $column->add('description', TextType::class, [
            'label' => 'Beschrijving',
            'help_label' => 'Voucher beschrijving, zichtbaar voor de klant',
        ]);
        $column->add('discount', EntityType::class, [
            'class' => Discount::class,
            'label' => 'Korting',
            'select2' => [
                'placeholder' => 'Selecteer korting',
            ],
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('discount')
                    ->where('discount.rule IS NOT NULL');
            },
            'choice_label' => function (Discount $discount) {
                return $discount->getRule()->getDescription() . ' (' . $discount->getType() . ': ' . $discount->getValue() . ')';
            },
        ]);
        $column->add('code', TextType::class, [
            'label' => 'Code',
            'attr' => [
                'placeholder' => 'Leeg voor willekeurig',
            ],
            'required' => false,
        ]);
        $column->add('usage', ChoiceType::class, [
            'label' => 'Gebruik',
            'choices' => VoucherUsageType::getChoices(),
            'placeholder' => 'Keuze...',
        ]);
        $column->add('limit', NumberType::class, [
            'label' => 'Limiet',
            'required' => false,
        ]);
        $column->add('validFrom', DateType::class, [
            'label' => 'Vanaf',
            'widget' => 'choice',
            'required' => true,
        ]);
        $column->add('validUntil', DateType::class, [
            'label' => 'Tot',
            'widget' => 'choice',
            'required' => true,
        ]);
        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Voucher $voucher */
            $voucher = $event->getData();

            if (null === $voucher->getCode()) {
                $voucher->setCode($this->voucherService->generateUniqueVoucherCode());
            }
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Voucher $voucher */
            $voucher = $event->getData();
            $form = $event->getForm();

            $request = $this->requestStack->getMasterRequest();
            if (null !== $request && $request->get('discount')) {
                /** @var Discount $discount */
                $discount = $this->entityManager->getRepository(Discount::class)->find($request->get('discount'));
                $voucher->setDiscount($discount);
                $form->get('container')->get('category_column')->remove('discount');
            }

            if ($voucher->getValidFrom() === null) {
                $voucher->setValidFrom(new \DateTime());
            }

            if ($voucher->getLimit() === null) {
                $voucher->setLimit(1);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Voucher::class,
        ]);
    }
}
