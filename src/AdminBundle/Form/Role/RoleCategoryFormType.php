<?php

namespace AdminBundle\Form\Role;

use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RoleCategoryFormType
 * @package AdminBundle\Form\Role
 */
class RoleCategoryFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('category_container', ContainerType::class);
        $column = $builder->create('category_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column->add('name', TextType::class, [
            'label' => 'Categorie',

        ])->add('description', TextType::class, [
            'label' => 'Beschrijving',
        ]);

        $container->add($column);
        $builder->add($container);
    }
}
