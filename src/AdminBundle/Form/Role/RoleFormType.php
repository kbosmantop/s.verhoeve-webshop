<?php

namespace AdminBundle\Form\Role;

use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RoleFormType extends AbstractFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Rol',
        ])->add('description', TextType::class, [
            'label' => 'Beschrijving',
        ])->add('category', EntityType::class, [
            'label' => 'Categorie',
            'class' => RoleCategory::class,
        ]);
    }

}
