<?php

namespace AdminBundle\Form\Fields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PriceRange
 * @package AdminBundle\Form\Fields
 */
class PriceRange extends AbstractType
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return TextType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'price_range';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'range-slider',
                'data-slider-min' => 0,
                'data-slider-max' => 1000,
                'data-slider' => 1,
                'data-slider-value' => '[0,500]'
            ]
        ]);

        parent::configureOptions($resolver);
    }
}