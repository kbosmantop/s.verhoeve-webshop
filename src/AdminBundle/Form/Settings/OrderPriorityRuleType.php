<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class OrderPriorityRuleType
 * @package AdminBundle\Form\Settings
 */
class OrderPriorityRuleType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $column = $builder->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
        ]);

        $column
            ->add('priority', NumberType::class, [
                'label' => 'Prioriteit',
                'help_label' => 'Hoger is meer prioriteit',
                'help_block' => 'Getal van 1 tot 100',
            ])
            ->add('rule', EntityType::class, [
                'label' => 'Regel',
                'class' => Rule::class,
            ]);

        $container->add($column);

        $builder->add($container);
    }
}
