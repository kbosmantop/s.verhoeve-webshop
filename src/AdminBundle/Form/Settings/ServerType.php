<?php

namespace AdminBundle\Form\Settings;

use AdminBundle\Services\OpenStack;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ServerType
 * @package AdminBundle\Form\Settings
 */
class ServerType extends AbstractFormType
{
    /**
     * @var OpenStack
     */
    private $openStack;

    /**
     * ServerType constructor.
     * @param OpenStack $openStack
     */
    public function __construct(OpenStack $openStack)
    {
        $this->openStack = $openStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $zones = $this->getZones();
        $flavors = $this->getFlavors();
        $images = $this->getImages();
        $groups = $this->getGroups();
        $networks = $this->getNetworks();

        $container
            ->add('name', TextType::class, [
                'label' => 'Name',
            ])
            ->add('zone', ChoiceType::class, [
                'label' => 'Zone',
                'choices' => $zones,
                'data' => 'NL2',
            ])
            ->add('flavor', ChoiceType::class, [
                'label' => 'Flavor',
                'choices' => $flavors,
                'placeholder' => ' ',
            ])
            ->add('image', ChoiceType::class, [
                'label' => 'Image',
                'choices' => $images,
                'data' => end($images),
            ])
            ->add('groups', ChoiceType::class, [
                'label' => 'Ansible group',
                'choices' => $groups,
                'multiple' => true,
                'placeholder' => "",
            ])
            ->add('network', ChoiceType::class, [
                'label' => 'Network',
                'choices' => $networks,
                'placeholder' => "",
            ]);

        $builder->add($container);

        $builder->add('close', ButtonType::class, [
            'label' => 'Annuleren',
            'attr' => [
                'title' => 'Wijzigingen worden niet opgeslagen',
                'swal' => 'confirm',
                'data-title' => 'Uw wijzigingen worden niet opgeslagen!',
                'data-type' => 'info',
                'data-cancelText' => 'Annuleren',
                'data-confirmText' => 'Bevestig',
                'data-callback' => 'function () { window.location.href=\'/admin/instellingen/servers\'; }',
            ],
            'icon' => 'cancel-circle2',
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Aanmaken',
            'icon' => 'checkmark-circle',
        ]);
    }

    /**
     * @return array
     */
    private function getZones(): array
    {
        return [
            'NL1' => 'NL1',
            'NL2' => 'NL2',
        ];
    }

    /**
     * @return array
     */
    private function getFlavors(): array
    {
        $flavors = [];

        foreach ($this->openStack->getComputeService()->flavorList() as $flavor) {
            if (strpos($flavor->getName(), 'Small HD') === false) {
                continue;
            }

            $flavors[$flavor->getName()] = $flavor->getId();
        }

        ksort($flavors);

        return $flavors;
    }

    /**
     * @return array
     */
    private function getImages(): array
    {
        $images = [];

        foreach ($this->openStack->getComputeService()->imageList() as $image) {
            if (strpos($image->getName(), 'CloudVPS Ubuntu') === false) {
                continue;
            }

            $images[$image->getName()] = $image->getId();
        }

        ksort($images);

        return $images;
    }

    /**
     * @return array
     */
    private function getGroups(): array
    {
        return [
            'Webservers' => 'web_servers',
            'Database servers' => 'db_servers',
            'Cache servers' => 'cache_servers',
            'Load balancers' => 'lb_servers',
            'Admin servers' => 'admin_servers',
            'Zoekindex servers' => 'search_servers',
            'Control server' => 'ctl_server',
        ];
    }

    /**
     * @return array
     */
    private function getNetworks(): array
    {
        $networks = [];

        foreach ($this->openStack->getNetworkService()->listNetworks() as $network) {
            $networks[$network->getName()] = $network->getId();
        }

        return $networks;
    }
}
