<?php

namespace AdminBundle\Form\Settings\Variables\DeliveryAddressType;

use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Geography\DeliveryAddressTypeField;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\TranslationType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryAddressTypeFieldType
 * @package AdminBundle\Form\Settings\Variables\DeliveryAddressType
 */
class DeliveryAddressTypeFieldType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ]);

        $builder->add('label', TranslationType::class, [
            'label' => false,
        ]);

        $builder->add('type', ChoiceType::class, [
            'required' => true,
            'label' => false,
            'choices' => [
                'Tekstveld' => 'text',
            ],
        ]);

        $builder->add('required', CheckboxType::class, [
            'required' => false,
            'label' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DeliveryAddressTypeField::class,
        ]);
    }
}
