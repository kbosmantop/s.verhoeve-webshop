<?php

namespace AdminBundle\Form\Settings\Geographic;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class PostcodeType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('city', EntityType::class, [
                'label' => 'Plaats',
                'class' => 'AppBundle\Entity\Geography\City',
                'group_by' => function ($city) {
                    return $city->getCountry();
                },
            ])
            ->add('postcode');
    }
}
