<?php

namespace AdminBundle\Form\Settings\Geographic;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\FormBuilderInterface;

class ProvinceType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('translations', TranslationsType::class, []);
    }
}
