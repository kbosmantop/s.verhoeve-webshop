<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Entity\Site\SiteCustomHtml;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteCustomHtmlType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ]);

        $builder->add('location', ChoiceType::class, [
            'choices' => [
                'Body' => 'body',
                'Head' => 'head',
            ],
        ]);

        $builder->add('manipulation', ChoiceType::class, [
            'choices' => [
                'Aan het begin' => 'prepend',
                'Aan het einde' => 'append',
            ],
        ]);

        $builder->add('content', TextareaType::class, [
            'attr' => [
                'style' => 'height: 200px;',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteCustomHtml::class,
        ]);
    }
}
