<?php

namespace AdminBundle\Form\Settings\Site;

use AppBundle\Entity\Site\OpeningHours;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\DayType;
use AppBundle\Form\Type\HoursType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OpeningHoursType
 * @package AdminBundle\Form\Settings\Site
 */
class OpeningHoursType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('day', DayType::class, [
            'label' => 'Dag',
            'placeholder' => '',
            'required' => true,
            'select2' => [
                'placeholder' => 'Selecteer Dag',
                'noSearch' => true,
            ],
        ])->add('fromHour', HoursType::class, [
            'label' => 'Open vanaf',
            'placeholder' => '',
            'required' => true,
            'select2' => [
                'placeholder' => 'Open vanaf',
                'noSearch' => true,
            ],
        ])->add('tillHour', HoursType::class, [
            'label' => 'Open t/m',
            'reverse' => true,
            'placeholder' => '',
            'required' => true,
            'select2' => [
                'placeholder' => 'Open t/m',
                'noSearch' => true,
            ],
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OpeningHours::class,
        ]);
    }
}
