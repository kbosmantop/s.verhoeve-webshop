<?php

namespace AdminBundle\Form\Settings;

use AdminBundle\Form\Settings\Site\OpeningHoursType;
use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\DBAL\Types\ThemeType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Site\SiteHomepageAssortment;
use AppBundle\Entity\Site\SitePaymentMethodSettings;
use AppBundle\Entity\Site\SiteUsp;
use AppBundle\Form\Extension\TabbedFormTypeExtension;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\PhpTranslationsLocaleType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationType;
use AppBundle\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SiteType
 * @package AdminBundle\Form\Settings
 */
class SiteType extends AbstractFormType
{
    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * SiteType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;

        $container = $builder->create('site_container', ContainerType::class, []);

        $container->add($this->getTabDefaultSettings());
        $container->add($this->getTabFinancial());
        $container->add($this->getTabContent());
        $container->add($this->getTabPaymentMethods());
        $container->add($this->getTabOpeningHours());

        $builder->add($container);

        // Add parameters to default tab.
        $this->builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Site::class,
            $container->get('site_container_tab_default'), ContainerType::class));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Site $site */
            $site = $event->getData();

            if(null !== $site) {
                $em = $this->container->get('doctrine')->getManager();

                /** @var Paymentmethod[] $paymentMethodRepository */
                $paymentMethodRepository = $em->getRepository(Paymentmethod::class)->findAll();

                foreach ($paymentMethodRepository as $paymentMethod) {
                    $siteHasPaymentMethods = (bool)$site->getPaymentMethodSettings()->filter(function (
                        SitePaymentMethodSettings $sitePaymentMethodSettings
                    ) use ($paymentMethod) {
                        return $sitePaymentMethodSettings->getPaymentMethod() === $paymentMethod;
                    })->count();

                    if (!$siteHasPaymentMethods) {
                        $sitePaymentMethodSettings = new SitePaymentMethodSettings();
                        $sitePaymentMethodSettings->setSite($site);
                        $sitePaymentMethodSettings->setPaymentMethod($paymentMethod);
                        $sitePaymentMethodSettings->setEnabled(true);

                        $site->getPaymentMethodSettings()->add($sitePaymentMethodSettings);
                    }
                }
            }

        });
    }

    /**
     * @return FormBuilderInterface
     * @throws \Exception
     */
    private function getTabDefaultSettings()
    {
        $tab = $this->builder->create('site_container_tab_default', TabType::class, [
            'label' => 'Basis instellingen',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $container = $this->builder->create('site_container_default', ContainerType::class, []);

        $column = $this->builder->create('site_container_default_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
            'icon' => 'cog2',
        ]);

        $column
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'placeholder' => '',
                'class' => 'AppBundle\Entity\Geography\Country',
            ])
            ->add('description', TranslationType::class, [
                'label' => 'Omschrijving',
            ])
            ->add('theme', ChoiceType::class, [
                'label' => 'Thema',
                'choices' => ThemeType::getChoices(),
                'placeholder' => 'Kies een thema voor deze website',
                'required' => true,
            ])
            ->add('customerServiceMenuDisabled', BooleanType::class, [
                'label' => 'Verberg klantenservice menu',
                'choices' => YesNoType::getChoices(),
                'required' => true,
            ])
            ->add('paymentMethodLogosHidden', BooleanType::class, [
                'label' => 'Verberg betaalmethode logo\'s',
                'choices' => YesNoType::getChoices(),
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
            ])
            ->add('phoneNumber', PhoneNumberType::class, [
                'label' => 'Telefoonnummer',
            ])
            ->add('googleAnalyticsTrackingId', TextType::class, [
                'label' => 'Google Analytics Tracking-ID ',
                'required' => false,
            ])
            ->add('slug', TextType::class, [
                'label' => 'Slug',
                'required' => false,
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Hoofdwebsite',
                'class' => Site::class,
                'required' => false,
            ])
            ->add('availableLocales', PhpTranslationsLocaleType::class, [
                'label' => 'Talen',
                'placeholder' => 'Kies de beschikbare talen voor deze website',
                'multiple' => true,
                'required' => true,
            ]);

        $container->add($column);
        $tab->add($container);

        /* Winkelwagen instellingen */
        $container = $this->builder->create('site_container_cart_settings', ContainerType::class, []);

        $column = $this->builder->create('site_container_cart_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Winkelwagen',
            'icon' => 'cart2',
        ]);

        $column->add('addToCartModalEnabled', BooleanType::class, [
            'label' => 'Toon modal na toevoegen product',
            'choices' => YesNoType::getChoices(),
        ]);

        $container->add($column);

        $tab->add($container);

        /* Bezorg instellingen */

        $container = $this->builder->create('site_container_default_delivery', ContainerType::class, []);

        $column = $this->builder->create('site_container_delivery_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Bezorging',
            'icon' => 'truck',
        ]);

        $column
            ->add('deliveryDateMethod', ChoiceType::class, [
                'label' => 'Weergave bezorgdatum',
                'required' => true,
                'choices' => [
                    'Niet tonen' => null,
                    'Voorkeur' => 'preference',
                    'Verplicht' => 'required',
                ],
            ])
            ->add('deliveryDateMethodDescription', TextareaType::class, [
                'label' => 'Omschrijving',
                'required' => false,
                'attr' => [
                    'style' => 'min-height: 95px',
                ],
            ])
            ->add('orderPickupEnabled', BooleanType::class, [
                'label' => 'Order afhalen mogelijk',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('pickupMethodDescription', TextareaType::class, [
                'label' => 'Omschrijving afhalen',
                'required' => false,
                'attr' => [
                    'style' => 'min-height: 95px',
                ],
            ])
            ->add('pickupAddress', EntityType::class, [
                'class' => Address::class,
                'label' => 'Afhaaladres',
                'required' => false,
                'select2' => [
                    'placeholder' => 'Selecteer een adres',
                    'ajax' => true,
                    'findBy' => [
                        'street',
                        'postcode',
                        'number',
                        'city',
                    ],
                ],
                'choice_label' => function (Address $address) {
                    return $address->format() .' ('. $address->getCompany()->getName() .')';
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('address')
                        ->andWhere('address.company = :company')
                        ->orderBy('address.street', 'ASC')
                        ->setParameter('company', $this->container->getParameter('topgeschenken_company_id'));
                },

            ])
            ->add('defaultProductTransportType', EntityType::class, [
                'class' => TransportType::class,
                'label' => 'Standaard bezorgmethode',
                'required' => false,
                'choice_label' => function (TransportType $transportType = null) {
                    if (is_null($transportType)) {
                        return '';
                    }

                    $vatGroup = $transportType->getVatGroupForCountry($this->container->get('doctrine')->getRepository(Country::class)->find('NL'));
                    $vatPercentage = $vatGroup->getRateByDate()->getFactor() * 100;

                    $displayPrice = number_format($transportType->getPrice() / 100 * (100 + $vatPercentage),
                        2, ',', '.');

                    return $transportType->getInternalName() . ' á € ' . $displayPrice;
                },
            ]);

        $container->add($column);
        $tab->add($container);

        /* HTTP SETTINGS */

        $container = $this->builder->create('site_container_default_http', ContainerType::class, []);

        $column = $this->builder->create('site_container_default_http_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'HTTP',
            'icon' => 'shield2',
        ]);

        $column
            ->add('httpScheme', ChoiceType::class, [
                'label' => 'Schema',
                'choices' => [
                    'HTTP' => 'http',
                    'HTTPS' => 'https',
                ],
            ])
            ->add('httpPort', IntegerType::class, [
                'label' => 'Poort',
                'attr' => [
                    'placeholder' => 80,
                ],
                'required' => false,
            ]);

        $container->add($column);

        $tab->add($container);

        /* Login settings */

        $container = $this->builder->create('site_container_default_login', ContainerType::class, []);

        $column = $this->builder->create('site_container_default_domain_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Login & Registratie',
            'icon' => 'user-check',
        ]);

        $column
            ->add('authenticationRequired', BooleanType::class, [
                'label' => 'Login forceren',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('disableCustomerLogin', BooleanType::class, [
                'label' => 'Login uitschakelen',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('disableCustomerRegistration', BooleanType::class, [
                'label' => 'Registratie uitschakelen',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('customerGroups', EntityType::class, [
                'label' => 'Beschikbaar voor',
                'placeholder' => 'Alle klanten',
                'class' => CustomerGroup::class,
                'required' => false,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Maak een keuze',
                ],
            ]);

        $container->add($column);

        $tab->add($container);

        /* Domain settings */

        $container = $this->builder->create('site_container_default_domain', ContainerType::class, []);

        $column = $this->builder->create('site_container_default_domain_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Domeinen',
            'icon' => 'earth',
        ]);

        $column
            ->add('domains', CollectionType::class, [
                'entry_type' => SiteDomainType::class,
                'label' => 'Domeinnamen',
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Domeinnaam *',
                            'width' => '250px',
                        ],
                        [
                            'label' => 'Primair *',
                            'width' => '100px',
                        ],
                        [
                            'label' => 'E-mail afzender *',
                            'width' => '200px',
                        ],
                        [
                            'label' => 'E-mailadres *',
                            'width' => '200px',
                        ],
                        [
                            'label' => 'Taal *',
                            'width' => '100px',
                        ],
                    ],
                ],
            ]);

        $container->add($column);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabFinancial()
    {
        $tab = $this->builder->create('site_container_tab_financial', TabType::class, [
            'label' => 'Financiele instelling',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        /* Default settings */

        $container = $this->builder->create('site_container_financial', ContainerType::class, []);

        $column = $this->builder->create('site_container_financial_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Financiele instelling',
            'icon' => 'coin-euro',
        ]);

        $column
            ->add('defaultDisplayPrice', ChoiceType::class, [
                'label' => 'Standaard prijs weergave',
                'required' => true,
                'choices' => [
                    'Inclusief BTW' => 'inclusive',
                    'Exclusief BTW' => 'exclusive',
                ],
            ]);

        $container->add($column);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabPaymentMethods()
    {
        $tab = $this->builder->create('site_container_tab_payment_methods', TabType::class, [
            'label' => 'Betaalmethodes',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        /* Default settings */

        $container = $this->builder->create('site_container_payment_methods', ContainerType::class, []);

        $column = $this->builder->create('site_container_payment_methods_column_0', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Betaalmethoden',
        ]);

        $column
            ->add('paymentMethodSettings', CollectionType::class, [
                'entry_type' => SitePaymentMethodSettingsType::class,
                'label' => 'Betaalmethoden',
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'width' => '200px',
                        ],
                        [
                            'width' => '200px',
                        ],
                    ],
                ],
            ]);

        $container->add($column);

        $column = $this->builder->create('site_container_payment_methods_column_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Adyen parameters',
            'icon' => 'coin-euro',
        ]);

        $column
            ->add('adyenMerchantAccount', TextType::class, [
                'label' => 'Merchant account',
                'required' => false,
            ])
            ->add('adyenUsername', TextType::class, [
                'label' => 'Username',
                'required' => false,
            ])
            ->add('adyenPassword', TextType::class, [
                'label' => 'Password',
                'required' => false,
            ])
            ->add('adyenSkinCode', TextType::class, [
                'label' => 'Skincode',
                'required' => false,
            ])
            ->add('adyenHmacKey', TextType::class, [
                'label' => 'HMAC key',
                'required' => false,
            ])
            ->add('adyenCseToken', TextType::class, [
                'label' => 'CSE token',
                'required' => false,
            ])
            ->add('adyenCseKey', TextareaType::class, [
                'label' => 'CSE key',
                'required' => false,
                'attr' => [
                    'style' => 'min-height: 95px',
                ],
            ]);

        $container->add($column);

        $column = $this->builder->create('site_container_payment_methods_column_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'AfterPay parameters',
            'icon' => 'coin-euro',
        ]);

        $column
            ->add('afterpayMerchantId', IntegerType::class, [
                'label' => 'Merchant ID',
                'required' => false,
            ])
            ->add('afterpayPortfolioId', IntegerType::class, [
                'label' => 'Portefeuille ID',
                'required' => false,
            ])
            ->add('afterpayPassword', TextType::class, [
                'label' => 'Wachtwoord',
                'required' => false,
            ])
            ->add('afterpayCaptureDelay', ChoiceType::class, [
                'label' => 'Capture vertraging',
                'required' => false,
                'choices' => [
                    '1 dag' => 1,
                    '2 dagen' => 2,
                    '3 dagen' => 3,
                    '4 dagen' => 4,
                    '5 dagen' => 5,
                    '6 dagen' => 6,
                    '7 dagen' => 7,
                ],
            ]);

        $container->add($column);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabContent()
    {
        $builder = $this->builder;

        $tab = $this->builder->create('site_container_tab_content', TabType::class, [
            'label' => 'Content',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        /* Default settings */

        $container = $this->builder->create('site_container_content', ContainerType::class, []);

        $column = $this->builder->create('site_container_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Homepage',
            'icon' => 'insert-template',
        ]);

        if ($builder->getData() && $builder->getData()->getId()) {
            $column->add('homepage', EntityType::class, [
                'label' => 'Tekstpagina',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);

            $column->add('homepageAssortment', EntityType::class, [
                'label' => 'Assortiment',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->join('a.sites', 'site', 'WITH', 'site.id = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Assortment $assortment) {
                    return $assortment->getName();
                },
                'placeholder' => 'Kies een assortiment',
                'required' => false,
            ]);

            $column->add('homepageAbout', EntityType::class, [
                'label' => 'About',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);

            $column->add('customerServicePopupPage', EntityType::class, [
                'label' => 'Klantenservice Popup',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);

            $column
                ->add('termsAndConditionsPage', EntityType::class, [
                    'label' => 'Algemene voorwaarden',
                    'class' => Page::class,
                    'query_builder' => function (PageRepository $er) use ($builder) {
                        $qb = $er->createQueryBuilder('p')
                            ->select('p')
                            ->where('p.site = :site')
                            ->setParameter('site', $builder->getData()->getId());

                        return $qb;
                    },
                    'choice_label' => function (Page $page) {
                        return $page->translate()->getTitle();
                    },
                    'placeholder' => 'Kies een tekstpagina',
                    'required' => false,
                ]);


            $container->add($column);


            $containerAssortments = $this->builder->create('site_container_content_homepage_assortments', ContainerType::class, []);

            $column = $this->builder->create('site_container_content_homepage_assortments_column', ColumnType::class, [
                'column_layout' => ColumnType::COLUMN_WIDTH_12,
                'label' => 'Kies hier de assortimenten voor op de homepage',
                'icon' => 'check',
            ]);

            $column
                ->add('homepageAssortments', CollectionType::class, [
                    'entry_type' => SiteHomepageAssortmentType::class,
                    'label' => false,
                    'by_reference' => false,
                    'collection' => [
                        'sortable' => true,
                        'columns' => [
                            [
                                'label' => 'Positie *',
                                'width' => '80px',
                            ],
                            [
                                'label' => 'Assortment *',
                                'width' => '300px',
                            ],
                        ],
                    ],
                ])
            ;

            $containerAssortments->add($column);

            $tab->add($container);
            $tab->add($containerAssortments);

            $container = $this->builder->create('site_container_order_success', ContainerType::class, []);

            $column = $this->builder->create('site_container_order_success_column', ColumnType::class, [
                'column_layout' => ColumnType::COLUMN_WIDTH_12,
                'label' => 'Bevestigingspagina\'s',
                'icon' => 'insert-template',
            ]);

            $column->add('orderSuccessPage', EntityType::class, [
                'label' => 'Bedankt',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => true,
            ]);

            $column->add('paymentPendingPage', EntityType::class, [
                'label' => 'Wacht op betaling',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);

            $container->add($column);

            $tab->add($container);
        }

        /* USPS */

        $container = $this->builder->create('site_container_content_usp', ContainerType::class, []);

        $column = $this->builder->create('site_container_content_usp_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => "Kies hier de USP's",
            'icon' => 'check',
        ]);

        $column
            ->add('usps', CollectionType::class, [
                'entry_type' => SiteUspType::class,
                'label' => 'USP',
                'by_reference' => false,
                'collection' => [
                    'sortable' => true,
                    'columns' => [
                        [
                            'label' => 'Positie *',
                            'width' => '80px',
                        ],
                        [
                            'label' => 'USP *',
                            'width' => '300px',
                        ],
                    ],
                ],
            ]);

        $column->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if (false !== isset($event->getData()['usps'])) {
                $rootForm = $event->getForm()->getRoot();

                /** @var Site $site */
                $site = $rootForm->getData();

                $index = [];

                /** @var SiteUsp $siteUsp */
                foreach ($event->getData()['usps'] as $siteUsp) {

                    $value = $site->getId() . '_' . $siteUsp['usp'];

                    if (\in_array($value, $index, true)) {
                        $event->getForm()->addError(new FormError('Een USP mag maar één keer aan een website worden toegevoegd.'));
                    }

                    $index[] = $value;
                }
            }
        });

        $container->add($column);

        $tab->add($container);

        /* Custom blocks */

        $container = $this->builder->create('site_container_content_custom_html', ContainerType::class, []);

        $column = $this->builder->create('site_container_content_custom_html_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Custom HTML',
            'icon' => 'embed',
        ]);

        $column
            ->add('siteCustomHtml', CollectionType::class, [
                'entry_type' => SiteCustomHtmlType::class,
                'label' => 'Custom HTML',
                'by_reference' => false,
                'collection' => [
                    'sortable' => true,
                    'columns' => [
                        [
                            'label' => 'Order *',
                            'width' => '50px',
                        ],
                        [
                            'label' => 'HTML-tag *',
                            'width' => '200px',
                        ],
                        [
                            'label' => 'Positie *',
                            'width' => '200px',
                        ],
                        [
                            'label' => 'Content *',
                            'width' => '500px',
                        ],
                    ],
                ],
            ]);

        $container->add($column);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabOpeningHours()
    {
        $tab = $this->builder->create('site_container_tab_openinghours', TabType::class, [
            'label' => 'Openingstijden',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $container = $this->builder->create('site_container_content_usp', ContainerType::class, []);

        $column = $this->builder->create('site_container_content_openings_times', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Openingstijden',
            'icon' => 'check',
        ])->add('siteOpeningHours', CollectionType::class, [
            'entry_type' => OpeningHoursType::class,
            'label' => 'Tijd',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Dag *',
                        'width' => '120px',
                    ],
                    [
                        'label' => 'Van tijd *',
                        'width' => '80px',
                    ],
                    [
                        'label' => 'Tot tijd *',
                        'width' => '80px',
                    ],
                ],
            ],
        ]);

        $container->add($column);

        $tab->add($container);

        return $tab;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
