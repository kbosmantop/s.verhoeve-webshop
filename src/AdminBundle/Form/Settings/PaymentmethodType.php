<?php

namespace AdminBundle\Form\Settings;

use AdminBundle\Form\Type\MediaType;
use AppBundle\DBAL\Types\PaymentGatewayType;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentmethodType
 * @package AdminBundle\Form\Settings
 */
class PaymentmethodType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'description' => [
                    'label' => 'Description',
                ],
                'image' => [
                    'label' => 'Logo',
                    'field_type' => MediaType::class,
                    'required' => false,
                ],
            ])
        );

        $container = $builder->create('container1', ContainerType::class, [
            'label' => false,
        ]);

        $container
            ->add('gateway', ChoiceType::class, [
                'label' => 'Gateway',
                'choices' => PaymentGatewayType::getChoices(),
                'placeholder' => "",
                'required' => false,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('code', TextType::class, [
                'label' => 'Code',
            ])
            ->add('product', EntityType::class, [
                'label' => 'Betaalkosten',
                'class' => Product::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p');
                    $qb
                        ->where(
                            $qb->expr()->like('p.sku', ':sku')
                        )
                        ->setParameter('sku', 'PAY%');

                    return $qb;
                },
                'required' => false,
                'choice_label' => function (Product $product) {
                    return $product->getName();
                },
                'placeholder' => "",
            ])
            ->add('maxAmount', MoneyType::class, [
                'label' => 'Maximum bedrag',
                'scale' => 2,
                'required' => false,
            ])
            ->add('countries', EntityType::class, [
                'class' => Country::class,
                'label' => 'Zichtbaar voor bestellers uit:',
                'help_label' => 'Indien niks aangevinkt, dan is deze zichtbaar voor alle bestellers.',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ]);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Paymentmethod::class,
        ]);
    }
}
