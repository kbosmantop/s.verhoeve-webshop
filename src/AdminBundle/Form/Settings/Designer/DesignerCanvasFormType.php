<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerCanvas;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerCanvasFormType
 * @package AdminBundle\Form\Settings\Designer
 */
class DesignerCanvasFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('name', TextType::class, [
            'label' => 'Naam',
        ]);

        $container->add('width', PercentType::class, [
            'scale' => 2,
            'label' => 'Breedte',
        ]);

        $container->add('height', PercentType::class, [
            'scale' => 2,
            'label' => 'Hoogte',
        ]);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerCanvas::class,
        ]);
    }
}
