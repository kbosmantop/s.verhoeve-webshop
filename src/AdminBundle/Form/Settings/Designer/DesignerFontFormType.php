<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerFont;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerFontFormType
 * @package AdminBundle\Form\Settings\Designer
 */
class DesignerFontFormType extends AbstractFormType
{
    /** @var string */
    private $googleApiKey;

    /**
     * DesignerFontFormType constructor.
     *
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->googleApiKey = $parameterBag->get('google_api_key');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('family', ChoiceType::class, [
            'label' => 'Lettertype',
            'select2' => [],
            'placeholder' => 'Kies een font',
            'choices' => $this->getFonts(),
        ]);

        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var $font DesignerFont */
            $font = $event->getData();

            if (in_array($font->getFamily(), $this->getGoogleFonts())) {
                $font->setType("google");
            } else {
                $font->setType(null);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerFont::class,
        ]);
    }

    /**
     * @return array
     */
    private function getFonts()
    {
        return [
            'WebFonts' => $this->getWebFonts(),
            'Google Fonts' => $this->getGoogleFonts(),
        ];
    }

    /**
     * @return array
     */
    private function getGoogleFonts()
    {
        if (!$this->googleApiKey) {
            return [];
        }

        $fontData = file_get_contents('https://www.googleapis.com/webfonts/v1/webfonts?key=' . $this->googleApiKey);

        if (!$fontData) {
            return [];
        }

        $data = json_decode($fontData);

        if (is_null($data)) {
            return [];
        }

        $fonts = [];

        foreach ($data->items as $font) {
            $fonts[$font->family] = $font->family;
        }

        return $fonts;
    }

    /**
     * @return array
     */
    private function getWebFonts()
    {
        return [
            "Arial" => "Arial",
            "Arial Black" => "Arial Black",
            "Comic Sans" => "Comic Sans",
            "Courier" => "Courier",
            "Georgia" => "Georgia",
            "Impact" => "Impact",
            "Lucida Console" => "Lucida Console",
            "Lucida Sans" => "Lucida Sans",
            "Palatino Linotype" => "Palatino Linotype",
            "Tahoma" => "Tahoma",
            "Times New Roman" => "Times New Roman",
            "Trebuchet" => "Trebuchet",
            "Verdana" => "Verdana",
        ];
    }
}
