<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerTemplateObject;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateObjectType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', ChoiceType::class, [
            'choices' => [
                //  'Tekst' => 'text',
                'Afbeelding' => 'image',
            ],
        ]);

        $builder->add('x', PercentType::class, [
            'scale' => 2,
        ]);

        $builder->add('y', PercentType::class, [
            'scale' => 2,
        ]);

        $builder->add('width', PercentType::class, [
            'scale' => 2,
        ]);

        $builder->add('height', PercentType::class, [
            'scale' => 2,
        ]);

//        $builder->add('editable', ChoiceType::class, array(
//            'choices' => [
//                'Ja' => true,
//                'Nee' => false
//            ]
//        ));
//
//        $builder->add('selectable', ChoiceType::class, array(
//            'choices' => [
//                'Ja' => true,
//                'Nee' => false
//            ]
//        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerTemplateObject::class,
        ]);
    }
}
