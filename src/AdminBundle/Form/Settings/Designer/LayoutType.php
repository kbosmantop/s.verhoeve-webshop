<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerLayout;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LayoutType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('name', TextType::class, [
            'label' => "Naam (omschrijving)",
        ]);

        $container->add('width', PercentType::class, [
            'label' => "Breedte",
        ]);

        $container->add('height', PercentType::class, [
            'label' => "Hoogte",
        ]);

//        $container->add('fontFamily', ChoiceType::class, array(
//            'label' => 'Lettertype',
//            'select2' => [
//
//            ],
//            'placeholder' => 'Kies een font',
//            'choices' => $this->getFonts()
//        ));

        $builder->add($container);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerLayout::class,
        ]);
    }
}
