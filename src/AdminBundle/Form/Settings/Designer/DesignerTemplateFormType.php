<?php

namespace AdminBundle\Form\Settings\Designer;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AdminBundle\Form\AddParameterFieldsSubscriber;
use AdminBundle\Form\Designer\DesignerType;
use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Designer\DesignerCanvas;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Entity\Designer\Tag;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\ImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerTemplateFormType
 * @package AdminBundle\Form\Settings\Designer
 */
class DesignerTemplateFormType extends AbstractFormType
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $containerTranslations = $builder->create('translations', ContainerType::class);

        $containerTranslations->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'title' => [
                    'label' => 'Titel',
                    'required' => true
                ],
            ])
        );

        $builder->add($containerTranslations);

        $container = $builder->create('container_general', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('name', TextType::class, [
            'label' => 'Interne naam',
            'help_label' => 'Niet zichtbaar voor klanten',
            'required' => true,
        ]);

        $container->add('canvas', EntityType::class, [
            'label' => 'Vorm',
            'class' => DesignerCanvas::class,
            'choice_label' => function (DesignerCanvas $designerCanvas) {
                return $designerCanvas->getName();
            },
            'placeholder' => '',
            'required' => true,
        ])
        ->add('path', MediaType::class, [
            'label' => 'Afbeelding',
            'instance' => 'form',
            'required' => false,
            'enable' => true,
            'media' => [
                'type' => MediaType::TYPE_IMAGE,
                'thumbnail' => true,
                'upload' => true,
                'dimensions' => [
                    'min' => 'designer_thumbnail',
                ],
            ],
        ])
        ;

        $container->add('tags', EntityType::class, [
            'label' => 'Labels',
            'class' => Tag::class,
            'multiple' => true,
            'expanded' => false,
            'choice_label' => function (Tag $designerTag) {
                return $designerTag->getName();
            },
            'choice_value' => function (Tag $designerTag) {
                return $designerTag->getId();
            },
            'placeholder' => 'Selecteer label(s)',
            'required' => false,
            'select2' => [
                'placeholder' => 'Selecteer label(s)',
            ],
        ]);

        $builder->add($container);


        $container3 = $builder->create('container_settings', ContainerType::class, [
            'label' => 'Instellingen',
        ]);

        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, DesignerTemplate::class,
            $container3));

        $builder->add($container3);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            $request = $this->container->get('request_stack')->getMasterRequest();

            if($request) {
                $companyId = $request->get('company');
                if($companyId) {
                    /** @var Company $company */
                    $company = $this->container->get('doctrine')->getRepository(Company::class)->find($companyId);

                    /** @var DesignerTemplate $template */
                    $template = $event->getData();
                    $template->setCompany($company);
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerTemplate::class,
        ]);
    }
}
