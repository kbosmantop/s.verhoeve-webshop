<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerCollage;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerCollageFormType
 * @package AdminBundle\Form\Settings\Designer
 */
class DesignerCollageFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('name', TextType::class, [
            'label' => 'Naam',
        ]);

        $builder->add($container);

        $container2 = $builder->create('container_objects', ContainerType::class, [
            'label' => false,
        ]);

        $container2->add('objects', CollectionType::class, [
            'entry_type' => DesignerCollageObjectFormType::class,
            'label' => false,
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Type',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'X-positie',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Y-positie',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Breedte',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Hoogte',
                        'width' => '100px',
                    ],
                ],
            ],
        ]);

        $builder->add($container2);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerCollage::class,
        ]);
    }
}
