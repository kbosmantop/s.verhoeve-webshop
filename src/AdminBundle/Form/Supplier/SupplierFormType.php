<?php

namespace AdminBundle\Form\Supplier;

use AppBundle\Entity\Supplier\DeliveryArea;
use Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class SupplierFormType
 * @package AdminBundle\Form\Supplier
 */
class SupplierFormType extends AbstractRegionFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('internalRemark', TextareaType::class, [
                'required' => false,
            ])
            ->add('deliveryAreas', CollectionType::class, [
                'entry_type' => DeliveryAreaCollectionType::class,
                'entry_options' => [
                    'delivery_intervals' => $this->deliveryAreaService->getDeliveryIntervalList(),
                ],
                'label' => 'Bezorggebieden',
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Postcode/Plaats *',
                        ],
                        [
                            'label' => 'Bezorgkosten *',
                        ],
                        [
                            'label' => 'Bezorgkosten Excl. BTW *',
                        ],
                        [
                            'label' => 'Bezorg BTW',
                        ],
                        [
                            'label' => 'Aannametijd *',
                        ],
                    ],
                ],
                'allow_add' => true,
                'allow_delete' => true,
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'onPostSetData']);
    }

    /**
     * @param FormEvent $event
     *
     * @throws Exception
     */
    public function onPostSetData(FormEvent $event)
    {
        $collectionFields = $event->getForm()->get('deliveryAreas')->all();

        foreach ($collectionFields as $key => $collectionField) {

            /** @var DeliveryArea $deliveryArea */
            $deliveryArea = $collectionField->getData();

            $options = $collectionField->get('postcode')->getConfig()->getOptions();

            $collectionField->add('postcode', EntityType::class, array_replace($options, [
                'choices' => [
                    $collectionField->getData()->getPostcode(),
                ],
            ]));

            $supplier = $deliveryArea->getCompany();

            $supplierGroup = $this->deliveryAreaService->getRequestedSupplierSupplierGroup($supplier);

            if ($supplierGroup !== null) {
                $postcode = $deliveryArea->getPostcode();

                [$price, $priceIncl, $vat] = $this->deliveryAreaService->calcRegionPrices($supplierGroup, $postcode, $deliveryArea, false);

                $collectionField->get('deliveryPriceInclVat')->setData($priceIncl);
                $collectionField->get('deliveryPrice')->setData($price);
                $collectionField->get('deliveryVat')->setData($vat);
            }
        }
    }
}
