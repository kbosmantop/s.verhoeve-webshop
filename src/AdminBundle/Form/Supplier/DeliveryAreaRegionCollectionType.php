<?php

namespace AdminBundle\Form\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryAreaRegionCollectionType
 * @package AdminBundle\Form\Supplier
 */
class DeliveryAreaRegionCollectionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', IntegerType::class, [
                'attr' => [
                    'min' => 0,
                    'class' => 'text-right',
                ],
                'mapped' => false,
            ])
            ->add('is_preferred', CheckboxType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'choice_label' => function (Company $supplier) {

                    $connector = $supplier->getSupplierConnector();
                    $label = $supplier->getName();
                    if (!empty($connector)) {
                        $label = "%%{$connector}%%" . $label;
                    }

                    return $label;
                },
                'placeholder' => 'Kies een leverancier',
                'mapped' => true,
                'select2' => [
                    'ajax' => true,
                    'findBy' => [
                        'company',
                    ],
                ],
                'required' => true,
            ])
            ->add('deliveryPriceInclVat', MoneyType::class, [
                'required' => true,
                'mapped' => false,
                'scale' => 2,
                'attr' => [
                    'class' => 'field-price-incl',
                ],
            ])
            ->add('deliveryPrice', MoneyType::class, [
                'required' => true,
                'scale' => 3,
                'attr' => [
                    'class' => 'field-price-excl',
                ],
            ])
            ->add('deliveryVat', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'field-vat',
                ],
            ])
            ->add('deliveryInterval', ChoiceType::class, [
                'choices' => $options['delivery_intervals'],
                'required' => true,
            ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefined([
            'delivery_intervals',
        ]);

        $resolver->setAllowedTypes('delivery_intervals', ['array', 'null']);

        $resolver->setDefaults([
            'data_class' => DeliveryArea::class,
            'delivery_intervals' => [],
        ]);
    }
}
