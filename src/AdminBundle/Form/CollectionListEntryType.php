<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CollectionListEntryType
 * @package AdminBundle\Form
 */
class CollectionListEntryType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return FormType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'collection_list_item';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'list_heading_labels' => false
        ]);
    }
}