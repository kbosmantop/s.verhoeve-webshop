<?php

namespace AdminBundle\Form\Report;

use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportColumn;
use AppBundle\Form\Type\AbstractFormType;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportColumnFormType
 * @package AdminBundle\Form\Report
 */
class ReportColumnFormType extends AbstractFormType
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ReportColumnFormType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', IntegerType::class, [])
            ->add('name', TextType::class, [
                'label' => false,
            ])
            ->add('path', ChoiceType::class, [
                'label' => false,
                'choices' => $this->getChoices(),
            ]);
    }

    /**
     * @param $start
     * @return array
     * @throws \Exception
     */
    private function getChoices()
    {
        $ruleTransformerService = $this->container->get('rule.transformer');
        $em = $this->container->get('doctrine')->getManager();

        $choices = [];

        if ($report = $this->getReport()) {
            $inputs = [
                ResolverService::QB_ALIAS => $em->getRepository(Entity::class)->findOneBy(['fullyQualifiedName' => $report->getRule()->getStart()]),
            ];

            $properties = $ruleTransformerService->getFilters($inputs);


            foreach ($properties as $property) {
                $choices[$property['optgroup'] . ' > ' . $property['label']] = str_replace('__result.', null,
                    $property['id']);
            }
        }

        return $choices;
    }

    /**
     * @return Report|null
     */
    private function getReport()
    {
        if ($this->container->get('request_stack')->getMasterRequest()->get('entity')) {
            $match = $this->container->get('request_stack')->getMasterRequest()->get('entity');
        } else {
            preg_match("/[0-9]+/", $_SERVER["REQUEST_URI"], $match);

            if (!$match) {
                return null;
            }
            $match = $match[0];
        }

        $report = $this->container->get('doctrine')->getRepository(Report::class)->find($match);

        return $report;
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReportColumn::class,
        ]);
    }

}