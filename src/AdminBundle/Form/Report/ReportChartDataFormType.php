<?php

namespace AdminBundle\Form\Report;

use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportChart;
use AppBundle\Entity\Report\ReportChartData;
use AppBundle\Form\Type\AbstractEntityFormType;
use AppBundle\Form\Type\DateType;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

/**
 * Class ReportChartDataFormType
 * @package AdminBundle\Form\Report
 */
class ReportChartDataFormType extends AbstractEntityFormType
{
    /**
     * @var FormBuilderInterface $builder
     */
    protected $builder;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;

        $builder
            ->add('fromDate', DateType::class, [
                'label' => 'Vanaf',
                'required' => false,
            ])
            ->add('tillDate', DateType::class, [
                'label' => 'Tot',
                'required' => false,
            ])
            ->add('sortByPath', ChoiceType::class, [
                'label' => 'Sorteer op',
                'choices' => $this->getChoices(),
            ])
            ->add('dataPath', ChoiceType::class, [
                'label' => 'Sorteer op',
                'choices' => $this->getChoices(),
            ])
            ->add('dataLabel', TextType::class, [
                'label' => 'Datalabel',
            ]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getChoices()
    {
        $requestStack = $this->container->get('request_stack');
        $uri = $requestStack->getMasterRequest()->getRequestUri();

        if(is_null($this->builder->getData())) {
            $report = $this->getEntity(Report::class);
        } else {
            /** @var ReportChart $reportChart */
            $reportChart = $this->getEntity(ReportChart::class);
            $report = $reportChart->getReport();
        }

        $em = $this->container->get('doctrine')->getManager();
        $ruleTransformer = $this->container->get('rule.transformer');

        $inputs = [
            ResolverService::QB_ALIAS => $em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode($report->getRule()->getStart()),
            ]),
        ];

        $filters = $ruleTransformer->getFilters($inputs);
        $filterArray = [];
        foreach($filters as $filter) {
            $filterArray[$filter['optgroup']][$filter['label']] = $filter['id'];
        }

        return $filterArray;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReportChartData::class,
        ]);
    }

}