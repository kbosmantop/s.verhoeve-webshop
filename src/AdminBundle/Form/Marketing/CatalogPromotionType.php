<?php

namespace AdminBundle\Form\Marketing;

use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateTimeType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CatalogPromotionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add($this->mainContainer($builder));
        $builder->add($this->targetingContainer($builder));
        $builder->add($this->discountContainer($builder));
    }

    private function mainContainer(FormBuilderInterface $builder)
    {
        $container = $builder->create('container_main', ContainerType::class);

        $container
            ->add('name', TextType::class, [
                'label' => 'Naam',
            ])
            ->add('start', DateTimeType::class, [
                'label' => 'Start datum/tijd',
            ])
            ->add('end', DateTimeType::class, [
                'label' => 'End datum/tijd',
            ]);


//
//        $column1 = $builder->create('column1', ColumnType::class, array(
//            'column_layout' => ColumnType::COLUMN_WIDTH_4,
//            'label' => 'Algemeen',
//            'icon' => 'profile'
//        ));

        return $container;
    }

    private function targetingContainer(FormBuilderInterface $builder)
    {
        $container = $builder->create('container_targeting', ContainerType::class);

        $container->add('sites', EntityType::class, [
            'label' => 'Site(s)',
            'class' => 'AppBundle\Entity\Site\Site',
            'choice_label' => function ($site) {
                return $site->translate()->getDescription();
            },
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('customergroups', EntityType::class, [
            'label' => 'Klantgroep(en)',
            'class' => 'AppBundle\Entity\Security\Customer\CustomerGroup',
            'choice_label' => function ($customerGroup) {
                return (string)$customerGroup;
            },
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('companies', EntityType::class, [
            'label' => 'Zakelijke klant(en)',
            'class' => 'AppBundle\Entity\Relation\Company',
            'choice_label' => function ($company) {
                return $company->getName();
            },
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('productgroups', EntityType::class, [
            'label' => 'Productgroep(en)',
            'class' => 'AppBundle\Entity\Catalog\Product\Productgroup',
            'choice_label' => function ($productgroup) {
                return $productgroup->getTitle();
            },
            'required' => false,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('products', EntityType::class, [
            'label' => 'Product(en)',
            'class' => 'AppBundle\Entity\Catalog\Product\Product',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->andWhere('p.parent IS NULL');
            },
            'choice_label' => function ($product) {
                return $product->getName();
            },
            'required' => false,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        return $container;
    }

    private function discountContainer(FormBuilderInterface $builder)
    {
        $container = $builder->create('container_discount', ContainerType::class);

        $container
            ->add('discount_percentage', PercentType::class, [
                'label' => 'Kortingspercentage',
                'mapped' => false,
            ]);

        $container
            ->add('discount_amount', MoneyType::class, [
                'label' => 'Kortingsbedrag',
                'mapped' => false,
//                'attr' => array(
//                    "onchange" => "bootbox.alert(\"Hello world!\");"
//                )
            ]);

        return $container;
    }
}
