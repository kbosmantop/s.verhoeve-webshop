<?php

namespace AdminBundle\Form\Finance;

use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;
use AppBundle\DBAL\Types\CompanyInvoiceFrequencyType;
use AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType;
use AppBundle\DBAL\Types\CompanyInvoiceTypeType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Relation\CompanyInvoiceSettings;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyInvoiceSettingsFormType
 * @package AdminBundle\Form\Finance
 */
class CompanyInvoiceSettingsFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $container = $builder->create('company_invoice_settings_container', ContainerType::class, []);

        $column = $builder->create('company_invoice_settings_column', ColumnType::class, [
            'label' => 'Factuur export instellingen',
        ]);

        $column->add('paymentTerm', IntegerType::class, [
            'label' => 'Betaaltermijn',
            'help_label' => 'In dagen',
        ]);

        $column->add('invoiceFrequency', ChoiceType::class, [
            'label' => 'Factuur frequentie',
            'choices' => CompanyInvoiceFrequencyType::getChoices(),
            'placeholder' => 'Maak een keuze',
        ]);

        $column->add('invoiceDiscount', ChoiceType::class, [
            'label' => 'Factuurkorting',
            'choices' => CompanyInvoiceDiscountType::getChoices(),
            'placeholder' => 'Maak een keuze',
        ]);

        $column->add('invoiceInclPersonalData', ChoiceType::class, [
            'label' => 'Persoonsgegevens op factuur',
            'choices' => CompanyInvoiceInclPersonalDataType::getChoices(),
            'placeholder' => 'Maak een keuze',
            'help_label' => 'Let op: privacy gevoelig!',
        ]);

        $column->add('invoiceZeroValue', BooleanType::class, [
            'label' => 'Factuur nulwaarde',
            'choices' => YesNoType::getChoices(),
        ]);

        $column->add('invoiceType', ChoiceType::class, [
            'label' => 'Factuur Type',
            'choices' => CompanyInvoiceTypeType::getChoices(),
            'placeholder' => 'Maak een keuze',
        ]);

        $column->add('invoiceReminderEmail', EmailType::class, [
            'label' => 'Herinnering E-mailadres (facturatie)',
            'required' => false,
        ]);

        $container->add($column);
        $builder->add($container);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompanyInvoiceSettings::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'invoice_settings';
    }
}
