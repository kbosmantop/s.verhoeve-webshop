<?php

namespace AdminBundle\Form\Finance\Vat;

use AppBundle\Entity\Finance\Tax\VatLevel;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VatLevelFormType
 * @package AdminBundle\Form\Finance\Vat
 */
class VatLevelFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('vat_level_container', ContainerType::class, []);

        $column = $builder->create('vat_level_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'BTW Niveau'
        ]);

        $column
            ->add('title', TextType::class, [
                'label' => 'Titel',
                'required' => true,
            ])
            ->add('special', BooleanType::class, [
                'label' => 'Speciaal tarief',
                'required' => false,
            ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VatLevel::class,
        ]);
    }
}