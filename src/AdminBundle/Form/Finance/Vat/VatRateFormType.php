<?php

namespace AdminBundle\Form\Finance\Vat;

use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VatRateFormType
 * @package AdminBundle\Form\Finance\Vat
 */
class VatRateFormType extends AbstractType
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * VatRateFormType constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('vat_rate_container', ContainerType::class, []);

        $column = $builder->create('vat_rate_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'BTW Tarief',
        ]);



        $column
            ->add('title', TextType::class, [
                'label' => 'Titel'
            ])
            ->add('code', TextType::class, [
                'label' => 'Code',
            ])
            ->add('purchase', ChoiceType::class, [
                'label' => 'Betreft',
                'choices' => [
                    'n.v.t.' => null,
                    'Verkoop' => false,
                    'Inkoop' => true,
                ],
                'data' => null === $builder->getData() || null === $builder->getData()->getId() ? false : $builder->getData()->isPurchase()
            ])
            ->add('validFrom', DateType::class, [
                'label' => 'Geldig vanaf'
            ])
            ->add('validUntil', DateType::class, [
                'label' => 'Geldig tot en met',
            ])
            ->add('factor', NumberType::class, [
                'label' => 'Factor',
                'scale' => 2,
                'help_label' => 'De factor is het aantal procent BTW gedeeld door 100. <br/>(Bijvoorbeeld: 9% BTW is een factor van 0,09)'
            ])
            ;

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var VatRate $vatRate */
            $vatRate = $event->getData();

            $vatRate->setCode(strtoupper($vatRate->getCode()));

            $request = $this->requestStack->getMasterRequest();
            if (null !== $request && $request->get('entityClass') === 'vatgroup') {
                /** @var VatGroup $vatGroup */
                $vatGroup = $this->entityManager->getRepository(VatGroup::class)->find($request->get('entity'));

                if (null !== $vatGroup) {
                    $vatRate->setGroup($vatGroup);
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VatRate::class,
        ]);
    }
}