<?php

namespace AdminBundle\Form\Finance\Vat;

use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatLevel;
use AppBundle\Entity\Geography\Country;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VatGroupFormType
 * @package AdminBundle\Form\Finance\Vat
 */
class VatGroupFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('vat_group_container', ContainerType::class, []);

        $column = $builder->create('vat_group_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'BTW Groep'
        ]);

        $column
            ->add('level', EntityType::class, [
                'label' => 'BTW niveau',
                'class' => VatLevel::class,
            ])
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'class' => Country::class,
            ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VatGroup::class,
        ]);
    }
}