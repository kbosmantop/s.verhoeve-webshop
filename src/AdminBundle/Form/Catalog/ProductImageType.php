<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductImageType
 * @package AdminBundle\Form\Catalog
 */
class ProductImageType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * ProductImageType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('position', IntegerType::class, [
                'attr' => [
                    'class' => 'text-right',
                ],
            ])
            ->add('path', MediaType::class, [
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => false,
                    'dimensions' => [
                        'min' => 'product',
                    ],
                ],
            ])
            ->add('main', CheckboxType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'image-application-main',
                ],
            ])
            ->add('category', CheckboxType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'image-application-category',
                ],
            ])
            ->add('description', TextType::class, [
                    'required' => false,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductImage::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'product_image';
    }
}
