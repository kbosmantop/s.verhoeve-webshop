<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\CompoundProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompoundProductsType
 * @package AdminBundle\Form\Catalog
 */
class CompoundProductsType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('quantity', IntegerType::class, [
            'required' => false,
        ]);

        $builder->add('compound_product', EntityType::class, [
            'class' => Product::class,
            'select2' => [
//                'ajax' => true,
//                'findBy' => array(
//                    'name'
//                )
            ],
            'choice_label' => function ($product) {
                return $product->getName();
            },
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompoundProduct::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'sub_product';
    }
}
