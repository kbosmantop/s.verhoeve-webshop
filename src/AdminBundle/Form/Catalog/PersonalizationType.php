<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\Catalog\Product\PublishCompanyProductSubscriber;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AppBundle\DBAL\Types\PersonalizationTypeType;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonalizationType
 * @package AdminBundle\Form\Catalog
 */
class PersonalizationType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'description',
        'shortDescription',
        'slug',
        'metaTitle',
        'imagePath',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var FormBuilderInterface $formContainer
     */
    private $formContainer;

    /**
     * @var array
     */
    private $options;

    /**
     * ProductType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;
        $this->formContainer = $this->builder->create('translations', ContainerType::class, []);

        $this->getImageContainer();
        $this->getProductInfoContainer();
        $this->getContentContainer();
        $this->getPublishableContainer();

        $this->builder->add($this->formContainer);

        $this->builder->addEventSubscriber(new PublishCompanyProductSubscriber());
    }

    public function getImageContainer(): void
    {
        $container = $this->builder->create('container_product_images', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $imageColumn = $this->builder->create('container_product_images', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
        ]);

        $columns = [
            [
                'label' => 'Positie',
                'width' => '150px',
            ],
            [
                'label' => 'Afbeelding',
                'width' => '100px',
            ],
            [
                'label' => 'Main',
                'width' => '50px',
            ],
            [
                'label' => 'Assortiment',
                'width' => '90px',
            ],
            [
                'label' => 'Omschrijving',
                'width' => '150px',
            ],
        ];

        $imageColumn->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => $columns,
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        $container->add($imageColumn);

        $this->formContainer->add($container);
    }

    private function getProductInfoContainer(): void
    {
        $infoContainer = $this->builder->create('container_product_details', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $infoColumn = $this->builder->create('column_product_info', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Extra informatie',
        ]);

        $infoColumn
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
                'help_label' => 'Niet zichtbaar voor klanten',
                'required' => true,
            ])
            ->add('productgroup', EntityType::class, [
                'label' => 'Productgroep',
                'class' => Productgroup::class,
                'required' => true,
            ])
            ->add('propertySet', EntityType::class, [
                'label' => 'Kenmerkenset',
                'class' => ProductgroupPropertySet::class,
                'required' => true,
            ])
            ->add('personalizationType', ChoiceType::class, [
                'label' => 'Type personalisatie',
                'choices' => PersonalizationTypeType::getChoices(),
                'required' => true,
            ])
            ->add('clothingPrint', CheckboxType::class, [
                'label' => 'Kledingopdruk',
                'help_block' => 'Wordt o.a. gebruikt om personalisatie gespiegeld af te drukken',
                'required' => false,
            ])
            ->add('printRotation', ChoiceType::class, [
                'label' => 'Gedraaid afdrukken',
                'placeholder' => false,
                'choices' => [
                    'Standaard' => 0,
                    'Linksom 90 graden draaien' => 90,
                    'Rechtsom 90 graden draaien' => -90,
                    '180 graden draaien' => 180,
                ],
                'help_block' => 'Wordt o.a. gebruikt om personalisatie gedraaid af te drukken',
                'required' => false,
            ]);

        $infoContainer->add($infoColumn);
        $this->formContainer->add($infoContainer);
    }

    public function getContentContainer(): void
    {
        $container = $this->builder->create('container_product_content', ContainerType::class, [
            'attr' => [
                'style' => 'margin: 10px; width: 100%;',
            ],
        ]);

        $column = $this->builder->create('container_details_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $column->add('translations', TranslationsType::class, $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
                'attr' => [
                    'class' => 'slugifyTitle',
                ],
            ],
            'description' => [
                'label' => 'Omschrijving',
                'field_type' => CKEditorType::class,
                'ckeditor' => [
                    'custom_toolbar' => 'basic',
                ],
            ],
            'shortDescription' => [
                'label' => 'Korte Omschrijving',
                'field_type' => TextareaType::class,
            ],
            'slug' => [
                'label' => 'Slug',
                'required' => false,
                'field_type' => SlugType::class,
                'base_on' => '.slugifyTitle',
            ],
        ])
        );

        $container->add($column);

        $this->formContainer->add($container);
    }

    private function getPublishableContainer(): void
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_publishable', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Publicatie',
            'icon' => 'traffic-lights',
        ]);

        parent::addPublishableFields($column, $this->builder, $this->options);

        $container->add($column);

        $this->formContainer->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personalization::class,
            'translatable_class' => Product::class,
        ]);
    }
}
