<?php

namespace AdminBundle\Form\Catalog;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AdminBundle\Form\Catalog\ProductgroupProperty\OptionType;
use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductGroupProperty;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductgroupPropertyType
 * @package AdminBundle\Form\Catalog
 */
class ProductgroupPropertyType extends AbstractFormType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add($this->renderContainer($builder));

        $builder->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'description' => [
                    'label' => "Omschrijving",
                    'required' => true,
                ],
                'slug' => [
                    'field_type' => SlugType::class,
                    'required' => false,
                ],
            ])

        );
    }

    private function renderContainer(FormBuilderInterface $builder)
    {
        $container = $builder->create('container_details', ContainerType::class);

        $column1 = $builder->create('column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Algemeen',
            'icon' => 'profile',
        ]);

        $column1
            ->add('key', TextType::class, [
                'label' => "Sleutelwaarde",
                'required' => true,
            ]);

        $container->add($column1);

        $column2 = $builder->create('column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => "Eigenschappen",
        ]);

        $column2
            ->add('form_type', ChoiceType::class, [
                'label' => "Type",
                'choices' => ProductgroupPropertyTypeType::getChoices(),
            ])
            ->add('suffix', TextType::class, [
                'label' => "Tekst toevoegen",
                'required' => false,
            ])
            ->add('form_type_options', ChoiceType::class, [
                'label' => "Opties",
                'required' => false,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Selecteer opties',
                ],
                'choices' => [
                    'Verplicht veld' => 'required',
                    'Alleen lezen' => 'read_only',
                    'Meerdere keuzes mogelijk' => 'multiple',
                    'Unieke waarde' => 'unique',
                ],
            ])
            ->add('configurable', CheckboxType::class, [
                'label' => "Configureerbaar",
                'required' => false,
            ])
            ->add('filterable', CheckboxType::class, [
                'label' => "Opnemen in filter",
                'required' => false,
            ])->add('onFrontend', CheckboxType::class, [
                'label' => "Tonen in front-end",
                'required' => false,
            ])->add('isUnique', CheckboxType::class, [
                'label' => "Waarde moet uniek zijn",
                'required' => false,
            ]);

        $container->add($column2);

        $column3 = $builder->create('column3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => '...',
            'icon' => 'price-tag3',
        ]);

        $column3->add('options', CollectionType::class, [
            'label' => "Keuze",
            'entry_type' => OptionType::class,
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
            ],
        ]);

        $column3->add('product', EntityType::class, [
            'class' => 'AppBundle\Entity\Catalog\Product\Product',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->andWhere('p.parent IS NULL');
            },
            'required' => false,
            'choice_label' => function (Product $product) {
                return $product->getName();
            },
        ]);

        $column3->add('assortment', EntityType::class, [
            'label' => "Assortment",
            'class' => 'AppBundle\Entity\Catalog\Assortment\Assortment',
            'required' => false,
            'choice_label' => function (Assortment $assortment) {
                return $assortment->getName();
            },
        ]);

        $container->add($column3);

        return $container;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductgroupProperty::class,
        ]);
    }
}
