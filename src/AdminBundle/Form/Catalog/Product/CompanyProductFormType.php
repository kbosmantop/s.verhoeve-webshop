<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Exceptions\ImageUploadException;
use AdminBundle\Form\Catalog\ProductImageType;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DesignSelectType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyProductFormType
 * @package AdminBundle\Form\Product
 */
class CompanyProductFormType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'description',
        'shortDescription',
        'slug',
        'metaTitle',
        'imagePath',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CompanyProductFormType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('translations', ContainerType::class, []);

        $column = $builder->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column->add('name', TextType::class, [
           'label' => 'Naam'
        ]);

        $column->add('translations', TranslationsType::class, $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
                'attr' => [
                    'class' => 'slugifyTitle',
                    'placeholder' => 'Overnemen van gerelateerd product',
                ],
                'required' => false,
            ],
            'description' => [
                'label' => 'Omschrijving',
                'field_type' => CKEditorType::class,
                'ckeditor' => [
                    'custom_toolbar' => 'basic',
                ],
                'required' => false,
                'attr' => [
                    'placeholder' => 'Overnemen van gerelateerd product',
                ],
            ],
            'shortDescription' => [
                'label' => 'Korte Omschrijving',
                'field_type' => TextareaType::class,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Overnemen van gerelateerd product',
                ],
            ],
            'slug' => [
                'label' => 'Slug',
                'help_label' => "Aanduiding in URL, bijv.: 'fruitmand-basis'",
                'required' => false,
                'field_type' => SlugType::class,
                'base_on' => '.slugifyTitle',
                'attr' => [
                    'placeholder' => 'Overnemen van gerelateerd product',
                ],
            ],
        ])
        );

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var FilesystemOrphanageStorage $manager */
            $manager = $this->container->get('oneup_uploader.orphanage_manager')->get('product_image');

            // get files
            $files = $manager->getFiles();
            $totalFiles = \count($files);

            // Check if we got uploaded files
            if (null !== $files && $totalFiles >= 1) {

                // upload all files to the configured storage
                $uploadedFiles = $manager->uploadFiles();

                if ($totalFiles !== \count($uploadedFiles)) {

                    $event->getForm()->addError(new FormError("Image couldn't be uploaded"));
                    throw new ImageUploadException();
                }
            }
        });

        $column->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Positie',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Afbeelding',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Main',
                        'width' => '50px',
                    ],
                    [
                        'label' => 'Assortiment',
                        'width' => '90px',
                    ],
                    [
                        'label' => 'Omschrijving',
                        'width' => '150px',
                    ],
                ],
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        $publicationColumn = $builder->create('publication_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Publicatie'
        ]);

        $container->add($column);

        $container->add($publicationColumn);
        $builder->add($container);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var CompanyProduct $product */
            $product = $event->getData();

            $form->get('translations')->get('publication_column')->add('hide', CheckboxType::class, [
                'label' => 'Verbergen',
                'data' => false === $product->getPublish(),
                'mapped' => false,
                'required' => false,
            ]);
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var CompanyProduct $product */
            $product = $event->getData();

            $hideFieldData = $form->get('translations')->get('publication_column')->get('hide')->getData();
            $product->setPublish($hideFieldData ? false : (
                true == $product->getRelatedProduct()->getPublish() ? true : null));
        });
    }

    /**
     * @param OptionsResolver $resolver
     * @return array
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return [
            'data_class' => CompanyProduct::class,
        ];
    }
}
