<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\CompanyTransportType;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyTransportTypeFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class CompanyTransportTypeFormType extends AbstractType
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CompanyTransportTypeFormType constructor.
     * @param RequestStack           $requestStack
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class, []);

        $column = $builder->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Bezorgkosten bedrijf',
        ]);

        $column
            ->add('productGroup', EntityType::class, [
                'class' => Productgroup::class,
                'label' => 'Productgroep',
            ])
            ->add('transportType', EntityType::class, [
                'class' => TransportType::class,
                'label' => 'Bezorgproduct',
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prijs',
                'help_label' => 'Exclusief BTW.',
            ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
           $request = $this->requestStack->getMasterRequest();

           /** @var CompanyTransportType $companyTransportType */
           $companyTransportType = $event->getData();
           if($request && $request->get('company')) {
               $company = $this->entityManager->getRepository(Company::class)->find($request->get('company'));

               $companyTransportType->setCompany($company);
           }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyTransportType::class,
        ]);
    }
}