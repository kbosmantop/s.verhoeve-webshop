<?php

namespace AdminBundle\Form\Catalog\Product;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonalizationType
 * @package AdminBundle\Form\Catalog
 */
class PersonalizationVariationFormType extends ProductVariationFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        if($builder->has('product_combination_container')) {
            $builder->remove('product_combination_container');
        }

        $builder->remove('container_usp');
        $builder->remove('container_product_stock');
        $builder->remove('container_transporttypes');
        $builder->remove('container_packagingunits');
        $builder->remove('container_bbe');
        $builder->remove('container_serial');
        $builder->remove('container_product_quantity');

        //Add parameter container
        $parameterContainer = $builder->create('personalization_variation_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen (Product)',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Personalization::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personalization::class,
            'translatable_class' => Product::class,
        ]);
    }
}
