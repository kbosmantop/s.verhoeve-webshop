<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\ExpirationMeta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ExpirationMetaFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class ExpirationMetaFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('period', IntegerType::class, [
                'label' => 'Periode',
                'required' => false,
                'help_block' => 'Het aantal dagen dat een product "vers" kan blijven'
            ])
            ->add('guarantee', IntegerType::class, [
                'label' => 'Garantietijd',
                'required' => false,
                'help_block' => 'Het aantal dagen dat een product "vers" kan blijven na levering'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExpirationMeta::class,
        ]);
    }

}