<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\SerialNumberMeta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SerialNumberMetaFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class SerialNumberMetaFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level', IntegerType::class, [
                'label' => 'Level',
                'required' => false,
                'help_block' => 'Vul voor unieke nummers de waarde 10 in'
            ])
            ->add('fixSer', TextType::class, [
                'label' => 'Prefix',
                'required' => false,
                'help_block' => 'Deze 8 cijfers bepalen het merk'
            ])
            ->add('mask', TextType::class, [
                'label' => 'Mask',
                'required' => false,
                'help_block' => 'Bijv: 99999999 Controleert of de eerste 8 karakters cijfers zijn'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SerialNumberMeta::class,
        ]);
    }

}