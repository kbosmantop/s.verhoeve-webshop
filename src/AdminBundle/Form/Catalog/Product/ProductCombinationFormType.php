<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Exceptions\ImageUploadException;
use AdminBundle\Form\Catalog\ProductImageType;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Extension\TabbedFormTypeExtension;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DesignSelectType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductCombinationFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class ProductCombinationFormType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var FormBuilderInterface $builder
     */
    protected $builder;

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * ProductCombinationFormType constructor.
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface     $container
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;

        $cardAssortmentType = $this->entityManager->getRepository(AssortmentType::class)->findOneBy([
            'key' => AssortmentType::TYPE_CARDS,
        ]);

        $additionalProductsAssortmentType = $this->entityManager->getRepository(AssortmentType::class)->findOneBy([
            'key' => AssortmentType::TYPE_ADDITIONAL_PRODUCTS,
        ]);

        $mainContainer = $builder->create('translations', ContainerType::class, [
            'label' => 'Details',
        ]);

        $container = $builder->create('translations', ContainerType::class, []);

        $imageColumn = $builder->create('container_product_images', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Afbeeldingen',
        ]);

        $imageColumn->add($this->getTabProductImages($builder));

        $container->add($imageColumn);

        $productSettingsColumn = $builder->create('container_product_settings', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_8,
            'label' => 'Instellingen',
        ]);


        $column1 = $builder->create('container_details_column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Algemeen',
        ]);

        $column1
            ->add('productgroup', EntityType::class, [
                'placeholder' => '',
                'label' => 'Productgroep',
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p, COALESCE(pp.title, p.title) as HIDDEN columnOrder')
                        ->leftJoin('p.parent', 'pp', 'p.parent = pp.id')
                        ->addOrderBy('columnOrder', 'ASC')
                        ->addOrderBy('p.parent', 'ASC')
                        ->addOrderBy('p.title', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Productgroup $productgroup) {
                    $label = '';

                    if ($productgroup->getParent()) {
                        $label .= ' - ';
                    }

                    return $label . (string)$productgroup;
                },
                'select2' => [],
            ]);

        $column1
            ->add('cards', EntityType::class, [
                'label' => 'Bijbehorende kaartjes',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($cardAssortmentType) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->andWhere('a.assortmentType = :assortmentType')
                        ->setParameter('assortmentType', $cardAssortmentType);

                    return $qb;
                },
                'choice_label' => function (Assortment $assortment) {
                    return $assortment->getName();
                },
                'placeholder' => '',
                'required' => false,
            ])
            ->add('additionalProductAssortments', EntityType::class, [
                'label' => 'Extra producten',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($additionalProductsAssortmentType) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->andWhere('a.assortmentType = :assortmentType')
                        ->setParameter('assortmentType', $additionalProductsAssortmentType)
                        ->orderBy('a.name', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Assortment $assortment) {
                    return $assortment->getName();
                },
                'placeholder' => '',
                'required' => false,
                'multiple' => true,
                'multiselect' => [],
            ]);

        $productSettingsColumn->add($column1);

        $column2 = $builder->create('container_details_column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => false,
            'icon' => false,
        ]);

        $column2
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
                'help_label' => 'Niet zichtbaar voor klanten',
                'required' => true,
            ])
            ->add('ean', NumberType::class, [
                'label' => 'EAN',
                'required' => false,
            ])
            ->add('sku', TextType::class, [
                'label' => 'SKU',
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ]);

        $productSettingsColumn->add($column2);

        $column3 = $builder->create('container_details_column3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
            'required' => true,
        ]);

        $column3->add('vat', VatType::class);

        $column3->add('customergroups', EntityType::class, [
            'label' => 'Beschikbaar voor',
            'placeholder' => 'Alle klanten',
            'class' => CustomerGroup::class,
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $purchasableOptions = [
            'label' => 'Product is aan te schaffen',
            'choices' => YesNoType::getChoices(),
            'required' => true,
        ];

        if ($builder->getData() && null === $builder->getData()->getPurchasable()) {
            $purchasableOptions['data'] = true;
        }


        $column3->add('purchasable', BooleanType::class, $purchasableOptions);

        $productSettingsColumn->add($column3);
        $container->add($productSettingsColumn);

        $mainContainer->add($container);

        $container = $builder->create('container_product_content', ContainerType::class, []);

        $column = $builder->create('container_details_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $column->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'title' => [
                    'label' => 'Titel',
                    'attr' => [
                        'class' => 'slugifyTitle',
                    ],
                ],
                'description' => [
                    'label' => 'Omschrijving',
                    'field_type' => CKEditorType::class,
                    'ckeditor' => [
                        'custom_toolbar' => 'basic',
                    ],
                ],
                'shortDescription' => [
                    'label' => 'Korte Omschrijving',
                    'field_type' => TextareaType::class,
                ],
                'slug' => [
                    'label' => 'Slug',
                    'required' => false,
                    'field_type' => SlugType::class,
                    'base_on' => '.slugifyTitle',
                ],
            ])
        );

        $container->add($column);

        $mainContainer->add($container);

        $container = $builder->create('container_publishable', ContainerType::class, []);

        $column = $builder->create('column_publishable', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        parent::addPublishableFields($column, $builder, []);

        $container->add($column);

        $mainContainer->add($container);

        $builder->add($mainContainer);

    }

    /**
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    private function getTabProductImages(FormBuilderInterface $builder)
    {

        /** @var GenericProduct $product */
        $product = $builder->getData();

        $tab = $this->builder->create('tab_product_images', TabType::class, [
            'label' => 'Afbeeldingen',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $this->builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var FilesystemOrphanageStorage $manager */
            $manager = $this->container->get('oneup_uploader.orphanage_manager')->get('product_image');

            // get files
            $files = $manager->getFiles();
            $totalFiles = \count($files);

            // Check if we got uploaded files
            if (null !== $files && $totalFiles >= 1) {

                // upload all files to the configured storage
                $uploadedFiles = $manager->uploadFiles();

                if ($totalFiles !== \count($uploadedFiles)) {

                    $event->getForm()->addError(new FormError('Image couldn\'t be uploaded'));
                    throw new ImageUploadException('Image couldn\'t be uploaded');
                }
            }
        });

        $columns = [
            [
                'label' => 'Positie',
                'width' => '150px',
            ],
            [
                'label' => 'Afbeelding',
                'width' => '100px',
            ],
            [
                'label' => 'Main',
                'width' => '50px',
            ],
            [
                'label' => 'Assortiment',
                'width' => '90px',
            ],
            [
                'label' => 'Omschrijving',
                'width' => '150px',
            ],
            [
                'label' => 'Variatie',
                'width' => '400px',
            ],
        ];

        $tab->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => $columns,
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        if ($product->hasPersonalization()) {
            $tab->add('metadata', DesignSelectType::class, [
                'required' => false,
            ]);
        }

        return $tab;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

}
