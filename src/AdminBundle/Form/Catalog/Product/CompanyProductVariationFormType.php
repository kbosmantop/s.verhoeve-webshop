<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Exceptions\ImageUploadException;
use AdminBundle\Form\Catalog\ProductImageType;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DesignSelectType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Oneup\UploaderBundle\Uploader\Orphanage\OrphanageManager;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyProductFormType
 * @package AdminBundle\Form\Product
 */
class CompanyProductVariationFormType extends AbstractFormType
{

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var OrphanageManager $orphanageManager
     */
    protected $orphanageManager;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'description',
        'shortDescription',
        'slug',
        'metaTitle',
        'imagePath',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /**
     * CompanyProductVariationFormType constructor.
     * @param OrphanageManager $orphanageManager
     */
    public function __construct(OrphanageManager $orphanageManager)
    {
        $this->orphanageManager = $orphanageManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('translations', ContainerType::class, []);

        $column = $builder->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column->add('translations', TranslationsType::class, $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
                'attr' => [
                    'class' => 'slugifyTitle',
                    'placeholder' => 'Overnemen van gerelateerd product'
                ],
                'required' => false,
            ],
            'description' => [
                'label' => 'Omschrijving',
                'field_type' => CKEditorType::class,
                'ckeditor' => [
                    'custom_toolbar' => 'basic',
                ],
                'required' => false,
                'attr' => [
                    'placeholder' => 'Overnemen van gerelateerd product'
                ],
            ],
            'shortDescription' => [
                'label' => 'Korte Omschrijving',
                'field_type' => TextareaType::class,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Overnemen van gerelateerd product'
                ],
            ],
        ])
        );

        $column->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
            'required' => false,
            'attr' => [
                'placeholder' => 'Overnemen van gerelateerd product'
            ],
        ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var FilesystemOrphanageStorage $manager */
            $manager = $this->orphanageManager->get('product_image');

            // get files
            $files = $manager->getFiles();
            $totalFiles = \count($files);

            // Check if we got uploaded files
            if (null !== $files && $totalFiles >= 1) {

                // upload all files to the configured storage
                $uploadedFiles = $manager->uploadFiles();

                if ($totalFiles !== \count($uploadedFiles)) {

                    $event->getForm()->addError(new FormError("Image couldn't be uploaded"));
                    throw new ImageUploadException();
                }
            }
        });

        $column->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Positie',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Afbeelding',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Main',
                        'width' => '50px',
                    ],
                    [
                        'label' => 'Assortiment',
                        'width' => '90px',
                    ],
                    [
                        'label' => 'Omschrijving',
                        'width' => '150px',
                    ],
                ],
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        $publicationColumn = $builder->create('publication_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Publicatie'
        ]);

        $detailsColumn = $builder->create('details_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Details'
        ])
        ->add('sku', TextType::class, [
            'label' => 'Bedrijfs SKU',
            'required' => false,
            'disabled' => true,
        ]);

        $container->add($column);
        $container->add($publicationColumn);
        $container->add($detailsColumn);

        $personalizationColumn = $builder->create('personalization_design_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Ontwerp',
        ]);

        $personalizationColumn->add('metadata', DesignSelectType::class, [
            'required' => false
        ]);

        $container->add($personalizationColumn);

        $builder->add($container);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var CompanyProduct $product */
            $product = $event->getData();

            $form->get('translations')->get('publication_column')->add('hide', CheckboxType::class, [
                'label' => 'Verbergen',
                'data' => false === $product->getPublish(),
                'mapped' => false,
                'required' => false,
            ]);
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var CompanyProduct $product */
            $product = $event->getData();

            $hideFieldData = $form->get('translations')->get('publication_column')->get('hide')->getData();
            $product->setPublish($hideFieldData ? false : (
                true == $product->getRelatedProduct()->getPublish() ? true : null));
        });
    }

    /**
     * @param OptionsResolver $resolver
     * @return array
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return [
            'data_class' => CompanyProduct::class,
        ];
    }
}
