<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProductgroupPropertySetFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class ProductgroupPropertySetFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_productgroup_property_set', ContainerType::class, []);

        $container
            ->add('name', TextType::class, [
                'label' => 'Set naam',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Beschrijving',
            ])
            ->add('productgroupProperties', EntityType::class, [
                'class' => ProductgroupProperty::class,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Selecteer kenmerken',
                ],
                'label' => 'Kenmerken',
            ])
            ->add('productgroup', EntityType::class, [
                'class' => Productgroup::class,
                'label' => 'Productgroep',
            ]);

        $builder->add($container);
    }
}
