<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Vat;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VatType
 * @package AdminBundle\Form\Product
 */
class VatType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => 'BTW',
            'required' => true,
            'class' => Vat::class,
            'placeholder' => 'Selecteer een BTW percentage',
        ]);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return EntityType::class;
    }
}