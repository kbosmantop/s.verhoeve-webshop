<?php

namespace AdminBundle\Form\Catalog\Product;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Product\ProductSticker;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductStickerFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class ProductStickerFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class, []);
        $column = $builder->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column
            ->add('description', TextType::class, [
                'label' => 'Beschrijving',
            ])
            ->add('rule', EntityType::class, [
                'label' => 'Regel',
                'class' => Rule::class,
            ])
            ->add('path', MediaType::class, [
                'label' => 'Afbeelding',
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => true,
                ],
            ]);

        parent::addPublishableFields($column, $builder, []);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductSticker::class,
        ]);
    }
}
