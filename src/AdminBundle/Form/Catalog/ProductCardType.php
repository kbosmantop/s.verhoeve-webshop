<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AdminBundle\Form\Catalog\Product\ProductPropertyTrait;
use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Form\Extension\TabbedFormTypeExtension;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use function preg_match;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Security\Customer\CustomerGroup;

/**
 * Class ProductCardType
 * @package AdminBundle\Form\Catalog
 */
class ProductCardType extends AbstractFormType
{
    use ProductPropertyTrait;

    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'imagePath',
    ];

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * @var Product|null
     */
    private $product;

    /**
     * ProductType constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface     $container
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->product = $this->getProduct();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;

        $container = $builder->create('translations', ContainerType::class, []);

        $container->add($this->getTabGeneral());
        $container->add($this->getTabPublishable());

        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $event->getData()->setType(null);
        });
        //Add parameter container
        $parameterContainer = $builder->create('product_card_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen (Product)',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, ProductCard::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneral(): FormBuilderInterface
    {
        $tab = $this->builder->create('tab_general', TabType::class, [
            'label' => 'Algemeen',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $tab->add($this->getTabGeneralContainerGeneral());
        $tab->add($this->getTabGeneralContainerFinance());
        $tab->add($this->getTabGeneralContainerImage());
        $tab->add($this->getTabGeneralContainerContent());

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerGeneral(): FormBuilderInterface
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, []);

        $column1 = $this->builder->create('container_details_column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Algemeen',
        ]);

        $column1
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
                'help_label' => 'Niet zichtbaar voor klanten',
                'required' => true,
            ])
            ->add('productgroup', EntityType::class, [
                'placeholder' => '',
                'label' => 'Productgroep',
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p, COALESCE(pp.title, p.title) as HIDDEN columnOrder')
                        ->leftJoin('p.parent', 'pp', 'p.parent = pp.id')
                        ->addOrderBy('columnOrder', 'ASC')
                        ->addOrderBy('p.parent', 'ASC')
                        ->addOrderBy('p.title', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Productgroup $productgroup) {
                    $label = '';

                    if ($productgroup->getParent()) {
                        $label .= ' - ';
                    }

                    return $label . (string)$productgroup;
                },
                'select2' => [],
            ]);

        $container->add($column1);

        $column2 = $this->builder->create('container_details_column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column2
            ->add('ean', NumberType::class, [
                'label' => 'EAN',
                'required' => false,
            ])
            ->add('sku', TextType::class, [
                'label' => 'SKU',
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ]);

        $container->add($column2);

        $column3 = $this->builder->create('container_details_column3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('customergroups', EntityType::class, [
            'label' => 'Beschikbaar voor',
            'placeholder' => 'Alle klanten',
            'class' => CustomerGroup::class,
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerFinance(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_finance', ContainerType::class);

        $column1 = $this->builder->create('price_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Financieël',
        ]);

        $column1->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
        ]);

        $container->add($column1);

        $column2 = $this->builder->create('price_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column2->add('purchasePrice', MoneyType::class, [
            'label' => 'Inkoopprijs',
            'required' => false,
        ]);

        $container->add($column2);

        $column3 = $this->builder->create('price_3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('vatGroups', EntityType::class, [
            'label' => 'BTW Groepen',
            'class' => VatGroup::class,
            'choice_label' => function(VatGroup $vatGroup) {
                return $vatGroup->getLevel()->getTitle().' ('.$vatGroup->getCountry()->translate()->getName().')';
            },
            'multiselect' => [],
            'multiple' => true,
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerImage(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_image', ContainerType::class);

        $column1 = $this->builder->create('price_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Afbeelding',
        ]);

        $column1->add('imagePath', MediaType::class, [
            'label' => 'Afbeelding',
            'instance' => 'form',
            'enable' => true,
            'media' => [
                'type' => MediaType::TYPE_IMAGE,
                'thumbnail' => true,
                'upload' => true,
                'dimensions' => [
                    'min' => 'product_card',
                ],
            ],
            'required' => false,
        ]);

        $container->add($column1);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerContent(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_content', ContainerType::class);

        $column1 = $this->builder->create('product_container_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $translationOptions = $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
            ],
        ]);

        $column1->add('translations', TranslationsType::class,
            $translationOptions
        );

        $columnProductProperties = $this->builder->create('column_product_properties', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Kenmerken',
        ]);

        $columnProductProperties->add($this->addProductProperties($this->product, $this->builder, $this->container->get('router'), $this->entityManager, $columnProductProperties));

        $container->add($column1);
        $container->add($columnProductProperties);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabPublishable(): FormBuilderInterface
    {
        $tab = $this->builder->create('tab_publishable', TabType::class, [
            'label' => 'Publicatie',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $container = $this->builder->create('container_publishable', ContainerType::class, []);

        parent::addPublishableFields($container, $this->builder, $this->options);

        $tab->add($container);

        return $tab;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductCard::class,
            'translatable_class' => Product::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'product';
    }

    /**
     * @return Product|null
     */
    private function getProduct(): ?Product
    {
        $masterRequest = $this->container->get('request_stack')->getMasterRequest();

        if (null !== $masterRequest && null !== $masterRequest->get('entity')) {
            $match = $masterRequest->get('entity');
        } else {
            preg_match('/\d+/', $_SERVER['REQUEST_URI'], $match);

            if (!$match) {
                return null;
            }

            $match = $match[0];
        }

        return $this->entityManager->getRepository(Product::class)->find($match);
    }
}
