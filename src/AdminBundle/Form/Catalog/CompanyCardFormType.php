<?php

namespace AdminBundle\Form\Catalog;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyCardFormType
 * @package AdminBundle\Form\Catalog
 */
class CompanyCardFormType extends ProductCardType
{
    /**
     * @var array
     */
    protected $translatableProperties = [
        'title',
        'slug',
        'imagePath',
        'shortDescription',
        'description',
        'metaTitle',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /** @var RequestStack $requestStack */
    protected $requestStack;

    /**
     * CompanyCardFormType constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack $requestStack
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, ContainerInterface $container)
    {
        parent::__construct($entityManager, $container);

        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('translations', ContainerType::class, []);

        $detailColumn = $builder->create('detail_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
        ]);

        $translationOptions = $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
            ],
        ]);

        $detailColumn
            ->add('price', MoneyType::class, [
                'label' => 'Verkoopprijs',
            ])
            ->add('imagePath', MediaType::class, [
                'label' => 'Afbeelding',
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => true,
                    'dimensions' => [
                        'min' => 'product_card',
                    ],
                ],
                'required' => false,
            ])
            ->add('translations', TranslationsType::class,
                $translationOptions
            );

        parent::addPublishableFields($detailColumn, $builder, []);

        $container->add($detailColumn);

        $builder->add($container);


        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var CompanyCard $companyCard */
            $companyCard = $event->getData();

            $request = $this->requestStack->getMasterRequest();
            if (null !== $request) {
                $companyId = $request->get('company');
                $parentId = $request->get('parent');

                if (null !== $companyId) {
                    /** @var Company $company */
                    $company = $this->entityManager->getRepository(Company::class)->find($companyId);
                    if (null !== $company) {
                        $companyCard->setCompany($company);
                        $company->addCard($companyCard);
                    }
                }

                if (null !== $parentId) {
                    $parent = $this->entityManager->getRepository(ProductCard::class)->find($parentId);
                    if (null !== $parent) {
                        $companyCard->setParent($parent);
                        $companyCard->setVatGroups($parent->getVatGroups());

                        foreach ($parent->getCustomergroups() as $customergroup) {
                            $companyCard->addCustomergroup($customergroup);
                        }
                    }
                }
            }

            /** @var Productgroup $productGroup */
            $productGroup = $this->entityManager->getRepository(Productgroup::class)->findOneBy([
                'title' => 'Kaartjes',
            ]);

            $companyCard->setProductgroup($productGroup);
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyCard::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'company_card';
    }

}
