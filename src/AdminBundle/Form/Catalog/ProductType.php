<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AdminBundle\Form\Catalog\Product\PublishCompanyProductSubscriber;
use AdminBundle\Form\Catalog\UpsellCollectionType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TranslationsType;
use AppBundle\ThirdParty\Google\Entity\Shopping\Taxonomy;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType
 * @package AdminBundle\Form\Catalog
 */
class ProductType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'description',
        'shortDescription',
        'slug',
        'metaTitle',
        'imagePath',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var FormBuilderInterface $formContainer
     */
    private $formContainer;

    /**
     * @var array
     */
    private $options;

    /**
     * ProductType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;
        $this->formContainer = $this->builder->create('translations', ContainerType::class, []);

        $this->getImageContainer();
        $this->getProductInfoContainer();
        $this->getContentContainer();
        $this->getSeoContainer();
        $this->getDisplayContainer();
        $this->getUspContainer();
        $this->getPublishableContainer();
        $this->getAccessibilityContainer();

        $this->builder->add($this->formContainer);

        $this->builder->addEventSubscriber(new PublishCompanyProductSubscriber());
    }

    public function getImageContainer(): void
    {
        $container = $this->builder->create('container_product_images', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $imageColumn = $this->builder->create('container_product_images', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
        ]);

        $columns = [
            [
                'label' => 'Positie',
                'width' => '150px',
            ],
            [
                'label' => 'Afbeelding',
                'width' => '100px',
            ],
            [
                'label' => 'Main',
                'width' => '50px',
            ],
            [
                'label' => 'Assortiment',
                'width' => '90px',
            ],
            [
                'label' => 'Omschrijving',
                'width' => '150px',
            ],
        ];

        $imageColumn->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => $columns,
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        $container->add($imageColumn);

        $this->formContainer->add($container);
    }

    private function getProductInfoContainer(): void
    {
        /** @var Product $product */
        $product = $this->builder->getData();

        $infoContainer = $this->builder->create('container_product_details', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $infoColumn = $this->builder->create('column_product_info', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Extra informatie',
        ]);

        $infoColumn->add('name', TextType::class, [
            'label' => 'Interne naam',
            'help_label' => 'Niet zichtbaar voor klanten',
            'required' => true,
        ]);

        $cardAssortmentType = $this->entityManager->getRepository(AssortmentType::class)->findOneBy([
            'key' => AssortmentType::TYPE_CARDS,
        ]);

        $additionalProductsAssortmentType = $this->entityManager->getRepository(AssortmentType::class)->findOneBy([
            'key' => AssortmentType::TYPE_ADDITIONAL_PRODUCTS,
        ]);

        $infoColumn
            ->add('cards', EntityType::class, [
                'label' => 'Bijbehorende kaartjes',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($cardAssortmentType) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->andWhere('a.assortmentType = :assortmentType ')
                        ->setParameter('assortmentType', $cardAssortmentType);

                    return $qb;
                },
                'choice_label' => function (Assortment $assortment) {
                    return $assortment->getName();
                },
                'placeholder' => '',
                'required' => false,
            ])
            ->add('additionalProductAssortments', EntityType::class, [
                'label' => 'Extra producten',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($additionalProductsAssortmentType) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->andWhere('a.assortmentType = :assortmentType ')
                        ->setParameter('assortmentType', $additionalProductsAssortmentType)
                        ->orderBy('a.name', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Assortment $assortment) {
                    return $assortment->getName();
                },
                'placeholder' => '',
                'required' => false,
                'multiple' => true,
                'multiselect' => [

                ],
            ])
            ->add('letter', EntityType::class, [
                'placeholder' => 'Selecteer een brief',
                'label' => 'Brief toevoegen',
                'class' => Letter::class,
                'choice_label' => 'displayName',
                'select2' => [],
                'required' => false,
            ]);

        if ($product->isCombination()) {
            $infoColumn
                ->add('configurable', CheckboxType::class, [
                    'label' => 'Configureerbaar',
                    'help_block' => 'Maak deze combinatie configureerbaar voor de klant',
                    'required' => false,
                ])
            ;
        }

        $infoContainer->add($infoColumn);

        $this->formContainer->add($infoContainer);
    }

    public function getContentContainer(): void
    {
        $container = $this->builder->create('container_product_content', ContainerType::class, [
            'attr' => [
                'style' => 'margin: 10px; width: 100%;',
            ],
        ]);

        $column = $this->builder->create('container_details_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $column->add('translations', TranslationsType::class, $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
                'attr' => [
                    'class' => 'slugifyTitle',
                ],
            ],
            'description' => [
                'label' => 'Omschrijving',
                'field_type' => CKEditorType::class,
                'ckeditor' => [
                    'custom_toolbar' => 'basic',
                ],
            ],
            'shortDescription' => [
                'label' => 'Korte Omschrijving',
                'field_type' => TextareaType::class,
            ],
            'slug' => [
                'label' => 'Slug',
                'required' => false,
                'field_type' => SlugType::class,
                'base_on' => '.slugifyTitle',
            ],
        ])
        );

        $container->add($column);

        $this->formContainer->add($container);
    }

    public function getDisplayContainer(): void
    {
        $container = $this->builder->create('container_product_display', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('container_details_option_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Weergave opties',
        ]);

        $column->add('show_options', BooleanType::class, [
            'label' => 'Opties tonen?',
            'choices' => YesNoType::getChoices(),
            'expanded' => true,
            'empty_data' => false,
        ]);

        $this->builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            /** @var Product $product */
            $product = $event->getData();

            if($product->isCombination()) {
                $event->getForm()->get('translations')->get('container_product_display')->get('container_details_option_column')->add('showContent', BooleanType::class, [
                    'label' => 'Producten in combinatie weergeven?',
                    'expanded' => true,
                    'choices' => YesNoType::getChoices(),
                ]);
            }
        });

        $container->add($column);

        $this->formContainer->add($container);
    }

    public function getUspContainer(): void
    {
        $container = $this->builder->create('container_product_usp', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('container_details_usp_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'USP\'s',
        ]);

        $column->add('usps', CollectionType::class, [
            'entry_type' => ProductUspType::class,
            'label' => 'USP',
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        'label' => 'Positie *',
                        'width' => '80px',
                    ],
                    [
                        'label' => 'USP *',
                        'width' => '300px',
                    ],
                ],
            ],
        ]);

        $container->add($column);

        $this->formContainer->add($container);
    }

    private function getPublishableContainer(): void
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_publishable', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Publicatie',
            'icon' => 'traffic-lights',
        ]);

        parent::addPublishableFields($column, $this->builder, $this->options);

        $container->add($column);

        $this->formContainer->add($container);
    }

    private function getAccessibilityContainer(): void
    {
        $container = $this->builder->create('container_accessibility', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $loginRequiredOptions = [
            'label' => 'Zichtbaar voor',
            'choices' => [
                'Iedereen' => 0,
                'Ingelogde gebruikers' => 1,
            ],
            'expanded' => true,
            'multiple' => false,
        ];

        if ($this->builder->getData() && $this->builder->getData()->getLoginRequired() !== true) {
            $loginRequiredOptions['data'] = 0;
        }

        $container->add('loginRequired', ChoiceType::class, $loginRequiredOptions);

        $container->add('accessibleCustomerGroups', EntityType::class, [
            'label' => 'Klantgroep(en)',
            'class' => CustomerGroup::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('customer_group')
                    ->orderBy('customer_group.description', 'ASC');
            },
            'multiple' => true,
            'required' => false,
            'multiselect' => [],
        ]);

        $this->formContainer->add($container);
    }

    /**
     * @return void
     */
    private function getSeoContainer(): void
    {
        $container = $this->builder->create('seo', ContainerType::class, [
            'attr' => [
                'style' => 'margin: 10px; width: 100%;',
            ],
        ]);

        $metaRobotsIndexOptions = [
            'field_type' => BooleanType::class,
            'choices' => [
                'Indexeren' => 1,
                'Niet indexeren' => 0,
            ],
            'expanded' => true,
            'empty_data' => '0',
        ];

        $MetaRobotsFollowOptions = [
            'field_type' => BooleanType::class,
            'choices' => [
                'Volgen' => 1,
                'Niets volgen' => 0,
            ],
            'expanded' => true,
            'empty_data' => '0',
        ];

        if ($this->builder->getData()
            && (null === $this->builder->getData()->translate()->getMetaRobotsIndex()
                || $this->builder->getData()->translate()->getMetaRobotsIndex() === true)) {
            $metaRobotsIndexOptions['data'] = true;
        }

        if ($this->builder->getData()
            && (null === $this->builder->getData()->translate()->getMetaRobotsFollow()
                || $this->builder->getData()->translate()->getMetaRobotsFollow() === true)) {
            $MetaRobotsFollowOptions['data'] = true;
        }

        $container->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'metaTitle' => [
                    'label' => 'Titel',
                    'required' => false,
                ],
                'metaDescription' => [
                    'label' => 'Omschrijving',
                    'required' => false,
                ],
                'metaRobotsIndex' => $metaRobotsIndexOptions,
                'metaRobotsFollow' => $MetaRobotsFollowOptions,
            ])
        );

        $container->add('googleShoppingTaxonomy', EntityType::class, [
            'label' => 'Google Shopping Taxonomy',
            'class' => Taxonomy::class,
            'required' => false,
            'placeholder' => '',
            'choice_label' => function (Taxonomy $taxonomy) {
                return $taxonomy->getProductCategory();
            },
            'select2' => [
                'ajax' => true,
                'findBy' => [
                    'productCategory',
                ],
            ],
        ]);

        $this->formContainer->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class
        ]);
    }
}
