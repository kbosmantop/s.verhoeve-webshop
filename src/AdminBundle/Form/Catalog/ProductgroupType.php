<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\DBAL\Types\UpsellQuantityType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Finance\Ledger;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductgroupType
 * @package AdminBundle\Form\Catalog
 */
class ProductgroupType extends AbstractFormType
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ProductgroupType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $container
            ->add('title', TextType::class, [
                'label' => 'Naam',
            ])
            ->add('parent', EntityType::class, [
                'placeholder' => '',
                'label' => 'Bovenliggende productgroep',
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('productgroup')
                        ->where('productgroup.parent IS NULL')
                        ->orderBy('productgroup.title', 'ASC');

                    return $qb;
                },
                'required' => false,
                'choice_label' => function ($productgroup) {
                    return (string)$productgroup;
                },
            ])
            ->add('ledger', EntityType::class, [
                'placeholder' => '',
                'label' => 'Grootboek',
                'class' => Ledger::class,
                'choice_label' => function ($ledger) {
                    return (string)$ledger;
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('ledger')
                        ->orderBy('ledger.name', 'ASC');
                },
            ])
            ->add('template', ChoiceType::class, [
                'placeholder' => 'Kies een template',
                'label' => 'Template',
                'choices' => $this->getCustomTemplates('ProductGroup'),
                'required' => false,
            ])
            ->add('product_template', ChoiceType::class, [
                'placeholder' => 'Kies een template',
                'label' => 'Template producten',
                'choices' => $this->getCustomTemplates('Product'),
                'required' => false,
            ])
            ->add('skuPrefix', TextType::class, [
                'label' => 'SKU Prefix',
                'required' => false,
            ])
            ->add('pickupEnabled', BooleanType::class, [
                'label' => 'Afhalen mogelijk',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('productPropertySelect', BooleanType::class, [
                'label' => 'Product selectie o.b.v. eigenschappen?',
                'choices' => YesNoType::getChoices(),
            ]);

        $builder->add($container);

        $container = $builder->create('transport_type_container', ContainerType::class);
        $column = $builder->create('transport_type_column', ColumnType::class, [
            'label' => 'Bezorging',
            'column_layout' => ColumnType::COLUMN_WIDTH_12
        ]);

        $column->add('productTransportTypes', CollectionType::class, [
            'entry_type' => ProductTransportTypeType::class,
            'label' => 'Bezorgkostenstaffels',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Transport methode *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Max. aantal items',
                        'width' => '150px',
                    ],
                    [],
                ],
            ],
        ]);

        $container->add($column);
        $builder->add($container);

        $container = $builder->create('upsell_container', ContainerType::class);

        $column = $builder->create('upsell_column', ColumnType::class, [
            'label' => 'Upsell',
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column
            ->add('availableForUpsell', CheckboxType::class, [
                'label' => 'Beschikbaar voor upsell',
                'required' => false,
            ])
            ->add('upsellQuantityMode', ChoiceType::class, [
                'label' => 'Aantal bij upsell',
                'choices' => UpsellQuantityType::getChoices(),
                'placeholder' => 'Selecteer een optie',
                'required' => false,
            ]);

        $container->add($column);
        $builder->add($container);

        //Add parameter container
        $parameterContainer = $builder->create('product_group_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Productgroup::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Productgroup::class,
        ]);
    }

    /**
     * @param $dir
     * @return array
     */
    private function getCustomTemplates($dir)
    {
        $fs = new Filesystem();
        $results = [];
        $customTemplateDir = './../src/AppBundle/Resources/views/custom/';

        if ($fs->exists($customTemplateDir . $dir)) {
            $finder = new Finder();
            $finder->files()->in($customTemplateDir . $dir);

            foreach ($finder as $file) {
                $fileBasename = $file->getFilename();
                $results[$fileBasename] = 'AppBundle:custom/' . $dir . ':' . $fileBasename;
            }
        }

        return $results;
    }
}
