<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AdminBundle\Form\Catalog\Product\VatType;
use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Extension\TabbedFormTypeExtension;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LetterType
 * @package AdminBundle\Form\Catalog
 */
class LetterType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * ProductType constructor.
     *
     * @param EntityManagerInterface $em
     * @param ContainerInterface     $container
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;

        $container = $builder->create('translations', ContainerType::class, []);

        $container->add($this->getTabGeneral());

        //Add parameter container
        $parameterContainer = $builder->create('letter_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen (Product)',
        ]);

        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Letter::class,
            $parameterContainer, ColumnType::class));
        $container->add($parameterContainer);

        $builder->add($container);
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneral(): FormBuilderInterface
    {
        $tab = $this->builder->create('tab_general', TabType::class, [
            'label' => 'Algemeen',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $tab->add($this->getGeneralContainer());
        $tab->add($this->getFinanceContainer());
        $tab->add($this->getContentContainer());
        $tab->add($this->getPublishableContainer());

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getGeneralContainer(): FormBuilderInterface
    {
        $container = $this->builder->create('container_general', ContainerType::class, []);

        $column1 = $this->builder->create('container_details_column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Algemeen',
        ]);

        $column1
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
                'help_label' => 'Niet zichtbaar voor klanten',
                'required' => true,
            ])
            ->add('productgroup', EntityType::class, [
                'placeholder' => '',
                'label' => 'Productgroep',
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p, COALESCE(pp.title, p.title) as HIDDEN columnOrder')
                        ->leftJoin('p.parent', 'pp', 'p.parent = pp.id')
                        ->addOrderBy('columnOrder', 'ASC')
                        ->addOrderBy('p.parent', 'ASC')
                        ->addOrderBy('p.title', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Productgroup $productgroup) {
                    $label = '';

                    if ($productgroup->getParent()) {
                        $label .= ' - ';
                    }

                    return $label . $productgroup;
                },
                'select2' => [],
            ]);

        $container->add($column1);

        $column2 = $this->builder->create('container_details_column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => false,
        ]);

        $column2
            ->add('ean', NumberType::class, [
                'label' => 'EAN',
                'required' => false,
            ])
            ->add('sku', TextType::class, [
                'label' => 'SKU',
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ]);

        $container->add($column2);

        $column3 = $this->builder->create('container_details_column3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => false,
        ]);

        $column3->add('customergroups', EntityType::class, [
            'label' => 'Beschikbaar voor',
            'placeholder' => 'Alle klanten',
            'class' => CustomerGroup::class,
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getFinanceContainer(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_finance', ContainerType::class);

        $column1 = $this->builder->create('price_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Financieel',
        ]);

        $column1->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
        ]);

        $container->add($column1);

        $column2 = $this->builder->create('price_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column2->add('purchasePrice', MoneyType::class, [
            'label' => 'Inkoopprijs',
            'required' => false,
        ]);

        $container->add($column2);

        $column3 = $this->builder->create('price_3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('vatGroups', EntityType::class, [
            'label' => 'BTW Groepen',
            'class' => VatGroup::class,
            'choice_label' => function(VatGroup $vatGroup) {
                return $vatGroup->getLevel()->getTitle().' ('.$vatGroup->getCountry()->translate()->getName().')';
            },
            'multiselect' => [],
            'multiple' => true,
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getContentContainer(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_content', ContainerType::class);

        $column1 = $this->builder->create('product_container_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $translationOptions = $this->getA2lixTranslatableFieldsOptions([
            'title' => [
                'label' => 'Titel',
            ],
        ]);

        $translationOptions['excluded_fields'] = [
            'shortDescription',
            'slug',
            'metaTitle',
            'imagePath',
            'metaDescription',
            'metaRobotsIndex',
            'metaRobotsFollow',
        ];

        $column1->add('translations', TranslationsType::class,
            $translationOptions
        );

        $container->add($column1);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getPublishableContainer()
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_publishable', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Publicatie',
            'icon' => 'traffic-lights',
        ]);

        parent::addPublishableFields($column, $this->builder, $this->options);

        $container->add($column);

       return $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Letter::class,
            'translatable_class' => Product::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'product';
    }

}
