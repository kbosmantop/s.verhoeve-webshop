<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\ProductUsp;
use AppBundle\Entity\Site\Usp;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductUspType
 * @package AdminBundle\Form\Catalog
 */
class ProductUspType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ]);

        $builder->add('usp', EntityType::class, [
            'class' => Usp::class,
            'choice_label' => function ($usp) {
                return $usp->translate()->getTitle();
            },
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductUsp::class,
        ]);
    }
}
