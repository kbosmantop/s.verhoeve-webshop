<?php

namespace AdminBundle\Form\Catalog;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TranslationType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SupplierGroupProductType
 * @package AdminBundle\Form\Catalog
 */
class SupplierGroupProductType extends AbstractFormType
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /** @var ContainerInterface */
    private $container;

    /**
     * SupplierProductType constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $requestStack
     * @param ContainerInterface     $container
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $column = $builder->create('body_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Content',
        ]);

        $column
            ->add('title', TranslationType::class, [
                'required' => false,
                'label' => 'Titel',
            ])
            ->add('description', TranslationType::class, [
                'required' => false,
                'label' => 'Beschrijving',
                'field_type' => TextareaType::class,
            ]);

        $supplierColumn = $builder->create('supplier_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Doorstuurgegevens',
        ]);

        $supplierColumn
            ->add('forwardPrice', MoneyType::class, [
                'required' => false,
                'label' => 'Doorgeefprijs',
            ])
            ->add('supplierGroup', EntityType::class, [
                'label' => 'Leveranciersgroep',
                'class' => SupplierGroup::class,
                'placeholder' => '',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('supplier_group')
                        ->orderBy('supplier_group.name', 'ASC');
                },
                'choice_label' => function (SupplierGroup $supplier = null) {
                    return $supplier->getName();
                },
            ])
            ->add('sku', TextType::class, [
                'label' => 'Artikelnummer',
                'required' => false,
            ])
            ->add('pickupEnabled', ChoiceType::class, [
                'label' => 'Afhalen mogelijk',
                'required' => false,
                'placeholder' => 'Instellingen overnemen van productgroep',
                'choices' =>  YesNoType::getChoices()
            ]);

        $container->add($supplierColumn);
        $container->add($column);

        $builder->add($container);
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $productId = $this->requestStack->getMasterRequest()->get('product');

            if(null !== $productId) {
                $product = $this->entityManager->getRepository(Product::class)->find($productId);

                /** @var SupplierGroupProduct $supplierGroupProduct */
                $supplierGroupProduct = $event->getData();
                $supplierGroupProduct->setProduct($product);
            }
        });

        //Add parameter container
        $parameterContainer = $builder->create('supplier_group_product_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, SupplierGroupProduct::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplierGroupProduct::class,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'supplier_group_product';
    }
}
