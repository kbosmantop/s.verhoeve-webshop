<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\EntityChoiceType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Repository\ProductRepository;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderLineType
 * @package AdminBundle\Form\Sales
 */
class OrderLineType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderLineType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', IntegerType::class, [
                'attr' => [
                    'min' => 1,
                    'max' => 999,
                    'data-element' => 'quantity',
                    'class' => 'element-quantity',
                ],
            ])
            ->add('product', EntityChoiceType::class, [
                'field' => 'name',
                'class' => Product::class,
                'choice_label' => function ($id, $value) {
                    return $value;
                },
                'attr' => [
                    'style' => 'width: 400px;',
                    'data-element' => 'product',
                    'class' => 'element-product',
                    'readonly' => true
                ]
            ])
            ->add('price', MoneyType::class, [
                'attr' => [
                    'style' => 'width: 80px;',
                    'data-element' => 'price',
                    'class' => 'element-price',
                ],
                'disabled' => true,
            ])
            ->add('vat', EntityType::class, [
                'class' => Vat::class,
                'attr' => [
                    'style' => 'width: 80px;',
                    'data-element' => 'vat',
                    'class' => 'element-vat',
                ],
                'required' => true,
            ])
            ->add('priceIncl', MoneyType::class, [
                'attr' => [
                    'style' => 'width: 80px;',
                    'data-element' => 'price-incl',
                    'class' => 'element-price-incl',
                ],
                'mapped' => false,
                'required' => false,
                'disabled' => true,
            ])
            ->add('discountAmount', MoneyType::class, [
                'required' => false,
                'attr' => [
                    'style' => 'width: 80px;',
                    'data-element' => 'discount-amount',
                    'class' => 'element-discount',
                ],
            ])
            ->add('metadata', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'data-element' => 'metadata',
                    'class' => 'element-metadata',
                    'readonly' => true
                ],
            ]);

        $builder->get('metadata')
            ->addModelTransformer(new CallbackTransformer(
                function ($metadataArray) {
                    // transform the array to a string
                    return json_encode($metadataArray);
                },
                function ($metadataJson) {
                    // transform the string back to an array
                    return json_decode($metadataJson, true);
                }
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function (FormEvent $event) {
            /** @var OrderLine $data */
            $data = $event->getData();
            $form = $event->getForm();

            if($data) {
                $form->remove('product');
                $form->add('product', EntityChoiceType::class, [
                    'field' => 'name',
                    'class' => Product::class,
                    'choice_label' => function ($id, $value) {
                        return $value;
                    },
                    'query_builder' => function(EntityRepository $er) use($data) {
                        $qb = $er->createQueryBuilder('product');

                        $qb->andWhere('product.id = :productId')
                            ->setParameter('productId', $data->getProduct()->getId());

                        return $qb;
                    },
                    'attr' => [
                        'style' => 'width: 400px;',
                        'data-element' => 'product',
                        'class' => 'element-product',
                        'readonly' => true
                    ],
                ]);
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var OrderLine $data */
            $data = $event->getData();

            $newProductId = (int)$event->getForm()->get('product')->getViewData();

            $productId = null;
            if(null !== $data->getProduct()) {
                $productId = $data->getProduct()->getId();
            }

            if(($newProductId > 0 && $newProductId !== $productId)
            || $productId === null) {
                $newProduct = $this->entityManager
                    ->getRepository(Product::class)
                    ->find($newProductId);

                $data->setProduct($newProduct);
            }

            $data->setTitle($data->getProduct()->getTitle());
            $data->setPrice($data->getProduct()->getPrice());
            $data->setLedger($data->getProduct()->getProductgroup()->getLedger());
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'order_line';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderLine::class,
        ]);
    }
}
