<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Services\PriceService;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SupplierOrderType
 * @package AdminBundle\Form\Sales
 */
class SupplierOrderLineType extends AbstractFormType
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * SupplierOrderLineType constructor.
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('sku', TextType::class, [
                'label' => 'Artikelnr.',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('product', TextType::class, [
                'label' => 'Product',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('quantity', NumberType::class, [
                'label' => 'Aantal',
                'required' => false,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('defaultForwardPrice', MoneyType::class, [
                'label' => 'Standaard doorgeefprijs',
                'required' => false,
                'scale' => 2,
                'mapped' => false,
                'attr' => [
                    'placeholder' => '-',
                    'readonly' => true,
                ],
            ])
            ->add('priceIncl', MoneyType::class, [
                'label' => 'Doorgeefprijs (incl.)',
                'required' => false,
                'scale' => 2,
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'n.v.t.',
                    'class' => 'price-incl'
                ]
            ])
            ->add('price', MoneyType::class, [
                'label' => false,
                'required' => false,
                'scale' => 3,
                'attr' => [
                    'class' => 'price-excl'
                ]
            ])
            ->add('vat', NumberType::class, [
                'label' => false,
                'mapped' => false,
                'attr' => [
                    'readonly' => true,
                    'class' => 'price-vat'
                ]
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                /** @var SupplierOrderLine $supplierOrderLine */
                $supplierOrderLine = $event->getData();
                $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

                $productTitle = $supplierProduct->translate()->getTitle() ?? $supplierProduct->getProduct()->translate()->getTitle();

                $form = $event->getForm();
                $form->get('sku')->setData($supplierProduct->getSku());
                $form->get('product')->setData($productTitle);

                if ($supplierOrderLine->getPrice() !== null) {
                    $priceIncl = $this->priceService->getRawDisplayPrice($supplierOrderLine->getPrice(), $supplierOrderLine->getVatRate());
                    $form->get('defaultForwardPrice')->setData($priceIncl);
                } else {
                    $form->get('price')->setData(0.000);
                }

                $form->get('vat')->setData($supplierOrderLine->getVatRate()->getFactor() * 100);

                if ($supplierOrderLine->getPrice() !== null) {
                    $form->get('priceIncl')->setData($priceIncl);
                }
            });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplierOrderLine::class,
        ]);
    }
}