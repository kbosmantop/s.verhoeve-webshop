<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Form\Common\EntityHiddenTransformer;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SupplierOrderType
 * @package AdminBundle\Form\Sales
 */
class SupplierOrderType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SupplierOrderType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('supplier', HiddenType::class)
            ->add('lines', CollectionType::class, [
                'label' => 'Regels',
                'entry_type' => SupplierOrderLineType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'allow_extra_fields' => false,
                'collection' => [
                    'columns' => [
                        ['label' => 'SKU'],
                        ['label' => 'Product'],
                        ['label' => 'Aantal'],
                        ['label' => 'Basisprijs leverancier'],
                        ['label' => 'Doorgeefprijs'],
                        ['label' => 'Doorgeefprijs (Excl.)'],
                        ['label' => 'VAT'],
                    ]
                ],
            ])
            ->add('reason', HiddenType::class, [
                'mapped' => false,
                'required' => false
            ])
        ;

        $builder->get('supplier')->addModelTransformer(
            new EntityHiddenTransformer($this->entityManager, Company::class, 'id')
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SupplierOrder::class,
        ));
    }
}
