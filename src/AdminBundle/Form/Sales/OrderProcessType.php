<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class OrderProcessType
 * @package AdminBundle\Form\Sales
 */
class OrderProcessType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('forwardPrices', HiddenType::class, [
                'mapped' => false,
                'attr' => [
                    'id' => 'forwardPrices',
                ]
            ])
            ->add('lines', CollectionType::class, [
                'by_reference' => false,
                'entry_type' => OrderLineType::class,
                'delete_empty' => true,
            ])
        ;
    }
}
