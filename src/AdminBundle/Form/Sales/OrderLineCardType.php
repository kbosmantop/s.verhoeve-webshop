<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderLineCardType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardText', TextareaType::class, [
                'attr' => [
                    'style' => 'width: 100%; min-height: 200px; resize: vertical',
                ],
                'label' => false,
                'mapped' => false,
                'required' => false,
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            if ($event->getForm()->get('cardText')) {

                $data = $event->getData();
                $cardTextField = $event->getForm()->get('cardText');

                if ($metadata = $data->getMetadata()) {
                    if (isset($metadata['is_card'])
                        && isset($metadata['text'])) {
                        $cardTextField->setData($metadata['text']);
                    }
                }
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var OrderOrderLine $orderLine */
            $orderLine = $event->getData();

            $cardTextField = $event->getForm()->get('cardText');

            $metadata = $orderLine->getMetadata();
            $metadata['text'] = $cardTextField->getData();

            $orderLine->setMetadata($metadata);

            $this->entityManager->flush();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'order_line_card';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderLine::class,
        ]);
    }
}