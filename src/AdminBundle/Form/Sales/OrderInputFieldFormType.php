<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Order\Order;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class OrderInputFieldFormType
 * @package AdminBundle\Form\Sales
 */
class OrderInputFieldFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Order $order */
            $orderLine = $event->getData();
            $form = $event->getForm();

            if (isset($orderLine->getMetadata()['input_fields'])) {
                foreach ($orderLine->getMetadata()['input_fields'] as $i => $inputField) {
                    $form->add('inputField_' . $i, TextType::class, [
                        'label' => 'Invoerveld ' . ($i +1),
                        'data' => $inputField,
                        'mapped' => false,
                    ]);
                }
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Order $order */
            $orderLine = $event->getData();
            $form = $event->getForm();

            $metadata = $orderLine->getMetadata();
            if (isset($metadata['input_fields'])) {
                foreach ($metadata['input_fields'] as $i => $inputField) {
                    $fieldValue = $form->get('inputField_' . $i)->getData();
                    $metadata['input_fields'][$i] = $fieldValue;
                }
            }

            $orderLine->setMetadata($metadata);
        });
    }
}