<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomerGroupType
 * @package AdminBundle\Form\App
 */
class CustomerGroupType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $builder->add($this->nameContainer($builder));
    }

    /**
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    private function nameContainer(FormBuilderInterface $builder): FormBuilderInterface
    {
        $container = $builder->create('container_main', ContainerType::class);

        $container->add('description', TextType::class, [
            'label' => 'Naam',
        ]);

        $container->add('teamleaderTag', TextType::class, [
            'label' => 'Teamleader tag',
            'required' => false,
        ]);

        return $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CustomerGroup::class,
        ]);
    }
}
