<?php

namespace AdminBundle\Form\App;

use AdminBundle\Form\Type\MediaType;
use AppBundle\DBAL\Types\BannerTextBlockPositionType;
use AppBundle\Entity\Site\Banner;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BannerType
 * @package AdminBundle\Form\App
 */
class BannerType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_banner', ContainerType::class);

        $container->add('name', TextType::class, [
            'label' => 'Naam',
        ]);

        $container->add('image', MediaType::class, [
            'required' => true,
            'label' => 'Afbeelding',
            'instance' => 'form',
            'enable' => true,
            'media' => [
                'type' => MediaType::TYPE_IMAGE,
                'thumbnail' => true,
                'upload' => true,
                'dimensions' => [
                    'min' => 'banner',
                ],
            ],
        ]);

        $container->add('textBlockPosition', ChoiceType::class, [
            'label' => 'Tekstblok positie',
            'choices' => BannerTextBlockPositionType::getChoices(),
        ]);

        $builder->add($container);

        $container = $builder->create('container_banner_publish', ContainerType::class);
        $builder->add(parent::addPublishableFields($container, $builder, $options));

        $builder->add($container);

        $builder->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'textBlockContent' => [
                    'label' => 'Tekstblok inhoud',
                    'field_type' => CKEditorType::class,
                    'required' => false,
                    'ckeditor' => [
                        'custom_toolbar' => "advanced",
                    ],
                ],
            ])
        );

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            if(!$event->getData()['container_banner']['image']) {
                $event->getForm()->addError(new FormError('Afbeelding is verplicht'));
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Banner::class,
        ]);
    }
}
