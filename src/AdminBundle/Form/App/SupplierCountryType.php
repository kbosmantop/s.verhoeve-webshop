<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\SupplierCountry;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierCountryType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'placeholder' => '',
                'choice_label' => function (Country $country = null) {
                    return $country->translate()->getName();
                },
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplierCountry::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'supplier_country';
    }
}
