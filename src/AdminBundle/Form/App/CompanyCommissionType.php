<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Relation\CompanyCommission;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyCommissionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('productGroup', EntityType::class, [
                'class' => Productgroup::class,
                'placeholder' => "",
                'choice_label' => function (Productgroup $productgroup = null) {
                    if (is_null($productgroup)) {
                        return "";
                    }

                    if ($productgroup->getParent()) {
                        return $productgroup->getParent()->getTitle() . " / " . $productgroup->getTitle();
                    }

                    return $productgroup->getTitle();
                },
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('productgroup')
                        ->select('productgroup')
                        ->addSelect("(COALESCE(productgroup_parent.title, productgroup.title)) AS HIDDEN tree_order")
                        ->leftJoin("productgroup.parent", "productgroup_parent")
                        ->addOrderBy("tree_order");;

                    return $qb;
                },
            ])
            ->add('percentage', PercentType::class, [
                'type' => 'integer',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyCommission::class,
        ]);
    }
}
