<?php

namespace AdminBundle\Form\App;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\DBAL\Types\CompanyTrustStatusType;
use AppBundle\DBAL\Types\PriceViewType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyType
 * @package AdminBundle\Form\App
 */
class CompanyType extends AbstractFormType
{
    /**
     * @var FormBuilderInterface
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CompanyType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $this->builder = $builder;
        $this->options = $options;

        // Add company tabs
        $companyDetailsContainer = $this->companyDetailsTab();
        $builder->add($companyDetailsContainer);

        //Add parameter container
        $parameterContainer = $this->builder->create('company_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen',
        ]);
        $this->builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Company::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @return FormBuilderInterface
     * @throws \Exception
     */
    public function companyDetailsTab()
    {

        $container = $this->builder->create('company_details_container', ContainerType::class, []);

        $column = $this->builder->create('company_details_column_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Algemeen',
            'icon' => 'profile',
        ]);

        $column
            ->add('name', TextType::class, [
                'label' => 'Naam',
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Bovenliggend account',
                'placeholder' => '',
                'class' => Company::class,
                'required' => false,
                'query_builder' => function (EntityRepository $repository) {
                    // and not parent
                    $qb = $repository->createQueryBuilder('company')
                        ->where('company.parent IS NULL')
                        ->orderBy('company.name', 'ASC');

                    return $qb;
                },
                'select2' => [
                    'ajax' => true,
                    'findBy' => ['name'],
                ],
                'choices' => [],
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'Telefoonnummer (algemeen)',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres (algemeen)',
            ])
            ->add('oinNumber', TextType::class, [
                'label' => 'Overheids identificatie nummer',
                'required' => false,
            ])
            ->add('verified', BooleanType::class, [
                'label' => 'Geverifieerd',
                'required' => true,
                'choices' => YesNoType::getChoices(),
                'help_block' => '"Ja" geeft de klant de mogelijkheid om op rekening te bestellen.',
            ])
            ->add('isCustomer', BooleanType::class, [
                'label' => 'Is klant',
                'required' => true,
                'choices' => YesNoType::getChoices(),
                'attr' => [
                    'data-customer-flag' => true,
                ],
            ])
            ->add('isSupplier', BooleanType::class, [
                'label' => 'Is leverancier',
                'required' => true,
                'choices' => YesNoType::getChoices(),
                'attr' => [
                    'data-supplier-flag' => true,
                ],
            ])
            ->add('trustStatus', ChoiceType::class, [
                'label' => 'Trust status',
                'required' => true,
                'choices' => CompanyTrustStatusType::getChoices(),
            ])
            ->add('overrideDefaultPriceView', ChoiceType::class, [
                'label' => 'Overschrijf standaard prijs weergave',
                'required' => false,
                'placeholder' => 'Niet overschrijven',
                'choices' => PriceViewType::getChoices(),
            ]);

        if ($this->builder->getData()->getChildren()->count()) {
            $column->add('handle_child_orders', CheckboxType::class, [
                'label' => 'Toegang tot alle orders van onderliggende leveranciers',
                'required' => false,
                'attr' => [
                    $this->builder->getData()->getHandleChildOrders() ? 'checked' : '',
                ],
            ]);
        }

        if ($this->builder->getData()->getParent()) {
            $column->add('bill_on_parent', CheckboxType::class, [
                'label' => 'Facturatie via hoofdvestiging',
                'required' => false,
                'attr' => [
                    $this->builder->getData()->getBillOnParent() ? 'checked' : '',
                ],
            ]);
        }

        $container->add($column);

        $column = $this->builder->create('company_details_custom_shop', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Eigen shop',
            'icon' => 'brush',
            'hidden' => true,
            'id' => 'company_custom_shop_column',
        ]);

        $container->add($column);

        return $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
