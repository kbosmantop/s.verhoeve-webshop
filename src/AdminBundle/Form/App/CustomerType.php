<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\EntityChoiceType;
use AppBundle\Form\Type\PhpTranslationsLocaleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomerType
 * @package AdminBundle\Form\App
 */
class CustomerType extends AbstractFormType
{
    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * CustomerType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack  $requestStack
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->builder = $builder;
        $this->options = $options;

        $builder->add($this->getCustomerDetailsContainer());
        $builder->add($this->getCustomerAccountContainer());
        $builder->add($this->getAccountOfficeContainer());

        $this->builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            if (null === $event->getData()->getId()) {
                /** @var FormInterface $field */
                $field = $event->getForm()->get('container_product_account_login')->get('container_product_account_login_column');

                $field->add('username', EmailType::class, [
                    'label' => 'Gebruikersnaam',
                    'help_block' => 'Gebruikersnaam kan niet worden gewijzigd!',
                ]);
            }
        });

        $this->builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Customer $customer */
            $customer = $event->getData();

            $request = $this->requestStack->getMasterRequest();
            if (null !== $request && $request->get('entityClass') === 'company') {
                /** @var Company $company */
                $company = $this->entityManager->getRepository(Company::class)->find($request->get('entity'));

                if (null !== $company) {
                    $customer->setCompany($company);
                }
            }
        });
    }


    /**
     * @return FormBuilderInterface
     */
    private function getCustomerDetailsContainer(): FormBuilderInterface
    {

        $container = $this->builder->create('container_product_details', ContainerType::class, []);

        $column1 = $this->builder->create('container_product_details_column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Contactgegevens',
            'icon' => 'headset',
        ]);

        $column1
            ->add('gender', ChoiceType::class, [
                'label' => 'Geslacht',
                'placeholder' => '',
                'choices' => [
                    'Man' => 'male',
                    'Vrouw' => 'female',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
                'disabled' => strpos($_SERVER['REQUEST_URI'], 'bewerken') !== false,
                'help_block' => 'E-mailadres kan niet worden gewijzigd!',
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'Telefoonnummer',
                'required' => false,
            ])
            ->add('mobilenumber', TextType::class, [
                'label' => 'Mobiel nummer',
                'required' => false,
            ]);

        $container->add($column1);

        return $container;

    }

    /**
     * @return FormBuilderInterface
     */
    private function getCustomerAccountContainer(): FormBuilderInterface
    {
        $container = $this->builder->create('container_product_account_login', ContainerType::class, []);

        $column = $this->builder->create('container_product_account_login_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Accountgegevens',
            'icon' => 'key',
        ]);

        $column
            ->add('registeredOnSite', EntityType::class, [
                'label' => 'Geregistreerd op site',
                'placeholder' => "",
                'class' => Site::class,
                'required' => true,
            ])
            ->add('locale', PhpTranslationsLocaleType::class, [
                'label' => 'Voorkeurstaal',
                'placeholder' => 'Kies een voorkeurstaal',
                'required' => true,
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'Accountstatus',
                'choices' => [
                    'Ingeschakeld' => 1,
                    'Uitgeschakeld' => 0,
                ],
            ])
            ->add('username', EmailType::class, [
                'label' => 'Gebruikersnaam',
                'disabled' => (strpos($_SERVER['REQUEST_URI'], "bewerken") !== false),
                'help_block' => 'Gebruikersnaam kan niet worden gewijzigd!',
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ]);

        $container->add($column);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    public function getAccountOfficeContainer(): FormBuilderInterface
    {
        $container = $this->builder->create('container_product_account_office', ContainerType::class, []);

        $column = $this->builder->create('container_product_account_office_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Zakelijk & klantgroep',
            'icon' => 'office',
        ]);

        $column
            ->add('company', EntityChoiceType::class, [
                'label' => 'Bedrijf',
                'field' => 'name',
                'class' => Company::class,
                'placeholder' => '',
                'choice_label' => function ($id, $value) {
                    return $value . ' (' . $id . ')';
                },
                'select2' => [
                    'ajax' => true,
                    'findBy' => [
                        'name',
                    ],
                ],
                'required' => false,
            ])
            ->add('customergroup', EntityType::class, [
                'label' => 'Klantgroep',
                'class' => CustomerGroup::class,
                'placeholder' => '',
                'choice_label' => 'description',
                'required' => true,
            ]);

        $container->add($column);

        return $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
