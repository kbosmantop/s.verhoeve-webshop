<?php

namespace AdminBundle\Form\Product;

use AppBundle\Entity\Catalog\Product\PackagingUnit;
use AppBundle\Entity\Catalog\Product\ProductPackagingUnit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductPackagingFormType
 * @package AdminBundle\Form\Product
 */
class ProductPackagingFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('packagingUnit', EntityType::class, [
                'label' => 'Verpakkingseenheid',
                'class' => PackagingUnit::class,
            ])
            ->add('quantity', IntegerType::class, [
                'label' => 'Hoeveelheid',
            ])
            ->add('dimensionsLength', IntegerType::class, [
                'label' => 'Lengte (mm)',
                'required' => false,
            ])
            ->add('dimensionsWidth', IntegerType::class, [
                'label' => 'Breedte (mm)',
                'required' => false,
            ])
            ->add('dimensionsHeight', IntegerType::class, [
                'label' => 'Hoogte (mm)',
                'required' => false,
            ])
            ->add('weight', IntegerType::class, [
                'label' => 'Gewicht (gram)',
                'required' => false,
            ])
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductPackagingUnit::class,
        ]);
    }

}
