<?php

namespace AdminBundle\Form\Product;

use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PackagingUnitFormType
 * @package AdminBundle\Form\Product
 */
class PackagingUnitFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $builder->create('packaging_unit_container', ContainerType::class, []);
        $column = $builder->create('packagin_unit_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Verpakkingseenheid'
        ]);

        $column->add('code', TextType::class, [
            'label' => 'GDSN code',
            'help_label' => 'Waarde moet 3 tekens bevatten'
        ])->add('name', TextType::class, [
            'label' => 'Naam',
        ]);

        $container->add($column);
        $builder->add($container);
    }
}
