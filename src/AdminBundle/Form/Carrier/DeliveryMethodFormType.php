<?php

namespace AdminBundle\Form\Carrier;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryMethodFormType
 * @package AdminBundle\Form\Carrier
 */
class DeliveryMethodFormType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CustomerType constructor.
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface     $container
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container
    ) {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param $id
     * @return bool
     */
    private function isDeliveryMethodForMultipleCarriers($id): bool
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select($qb->expr()->count('carr.id'))
            ->from(Carrier::class, 'carr')
            ->leftJoin('carr.deliveryMethods', 'dlme')
            ->where('dlme.id = :value')
            ->setParameters(['value' => $id]);

        try {
            return ((int)$qb->getQuery()->getSingleScalarResult() > 1);
        } catch (NoResultException $e) {
            return false;
        } catch (NonUniqueResultException $e) {
            return false;
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $container = $builder->create('delivery_method_container', ContainerType::class, []);

        $formLabel = 'Leveringswijzen ';
        if ($this->isDeliveryMethodForMultipleCarriers($builder->getData()->getId())) {
            $formLabel .= 'Let op, wijzigingen gelden voor alle leveranciers waarbij deze leveringswijze gekoppeld is!';
        }

        $column = $builder->create('delivery_method_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => $formLabel,
            'icon' => 'truck',
        ]);

        $column->add('name', TextType::class, [
            'label' => 'Naam',
        ]);

        $column->add('carriers', EntityType::class, [
            'label' => 'Beschikbaar voor',
            'placeholder' => 'Kies transporteur',
            'class' => Carrier::class,
            'query_builder' => static function (EntityRepository $er) {
                return $er->createQueryBuilder('carr')
                    ->orderBy('carr.shortName', 'ASC');
            },
            'required' => false,
            'multiple' => true,
            'by_reference' => false,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add($column);
        $builder->add($container);

        //Add parameter container
        $parameterContainer = $builder->create('delivery_method_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, DeliveryMethod::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DeliveryMethod::class,
        ]);
    }

}
