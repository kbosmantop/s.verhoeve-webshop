<?php

namespace AdminBundle\Form\DataSet\Settings;

use AdminBundle\Form\CollectionListType;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFieldCollectionType
 * @package AdminBundle\Form\DataSet\Settings
 */
class DataSetFilterFieldCollectionType extends AbstractFormType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'label' => false,
            'required' => false,
            'entry_type' => DataSetFilterFieldEntryType::class,
            'by_reference' => false,
            'collection' => [
                'list_headings' => ['label'],
                'sortable' => true,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return CollectionListType::class;
    }
}
