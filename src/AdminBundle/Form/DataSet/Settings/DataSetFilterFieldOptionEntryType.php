<?php

namespace AdminBundle\Form\DataSet\Settings;

use AdminBundle\Form\CollectionListEntryType;
use AdminBundle\Services\DataSet\DataSetFilterService;
use AppBundle\Entity\Common\DataSet\DataSetFilterFieldOption;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFieldOptionEntryType
 * @package AdminBundle\Form\DataSet\Settings
 */
class DataSetFilterFieldOptionEntryType extends AbstractFormType
{
    /**
     * @var array
     */
    private const OPERATOR_TRANSLATIONS = [
        'less' => 'kleiner dan',
        'greater' => 'grooter dan',
        'equal' => 'gelijk aan',
        'not_equal' => 'niet gelijk aan',
        'is_null' => 'is leeg',
        'is_not_null' => 'is niet leeg',
        'less_or_equal' => 'kleiner dan of gelijk aan',
        'greater_or_equal' => 'groter dan of gelijk aan',
    ];

    /**
     * @var array
     */
    protected static $choices = [];

    /**
     * @var DataSetFilterService
     */
    private $dataSetFilterService;

    /**
     * DataSetFilterFieldOptionEntryType constructor.
     * @param DataSetFilterService $dataSetFilterService
     */
    public function __construct(DataSetFilterService $dataSetFilterService)
    {
        $this->dataSetFilterService = $dataSetFilterService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', TextType::class, [
                'label' => 'Position',
                'attr' => ['class' => 'position'],
            ])
            ->add('label', TextType::class, [
                'label' => 'Label',
            ])
            ->add('operator', ChoiceType::class, [
                'label' => 'Operator',
                'placeholder' => 'Maak een keuze',
                'choices' => $this->dataSetFilterService->getOperatorChoices(),
                'choice_label' => function ($choiceValue, $key, $value) {
                    return self::OPERATOR_TRANSLATIONS[$key];
                },
            ])
            ->add('value', TextType::class, [
                'label' => 'Waarde',
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DataSetFilterFieldOption::class,
        ]);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return CollectionListEntryType::class;
    }
}
