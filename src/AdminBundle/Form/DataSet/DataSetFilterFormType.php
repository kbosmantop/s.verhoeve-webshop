<?php

namespace AdminBundle\Form\DataSet;

use AdminBundle\Form\Fields\PriceRange;
use AppBundle\Entity\Common\DataSet\DataSetFilterField;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Property;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFormType
 * @package AdminBundle\Form\DataSet
 */
class DataSetFilterFormType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DataSetFilterFormType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $this->generateFilterFields($builder->getData(), $builder);
    }

    /**
     * @param Entity               $entity
     * @param FormBuilderInterface $builder
     */
    private function generateFilterFields(Entity $entity, FormBuilderInterface $builder)
    {
        $filterFields = $entity->getDataSetFilterFields();

        foreach ($filterFields as $filterField) {
            $key = 'filter_' . $filterField->getId();
            $formType = $filterField->getFormType();
            $path = $filterField->getPath();

            $filterFieldOptions = [
                'label' => $filterField->getLabel(),
                'required' => false,
                'mapped' => false,
            ];

            $choices = $this->getFilterFieldChoices($filterField);

            if ($formType === ChoiceType::class && null !== $choices) {
                $filterFieldOptions['placeholder'] = $filterField->getPlaceholder();
                $filterFieldOptions['choices'] = $choices;
                $filterFieldOptions['attr']['class'] = 'select2';
                $filterFieldOptions['multiple'] = $filterField->isMultiple();
            }

            if ($formType === EntityType::class) {
                $propertyName = $this->getLastPropertyNameFromPath($path);

                $property = $this->entityManager->getRepository(Property::class)->findOneBy([
                    'name' => $propertyName,
                ]);

                // Ignore if no property found.
                if (null === $property) {
                    continue;
                }

                $filterFieldOptions['multiple'] = $filterField->isMultiple();
                $filterFieldOptions['placeholder'] = $filterField->getPlaceholder();
                $filterFieldOptions['class'] = $property->getType();
                $filterFieldOptions['attr']['class'] = 'select2';
            }

            if ($formType === PriceRange::class) {
                $options = $filterField->getDataSetFilterFieldOptions();

                $min = $max = null;
                if (!$options->isEmpty()) {
                    foreach ($options as $option) {
                        if (strtolower($option->getLabel()) === 'min') {
                            $min = (int)$option->getValue();
                        }
                        if (strtolower($option->getLabel()) === 'max') {
                            $max = (int)$option->getValue();
                        }
                    }
                }

                if ($min !== null && $max !== null) {
                    $filterFieldOptions['attr'] = [
                        'class' => 'range-slider',
                        'data-slider-min' => $min,
                        'data-slider-max' => $max,
                        'data-slider' => 1,
                        'data-slider-value' => '[' . $min . ',' . $max . ']',
                    ];
                }
            }

            $builder->add($key, $formType, $filterFieldOptions);
        }
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'datatable_filters';
    }

    /**
     * @param DataSetFilterField $filterField
     *
     * @return null|array
     */
    private function getFilterFieldChoices(DataSetFilterField $filterField): ?array
    {
        $options = $filterField->getDataSetFilterFieldOptions();

        if (null === $options) {
            return null;
        }

        $choices = [];

        foreach ($options as $option) {
            $label = $option->getLabel();

            if (empty($label)) {
                $label = trim($option->getOperator() . ' ' . $option->getValue());
            }

            $choices[$label] = $option->getId();
        }

        return $choices;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @param string $path
     * @return bool|string
     */
    private function getLastPropertyNameFromPath(string $path)
    {
        $path = str_replace('.id', '', $path);

        return substr($path, strrpos($path, '.') + 1);
    }
}
