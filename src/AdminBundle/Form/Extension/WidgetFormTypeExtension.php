<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WidgetFormTypeExtension
 * @package AdminBundle\Form\Extension
 */
class WidgetFormTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (null === $options['widget_addon_append'] && \in_array('percent', $view->vars['block_prefixes'], true)) {
            $options['widget_addon_append'] = [];
        }

        if (null === $options['widget_addon_prepend'] && \in_array('money', $view->vars['block_prefixes'], true)) {
            $options['widget_addon_prepend'] = [];
        }

        $view->vars['widget_form_control_class'] = $options['widget_form_control_class'];
        $view->vars['widget_form_element_class'] = $options['widget_form_element_class'];
        $view->vars['widget_form_control_feedback'] = $options['widget_form_control_feedback'];
        $view->vars['widget_form_group'] = $options['widget_form_group'];
        $view->vars['widget_addon_prepend'] = $options['widget_addon_prepend'];
        $view->vars['widget_addon_append'] = $options['widget_addon_append'];
        $view->vars['widget_btn_prepend'] = $options['widget_btn_prepend'];
        $view->vars['widget_btn_append'] = $options['widget_btn_append'];
        $view->vars['widget_prefix'] = $options['widget_prefix'];
        $view->vars['widget_suffix'] = $options['widget_suffix'];
        $view->vars['widget_type'] = $options['widget_type'];
        $view->vars['widget_items_attr'] = $options['widget_items_attr'];
        $view->vars['widget_form_group_attr'] = $options['widget_form_group_attr'];
        $view->vars['widget_checkbox_label'] = $options['widget_checkbox_label'];

        $view->vars['widget_horizontal_label_class'] = $options['widget_horizontal_label_class'];
        $view->vars['widget_horizontal_label_offset_class'] = $options['widget_horizontal_label_offset_class'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'widget_form_control_class' => 'form-control',
            'widget_form_control_feedback' => null,
            'widget_form_group' => true,
            'widget_form_element_class' => 'col-sm-8 col-md-8 col-lg-9',
            'widget_addon_prepend' => null,
            'widget_addon_append' => null,
            'widget_btn_prepend' => null,
            'widget_btn_append' => null,
            'widget_prefix' => null,
            'widget_suffix' => null,
            'widget_type' => '',
            'widget_items_attr' => [],
            'widget_form_group_attr' => [
                'class' => 'form-group',
            ],
            'widget_checkbox_label' => 'widget',
            'widget_horizontal_label_class' => 'col-sm-4 col-md-4 col-lg-3',
            'widget_horizontal_label_offset_class' => null,
        ]);

        $resolver->setAllowedValues('widget_type', ['inline', 'inline-btn', '']);
        $resolver->setAllowedValues('widget_checkbox_label', ['label', 'widget', 'both']);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return FormType::class;
    }
}
