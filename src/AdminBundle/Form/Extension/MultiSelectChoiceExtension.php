<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MultiSelectChoiceExtension extends AbstractMultiSelectExtension
{

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ChoiceType::class;
    }
}