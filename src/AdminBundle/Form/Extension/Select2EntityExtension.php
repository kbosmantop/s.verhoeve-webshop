<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\EntityType;

class Select2EntityExtension extends AbstractSelect2Extension
{

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return EntityType::class;
    }
}