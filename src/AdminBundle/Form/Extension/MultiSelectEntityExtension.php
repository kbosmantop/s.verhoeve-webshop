<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\EntityType;

class MultiSelectEntityExtension extends AbstractMultiSelectExtension
{
    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return EntityType::class;
    }
}