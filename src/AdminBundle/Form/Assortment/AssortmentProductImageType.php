<?php

namespace AdminBundle\Form\Assortment;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Assortment\AssortmentProductImage;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AssortmentProductImageType
 * @package AdminBundle\Form\Assortment
 */
class AssortmentProductImageType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * AssortmentProductImageType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('position', IntegerType::class, [
                'attr' => [
                    'class' => 'text-right',
                ],
            ])
            ->add('path', MediaType::class, [
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => false,
                    'dimensions' => [
                        'min' => 'product',
                    ],
                ],
            ])
            ->add('main', CheckboxType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'image-application-main',
                ],
            ])
            ->add('category', CheckboxType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'image-application-category',
                ],
            ])
            ->add('description', TextType::class, [
                    'required' => false,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AssortmentProductImage::class,
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'product_image';
    }
}
