<?php

namespace AdminBundle\Form\Address;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Geography\Country;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyEstablishmentAddressType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, [
                'required' => false,
                'label' => 'Beschrijving',
            ])
            ->add('attn', TextType::class, [
                'required' => false,
                'label' => 'Contactpersoon',
            ])
            ->add('street', TextType::class, [
                'label' => 'Straat',
            ])
            ->add('number', TextType::class, [
                'label' => 'Huisnummer',
            ])
            ->add('numberAddition', TextType::class, [
                'label' => 'Huisnummer toevoeging',
                'required' => false,
            ])
            ->add('postcode', TextType::class, [])
            ->add('city', TextType::class, [
                'label' => 'Stad',
            ])
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'placeholder' => 'Kies een land...',
                'class' => Country::class,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
