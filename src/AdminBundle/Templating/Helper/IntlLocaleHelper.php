<?php

namespace AdminBundle\Templating\Helper;

use Symfony\Component\Intl\Intl;

/**
 * LocaleHelper displays culture information.
 *
 */
class IntlLocaleHelper extends AbstractIntlHelper
{
    /**
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function country($code, $locale = null)
    {
        $name = Intl::getRegionBundle()->getCountryName($code, $locale ?: $this->localeDetector->getLocale());

        return $name;
    }

    /**
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function language($code, $locale = null)
    {
        $codes = explode('_', $code);

        $name = Intl::getLanguageBundle()->getLanguageName($codes[0], isset($codes[1]) ? $codes[1] : null,
            $locale ?: $this->localeDetector->getLocale());

        return $name;
    }

    /**
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function locale($code, $locale = null)
    {
        $name = Intl::getLocaleBundle()->getLocaleName($code, $locale ?: $this->localeDetector->getLocale());

        return $name;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_intl_locale';
    }
}
