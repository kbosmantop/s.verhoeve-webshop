<?php


namespace AdminBundle\Templating\Helper;

use AdminBundle\Interfaces\LocaleDetectorInterface;
use Symfony\Component\Templating\Helper\Helper;

abstract class AbstractIntlHelper extends Helper
{

    /**
     * @var LocaleDetectorInterface
     */
    protected $localeDetector;

    /**
     * Constructor.
     *
     * @param string                  $charset The output charset of the helper
     * @param LocaleDetectorInterface $localeDetector
     */
    public function __construct($charset, LocaleDetectorInterface $localeDetector)
    {
        $this->setCharset($charset);

        $this->localeDetector = $localeDetector;
    }
}
