<?php

namespace AdminBundle\Interfaces;

interface ImageInterface
{
    public function getPath();

    /**
     * @param $path
     *
     * @return mixed
     */
    public function setPath($path);

    public function getPathColumn();

    public function getId();

    public function generateName();

    public function getStoragePath();
}