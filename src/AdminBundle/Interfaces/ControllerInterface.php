<?php

namespace AdminBundle\Interfaces;

/**
 * Interface ControllerInterface
 * @package AdminBundle\Interfaces
 */
interface ControllerInterface
{
    public function getLabel();

    /**
     * @param string $action
     * @param array  $params
     * @return mixed
     */
    public function getUrl(string $action, $params = []);
}
