<?php

namespace AdminBundle\Interfaces;

use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface DatatableInterface
{
    /**
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null);

    public function buildDatatable(DatatableBuilder $datatableBuilder, array $options);
}