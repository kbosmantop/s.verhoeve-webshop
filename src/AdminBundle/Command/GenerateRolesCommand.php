<?php

namespace AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateRolesCommand
 * @package AdminBundle\Command
 */
class GenerateRolesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:roles:generate');
        $this->setDescription('Generate roles based on controllers');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('cms.role_manager')->generateRoles();

        $output->writeln('Roles generated successfully');
    }
}
