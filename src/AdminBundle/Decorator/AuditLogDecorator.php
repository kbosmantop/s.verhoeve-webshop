<?php

namespace AdminBundle\Decorator;

use AppBundle\Entity\Security\Employee\User;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AuditLogDecorator
{

    use ContainerAwareTrait;

    /**
     * @var AuditLog
     */
    private $log;

    /**
     * @param AuditLog $log
     */
    public function set(AuditLog $log)
    {
        $this->log = $log;
    }

    /**
     * @return AuditLog
     */
    public function get()
    {
        return $this->log;
    }

    /**
     * @return array
     */
    public function getAssociatedLogs()
    {
        $em = $this->container->get('doctrine')->getManager();
        $classMetaData = $em->getClassMetadata($this->log->getSource()->getClass());

        $associatedFields = [];
        //fill the array with all associated classes
        foreach ($classMetaData->associationMappings as $associatedField) {
            $associatedFields[] = $associatedField['targetEntity'];
        }

        $logTime = $this->log->getLoggedAt();

        /**
         * @var QueryBuilder $qb
         */
        $qb = $em->getRepository(AuditLog::class)->createQueryBuilder('log')
            ->leftJoin('log.source', 's')
            ->leftJoin('log.blame', 'b')
            ->andWhere('s.class IN (:classes)')
            ->andWhere('log.loggedAt = :loggedAt')
            ->orderBy('log.loggedAt', 'DESC')
            ->orderBy('log.id', 'DESC')
            ->setParameter('classes', array_values($associatedFields))
            ->setParameter('loggedAt', $logTime);

        if ($this->log->getBlame()) {
            $qb->andWhere('b.fk = :blame')
                ->setParameter('blame', $this->log->getBlame()->getFk());
        }

        $associatedLogs = $qb->getQuery()->getResult();

        $decoratedAssociatedLogs = [];
        $auditLogFactory = $this->container->get('app.audit_log.factory');
        foreach ($associatedLogs as $log) {
            $decoratedAssociatedLogs[] = $auditLogFactory->get($log);
        }

        return $decoratedAssociatedLogs;
    }

    /**
     * @return null|User
     */
    public function getBlame()
    {
        if (!$this->log->getBlame()) {
            return null;
        }

        /**
         * @var User $user
         */
        $user = $this->container->get('doctrine')->getRepository($this->log->getBlame()->getClass())->find($this->log->getBlame()->getFk());

        return $user;
    }

}