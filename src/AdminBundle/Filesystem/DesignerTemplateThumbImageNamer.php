<?php

namespace AdminBundle\Filesystem;

use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;

/**
 * Class DesignerTemplateThumbImageNamer
 * @package AdminBundle\Filesystem
 */
class DesignerTemplateThumbImageNamer implements NamerInterface
{
    /**
     * @param FileInterface $file
     * @return string
     */
    public function name(FileInterface $file)
    {
        return sprintf('tmp/images/designer/%s.%s', uniqid(), $file->getExtension());
    }
}
