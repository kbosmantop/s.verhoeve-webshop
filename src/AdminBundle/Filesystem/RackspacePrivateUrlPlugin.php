<?php

namespace AdminBundle\Filesystem;

use League\Flysystem\Rackspace\RackspaceAdapter;

class RackspacePrivateUrlPlugin extends AbstractRackspaceUrlPlugin
{
    /**
     *
     * @param string|null $path
     *
     * @return  string The full url to the file
     */
    public function handle($path = null)
    {
        if ($this->adapter instanceof RackspaceAdapter) {
            $fullPath = $this->getUrl() . '/private/';

            if ($path) {
                $fullPath .= ltrim($path, '/');
            }

            return rtrim($fullPath, '/');
        }

        return false;
    }

    /**
     * Gets the method name.
     *
     * @return string
     */
    public function getMethod()
    {
        return 'getPrivateUrl';
    }
}