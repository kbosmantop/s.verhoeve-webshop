<?php

namespace AdminBundle\Filesystem;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\PluginInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class AbstractRackspaceUrlPlugin implements PluginInterface
{
    use ContainerAwareTrait;

    /**
     * @var FilesystemInterface
     */
    protected $filesystem;

    /**
     * @var Filesystem adapter
     */
    protected $adapter;

    /**
     * @var string $objectStoreUrl
     */
    private $objectStoreUrl;

    /**
     * @var string $environment
     */
    private $environment;

    /**
     * RackspaceUrlPlugin constructor.
     *
     * @param string $objectStoreUrl
     * @param string $environment
     */
    public function __construct($objectStoreUrl, $environment)
    {
        $this->objectStoreUrl = $objectStoreUrl;
        $this->environment = $environment;
    }

    /**
     * Set the Filesystem object.
     *
     * Note: We require the Filesystem, not the FilesystemInterface because
     *       the getAdapter method is not a part of the interface. :sadpanda:
     *
     * @param FilesystemInterface $filesystem
     */
    public function setFilesystem(FilesystemInterface $filesystem)
    {
        $adapter = $filesystem->getAdapter();

        if (false === $this->isAdapterSupported($adapter)) {
            throw new InvalidAdapterException(sprintf(
                "RackspaceUrlPlugin does not support adapter of type: %s",
                get_class($adapter)
            ));
        }

        $this->adapter = $adapter;
        $this->filesystem = $filesystem;
    }

    /**
     * Gets the method name.
     *
     * @return string
     */
    abstract public function getMethod();

    /**
     * Gets the url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->objectStoreUrl;
    }

    /**
     * Tells if the adapter is supported.
     *
     * Currently supported adapters:
     * - League\Flysystem\Adapter\RackpaceAdapater
     *
     * @param object Filesystem adapter
     *
     * @return  boolean True if the adapter is supported
     */
    protected function isAdapterSupported($adapter)
    {
        $supportedAdapters = [
            'RackspaceAdapter',
        ];

        $isAdapterSupported = false;
        foreach ($supportedAdapters as $supportedAdapter) {
            if (!$adapter instanceof $supportedAdapter) {
                $isAdapterSupported = true;
                break;
            }
        }

        return $isAdapterSupported;
    }
}
