<?php

namespace AdminBundle\Filesystem;

use League\Flysystem\Rackspace\RackspaceAdapter;

class RackspaceTempUrlPlugin extends AbstractRackspaceUrlPlugin
{
    /**
     * @param string|null $path
     * @param int         $expiresIn
     *
     * @return string;
     * @throws \Exception
     */
    public function handle($path = null, $expiresIn = 30)
    {
        if (!($this->adapter instanceof RackspaceAdapter)) {
            throw new \Exception();
        }

        $expires = time() + $expiresIn;

        $privateUrl = $this->filesystem->get($path)->getPrivateUrl();

        return $privateUrl . "?" . http_build_query([
                "temp_url_sig" => $this->calculateSignature($path, $expires),
                "temp_url_expires" => $expires,
            ]);
    }

    /**
     * Gets the method name.
     *
     * @return string
     */
    public function getMethod()
    {
        return 'getTempUrl';
    }

    /**
     * @param        $path
     * @param        $expires
     * @param string $method
     *
     * @return string
     */
    private function calculateSignature($path, $expires, $method = "GET")
    {
        return hash_hmac('sha1', implode("\n", [
            $method,
            $expires,
            "/private/" . $path,
        ]), $this->container->getParameter("openstack_temp_url_key"));
    }
}
