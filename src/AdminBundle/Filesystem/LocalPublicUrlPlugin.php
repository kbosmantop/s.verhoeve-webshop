<?php

namespace AdminBundle\Filesystem;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\PluginInterface;
use League\Flysystem\Rackspace\RackspaceAdapter;

class LocalPublicUrlPlugin implements PluginInterface
{

    /**
     * @var Filesystem adapter
     */
    protected $adapter;

    /**
     *
     * @param string|null $path
     *
     * @return  string The full url to the file
     */
    public function handle($path = null)
    {
        if ($this->adapter instanceof RackspaceAdapter) {
            $fullPath = $this->getUrl() . '/files/';

            if ($path) {
                $fullPath .= ltrim($path, '/');
            }

            return rtrim($fullPath, '/');
        }

        return false;
    }

    /**
     * Set the Filesystem object.
     *
     * Note: We require the Filesystem, not the FilesystemInterface because
     *       the getAdapter method is not a part of the interface. :sadpanda:
     *
     * @param FilesystemInterface $filesystem
     */
    public function setFilesystem(FilesystemInterface $filesystem)
    {
        $adapter = $filesystem->getAdapter();

        if (false === $this->isAdapterSupported($adapter)) {
            throw new InvalidAdapterException(sprintf(
                "RackspaceUrlPlugin does not support adapter of type: %s",
                get_class($adapter)
            ));
        }

        $this->adapter = $adapter;
        $this->filesystem = $filesystem;
    }

    /**
     * Gets the method name.
     *
     * @return string
     */
    public function getMethod()
    {
        return 'getPublicUrl';
    }
}