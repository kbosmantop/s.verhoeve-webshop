<?php

namespace AdminBundle\Services\Router;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class BaseRouteService
 * @package AdminBundle\Services\Router
 */
class BaseRouteService
{
    /** @var RequestStack */
    private $requestStack;

    /**
     * BaseRouteService constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $action
     * @return string
     */
    public function getBaseRouteForAction(string $action)
    {
        if (null !== ($masterRequest = $this->requestStack->getMasterRequest())) {

            $parts = explode('_', $masterRequest->get('_route'));

            array_pop($parts);

            return implode('_', $parts) . '_' . $action;
        }

        return '';
    }
}
