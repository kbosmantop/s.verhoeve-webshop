<?php

namespace AdminBundle\Services\Role;

use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\RoleColumn;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoleColumnService
 * @package AdminBundle\Services\Role
 */
class RoleColumnService
{
    use ContainerAwareTrait;

    /**
     * @param RoleCategory $category
     * @param Request      $request
     * @return RoleColumn|bool
     */
    public function addNewColumn(RoleCategory $category, Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $rolePrefix = $category->guessRolePrefix();

        /**
         * @var RoleColumn $column
         */
        $column = $doctrine->getRepository(RoleColumn::class)->findOneBy([
            'category' => $category,
            'name' => strtolower($request->get('column')),
        ]);


        if (!$column) {
            $column = new RoleColumn();
            $column->setName($request->get('column'));
            $column->setCategory($category);

            $doctrine->getManager()->persist($column);
        }

        $columnRolePrefix = $rolePrefix . '_COLUMN_' . strtoupper($column->getName());
        $roles = json_decode($request->get('roles'), true);

        if (\is_array($roles)) {
            foreach ($roles as $roleName => $apply) {
                if ($apply) {
                    $roleName = strtoupper($roleName);
                    $role = $doctrine->getRepository(Role::class)->findOneBy([
                        'category' => $category,
                        'name' => $columnRolePrefix . '_' . $roleName,
                    ]);

                    if (!$role) {
                        $role = new Role();
                        $role->setCategory($category);
                        $role->setName($columnRolePrefix . '_' . $roleName);
                    }

                    $role->setColumn($column);
                    $column->addRole($role);
                }
            }

            $doctrine->getManager()->flush();

            return $column;
        }

        return false;
    }

    /**
     * @param RoleColumn $column
     */
    public function removeColumn(RoleColumn $column)
    {
        $entiyManager = $this->container->get('doctrine')->getManager();

        $entiyManager->remove($column);
        $entiyManager->flush();
    }

}
