<?php

namespace AdminBundle\Services\Role;

use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class RoleManagerService
 * @package AdminBundle\Services\Role
 */
class RoleManagerService
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var string
     */
    private $baseRoleName;

    /**
     * @var string
     */
    private $roleAction;

    /**
     * @var mixed
     */
    private $object;

    protected static $readableRoles = [
        'READ' => 'Lezen',
        'CREATE' => 'Schrijven',
        'UPDATE' => 'Bewerken',
        'DELETE' => 'Verwijderen',
    ];

    /**
     * RoleManagerService constructor.
     * @param Registry     $doctrine
     * @param TokenStorage $tokenStorage
     * @param string       $rootDir
     */
    public function __construct(
        Registry $doctrine,
        TokenStorage $tokenStorage,
        string $rootDir
    ) {
        $this->doctrine = $doctrine;
        $this->tokenStorage = $tokenStorage;
        $this->rootDir = $rootDir;
    }

    /**
     * @param        $model
     * @param string $roleAction
     * @return array
     * @throws \ReflectionException
     */
    public function getHiddenFields($model, $roleAction = 'WRITE')
    {
        $fields = [];

        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            return $fields;
        }

        $class = (new \ReflectionClass($model))->getShortName();

        $this->baseRoleName = 'ROLE_' . strtoupper($this->camelCaseToUnderscore($class) . '_COLUMN_');
        $this->roleAction = $roleAction;

        /**
         * @var QueryBuilder $queryBuilder
         */
        $queryBuilder = $this->getRepository()->createQueryBuilder('r');
        $queryBuilder->where('r.name LIKE :roleName');
        $queryBuilder->setParameter('roleName', $this->baseRoleName . '%');

        $results = $queryBuilder->getQuery()->getResult();

        /** @var Role $role */
        foreach ($results as $role) {
            if (strpos($role->getName(), $this->roleAction) !== false && !$this->getUser()->getRoles(true,
                    true)->contains($role)) {
                $fields[] = $this->convertRoleToFieldName($role->getName());
            }
        }

        return $fields;
    }

    /**
     * @param null $attributes
     * @param null $object
     * @return bool
     * @throws \ReflectionException
     */
    public function denyAccessUnlessGranted($attributes = null, $object = null)
    {
        if (null === $attributes) {
            if ($this->object) {
                $object = $this->object;
            }

            $attributes = $this->getRolesFor($object);
        }

        foreach ($attributes as $attribute) {
            if ($this->getUser()->getRoles(true, true)->contains($attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param null $attributes
     * @param null $object
     * @return bool
     * @throws \ReflectionException
     */
    public function userIsGrantedTo($attributes = null, $object = null)
    {
        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        if ($attributes === null) {
            if ($this->object) {
                $object = $this->object;
            }

            $attributes = $this->getRolesFor($object);
        }

        foreach ($attributes as $attribute) {
            if ($this->getUser()->getRoles(true, true)->contains($attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param      $object
     * @param null $action
     * @return array
     * @throws \ReflectionException
     */
    public function getRolesFor($object, $action = null)
    {
        $normalizer = new CamelCaseToSnakeCaseNameConverter();

        $replacements = [
            'controller',
            'service',
        ];

        $className = strtoupper(ltrim($normalizer->normalize(str_replace($replacements, null,
            strtolower((new \ReflectionClass($object))->getShortName()))), '_'));

        $roleName = 'ROLE_' . $className . ($action ? '_' . strtoupper($action) : null);
        return $this->getRepository()->findBy(['name' => ['ROLE_ADMIN', $roleName]]);
    }

    /**
     * @param $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @param string $name
     * @return Role|null
     */
    public function findRoleByName(string $name)
    {
        $role = $this->doctrine->getRepository(Role::class)->findOneBy([
            'name' => $name,
        ]);

        return $role;
    }

    /**
     * @param $role
     * @return string
     */
    private function convertRoleToFieldName($role)
    {
        $field = strtolower(substr($role, \strlen($this->baseRoleName)));
        $field = str_replace(strtolower('_' . $this->roleAction), null, $field);
        return lcfirst(implode('', array_map('ucfirst', explode('_', $field))));
    }

    /**
     * @param $input
     * @return string
     */
    private function camelCaseToUnderscore($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    /**
     * @return EntityRepository
     */
    private function getRepository()
    {
        return $this->doctrine->getRepository(Role::class);
    }

    /**
     * @return User
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if (null === $token) {
            throw new \RuntimeException('No token received');
        }

        return $token->getUser();
    }

    /**
     * @throws \ReflectionException
     * @throws \TypeError
     */
    public function generateRoles()
    {
        $em = $this->doctrine->getManager();
        $directory = $this->rootDir . '/../src/*/Controller';

        $finder = new Finder();
        $finder
            ->in($directory)
            ->notName('*Interface.php')
            ->name('*.php');

        foreach ($finder as $file) {
            $realPath = $file->getRealPath();

            $srcPath = explode('src/', $realPath);

            $class = str_replace('/', '\\', end($srcPath));

            if (!$class) {
                continue;
            }

            $classPath = strstr($class, '.', true);

            if (!class_exists($classPath)) {
                continue;
            }

            if (!method_exists($classPath, 'getLabel')) {
                continue;
            }

            if (stripos($classPath, 'CrudController') !== false) {
                continue;
            }

            if (!\in_array(Controller::class, class_parents($classPath), true)) {
                continue;
            }

            $permissionArray = ['READ'];

            if (\in_array(CrudControllerInterface::class, class_implements($classPath), true)) {
                foreach (['CREATE', 'UPDATE', 'DELETE'] as $value) {
                    $permissionArray[] = $value;
                }
            }

            if (method_exists($classPath, 'getControllerRoles')) {
                $controllerSpecificPermissions = \call_user_func([$classPath, 'getControllerRoles']);

                if (\is_array($controllerSpecificPermissions)) {
                    foreach ($controllerSpecificPermissions as $key => $value) {
                        $permissionArray[$key] = $value;
                    }
                }

            }

            array_unique($permissionArray);

            $className = \call_user_func([$classPath, 'getLabel']);
            $classNameUpper = strtoupper($className);

            if (!$classNameUpper) {
                continue;
            }

            /** @var RoleCategory $roleCategory */
            $roleCategory = $em->getRepository(RoleCategory::class)->findOneOrCreate([
                'name' => 'CATEGORY_' . $classNameUpper,
            ]);

            $roleCategory->setDescription(ucfirst($className));

            foreach ($permissionArray as $permission) {
                //create category when permission is null
                if (null === $permission) {
                    continue;
                }

                $roleName = $this->getRole($classPath, $permission);

                /** @var Role $role */
                $role = $em->getRepository(Role::class)->findOneOrCreate([
                    'name' => $roleName,
                ]);

                $role->setDescription(self::$readableRoles[$permission]);
                $roleCategory->addRole($role);
            }

            $em->persist($roleCategory);
        }

        //create admin role
        /** @var RoleCategory $adminCategory */
        $adminCategory = $em->getRepository(RoleCategory::class)->findOneOrCreate([
            'name' => 'CATEGORY_ADMIN',
            'description' => 'Admin'
        ]);

        /** @var Role $role */
        $role = $em->getRepository(Role::class)->findOneOrCreate([
            'name' => 'ROLE_ADMIN',
        ]);

        $role->setDescription('Toegang tot Admin');
        $adminCategory->addRole($role);

        $em->persist($adminCategory);
        $em->flush();
    }

    /**
     * @param $class
     * @param $permission
     * @return string
     * @throws \ReflectionException
     */
    private function getRole($class, $permission)
    {
        $normalizer = new CamelCaseToSnakeCaseNameConverter();

        $replacements = [
            'controller',
            'service',
        ];

        $className = strtoupper(ltrim($normalizer->normalize(str_replace($replacements, null,
            strtolower((new \ReflectionClass($class))->getShortName()))), '_'));

        $role = 'ROLE_' . $className . ($permission ? '_' . strtoupper($permission) : null);

        return $role;
    }

}
