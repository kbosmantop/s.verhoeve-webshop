<?php

namespace AdminBundle\Services\Product;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductCombinationService
 * @package AppBundle\Entity\Product
 */
class ProductCombinationService
{
    use ContainerAwareTrait;

    /**
     * @param Request $request
     * @param Product $parent
     *
     * @return bool
     */
    public function addVariation(Request $request, Product $parent): bool
    {
        $em = $this->container->get('doctrine')->getManager();

        $product = new GenericProduct();
        $product->setParent($parent);
        $product->translate('nl')->setTitle($request->get('title'));
        $product->setPrice($request->get('price'));

        $product->mergeNewTranslations();

        $products = json_decode($request->get('products'), true);

        $main = true;
        foreach ($products as $productId) {
            /** @var Product $pr */
            $pr = $this->container->get('doctrine')->getRepository(Product::class)->find($productId);
            $combination = $this->createCombination($product, $pr, ['main' => $main]);
            $main = false;

            $product->addCombination($combination);
        }

        $em->persist($product);
        $em->flush();

        return true;
    }

    /**
     * @param Product $combinationProduct
     * @param Product $product
     * @param array   $options
     * @return Combination
     */
    public function createCombination(Product $combinationProduct, Product $product, array $options = []): Combination
    {
        $options = $this->resolveCombinationOptions($options);

        $combination = new Combination();
        $combination->setCombinationProduct($combinationProduct);
        $combination->setProduct($product);

        $combination->setQuantity($options['quantity']);
        $combination->setMain($options['main']);
        $combination->setStock($options['stock']);

        return $combination;
    }

    /**
     * @param $options
     * @return array
     */
    private function resolveCombinationOptions($options): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefined([
            'quantity',
            'main',
            'stock',
        ]);

        $resolver->setDefaults([
            'quantity' => 1,
            'main' => false,
            'stock' => false,
        ]);

        return $resolver->resolve($options);
    }
}
