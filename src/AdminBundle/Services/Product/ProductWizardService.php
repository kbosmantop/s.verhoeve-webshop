<?php

namespace AdminBundle\Services\Product;

use AppBundle\DBAL\Types\ProductTypeType;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Manager\Catalog\Product\ProductCombinationManager;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use function array_merge;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductWizardService
 * @package AdminBundle\Services\Product
 */
class ProductWizardService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProductCombinationManager */
    private $productCombinationManager;

    /**
     * ProductWizardService constructor.
     * @param EntityManagerInterface             $entityManager
     * @param ProductCombinationManager $productCombinationManager
     */
    public function __construct(EntityManagerInterface $entityManager, ProductCombinationManager $productCombinationManager)
    {
        $this->entityManager = $entityManager;
        $this->productCombinationManager = $productCombinationManager;
    }

    /**
     * @param Request $request
     * @return Product
     */
    public function processWizard(Request $request): Product
    {
        $productType = $request->get('producttype');
        $title = $request->get('title');

        $product = new GenericProduct();
        $product->translate('nl_NL')->setTitle($title);
        $product->setName($title);
        $product->mergeNewTranslations();

        /**
         * @var Productgroup $productGroup
         */
        $productGroup = $this->entityManager->getRepository(Productgroup::class)->find($request->get('productgroup'));

        $product->setProductgroup($productGroup);

        //create standard variation for product
        $variation = new GenericProduct();
        $variation->translate('nl_NL')->setTitle($product->translate('nl_NL')->getTitle());
        $variation->setName($product->getName());
        $variation->mergeNewTranslations();
        $variation->setPrice(0.00);

        if ($productType !== 'combination') {
            if ($request->get('product_set')) {
                /** @var ProductgroupPropertySet $productSet */
                $propertySet = $this->entityManager->getRepository(ProductgroupPropertySet::class)->find($request->get('product_set'));

                if (null !== $propertySet) {
                    $product->setPropertySet($propertySet);
                    $variation->setPropertySet($propertySet);

                    foreach ($propertySet->getProductgroupProperties() as $property) {
                        $productProperty = new ProductProperty();
                        $productProperty->setProduct($product);
                        $productProperty->setProductgroupProperty($property);

                        $variation->addProductProperty($productProperty);
                    }
                }
            }

            $product->addVariation($variation);
        } else {
            $productIds = [];
            $quantities = [];

            $productJson = json_decode($request->get('products'));
            if ($productJson) {
                foreach ($productJson as $p) {
                    $productIds[] = $p->id;
                    $quantities[$p->id] = $p->quantity;
                }
            }

            /** @var Product[] $products */
            $products = $this->entityManager->getRepository(Product::class)->findBy(['id' => $productIds]);

            $price = 0;
            $vat = null;

            $i = 0;

            $vatGroups = new ArrayCollection();
            foreach ($products as $p) {
                $quantity = \in_array($p->getId(), $quantities, true) ? $quantities[$p->getId()] : 1;
                $price += ($p->getPrice() * $quantity);
                $vatGroups = $p->getVatGroups();

                $combination = new Combination();
                $combination->setCombinationProduct($product);
                $combination->setProduct($p);
                $combination->setMain(!$i);
                $combination->setStock(false);
                $combination->setQuantity($quantity);

                $product->addCombination($combination);

                $i++;
            }

            $product->setVatGroups($vatGroups);
            $product->setPrice($price);
            $product->translate('nl_NL')->setSlug(Urlizer::urlize($title));
            $product->setType(ProductTypeType::PRODUCT_TYPE_COMBINATION);
        }

        $this->entityManager->persist($product);

        return $product;
    }

    /**
     * @param $productgroupId
     * @return array
     */
    public function getPropertySetsByProductgroup($productgroupId)
    {
        /** @var Productgroup $productGroup */
        $productGroup = $this->entityManager->getRepository(Productgroup::class)->find($productgroupId);

        $sets = [];
        foreach($productGroup->getProductgroupPropertySets() as $set) {
            $sets[] = $set;
        }

        if($productGroup->getParent()) {
            $sets = array_merge($sets, $this->getPropertySetsByProductgroup($productGroup->getParent()->getId()));
        }

        return $sets;
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getProducts(Request $request): array
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->entityManager->getRepository(GenericProduct::class)
            ->createQueryBuilder('p');

        return $this->getResults($qb, $request);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getPersonalizations(Request $request): array
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->entityManager->getRepository(Personalization::class)
            ->createQueryBuilder('p');

        return $this->getResults($qb, $request);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param Request      $request
     * @return array
     * @throws \Exception
     */
    private function getResults(QueryBuilder $queryBuilder, Request $request)
    {
        $results = $queryBuilder
            ->distinct()
            ->select('p')
            ->leftJoin(ProductTranslation::class, 'pt', 'WITH', 'p.id = pt.translatable AND pt.locale = :locale')
            ->andWhere('p.parent IS NULL')
            ->andWhere('p.type != :type OR p.type IS NULL')
            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('pt.title', ':search'),
                $queryBuilder->expr()->like('pt.shortDescription', ':search'),
                $queryBuilder->expr()->like('pt.description', ':search')
            ))
            ->setParameters([
                'type' => 'combination',
                'locale' => 'nl_NL',
                'search' => '%' . $request->get('query') . '%',
            ])
            ->getQuery()->getResult();

        return $this->productCombinationManager->productSelectionJson($results);
    }

    /**
     * @param Product $product
     *
     * @return array
     * @throws \Exception
     */
    public function getVariations(Product $product): array
    {
        $variations = [];

        foreach ($product->getVariations() as $variation) {
            $variations[] = [
                'id' => $variation->getId(),
                'title' => $variation->getName(),
                 'price' => $variation->getPrice(),
            ];
        }

        return $variations;
    }
}
