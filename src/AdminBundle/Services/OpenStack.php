<?php

namespace AdminBundle\Services;

use OpenCloud;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class OpenStack
{
    use ContainerAwareTrait;

    /**
     * @var OpenCloud\OpenStack
     */
    private $openStack;

    private function getOpenStack()
    {
        if (!$this->openStack) {
            $this->openStack = new OpenCloud\OpenStack($this->container->getParameter('openstack_url'),
                [
                    'username' => $this->container->getParameter('openstack_username'),
                    'password' => $this->container->getParameter('openstack_password'),
                    'tenantName' => $this->container->getParameter('openstack_project'),
                ]
            );
        }

        return $this->openStack;
    }

    /**
     * @return OpenCloud\Compute\Service
     * @link http://developer.openstack.org/api-ref-compute-v2.1.html
     */
    public function getComputeService()
    {
        return $this->getOpenStack()->computeService("nova", "NL");
    }

    /**
     * @return OpenCloud\Networking\Service
     * @link http://developer.openstack.org/api-ref/networking/v2/index.html
     */
    public function getNetworkService()
    {
        return $this->getOpenStack()->networkingService("quantum", "NL");
    }
}
