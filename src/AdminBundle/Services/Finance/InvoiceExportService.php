<?php

namespace AdminBundle\Services\Finance;

use AppBundle\DBAL\Types\CompanyInvoiceFrequencyType;
use AppBundle\DBAL\Types\ExactInvoiceExportStatusType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use Recurr\Exception as RecurrException;
use Recurr\Recurrence;
use Recurr\RecurrenceCollection;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\Constraint\BeforeConstraint;

/**
 * Class InvoiceExportService
 */
class InvoiceExportService
{
    public const RRULE_DAILY = 'FREQ=DAILY;INTERVAL=1';
    public const RRULE_WEEKLY = 'FREQ=WEEKLY;BYDAY=SU;INTERVAL=1';
    public const RRULE_BIWEEKLY = 'FREQ=WEEKLY;BYDAY=SU;INTERVAL=2';
    public const RRULE_MONTHLY = 'FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=-1';
    public const RRULE_QUARTERLY = 'FREQ=MONTHLY;INTERVAL=1;BYMONTH=3,6,9,12;BYMONTHDAY=-1';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * InvoiceExportService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param DateTime $deliveryDate
     * @param Company|null $company
     *
     * @return Order[]
     * @throws RecurrException
     */
    public function getOrdersForExport(DateTime $deliveryDate, ?Company $company = null): array
    {
        $qb = $this->getOrdersForExportQueryBuilder($deliveryDate, $company);

        $orders = array_merge(
            $qb->getQuery()->getResult(),
            $this->getOrderCollectionOrdersForExport($deliveryDate, $company)
        );

        $orders = array_unique($orders, SORT_REGULAR);

        return $orders;
    }

    /**
     * @param DateTime $deliveryDate
     *
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws RecurrException
     */
    public function ordersAvailableForExport(DateTime $deliveryDate): bool
    {
        $orderQb = $this->getOrdersForExportQueryBuilder($deliveryDate);
        $orderQb->select('COUNT(ord.id)');
        $count = $orderQb->getQuery()->getSingleResult();

        if($count) {
            return true;
        }

        $orderCollectionQb = $this->getOrderCollectionOrdersForExportQueryBuidler($deliveryDate);
        $orderCollectionQb->select('COUNT(_order_collection.id)');
        $count = $orderCollectionQb->getQuery()->getSingleResult();

        if($count) {
            return true;
        }

        return false;
    }

    /**
     * @param DateTime $deliveryDate
     * @param Company|null $company
     *
     * @return QueryBuilder
     * @throws RecurrException
     */
    private function getOrdersForExportQueryBuilder(DateTime $deliveryDate, ?Company $company = null): QueryBuilder
    {
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('ord');
        $qb->leftJoin('ord.orderCollection', 'order_collection');
        $qb->leftJoin('order_collection.company', 'company');

        if ($company) {
            $qb->andWhere('company.id = :company');
            $qb->setParameter('company', $company->getId());
        }

        $this->applyDeliveryDateExpressions($qb, $deliveryDate);
        $this->applyOrderExpressions($qb);

        $qb->addOrderBy('ord.deliveryDate');

        return $qb;
    }

    /**
     * @param DateTime $deliveryDate
     * @param Company|null $company
     *
     * @return array
     */
    private function getOrderCollectionOrdersForExport(DateTime $deliveryDate, ?Company $company = null): array
    {
        $qb = $this->getOrderCollectionOrdersForExportQueryBuidler($deliveryDate, $company);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param DateTime $deliveryDate
     * @param Company|null $company
     *
     * @return QueryBuilder
     */
    private function getOrderCollectionOrdersForExportQueryBuidler(DateTime $deliveryDate, ?Company $company = null): QueryBuilder
    {
        $qbIn = $this->entityManager->getRepository(OrderCollection::class)->createQueryBuilder('_order_collection');
        $qbIn->leftJoin('_order_collection.orders', '_order');
        $qbIn->groupBy('_order_collection.id');
        $qbIn->andHaving("SUM(IF(_order.deliveryDate <= :deliveryDate, 1, 0)) = SUM(IF(_order.status IN ('pending', 'processed', 'sent', 'complete'), 1, 0))");

        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('ord');
        $qb->leftJoin('ord.orderCollection', 'order_collection');
        $qb->leftJoin('order_collection.company', 'company');
        $qb->andWhere($qb->expr()->in('ord.orderCollection', $qbIn->getDQL()));
        $qb->setParameter('deliveryDate', $deliveryDate);

        if ($company) {
            $qb->andWhere('company.id = :company');
            $qb->setParameter('company', $company->getId());
        }

        $qb->addOrderBy('ord.deliveryDate');

        $this->applyOrderExpressions($qb);

        return $qb;
    }

    /**
     * @param DateTime $deliveryDate
     * @param bool $returnQb
     *
     * @param QueryBuilder|null $qb
     *
     * @return Company[] | QueryBuilder
     * @throws RecurrException
     */
    public function getCompaniesForExport(DateTime $deliveryDate, bool $returnQb = false, QueryBuilder $qb = null)
    {
        if($qb === null) {
            $qb = $this->entityManager->getRepository(Company::class)->createQueryBuilder('company');
        }
        $qb->leftJoin('company.orders', 'order_collection');
        $qb->leftJoin('order_collection.orders', 'ord');

        $this->applyDeliveryDateExpressions($qb, $deliveryDate);
        $this->applyOrderExpressions($qb);

        return $returnQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * @return bool
     */
    public function canCreateNewExport(): bool
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select($qb->expr()->count('inex.id'))
            ->from(InvoiceExport::class, 'inex')
            ->where('inex.exportStatus IN (:value)')
            ->setParameters([
                'value' => [
                    ExactInvoiceExportStatusType::NEW,
                    ExactInvoiceExportStatusType::IN_PROGRESS,
                ],
            ]);

        try {
            return ((int)$qb->getQuery()->getSingleScalarResult() === 0);
        } catch (NoResultException | NonUniqueResultException $e) {
            return false;
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param DateTime $deliveryDate
     *
     * @throws RecurrException
     */
    private function applyDeliveryDateExpressions(QueryBuilder $qb, DateTime $deliveryDate): void
    {
        $expressions = [];

        $qb->leftJoin('company.companyInvoiceSettings', 'invoiceSettings');

        foreach ($this->calculateRules($deliveryDate) as $key => $ruleDeliveryDate) {
            if ($key === 'order') {
                $expressions[] = $qb->expr()->andX(
                    $qb->expr()->isNull('invoiceSettings.invoiceFrequency'),
                    $qb->expr()->lte('ord.deliveryDate', ':delivery_date_' . $key)
                );
            } else {
                $expressions[] = $qb->expr()->andX(
                    $qb->expr()->eq('invoiceSettings.invoiceFrequency', ':' . $key),
                    $qb->expr()->lte('ord.deliveryDate', ':delivery_date_' . $key)
                );

                $qb->setParameter($key, $key);
            }

            $qb->setParameter('delivery_date_' . $key, $ruleDeliveryDate);
        }

        $qb->andWhere(call_user_func_array([$qb->expr(), 'orX'], $expressions));
    }

    /**
     * @param QueryBuilder $qb
     */
    private function applyOrderExpressions(QueryBuilder $qb): void
    {
        $qb->andWhere('ord.deletedAt IS NULL');

        $qb->andWhere('order_collection.deletedAt IS NULL');

        $qb->andWhere('company.deletedAt IS NULL');
        $qb->andWhere('company.id IS NOT NULL');

        $qb->andWhere("IF(ord.status IN ('pending', 'processed', 'sent', 'complete'), 1, 0) = 1");

        $qb->join('order_collection.payments', 'payment');
        $qb->andWhere('payment.status = :paymentstatus')
            ->setParameter('paymentstatus', 'authorized');

        $qb->join('payment.paymentmethod', 'paymentmethod');
        $qb->andWhere('paymentmethod.code = :paymentmethodcode')
            ->setParameter('paymentmethodcode', 'invoice');

        // Don't invoice orders already exported with Twinfield (should be removed Twinfield export is being removed)
        $qb->leftJoin('ord.twinfieldExportOrders', 'twinfield_export_orders');
        $qb->andWhere('twinfield_export_orders.id IS NULL');

        //TODO check for credit invoices
        $qb->leftJoin('ord.billingItems', 'billing_items');
        $qb->andWhere('billing_items.id IS NULL');
    }

    /**
     * @return array
     */
    private function getRules(): array
    {
        return [
            CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_COLLECTIVE => self::RRULE_DAILY,
            CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_WEEKLY => self::RRULE_WEEKLY,
            CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_BIWEEKLY => self::RRULE_BIWEEKLY,
            CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_MONTHLY => self::RRULE_MONTHLY,
            CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_QUARTERLY => self::RRULE_QUARTERLY,
        ];
    }

    /**
     * @param DateTime $deliveryDate
     *
     * @return array
     * @throws RecurrException
     */
    private function calculateRules(DateTime $deliveryDate): array
    {
        $deliveryDate = clone $deliveryDate;
        $deliveryDate->setTime(23, 59, 59);

        $results = [
            'order' => $deliveryDate,
        ];

        foreach ($this->getRules() as $key => $rule) {
            $occurences = $this->calculateRuleOccurences($rule, $deliveryDate);

            if (!$occurences->isEmpty()) {
                $results[$key] = clone $occurences->last()->getStart();
            }
        }

        return $results;
    }

    /**
     * @param string $ruleString
     * @param DateTime $deliveryDate
     *
     * @return Recurrence[]|RecurrenceCollection
     * @throws RecurrException
     * @throws Exception
     * @internal only for unit test
     */
    public function calculateRuleOccurences(string $ruleString, DateTime $deliveryDate): RecurrenceCollection
    {
        $rule = new Rule($ruleString, $this->getStartDate(), null);

        return (new ArrayTransformer())->transform($rule, new BeforeConstraint($deliveryDate));
    }

    /**
     *
     */
    public function checkForFailedExportJobs(): void
    {
        try {
            $exportsUpdated = false;
            $inProgressExports = $this->entityManager->getRepository(InvoiceExport::class)->findBy([
                'exportStatus' => ExactInvoiceExportStatusType::IN_PROGRESS,
            ]);

            foreach ($inProgressExports as $inProgressExport) {
                $jobQuery = $this->entityManager->getRepository(Job::class)->createQueryBuilder('job');
                $jobQuery->select('job.state');
                $jobQuery->where("job.command = 'app:finance:invoice-export-generator' AND job.args = :exportId");
                $jobQuery->setParameter('exportId', sprintf('[%d]', $inProgressExport->getId()));

                if ($jobQuery->getQuery()->getSingleScalarResult() === Job::STATE_FAILED) {
                    $exportsUpdated = true;
                    $inProgressExport->setExportStatus(ExactInvoiceExportStatusType::FAILED);
                }
            }
            if ($exportsUpdated) {
                $this->entityManager->flush();
            }
        } catch (NonUniqueResultException $e) {
            //For the very small change this race condition ever occurs.
            //There is no job for the currently in progress export, this will be created very shortly by a different process.
        }
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    private function getStartDate()
    {
        // NEVER CHANGE THIS DATE !! RRULE_BIWEEKLY COULD SHIFT FROM A ODD TO EVEN WEEK
        return new DateTime('2018-12-30');
    }
}