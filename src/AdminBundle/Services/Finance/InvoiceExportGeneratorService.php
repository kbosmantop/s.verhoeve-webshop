<?php

namespace AdminBundle\Services\Finance;

use AppBundle\DBAL\Types\AppliesToOrderType;
use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;
use AppBundle\DBAL\Types\CompanyInvoiceFrequencyType;
use AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\BillingItem;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyInvoiceSettings;
use AppBundle\Manager\Order\BillingItemGroupManager;
use AppBundle\Manager\Order\BillingItemManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use AppBundle\ThirdParty\Exact\Entity\InvoiceLine;
use AppBundle\ThirdParty\Exact\Xml\DebtorAdapter;
use AppBundle\ThirdParty\Exact\Xml\ExactXmlFactory;
use AppBundle\ThirdParty\Exact\Xml\InvoiceAdapter;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use InvalidArgumentException;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use League\Flysystem\MountManager;
use Recurr\Exception;
use RuntimeException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class InvoiceExportService
 *
 * @link https://topgeschenken.atlassian.net/wiki/spaces/TOP/pages/516784172/Export+naar+Exact
 */
class InvoiceExportGeneratorService
{
    private const EXACT_GLOBE_DECIMAL_PRECISION = 3;

    /**
     * @var InvoiceExportService
     */
    private $invoiceExportService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderLineManager
     */
    private $orderLineManager;

    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * @var MountManager
     */
    private $mountManager;

    /**
     * @var ExactXmlFactory
     */
    private $exactXmlFactory;

    /**
     * @var int
     */
    private $decimalPrecision;

    /**
     * @var BillingItemGroupManager
     */
    private $billingItemGroupManager;

    /**
     * @var BillingItemManager
     */
    private $billingItemManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * InvoiceExportGeneratorService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ExactXmlFactory $exactXmlFactory
     * @param InvoiceExportService $invoiceExportService
     * @param MountManager $mountManager
     * @param OrderLineManager $orderLineManager
     * @param BillingItemGroupManager $billingItemGroupManager
     * @param BillingItemManager $billingItemManager
     * @param TranslatorInterface $translator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ExactXmlFactory $exactXmlFactory,
        InvoiceExportService $invoiceExportService,
        MountManager $mountManager,
        OrderLineManager $orderLineManager,
        BillingItemGroupManager $billingItemGroupManager,
        BillingItemManager $billingItemManager,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->invoiceExportService = $invoiceExportService;
        $this->mountManager = $mountManager;
        $this->orderLineManager = $orderLineManager;
        $this->exactXmlFactory = $exactXmlFactory;

        $this->decimalPrecision = self::EXACT_GLOBE_DECIMAL_PRECISION;
        $this->billingItemGroupManager = $billingItemGroupManager;
        $this->billingItemManager = $billingItemManager;
        $this->translator = $translator;
    }

    /**
     * @param int $decimalPrecision
     *
     * @internal Should only used in unit testing
     */
    public function setDecimalPrecision(int $decimalPrecision): void
    {
        if (!in_array($decimalPrecision, [2, 3], true)) {
            throw new RuntimeException('Value must be either 2 or 3');
        }

        $this->decimalPrecision = $decimalPrecision;
    }

    /**
     * @param InvoiceExport $invoiceExport
     *
     * @throws Exception
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function generate(InvoiceExport $invoiceExport): void
    {
        $this->entityManager->beginTransaction();
        $companyRespository = $this->entityManager->getRepository(Company::class);

        $qb = $this->entityManager->getRepository(Invoice::class)->createQueryBuilder('invoice');
        $qb->delete();
        $qb->andWhere('invoice.invoiceExport = :invoiceExport');
        $qb->setParameter('invoiceExport', $invoiceExport->getId());
        $qb->getQuery()->execute();

        foreach ($invoiceExport->getCompaniesToProcess() as $companyId) {
            $company = $companyRespository->find($companyId);
            if ($company === null || $company->getCompanyInvoiceSettings() === null) {
                throw new InvalidArgumentException('Company invoice settings must be defined');
            }

            $orders = $this->invoiceExportService->getOrdersForExport($invoiceExport->getDeliveryDate(), $company);

            $groupOrders = $this->groupOrders($orders);

            foreach ($groupOrders as $orders) {
                $invoice = new Invoice();
                $invoice->setInvoiceExport($invoiceExport);
                $invoice->setCompany($company);
                $invoice->setInvoiceDate($invoiceExport->getInvoiceDate());

                /** @var CompanyInvoiceSettings $companyInvoiceSettings */
                $companyInvoiceSettings = $company->getCompanyInvoiceSettings();

                $externalRef = vsprintf('Bestelling %s', [
                    $orders[0]->getOrderCollection()->getNumber(),
                ]);

                switch ($companyInvoiceSettings->getInvoiceFrequency()) {
                    case CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER:
                        $invoice->setExternalRef($externalRef);

                        if ($orders[0]->getInvoiceReference()) {
                            $invoice->setExternalRef($invoice->getExternalRef() . ', ' . $orders[0]->getInvoiceReference());
                        }

                        break;
                    case CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER_COLLECTION:
                    default:
                        $invoice->setExternalRef($externalRef);
                }

                $billingItemGroups = [];
                /** @var Order $order */
                foreach ($orders as $order) {
                    $orderCollection = $order->getOrderCollection();

                    if (!isset($billingItemGroups[$orderCollection->getId()])) {
                        $billingItemGroup = $this->billingItemGroupManager->create($orderCollection);
                        $billingItemGroups[$orderCollection->getId()] = $billingItemGroup;

                        $this->entityManager->persist($billingItemGroup);
                        $invoice->addBillingItemGroup($billingItemGroup);
                    } else {
                        $billingItemGroup = $billingItemGroups[$orderCollection->getId()];
                    }

                    $billingItem = $this->billingItemManager->createByOrder($order, $billingItemGroup);

                    if($billingItem === null){
                        continue;
                    }

                    $this->transformOrderToInvoiceLines($invoice, $billingItem);
                }

                $this->entityManager->persist($invoice);
            }
        }

        $this->entityManager->flush();
        $this->entityManager->commit();

        $this->writeExactXmlToFile($invoiceExport);
    }

    /**
     * @param array|Order[] $orders
     *
     * @return array
     * @internal for unit test
     */
    public function groupOrders(array $orders): array
    {
        if (count($orders) === 0) {
            return [];
        }

        $company = $orders[0]->getOrderCollection()->getCompany();

        if ($company->getCompanyInvoiceSettings() === null) {
            throw new InvalidArgumentException('Company invoice settings must be defined');
        }

        $groupedOrders = [];

        if ($company->getCompanyInvoiceSettings()->getInvoiceFrequency() === CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER) {
            foreach ($orders as $order) {
                $groupedOrders[$order->getId()][] = $order;
            }

            return $groupedOrders;
        }

        if ($company->getCompanyInvoiceSettings()->getInvoiceFrequency() === CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER_COLLECTION) {
            foreach ($orders as $order) {
                $groupedOrders[$order->getOrderCollection()->getId()][] = $order;
            }

            return $groupedOrders;
        }

        $groupFields = [];

        foreach ($company->getCustomFields() as $customField) {
            if ($customField->isGroupInvoices()) {
                $groupFields[$customField->getFieldKey()];
            }
        }

        if (!$groupFields) {
            // if grouping is not required, return the orders in a single group;
            return [$orders];
        }

        foreach ($orders as $order) {
            if ($company->getCompanyInvoiceSettings()->getInvoiceFrequency() === CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER) {
                $groupedOrders[$order->getId()] = [$order];
            }

            $groupFieldValues = [];

            foreach ($order->getOrderCollection()->getCompanyCustomerOrderFieldValues() as $fieldValue) {
                if ($fieldValue->getCompanyCustomField() === null
                    || !$fieldValue->getCompanyCustomField()->isGroupInvoices()
                    || !in_array($fieldValue->getCompanyCustomField()->getAppliesTo(),
                        [AppliesToOrderType::APPLIES_TO_ORDER_COLLECTION, AppliesToOrderType::APPLIES_TO_NONE], true)) {
                    continue;
                }

                $groupFieldValues[$fieldValue->getCompanyCustomField()->getFieldKey()] = $fieldValue->getValue();
            }

            ksort($groupFieldValues);

            $groupIdentifier = implode(',', $groupFieldValues);

            if (!array_key_exists($groupIdentifier, $groupedOrders)) {
                $groupedOrders[$groupIdentifier] = [];
            }

            $groupedOrders[$groupIdentifier][] = $order;
        }

        return $groupedOrders;
    }

    /**
     * @param Invoice $invoice
     * @param BillingItem $billingItem
     *
     * @internal
     */
    public function transformOrderToInvoiceLines(Invoice $invoice, BillingItem $billingItem): void
    {
        $publishableFilter = DisableFilter::temporaryDisableFilter('publishable');
        $softdeletableFilter = DisableFilter::temporaryDisableFilter('softdeleteable');

        $company = $invoice->getCompany();

        if ($company->getInvoiceAddress() === null) {
            throw new RuntimeException(vsprintf("Company with id %d doesn't have an country set", [
                $company->getId(),
            ]));
        }

        $invoiceSettings = $company->getCompanyInvoiceSettings();

        if ($invoiceSettings === null) {
            throw new RuntimeException(vsprintf('no company invoice settings configured for company with id %d', [
                $company->getId(),
            ]));
        }

        $order = $billingItem->getOrder();
        $this->translator->setLocale($order->getLocale());

        foreach ($order->getLines() as $orderLine) {
            foreach ($orderLine->getVatLines() as $vatLine) {
                if (!$invoiceSettings->isInvoiceZeroValue() && $vatLine->getAmount() === 0.0) {
                    continue;
                }

                $discountAmountExcl = $this->orderLineManager->getDiscountAmountExcl($orderLine);

                if ($discountAmountExcl && $invoiceSettings->getInvoiceDiscount() === CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED) {
                    $description = $orderLine->getTitle() . ' (' . $this->translator->trans('label.incl.discount') . ')';
                    $priceValue = ($vatLine->getAmount() - $discountAmountExcl) / $orderLine->getQuantity();
                } else {
                    $description = $orderLine->getTitle();
                    $priceValue = round($vatLine->getAmount() / $orderLine->getQuantity(), 3);
                }

                if ($priceValue === 0.00 && !$invoiceSettings->isInvoiceZeroValue()) {
                    continue;
                }

                $productGroup = $orderLine->getProduct()->getProductgroup();
                if($productGroup === null){
                    throw new RuntimeException(vsprintf('No product group found for product: %d', [
                        $orderLine->getProduct()->getId(),
                    ]));
                }
                $ledger = $productGroup->getLedger();
                if($ledger === null){
                    throw new RuntimeException(vsprintf('No ledger found for product group: %d', [
                        $productGroup->getId(),
                    ]));
                }

                $invoiceLine = new InvoiceLine();
                $invoiceLine->setOrderLine($orderLine);
                $invoiceLine->setDescription($description);
                $invoiceLine->setItemCode($ledger->getNumber());
                $invoiceLine->setQuantity($orderLine->getQuantity());
                $invoiceLine->setPriceValue(round($priceValue, $this->decimalPrecision));
                $invoiceLine->setVatCode($vatLine->getVatRate() ? $vatLine->getVatRate()->getCode() : null);

                $invoice->addLine($invoiceLine);

                if ($discountAmountExcl && $invoiceSettings->getInvoiceDiscount() === CompanyInvoiceDiscountType::INVOICE_DISCOUNT_SEPARATE) {
                    $discountDescription = $this->orderLineManager->getDiscountDescription($orderLine);

                    if (!$discountDescription) {
                        $discountDescription = $this->translator->trans('label.discount');
                    }

                    $invoiceLine = new InvoiceLine();
                    $invoiceLine->setOrderLine($orderLine);
                    $invoiceLine->setDescription(' - ' . $discountDescription);
                    $invoiceLine->setQuantity(1);
                    $invoiceLine->setPriceValue(round(0 - $this->orderLineManager->getDiscountAmountExcl($orderLine),
                        $this->decimalPrecision));
                    $invoiceLine->setVatCode($vatLine->getVatRate() ? $vatLine->getVatRate()->getCode() : null);

                    $invoice->addLine($invoiceLine);
                }
            }
        }

        if ($invoiceSettings->isInvoiceZeroValue()) {
            if ($invoiceSettings->getInvoiceInclPersonalData() !== CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_NONE) {
                $personalData = [];

                if (in_array($invoiceSettings->getInvoiceInclPersonalData(), [
                    CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_RECIPIENT,
                    CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_ALL,
                ], true)) {
                    $personalData[] = $this->translator->trans('label.receiver', [
                        'receiver' => $this->personalDataFormatter($order->getDeliveryAddressAttn(),
                            $order->getDeliveryAddressCompanyName()),
                    ]);
                }

                if (in_array($invoiceSettings->getInvoiceInclPersonalData(), [
                    CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_CUSTOMER,
                    CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_ALL,
                ], true)) {
                    $personalData[] = $this->translator->trans('label.sender', [
                        'sender' => $this->personalDataFormatter($order->getOrderCollection()->getInvoiceAddressAttn(),
                            $order->getOrderCollection()->getInvoiceAddressCompanyName()),
                    ]);
                }

                $invoiceLine = new InvoiceLine();
                $invoiceLine->setDescription(implode(', ', $personalData));

                $invoice->addLine($invoiceLine);
            }

            $customFields = [];

            foreach ($order->getOrderCollection()->getCompanyCustomerOrderFieldValues() as $fieldValue) {
                if ($fieldValue->getCompanyCustomField() === null || !in_array($fieldValue->getCompanyCustomField()->getAppliesTo(),
                        [AppliesToOrderType::APPLIES_TO_ORDER_COLLECTION, AppliesToOrderType::APPLIES_TO_NONE], true)) {
                    continue;
                }

                $customFields[] = $fieldValue->getCompanyCustomField()->translate($order->getLocale())->getLabel() . ': ' . $fieldValue->getValue();
            }

            if ($customFields) {
                $invoiceLine = new InvoiceLine();
                $invoiceLine->setDescription(implode(' ,', $customFields));

                $invoice->addLine($invoiceLine);
            }
        }

        $publishableFilter->reenableFilter();
        $softdeletableFilter->reenableFilter();
    }

    /**
     * @param string|null $attn
     * @param string|null $company
     *
     * @return string
     */
    private function personalDataFormatter(?string $attn, ?string $company): string
    {
        $formattedPersonalData = (string)$attn;

        if ($company) {
            $formattedPersonalData .= ' (' . $company . ')';
        }

        return trim($formattedPersonalData);
    }

    /**
     * @return Filesystem
     */
    private function getFilesystem(): Filesystem
    {
        if (null === $this->filesystem) {
            $this->filesystem = $this->mountManager->getFilesystem('filesystem_private');
        }

        return $this->filesystem;
    }

    /**
     * @param string $path
     * @param string $data
     * @param bool $overwrite
     *
     * @return bool
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function write($path, $data, $overwrite = false): bool
    {
        if (true === $overwrite && $this->getFilesystem()->has($path)) {
            $this->getFilesystem()->delete($path);
        }
        return $this->getFilesystem()->write($path, $data);
    }

    /**
     * @param InvoiceExport $invoiceExport
     * @param string $type
     *
     * @return string
     */
    private function getXmlPath(InvoiceExport $invoiceExport, string $type): string
    {
        $basePath = 'exact/imports';
        return vsprintf('%s/%s-%d.xml', [
            $basePath,
            $type,
            $invoiceExport->getId(),
        ]);
    }

    /**
     * @param InvoiceExport $invoiceExport
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function writeExactXmlToFile(InvoiceExport $invoiceExport): void
    {
        $deptorXml = $this->exactXmlFactory->create(DebtorAdapter::class, $invoiceExport);
        $this->write($this->getXmlPath($invoiceExport, 'debtors'), $deptorXml->generateXml(), true);

        $invoiceXml = $this->exactXmlFactory->create(InvoiceAdapter::class, $invoiceExport);
        $this->write($this->getXmlPath($invoiceExport, 'invoices'), $invoiceXml->generateXml(), true);
    }

    /**
     * @param InvoiceExport $invoiceExport
     *
     * @return bool|false|string
     * @throws FileNotFoundException
     */
    public function readDebtorsXml(InvoiceExport $invoiceExport)
    {
        $filesystem = $this->getFilesystem();
        return $filesystem->read($this->getXmlPath($invoiceExport, 'debtors'));
    }

    /**
     * @param InvoiceExport $invoiceExport
     *
     * @return bool|false|string
     * @throws FileNotFoundException
     */
    public function readInvoicesXml(InvoiceExport $invoiceExport)
    {
        $filesystem = $this->getFilesystem();
        return $filesystem->read($this->getXmlPath($invoiceExport, 'invoices'));
    }

    /**
     * @param string $country
     *
     * @return VatRate
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function findNullVatGroup(string $country): VatRate
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->entityManager->getRepository(VatRate::class);

        $qb = $entityRepository->createQueryBuilder('vat_rate');
        $qb->leftJoin('vat_rate.group', '_group');
        $qb->leftJoin('_group.level', 'level');
        $qb->andWhere('level.id = 3');
        $qb->andWhere('_group.country = :country');
        $qb->setParameter('country', $country);
        $qb->setMaxResults(1);

        return $qb->getQuery()->getSingleResult();
    }
}