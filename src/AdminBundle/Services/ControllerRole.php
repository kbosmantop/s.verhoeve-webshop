<?php

namespace AdminBundle\Services;

use AdminBundle\Controller\Controller;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class ControllerRole
{
    private $roles = [];

    public function getRoles($normalized = false, $directory = "../src/", $namespace = null)
    {
        $this->generateRolesList($directory, $namespace, $normalized);

        return $this->roles;
    }

    private function generateRolesList($directory, $namespace, $normalized)
    {
        $roles = $this->scanDirectory($directory, $namespace, $normalized);
        $this->roles = $this->roles + $roles;
    }

    private function scanDirectory($directory, $namespace, $normalized)
    {
        $roles = [
            'crud' => [],
            'other' => [],
        ];

        $finder = new Finder();
        $finder
            ->in($directory)
            ->exclude('Resources')
            ->exclude([
                "Tests",
                "tests",
            ])
            ->notName("*Interface.php")
            ->name('*.php');

        foreach ($finder as $file) {
            $class = str_replace("/", "\\", str_replace($directory, null, $file->getRelativePath()));

            if (!$class) {
                continue;
            }

            $classPath = $namespace . $class . '\\' . $file->getBasename('.php');

            if (!class_exists($classPath)) {
                continue;
            }

            if (stripos($classPath, 'CrudController') !== false) {
                continue;
            }

            if (!in_array(Controller::class, class_parents($classPath))) {
                continue;
            }

            $permissionArray = [null];

            if (in_array(CrudControllerInterface::class, class_implements($classPath))) {
                $permissionArray = array_merge($permissionArray, ['READ', 'CREATE', 'UPDATE', 'DELETE']);
            }

            if (method_exists($classPath, 'getControllerRoles')) {
                $controllerSpecificPermissions = call_user_func([$classPath, 'getControllerRoles']);

                if (is_array($controllerSpecificPermissions)) {
                    $permissionArray = array_merge($permissionArray, $controllerSpecificPermissions);
                }
            }

            array_unique($permissionArray);

            $className = call_user_func([$classPath, 'getLabel']);

            foreach ($permissionArray as $permission) {
                $classNameUpper = strtoupper($className);

                $role = $this->getRole($classPath, $permission);

                if (!$normalized) {
                    if (!$permission) {
                        $permission = 'VIEW';
                    }

                    if (!in_array(CrudControllerInterface::class, class_implements($classPath))) {
                        $roles['other'][$classNameUpper][$permission] = $role;
                    } else {
                        $roles['crud'][$classNameUpper][$permission] = $role;
                    }
                } else {
                    if (!in_array(CrudControllerInterface::class, class_implements($classPath))) {
                        $roles['other'][$role] = $role;
                    } else {
                        $roles['crud'][$role] = $role;
                    }
                }
            }
        }

        return $roles;
    }

    public function getRole($class, $permission)
    {
        $normalizer = new CamelCaseToSnakeCaseNameConverter();

        $replacements = [
            'controller',
            'service',
        ];

        $className = ltrim(strtoupper($normalizer->normalize(str_replace($replacements, null,
            strtolower((new \ReflectionClass($class))->getShortName())))), '_');

        $role = 'ROLE_' . $className . (($permission) ? '_' . strtoupper($permission) : null);

        return $role;
    }
}
