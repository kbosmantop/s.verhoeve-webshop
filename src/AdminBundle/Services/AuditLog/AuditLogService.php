<?php

namespace AdminBundle\Services\AuditLog;

use AppBundle\Interfaces\Audit\AuditEntityInterface;
use DataDog\AuditBundle\Entity\Association;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AuditLogService
 * @package AdminBundle\Services\AuditLog
 */
class AuditLogService
{
    use ContainerAwareTrait;

    /** @var array */
    private $historyInfo;

    /**
     * @param AuditLog $log
     * @return array
     */
    public function getLogs(AuditLog $log)
    {
        /** @var Association $source */
        $source = $log->getSource();

        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->container->get('doctrine')->getRepository(AuditLog::class)->createQueryBuilder('log')
            ->leftJoin('log.source', 's')
            ->andWhere('s.fk = :fk')
            ->andWhere('s.class = :class')
            ->orderBy('log.loggedAt', 'DESC')
            ->setParameter('class', $source->getClass())
            ->setParameter('fk', $source->getFk());

        $logs = $qb->getQuery()->getResult();

        $auditLogFactory = $this->container->get('app.audit_log.factory');
        $decoratedLogs = array_map(function ($log) use ($auditLogFactory) {
            return $auditLogFactory->get($log);
        }, $logs);

        return $decoratedLogs;
    }

    /**
     * @return array
     */
    public function getHistoryInfo()
    {
        if ($this->historyInfo === null) {
            /** @var EntityManagerInterface $em */
            $em = $this->container->get('doctrine')->getManager();

            $meta = $em->getMetadataFactory()->getAllMetadata();

            $tables = [];

            /** @var ClassMetadata $entity */
            foreach ($meta as $entity) {
                $interfaces = $entity->getReflectionClass()->getInterfaces();

                if (!empty($interfaces) && isset($interfaces[AuditEntityInterface::class])) {
                    $tables[$entity->getTableName()] = $entity->getTableName();
                }
            }

            return ($this->historyInfo = [
                'actions' => ['insert', 'update', 'view'],
                'tables' => array_keys($tables),
            ]);
        } else {
            return $this->historyInfo;
        }
    }
}
