<?php


namespace AdminBundle\Services;

use AdminBundle\Entity\AuthenticationLog;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Recurr\Exception;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AuthenticationLogger
 * @package AdminBundle\Services
 */
class AuthenticationLogger
{
    const REASON_USERNAME_UNKNOWN = 1;
    const REASON_INVALID_CREDENTIALS = 2;
    const REASON_DISABLED = 3;
    const REASON_LOCKED = 4;
    const REASON_EXPIRED = 5;
    const REASON_IP_BLOCKED = 6;
    const REASON_TOO_MANY_ATTEMPTS = 7;
    const REASON_UNKNOWN = 255;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param RequestStack  $requestStack
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param             $type
     * @param string|null $username
     * @param User|null   $user
     * @param string|null $ip
     * @param string|null $userAgent
     *
     * @return bool
     */
    public function success($type, $username = null, User $user = null, $ip = null, $userAgent = null)
    {
        if (!in_array(strtolower($type), ['login', 'logout'])) {
            return false;
        }

        $this->log($type, 'success', null, $username, $user, $ip, $userAgent);

        return true;
    }

    /**
     * @param string|null $username
     * @param integer     $reason
     * @param string|null $ip
     * @param string|null $userAgent
     */
    public function failure($username = null, $reason, $ip = null, $userAgent = null): void
    {
        $this->log('login', 'failure', $reason, $username, null, $ip, $userAgent);
    }

    /**
     * @param string       $type
     * @param string       $event
     * @param integer|null $reason
     * @param string|null  $username
     * @param User|null    $user
     * @param string|null  $ip
     * @param string|null  $userAgent
     *
     * @return bool
     */
    private function log($type, $event, $reason, $username = null, $user = null, $ip = null, $userAgent = null)
    {
        try {
            if (is_null($ip) && $this->requestStack->getMasterRequest()->getClientIp()) {
                $ip = $this->requestStack->getMasterRequest()->getClientIp();
            }

            if (is_null($userAgent) && $this->requestStack->getMasterRequest()->headers->has('User-Agent')) {
                $userAgent = $this->requestStack->getMasterRequest()->headers->get('User-Agent');
            }

            $authenticationLog = new AuthenticationLog();
            $authenticationLog->setType($type)
                ->setEvent($event)
                ->setReason($reason)
                ->setUsername($username)
                ->setUser($user)
                ->setDatetime(new \DateTime())
                ->setIp($ip)
                ->setUserAgent($userAgent);

            $this->entityManager->persist($authenticationLog);
            $this->entityManager->flush($authenticationLog);

            // Implement monolog messages
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public static function getReasonDescription($reason)
    {
        $description = null;

        switch ($reason) {
            case self::REASON_USERNAME_UNKNOWN:
                $description = 'Gebruikersnaam onbekend';

                break;
            case self::REASON_INVALID_CREDENTIALS:
                $description = 'Ongeldige combinatie gebruikersnaam/wachtwoord';

                break;
            case self::REASON_DISABLED:
                $description = 'Account uitgeschakeld';

                break;
            case self::REASON_LOCKED:
                $description = 'Account geblokkeerd';

                break;
            case self::REASON_EXPIRED:
                $description = 'Account verlopen';

                break;
            case self::REASON_IP_BLOCKED:
                $description = 'IP-adres geblokkeerd';

                break;
            case self::REASON_TOO_MANY_ATTEMPTS:
                $description = 'Teveel pogingen';

                break;
            case self::REASON_UNKNOWN:
            default:
                $description = 'Onbekende reden';

                break;
        }

        return $description;
    }
}
