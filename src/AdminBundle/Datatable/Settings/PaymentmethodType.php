<?php

namespace AdminBundle\Datatable\Settings;

use AdminBundle\Components\Datatable\Column\ChoiceColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\DBAL\Types\PaymentGatewayType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PaymentmethodType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add("description", TextColumn::class, [
                'label' => 'Omschrijving',
            ])
            ->add('gateway', ChoiceColumn::class, [
                'label' => 'Gateway',
                'choices' => array_flip(PaymentGatewayType::getChoices()),
            ])
            ->add('code', TextColumn::class, [
                'label' => 'Code',
            ]);
    }
}
