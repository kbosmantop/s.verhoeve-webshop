<?php

namespace AdminBundle\Datatable\Settings;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Entity\Settings\OrderPriorityRule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderPriorityRuleDatatable
 * @package AdminBundle\Datatable\Settings
 */
class OrderPriorityRuleDatatable extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('priority', TextColumn::class, [
                'label' => 'Prioriteit',
            ])
            ->add('rule', EntityColumn::class, [
                'label' => 'Regel',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderPriorityRule::class,
        ]);
    }
}
