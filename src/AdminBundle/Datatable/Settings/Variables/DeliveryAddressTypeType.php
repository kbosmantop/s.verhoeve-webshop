<?php

namespace AdminBundle\Datatable\Settings\Variables;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DeliveryAddressTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ]);
    }
}
