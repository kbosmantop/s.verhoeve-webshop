<?php

namespace AdminBundle\Datatable\Settings;

use AdminBundle\Components\Datatable\Column\CollectionColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Site\Domain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;

/**
 * Class SiteType
 * @package AdminBundle\Datatable\Settings
 */
class SiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return FormBuilderInterface
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('description', TextColumn::class, [
                'label' => 'Omschrijving',
                'orderable' => false
            ])
            ->add('domains', CollectionColumn::class, [
                'label' => 'Primaire domein(en)',
                'property' => Domain::class,
                'value' => function (Domain $domain) {
                    if ($domain->getMain()) {
                        $language = $domain->getDefaultLocale();

                        $url = [];
                        $url[] = $domain->getSite()->getHttpScheme();
                        $url[] = '://';
                        $url[] = $domain->getDomain();

                        if ($domain->getSite()->getHttpPort() !== 80) {
                            $url[] = ':';
                            $url[] = $domain->getSite()->getHttpPort();
                        }

                        $url = implode('', $url);

                        $locale = explode('_', $language);

                        $language = Intl::getLanguageBundle()->getLanguageName($locale[0], $locale[1]);

                        return '<a href="' . $url . '" target="_blank">' . $domain->getDomain() . ' (' . $language . ')</a>';
                    }

                    return false;
                },
            ]);

        return $builder;
    }
}
