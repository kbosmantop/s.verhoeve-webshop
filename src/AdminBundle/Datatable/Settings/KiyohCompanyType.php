<?php

namespace AdminBundle\Datatable\Settings;

use AdminBundle\Components\Datatable\Column\BooleanColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KiyohCompanyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('syncEnabled', BooleanColumn::class, [
                'label' => 'Synchronisatie actief',
            ])
            ->add('totalScore', TextColumn::class, [
                'label' => 'Score',
            ])
            ->add('totalReviews', TextColumn::class, [
                'label' => 'Aantal reviews',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KiyohCompany::class,
        ]);
    }
}
