<?php

namespace AdminBundle\Datatable\Settings\Designer;

use AdminBundle\Components\Datatable\Column\MediaColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Services\Designer;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DesignerTemplateDatatableType
 * @package AdminBundle\Datatable\Settings\Designer
 */
class DesignerTemplateDatatableType extends AbstractType implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        // todo: autowire SF4
        $designer = $this->container->get('app.designer');

        $builder
            ->add('image', MediaColumn::class, [
                'label' => 'Afbeelding',
                'width' => 150,
                'value' => function (DesignerTemplate $designerTemplate) use ($designer) {

                    $designer->setUuid($designerTemplate->getUuid());
                    $designer->setIsPreDesign(true);

                    $image = $designer->getTemplatePreview();

                    if(null === $image) {
                        $image = $designer->getFileUrl(Designer::FILE_PREVIEW);
                    }

                    return $image;
                },
            ])
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
        ;
    }
}
