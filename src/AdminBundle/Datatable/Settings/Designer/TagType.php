<?php

namespace AdminBundle\Datatable\Settings\Designer;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TagType
 * @package AdminBundle\Datatable\Settings\Designer
 */
class TagType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Labels',
            ]);
    }
}
