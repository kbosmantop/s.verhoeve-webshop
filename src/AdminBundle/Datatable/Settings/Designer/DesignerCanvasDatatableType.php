<?php

namespace AdminBundle\Datatable\Settings\Designer;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DesignerCanvasDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ]);
    }
}
