<?php

namespace AdminBundle\Datatable\Settings;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Entity\Settings\WmsOrderType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WmsOrderTypeDatatable
 * @package AdminBundle\Datatable\Settings
 */
class WmsOrderTypeDatatable extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('code', TextColumn::class, [
                'label' => ' Code',
            ])
            ->add('priority', TextColumn::class, [
                'label' => 'Prioriteit',
            ])
            ->add('rule', EntityColumn::class, [
                'label' => 'Regel',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WmsOrderType::class,
        ]);
    }
}
