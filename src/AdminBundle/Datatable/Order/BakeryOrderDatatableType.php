<?php

namespace AdminBundle\Datatable\Order;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Order\Order;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Services\ConnectorHelper;

/**
 * Class BakeryOrderDatatableType
 * @package AdminBundle\Datatable\Order
 */
class BakeryOrderDatatableType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     */
    public function buildDatatable($builder)
    {
        $builder
            ->add('supplierConnector', CallbackColumn::class, [
                'label' => false,
                'callback' => function($value, Order $order) {
                    $connector = $this->container->get(ConnectorHelper::class)->getConnector($order->getSupplierOrder());

                    return '<div style="position: relative;"><div class="rotate-90-inverse label" style="display: block; position: absolute; left: -56px; top: -2px; width: 85px; background-color: '.$connector->getColor().'">'.$value.'</div></div>';
                }
            ])
            ->add('number', CallbackColumn::class, [
                'label' => '#',
                'callback' => function($value, Order $order) {
                    return $order->getOrderNumber();
                }
            ])
            ->add('supplier', CallbackColumn::class, [
                'label' => 'Leverancier',
                'callback' => function($value, Order $order) {
                    return '<a href="'.$this->container->get('router')->generate('admin_supplier_supplier_view', [
                        'id' => $order->getSupplierOrder()->getSupplier()->getId()
                        ]).'">'.$order->getSupplierOrder()->getSupplier()->getName().'</a>';
                }
            ])
            ->add('deliveryDate', DateColumn::class, [
                'label' => 'Afleverdatum'
            ])
        ;
    }
}