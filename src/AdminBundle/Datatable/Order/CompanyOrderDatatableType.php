<?php

namespace AdminBundle\Datatable\Order;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AppBundle\Entity\Order\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CustomerOrderDatatableType
 * @package AdminBundle\Datatable\Order
 */
class CompanyOrderDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildDatatable($builder)
    {
        $builder
            ->add('status', EntityColumn::class, [
                'label' => 'Status',
            ])
            ->add('ordernumber', CallbackColumn::class, [
                'label' => 'Ordernummer',
                'callback' => function ($value, Order $order) {
                    $returnStr = '<strong>' . $order->getOrderCollection()->getNumber() . '-' . $order->getNumber() . '</strong> <br />';

                    if (null !== $order->getDeliveryDate()) {
                        $returnStr .= $order->getDeliveryDate()->format('d M Y') . '<br/>';
                    }

                    $returnStr .= $order->getOrderCollection()->getSite()->translate()->getDescription();

                    return $returnStr;
                },
            ])
            ->add('besteller', CallbackColumn::class, [
                'label' => 'Besteller',
                'callback' => function ($value, Order $order) {
                    $returnStr = '';

                    $orderCollection = $order->getOrderCollection();
                    $returnStr .= $orderCollection->getInvoiceAddressAttn().'<br/>';
                    $returnStr .= $orderCollection->getInvoiceAddressStreetAndNumber() .'<br/>';
                    $returnStr .= $order->getOrderCollection()->getInvoiceAddressPostcode().' '. $order->getOrderCollection()->getInvoiceAddressCity() .'<br/>';

                    return $returnStr;
                },
            ])
            ->add('receiver', CallbackColumn::class, [
                'label' => 'Ontvanger',
                'callback' => function ($value, Order $order) {
                    $returnStr = '';

                    if (null === $order->getPickupAddress()) {
                        if ($order->getDeliveryAddressCompanyName()) {
                            $returnStr .= $order->getDeliveryAddressCompanyName() . '<br/>';
                        }

                        $returnStr .= $order->getDeliveryAddressAttn() . '<br/>';
                        $returnStr .= $order->getDeliveryAddressStreetAndNumber() . '<br/>';
                        $returnStr .= $order->getDeliveryAddressPostcode() . ' ' . $order->getDeliveryAddressCity() . ' ' . $order->getDeliveryAddressCountry();
                    } else {
                        $returnStr = sprintf('Bestelling wordt afgehaald bij <strong>%s</strong> in <br/><br/>%s',
                            $order->getPickupAddress()->getCompanyName(), $order->getPickupAddress()->getCity());
                    }

                    return $returnStr;
                },
            ])
            ->add('lines', CallbackColumn::class, [
                'label' => 'Bestelling',
                'callback' => function ($value, Order $order) {
                    $i = 1;

                    $returnStr = '';
                    foreach ($order->getLines() as $line) {
                        if ($i === 10) {
                            $returnStr .= 'En meer producten';
                        }

                        if (null !== $line->getProduct()) {
                            $returnStr .= sprintf('<span style="white-space: nowrap">%s x %s</span><br />', $line->getQuantity(), $line->getProduct()->getName());
                        }

                        $i++;
                    }

                    return $returnStr;
                },
            ]);
    }
}