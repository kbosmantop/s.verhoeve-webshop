<?php

namespace AdminBundle\Datatable\Authentication;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IpWhitelistType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('username', TextColumn::class, [
                'label' => 'IP-adres',
            ])
            ->add('firstname', TextColumn::class, [
                'label' => 'Omschrijving',
            ]);
    }
}
