<?php

namespace AdminBundle\Datatable\Company;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\DBAL\Types\CompanyReportCustomerFrequencyType;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyReportCustomerDatatableType
 * @package AdminBundle\Datatable\Company
 */
class CompanyReportCustomerDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder->add('customerName', CallbackColumn::class, [
            'label' => 'Klant',
            'callback' => function ($value, CompanyReportCustomer $companyReportCustomer) {
                return (string) $companyReportCustomer->getCustomer();
            },
        ]);
        $builder->add('startDate', DateTimeColumn::class, [
            'label' => 'Vanaf'
        ]);
        $builder->add('frequency', CallbackColumn::class, [
            'label' => 'Frequentie',
            'callback' => function($value) {
                $choices = array_flip(CompanyReportCustomerFrequencyType::getChoices());
                return $choices[$value];
            }
        ]);
    }
}