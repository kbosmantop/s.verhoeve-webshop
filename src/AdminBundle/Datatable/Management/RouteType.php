<?php

namespace AdminBundle\Datatable\Management;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RouteType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('controller', TextColumn::class, [
                'label' => 'Controller',
            ])
            ->add('method', TextColumn::class, [
                'label' => 'Methode',
            ])
            ->add('scheme', TextColumn::class, [
                'label' => 'Schema',
            ])
            ->add('host', TextColumn::class, [
                'label' => 'Host',
            ])
            ->add('path', TextColumn::class, [
                'label' => 'Pad',
            ]);
    }
}
