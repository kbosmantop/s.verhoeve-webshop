<?php

namespace AdminBundle\Datatable\App;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EmailColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\PhoneNumberColumn;
use AdminBundle\Components\Datatable\Column\StatusColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\Column\TimeAgoColumn;
use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CustomerType
 * @package AdminBundle\Datatable\App
 */
class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options): void
    {
        void($options);

        $builder
            ->add('fullname', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('company', EntityColumn::class, [
                'label' => 'Bedrijf',
                'path' => 'admin_customer_company_view',
            ])
            ->add('username', CallbackColumn::class, [
                'label' => 'Gebruikersnaam',
                'callback' => function ($value, Customer $customer) {
                    $displayName = PHP_EOL . ($customer->getFullname() ?: $customer->getUsername());
                    $registeredSiteId = ($customer->getRegisteredOnSite() !== null && $customer->getRegisteredOnSite()->getId() ? $customer->getRegisteredOnSite()->getId() : -1);

                    if ($customer->canLogin()) {
                        return '<a class="login-as-customer" data-customer-id="' . $customer->getId() . '" data-customer-registered-site-id="' . $registeredSiteId . '" data-customer-name="' . $displayName . '" style="color: #333;" title="Inloggen als gebruiker"><i class="icon-lock2"></i> <i>' . $value . '</i></span>';
                    }

                    if ($customer->getUsername()) {
                        return '<span class="login-as-customer-blocked" style="opacity: .5" title="Inlog geblokkeerd"><i class="fa fa-exclamation-triangle"></i> <i>' . $customer->getUsername() . '</i></span>';
                    }

                    return '';
                },
            ])
            ->add('email', EmailColumn::class, [
                'label' => 'E-mailadres',
            ])
            ->add('phoneNumber', PhoneNumberColumn::class, [
                'label' => 'Telefoonnummer',
            ])
            ->add('customerGroup', EntityColumn::class, [
                'label' => 'Klantgroep',
                'path' => 'admin_customer_customergroup_edit',
            ])
            ->add('lastLogin', TimeAgoColumn::class, [
                'label' => 'Laatst ingelogd',
                'empty_data' => '<i>nooit</i>',
            ])
            ->add('enabled', StatusColumn::class, [
                'label' => 'Status'
            ])
        ;
    }
}
