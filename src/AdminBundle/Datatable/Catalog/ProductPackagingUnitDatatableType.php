<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PackagingUnitDatatableType
 * @package AdminBundle\Datatable\Product
 */
class ProductPackagingUnitDatatableType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {

        $builder
            ->add('code', TextColumn::class, [
                'label' => 'GDSN code',
            ])
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ]);

    }

}
