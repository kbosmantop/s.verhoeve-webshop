<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Manager\StockManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductDatatableType
 * @package AdminBundle\Datatable\Catalog
 */
class ProductDatatableType extends ProductVariationDatatableType
{
    use ContainerAwareTrait;

    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        parent::buildDatatable($builder);

        $builder->remove('sku');

        $builder->add('hasVariationStock', CallbackColumn::class, [
            'label' => '<i style="font-size: 1.4em;" class="mdi mdi-buffer"></i>',
            'callback' => function ($value, Product $currentProduct) {
                $hasStock = $currentProduct->getHasStock();
                $inStockCount = 0;

                foreach ($currentProduct->getVariations() as $variation) {
                    if ($variation->getHasStock()) {
                        $inStockCount += $this->container->get(StockManager::class)->getPhysicalStock($variation);
                    }
                }

                $class = $inStockCount ? 'success' : 'danger';
                $label = $inStockCount ? 'Op voorraad (' . $inStockCount . ')' : 'Voorraadbeheer beschikbaar maar niet op voorraad';
                $html = sprintf('<span class="icon-primitive-dot column-accessible text-%s" data-popup="tooltip" data-html="true" title="" data-placement="bottom" data-original-title="%s"></span>',
                    $class, $label);

                return ((false === $hasStock) ? '' : $html);
            },
        ], 2);

    }
}
