<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\MediaColumn;
use AdminBundle\Components\Datatable\Column\PriceColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;

/**
 * Class ProductCardType
 * @package AdminBundle\Datatable\Catalog
 */
class ProductCardType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param DatatableBuilder $builder
     * @param array            $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        void($options);

        /** @var CacheManager */
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');

        $builder
            ->add('image', MediaColumn::class, [
                'label' => 'Afbeelding',
                'value' => function (ProductCard $product) use ($imagineCacheManager) {
                    if (!$product->getPath()) {
                        return null;
                    }

                    $image = $imagineCacheManager->getBrowserPath($product->getPath(), 'product_card');

                    if (!$image) {
                        return null;
                    }

                    return $image;
                },
                'width' => 200,
            ])
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('price', PriceColumn::class, [
                'label' => 'Prijs (incl. BTW)',
            ]);
    }
}
