<?php

namespace AdminBundle\Datatable\Catalog\Product;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\PriceColumn;
use AdminBundle\Components\Datatable\Column\TextareaColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;

/**
 * Class TransportTypeType
 * @package AdminBundle\Datatable\Catalog\Product
 */
class TransportTypeType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('title', TextColumn::class, [
                'label' => 'Titel',
            ])
            ->add('shortDescription', TextareaColumn::class, [
                'label' => 'Omschrijving',
            ])
            ->add('price_excl', PriceColumn::class, [
                'label' => 'Prijs (excl. BTW)',
                'withVat' => false,
                'width' => 140,
                'column' => 'price'
            ])
            ->add('price_incl', PriceColumn::class, [
                'label' => 'Prijs (incl. BTW)',
                'width' => 140,
                'column' => 'price'
            ]);
    }
}
