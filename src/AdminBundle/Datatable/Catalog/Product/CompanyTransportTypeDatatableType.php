<?php

namespace AdminBundle\Datatable\Catalog\Product;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\PriceColumn;
use Symfony\Component\Form\AbstractType;

/**
 * Class CompanyTransportTypeDatatableType
 * @package AdminBundle\Datatable\Catalog\Product
 */
class CompanyTransportTypeDatatableType extends AbstractType
{
    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('company', EntityColumn::class, [
                'label' => 'Bedrijf',
            ])
            ->add('productGroup', EntityColumn::class, [
                'label' => 'Productgroep',
            ])
            ->add('transportType', EntityColumn::class, [
                'label' => 'Bezorgproduct',
            ])
            ->add('price_excl', PriceColumn::class, [
                'label' => 'Prijs (excl. BTW)',
                'withVat' => false,
                'width' => 140,
                'column' => 'price',
            ]);
    }
}
