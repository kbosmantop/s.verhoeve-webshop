<?php

namespace AdminBundle\Datatable\Catalog\Product;

use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;

/**
 * Class ProductStickerDatatableType
 * @package AdminBundle\Datatable\Catalog\Product
 */
class ProductStickerDatatableType extends AbstractType
{
    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        $builder
            ->add('description', TextColumn::class, [
                'label' => 'Beschrijving',
            ]);
    }
}