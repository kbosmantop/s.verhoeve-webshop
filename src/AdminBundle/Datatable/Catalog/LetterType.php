<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\AccessibilityColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\MediaColumn;
use AdminBundle\Components\Datatable\Column\PriceColumn;
use AdminBundle\Components\Datatable\Column\TextareaColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use AppBundle\Decorator\Factory\ProductDecoratorFactory;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Form\AbstractType;

/**
 * Class LetterType
 * @package AdminBundle\Datatable\Catalog
 */
class LetterType extends AbstractType
{
    /** @var CacheManager */
    private $cacheManger;
    /** @var ProductDecoratorFactory */
    private $productFactory;

    /**
     * @param CacheManager            $cacheManger
     */
    public function __construct(CacheManager $cacheManger)
    {
        $this->cacheManger = $cacheManger;
    }

    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        $imagineCacheManager = $this->cacheManger;

        $builder
            ->add('image', MediaColumn::class, [
                'label' => 'Afbeelding',
                'value' => function (Product $product) use ($imagineCacheManager) {
                    if (!$product->getMainImage()) {
                        return null;
                    }

                    $image = $imagineCacheManager->getBrowserPath($product->getMainImage()->getPath(),
                        'product_assortment');

                    if (!$image) {
                        return null;
                    }

                    return $image;
                },
            ])
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('sku', TextColumn::class, [
                'label' => 'Sku',
            ])
            ->add('shortDescription', TextareaColumn::class, [
                'label' => 'Omschrijving',
            ])
            ->add('customergroups', EntityColumn::class, [
                'label' => 'Beschikbaar voor',
                'value' => function (CustomerGroup $customerGroup) {
                    return $customerGroup->getDescription();
                },
            ])
            ->add('price', PriceColumn::class, [
                'label' => 'Prijs (incl. BTW)',
            ])
            ->add('loginRequired', AccessibilityColumn::class, [
                'width' => 60,
            ]);
    }
}
