<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\AccessibilityColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Site\Site;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AssortmentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('title', TextColumn::class, [
                'label' => 'Titel',
            ])
            ->add('slug', TextColumn::class, [
                'label' => 'URL',
            ])
            ->add('sites', EntityColumn::class, [
                'label' => 'Sites',
                'path' => "admin_settings_site_edit",
                'value' => function (Site $site) {
                    return $site->translate()->getDescription();
                },
            ])
            ->add('loginRequired', AccessibilityColumn::class, [
                'width' => 60,
            ]);
    }
}
