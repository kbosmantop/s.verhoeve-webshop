<?php

namespace AdminBundle\Datatable\Supplier;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SupplierProductDatatableType
 * @package AdminBundle\Datatable\Supplier
 */
class SupplierProductDatatableType extends AbstractType
{

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('product', EntityColumn::class, [
                'label' => 'Product',
                'value' => function (Product $product) {
                    return $product->getName();
                }
            ])
            ->add('supplier', EntityColumn::class, [
                'label' => 'Leverancier'
            ])
            ->add('sku', TextColumn::class, [
                'label' => 'Sku'
            ])
            ->add('forwardPrice', TextColumn::class, [
                'label' => 'Doorstuurprijs'
            ]);
    }

}
