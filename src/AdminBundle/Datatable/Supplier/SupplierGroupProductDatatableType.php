<?php

namespace AdminBundle\Datatable\Supplier;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SupplierProductDatatableType
 * @package AdminBundle\Datatable\Supplier
 */
class SupplierGroupProductDatatableType extends AbstractType
{

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options): void
    {
        $builder
            ->add('product', EntityColumn::class, [
                'label' => 'Product',
                'value' => function (Product $product) {
                    return $product->getName();
                }
            ])
            ->add('supplierGroup', EntityColumn::class, [
                'label' => 'Leveranciersgroep'
            ])
            ->add('sku', TextColumn::class, [
                'label' => 'Sku'
            ])
            ->add('forwardPrice', TextColumn::class, [
                'label' => 'Doorstuurprijs'
            ]);
    }

}
