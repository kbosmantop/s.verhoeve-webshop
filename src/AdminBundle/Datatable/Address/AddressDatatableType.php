<?php

namespace AdminBundle\Datatable\Address;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Relation\Address;
use Symfony\Component\Form\AbstractType;

class AddressDatatableType extends AbstractType
{

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('street', TextColumn::class, [
                'label' => 'Straat',
            ])
            ->add('number', TextColumn::class, [
                'label' => 'Nummer',
            ])
            ->add('postcode', TextColumn::class, [
                'label' => 'Postcode',
            ])
            ->add('city', TextColumn::class, [
                'label' => 'Stad',
            ])
            ->add('country', EntityColumn::class, [
                'label' => 'Land',
            ])
            ->add('defaultDeliveryAddress', CallbackColumn::class, [
                'label' => 'Standaard bezorgadres',
                'callback' => function ($value, Address $address) {
                    if ((null !== $address->getCompany() && null !== $address->getCompany()->getDefaultDeliveryAddress() && $address->getCompany()->getDefaultDeliveryAddress()->getId() === $address->getId()) ||
                        (null !== $address->getCustomer() && null !== $address->getCustomer()->getDefaultDeliveryAddress() && $address->getCustomer()->getDefaultDeliveryAddress()->getId() === $address->getId())) {
                        $activeStatusLabel = 'Standaard';
                        $activeStatusClass = 'success';

                        $class = "label label-flat border-{$activeStatusClass} text-{$activeStatusClass}-600";
                        return '<span class="' . $class . '">' . $activeStatusLabel . '</span>';
                    }

                    return '';
                },
            ]);
    }

}
