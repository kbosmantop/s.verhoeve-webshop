<?php

namespace AdminBundle\Datatable\Role;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;

class RoleDatatableType
{

    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Rol',
            ])
            ->add('description', TextColumn::class, [
                'label' => 'Beschrijving',
            ])
            ->add('category', EntityColumn::class, [
                'label' => 'Categorie',
            ]);
    }
}
