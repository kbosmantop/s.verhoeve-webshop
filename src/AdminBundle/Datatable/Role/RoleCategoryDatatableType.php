<?php

namespace AdminBundle\Datatable\Role;

use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;

/**
 * Class RoleCategoryDatatableType
 * @package AdminBundle\Datatable\Role
 */
class RoleCategoryDatatableType
{
    /**
     * @param DatatableBuilder $builder
     * @param array             $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Categorie naam',
                'searchable' => true,
            ]);
    }
}
