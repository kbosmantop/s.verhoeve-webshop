<?php

namespace AdminBundle\Datatable\Discount;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Discount\Discount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyDiscountDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class CompanyDiscountDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('voucher', CallbackColumn::class, [
                'callback' => function($value, Discount $discount) {
                    if($discount->getVouchers()->isEmpty()) {
                        return 'Geen code beschikbaar';
                    }

                    return $discount->getVouchers()->current()->getCode();
                },
                'label' => 'Code'
            ])
            ->add('rule', EntityColumn::class, [
                'label' => 'Regel',
            ])
            ->add('value', CallbackColumn::class, [
                'callback' => function($value, Discount $discount) {
                    $text = $discount->getValue();

                    if($discount->getType() === 'percentage') {
                        $text.= '%';
                    } else {
                        $text .= ' euro';
                    }
                    return $text;
                },
                'label' => 'Waarde'
            ])
            ->add('validFrom', CallbackColumn::class, [
                'callback' => function($value, Discount $discount) {
                    if($discount->getVouchers()->isEmpty()) {
                        return 'Geen datum beschikbaar';
                    }

                    return $discount->getVouchers()->current()->getValidFrom()->format('d-m-Y');
                },
                'label' => 'Geldig vanaf'
            ])->add('validUntil', CallbackColumn::class, [
                'callback' => function($value, Discount $discount) {
                    if($discount->getVouchers()->isEmpty() || null === $discount->getVouchers()->current()->getValidUntil()) {
                        return 'Geen datum beschikbaar';
                    }

                    return $discount->getVouchers()->current()->getValidUntil()->format('d-m-Y');
                },
                'label' => 'Geldig tot'
            ]);
    }
}
