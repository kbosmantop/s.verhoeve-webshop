<?php

namespace AdminBundle\Datatable\Discount;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DiscountPropertyDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class DiscountPropertyDatatableType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('description', TextColumn::class, [
                'label' => 'Beschrijving'
            ])
            ->add('path', TextColumn::class, [
                'label' => 'Pad'
            ]);
    }

}