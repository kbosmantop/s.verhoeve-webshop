<?php

namespace AdminBundle\Datatable\Rule;

use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;

/**
 * Class RuleDatatableType
 * @package AdminBundle\Datatable\Rule
 */
class RuleDatatableType extends AbstractType
{
    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        $builder->add('description', TextColumn::class, [
            'label' => 'Omschrijving'
        ]);
    }
}