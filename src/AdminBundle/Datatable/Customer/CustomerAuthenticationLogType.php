<?php

namespace AdminBundle\Datatable\Customer;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\IpColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Security\Customer\AuthenticationLog;
use AppBundle\Services\Customer\AuthenticationLogger;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerAuthenticationLogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('createdAt', DateTimeColumn::class, [
                'label' => 'Datum',
                'datetype' => \IntlDateFormatter::MEDIUM,
                'timetype' => \IntlDateFormatter::SHORT,
            ])
            ->add('type', TextColumn::class, [
                'label' => 'Type',
            ])
            ->add('event', TextColumn::class, [
                'label' => 'Event',
            ])
            ->add('reason', CallbackColumn::class, [
                'label' => 'Reden',
                'callback' => function ($value, AuthenticationLog $entity) {
                    if ($entity->getEvent() != 'failure') {
                        return $value;
                    }

                    return AuthenticationLogger::getReasonDescription($value);
                },
            ])
            ->add('username', TextColumn::class, [
                'label' => 'Gebruikersnaam',
            ])
            ->add('employee', CallbackColumn::class, [
                'label' => 'Medewerker',
                'callback' => function ($value, AuthenticationLog $entity) {
                    void($value);

                    if (!$entity->getEmployee()) {
                        return '<em>n.v.t.</em>';
                    }


                    return $entity->getEmployee()->getDisplayName();
                },
            ])
            ->add('ip', IpColumn::class, [
                'label' => 'IP-adres',
            ])
            ->add('userAgent', TextColumn::class, [
                'label' => 'User Agent',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AuthenticationLog::class,
        ]);
    }
}
