<?php

namespace AdminBundle\Datatable\Finance;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\UserColumn;
use AppBundle\ThirdParty\Twinfield\Entity\Export;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ExportType
 * @package AdminBundle\Datatable\Finance
 */
class ExportType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * ExportType constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('invoiceDate', DateColumn::class, [
                'label' => 'Factuurdatum',
            ])
            ->add('companiesCount', CallbackColumn::class, [
                'label' => 'Debiteuren',
                'callback' => function ($value, Export $entity) {
                    return '<a href="' . $this->router->generate('admin_finance_export_downloadcompanies',
                            [
                                'export' => $entity->getId(),
                            ]) . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>&nbsp;&nbsp;&nbsp;' . $value . ' debiteur(en)';
                },
            ])
            ->add('ordersCount', CallbackColumn::class, [
                'label' => 'Bestellingen',
                'callback' => function ($value, Export $entity) {
                    return '<a href="' . $this->router->generate('admin_finance_export_downloadorders',
                            [
                                'export' => $entity->getId(),
                            ]) . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>&nbsp;&nbsp;&nbsp;' . $value . ' bestelling(en)';
                },
            ])
            ->add('createdBy', UserColumn::class, [
                'label' => 'Aangemaakt door',
            ])
            ->add('createdAt', DateTimeColumn::class, [
                'label' => 'Aangemaakt op',
            ]);
    }
}