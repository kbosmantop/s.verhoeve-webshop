<?php

namespace AdminBundle\Datatable\Finance;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\UserColumn;
use AppBundle\DBAL\Types\ExactInvoiceExportStatusType;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class InvoiceExportType
 * @package AdminBundle\Datatable\Finance
 */
class InvoiceExportType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExportType constructor.
     * @param RouterInterface        $router
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager)
    {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('invoiceDate', DateColumn::class, [
                'label' => 'Factuurdatum',
            ])
            ->add('companiesCount', CallbackColumn::class, [
                'label' => 'Debiteuren',
                'callback' => function ($value, InvoiceExport $invoiceExport) {
                    $downloadButton = '';
                    if($invoiceExport->getExportStatus() === ExactInvoiceExportStatusType::EXPORTED) {
                        $href = $this->router->generate('admin_finance_invoiceexport_downloadcompanies', [
                            'invoiceExport' => $invoiceExport->getId(),
                        ]);
                        $downloadButton = '&nbsp;&nbsp;&nbsp;<a href="' . $href . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>';
                    }

                    $deptorCount = count($this->entityManager->getRepository(Company::class)->findByInvoiceExport($invoiceExport));

                    return $deptorCount . ' debiteur(en)' . $downloadButton;
                },
            ])
            ->add('ordersCount', CallbackColumn::class, [
                'label' => 'Bestellingen',
                'callback' => function ($value, InvoiceExport $invoiceExport) {
                    $downloadButton = '';
                    if($invoiceExport->getExportStatus() === ExactInvoiceExportStatusType::EXPORTED) {
                        $href = $this->router->generate('admin_finance_invoiceexport_downloadorders', [
                            'invoiceExport' => $invoiceExport->getId(),
                        ]);
                        $downloadButton = '&nbsp;&nbsp;&nbsp;<a href="' . $href . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>';
                    }

                    $ordersCount = count($this->entityManager->getRepository(Invoice::class)->findByInvoiceExport($invoiceExport));

                    return $ordersCount . ' bestelling(en)' . $downloadButton;
                },
            ])
            ->add('exportStatus', CallbackColumn::class, [
                'label' => 'Status',
                'callback' => static function($value){
                    return ExactInvoiceExportStatusType::getReadableValue($value);
                },
            ])
            ->add('createdBy', UserColumn::class, [
                'label' => 'Aangemaakt door',
            ])
            ->add('createdAt', DateTimeColumn::class, [
                'label' => 'Aangemaakt op',
            ])
            ->add('actions', CallbackColumn::class, [
                'label' => '',
                'callback' => function ($value, InvoiceExport $invoiceExport) {
                    if($invoiceExport->getExportStatus() !== ExactInvoiceExportStatusType::NEW){
                        return '';
                    }
                    $companySelectUrl = $this->router->generate('admin_finance_invoiceexport_saveselectcompaniesforinvoice', [
                        'invoiceExport' => $invoiceExport->getId(),
                    ]);

                    $exportDeleteUrl = $this->router->generate('admin_finance_invoiceexport_delete', [
                        'id' => $invoiceExport->getId(),
                    ]);

                    return '<a style="color: rgba(0, 0, 0, .54); font-size: 18px;" href="' . $companySelectUrl . '"><i class="mdi mdi-pencil"></i>
                            <a data-type="delete" data-method="DELETE" style="color: rgba(0, 0, 0, .54); font-size: 18px;" href="' . $exportDeleteUrl . '"><i class="mdi mdi-delete"></i>';
                },
            ]);
    }
}
