<?php

namespace AdminBundle\Datatable\Finance\Vat;

use AdminBundle\Components\Datatable\Column\BooleanColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;

/**
 * Class VatRuleDatatableType
 * @package AdminBundle\Datatable\Finance\Vat
 */
class VatRuleDatatableType extends AbstractType
{
    /**
     * @param DatatableBuilder $builder
     * @param array                $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        $builder
            ->add('position', TextColumn::class, [
                'label' => 'Prioriteit',
            ])
            ->add('rule', EntityColumn::class, [
                'label' => 'Regel',
            ])
            ->add('allowSplit', BooleanColumn::class, [
                'label' => 'Mag splitsen',
            ]);
    }
}