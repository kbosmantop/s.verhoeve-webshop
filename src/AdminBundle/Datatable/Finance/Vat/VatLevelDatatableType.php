<?php

namespace AdminBundle\Datatable\Finance\Vat;

use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;

/**
 * Class VatLevelDatatableType
 * @package AdminBundle\Datatable\Finance\Vat
 */
class VatLevelDatatableType extends AbstractType
{
    /**
     * @param DatatableBuilder $builder
     * @param array                $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        $builder->add('title', TextColumn::class, [
            'label' => 'Naam'
        ]);
    }
}