<?php

namespace AdminBundle\Datatable\Seo;

use AdminBundle\Components\Datatable\Column\ChoiceColumn;
use AdminBundle\Components\Datatable\Column\CollectionColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;

class RedirectType extends AbstractType
{
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('status', ChoiceColumn::class, [
                'label' => 'Status',
                'choices' => [
                    301 => "301 Permanent verhuisd",
                    302 => "302 Gevonden",
                    307 => "307 Tijdelijke redirect",
                    410 => "410 Inhoud verwijderd",
                    451 => "451 Niet beschikbaar vanwege jurisiche redenen",
                ],
            ])
            ->add("oldUrl", TextColumn::class, [
                "label" => "Oude url",
            ])
            ->add("newUrl", TextColumn::class, [
                "label" => "Url",
            ])
            ->add("domains", CollectionColumn::class, [
                "label" => "Domeinen",
                "property" => "domain",
            ])
            ->add("createdAt", DateTimeColumn::class, [
                "label" => "Aangemaakt op",
                "datetype" => \IntlDateFormatter::FULL,
                "timetype" => \IntlDateFormatter::SHORT,
                "width" => 250,
            ]);
    }
}
