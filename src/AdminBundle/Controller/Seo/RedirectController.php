<?php

namespace AdminBundle\Controller\Seo;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Seo\RedirectType as DatatableRedirectType;
use AdminBundle\Form\Seo\RedirectType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Site\Redirect;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/seo/redirect")
 */
class RedirectController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'SEO Redirect';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Redirect::class,
            'form_type' => RedirectType::class,
            'datatable' => [
                'entity_class' => Redirect::class,
                'form_type' => RedirectType::class,
                'datatable_type' => DatatableRedirectType::class,
            ],
        ];
    }
}
