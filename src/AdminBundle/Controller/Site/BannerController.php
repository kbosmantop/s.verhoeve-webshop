<?php

namespace AdminBundle\Controller\Site;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Entity\Site\Banner;

/**
 * @Route("/site/banners")
 */
class BannerController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return mixed|null|string
     */
    public function getLabel($n = null)
    {
        return 'Banners';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Banner::class,
        ];
    }
}
