<?php

namespace AdminBundle\Controller\Site\Navigation;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Site\Navigation\MenuType as MenuDatatableType;
use AdminBundle\Form\Site\Navigation\MenuType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Site\Menu;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/site/navigatie/menu")
 */
class MenuController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return "Menu's";
    }

    public function getOptions()
    {
        return [
            'form_type' => MenuType::class,
            'entity_class' => Menu::class,
            'datatable' => [
                'datatable_type' => MenuDatatableType::class,
                'form_type' => MenuType::class,
                'entity_class' => Menu::class,
            ],
        ];
    }
}
