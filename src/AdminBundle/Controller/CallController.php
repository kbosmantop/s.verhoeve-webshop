<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RuleController
 * @package AppBundle\Controller
 * @Route("/call")
 */
class CallController extends Controller
{
    /**
     * @Route("/{number}")
     * @param string $number
     * @return Response
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function number(string $number): ?Response
    {
        if (!$this->getUser() || !$this->getUser() instanceof User) {
            throw new AccessDeniedHttpException();
        }

        $extension = $this->getUser()->getVoipExtension();

        try {
            if ($extension === null) {
                throw new BadRequestHttpException('Please provide voip extension for user.');
            }

            $this->get('voicedata')->outgoingCall($extension, $number);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }

        return new Response('OK');
    }
}
