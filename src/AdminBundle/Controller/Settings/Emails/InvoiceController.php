<?php

namespace AdminBundle\Controller\Settings\Emails;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Mailer\InvoiceMailer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/instellingen/emails/voorbeeld/factuur")
 */
class InvoiceController extends Controller
{
    /**
     * @Route("/{orderCollection}");
     * @param OrderCollection $orderCollection
     * @return Response
     * @throws \Exception
     */
    public function preview(OrderCollection $orderCollection)
    {
        $data = null;
        $this->get(InvoiceMailer::class)->send([
            'orderCollection' => $orderCollection,
            'to' => 'tester@topgeschenken.nl'
        ], function (\Swift_Message $message) use (&$data) {
            $data = $message->getBody();
            return false;
        });

        return new Response($data);
    }
}
