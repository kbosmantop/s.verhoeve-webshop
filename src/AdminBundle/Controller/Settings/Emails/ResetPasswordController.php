<?php

namespace AdminBundle\Controller\Settings\Emails;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Services\Mailer;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\Mailer\PasswordResetMailer;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/instellingen/emails/voorbeeld/wachtwoord-resetten")
 */
class ResetPasswordController extends Controller
{
    /**
     * @Route("/");
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function preview(Request $request)
    {
        /* Todo: SF4 */
        /** @var $tokenGenerator TokenGeneratorInterface */
        $tokenGenerator = $this->get('fos_user.util.token_generator');

        $siteId = (int)$request->get('site', 1);
        $customerId = (int)$request->get('customer', 1);

        $em = $this->get('doctrine')->getManager();

        $site = $em->find(Site::class, $siteId);
        $customer = $em->find(Customer::class, $customerId);

        if (null === $site) {
            throw new \RuntimeException('Site not found.');
        }

        if (null === $customer) {
            throw new \RuntimeException('Customer not found.');
        }

        if (null === $customer->getConfirmationToken()) {
            $customer->setConfirmationToken($tokenGenerator->generateToken());

            $em->flush();
        }

        $data = null;
        $this->get(PasswordResetMailer::class)->send([
            'site' => $site,
            'customer' => $customer
        ], function (\Swift_Message $message) use (&$data) {
            $data = $message->getBody();
            return false;
        });

        return new Response($data);
    }
}
