<?php

namespace AdminBundle\Controller\Settings\Emails;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Services\Mailer\PaymentRequestMailer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/instellingen/emails/voorbeeld/betaalverzoek")
 */
class PaymentRequestController extends Controller
{
    /**
     * @Route("/{payment}", methods={"GET"})
     *
     * @param Payment $payment
     * @return Response
     * @throws \Exception
     */
    public function previewAction(Payment $payment): ?Response
    {
        $data = null;
        $this->get(PaymentRequestMailer::class)->send(
            [
                'payment' => $payment,
            ],
            function (\Swift_Message $message) use (&$data) {
                $data = $message->getBody();
                return false;
            }
        );

        return new Response($data);
    }
}
