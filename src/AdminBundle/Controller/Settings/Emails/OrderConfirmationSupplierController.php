<?php

namespace AdminBundle\Controller\Settings\Emails;

use AppBundle\Connector\AbstractMailConnector;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Order\Order;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Services\ConnectorHelper;

/**
 * @Route("/instellingen/emails/voorbeeld/order-bevestiging-leverancier")
 */
class OrderConfirmationSupplierController extends Controller
{
    /**
     * @Route("/{supplier}/{order}");
     *
     * @param Company         $supplier
     * @param Order           $order
     * @param ConnectorHelper $connectorHelper
     * @return Response
     *
     * @throws \Twig_Error
     * @throws \ReflectionException
     */
    public function previewAction(Company $supplier, Order $order, ConnectorHelper $connectorHelper)
    {
        /** @var AbstractMailConnector $connector */
        $connector = $connectorHelper->getConnector($supplier);

        if (!method_exists($connector, 'preview')) {
            return new Response(sprintf('Preview method not found for connector %s.', $connector->getName()), 422);
        }

        return new Response($connector->preview($order->getSupplierOrder()));
    }
}
