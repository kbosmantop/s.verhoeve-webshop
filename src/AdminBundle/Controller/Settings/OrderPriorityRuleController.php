<?php

namespace AdminBundle\Controller\Settings;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\OrderPriorityRuleDatatable;
use AdminBundle\Entity\Settings\OrderPriorityRule;
use AdminBundle\Form\Settings\OrderPriorityRuleType;
use AppBundle\Entity\Order\Order;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderPriorityRuleController
 * @package AppBundle\Controller
 * @Route("/instellingen/prioriteitsbestellingen")
 */
class OrderPriorityRuleController extends CrudController
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Prioriteitsregels';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => OrderPriorityRule::class,
            'form_type' => OrderPriorityRuleType::class,
            'datatable' => [
                'entity_class' => OrderPriorityRule::class,
                'form_type' => OrderPriorityRuleType::class,
                'datatable_type' => OrderPriorityRuleDatatable::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_settings_orderpriorityrule_wizard',
                        'label' => 'Toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                        'identifier' => 'rule.id',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/wizard", methods={"GET"})
     * @Template()
     *
     * @return array
     * @throws \Exception
     */
    public function wizardAction()
    {
        return [];
    }

    /**
     * @Route("/aanmaken", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function createRuleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ruleTransformer = $this->get('rule.transformer');

        if ($request->isMethod(Request::METHOD_POST)) {
            return $this->store($request);
        }

        $entities = $em->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
            'fullyQualifiedName' => Order::class,
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => Order::class,
            ]),
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => $entities,
        ];
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    private function store(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rule = $this->container->get('rule')->store($request, true);

        $orderPriorityRule = new OrderPriorityRule();
        $orderPriorityRule->setRule($rule);

        $em->persist($orderPriorityRule);
        $em->flush();

        return $this->redirectToRoute('admin_settings_orderpriorityrule_edit', ['id' => $orderPriorityRule->getId()]);
    }
}
