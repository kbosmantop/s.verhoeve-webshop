<?php

namespace AdminBundle\Controller\Settings;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\PaymentmethodType as DatatablePaymentMethodType;
use AdminBundle\Form\Settings\PaymentmethodType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Payment\Paymentmethod;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/betaalmethoden")
 */
class PaymentmethodController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Betaalmethoden';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Paymentmethod::class,
            'form_type' => PaymentmethodType::class,
            'datatable' => [
                'entity_class' => Paymentmethod::class,
                'form_type' => PaymentmethodType::class,
                'datatable_type' => DatatablePaymentMethodType::class,
            ],
        ];
    }
}
