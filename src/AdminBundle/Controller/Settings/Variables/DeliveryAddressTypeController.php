<?php

namespace AdminBundle\Controller\Settings\Variables;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Variables\DeliveryAddressTypeType as DeliveryAddressDatatableType;
use AdminBundle\Form\Settings\Variables\DeliveryAddressTypeType as DeliveryAddressFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Geography\DeliveryAddressType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/variabelen/type-bezorgadres")
 *
 */
class DeliveryAddressTypeController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Type bezorgadres';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => DeliveryAddressType::class,
            'form_type' => DeliveryAddressFormType::class,
            'datatable' => [
                'entity_class' => DeliveryAddressType::class,
                'form_type' => DeliveryAddressFormType::class,
                'datatable_type' => DeliveryAddressDatatableType::class,
            ],
        ];
    }

}
