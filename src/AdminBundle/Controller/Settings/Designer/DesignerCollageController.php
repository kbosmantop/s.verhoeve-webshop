<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Designer\DesignerCollageDatatableType;
use AdminBundle\Form\Settings\Designer\DesignerCollageFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\DesignerCollage;
use Symfony\Component\Routing\Annotation\Route;
use AdminBundle\Datatable\Settings\Designer\DesignerFontDatatableType;
use AdminBundle\Form\Settings\Designer\DesignerFontFormType;
use AppBundle\Entity\Designer\DesignerFont;

/**
 * @Route("/designs/collages")
 *
 */
class DesignerCollageController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Collages';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Collages',
            'title' => function (DesignerCollage $designerCollage) {
                return $designerCollage->getName();
            },
            'entity_class' => DesignerCollage::class,
            'form_type' => DesignerCollageFormType::class,
            'datatable' => [
                'entity_class' => DesignerCollage::class,
                'form_type' => DesignerCollageFormType::class,
                'datatable_type' => DesignerCollageDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true
                    ]
                ],
            ]
        ];
    }
}
