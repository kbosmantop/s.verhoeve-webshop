<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Designer\LayoutType as DesignerLayoutType;
use AdminBundle\Form\Settings\Designer\LayoutType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\DesignerLayout;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/designer/formaten")
 *
 */
class LayoutController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Formaten';
    }

    public function getOptions()
    {
        return [
            'entity_class' => DesignerLayout::class,
            'form_type' => LayoutType::class,
            'datatable' => [
                'entity_class' => DesignerLayout::class,
                'form_type' => LayoutType::class,
                'datatable_type' => DesignerLayoutType::class,
            ],
        ];
    }

}
