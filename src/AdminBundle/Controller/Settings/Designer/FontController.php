<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Designer\FontType as DatatableFontType;
use AdminBundle\Form\Settings\Designer\FontType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\DesignerFont;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/designer/lettertypen")
 *
 */
class FontController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Fonts';
    }

    public function getOptions()
    {
        return [
            'entity_class' => DesignerFont::class,
            'form_type' => FontType::class,
            'datatable' => [
                'entity_class' => DesignerFont::class,
                'form_type' => FontType::class,
                'datatable_type' => DatatableFontType::class,
            ],
        ];
    }

}
