<?php

namespace AdminBundle\Controller\Settings;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\WmsOrderTypeDatatable;
use AdminBundle\Entity\Settings\WmsOrderType;
use AdminBundle\Form\Settings\WmsOrderTypeType;
use AppBundle\Entity\Order\Order;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\JobQueueBundle\Entity\Job;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WmsOrderTypeController
 * @package AppBundle\Controller
 * @Route("/instellingen/wms")
 */
class WmsOrderTypeController extends CrudController
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'WMS';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => WmsOrderType::class,
            'form_type' => WmsOrderTypeType::class,
            'datatable' => [
                'entity_class' => WmsOrderType::class,
                'form_type' => WmsOrderTypeType::class,
                'datatable_type' => WmsOrderTypeDatatable::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_settings_wmsordertype_wizard',
                        'label' => 'Toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                        'identifier' => 'rule.id',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/wizard", methods={"GET"})
     * @Template()
     *
     * @return array
     * @throws \Exception
     */
    public function wizardAction()
    {
        $em = $this->container->get('doctrine')->getManager();
        $entities = $em->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
            'fullyQualifiedName' => Order::class,
        ]);

        //generate entities when entities do not exist yet
        if (empty($entities)) {
            $jobManager = $this->container->get('job.manager');

            $job = new Job('topgeschenken:entities:generate');
            $jobManager->addJob($job);
            $this->container->get('doctrine')->getManager()->flush();

            return [
                'no_entities' => true,
            ];
        }

        return [];
    }

    /**
     * @Route("/aanmaken", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function createRuleAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getManager();

        if ($request->isMethod(Request::METHOD_POST)) {
            return $this->store($request, $em);
        }

        $EntityRepository = $em->getRepository(Entity::class);

        /** @var Entity $entities */
        $entities = $EntityRepository->findBy([
            'ruleStartPoint' => 1,
            'fullyQualifiedName' => Order::class,
        ]);

        return [
            'filters' => $this->get('rule.transformer')->getFilters([
                ResolverService::QB_ALIAS => $EntityRepository->findOneBy([
                    'fullyQualifiedName' => Order::class,
                ]),
            ]),
            'entities' => $entities,
        ];
    }

    /**
     * @param Request       $request
     * @param ObjectManager $em
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    private function store(Request $request, ObjectManager $em)
    {
        /** @var Rule $rule */
        $rule = $this->container->get('rule')->store($request, true);

        $wmsOrderType = new WmsOrderType();
        $wmsOrderType->setRule($rule);

        $em->persist($wmsOrderType);
        $em->flush();

        return $this->redirectToRoute('admin_settings_wmsordertype_edit', ['id' => $wmsOrderType->getId()]);
    }
}
