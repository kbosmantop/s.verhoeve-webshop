<?php

namespace AdminBundle\Controller\Settings;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\KiyohCompanyType as KiyohCompanyDatatableType;
use AdminBundle\Form\Settings\KiyohCompanyType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/kiyoh")
 */
class KiyohController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Kiyoh';
    }

    public function getOptions()
    {
        return [
            'entity_class' => KiyohCompany::class,
            'form_type' => KiyohCompanyType::class,
            'datatable' => [
                'entity_class' => KiyohCompany::class,
                'form_type' => KiyohCompanyType::class,
                'datatable_type' => KiyohCompanyDatatableType::class,
            ],
        ];
    }
}
