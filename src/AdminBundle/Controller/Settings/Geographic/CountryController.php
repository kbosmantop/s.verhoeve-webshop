<?php

namespace AdminBundle\Controller\Settings\Geographic;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Geographic\CountryType as CountryDatatableType;
use AdminBundle\Form\Settings\Geographic\CountryType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Geography\Country;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/geografisch/landen")
 */
class CountryController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Landen';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Country::class,
            'form_type' => CountryType::class,
            'datatable' => [
                'entity_class' => Country::class,
                'datatable_type' => CountryDatatableType::class,
                'form_type' => CountryType::class,
            ],
            'sidebar_menu' => 'AdminBundle:Geographic:menu',
        ];
    }
}
