<?php namespace AdminBundle\Controller\Admin;

use AdminBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RefactorController
 * @Route("/refactor")
 */
class RefactorController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        //
    }

    /**
     * @Route("/execute/{type}")
     * @param null $type
     * @return JsonResponse
     * @throws \Exception
     */
    public function execAction($type = null)
    {
        switch ($type) {
            case 'duplicate':
                $cmd = 'cd ./../bin && php phpcpd.phar ./../src';
                break;
            case 'cleancode':
            case 'codesize':
            case 'controversial':
            case 'design':
            case 'naming':
            case 'unusedcode':
                $cmd = 'cd ./../bin && php phpmd.phar ./../src text ' . $type;
                break;
            default:
                throw new \Exception('Invalid type ' . $type);
        }

        exec($cmd, $output);

        return new JsonResponse($output, 200);
    }
}
