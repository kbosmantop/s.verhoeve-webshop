<?php

namespace AdminBundle\Controller\Relation;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Address\AddressDatatableType;
use AdminBundle\Form\Relation\AddressFormType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Utils\Serializer\EntitySerializer;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AddressController
 * @package AppBundle\Controller\Geography
 * @Route("/adressen")
 */
class AddressController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Address::class,
            'form_type' => AddressFormType::class,
            'datatable' => [
                'datatable_type' => AddressDatatableType::class,
                'entity_class' => Address::class,
                'form_type' => AddressFormType::class,
            ],
        ];
    }

    /**
     * @Route("/ophalen/{address}", methods={"GET"})
     *
     * @ParamConverter("address", options={"mapping": { "address" = "uuid"}})
     * @param $address
     * @return JsonResponse
     */
    public function getAddress(Address $address)
    {
        $serializer = new EntitySerializer([
            Address::class => [
                'id',
                'uuid' => ['type' => 'uuid'],
                'company',
                'point' => ['type' => 'point'],
            ],
            Company::class => [
                'id',
                'name'
            ],
            Point::class => [
                'latitude' => ['keyName' => 'lat'],
                'longitude' => ['keyName' => 'lng']
            ],
        ]);

        return new JsonResponse($serializer->normalize($address));
    }
}
