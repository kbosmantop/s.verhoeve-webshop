<?php

namespace AdminBundle\Controller\Firebase;

use AppBundle\ThirdParty\Firebase\Entity\FirebaseToken;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/firebase")
 */
class IndexController extends Controller
{
    /**
     * @Route
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $token = $request->get("token");

        if (!$token) {
            return new JsonResponse(null, 500);
        }

        $entity = $this->getDoctrine()->getManager()->getRepository(FirebaseToken::class)->find($token);

        if (!$entity) {
            $entity = new FirebaseToken();
            $entity->setToken($token);

            $this->getDoctrine()->getManager()->persist($entity);
        } else {
            $entity->setUpdatedAt(new \DateTime());
        }

        $entity->setUser($this->getUser());

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse();
    }
}
