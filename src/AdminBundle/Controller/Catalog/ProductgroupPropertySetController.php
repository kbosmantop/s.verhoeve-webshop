<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductGroupPropertySetDatatableType;
use AdminBundle\Form\Catalog\Product\ProductgroupPropertySetFormType;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten-kenmerken/sets")
 * Class ProductgroupPropertySetController
 * @package AdminBundle\Controller\Catalog
 */
class ProductgroupPropertySetController extends CrudController
{

    public function getOptions()
    {
        return [
            'entity_class' => ProductgroupPropertySet::class,
            'form_type' => ProductgroupPropertySetFormType::class,
            'datatable' => [
                'entity_class' => ProductgroupPropertySet::class,
                'form_type' => ProductgroupPropertySetFormType::class,
                'datatable_type' => ProductGroupPropertySetDatatableType::class,
            ],
        ];
    }

}
