<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Datatable\Catalog\ProductVariationDatatableType;
use AdminBundle\Form\Catalog\Product\ProductCombinationFormType;
use AdminBundle\Form\Catalog\Product\ProductVariationFormType;
use AdminBundle\Form\Catalog\ProductType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AdminBundle\Services\Product\ProductWizardService;
use AppBundle\DBAL\Types\ProductTypeType;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use AppBundle\Interfaces\Preview\PreviewableControllerInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use AppBundle\Model\Product\ProductModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\SupplierManagerService;

/**
 * @Route("/catalogus/producten")
 * Class ProductController
 * @package AdminBundle\Controller\Catalog
 */
class ProductController extends CrudController implements CrudControllerInterface, PreviewableControllerInterface, PageViewControllerInterface
{
    protected $tableOptions = [
        'datatable_type' => ProductDatatableType::class,
        'entity_class' => GenericProduct::class,
        'form_type' => ProductVariationFormType::class,
        'results_per_page' => 5,
        'parent_column' => 'parent',
        'allow_add' => false,
        'allow_read' => true,
        'base_route' => 'admin_catalog_productvariation',
    ];

    /**
     * @param null $n
     *
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Producten';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Product',
            'entity_class' => GenericProduct::class,
            'form_type' => ProductType::class,
            'assets' => [
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/product.js',
                'admin/css/product/product-item.css',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/form/product.css',
                'admin/css/designer/DesignSelectModal.css',
                'admin/js/form/assortment.js',
                'admin/css/form/assortment.css',
            ],
            'title' => function (GenericProduct $product) {
                return $product->getName();
            },
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => GenericProduct::class,
                'form_type' => ProductType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_catalog_product_wizard',
                        'label' => 'Product toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-table-search',
                        'type' => 'preview',
                        'label' => 'Voorbeeld',
                        'url' => function (GenericProduct $product) {
                            return $this->generateUrl('admin_catalog_product_preview', [
                                'entity' => $product->getId(),
                            ]);
                        },
                    ],
                ],
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->andWhere('product.parent IS NULL');
                },
                'allow_read' => true,
                'allow_edit' => false,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'order' => [
                    [2, 'asc'],
                ],
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description_panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/description-panel.html.twig',
                    ],
                    [
                        'id' => 'variation-panel',
                        'title' => 'Variaties',
                        'type' => 'datatable',
                        'datatable' => [
                            'datatable_type' => ProductVariationDatatableType::class,
                            'entity_class' => GenericProduct::class,
                            'form_type' => ProductVariationFormType::class,
                            'results_per_page' => 5,
                            'parent_column' => 'parent',
                            'base_route' => 'admin_catalog_productvariation',
                            'allow_read' => false,
                            'allow_reorder' => true,
                            'item_actions' => [
                                [
                                    'icon' => 'mdi mdi-truck-fast',
                                    'route' => 'admin_supplier_supplierproduct_supplier',
                                    'type' => 'open-content',
                                    'method' => 'GET',
                                ],
                            ],
                            'refresh_panel' => 'variation-panel',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/{id}/bekijken", requirements={"id"="\d+"}, methods={"GET"})
     * @Breadcrumb()
     * @param $id
     *
     * @throws \Exception
     * @throws \Twig_Error
     * @return Response
     */
    public function viewAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('read'));

        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository(GenericProduct::class)->find($id);

        if (!$product) {
            throw new \RuntimeException("Can't find product");
        }

        if (!$product->isCombination()) {
            return parent::viewAction($id);
        }

        $templates = [
            '@Admin/Product/description-panel.html.twig',
            '@Admin/Product/combination-products.html.twig',
            '@Admin/Product/combination-variations.html.twig',
        ];

        $this->tableOptions['actions'] = [
            [
                'icon' => 'mdi mdi-plus',
                'route' => 'admin_catalog_productcombination_add',
                'route_parameters' => [
                    'combinationProduct' => $product->getId(),
                ],
                'label' => 'Variatie toevoegen',
                'className' => 'btn-success',
            ],
        ];

        $datatable = $this->container->get('admin.datatable.service')->getTable($this->tableOptions);

        return $this->render(
            '@Admin/Product/combination-view.html.twig',
            [
                'templates' => $templates,
                'genericProduct' => $product,
                'assets' => $this->getAssets(),
                'datatable' => $datatable,
            ]
        );
    }

    /**
     * @Route("/{id}/data", requirements={"id"="\d+"}, methods={"GET", "POST"})
     *
     * @param GenericProduct $product
     * @return JsonResponse
     * @throws \Exception
     */
    public function tableDataAction(GenericProduct $product)
    {
        $this->tableOptions['parent']['entity'] = $this->getEntityName(\get_class($product));
        $this->tableOptions['parent']['parentId'] = $product->getId();

        $dataTable = $this->container->get('admin.datatable.service')->getData($this->tableOptions);

        return $dataTable;
    }

    /**
     * @Route("/preview/{entity}/sites", methods={"GET"})
     * @ParamConverter("entity", class="AppBundle\Entity\Catalog\Product\Product")
     *
     * @param PreviewableEntityInterface|Product $entity
     * @param Request                            $request
     *
     * @return JsonResponse
     */
    public function previewableSitesAction(PreviewableEntityInterface $entity, Request $request)
    {
        $sites = new ArrayCollection();
        $results = [];

        foreach ($entity->getAssortments() as $assortment) {
            foreach ($assortment->getSites() as $site) {
                if ($sites->contains($site) || $site->getParent() !== null) {
                    continue;
                }

                $sites->add($site);
            }
        }

        foreach ($sites as $site) {
            $results[$site->getId()] = $site->translate()->getDescription();
        }

        return new JsonResponse([
            'sites' => $results,
        ]);
    }

    /**
     * @Route("/preview/{entity}/{site}", methods={"GET"})
     * @ParamConverter("entity", class="AppBundle\Entity\Catalog\Product\Product")
     *
     * @param PreviewableEntityInterface|Product $entity
     * @param Site|null                          $site
     * @param Request                            $request
     *
     * @return RedirectResponse
     */
    public function previewAction(PreviewableEntityInterface $entity, Site $site = null, Request $request)
    {
        if (null === $site) {
            throw new \RuntimeException('No site found');
        }

        $values = [
            'class' => \get_class($entity),
            'identifier' => $entity->getId(),
            'siteId' => $site->getId(),
            'admin_url' => $request->query->get('return_uri', null),
        ];

        $url = $this->get('preview')->getRequestUrl($site->getPrimaryDomain()->getUri(), $values);

        return new RedirectResponse($url);
    }

    /**
     * @Route("/wizard", methods={"GET"})
     * @Template()
     *
     * @return array
     */
    public function wizardAction()
    {
        $doctrine = $this->getDoctrine();
        $productTypes = ProductTypeType::getChoices();
        $productSets = $doctrine->getRepository(ProductgroupPropertySet::class)->findAll();
        $productGroups = $doctrine->getRepository(Productgroup::class)->findBy([], ['title' => 'ASC']);
        $products = $doctrine->getRepository(GenericProduct::class)->findBy(['publish' => true]);

        return [
            'productTypes' => $productTypes,
            'productSets' => $productSets,
            'productGroups' => $productGroups,
            'products' => $products,
        ];
    }

    /**
     * @Route("/wizard", methods={"POST"})
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function persistWizardAction(Request $request)
    {
        $product = $this->container->get(ProductWizardService::class)->processWizard($request);
        $this->container->get('doctrine')->getManager()->flush();

        $url = $this->container->get('router')->generate($this->getRoute('view'), ['id' => $product->getId()]);

        return $this->redirect($url);
    }

    /**
     * @Route("/beschikbare-sets", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function productsetsByGroupAction(Request $request)
    {
        $sets = $this->container->get(ProductWizardService::class)->getPropertySetsByProductgroup($request->get('productgroup'));

        $productSets = [];
        foreach ($sets as $set) {
            $productSets[] = ['id' => $set->getId(), 'name' => $set->getName()];
        }

        return new JsonResponse(['sets' => $productSets]);
    }

    /**
     * @Route("/{product}/combinatie-bewerken", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form_xhr.html.twig")
     * @param GenericProduct $product
     * @param Request        $request
     *
     * @return array
     */
    public function editCombinationProduct(GenericProduct $product, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProductCombinationFormType::class, $product, [
            'action' => $this->generateUrl('admin_catalog_product_editcombinationproduct', [
                'product' => $product->getId(),
            ]),
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
        }
        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/zoeken", methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     *
     * @return array|JsonResponse
     * @throws Exception
     */
    public function searchProductsAction(Request $request)
    {
        $wizard = $this->container->get(ProductWizardService::class);
        if ($request->isMethod('POST')) {
            $products = $wizard->getProducts($request);
            $personalizations = $wizard->getPersonalizations($request);

            return new JsonResponse(array_merge($personalizations, $products));
        }

        return [];
    }

    /**
     * @Route("/combinatie-producten-zoeken", methods={"GET"})
     * @Template()
     *
     * @return array
     */
    public function searchCombinationProductsAction()
    {
        return [];
    }

    /**
     * @Route("/check-combinatie", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkCombinationAction(Request $request)
    {
        $supplierManager = $this->container->get(SupplierManagerService::class);

        $repo = $this->getDoctrine()->getRepository(Product::class);

        /** @var Product[] $products */
        $products = $repo->findBy([
            'id' => $request->get('products'),
        ]);

        /** @var Product $checkProduct */
        $checkProduct = $repo->find($request->get('product'));
        foreach ($products as $product) {
            if (!$supplierManager->canCombineProductWith($product, $checkProduct)) {
                return new JsonResponse(false, 404);
            }

            if ($checkProduct instanceof Personalization && $product instanceof Personalization) {
                return new JsonResponse('Er is al een personalisatie aanwezig!', 406);
            }
        }

        return new JsonResponse(true);
    }

    /**
     * @Route("/{product}/{personalization}/selecteer-design", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param ProductInterface $product
     * @param Personalization $personalization
     *
     * @return array
     * @throws Exception
     */
    public function selectCombinationDesign(
        Request $request,
        ProductInterface $product,
        Personalization $personalization
    ) {
        $searchKey = $request->get('searchKey');
        $companyId = $request->get('company');

        $dimensions = $personalization->getDesignDimensions();

        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $er */
        $er = $em->getRepository(DesignerTemplate::class);

        $qb = $er->createQueryBuilder('template');
        $qb->leftJoin('template.canvas', 'canvas');

        $qb->andWhere('canvas.width = :width');
        $qb->setParameter('width', $dimensions->percWidth);

        $qb->andWhere('canvas.height = :height');
        $qb->setParameter('height', $dimensions->percHeight);

        if($companyId) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('template.company'),
                $qb->expr()->eq('template.company', ':company')
            ))
            ->setParameter('company', $companyId);
        } else {
            $qb->andWhere('template.company IS NULL');
        }

        if ($searchKey) {
            $qb->andWhere('template.name LIKE :searchKey');
            $qb->setParameter('searchKey', '%' . $searchKey . '%');
        }

        $designs = $qb->getQuery()->getResult();

        /** @var ProductModel $product */
        return [
            'searchKey' => $request->get('searchKey'),
            'designs' => $designs,
            'product' => $product,
            'personalization' => $personalization,
        ];
    }

    /**
     * @Route("/variatie-ophalen", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function selectVariationAction(Request $request)
    {
        $productId = $request->get('product');

        /** @var Product $product */
        $product = $this->container->get('doctrine')->getRepository(Product::class)->find($productId);

        $variations = $this->container->get(ProductWizardService::class)->getVariations($product);

        return new JsonResponse($variations);
    }

    /**
     * @return array
     */
    public function getPageviewSettings()
    {
        return [
            'route' => 'admin_catalog_product_view',
            'entity' => GenericProduct::class,
            'param' => 'id',
        ];
    }
}
