<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/productgroupkenmerken")
 */
class ProductgroupPropertyController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Productgroep eigenschappen';
    }

    public function getOptions()
    {
        return [
            'entity_class' => 'AppBundle\Entity\Catalog\Product\ProductgroupProperty',
            'label' => 'Productkenmerk',
            'form_type' => 'AdminBundle\Form\Catalog\ProductgroupPropertyType',
            'assets' => [
                'admin/js/form/product-property.js',
            ],
            'datatable' => [
                'entity_class' => 'AppBundle\Entity\Catalog\Product\ProductgroupProperty',
                'datatable_type' => 'AdminBundle\Datatable\Catalog\ProductgroupPropertyType',
                'form_type' => 'AdminBundle\Form\Catalog\ProductgroupPropertyType',
            ],
        ];
    }

    /**
     * @Route("/is-uniek", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uniqueValueAction(Request $request)
    {
        if ($request->get('productgroupPropertyId')) {
            /**
             * @var QueryBuilder $qb
             */
            $qb = $this->getDoctrine()->getRepository(ProductProperty::class)->createQueryBuilder('pp')
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->eq('pp.productgroupPropertyOption', ':value'),
                    $qb->expr()->eq('pp.value', ':value')
                ))
                ->andWhere('pp.productgroupProperty = :productgroupPropertyId')
                ->setParameter('productgroupPropertyId', $request->get('productgroupPropertyId'))
                ->setParameter('value', $request->get('value'));

            if (count($qb->getQuery()->getResult())) {
                return new JsonResponse(['unique' => false]);
            }
        }

        return new JsonResponse(['unique' => true]);
    }
}
