<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Form\Catalog\Product\PersonalizationVariationFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\Personalization;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten/personalisatie-variaties")
 *
 */
class PersonalizationVariationController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Personalisatie variaties';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Product',
            'entity_class' => Personalization::class,
            'form_type' => PersonalizationVariationFormType::class,
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => Personalization::class,
                'form_type' => PersonalizationVariationFormType::class,
                'allow_add' => false
            ],
        ];
    }
}
