<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Manager\Catalog\Product\ProductCombinationManager;
use function count;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/product-combinaties")
 * Class ProductCombinationController
 * @package AdminBundle\Controller\Catalog
 */
class ProductCombinationController extends Controller
{
    /**
     * @Route("/{combinationProduct}/toevoegen", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Product $combinationProduct
     * @return array|RedirectResponse
     */
    public function add(Request $request, Product $combinationProduct)
    {
        $errors = [];
        if ($request->isMethod('post')) {
            $products = json_decode($request->get('products'), true);

            if (empty($products) || count($products) < 1) {
                $errors[] = 'Minimaal 1 product is vereist.';
            }

            if (empty($errors)) {
                $this->get('admin.product_combination')->addVariation($request, $combinationProduct);

                return $this->redirectToRoute(
                    'admin_catalog_product_view', ['id' => $combinationProduct->getId()]
                );
            }
        }

        $availableProducts = new ArrayCollection();

        /** @var Combination $combination */
        foreach ($combinationProduct->getCombinations() as $combination) {
            $availableProducts->add($combination->getProduct());
        }

        return [
            'errors' => $errors,
            'product' => $combinationProduct,
            'availableProducts' => $availableProducts
        ];
    }

    /**
     * @Route("/{combination}/{product}/verwijderen", methods={"DELETE"})
     * @param Combination $combination
     * @param Product     $product
     *
     * @return JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws ReflectionException
     */
    public function removeProductAction(Combination $combination, Product $product): JsonResponse
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('delete'));

        $productCombinationManager = $this->get(ProductCombinationManager::class);
        $removeProductConditions = $productCombinationManager->removeProductConditions($combination, $product);

        if (! empty($removeProductConditions)) {
            return $this->json( $removeProductConditions, 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($combination);
        $entityManager->flush();

        return $this->json([
            'success' => 'Het product is verwijderd uit de combinatie'
        ]);
    }

    /**
     * @Route("/aantallen-wijzigen", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeQuantityAction(Request $request): JsonResponse
    {
        $combination = $this->getDoctrine()->getRepository(Combination::class)->find($request->get('combination'));
        if ($combination === null) {
            throw new \RuntimeException('Fout met ophalen van combinaties');
        }

        $combination->setMinQuantity($request->get('minQuantity'));
        $combination->setMaxQuantity($request->get('maxQuantity'));

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 'ok'
        ]);
    }

    /**
     * @Route("/beschikbare-combinatieproducten", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getAvailableCombinationProducts(Request $request): JsonResponse
    {
        $productCombinationManager = $this->get(ProductCombinationManager::class);

        return $productCombinationManager->combinationSelectionQuery($request);
    }

    /**
     * @Route("/combinatie-uitbreiden", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function extendCombination(Request $request): JsonResponse
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $entityManager = $this->getDoctrine()->getManager();
        $combination = $repo->find($request->get('combination_id'));
        $product = $repo->find($request->get('product_id'));
        $productCombinationService = $this->get('admin.product_combination');

        $newCombination = $productCombinationService->createCombination($combination, $product);
        $entityManager->persist($newCombination);
        $entityManager->flush();

        return $this->json(['success' => 'De combinatie is uitgebreid']);
    }
}
