<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\TransportType;
use Symfony\Component\Routing\Annotation\Route;
use AdminBundle\Datatable\Catalog\Product\TransportTypeType as TransportDatatableType;
use AdminBundle\Form\Catalog\Product\TransportTypeType as TransportFormType;

/**
 * @Route("/catalogus/producten/bezorgkosten")
 *
 */
class ProductTransportTypeController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bezorgkosten';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Bezorgkosten',
            'entity_class' => TransportType::class,
            'form_type' => TransportFormType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/form/product.css',
                'admin/css/product/product-item.css',
                'admin/css/designer/DesignSelectModal.css',
            ],
            'datatable' => [
                'entity_class' => TransportType::class,
                'datatable_type' => TransportDatatableType::class,
                'form_type' => TransportFormType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'allow_edit' => false,
                'allow_read' => true,
            ],
            'title' => function (TransportType $product) {
                return $product->getName();
            },
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/transport-description-panel.html.twig',
                    ],
                ],
            ],
        ];
    }
}
