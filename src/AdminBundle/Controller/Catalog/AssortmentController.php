<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\AssortmentType as AssortmentTypeDatatable;
use AdminBundle\Form\Catalog\AssortmentType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Preview\PreviewableControllerInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/assortimenten")
 */
class AssortmentController extends CrudController implements CrudControllerInterface, PreviewableControllerInterface
{
    /**
     * @param null $n
     *
     * @return mixed|null|string
     */
    public function getLabel($n = null)
    {
        return 'Assortiment';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Assortment::class,
            'form_type' => AssortmentType::class,
            'datatable' => [
                'entity_class' => Assortment::class,
                'form_type' => AssortmentType::class,
                'datatable_type' => AssortmentTypeDatatable::class,
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-table-search',
                        'type' => 'preview',
                        'label' => 'Voorbeeld',
                        'url' => function (Assortment $assortment) {
                            return $this->generateUrl('admin_catalog_assortment_preview', [
                                'entity' => $assortment->getId(),
                            ]);
                        },
                    ],
                ],
            ],
            'assets' => [
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/assortment.js',
                'admin/css/form/assortment.css',
                'admin/js/form/product.js',
            ],
        ];
    }

    /**
     * @Route("/nieuw", methods={"GET"})
     * @Template()
     */
    public function addAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor("create"));

        $entity = $this->getEntity();

        return $this->form($entity, false);
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @Template()
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     * @throws \ReflectionException
     */
    public function editAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor("create"));

        $entity = $this->getRepository()->find($id);

        return $this->form($entity, false);
    }

    /**
     * @Route("/zoeken", methods={"GET"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function searchAction(Request $request)
    {
        $currentProductIds = [];
        $assortment = null;

        if ($request->query->get('assortment')) {
            $assortment = $this->getDoctrine()->getRepository(Assortment::class)->find($request->query->get('assortment'));
        }

        if ($assortment) {
            foreach ($assortment->getProducts() as $product) {
                if ($product === null) {
                    continue;
                }

                $currentProductIds[] = $product->getId();
            }
        }

        $entityClass = $request->get('isCardAssortment') ? ProductCard::class : GenericProduct::class;

        $results = $this->getSearchResults($entityClass, $request->query->get('query'));

        if ($request->get('isCompanyAssortment')) {
            $company = $request->get('company');

            $class = $request->get('isCardAssortment') ? CompanyCard::class : CompanyProduct::class;

            //get all company products from owner
            $companyResults = $this->getSearchResults($class, $request->query->get('query'), $company);
            $results = array_merge($results, $companyResults);
        }

        return [
            'currentProductIds' => $currentProductIds,
            'results' => $results,
        ];
    }

    /**
     * @param $entityClass
     * @param $query
     * @return array
     */
    private function getSearchResults($entityClass, $query, $companyId = null)
    {
        $results = [];

        $qb = $this->getDoctrine()->getManager()->getRepository($entityClass)
            ->createQueryBuilder('p');

        $productResults = $qb->distinct()
            ->select('p')
            ->leftJoin(ProductTranslation::class, 'pt', 'WITH',
                'p.id = pt.translatable AND pt.locale = :locale')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('pt.title', ':search'),
                $qb->expr()->like('pt.shortDescription', ':search'),
                $qb->expr()->like('pt.description', ':search')
            ));

        if ($entityClass !== ProductCard::class) {
            $qb->andWhere('p.parent IS NULL');
        }

        $parameters = [
            'locale' => 'nl_NL',
            'search' => '%' . $query . '%',
        ];

        if ($companyId) {
            $productResults->andWhere('p.company = :company');
            $parameters['company'] = $companyId;
        }

        $productResults = $productResults->setParameters($parameters)->getQuery()->getResult();

        /** @var Product $product */
        foreach ($productResults as $product) {
            if ($product instanceof CompanyProduct || $product instanceof CompanyCard || !$product->getProductgroup()) {
                $categoryTitle = 'Niet gecategoriseerd';
                if ($product instanceof CompanyProduct) {
                    $categoryTitle = 'Bedrijfsproducten';
                }
                if ($product instanceof CompanyCard) {
                    $categoryTitle = 'Bedrijfskaartjes';
                }

                $results[null] = [
                    'id' => null,
                    'name' => $categoryTitle,
                    'products' => [],
                ];

                $results[null]['products'][] = $product;
            } else {
                if (empty($results[$product->getProductgroup()->getId()])) {
                    $results[$product->getProductgroup()->getId()] = [
                        'id' => $product->getProductgroup()->getId(),
                        'name' => $product->getProductgroup()->getTitle(),
                        'products' => [],
                    ];
                }

                $results[$product->getProductgroup()->getId()]['products'][] = $product;
            }
        }

        return $results;
    }


    /**
     * @Route("/preview/{entity}/sites", methods={"GET"})
     * @ParamConverter("entity", class="AppBundle\Entity\Catalog\Assortment\Assortment")
     *
     * @param PreviewableEntityInterface|Assortment $entity
     * @param Request                               $request
     *
     * @return JsonResponse
     */
    public function previewableSitesAction(PreviewableEntityInterface $entity, Request $request)
    {
        $sites = new ArrayCollection();
        $results = [];

        foreach ($entity->getSites() as $site) {
            if ($sites->contains($site)) {
                continue;
            }

            $sites->add($site);
        }

        foreach ($sites as $site) {
            $results[$site->getId()] = $site->translate()->getDescription();
        }

        return new JsonResponse([
            'sites' => $results,
        ]);
    }

    /**
     * @Route("/preview/{entity}/{site}", defaults={"site" = null}, methods={"GET"})
     * @ParamConverter("entity", class="AppBundle\Entity\Catalog\Assortment\Assortment")
     *
     * @param PreviewableEntityInterface|Assortment $entity
     * @param Site|null                             $site
     * @param Request                               $request
     *
     * @return RedirectResponse
     */
    public function previewAction(PreviewableEntityInterface $entity, Site $site = null, Request $request)
    {
        $values = [
            'class' => get_class($entity),
            'identifier' => $entity->getId(),
            'siteId' => $site->getId(),
            'admin_url' => $request->query->get('return_uri', null),
        ];

        $url = $this->get('preview')->getRequestUrl($site->getPrimaryDomain()->getUri(), $values);

        return new RedirectResponse($url);
    }

    /**
     * @Route("/ophalen", methods={"GET"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function listAll(Request $request)
    {
        $assortments =  $this->getDoctrine()->getRepository(Assortment::class)->findAll();

        if ($request->isXmlHttpRequest()) {
            $returnArray = [];

            /** @var Assortment $assortment */
            foreach($assortments as $assortment) {
                $name = $assortment->translate()->getTitle();

                if($assortment->getOwner()) {
                    $name .= sprintf(' (%s)', $assortment->getOwner());
                }

                $returnArray[$assortment->getId()] = $name;
            }

            return new JsonResponse($returnArray);
        }

        return $assortments;
    }
}
