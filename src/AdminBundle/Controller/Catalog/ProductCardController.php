<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductCardType as ProductCardDatatableType;
use AdminBundle\Form\Catalog\ProductCardType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Services\Parameter\ParameterService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten/kaartjes")
 *
 */
class ProductCardController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Kaartjes';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Kaartjes',
            'entity_class' => ProductCard::class,
            'form_type' => ProductCardType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/form/product.css',
                'admin/css/product/product-item.css',
                'admin/css/designer/DesignSelectModal.css',
            ],
            'datatable' => [
                'entity_class' => ProductCard::class,
                'form_type' => ProductCardType::class,
                'datatable_type' => ProductCardDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'allow_read' => true,
                'allow_edit' => false,
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_catalog_productcard_wizard',
                        'label' => 'Kaartje toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'actions_processing' => 'override',
            ],
            'title' => function (ProductCard $product) {
                return $product->getName();
            },
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/product-card-description-panel.html.twig',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/wizard", methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function wizardAction(Request $request)
    {
        $productGroupId = $this->container->get(ParameterService::class)->setEntity()->getValue('card_productgroup');
        /** @var Productgroup $productGroup */
        $productGroup = $this->getDoctrine()->getRepository(Productgroup::class)->find($productGroupId);

        if($request->isMethod('POST')) {
            $title = $request->get('title');
            /** @var ProductgroupPropertySet $propertySet */
            $propertySet = $this->getDoctrine()->getRepository(ProductgroupPropertySet::class)->find($request->get('property_set'));

            $card = new ProductCard();
            $card->translate('nl_NL')->setTitle($title);
            $card->setName($title);
            $card->setProductgroup($productGroup);
            $card->setPropertySet($propertySet);
            $card->mergeNewTranslations();

            $this->getDoctrine()->getManager()->persist($card);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_catalog_productcard_edit', ['id' => $card->getId()]);
        }

        return [
            'propertySets' => $productGroup->getProductgroupPropertySets()
        ];
    }
}
