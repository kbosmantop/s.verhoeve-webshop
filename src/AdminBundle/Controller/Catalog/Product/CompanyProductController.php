<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\CompanyProductDatatableType;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Datatable\Catalog\ProductVariationDatatableType;
use AdminBundle\Form\Catalog\Product\CompanyProductFormType;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Services\Company\CompanySkuService;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/bedrijfsproducten")
 * Class CompanyProductController
 * @package AdminBundle\Controller\Catalog\Product
 */
class CompanyProductController extends CrudController
{

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bedrijfsproducten';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => CompanyProduct::class,
            'form_type' => CompanyProductFormType::class,
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => CompanyProduct::class,
                'form_type' => CompanyProductFormType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_catalog_product_wizard',
                        'label' => 'Product toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
            ],
            'assets' => [
                'admin/js/form/product.js',
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description_panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/company-product-description-panel.html.twig',
                    ],
                    [
                        'id' => 'variation-panel',
                        'title' => 'Variaties',
                        'type' => 'datatable',
                        'datatable' => [
                            'datatable_type' => CompanyProductDatatableType::class,
                            'entity_class' => CompanyProduct::class,
                            'form_type' => CompanyProductFormType::class,
                            'results_per_page' => 5,
                            'parent_column' => 'parent',
                            'base_route' => 'admin_catalog_product_companyproductvariation',
                            'allow_read' => false,
                            'refresh_panel' => 'variation-panel',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/producten-lijst", methods={"GET"})
     * @param Company $company
     * @return JsonResponse
     */
    public function productListAction()
    {
        return new JsonResponse([
            'products' => $this->getProducts(GenericProduct::class),
        ]);
    }

    /**
     * @param $class
     * @return array
     */
    private function getProducts($class)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository($class)->createQueryBuilder('gp');
        $qb->andWhere('gp.parent is null');

        $products = $qb->getQuery()->getResult();

        $productsArray = [];
        /** @var Product $product */
        foreach ($products as $product) {
            $productsArray[] = [
                'id' => $product->getId(),
                'text' => $product->getName(),
                'published' => $product->isPublished(),
                'combination' => $product->isCombination()
            ];
        }

        return $productsArray;
    }

    /**
     * @Route("/{company}/product-kopieren", methods={"POST"})
     *
     * @param Company $company
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function copyProductAction(Company $company, Request $request)
    {
        $this->em = $this->getDoctrine()->getManager();

        $product = $this->getDoctrine()->getRepository(GenericProduct::class)->find($request->get('product'));

        if (null !== $product) {
            $this->createCompanyProduct($company, $product);
            $this->em->flush();
        }

        return new JsonResponse();
    }

    /**
     * @param Company $company
     * @param Product $product
     * @return CompanyProduct
     * @throws \Exception
     */
    private function createCompanyProduct(Company $company, Product $product)
    {
        $companyProduct = new CompanyProduct();
        $companyProduct->setCompany($company);
        $companyProduct->setRelatedProduct($product);
        $companyProduct->setPublish($product->getPublish() ?: null);
        $companyProduct->setPublishStart($product->getPublishStart());
        $companyProduct->setPublishEnd($product->getPublishEnd());
        $companyProduct->setName($product->getName());
        if($product->getParent() !== null) {
            $companyProduct->setSku($this->get(CompanySkuService::class)->getNextAvailableProductSkuForCompany($company));
        }

        $companyProductTranslation = new ProductTranslation();
        $companyProductTranslation->setTitle($product->getTitle());
        $companyProductTranslation->setLocale('nl_NL');

        $companyProduct->addTranslation($companyProductTranslation);

        $this->em->persist($companyProductTranslation);
        $this->em->persist($companyProduct);

        foreach ($product->getVariations() as $variation) {
            $companyProduct->addVariation($this->createCompanyProduct($company, $variation));
        }

        return $companyProduct;
    }

}
