<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\Product\CompanyTransportTypeDatatableType;
use AdminBundle\Datatable\Catalog\Product\ProductStickerDatatableType;
use AdminBundle\Form\Catalog\Product\CompanyTransportTypeFormType;
use AdminBundle\Form\Catalog\Product\ProductStickerFormType;
use AppBundle\Entity\Catalog\Product\CompanyTransportType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductSticker;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bedrijven/bezorgkosten")
 * Class CompanyTransportTypeController
 * @package AdminBundle\Controller\Catalog\Product
 */
class CompanyTransportTypeController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => CompanyTransportType::class,
            'form_type' => CompanyTransportTypeFormType::class,
            'datatable' => [
                'datatable_type' => CompanyTransportTypeDatatableType::class,
                'entity_class' => CompanyTransportType::class,
                'form_type' => CompanyTransportTypeFormType::class,
            ]
        ];
    }
}