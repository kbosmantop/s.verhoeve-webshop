<?php

namespace AdminBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/")
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login")
     * @Template("@Admin/Security/login.html.twig")
     */
    public function loginAction()
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('admin_dashboard_index');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            // last username entered by the user
            'last_username' => $lastUsername,
            'error' => $error ? 'Ongeldige gebruikersnaam of wachtwoord.' : null,
        ];
    }

    /**
     * @Route("/logout")
     */
    public function logout()
    {
        return [];
    }
}
