<?php

namespace AdminBundle\Controller\Sales;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Order\Order;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\Parameter\ParameterService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/bestelling/vergoeding")
 * @Security("has_role('ROLE_ADMIN')")
 */
class RestitutionController extends Controller
{
    /**
     * @var ParameterService $parameterService
     */
    private $parameterService;

    /**
     * @var VoucherService $voucherService
     */
    private $voucherService;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var OrderManager $orderManager
     */
    private $orderManager;

    /**
     * RestitutionController constructor.
     * @param ParameterService        $parameterService
     * @param VoucherService          $voucherService
     * @param EntityManagerInterface  $entityManager
     * @param ContainerInterface|null $container
     */
    public function __construct(ParameterService $parameterService, VoucherService $voucherService, EntityManagerInterface $entityManager, OrderManager $orderManager, ContainerInterface $container = null)
    {
        $this->parameterService = $parameterService;
        $this->voucherService = $voucherService;
        $this->setContainer($container);
        $this->entityManager = $entityManager;
        $this->orderManager = $orderManager;
    }

    /**
     * @Route("/{order}/opties", methods={"GET"})
     *
     * @param Request $request
     * @param Order   $order
     * @return array|Response
     */
    public function indexAction(Request $request, Order $order)
    {
        $fixedDiscounts = $this->entityManager->getRepository(Discount::class)->getAllFixedRestitutionDiscounts($this->parameterService->setEntity()->getValue('order_restitution_voucher_values'));
        $percentDiscounts = $this->entityManager->getRepository(Discount::class)->getAllRelativeRestitutionDiscounts($this->parameterService->setEntity()->getValue('order_restitution_voucher_relative_values'));

        $restitutionValues = [
            'fixed' => $fixedDiscounts,
            'percent' => $percentDiscounts,
        ];

        $restitutionMaxValue = $this->orderManager->getTotalPriceIncl($order) * ((int)$this->parameterService->setEntity()->getValue('order_restitution_voucher_max_relative_pct') / 100);

        return $this->render('@Admin/Sales/Restitution/restitution-options.html.twig', [
            'order' => $order,
            'restitutionValues' => $restitutionValues,
            'restitutionMaxValue' => $restitutionMaxValue,
        ]);
    }

    /**
     * @Route("/{order}/opties", methods={"POST"})
     *
     * @param Request $request
     * @param Order   $order
     * @return array|Response
     * @throws \Exception
     */
    public function saveAction(Request $request, Order $order)
    {
        $restitutionMaxValue = $this->orderManager->getTotalPriceIncl($order) * ((int)$this->parameterService->setEntity()->getValue('order_restitution_voucher_max_relative_pct') / 100);
        $requestOption = $request->request->get('restitution-option');

        if($requestOption === 'fixed' && ($fixed = $request->request->get('fixed'))){
            /** @var Discount $discount */
            $discount = $this->entityManager->getRepository(Discount::class)->find($fixed);
            if($discount !== null && $discount->getValue() <= $restitutionMaxValue){
                if($order->getRestitutionVoucher() === null) {
                    $voucher = $this->voucherService->generateRestitutionVoucher($order, $discount);
                    $this->addFlash('success',
                        sprintf('Voucher €%s aangemaakt', number_format($voucher->getDiscount()->getValue(), 2)));
                } else {
                    $this->addFlash('success', 'Er bestaat al een voucher voor deze order, om het type te veranderen verwijder eerst de huidige voucher');
                }
            } else {
                $this->addFlash('warning', 'Voucher bedrag te hoog');
            }
//        } elseif($requestOption === 'percent' && ($percent = $request->request->get('percent'))){
//            /** @var Discount $discount */
//            $discount = $this->entityManager->getRepository(Discount::class)->find($percent);
//            if($discount !== null && ($this->orderManager->getTotalPriceIncl($order) * $discount->getValue() / 100) <= $restitutionMaxValue){
//                if($order->getRestitutionVoucher() === null) {
//                    $voucher = $this->voucherService->generateRestitutionVoucher($order, $discount);
//                    $this->addFlash('success',
//                        sprintf('Voucher %s%% aangemaakt', number_format($voucher->getDiscount()->getValue(), 2)));
//                } else {
//                    $this->addFlash('success', 'Er bestaat al een voucher voor deze order, om het type te veranderen verwijder eerst de huidige voucher');
//                }
//            } else {
//                $this->addFlash('warning', 'Voucher bedrag te hoog');
//            }
        } else {
            if($order->getRestitutionVoucher() !== null) {
                $isVoucherInvalidated = $this->voucherService->invalidateVoucher($order->getRestitutionVoucher());
                if ($isVoucherInvalidated === true) {
                    $order->setRestitutionVoucher(null);
                    $this->addFlash('warning', 'Voucher ongeldig gemaakt');
                } else {
                    $this->addFlash('warning', 'Voucher is al gebruikt en kan niet ongeldig gemaakt worden');
                }
            } else {
                $this->addFlash('warning', 'Geen voucher..');
            }
        }

        $this->entityManager->flush();

        return $this->indexAction($request, $order);
    }

}
