<?php

namespace AdminBundle\Controller\Sales;

use AdminBundle\Form\Sales\OrderCancelType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\Registry;
use AppBundle\Services\ConnectorHelper;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bestelling-order")
 *
 */
class OrderOrderCancelController extends Controller
{
    /**
     * @Route("/{order}/annuleren", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Order   $order
     * @return array|Response
     */
    public function indexAction(Request $request, Order $order)
    {
        if ($order->getStatus() === 'cancelled') {
            return $this->render('@Admin/Sales/OrderOrderCancel/success.html.twig', [
                'order' => $order,
                'message' => 'Order is al geannuleerd',
            ]);
        }

        $message = null;

        $form = $this->getForm($order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $orderActivity = new OrderActivity();
            $orderActivity->setUser($this->getUser());
            $orderActivity->setOrder($order);
            $orderActivity->setActivity(($form->has('company') && $form->get('company')->getData() ? 'order_canceled_at_supplier' : 'order_cancelled'));
            $orderActivity->setText($form->get('reason')->getData());

            $this->getDoctrine()->getManager()->persist($orderActivity);

            $supplierOrder = $order->getSupplierOrder();

            if ($supplierOrder !== null) {
                // TODO SF4
                $registry = $this->container->get(Registry::class);
                $connector = $this->container->get(ConnectorHelper::class)->getConnector($supplierOrder);

                if ($connector) {
                    try {
                        $order = $connector->cancel($supplierOrder);
                        $registry->get($supplierOrder)->apply($supplierOrder, 'cancel');

                    } catch (Exception $exception) {
                        return $this->render('@Admin/Sales/OrderOrderCancel/index.html.twig', [
                            'order' => $order,
                            'form' => $form->createView(),
                            'message' => $exception->getMessage(),
                        ]);
                    }
                }
            }

            if ($form->has('company') && $form->get('company')->getData()) {
                $order->setStatus('new'); // @todo: workflow refactor
                $message = 'Order weg gehaald bij leverancier';

                $order->setForceManual(true);

                /** @var OrderActivityService $orderActivityService */
                $orderActivityService = $this->container->get('order.activity');
                $orderActivityService->add('force_manual', $order, null, ['text' => $message]);

            } else {
                $order->setStatus("cancelled"); // @todo: workflow refactor
                $message = 'Order geannuleerd voor consument';
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->render('@Admin/Sales/OrderOrderCancel/success.html.twig', [
                'order' => $order,
                'message' => $message,
            ]);
        }

        return [
            'order' => $order,
            'form' => $form->createView(),
            'message' => lcfirst($message),
        ];
    }

    /**
     * @param Order $order
     * @return FormInterface
     */
    private function getForm(Order $order)
    {
        return $this->createForm(OrderCancelType::class, null, [
            'action' => $this->generateUrl('admin_sales_orderordercancel_index', [
                'order' => $order->getId(),
            ]),
        ]);
    }

    /**
     * @Route("/{order}/consumer-ajax-cancel")
     * @param Request $request
     * @param Order   $order
     * @return Response
     */
    public function cancelOrderAjax(Request $request, Order $order)
    {
        $reason = $request->get('reason');
        $type = $request->get('type');

        return $this->cancel($type, $reason, $order);
    }

    /**
     * @param       $type
     * @param       $reason
     * @param Order $order
     * @return Response
     */
    private function cancel($type, $reason, Order $order)
    {
        $this->container->get(OrderManager::class)->cancel($order, $reason, $type);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new Response('Ok', 200);
    }
}
