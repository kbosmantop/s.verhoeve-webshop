<?php

namespace AdminBundle\Controller\Sales;

use AdminBundle\Controller\Controller;
use AdminBundle\Datatable\Order\BakeryOrderDatatableType;
use AdminBundle\Form\Sales\OrderType;
use AppBundle\Entity\Order\Order;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BakeryOrderController
 * @package AdminBundle\Controller\Sales
 * @Route("/bakkers")
 */
class BakeryOrderController extends Controller
{
    /**
     * @Route("/onverwerkte-orders", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws Exception
     */
    public function unprocessedOrdersAction()
    {
        $type = 'unprocessed';

        return $this->getDatatable($type);
    }

    /**
     * @Route("/verwerkte-orders", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     */
    public function processedOrdersAction()
    {
        $type = 'processed';

        return $this->getDatatable($type);
    }

    /**
     * @param $type
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     */
    private function getDatatable($type)
    {
        return $this->container->get('admin.datatable.service')->getData([
            'entity_class' => Order::class,
            'datatable_type' => BakeryOrderDatatableType::class,
            'form_type' => OrderType::class,
            'query_builder' => function (QueryBuilder $qb) use ($type) {
                return $this->getQueryBuilder($qb, $type);
            },
            'allow_add' => false,
            'allow_read' => false,
            'allow_delete' => false,
            'allow_edit' => false,
            'add_checkbox' => false,
            'alias' => 'o',
            'item_actions' => [
                [
                    'icon' => 'mdi mdi-eye',
                    'type' => 'get-order',
                    'method' => 'GET',
                    'url' => function (Order $order) {
                        return $this->generateUrl('admin_sales_order_order', [
                            'orderCollection' => $order->getOrderCollection()->getId()
                        ]) . '#'. $order->getNumber();
                    }
                ],
                [
                    'icon' => 'fa fa-check',
                    'type' => 'manually-process-order',
                    'method' => 'GET',
                    'url' => function (Order $order) {
                        return $this->generateUrl('admin_sales_orderorderprocess_manuallyprocess', [
                            'order' => $order->getId()
                        ]);
                    }
                ],
            ]
        ]);
    }

    /**
     * @param QueryBuilder $qb
     * @param              $type
     * @return QueryBuilder
     */
    private function getQueryBuilder(QueryBuilder $qb, $type)
    {
        $tomorrow = date('Y-m-d', strtotime('+1 day'));

        if (date('N', strtotime($tomorrow)) == '7') {
            $tomorrow = date('Y-m-d', strtotime('+2 day'));
        }

        $qb->leftJoin('o.supplierOrder', 'so');
        $qb->leftJoin('o.orderCollection', 'oc')->addSelect('oc');
        $qb->andWhere('o.deliveryDate <= :tomorrow');
        $qb->andWhere('o.deliveryDate >= :past');
        $qb->andWhere('o.supplierOrder is not null');
        $qb->andWhere('so.connector = :bakery or so.connector = :bakeryMail');

        if ($type === 'unprocessed') {
            $qb->andWhere('o.status = :status');
        } else {
            $qb->andWhere('o.deliveryStatus is null or o.deliveryStatus = :status');
        }

        // Query pre-fetching optimalization
        $qb->leftJoin('o.autoOrder', 'auto_order')->addSelect('auto_order');
        $qb->leftJoin('o.cartOrder', 'cart_order')->addSelect('cart_order');
        $qb->leftJoin('oc.cart', 'cart')->addSelect('cart');
        $qb->leftJoin('oc.review', 'review')->addSelect('review');

        $qb->addOrderBy('o.deliveryDate', 'ASC');

        $qb->setParameters([
            'tomorrow' => $tomorrow,
            'past' => date('Y-m-d', strtotime('-7 day')),
            'bakery' => 'Bakker',
            'bakeryMail' => 'BakkerMail',
            'status' => $type === 'unprocessed' ? 'pending' : 'unknown',
        ]);

        return $qb;
    }
}
