<?php

namespace AdminBundle\Controller\Authentication;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use AdminBundle\Entity\AuthenticationLog;
use AdminBundle\Datatable\Authentication\LogType;

/**
 * @Route("/instellingen/authenticatie/logboek")
 *
 */
class LogController extends CrudController implements CrudControllerInterface
{
    /**
     * @param int $n
     *
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Authenticatie Logboek';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'label' => 'Logboek',
            'entity_class' => AuthenticationLog::class,
            'form_type' => false,
            'datatable' => [
                'entity_class' => AuthenticationLog::class,
                'datatable_type' => LogType::class,
                'results_per_page' => 50,
                'form_type' => false,
                'item_actions' => false,
                'bulk_actions' => false,
                'order' => [
                    [1, 'desc']
                ]
            ],
        ];
    }
}
