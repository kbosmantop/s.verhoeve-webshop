<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\Company\Financial\CompanyFinancialBillingSettingsType;
use AdminBundle\Form\Company\Financial\CompanyFinancialGeneralSettingsType;
use AppBundle\Entity\Relation\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompanyFinancialController
 * @package AdminBundle\Controller\Company
 * @Route("/klanten/bedrijven/financieel/instellingen")
 */
class CompanyFinancialController extends Controller
{
    /**
     * @Route("/{company}/algemeen", methods={"GET", "POST"})
     * @Template()
     *
     * @param Company $company
     * @param Request $request
     * @return array|JsonResponse
     */
    public function generalAction(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyFinancialGeneralSettingsType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{company}/facturatie", methods={"GET", "POST"})
     * @Template()
     *
     * @param Company $company
     *
     * @param Request $request
     * @return array|JsonResponse
     */
    public function billingAction(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyFinancialBillingSettingsType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return [
            'form' => $form->createView()
        ];
    }
}
