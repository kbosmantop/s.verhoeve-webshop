<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\Controller;
use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Menu;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompanyMenuController
 * @package AdminBundle\Controller\Company
 * @Route("/bedrijfsmenu")
 */
class CompanyMenuController extends Controller {

    /**
     * @Route("/{company}/bewerken", methods={"GET"})
     *
     * @param Company $company
     * @return RedirectResponse
     */
    public function editAction(Company $company) {
        $em = $this->getDoctrine()->getManager();

        $menu =  $company->getMenu();
        if(null === $menu) {
            $menu = new Menu();
            $menu->setLocation(MenuLocationType::MENU_LOCATION_MAIN);
            $menu->setName($company->getName().' Hoofdmenu');

            $company->setMenu($menu);

            $em->persist($menu);
            $em->flush();
        }

        return $this->redirectToRoute('admin_site_navigation_menu', ['id' => $menu->getId()]);
    }

}
