<?php

namespace AdminBundle\Controller\Company;


use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Company\CompanyCustomFieldDatatableType;
use AppBundle\Entity\Relation\CompanyCustomField;
use AdminBundle\Form\Company\CompanyCustomFieldFormType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompanyCustomFieldController
 * @package AdminBundle\Controller\Company
 * @Route("/bedrijven/bedrijfsvelden")
 */
class CompanyCustomFieldController extends CrudController
{

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bedrijfsvelden';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'form_type' => CompanyCustomFieldFormType::class,
            'entity_class' => CompanyCustomField::class,
            'datatable' => [
                'form_type' => CompanyCustomFieldFormType::class,
                'entity_class' => CompanyCustomField::class,
                'datatable_type' => CompanyCustomFieldDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
            ],
        ];
    }

}
