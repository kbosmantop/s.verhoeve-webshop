<?php

namespace AdminBundle\Controller\Payment;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;

/**
 * Class InvoiceCancellationController
 * @package AdminBundle\Controller\Payment
 *
 * @Route("/betaling")
 * @Security("has_role('ROLE_ADMIN')")
 */
class InvoiceCancellationController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderActivityService
     */
    private $orderActivityService;

    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @param EntityManagerInterface $entityManager
     * @param OrderActivityService $orderActivityService
     * @param OrderCollectionManager $orderCollectionManager
     * @param Registry $registry
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        OrderActivityService $orderActivityService,
        OrderCollectionManager $orderCollectionManager,
        Registry $registry
    ) {
        $this->entityManager = $entityManager;
        $this->orderActivityService = $orderActivityService;
        $this->orderCollectionManager = $orderCollectionManager;
        $this->registry = $registry;
    }

    /**
     * @Route("/{orderCollection}/factuurbetaling-annuleren")
     * @param OrderCollection $orderCollection
     *
     * @return JsonResponse
     */
    public function cancel(OrderCollection $orderCollection): JsonResponse
    {
        if (! $this->orderCollectionManager->hasCancellableInvoicePayment($orderCollection)) {
            return $this->json([
                'error' => 'Geen factuurbetalingsmethode in de order of het verwerkingsproces is al begonnen',
            ], 422);
        }

        $this->setOrdersToPaymentPending($orderCollection);

        $this->cancelInvoicePayments($orderCollection);

        $this->addOrderActivity($orderCollection);

        return $this->json([
            'success' => 'De factuurbetalingmethode is geannuleerd',
        ]);
    }

    /**
     * @param OrderCollection $orderCollection
     */
    private function setOrdersToPaymentPending(OrderCollection $orderCollection): void
    {
        /** @var Order $order */
        foreach ($orderCollection->getOrders() as $order) {
            $workflow = $this->registry->get($order);
            $transition = 'cancel_invoice_payment_method';

            if ($workflow->can($order, $transition)) {
                $workflow->apply($order, $transition);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param OrderCollection $orderCollection
     */
    private function cancelInvoicePayments(OrderCollection $orderCollection): void
    {
        $cancellablePayment = new ArrayCollection();

        /** @var Payment $payment */
        foreach ($orderCollection->getPayments() as $payment) {
            $paymentMethod = $payment->getPaymentmethod();
            if (null !== $paymentMethod && $paymentMethod->getCode() === 'invoice') {
                $cancellablePayment->add($payment);
            }
        }

        foreach ($cancellablePayment->getIterator() as $payment) {
            $payment->setStatus(
                $this->entityManager->getRepository(PaymentStatus::class)->find('cancelled')
            );
        }
    }

    /**
     * @param OrderCollection $orderCollection
     */
    private function addOrderActivity(OrderCollection $orderCollection): void
    {
        $this->orderActivityService->add(
            'invoice_payment_cancelled',
            $orderCollection,
            $this->getUser(),
            ['text' => 'Factuurbetalingsmethode handmatig geannuleerd']
        );

        $this->entityManager->flush();
    }
}
