<?php

namespace AdminBundle\Controller\Payment;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Services\Payment\PaymentRequestService;
use AppBundle\Services\Sales\Order\OrderActivityService;
use DateTime;
use Exception;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaymentRequestController
 * @package AdminBundle\Controller\Payment
 *
 * @Route("/betaalverzoek")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PaymentRequestController extends Controller
{
    /**
     * @Route("/{orderCollection}/aanmaken", methods={"GET"})
     * @Template()
     *
     * @param OrderCollection $orderCollection
     *
     * @return array
     * @throws ReflectionException
     */
    public function create(OrderCollection $orderCollection)
    {
        $form = $this->createFormBuilder();

        $container = $form->create('container', ContainerType::class);
        $column = $form->create('column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $methods = $this->container->get('app.payment')->getMethods($orderCollection);

        $paymentmethods = [];
        foreach ($methods as $method) {
            $paymentmethods[$method->getCode()] = $method->getName();
        }

        $column
            ->add('amount', MoneyType::class, [
                'label' => 'Bedrag',
                'data' => $orderCollection->getPaymentAmount(),
            ])
            ->add('name', TextType::class, [
                'label' => 'Naam',
                'data' => $orderCollection->getCustomer()->getFullname(),
            ])
            ->add('email', EmailType::class, [
                'data' => $orderCollection->getCustomer()->getEmail(),
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Persoonlijke tekst',
                'required' => false,
            ])
            ->add('methods', ChoiceType::class, [
                'label' => 'Betaalmethoden',
                'choices' => array_flip($paymentmethods),
                'multiple' => true,
                'multiselect' => [],
                'choice_attr' => function () {
                    return ['checked' => 'checked'];
                },
            ]);

        $container->add($column);
        $form->add($container);

        return [
            'form' => $form->getForm()->createView(),
        ];
    }

    /**
     * @Route("/{orderCollection}/aanmaken", methods={"POST"})
     *
     * @param OrderCollection $orderCollection
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createPersist(OrderCollection $orderCollection, Request $request)
    {
        $requestParameters = $request->request->all();

        $paymentData = $requestParameters['form']['container']['column'];
        $paymentData['amount'] = (float) str_replace(',', '.', $paymentData['amount']);

        // todo SF4
        $this->get(PaymentRequestService::class)->create([
            'amount' => $paymentData['amount'],
            'name' => $paymentData['name'],
            'email' => $paymentData['email'],
            'methods' => $paymentData['methods'],
            'description' => $paymentData['description'],
            'orderCollection' => $orderCollection,
        ]);

        $this->addActivity($orderCollection, $paymentData);

        return $this->json(['success']);
    }

    /**
     * @param OrderCollection $orderCollection
     * @param $paymentData
     *
     * @throws Exception
     */
    private function addActivity(OrderCollection $orderCollection, $paymentData): void
    {
        $paymentData['methods'] = implode(', ', $paymentData['methods']);

        // todo SF4
        $this->get(OrderActivityService::class)->add(
            'payment_request_sent',
            $orderCollection,
            $this->getUser(),
            ['text' => json_encode($paymentData)]
        );

        // todo SF4
        $this->getDoctrine()->getManager()->flush();
    }
}
