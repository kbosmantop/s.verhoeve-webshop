<?php

namespace AdminBundle\Controller\Designer;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\DesignerControllerInterface;
use AppBundle\Services\Designer;
use AppBundle\Traits\DesignerTrait;
use Exception;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/designer")
 */
class DesignerController extends Controller implements DesignerControllerInterface
{
    use DesignerTrait;

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('admin_dashboard_index');
    }

    /**
     * @Route("/preview/{uuid}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|__UUID__",
     * }, methods={"GET"})
     * @param string $uuid
     *
     * @param Request $request
     *
     * @return string|null
     */
    public function getPreviewUrl(string $uuid, Request $request) {
        $designer = $this->get(Designer::class);
        $designer->setUuid($uuid);
        $designer->setIsPreDesign($request->get('predesign', false));

        return new JsonResponse($designer->getPreviewUrl(60, Designer::SOURCE_LOCAL));
    }

    /**
     * @Route("/screenshot/{uuid}/{product}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "product":"\d+"
     * }, defaults={"product" = null}, methods={"GET", "POST"})
     * @Template("@Admin/Designer/Designer/edit.html.twig")
     *
     * @param Request      $request
     * @param string       $uuid
     * @param Product|null $product
     * @return array
     * @throws Exception
     */
    public function screenshot(Request $request, $uuid, Product $product = null)
    {
        if ($this->container->has('profiler')) {
            $profiler = $this->container->get('profiler');
            $profiler->disable();
        }

        $screenshotModus = true;

        $params = $this->edit($request, $uuid, $product, $screenshotModus);

        /** @var Designer $designer */
        $designer = $params['designer'];

        $config = $designer->getConfig();
        $params['screenshotModus'] = $screenshotModus;
        $params['width'] = $designer->convertCmToPx($config->dimensions->width);
        $params['height'] = $designer->convertCmToPx($config->dimensions->height);

        return $params;
    }

    /**
     * @Route("/ontwerpen/{product}/toevoegen", requirements={
     *     "product":"\d+|__PRODUCT__"
     * }, defaults={"product" = null}, methods={"GET", "POST"})
     * @Template("@Admin/Designer/Designer/edit.html.twig")
     *
     * @param Request      $request
     * @param Product|null $product
     * @return array|JsonResponse
     * @throws Exception
     */
    public function addAction(Request $request, Product $product = null)
    {
        $designer = $this->get('app.designer');

        $designer->setIsPreDesign((bool)$request->get('is-pre-design'));
        $designer->setCallback($request->get('callback'));

        $uuid = $designer->getUuid();

        $params = [
            'uuid' => $uuid,
            'is-pre-design' => $designer->isPreDesign(),
        ];

        if (null !== $product) {
            $params['product'] = $product->getId();
            $designer->setProduct($product);

            $personalization = $product->getFirstPersonalization();

            if (null !== $personalization) {
                $designer->setPersonalization($personalization);
            } else {
                throw new RuntimeException('Product personalization not found.');
            }
        }

        // Set designer in debug modus for environments dev / test.
        if (\in_array($this->container->get('kernel')->getEnvironment(), ['dev', 'test'])) {
            $designer->setDebugMode(true);
        }

        $designer->setUploadPath($this->buildRouteUrl('admin_designer_designer_edit', $params));
        $designer->setApiUrl($this->buildRouteUrl('admin_designer_designer_index', $params));

        if ($request->getMethod() === Request::METHOD_POST) {
            return new JsonResponse($designer->processUpload());
        }

        return [
            'designer' => $designer,
        ];
    }

    /**
     * @Route("/ontwerpen/{uuid}/{product}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|__UUID__",
     *     "product":"\d+|__PRODUCT__"
     * }, methods={"GET", "POST"}, defaults={"product" = null})
     * @Template("@Admin/Designer/Designer/edit.html.twig")
     *
     * @param Request $request
     * @param string $uuid
     * @param Product|null $product
     * @param bool $screenshotModus
     *
     * @return array|JsonResponse
     * @throws Exception
     */
    public function edit(Request $request, $uuid, Product $product = null, bool $screenshotModus = false)
    {
        if ($product === '__PRODUCT__' || $uuid === '__UUID__') {
            throw new InvalidParameterException('PRODUCT and UUID parameters are for templating only');
        }

        $designer = $this->get('app.designer');
        $router = $this->get('router');

        $designer->setUuid($uuid);

        if($request->get('clone') === '1'
            && $request->getMethod() === Request::METHOD_GET) {
            $uuid = $designer->cloneDesign();
            $designer->setUuid($uuid);
        }

        $designer->setIsPreDesign((bool)$request->get('is-pre-design'));
        $designer->setCallback($request->get('callback'));
        $designer->setScreenshotModus($screenshotModus);
        $designer->setConvertOnProcess(true);

        if($designer->isPreDesign()) {
            $designer->setTemplateUuid($uuid);
        }

        $params = [
            'uuid' => $uuid,
            'is-pre-design' => $designer->isPreDesign(),
            'clone' => $request->get('clone')
        ];

        if (null !== $product) {
            $params['product'] = $product->getId();
            $designer->setProduct($product);

            try {
                $productProperty = $product->findProductProperty('personalization');
            } catch (Exception $e) {
                $productProperty = null;
            }

            if($product->isCombination() && $product->hasPersonalization()) {
                $designer->setPersonalization($product->getPersonalizations()->first());
            } else if (null !== $productProperty && $productProperty->getProductVariation()) {
                $designer->setPersonalization($productProperty->getProductVariation());
            } else {
                throw new RuntimeException('Product personalization not found.');
            }
        }

        // Set designer in debug modus for environments dev / test.
        if (\in_array($this->container->get('kernel')->getEnvironment(), ['dev', 'test'])) {
            $designer->setDebugMode(true);
        }

        $designer->setUploadPath($this->buildRouteUrl('admin_designer_designer_edit', $params));
        $designer->setApiUrl($this->buildRouteUrl('admin_designer_designer_index', $params));

        if ($request->getMethod() === Request::METHOD_POST) {
            $convertUrl = $router->generate('admin_designer_designer_screenshot', $params, UrlGeneratorInterface::ABSOLUTE_URL);

            $designer->setConvertUrl($convertUrl);

            return new JsonResponse([
                'imageUrl' => $designer->processUpload()
            ]);
        }

        return [
            'designer' => $designer,
        ];
    }

    /**
     * @param string $route
     * @param array  $params
     *
     * @return string
     *
     * @throws Exception
     */
    private function buildRouteUrl(string $route = null, array $params = []): string
    {
        return $this->getRouterGenerator()->generate($route, $params,
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @return UrlGeneratorInterface
     *
     * @throws Exception
     */
    private function getRouterGenerator()
    {
        $router = $this->container->get('router');
        return $router->getGenerator();
    }

}
