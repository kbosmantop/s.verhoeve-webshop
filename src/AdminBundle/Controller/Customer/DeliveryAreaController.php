<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\Supplier\RegionFormType;
use AdminBundle\Form\Supplier\SupplierFormType;
use AppBundle\Connector\AbstractConnector;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Interfaces\DeliveryArea\DatasetInterface;
use function array_pop;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use PDO;
use PHPExcel_IOFactory;
use ReflectionClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Exceptions\DeliveryArea\DeliveryAreaFilterException;

/**
 * @Route("/bezorggebieden")
 */
class DeliveryAreaController extends Controller
{
    /**
     * @return array
     */
    public static function getControllerRoles()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bezorggebieden';
    }

    /**
     * @var string
     */
    private const DEFAULT_DATASET = 'CoverageBakkers';

    /**
     * @param Request $request
     * @return string
     */
    private function determineDataset(Request $request)
    {
        if ($request->get('dataset')) {
            return $request->get('dataset');
        } elseif ($this->get('session')->get('dataset')) {
            return $this->get('session')->get('dataset');
        } else {
            return self::DEFAULT_DATASET;
        }
    }

    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function indexAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted($this->getRolesFor());

        $dataset = $this->determineDataset($request);
        $deliveryAreaService = $this->get('delivery_area');
        $dataset = $deliveryAreaService->getDatasetClass($dataset);

        $viewModus = $request->get('view-modus', 'default');
        $deliveryAddress = $request->get('delivery-address');
        $supplierGroup = $request->get('supplier-group', $dataset::DEFAULT_DELIVERY_GROUP);
        $supplierConnectors = $request->get('supplier-connectors', []);
        $suppliers = $request->get('suppliers', []);
        $intervals = $request->get('intervals', []);

        $deliveryAreaService = $this->get('delivery_area');

        $reflection = new ReflectionClass($dataset);
        $dataset = $deliveryAreaService->getDatasetClass($reflection->getShortName());

        $template = '@Admin/Customer/DeliveryArea/index.html.twig';

        if($viewModus === 'select-supplier') {
            $template = '@Admin/Customer/DeliveryArea/select-supplier.html.twig';
        }

        return $this->render($template, [
            'page_title' => 'Bezorggebieden',
            'viewModus' => $viewModus,
            'deliveryAddress' => $deliveryAddress,
            'filters' => [
                'supplierGroup' => $supplierGroup,
                'supplierConnectors' => $supplierConnectors,
                'suppliers' => $suppliers,
                'intervals' => $intervals,
            ],
            'currentDataset' => $dataset,
            'datasets' => $deliveryAreaService->getDatasetClasses(),
            'visualGoogleApiKey' => $this->getParameter('visual_google_api_key'),
            'connectorColors' => $deliveryAreaService->getConnectorColors(),
        ]);
    }

    /**
     * @Route("/get-pointers", methods={"GET"})
     * @param Request $request
     * @return StreamedResponse|JsonResponse
     */
    public function getPointersAction(Request $request)
    {
        $supplierGroup = $request->get('supplierGroup');

        $deliveryAreaService = $this->get('delivery_area');

        $suppliers = $deliveryAreaService->getSuppliers($supplierGroup);

        if ($suppliers === null) {
            return new JsonResponse([]);
        }

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->setCallback(function () use ($suppliers) {
            print '[';

            $i = 0;

            // loop trough suppliers.
            foreach ($suppliers as $supplier) {
                // loop through establishments
                $establishments = $supplier->getEstablishments();

                if (null === $establishments || $establishments->isEmpty()) {
                    continue;
                }

                foreach ($establishments as $establishment) {
                    /** @var AbstractConnector $connector */
                    $connector = $this->get(ConnectorHelper::class)->getConnector($supplier);

                    if ($establishment->getAddress() === null) {
                        continue;
                    }

                    try {
                        $point = $establishment->getAddress()->getPoint();
                    } catch (\Exception $exception) {
                        continue;
                    }

                    if ($point === null) {
                        continue;
                    }

                    if ($i > 0) {
                        print ',';
                    }

                    try {
                        $color = $connector->getColor();
                    } catch (\Exception $e) {
                        $color = '#000000';
                    }

                    if (!$establishment->isMain()) {
                        $color = '#673ab7';
                    }

                    $color = urlencode($color);

                    $iconType = strtolower($supplier->getSupplierConnector());
                    $icon = "/admin/bezorggebieden/get-pointer/{$iconType}/{$color}/icon.svg";

                    print json_encode([
                        'icon' => $icon,
                        'id' => $supplier->getId(),
                        'company' => [
                            'id' => $supplier->getId(),
                            'uuid' => $supplier->getUuid(),
                            'name' => $supplier->getName(),
                        ],
                        'main' => $establishment->isMain(),
                        'name' => $establishment->getName(),
                        'address' => $establishment->getAddress()->getStreet() . ' ' . $establishment->getAddress()->getNumber(),
                        'postcode' => $establishment->getAddress()->getPostcode(),
                        'city' => $establishment->getAddress()->getCity(),
                        'phone' => $establishment->getAddress()->getFormattedPhoneNumber(),
                        'country' => $establishment->getAddress()->getCountry()->getCode(),
                        'lat' => $point->getLatitude(),
                        'lng' => $point->getLongitude(),
                        'connector' => $supplier->getSupplierConnector(),
                    ]);

                    flush();

                    $i++;
                }
            }

            print ']';
        });

        return $response;
    }

    /**
     * @Route("/get-postcodes-within-bounds", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws DBALException
     */
    public function getPostcodesWithinBoundsAction(Request $request)
    {
        $bounds = $request->get('bounds');
        $deliveryAreaService = $this->get('delivery_area');
        $postcodesWithinBounds = $deliveryAreaService->getPostcodesWithinBounds($bounds);

        return new JsonResponse($postcodesWithinBounds);
    }

    /**
     * @Route("/get-dataset", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getDatasetAction(Request $request)
    {
        $data['regions'] = [];

        $deliveryAreaService = $this->get('delivery_area');

        if ($request->get('dataset')) {
            $this->get('session')->set('dataset', $request->get('dataset'));

            /** @var DatasetInterface $dataset */
            $dataset = $deliveryAreaService->getDatasetClass($request->get('dataset'));



            $filter = new \stdClass();
            $filter->suppliers = $request->get('suppliers', []);
            $filter->supplierGroup = $request->get('supplierGroup', null);
            $filter->supplierConnectors = $request->get('supplierConnectors', []);
            $filter->date = $request->get('date');
            $filter->intervals = $request->get('intervals', []);

            if(empty($filter->supplierGroup)) {
                $filter->supplierGroup = $dataset::DEFAULT_DELIVERY_GROUP;
            }

            $data = $dataset->{$dataset->getEvent()}($filter);

            $suppliers = $deliveryAreaService->getSuppliers($filter->supplierGroup);

            // Add suppliers without data to the filters.
            if ($suppliers !== null) {
                foreach ($suppliers as $supplier) {
                    if (!isset($data['suppliers'][$supplier->getId()])) {
                        $data['suppliers'][$supplier->getId()] = $supplier->getName();
                    }
                }
            }

            $data['supplierGroups'] = $dataset->getSupplierGroups();
            $data['supplierConnectors'] = $dataset->getSupplierConnectors();
            $data['legend'] = $dataset->getLegend();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/get-region/{postcode}/{country}/", methods={"GET", "POST"})
     * @param Request $request
     * @param null    $postcode
     * @param string  $country
     * @return JsonResponse|array
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @Template()
     */
    public function getRegionAction(Request $request, $postcode = null, $country = 'NL')
    {
        $deliveryAreaService = $this->get('delivery_area');
        /** @var Postcode $postcode */
        $postcode = $deliveryAreaService->getPostcode($country, (int)$postcode);

        if ($result = $this->handleSupplierSearch($request, $postcode)) {
            return new JsonResponse($result);
        }

        $form = $this->getRegionForm($request, $postcode);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $postcode = $form->getData();

            try {
                $em = $this->getDoctrine()->getManager();

                // Needed for saving re-added filtered delivery areas
                $em->persist($postcode);
                $em->flush();
            } catch (UniqueConstraintViolationException $e) {
                $form->addError(new FormError('Één of meerdere postcode / leverancier combinaties komen al voor.'));
            }

            if ($form->getErrors()->count() === 0) {
                return new JsonResponse();
            }
        }

        return [
            'form' => $form->createView(),
            'postcode' => $postcode,
            'errors' => $form->getErrors(),
        ];

    }

    /**
     * @Route("/get-region-info/{postcodeString}/{countryString}/", methods={"GET"})
     * @param Request $request
     * @param int     $postcodeString
     * @param string  $countryString
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getRegionInfoAction(Request $request, int $postcodeString, string $countryString = 'NL')
    {
        $reqSupplierId = $request->get('supplierId');

        $deliveryAreaService = $this->get('delivery_area');

        /** @var Postcode $postcode */
        $postcode = $deliveryAreaService->getPostcode($countryString, $postcodeString);

        $supplier = $this->getDoctrine()->getRepository(Company::class)->find($reqSupplierId);

        /** @var SupplierGroup $supplierGroup */
        $supplierGroup = $deliveryAreaService->getRequestedSupplierSupplierGroup($supplier);

        [$priceFormatted, $priceInclFormatted, $vat, $interval] = $deliveryAreaService->calcRegionPrices($supplierGroup, $postcode);

        return new JsonResponse([
            'id' => $postcode->getId(),
            'postcode' => $postcode->getPostcode(),
            'label' => $postcode->getLabel(),
            'defaultPrice' => $priceFormatted,
            'defaultPriceIncl' => $priceInclFormatted,
            'defaultVat' => $vat,
            'defaultInterval' => $interval,
        ]);
    }

    /**
     * @Route("/get-supplier-info/{supplier}", methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param Company $supplier
     * @return array|Response|JsonResponse
     */
    public function getSupplierInfoAction(Request $request, Company $supplier)
    {
        $form = $this->getSupplierForm($supplier);

        if ($result = $this->handlePostcodeSearch($request)) {
            return new JsonResponse($result);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new Response();
        }

        return [
            'company' => $supplier,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return object|null
     */
    private function handlePostcodeSearch(Request $request)
    {
        if ($request->get('name') && $request->get('search')) {
            $postcode = $request->get('search');

            /** @var EntityRepository $entityRepository */
            $entityRepository = $this->getDoctrine()->getManager()->getRepository(Postcode::class);

            $qb = $entityRepository->createQueryBuilder('postcode')
                ->andWhere('postcode.postcode LIKE :postcode')
                ->setParameter('postcode', '%' . $postcode . '%');

            $items = [];

            /** @var Postcode[] $entities */
            $entities = $qb->getQuery()->getResult();

            foreach ($entities as $entity) {
                $label = (string)$entity;

                if ($entity->getCities() !== null) {
                    $label .= ' ' . $entity->getCities()->first();
                }

                $items[] = ['id' => $entity->getId(), 'text' => trim($label)];
            }

            return (object)[
                'items' => $items,
            ];
        }

        return null;
    }

    /**
     * @param Request  $request
     * @param Postcode $postcode
     * @return null|object
     * @throws \Exception
     */
    private function handleSupplierSearch(Request $request, Postcode $postcode)
    {
        if ($request->get('name') && $request->get('search')) {
            $supplierName = $request->get('search');
            $supplierGroup = $request->get('supplierGroup', null);

            $qb = $this->getDoctrine()
                ->getManager()
                ->getRepository(Company::class)
                ->createQueryBuilder('supplier')
                ->leftJoin('supplier.supplierGroups', 'supplierGroup')
                ->andWhere('supplierGroup.name = :supplierGroup')
                ->setParameter('supplierGroup', $supplierGroup)
                ->andWhere('supplier.name LIKE :supplier')
                ->setParameter('supplier', '%' . $supplierName . '%')
                ->andWhere('supplier.isSupplier = 1');

            $items = [];

            /** @var Company[] $entities */
            $suppliers = $qb->getQuery()->getResult();

            $supplierGroup = $this->getDoctrine()->getRepository(SupplierGroup::class)->findOneBy([
                'name' => $supplierGroup,
            ]);

            $deliveryAreaService = $this->get('delivery_area');

            [$priceFormatted, $priceInclFormatted, $vat, $interval] = $deliveryAreaService->calcRegionPrices($supplierGroup, $postcode);

            /** @var Company $supplier */
            foreach ($suppliers as $supplier) {

                $label = $supplier->getName() . ' (' . $supplier->getId() . ')';

                $connector = $supplier->getSupplierConnector();
                if (!empty($connector)) {
                    $label = "%%{$connector}%%" . $label;
                }

                $items[] = [
                    'id' => $supplier->getId(),
                    'text' => trim($label),
                    'priceFormatted' => $priceFormatted,
                    'priceInclFormatted' => $priceInclFormatted,
                    'vat' => $vat,
                    'interval' => $interval,
                ];
            }

            return (object)[
                'items' => $items,
            ];
        }

        return null;
    }

    /**
     * @param Company $supplier
     * @return Form|FormInterface
     */
    private function getSupplierForm(Company $supplier)
    {
        $form = $this
            ->createForm(SupplierFormType::class, $supplier, [
                'attr' => [
                    'id' => 'form_edit_supplier_regions',
                ],
                'action' => $this->generateUrl('admin_customer_deliveryarea_getsupplierinfo', [
                    'supplier' => $supplier->getId(),
                ]),
            ]);

        return $form;
    }

    /**
     * @param Request  $request
     * @param Postcode $postcode
     * @return Form|FormInterface
     */
    private function getRegionForm(Request $request, Postcode $postcode)
    {
        $form = $this->createForm(RegionFormType::class, $postcode, [
            'attr' => [
                'id' => 'form_edit_region',
            ],
            'action' => $this->generateUrl('admin_customer_deliveryarea_getregion', [
                'postcode' => $postcode->getPostcode(),
                'supplierGroup' => $request->get('supplierGroup'),
            ]),
        ]);

        return $form;
    }

    /**
     * @Route("/check-version", methods={"GET"})
     */
    public function getLastVersion()
    {
        /** @var EntityManagerInterface $em */
        $em = $this->get('doctrine.orm.entity_manager');

        /** @var EntityRepository $er */
        $er = $em->getRepository(Postcode::class);

        /** @var QueryBuilder $qb */
        $qb = $er->createQueryBuilder('postcode');

        $qb->select('postcode.updatedAt');
        $qb->setMaxResults('1');
        $qb->orderBy('postcode.updatedAt', 'DESC');

        $response = new \stdClass();
        try {
            $result = $qb->getQuery()->getSingleScalarResult();

            $dateTime = new \DateTime($result);
            $response->status = 'success';
            $response->result = $dateTime->getTimestamp();

        } catch (\Exception $exception) {
            $response->status = 'failed';
        }

        return new JsonResponse($response);

    }

    /**
     * @Route("/get-near-regions/{lat}/{lng}/{distance}", methods={"GET"})
     * @param float $lat
     * @param float $lng
     * @param int   $distance
     * @return JsonResponse
     * @throws DBALException
     */
    public function getNearRegionsAction(float $lat, float $lng, int $distance = 8)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Connection $connection */
        $connection = $em->getConnection();

        $sql = 'SELECT post.id, ROUND((6371*acos(cos(radians(:lat1))*cos(radians(Y(poge.point)))*cos(radians(X(poge.point))-radians(:lng))+sin(radians(:lat2))*sin(radians(Y(poge.point))))), 0) AS distance
            , Y(poge.point) AS lat
            , X(poge.point) AS lng
            , AsText(poge.multipolygon) AS geometry
            , post.postcode
            , prov.country

            FROM postcode post
            LEFT JOIN postcode_geometry poge ON post.geometry_id = poge.id
            INNER JOIN province prov ON post.province_id = prov.id
            WHERE `point` IS NOT NULL
            HAVING distance < :distance
        ';

        $statement = $connection->prepare($sql);

        $distance += 2;

        $statement->bindValue(':lat1', $lat, PDO::PARAM_STR);
        $statement->bindValue(':lat2', $lat, PDO::PARAM_STR);
        $statement->bindValue(':lng', $lng, PDO::PARAM_STR);
        $statement->bindValue(':distance', (int)$distance, PDO::PARAM_INT);

        $statement->execute();

        return new JsonResponse($statement->fetchAll());
    }

    /**
     * @Route("/get-regions", methods={"GET"})
     */
    public function getRegionsAction()
    {
        @ob_end_clean();
        ob_implicit_flush();

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getRepository(Postcode::class);

        $qb = $entityRepository->createQueryBuilder('postcode')
            ->andWhere('postcode.geometry IS NOT NULL');

        $iterableResult = $qb->getQuery()->iterate();

        if (!is_iterable($iterableResult) && !$iterableResult->next()) {
            return new JsonResponse([]);
        }

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->setCallback(function () use ($iterableResult) {
            $i = 0;

            print '[';

            foreach ($iterableResult as $row) {
                set_time_limit(1);

                /** @var Postcode $postcode */
                $postcode = $row[0];

                if (!$postcode) {
                    continue;
                }

                if ($i > 0) {
                    print ',';
                }

                print json_encode([
                    'country' => $postcode->getCountry()->getCode(),
                    'postcode' => (int)$postcode->getPostcode(),
                    'geometry' => 'POLYGON' . (string)$postcode->getGeometry()->getMultipolygon(),
                    'lat' => $postcode->getGeometry()->getPoint()->getLatitude(),
                    'lng' => $postcode->getGeometry()->getPoint()->getLongitude(),
                ]);

                flush();
                $i++;

                $this->getDoctrine()->getManager()->clear();
            }

            print ']';
        });

        return $response;
    }

    /**
     * @Route("/get-export-postcodes/{company}", methods={"GET"})
     * @param Company $company
     * @return StreamedResponse
     * @throws \PHPExcel_Exception
     */
    public function getExportPostcodesAction(Company $company)
    {
        $deliveryAreaService = $this->get('delivery_area');
        $excel = $deliveryAreaService->getSupplierExcelExport($company);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($excel) {
            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save('php://output');
        });

        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition',
            'attachment;filename=' . $company->getName() . ' bezorggebieden ' . date('d-m-Y') . '.xlsx');

        return $response;
    }

    /**
     * @Route("/get-pointer/{type}/{color}/icon.svg", methods={"GET"})
     * @param string $type
     * @param string $color
     * @return Response
     */
    public function getPointerIcon($type = 'general', $color = '000000')
    {
        switch ($type) {
            case 'delivery-address':
                $data = $this->renderView('AdminBundle:Customer/DeliveryArea:getDeliveryAddressIcon.svg.twig', [
                    'color' => urldecode($color),
                ]);

                break;
            case 'florist':
            default:
                $data = $this->renderView('AdminBundle:Customer/DeliveryArea:getPointerIcon.svg.twig', [
                    'color' => urldecode($color),
                ]);
        }

        $response = new Response($data);
        $response->headers->set('Content-Type', 'image/svg+xml');

        return $response;
    }
}
