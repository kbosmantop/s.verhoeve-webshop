<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\App\SubscriptionType as SubscriptionTypeDatatable;
use AdminBundle\Form\App\SubscriptionType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\Subscription;
use Recurr;
use Recurr\Transformer\ArrayTransformerConfig;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/abonnementen")
 */
class SubscriptionController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Abonnementen';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Subscription::class,
            'form_type' => SubscriptionType::class,
            'datatable' => [
                'entity_class' => Subscription::class,
                'form_type' => SubscriptionType::class,
                'datatable_type' => SubscriptionTypeDatatable::class,
            ],
        ];
    }

    /**
     * @Route("/{subscription}", requirements={"subscription" = "\d+"}, methods={"GET"})
     * @Template()
     * @param $subscription
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function viewAction($subscription)
    {
        $recurrences = $this->getRecurrences($subscription, new \DateTime());
        $orderlist = [];

        foreach ($this->getOrders($subscription) as $order) {
            $date = ($order->getSubscriptionDate() ? $order->getSubscriptionDate()->format("Ymd") : null);

            if (!array_key_exists($date, $orderlist)) {
                $orderlist[$date] = (object)[
                    "datetime" => $order->getSubscriptionDate(),
                    "orders" => [],
                ];
            }

            array_push($orderlist[$date]->orders, $order);
        }

        $events = [];

        foreach ($orderlist as $orders) {
            array_push($events, (object)[
                "title" => implode(", ", array_map(function (OrderCollection $orderCollection) {
                    return '<a href="#">' . $orderCollection->getNumber() . '</a>';
                }, $orders->orders)),
                "start" => ($orders->datetime ? $orders->datetime->format("Y-m-d") : null),
            ]);
        }

        foreach ($recurrences as $recurrence) {
            array_push($events, (object)[
                "title" => '<i class="icon-truck"></i>',
                "start" => ($recurrence->getStart() ? $recurrence->getStart()->format("Y-m-d") : null),
            ]);
        }

        return [
            "orders" => $orderlist,
            "subscription" => $subscription,
            "exdates" => $this->getExDates($subscription),
            "frequency" => $subscription->getRruleAsText(),
            "recurrences" => $recurrences,
            "events" => $events,
            "page_title" => $subscription->getDescription() . " (" . $subscription->getCompany()->getName() . ")",
        ];
    }

    private function getOrders(Subscription $subscription)
    {
        return $this->getDoctrine()->getManager()->getRepository(OrderCollection::class)->findBy([
            "subscription" => $subscription,
        ]);
    }

    private function getExDates($subscription)
    {
        $exDates = [];

        foreach ($subscription->getSubscriptionInterruptions() as $interruption) {
            $startDate = clone $interruption->getStartDate();
            $startDate->setTime(0, 0, 0);

            $endDate = clone $interruption->getEndDate();
            $endDate = $endDate->modify('+1D');
            $endDate->setTime(23, 59, 59);

            $dateRange = new \DatePeriod($startDate, new \DateInterval('P1D'), $endDate);

            foreach ($dateRange as $date) {
                array_push($exDates, $date->format("Ymd")); //new Recurr\DateExclusion($date, false, false));
            }
        }

        return $exDates;
    }

    private function getRecurrences(Subscription $subscription, \DateTime $startDate = null)
    {
        $exDates = $this->getExDates($subscription);

        $rule = new Recurr\Rule($subscription->getRrule(), clone $subscription->getStartDate());
        $rule->setExDates($exDates);
        //$rule->setCount(100);

        $transformerConfig = new ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();

        $transformer = new Recurr\Transformer\ArrayTransformer();
        $transformer->setConfig($transformerConfig);

        if (!$startDate || $startDate <= $subscription->getStartDate()) {
            $startDate = $subscription->getStartDate();
        }

        $startDate->setTime(0, 0, 0);

        if (!$subscription->getEndDate()) {
            $constraint = new Recurr\Transformer\Constraint\AfterConstraint($startDate, true);
        } else {
            $endDate = clone $subscription->getEndDate();
            $endDate->setTime(23, 59, 59);

            $constraint = new Recurr\Transformer\Constraint\BetweenConstraint($startDate, $endDate, true);
        }

        /** functie moet controleren of de einddatum leidend is
         * //        if ($request->get("start") && $request->get("end")) {
         * //            $constraint = new Recurr\Transformer\Constraint\BetweenConstraint(new \DateTime($request->get("start")), new \DateTime($request->get("end")), true);
         * //        } */

        return $transformer->transform($rule, $constraint);
    }
}
