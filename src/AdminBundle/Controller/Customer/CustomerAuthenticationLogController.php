<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Customer\CustomerAuthenticationLogType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Customer\AuthenticationLog;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/klanten/authenticatie/logboek")
 * @author Robbert van Mourik
 *
 */
class CustomerAuthenticationLogController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Authenticatie Logboek';
    }

    public function getOptions()
    {
        return [
            'label' => 'Authenticatie Logboek',
            'form_type' => false,
            'entity_class' => AuthenticationLog::class,
            'datatable' => [
                'entity_class' => AuthenticationLog::class,
                'datatable_type' => CustomerAuthenticationLogType::class,
                'results_per_page' => 50,
                'form_type' => false,
                'item_actions' => false,
                'bulk_actions' => false,
                'order' => [
                    [1, 'desc']
                ]
            ],
        ];
    }
}
