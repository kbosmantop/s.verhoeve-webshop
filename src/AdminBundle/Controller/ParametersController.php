<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\Parameter\ParameterDatatableType;
use AdminBundle\Form\Parameter\SystemParameterFormType;
use AppBundle\Entity\Common\Parameter\Parameter;
use AppBundle\Form\Admin\ParameterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ParametersController
 * @package AdminBundle\Controller
 *
 * @Route("/instellingen/parameters")
 */
class ParametersController extends Controller
{
    protected $datatableOptions = [
        'datatable_type' => ParameterDatatableType::class,
        'form_type' => ParameterType::class,
        'entity_class' => Parameter::class,
        'allow_delete' => false,
        'allow_edit' => false,
    ];

    /**
     * @Route("/", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor());


        $dataTable = $this->container->get('admin.datatable.service')->getTable($this->datatableOptions);

        return $this->render(
            'AdminBundle:Crud:datatable.html.twig',
            [
                'datatable' => $dataTable,
            ]
        );
    }

    /**
     * @Route("/data", methods={"GET", "POST"})
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function dataTableDataAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor());

        $dataTable = $this->container->get('admin.datatable.service')->getData($this->datatableOptions);

        return $dataTable;
    }

    /**
     * @Route("/toevoegen", methods={"GET", "POST"})
     * @Template("@Admin/Parameters/form.html.twig")
     *
     * @param Request $request
     * @param null    $message
     * @return RedirectResponse | array
     */
    public function addAction(Request $request, $message = null)
    {
        $form = $this->createForm(ParameterType::class, new Parameter())
            ->add('submit', SubmitType::class, [
                'label' => 'Opslaan',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parameter = $form->getData();

            $em = $this->container->get('doctrine')->getManager();
            $em->persist($parameter);
            $em->flush();

            return $this->redirectToRoute('admin_parameters_index');
        }

        return [
            'form' => $form->createView(),
            'message' => $message,
        ];
    }

    /**
     * @Route("/systeem", methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     *
     * @return array
     */
    public function systemAction(Request $request)
    {
        $form = $this->createForm(SystemParameterFormType::class, null, []);

        $form->handleRequest($request);

        return [
            'form' => $form->createView(),
        ];
    }
}
