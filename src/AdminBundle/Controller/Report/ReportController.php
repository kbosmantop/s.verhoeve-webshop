<?php

namespace AdminBundle\Controller\Report;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Report\ReportChartDatatableType;
use AdminBundle\Datatable\Report\ReportDatatableType;
use AdminBundle\Form\Report\ReportChartFormType;
use AdminBundle\Form\Report\ReportDownloadFormType;
use AdminBundle\Form\Report\ReportFormType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportChart;
use AppBundle\Services\Report\ReportService;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleService;
use RuleBundle\Service\RuleTransformerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rapportages")
 * Class ReportController
 * @package AdminBundle\Report
 */
class ReportController extends CrudController
{
    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var RuleService
     */
    protected $ruleService;

    /**
     * @var RuleTransformerService
     */
    protected $ruleTransformer;

    /**
     * ReportController constructor.
     * @param ReportService          $reportService
     * @param EntityManagerInterface $entityManager
     * @param RuleService            $ruleService
     * @param RuleTransformerService $ruleTransformerService
     */
    public function __construct(ReportService $reportService, EntityManagerInterface $entityManager, RuleService $ruleService, RuleTransformerService $ruleTransformerService)
    {
        $this->reportService = $reportService;
        $this->entityManager = $entityManager;
        $this->ruleService = $ruleService;
        $this->ruleTransformer = $ruleTransformerService;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Report::class,
            'form_type' => ReportFormType::class,
            'datatable' => [
                'datatable_type' => ReportDatatableType::class,
                'entity_class' => Report::class,
                'form_type' => ReportFormType::class,
                'disable_filter' => true,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_report_report_wizard',
                        'label' => 'Profiel toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-download',
                        'route' => 'admin_report_report_download',
                        'type' => 'open-content',
                        'method' => 'GET',
                    ],
                ],
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->leftJoin(CompanyReport::class, 'cr', Join::WITH, 'cr.report = report.id');
                    $qb->where('cr.report IS NULL');
                },
            ],
            'view' => [
                'label_subject' => 'Rapportage: %%title%%',
                'panels' => [
                    [
                        'id' => 'chart_table',
                        'type' => 'datatable',
                        'title' => 'Grafieken',
                        'datatable' => [
                            'datatable_type' => ReportChartDatatableType::class,
                            'entity_class' => ReportChart::class,
                            'form_type' => ReportChartFormType::class,
                            'base_route' => 'admin_report_reportchart',
                            'allow_read' => false,
                            'item_actions' => [
                                [
                                    'icon' => 'mdi mdi-chart-line',
                                    'route' => 'admin_report_reportchart_displaychart',
                                    'type' => 'open-content',
                                    'method' => 'GET',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/genereer/{report}", methods={"GET"})
     *
     * @param Report $report
     *
     * @return BinaryFileResponse
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \Exception
     */
    public function generateAction(Report $report)
    {
        return $this->reportService->generate($report);
    }

    /**
     * @Route("/{id}/downloaden", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Report $id
     *
     * @return array|BinaryFileResponse|JsonResponse
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \ReflectionException
     */
    public function downloadAction(Request $request, Report $id)
    {
        $report = $id;

        $form = $this->createForm(ReportDownloadFormType::class);

        if (parent::getRequest()->get('name') && parent::getRequest()->get('search')) {
            return $this->autoComplete($form);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $parameters = [];
            foreach ($form->getData() as $key => $data) {
                if ($data !== null) {
                    $parameters[$key] = $data;
                }
            }

            return $this->reportService->generate($report, $parameters);
        }

        return [
            'report' => $report,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/wizard", methods={"GET"})
     * @Template()
     *
     * @return array
     */
    public function wizardAction()
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Rule::class)->createQueryBuilder('r');
        $qb->innerJoin(Report::class, 'report')
            ->andWhere('r.id = report.rule');

        /** @var Rule[] $rules */
        $rules = $qb->getQuery()->getResult();

        $rulesArray = [];
        foreach ($rules as $rule) {
            $rulesArray[$rule->getId()] = $rule->getDescription();
        }

        return [
            'rules' => $rulesArray,
        ];
    }

    /**
     * @Route("/aanmaken", methods={"GET", "POST"})
     * @Template("@Admin/Report/Report/create-rule.html.twig")
     *
     * @param Request $request
     * @return RedirectResponse|array
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $entities = $this->entityManager->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            /** @var Rule $rule */
            $rule = $this->ruleService->store($request, true);

            if (!($rule instanceof Rule)) {
                throw new \Exception('Returned response was not a rule instance');
            }

            $report = new Report();
            $report->setRule($rule);
            $report->setTitle($rule->getDescription());

            $this->entityManager->persist($report);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_report_report_edit', ['id' => $report->getId()]);
        }

        $inputs = [
            ResolverService::QB_ALIAS => $this->entityManager->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => Order::class,
            ]),
        ];

        return [
            'filters' => $this->ruleTransformer->getFilters($inputs),
            'entities' => $entities,
        ];
    }

    /**
     * @Route("/aanmaken-met-regel", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createFromExistingAction(Request $request)
    {
        /** @var Rule $rule */
        $rule = $this->entityManager->getRepository(Rule::class)->find($request->get('rule'));

        if ($rule) {
            $report = new Report();
            $report->setTitle($request->get('title'));
            $report->setRule($rule);

            $this->entityManager->persist($report);
            $this->entityManager->flush();

            return new JsonResponse([
                'refreshUrl' => $this->generateUrl('admin_report_report_edit', ['id' => $report->getId()]),
            ]);
        }

        return new JsonResponse([], 500);
    }

}
