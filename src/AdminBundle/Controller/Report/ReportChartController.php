<?php

namespace AdminBundle\Controller\Report;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Report\ReportChartDatatableType;
use AdminBundle\Form\Report\ReportChartFormType;
use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportChart;
use AppBundle\Entity\Report\ReportChartData;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rapportages/grafieken")
 * Class ReportChartController
 * @package AdminBundle\Controller
 */
class ReportChartController extends CrudController {

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'form_type' => ReportChartFormType::class,
            'entity_class' => ReportChart::class,
            'datatable' => [
                'datatable_type' => ReportChartDatatableType::class,
                'entity_class' => ReportChart::class,
                'form_type' => ReportChartFormType::class,
            ],
        ];
    }

    /**
     * @Route("/{id}/grafiek", methods={"GET"})
     * @Template()
     *
     * @param ReportChart $id
     * @return array
     */
    public function displayChartAction(ReportChart $id)
    {
        $reportChart = $id;

        $data = [];
        $labels = [];

        /** @var ReportChartData $reportChartData */
        foreach($reportChart->getReportChartData() as $reportChartData) {
            if(empty($reportChartData->getData())) {
                continue;
            }

            if(empty($labels)) {
                $labels = $reportChartData->getData()['label'];
            }

            $chartData = [
                'label' => $reportChartData->getDataLabel(),
                'data' => $reportChartData->getData()['data']
            ];

            $data[] = $chartData;
        }

        return [
            'labels' => $labels,
            'data' => $data
        ];
    }
}
