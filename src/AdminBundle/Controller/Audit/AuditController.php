<?php

namespace AdminBundle\Controller\Audit;

use AdminBundle\Controller\Controller;
use AdminBundle\Datatable\Audit\AuditType;
use AdminBundle\Services\AuditLog\AuditLogService;
use AdminBundle\Services\DataTable\DataTableService;
use AdminBundle\Services\Router\BaseRouteService;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/systeemlogs")
 * Class AuditController
 * @package AdminBundle\Controller\Audit
 */
class AuditController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     * @Template
     */
    public function indexAction()
    {
        $dataTable = $this->container->get(DataTableService::class)->getTable($this->getOptions()['datatable']);

        return [
            'dataTable' => $dataTable,
        ];
    }

    /**
     * @Route("/data")
     */
    public function dataTableData()
    {
        $data = $this->container->get(DataTableService::class)->getData($this->getOptions()['datatable']);

        return $data;
    }

    /**
     * @Route("/diff/{id}", methods={"GET"})
     * @Template("@Admin/Audit/Audit/log.html.twig")
     * @param AuditLog $log
     * @return array
     */
    public function diffAction(AuditLog $log)
    {
        $decoratedLogs = $this->container->get(AuditLogService::class)->getLogs($log);

        return [
            'logs' => $decoratedLogs,
            'selectedLogId' => $log->getId(),
        ];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => AuditLog::class,
            'datatable' => [
                'entity_class' => AuditLog::class,
                'datatable_type' => AuditType::class,
                'form_type' => false,
                'actions_processing' => 'override',
                'item_actions' => [
                    [
                        "type" => "view",
                        "icon" => 'icon-eye',
                        "attr" => [
                            'data-action' => 'open-log',
                        ],
                        'route' => $this->container->get(BaseRouteService::class)->getBaseRouteForAction('diff'),
                    ],
                ],
                'results_per_page' => 20,
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->orderBy('audit_logs.id', 'DESC');
                },
            ],
            'form_type' => false,
            'assets' => [
                'admin/js/form/audit.js',
            ],

        ];
    }
}
