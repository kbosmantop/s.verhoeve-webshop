<?php

namespace AdminBundle\Controller\Management;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/beheer/routes")
 * @author Robbert van Mourik
 *
 */
class RouteController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Routes';
    }

    public function getOptions()
    {
        return [
            'label' => 'Route',
            'datatable' => [
                'datatable_type' => 'AdminBundle\Datatable\Management\RouteType',
            ],
        ];
    }

    /**
     * @return ArrayCollection
     */
    public function getCollection()
    {
        $collection = new ArrayCollection();

        foreach ($this->container->get('router')->getRouteCollection()->all() as $routeName => $route) {
            // ignore all service routes like assetic, web_profiler, twig
            if (strpos($route->getDefault('_controller'), ".") !== false) {
                continue;
            }

            $object = new \stdClass();
            $object->name = $routeName;
            $object->controller = $route->getDefault('_controller');
            $object->method = $route->getMethods() ? implode(",", $route->getMethods()) : 'ANY';
            $object->scheme = $route->getSchemes() ? implode(",", $route->getSchemes()) : 'ANY';
            $object->host = $route->getHost() ? $route->getHost() : 'ANY';
            $object->path = $route->getPath();

            $collection[] = $object;
        }

        return $collection;
    }
}
