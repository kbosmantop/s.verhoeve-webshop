<?php

namespace AdminBundle\Controller\Examples\Crud;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * @Route("/examples/crud/doctrine-entity")
 */
class DoctrineEntityController extends CrudController implements CrudControllerInterface
{

    public function getLabel($n = null)
    {
        return 'Test Doctrine Entity';
    }

//    public function getOptions()
//    {
//        return array(
//            'label' => 'Product',
//            'entity_class' => 'AppBundle\Entity\Catalog\Product\Product',
//            'form_type' => 'AdminBundle\Form\Catalog\ProductType',
//            'assets' => array(
//                'admin/js/form/product.js',
//                'admin/css/form/product.css'
//            ),
//            'datatable_type' => 'AdminBundle\Datatable\Catalog\ProductType',
//        );
//    }
}
