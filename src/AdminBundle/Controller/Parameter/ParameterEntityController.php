<?php

namespace AdminBundle\Controller\Parameter;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Parameter\ParameterEntityDatatableType;
use AdminBundle\Form\Parameter\ParameterEntityFormType;
use AppBundle\Entity\Common\Parameter\ParameterEntity;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/parameters-entities")
 * Class ParameterEntityController
 * @package AdminBundle\Controller\Parameter
 */
class ParameterEntityController extends CrudController
{

    protected $datatableOptions = [
        'datatable_type' => ParameterEntityDatatableType::class,
        'form_type' => ParameterEntityFormType::class,
        'entity_class' => ParameterEntity::class,
        'allow_delete' => false,
        'allow_edit' => true,
    ];

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'form_type' => ParameterEntityFormType::class,
            'entity_class' => ParameterEntity::class,
            'datatable' => $this->datatableOptions,
        ];
    }
}
