<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\StockDatatableType;
use AdminBundle\Form\StockFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Manager\StockManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/voorraadbeheer")
 */
class StockController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Voorraadbeheer';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'form_type' => StockFormType::class,
            'datatable' => [
                'datatable_id' => 'stock_manager',
                'disable_parent' => true,
                'entity_class' => GenericProduct::class,
                'datatable_type' => StockDatatableType::class,
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-pencil',
                        'route' => 'admin_stock_managestock',
                        'type' => 'open-content',
                        'attr' => [
                            'data-action' => 'edit',
                            'data-popup' => 'tooltip',
                            'title' => '',
                            'data-original-title' => 'Bewerken',
                        ],
                        'identifier' => [
                            'field' => 'product',
                            'id' => 'id',
                        ],
                        'method' => 'GET',
                        'override_propagation' => true,
                    ],
                    [
                        'icon' => 'mdi mdi-av-timer',
                        'route' => 'admin_stock_displaymanagestocklog',
                        'type' => 'open-content',
                        'attr' => [
                            'data-action' => 'log',
                            'data-popup' => 'tooltip',
                            'title' => '',
                            'data-original-title' => 'Geschiedenis',
                        ],
                        'identifier' => [
                            'field' => 'product',
                            'id' => 'id',
                        ],
                        'method' => 'GET',
                        'override_propagation' => true,
                    ]
                ],
                'alias' => 'product',
                'query_builder' => function (QueryBuilder $qb) {
                    $orX = $qb->expr()->orX(
                        $qb->expr()->eq('product.hasStock', true),
                        $qb->expr()->eq('variation.hasStock', true)
                    );

                    $qb->andWhere($orX)
                        ->leftJoin('product.variations', 'variation');

                    return $qb;
                },
                'allow_edit' => true,
                'allow_delete' => false,
            ],
        ];
    }

    /**
     * @Route("/")
     * @Template()
     *
     * @return array|JsonResponse
     */
    public function index()
    {
        $stockManager = $this->container->get(StockManager::class);
        $stockManager->disableCache();

        return [
            'products' => $stockManager->getProducts(),
        ];
    }

    /**
     * @return Product[]|\Doctrine\Common\Collections\ArrayCollection
     */
    public function getCollection(){
        $stockManager = $this->container->get(StockManager::class);
        $stockManager->disableCache();
        return $stockManager->getProducts();
    }

    /**
     * @Route("/{product}/bewerken", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Product $product
     * @return array|Response
     *
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function manageStock(Request $request, Product $product)
    {
        $form = $this->getStockForm($product);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $stockFormData = $request->get('stock_form');

            if (null === $stockFormData) {
                $form->addError(new FormError('Stock data not found.'));
            }

            if ($form->isValid()) {

                $stockManager = $this->container->get(StockManager::class);
                $stockManager->disableCache();

                $mutation = (int)($stockFormData['mutation'] ?? 0);
                $threshold = (int)($stockFormData['threshold'] ?? null);

                if ($mutation > 0) {
                    $stockManager->addPhysicalStock($product, $mutation);
                } elseif ($mutation < 0) {
                    $stockManager->subtractPhysicalStock($product, abs($mutation));
                }

                if ($threshold >= 0) {
                    $stockManager->setThreshold($product, $threshold);
                }

                return new JsonResponse([
                    'physicalStock' => $stockManager->getPhysicalStock($product),
                    'virtualStock' => $stockManager->getVirtualStock($product),
                    'threshold' => $stockManager->getThreshold($product),
                    'stockStatus' => $stockManager->getStockStatus($product),
                ]);
            }
        }

        return [
            'product' => $product,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{product}/geschiedenis", methods={"GET"})
     * @Template()
     * @param Product $product
     * @return array
     */
    public function displayManageStockLog(Product $product)
    {
        $stockManager = $this->container->get(StockManager::class);

        $logLines = $stockManager->getLog($product);

        return [
            'product' => $product,
            'logLines' => $logLines,
        ];
    }

    /**
     * @param Product $product
     * @return FormInterface
     */
    private function getStockForm(Product $product)
    {
        return $this->createForm(StockFormType::class, $product, [
            'action' => $this->generateUrl('admin_stock_managestock', [
                'product' => $product->getId(),
            ]),
        ]);
    }
}
