<?php

namespace AdminBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/admin/mijn-account")
 */
class ProfileController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {

        return [];
    }

    /**
     * @Template("@Admin/Security/two-step-authentication.html.twig")
     */
    public function twoStepAuthenticationAction()
    {
        // Check if user already has a secret token, otherwise generate one
        $secret = $this->getUser()->getGoogleAuthenticatorSecret();

        if (!$secret) {
            $secret = $this->container->get("scheb_two_factor.security.google_authenticator")->generateSecret();

            $em = $this->getDoctrine()->getManager();

            $this->getUser()->setGoogleAuthenticatorSecret($secret);

            $em->persist($this->getUser());
            $em->flush();
        }

        return [
            'google_authenticator_secret' => $this->container->get("scheb_two_factor.security.google_authenticator")->getUrl($this->getUser()),
        ];
    }
}
