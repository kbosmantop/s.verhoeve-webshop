<?php

namespace AdminBundle\Controller\Finance;

use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Finance\CommissionBatchType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Finance\CommissionBatch;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\DateType;
use AppBundle\Manager\CommissionBatchManager;
use AppBundle\Services\Commission\Batch\GeneratorService;
use AppBundle\Services\Commission\BatchService;
use AppBundle\Services\CommissionService;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/financieel/commissies/batches")
 */
class CommissionBatchController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null|int $n
     *
     * @return string
     */
    public function getLabel($n = null): string
    {
        if ($n === 1) {
            return 'Maandafrekeningen batch';
        }

        return 'Maandafrekeningen batches';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => CommissionBatch::class,
            'form_type' => CommissionBatchType::class,
            'datatable' => [
                'datatable_type' => \AdminBundle\Datatable\Finance\CommissionBatchType::class,
                'entity_class' => CommissionBatch::class,
                'form_type' => CommissionBatchType::class,
                'allow_edit' => false,
                'allow_delete' => false,
            ],
        ];
    }

    /**
     * @Route("/nieuw")
     * @Template()
     *
     * @return array|RedirectResponse|Response
     * @throws \Exception
     */
    public function addAction()
    {
        if($this->container->get(CommissionBatchManager::class)->isBatchRunning()) {
            return $this->render('@Admin/Finance/CommissionBatch/running.html.twig');
        }

        $invoiceDate = new \DateTime();
        if ($invoiceDate->format('d') <= 15) {
            $invoiceDate->setDate($invoiceDate->format('Y'), $invoiceDate->format('m'), 1);
            $invoiceDate->sub(new \DateInterval('P1D'));
        } else {
            $invoiceDate->setDate($invoiceDate->format('Y'), $invoiceDate->format('m'), 15);
        }

        $commissionService = $this->container->get(CommissionService::class);

        $suppliers = $commissionService->getCommissionableSuppliers($invoiceDate);

        $form = $this->getCommissionForm($suppliers, $invoiceDate);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            $batch = new CommissionBatch();
            $batch->setSuppliers(new ArrayCollection($form->getData()['suppliers']));
            $batch->setInvoiceDate($form->getData()['date']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($batch);
            $em->flush();

            $job = new Job('app:finance:commission:generate-batch', [$batch->getId()]);
            $job->addRelatedEntity($batch);

            $this->container->get('job.manager')->addJob($job);
            $em->flush();

            $this->get('session')->getFlashBag()->add('backend.success',
                'De batch is succesvol ingepland. Deze wordt in enkele ogenblikken gestart.');

            return $this->redirectToRoute('admin_finance_commissioninvoice_index');
        }

        foreach ($form->getErrors(true, true) as $error) {
            $messageParameters = implode(', ', $error->getMessageParameters());

            $company = $this->container->get('doctrine')->getManager()->find(Company::class,
                $error->getOrigin()->getViewData());

            if (!$company) {
                $this->get('session')->getFlashBag()->add('backend.error',
                    sprintf('%s: %s', $error->getMessage(), $messageParameters));
            } else {
                $this->get('session')->getFlashBag()->add('backend.error',
                    sprintf('%s<br /><i>%s: %s</i>', $company->getName(), $error->getMessage(), $messageParameters));
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param ArrayCollection $suppliers
     * @param \DateTime       $date
     *
     * @return FormInterface
     */
    private function getCommissionForm(ArrayCollection $suppliers, \DateTime $date): FormInterface
    {
        $form = $this->createFormBuilder();
        $form
            ->add('date', DateType::class, [
                'label' => 'Factuurdatum',
                'data' => $date,
                'data_class' => null,
            ])
            ->add('suppliers', ChoiceType::class, [
                'label' => 'Leveranciers',
                'help_label' => '<a href="#" id="selectAll">Alles (de-)selecteren</a>',
                'choices' => $suppliers,
                'expanded' => true,
                'multiple' => true,
                'choice_value' => function (Company $supplier = null) {
                    if (null === $supplier) {
                        return null;
                    }

                    return $supplier->getId();
                },
                'choice_label' => function (Company $supplier) {
                    return $supplier->getName();
                },
                'group_by' => function (Company $company) {
                    return $company->getSupplierConnector();
                },
                'data' => $suppliers->toArray(),
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Commissie batch aanmaken',
                'attr' => [
                    'class' => 'btn-primary',
                ],
                'icon' => 'checkmark-circle',
            ]);

        $form = $form->getForm();

        return $form;
    }

    /**
     * @Route("twinfield-export/{commissionBatch}")
     * @param CommissionBatch $commissionBatch
     *
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function twinfieldExportAction(CommissionBatch $commissionBatch): BinaryFileResponse
    {
        $twinfieldJournal = $this->container->get('commission.batch.twinfield_journal');
        $twinfieldJournal->setCommissionBatch($commissionBatch);

        return $twinfieldJournal->export();
    }

    /**
     * @Route("twinfield-debiteuren-export/{commissionBatch}")
     * @param CommissionBatch $commissionBatch
     *
     * @return BinaryFileResponse
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function twinfieldDebtorsExportAction(CommissionBatch $commissionBatch): BinaryFileResponse
    {
        set_time_limit(0);

        \PHPExcel_Settings::setLocale('nl');

        $excel = new \PHPExcel();

        $sheet = $excel->getActiveSheet();

        $sheet->setCellValue('A1', 'code');
        $sheet->setCellValue('B1', 'naam');
        $sheet->setCellValue('C1', 'website');
        $sheet->setCellValue('D1', 'standaardadres');
        $sheet->setCellValue('E1', 'adrestype');
        $sheet->setCellValue('F1', 'naam');
        $sheet->setCellValue('G1', 'tav');
        $sheet->setCellValue('H1', 'adresregel1');
        $sheet->setCellValue('I1', 'adresregel2');
        $sheet->setCellValue('J1', 'postcode');
        $sheet->setCellValue('K1', 'plaats');
        $sheet->setCellValue('L1', 'land');
        $sheet->setCellValue('M1', 'telefoon');
        $sheet->setCellValue('N1', 'fax');
        $sheet->setCellValue('O1', 'btwnummer');
        $sheet->setCellValue('P1', 'kvknummer');
        $sheet->setCellValue('Q1', 'emailadres');
        $sheet->setCellValue('R1', 'incasso');
        $sheet->setCellValue('S1', 'incassocode');
        $sheet->setCellValue('T1', 'machtigingskenmerk');
        $sheet->setCellValue('U1', 'eersteincasso');
        $sheet->setCellValue('V1', 'ondertekeningsdatum');
        $sheet->setCellValue('W1', 'standaardbank');
        $sheet->setCellValue('X1', 'Tenaamstelling');
        $sheet->setCellValue('Y1', 'rekeningnummer');
        $sheet->setCellValue('Z1', 'IBAN');
        $sheet->setCellValue('AA1', 'BIC');
        $sheet->setCellValue('AB1', 'NationalebankID');
        $sheet->setCellValue('AC1', 'Banknaam');
        $sheet->setCellValue('AD1', 'bankadres');
        $sheet->setCellValue('AE1', 'bankadresnummer');
        $sheet->setCellValue('AF1', 'bankpostcode');
        $sheet->setCellValue('AG1', 'bankplaats');
        $sheet->setCellValue('AH1', 'bankstaat');
        $sheet->setCellValue('AI1', 'referentie');
        $sheet->setCellValue('AJ1', 'bankland');
        $sheet->setCellValue('AK1', 'vervaldagen');
        $sheet->setCellValue('AL1', 'ebilling');
        $sheet->setCellValue('AM1', 'ebillingemailadres');
        $sheet->setCellValue('AN1', 'grootboekrekening');
        $sheet->setCellValue('AO1', 'kredietlimiet');
        $sheet->setCellValue('AP1', 'kredietbeheerder');
        $sheet->setCellValue('AQ1', 'geblokkeerd');
        $sheet->setCellValue('AR1', 'gebruiksrecht');
        $sheet->setCellValue('AS1', 'segmentcode');
        $sheet->setCellValue('AT1', 'aanmanen');
        $sheet->setCellValue('AU1', 'aanmanenemailadres');
        $sheet->setCellValue('AV1', 'commentaar');
        $sheet->setCellValue('AW1', 'kortingsartikel');

        $rowIndex = 2;

        foreach ($commissionBatch->getCommissionInvoices() as $commissionInvoice) {
            $company = $commissionInvoice->getCompany();

            $companyRegistration = $company->getCompanyRegistration();

            $sheet->setCellValue('A' . $rowIndex, $company->getTwinfieldRelationNumber());
            $sheet->setCellValue('B' . $rowIndex, $company->getName());
            $sheet->setCellValue('C' . $rowIndex, '');
            $sheet->setCellValue('D' . $rowIndex, 'true');
            $sheet->setCellValue('E' . $rowIndex, 'invoice');
            $sheet->setCellValue('F' . $rowIndex, $company->getName());
            $sheet->setCellValue('G' . $rowIndex,
                ($company->getInvoiceAddress() !== null && $company->getInvoiceAddress()->getAttn() ? $company->getInvoiceAddress()->getAttn() : 'Financiële administratie'));
            $sheet->setCellValue('H' . $rowIndex,
                ($company->getInvoiceAddress() !== null ? $company->getInvoiceAddress()->getStreet() : '') . ' ' . ($company->getInvoiceAddress() !== null ? $company->getInvoiceAddress()->getNumber() : ''));
            $sheet->setCellValue('I' . $rowIndex, '');
            $sheet->setCellValue('J' . $rowIndex,
                ($company->getInvoiceAddress() !== null ? $company->getInvoiceAddress()->getPostcode() : ''));
            $sheet->setCellValue('K' . $rowIndex,
                ($company->getInvoiceAddress() !== null ? $company->getInvoiceAddress()->getCity() : ''));
            $sheet->setCellValue('L' . $rowIndex,
                ($company->getInvoiceAddress() !== null ? $company->getInvoiceAddress()->getCountry()->getCode() : ''));
            $sheet->getCell('M' . $rowIndex)->setValueExplicit($company->getFormattedPhoneNumber(),
                \PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('N' . $rowIndex, '');
            $sheet->setCellValue('O' . $rowIndex, $companyRegistration ? $companyRegistration->getVatNumber() : null);
            $sheet->setCellValue('P' . $rowIndex, $companyRegistration ? $companyRegistration->getKvKNumber() : null);
            $sheet->setCellValue('Q' . $rowIndex, $company->getEmail());
            $sheet->setCellValue('R' . $rowIndex, 'true');
            $sheet->setCellValue('S' . $rowIndex, 'SEPABAKKER');
            $sheet->setCellValue('T' . $rowIndex,
                $company->getTwinfieldRelationNumber() . ($company->getInvoiceAddress() !== null && $company->getInvoiceAddress()->getCountry()->getCode() ? $company->getInvoiceAddress()->getCountry()->getCode() : ''));
            $sheet->setCellValue('U' . $rowIndex, $company->getCreatedAt()->format('Ymd'));
            $sheet->setCellValue('V' . $rowIndex, $company->getCreatedAt()->format('Ymd'));
            $sheet->setCellValue('W' . $rowIndex, 'true');
            $sheet->setCellValue('X' . $rowIndex,
                $company->getBankAccountHolder() ?: $company->getName());
            $sheet->setCellValue('Y' . $rowIndex, '');
            $sheet->setCellValue('Z' . $rowIndex, strtoupper($company->getIban()));
            $sheet->setCellValue('AA' . $rowIndex, $company->getBic());
            $sheet->setCellValue('AB' . $rowIndex, '');
            $sheet->setCellValue('AC' . $rowIndex, '');
            $sheet->setCellValue('AD' . $rowIndex, '');
            $sheet->setCellValue('AE' . $rowIndex, '');
            $sheet->setCellValue('AF' . $rowIndex, '');
            $sheet->setCellValue('AG' . $rowIndex, '');
            $sheet->setCellValue('AH' . $rowIndex, '');
            $sheet->setCellValue('AI' . $rowIndex, '');
            $sheet->setCellValue('AJ' . $rowIndex, strtoupper(substr($company->getIban(), 0, 2)));
            $sheet->setCellValue('AK' . $rowIndex, 14);
            $sheet->setCellValue('AL' . $rowIndex, 'true');
            $sheet->setCellValue('AM' . $rowIndex,
                $company->getInvoiceEmail() ?: $company->getEmail());
            $sheet->setCellValue('AN' . $rowIndex, 1400);
            $sheet->setCellValue('AO' . $rowIndex, 0);
            $sheet->setCellValue('AP' . $rowIndex, '');
            $sheet->setCellValue('AQ' . $rowIndex, 'false');
            $sheet->setCellValue('AR' . $rowIndex, 'false');
            $sheet->setCellValue('AS' . $rowIndex, '');
            $sheet->setCellValue('AT' . $rowIndex, 'true');
            $sheet->setCellValue('AU' . $rowIndex,
                $company->getInvoiceEmail() ?: $company->getEmail());
            $sheet->setCellValue('AV' . $rowIndex, '');
            $sheet->setCellValue('AW' . $rowIndex, '');

            $rowIndex++;
        }

        $c = 'A';

        while (true) {
            $sheet->getColumnDimension($c)->setAutoSize(true);

            if ($c === 'AW') {
                break;
            }

            $c++;
        }

        $path = tempnam(sys_get_temp_dir(), 'export');

        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($path);

        $name = 'Twinfield - Debiteuren';

        $response = new BinaryFileResponse($path, 200, [
            'Content-type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $name . '.xls"',
        ]);

        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @Route("send/{commissionBatch}")
     *
     * @param CommissionBatch $commissionBatch
     *
     * @return RedirectResponse
     */
    public function sendAction(CommissionBatch $commissionBatch): RedirectResponse
    {
        $this->container->get(BatchService::class)->setCommissionBatch($commissionBatch)->send();

        $this->addFlash('backend.success', 'Versturen van facturen wordt op de achtergrond uitgevoerd.');

        return $this->redirectToRoute('admin_finance_commissionbatch_index');
    }
}
