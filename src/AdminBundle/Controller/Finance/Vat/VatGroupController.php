<?php

namespace AdminBundle\Controller\Finance\Vat;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\Vat\VatGroupDatatableType;
use AdminBundle\Datatable\Finance\Vat\VatRateDatatableType;
use AdminBundle\Form\Finance\Vat\VatGroupFormType;
use AdminBundle\Form\Finance\Vat\VatRateFormType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRate;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/financieel/btw-groepen")
 * Class VatGroupController
 * @package AdminBundle\Controller\Finance\Vat
 */
class VatGroupController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => VatGroup::class,
            'form_type' => VatGroupFormType::class,
            'datatable' => [
                'entity_class' => VatGroup::class,
                'form_type' => VatGroupFormType::class,
                'datatable_type' => VatGroupDatatableType::class,
                'allow_edit' => false,
                'allow_read' => true
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description_panel',
                        'type' => 'template',
                        'template' => '@Admin/Finance/Vat/description-panel.html.twig'
                    ],
                    [
                        'id' => 'vat_rate_panel',
                        'type' => 'datatable',
                        'datatable' => [
                            'entity_class' => VatRate::class,
                            'form_type' => VatRateFormType::class,
                            'datatable_type' => VatRateDatatableType::class,
                            'parent_column' => 'group',
                            'base_route' => 'admin_finance_vat_vatrate'
                        ]
                    ]
                ]
            ]
        ];
    }
}
