<?php

namespace AdminBundle\Controller\Finance\Vat;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\Vat\VatRateDatatableType;
use AdminBundle\Form\Finance\Vat\VatRateFormType;
use AppBundle\Entity\Finance\Tax\VatRate;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/financieel/btw-tarieven")
 * Class VatRateController
 * @package AdminBundle\Controller\Finance\Vat
 */
class VatRateController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => VatRate::class,
            'form_type' => VatRateFormType::class,
            'datatable' => [
                'entity_class' => VatRate::class,
                'form_type' => VatRateFormType::class,
                'datatable_type' => VatRateDatatableType::class
            ],
        ];
    }
}
