<?php

namespace AdminBundle\Controller\Finance\Vat;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\Vat\VatRuleDatatableType;
use AdminBundle\Form\Finance\Vat\VatRuleFormType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRule;
use Doctrine\Common\Annotations\AnnotationException;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/btw-regels")
 * Class VatRuleController
 * @package AdminBundle\Controller\Finance\Vat
 */
class VatRuleController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'assets' => [
                'admin/js/vat/vat.js',
            ],
            'entity_class' => VatRule::class,
            'form_type' => VatRuleFormType::class,
            'datatable' => [
                'entity_class' => VatRule::class,
                'form_type' => VatRuleFormType::class,
                'datatable_type' => VatRuleDatatableType::class,
                'allow_add' => false,
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_finance_vat_vatrule_createrule',
                        'label' => 'Toevoegen',
                        'className' => 'btn-success',
                        'action' => 'create-rule'
                    ]
                ]
            ]
        ];
    }

    /**
     * @Route("/regel-aanmaken", methods={"GET"})
     * @Template("@Admin/Rule/create-modal.html.twig")
     * @param Request $request
     * @return array
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function createRule(Request $request)
    {
        $ruleTransformer = $this->get('rule.transformer');

        $entities = $this->getDoctrine()->getRepository(Entity::class)->findBy([
            'fullyQualifiedName' => [VatGroup::class],
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $entities[0],
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => $entities,
            'persistUrl' => $this->container->get('router')->generate('admin_finance_vat_vatrule_createpersist'),
        ];
    }

    /**
     * @Route("/aanmaken", methods={"POST"})
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createPersistAction(): RedirectResponse
    {
        $em = $this->container->get('doctrine')->getManager();
        /** @var Rule $rule */
        $rule = $this->container->get('rule')->store($this->getRequest(), true);

        $vatRule = new VatRule();
        $vatRule->setRule($rule);
        $vatRule->setPosition(0);
        $vatRule->setAllowSplit(false);

        $em->persist($vatRule);
        $em->flush();

        return $this->redirectToRoute('admin_finance_vat_vatrule_edit', ['id' => $vatRule->getId()]);
    }
}
