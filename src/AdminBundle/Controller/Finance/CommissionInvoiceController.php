<?php

namespace AdminBundle\Controller\Finance;

use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Finance\CommissionInvoiceType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Finance\CommissionInvoice;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\Commission\InvoiceService;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/financieel/commissies/facturen")
 */
class CommissionInvoiceController extends CrudController implements CrudControllerInterface
{
    /**
     * @param int $n
     * @return string
     */
    public function getLabel($n = null)
    {
        if ($n == 1) {
            return 'Maandafrekening';
        } else {
            return 'Maandafrekeningen';
        }
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => CommissionInvoice::class,
            'form_type' => CommissionInvoiceType::class,
            'datatable' => [
                'entity_class' => CommissionInvoice::class,
                'form_type' => CommissionInvoiceType::class,
                'datatable_type' => \AdminBundle\Datatable\Finance\CommissionInvoiceType::class,
                'item_actions' => false,
                'allow_edit' => false,
                'allow_delete' => false,
                'alias' => 'commission_invoice',
                'query_builder' => function(QueryBuilder $qb) {
                        $qb->orderBy('commission_invoice.id', 'DESC');
                }
            ],
        ];
    }

    /**
     * @Route("/export")
     */
    public function addAction()
    {
        return $this->redirectToRoute("admin_finance_commissionbatch_add");
    }

    /**
     * @Route("/{commissionInvoice}/render")
     *
     * @param CommissionInvoice $commissionInvoice
     * @return Response
     */
    public function renderAction(CommissionInvoice $commissionInvoice)
    {
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $calculator = $this->container->get(InvoiceService::class);
        $calculator->setCommissionInvoice($commissionInvoice);

        return $calculator->renderPdf();
    }
}
