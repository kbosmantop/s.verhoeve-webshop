<?php

namespace AdminBundle\Controller\Finance;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/financieel/grootboeken")
 */
class LedgerController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Grootboeken';
    }

    public function getOptions()
    {
        return [
            'entity_class' => 'AppBundle\Entity\Finance\Ledger',
        ];
    }
}
