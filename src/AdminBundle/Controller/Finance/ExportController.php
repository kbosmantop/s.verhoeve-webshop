<?php

namespace AdminBundle\Controller\Finance;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\ExportType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\DateType;
use AppBundle\ThirdParty\Twinfield\Entity\Export;
use AppBundle\Utils\DisableFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/financieel/export")
 */
class ExportController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Twinfield export',
            'entity_class' => Export::class,
            'datatable' => [
                'entity_class' => Export::class,
                'form_type' => false,
                'datatable_type' => ExportType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_finance_export_add',
                        'label' => 'Export toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/export", methods={"GET", "POST"})
     * @Template()
     */
    public function addAction()
    {
        $form = $this->getInvoiceExportForm();
        $form->handleRequest($this->getRequest());

        if ($form->get("invoiceDate")->getData() < $form->get("deliveryDate")->getData()) {
            $form->addError(new FormError('Factuurdatum mag niet vroeger zijn dan de bezorgdatum.'));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $companies = new ArrayCollection();

            $orders = $this->getOrdersToExport($form->get("sites")->getData(), $form->get("deliveryDate")->getData());

            if ($orders->isEmpty()) {
                $this->addFlash("backend.error",
                    "Twinfield export niet aangemaakt.<br /><i>Geen bestellingen om te exporteren.</i>"
                );

                return $this->redirectToRoute('admin_finance_export_index');
            }

            foreach ($orders as $order) {
                if (!$companies->contains($order->getOrderCollection()->getCompany())) {
                    $companies->add($order->getOrderCollection()->getCompany());
                }
            }

            $export = new Export();
            $export->setInvoiceDate($form->get("invoiceDate")->getData());

            foreach ($orders as $order) {
                $export->addOrder($order);
            }

            foreach ($companies as $company) {
                $export->addCompany($company);
            }

            $this->getDoctrine()->getManager()->persist($export);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("backend.success", "Twinfield export is aangemaakt.");

            return $this->redirectToRoute('admin_finance_export_index');
        }

        return [
            "form" => $form->createView(),
        ];
    }

    /**
     * @Route("/{export}/download-companies")
     * @param Export $export
     * @return BinaryFileResponse
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function downloadCompaniesAction(Export $export)
    {
        set_time_limit(0);

        \PHPExcel_Settings::setLocale("nl");

        $filter = DisableFilter::temporaryDisableFilter('softdeleteable');

        $excel = new \PHPExcel();

        $sheet = $excel->getActiveSheet();

        $sheet->setCellValue("A1", "code");
        $sheet->setCellValue("B1", "naam");
        $sheet->setCellValue("C1", "website");
        $sheet->setCellValue("D1", "standaardadres");
        $sheet->setCellValue("E1", "adrestype");
        $sheet->setCellValue("F1", "tav");
        $sheet->setCellValue("G1", "adresregel1");
        $sheet->setCellValue("H1", "adresregel2");
        $sheet->setCellValue("I1", "postcode");
        $sheet->setCellValue("J1", "plaats");
        $sheet->setCellValue("K1", "land");
        $sheet->setCellValue("L1", "telefoon");
        $sheet->setCellValue("M1", "fax");
        $sheet->setCellValue("N1", "BTWnummer");
        $sheet->setCellValue("O1", "KvKnummer");
        $sheet->setCellValue("P1", "emailadres");
        $sheet->setCellValue("Q1", "standaardbank");
        $sheet->setCellValue("R1", "begunstigde");
        $sheet->setCellValue("S1", "rekeningnummer");
        $sheet->setCellValue("T1", "bankplaats");
        $sheet->setCellValue("U1", "vervaldagen");
        $sheet->setCellValue("V1", "incasso");
        $sheet->setCellValue("W1", "incassocode");
        $sheet->setCellValue("X1", "ebilling");
        $sheet->setCellValue("Y1", "ebillingemailadres");
        $sheet->setCellValue("Z1", "kredietbeheerder");
        $sheet->setCellValue("AA1", "kredietlimiet");
        $sheet->setCellValue("AB1", "aanmanen");
        $sheet->setCellValue("AC1", "aanmanenemailadres");
        $sheet->setCellValue("AD1", "commentaar");
        $sheet->setCellValue("AE1", "gebruiksrecht");
        $sheet->setCellValue("AF1", "segmentcode");
        $sheet->setCellValue("AG1", "geblokkeerd");
        $sheet->setCellValue("AH1", "kortingsartikel");
        $sheet->setCellValue("AI1", "bankland");

        $rowIndex = 2;

        foreach ($export->getCompanies() as $company) {
            $companyRegistration = $company->getCompanyRegistration();

            $sheet->setCellValue("A". $rowIndex, $company->getTwinfieldRelationNumber());
            $sheet->setCellValue("B". $rowIndex, $company->getName());
            $sheet->setCellValue("C". $rowIndex, "");
            $sheet->setCellValue("D". $rowIndex, "true");
            $sheet->setCellValue("E". $rowIndex, "invoice");
            $sheet->setCellValue("F". $rowIndex, $company->getInvoiceAddress()->getAttn());
            $sheet->setCellValue("G". $rowIndex, $company->getInvoiceAddress()->getStreet() ." ". $company->getInvoiceAddress()->getNumber());
            $sheet->setCellValue("H". $rowIndex, "");
            $sheet->setCellValue("I". $rowIndex, $company->getInvoiceAddress()->getPostcode());
            $sheet->setCellValue("J". $rowIndex, $company->getInvoiceAddress()->getCity());
            $sheet->setCellValue("K". $rowIndex, $company->getInvoiceAddress()->getCountry()->getCode());
            $sheet->getCell("L". $rowIndex)->setValueExplicit($company->getInvoiceAddress()->getFormattedPhoneNumber(), \PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue("M". $rowIndex, "");
            $sheet->setCellValue("N". $rowIndex, $companyRegistration ? $companyRegistration->getVatNumber() : null);
            $sheet->setCellValue("O". $rowIndex, $companyRegistration ? $companyRegistration->getKvKNumber() : null);
            $sheet->setCellValue("P". $rowIndex, $company->getEmail());
            $sheet->setCellValue("Q". $rowIndex, "");
            $sheet->setCellValue("R". $rowIndex, "");
            $sheet->setCellValue("S". $rowIndex, "");
            $sheet->setCellValue("T". $rowIndex, "");
            $sheet->setCellValue("U". $rowIndex, $company->getPaymentTerm());
            $sheet->setCellValue("V". $rowIndex, "false");
            $sheet->setCellValue("W". $rowIndex, "");
            $sheet->setCellValue("X". $rowIndex, "true");
            $sheet->setCellValue("Y". $rowIndex, $company->getInvoiceEmail() ?: $company->getEmail());
            $sheet->setCellValue("Z". $rowIndex, "");
            $sheet->setCellValue("AA". $rowIndex, "0");
            $sheet->setCellValue("AB". $rowIndex, "true");
            $sheet->setCellValue("AC". $rowIndex, $company->getInvoiceEmail() ?: $company->getEmail());
            $sheet->setCellValue("AD". $rowIndex, "");
            $sheet->setCellValue("AE". $rowIndex, "");
            $sheet->setCellValue("AF". $rowIndex, "");
            $sheet->setCellValue("AG". $rowIndex, "");
            $sheet->setCellValue("AH". $rowIndex, "");
            $sheet->setCellValue("AI". $rowIndex, "");

            $rowIndex++;
        }

        $c = "A";

        while (true) {
            $sheet->getColumnDimension($c)->setAutoSize(true);

            if ($c == "AI") {
                break;
            }

            $c++;
        }

        $path = tempnam(sys_get_temp_dir(), "export");

        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($path);

        $formatter = new \IntlDateFormatter("nl_NL", \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);

        $name = "Twinfield - Debiteuren (" . $formatter->format($export->getInvoiceDate()) . ")";

        $response = new BinaryFileResponse($path, 200, [
            "Content-type" => "application/vnd.ms-excel",
            "Content-Disposition" => 'attachment; filename="' . $name . '.xls"',
        ]);

        $response->deleteFileAfterSend(true);

        $filter->reenableFilter();

        return $response;
    }

    /**
     * @Route("/{export}/download-orders")
     *
     * @param Export $export
     * @return BinaryFileResponse
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function downloadOrdersAction(Export $export)
    {
        set_time_limit(0);

        \PHPExcel_Settings::setLocale("nl");

        $filter = DisableFilter::temporaryDisableFilter('softdeleteable');

        $excel = new \PHPExcel();

        $sheet = $excel->getActiveSheet();

        $sheet->setCellValue("A1", "Invoice");
        $sheet->setCellValue("B1", "AR number");
        $sheet->setCellValue("C1", "Group");
        $sheet->setCellValue("D1", "invoice date");
        $sheet->setCellValue("E1", "due date");
        $sheet->setCellValue("F1", "header");
        $sheet->setCellValue("G1", "footer");
        $sheet->setCellValue("H1", "currency");
        $sheet->setCellValue("I1", "quantity");
        $sheet->setCellValue("J1", "article");
        $sheet->setCellValue("K1", "subarticle");
        $sheet->setCellValue("L1", "description");
        $sheet->setCellValue("M1", "article price(vatexclusive)");
        $sheet->setCellValue("N1", "article price(vatinclusive)");
        $sheet->setCellValue("O1", "VAT");
        $sheet->setCellValue("P1", "GL Account");
        $sheet->setCellValue("Q1", "Free text field 1");
        $sheet->setCellValue("R1", "Free text field 2");
        $sheet->setCellValue("S1", "Free text field 3");

        $rowIndex = 2;

        foreach ($export->getOrders() as $order) {
            if ($order->getLines()->isEmpty()) {
                continue;
            }

            $theme = strtoupper($order->getOrderCollection()->getSite()->getTheme());

            foreach ($order->getLines() as $line => $orderLine) {
                foreach($orderLine->getVatLines() as $vatLine) {
                    $sheet->setCellValue("A" . $rowIndex, $theme);
                    $sheet->setCellValue("B" . $rowIndex,
                        $order->getOrderCollection()->getCompany()->getTwinfieldRelationNumber());
                    $sheet->setCellValue("D" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($export->getInvoiceDate()));
                    $sheet->setCellValue("H" . $rowIndex, "EUR");

                    if ($orderLine->getQuantity()) {
                        $sheet->setCellValue("I" . $rowIndex, $orderLine->getQuantity());
                        $sheet->setCellValue("J" . $rowIndex, "0");
                    } else {
                        $sheet->setCellValue("J" . $rowIndex, "-");
                    }

                    $sheet->setCellValue("L" . $rowIndex, $orderLine->getTitle());

                    if ($orderLine->getQuantity()) {
                        $sheet->setCellValue("M" . $rowIndex, round($orderLine->getPrice(), 2));
                        $sheet->setCellValue("O" . $rowIndex, $vatLine->getVatRate()->getCode());
                    }

                    $sheet->setCellValue("P" . $rowIndex, $orderLine->getLedger()->getNumber());

                    if ($line === 0) {
                        $sheet->setCellValue("Q" . $rowIndex, $order->getOrderNumber());

                        if ($order->getDeliveryDate()) {
                            $sheet->setCellValue("R" . $rowIndex, $order->getDeliveryDate()->format("j-n-Y"));
                        }
                    }

                    $sheet->getStyle('D' . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                    $rowIndex++;

                    if ($orderLine->getDiscountAmount() < 0) {
                        $sheet->setCellValue("A" . $rowIndex, $theme);
                        $sheet->setCellValue("B" . $rowIndex,
                            $order->getOrderCollection()->getCompany()->getTwinfieldRelationNumber());
                        $sheet->setCellValue("D" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($export->getInvoiceDate()));

                        $sheet->setCellValue("H" . $rowIndex, "EUR");
                        $sheet->setCellValue("I" . $rowIndex, 1);
                        $sheet->setCellValue("J" . $rowIndex, "0");
                        $sheet->setCellValue("L" . $rowIndex, " - korting over € " . str_replace(".", ",",
                                round($orderLine->getPrice(), 2) * $orderLine->getQuantity()));

                        if ($orderLine->getQuantity()) {
                            $sheet->setCellValue("M" . $rowIndex, round($orderLine->getDiscountAmount(), 2));
                            $sheet->setCellValue("O" . $rowIndex, $vatLine->getVatRate()->getCode());
                        }

                        $sheet->setCellValue("P" . $rowIndex, $orderLine->getLedger()->getNumber());

                        $sheet->getStyle('D' . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                        $rowIndex++;
                    }
                }
            }

            $sheet->setCellValue("A" . $rowIndex, $theme);
            $sheet->setCellValue("B" . $rowIndex,
                $order->getOrderCollection()->getCompany()->getTwinfieldRelationNumber());
            $sheet->setCellValue("D" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($export->getInvoiceDate()));
            $sheet->setCellValue("H" . $rowIndex, "EUR");
            $sheet->setCellValue("J" . $rowIndex, "-");
            $sheet->setCellValue("L" . $rowIndex,
                "Besteller: " . $order->getOrderCollection()->getCustomer()->getFullname());

            $sheet->getStyle('D' . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

            $rowIndex++;

            $sheet->setCellValue("A" . $rowIndex, $theme);
            $sheet->setCellValue("B" . $rowIndex,
                $order->getOrderCollection()->getCompany()->getTwinfieldRelationNumber());
            $sheet->setCellValue("D" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($export->getInvoiceDate()));
            $sheet->setCellValue("H" . $rowIndex, "EUR");
            $sheet->setCellValue("J" . $rowIndex, "-");
            $sheet->setCellValue("L" . $rowIndex, "Ontvanger: " . $order->getDeliveryAddressAttn());

            $sheet->getStyle('D' . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

            $rowIndex++;

            if ($order->getInvoiceReference()) {
                $sheet->setCellValue("A" . $rowIndex, $theme);
                $sheet->setCellValue("B" . $rowIndex,
                    $order->getOrderCollection()->getCompany()->getTwinfieldRelationNumber());
                $sheet->setCellValue("D" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($export->getInvoiceDate()));
                $sheet->setCellValue("H" . $rowIndex, "EUR");
                $sheet->setCellValue("J" . $rowIndex, "-");
                $sheet->setCellValue("L" . $rowIndex, "Referentie: " . $order->getInvoiceReference());

                $sheet->getStyle('D' . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                $rowIndex++;
            }
        }

        $path = tempnam(sys_get_temp_dir(), "export");

        $writer = new \PHPExcel_Writer_Excel5($excel);
        $writer->save($path);

        $formatter = new \IntlDateFormatter("nl_NL", \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);

        $name = "Twinfield - Facturen (" . $formatter->format($export->getInvoiceDate()) . ")";

        $response = new BinaryFileResponse($path, 200, [
            "Content-type" => "application/vnd.ms-excel",
            "Content-Disposition" => 'attachment; filename="' . $name . '.xls"',
        ]);

        $response->deleteFileAfterSend(true);

        $filter->reenableFilter();

        return $response;
    }

    /**
     * @param Site[]    $sites
     * @param \DateTime $deliveryDate
     * @return ArrayCollection|Order[]
     */
    private function getOrdersToExport($sites, \DateTime $deliveryDate)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getManager()->getRepository(Order::class);

        $qb = $entityRepository->createQueryBuilder("order_order");

        $qb->andWhere("(order_order.deliveryDate <= :deliveryDate OR (order_order.deliveryDate IS NULL AND order_order.createdAt <= :deliveryDate))");
        $qb->setParameter("deliveryDate", $deliveryDate);

        $qb->andWhere("order_order.deletedAt IS NULL");

        $qb->join("order_order.orderCollection", "order_collection");
        $qb->andWhere("order_collection.deletedAt IS NULL");
        $qb->andWhere("order_collection.site IN (:sites)");
        $qb->setParameter("sites", $sites);

        $qb->andWhere('order_order.status IN (:statuses)')
            ->setParameter('statuses', ['complete', 'pending', 'processed', 'sent']);

        $qb->join("order_collection.company", "company");
        $qb->andWhere("company.id IS NOT NULL");

        $qb->join("order_collection.payments", "payment");
        $qb->andWhere("payment.status = 'authorized'");

        $qb->join("payment.paymentmethod", "paymentmethod");
        $qb->andWhere("paymentmethod.code = 'invoice'");

        $qb->leftJoin("order_order.twinfieldExportOrders", "twinfield_export_orders");
        $qb->andWhere("twinfield_export_orders.id IS NULL");

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    /**
     * @return FormInterface
     * @throws \Exception
     */
    private function getInvoiceExportForm()
    {
        $defaultDeliveryDate = new \DateTime();
        $defaultDeliveryDate->sub(new \DateInterval("P1D"));

        $defaultInvoiceDate = new \DateTime();
        $defaultInvoiceDate->sub(new \DateInterval("P1D"));

        $sites = $this->getDoctrine()->getRepository(Site::class)->findAll();

        $form = $this->createFormBuilder();
        $form
            ->add("deliveryDate", DateType::class, [
                'label' => "Bezorgdatum (tot en met)",
                'data_class' => null,
                'data' => $defaultDeliveryDate,
            ])
            ->add("invoiceDate", DateType::class, [
                'label' => "Factuurdatum",
                'data_class' => null,
                'data' => $defaultInvoiceDate,
            ])
            ->add("sites", EntityType::class, [
                'label' => "Sites",
                'class' => Site::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => function (Site $site) {
                    return $site->translate()->getDescription();
                },
                'data' => $sites,
            ])
            ->add("submit", SubmitType::class, [
                'label' => "Export aanmaken",
                "attr" => [
                    "class" => "btn-primary",
                ],
                "icon" => "checkmark-circle",
            ]);

        $form = $form->getForm();

        return $form;
    }
}
