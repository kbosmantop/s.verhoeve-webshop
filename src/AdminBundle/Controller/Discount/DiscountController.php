<?php

namespace AdminBundle\Controller\Discount;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Discount\DiscountDatatableType;
use AdminBundle\Datatable\Discount\VoucherDatatableType;
use AdminBundle\Form\Discount\DiscountFormType;
use AdminBundle\Form\Discount\VoucherFormType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\DiscountProperty;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/kortingen")
 * Class DiscountController
 * @package AdminBundle\Controller\Discount
 */
class DiscountController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Discount::class,
            'form_type' => DiscountFormType::class,
            'datatable' => [
                'entity_class' => Discount::class,
                'form_type' => DiscountFormType::class,
                'datatable_type' => DiscountDatatableType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_discount_discount_wizard',
                        'label' => 'Korting toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-content-copy',
                        'route' => 'admin_discount_discount_duplicate',
                        'method' => 'GET',
                        'type' => 'edit'
                    ],
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                        'identifier' => 'rule.id',
                    ],
                ],
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->leftJoin('discount.companies','c')
                        ->having('COUNT(c.id) = 0')
                        ->groupBy('discount.id');
                    return $qb;
                },
            ],
        ];
    }

    /**
     * @Route("/wizard", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @return array|JsonResponse
     * @throws \Exception
     */
    public function wizardAction(Request $request)
    {
        $ruleTransformer = $this->container->get('rule.transformer');
        $em = $this->container->get('doctrine')->getManager();
        $start = OrderLineInterface::class;

        if ($request->isMethod('POST')) {
            $rule = $this->get('rule')->store($request, true);

            if ($rule instanceof Rule) {
                $discount = new Discount();
                $discount->setDescription($request->get('description'));
                $discount->setRule($rule);
                $discount->setType($request->get('type'));
                $discount->setValue($request->get('amount'));

                $em->persist($discount);
                $em->flush();

                return new JsonResponse(['message' => 'ok'], 200);
            }

            return new JsonResponse(['message' => 'store.failed'], 422);
        }

        $inputs = [
            ResolverService::QB_ALIAS => $em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => $start,
            ]),
        ];

        return [
            'fields' => $em->getRepository(DiscountProperty::class)->findAll(),
            'filters' => $ruleTransformer->getFilters($inputs),
            'ruleStart' => $start,
        ];
    }

    /**
     * @Route("/aanmaken", methods={"GET"})
     * @Template("@Admin/Rule/create-modal.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function createAction(): array
    {
        $ruleTransformer = $this->get('rule.transformer');

        $entities = $this->getDoctrine()->getRepository(Entity::class)->findBy([
            'fullyQualifiedName' => [OrderLineInterface::class, Product::class],
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $entities[0],
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => $entities,
            'persistUrl' => $this->container->get('router')->generate('admin_discount_discount_createpersist'),
        ];
    }

    /**
     * @Route("/aanmaken", methods={"POST"})
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createPersistAction(): RedirectResponse
    {
        $em = $this->container->get('doctrine')->getManager();
        /** @var Rule $rule */
        $rule = $this->container->get('rule')->store($this->getRequest(), true);

        $discount = new Discount();
        $discount->setRule($rule);
        $discount->setValue(0);

        $em->persist($discount);
        $em->flush();

        return $this->redirectToRoute('admin_discount_discount_edit', ['id' => $discount->getId()]);
    }

    /**
     * @Route("/{discount}/regel-aanmaken", methods={"GET", "POST"})
     * @Template("@Admin/Rule/create-modal.html.twig")
     * @param Discount $discount
     * @param Request  $request
     * @return array|JsonResponse
     * @throws AnnotationException
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \ReflectionException
     */
    public function addRule(Discount $discount, Request $request) {
        $ruleTransformer = $this->get('rule.transformer');

        if($request->getMethod() === 'POST') {
            $em = $this->container->get('doctrine')->getManager();
            /** @var Rule $rule */
            $rule = $this->container->get('rule')->store($this->getRequest(), true);

            $discount->setRule($rule);
            $discount->setDescription($request->get('description'));
            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }

        $start = $request->get('start');
        if($start === 'product') {
            $start = Product::class;
        } else {
            $start = OrderLineInterface::class;
        }

        $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy([
            'fullyQualifiedName' => $start,
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $entity,
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => [$entity],
            'persistUrl' => $this->container->get('router')->generate('admin_discount_discount_addrule', ['discount' => $discount->getId()]),
        ];
    }

    /**
     * @Route("/{discount}/vouchers", methods={"GET"})
     * @Template()
     * @param Discount $discount
     * @return array
     * @throws \Twig_Error
     */
    public function voucherAction(Discount $discount)
    {
        $datatable = $this->container->get('admin.datatable.service')->getTable([
            'entity_class' => Voucher::class,
            'datatable_type' => VoucherDatatableType::class,
            'form_type' => VoucherFormType::class,
            'data_url' => $this->container->get('router')->generate('admin_discount_discount_voucherdata', [
                'discount' => $discount->getId()
            ]),
            'disable_search' => true,
            'base_route' => 'admin_discount_voucher',
        ]);

        return [
            'datatable' => $datatable,
            'discount' => $discount,
        ];
    }

    /**
     * @Route("/{discount}/vouchers/data", methods={"GET", "POST"})
     * @param Discount $discount
     * @return JsonResponse
     * @throws \Exception
     */
    public function voucherDataAction(Discount $discount): JsonResponse
    {
        return $this->container->get('admin.datatable.service')->getData([
            'parent' => [
                'entity' => 'discount',
                'parentId' => $discount->getId(),
            ],
            'entity_class' => Voucher::class,
            'datatable_type' => VoucherDatatableType::class,
            'form_type' => VoucherFormType::class,
            'base_route' => 'admin_discount_discount',
        ]);
    }

    /**
     * @Route("/{id}/dupliceren", methods={"GET"})
     * @param Discount $id
     * @return RedirectResponse
     */
    public function duplicate(Discount $id)
    {
        $duplicatedDiscount = clone $id;
        $duplicatedDiscount->setDescription($duplicatedDiscount->getDescription().' (duplicaat)');

        $em = $this->getDoctrine()->getManager();

        $em->persist($duplicatedDiscount);
        $em->flush();

        return $this->redirectToRoute('admin_discount_discount_edit', ['id' => $duplicatedDiscount->getId()]);
    }
}
