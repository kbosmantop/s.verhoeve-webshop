<?php

namespace AdminBundle\Controller\Discount;

use AdminBundle\Components\Crud\CrudActionResponse;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Discount\VoucherDatatableType;
use AdminBundle\Form\Discount\VoucherFormType;
use AppBundle\Entity\Discount\Voucher;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * @Route("/vouchers")
 * Class VoucherController
 * @package AdminBundle\Controller\Discount
 */
class VoucherController extends CrudController {

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Voucher::class,
            'form_type' => VoucherFormType::class,
            'datatable' => [
                'entity_class' => Voucher::class,
                'form_type' => VoucherFormType::class,
                'datatable_type' => VoucherDatatableType::class,
                'allow_read' => false,
                'query_builder' => function(QueryBuilder $qb) {
                    return $qb->andWhere('voucher.batch IS NULL')
                        ->leftJoin('discount.companies','c')
                        ->having('COUNT(c.id) = 0')
                        ->groupBy('voucher.id')
                        ->orderBy('voucher.id', 'DESC');
                },
                'bulk_actions' => [
                    [
                        'route' => 'admin_discount_voucher_extend',
                        'label' => 'Actie verlengen',
                        'xhr' => true,
                        'multiple' => true,
                        'pre_confirm' => [
                            'title' => 'Actie verlengen',
                            'text' => 'Hoeveel dagen moeten de geselecteerde vouchers verlengd worden?',
                            'type' => 'question',
                            'input' => 'range',
                            'confirmButtonColor' => '#5cb85c',
                            'inputAttributes' => [
                                'min' => 1,
                                'max' => 60,
                                'step' => 1,
                            ],
                            'inputValue' => 1,
                        ],
                        'method' => 'POST',
                        'icon' => 'mdi mdi-plus',

                    ],
                ],
            ]
        ];
    }

    /**
     * @Route("/extend", methods={"POST"})
     * @param Request $request
     * @return CrudActionResponse
     */
    public function extendAction(Request $request)
    {
        $ids = $request->request->get('ids');
        $days = $request->request->get('data');

        try {
            $days = (int)$days;
            if($days === null || $days <= 0 || $days > 60){
                throw new \OutOfBoundsException('Days must be integer between 1 and 60');
            }

            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $em = $this->getDoctrine()->getManager();

            $vouchers = $this->getRepository()->findBy([
                'id' => $ids,
            ]);

            /** @var Voucher $voucher */
            foreach ($vouchers as $voucher) {
                $newDate = clone $voucher->getValidUntil();
                $newDate->modify(sprintf('+%d day', $days));

                $voucher->setValidUntil($newDate);
            }

            $em->flush();
        } catch (\Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'title' => 'Actie is succesvol uitgevoerd',
                'type' => 'success',
                'text' => 'De geselecteerde items zijn succesvol verlengd.',
                'callback' => 'CrudRefresh',
            ],
        ]);
    }
}
