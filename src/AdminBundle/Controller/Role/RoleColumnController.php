<?php

namespace AdminBundle\Controller\Role;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Security\Employee\RoleColumn;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/rollen/kolommen")
 * Class RoleColumnController
 * @package AdminBundle\Controller\Role
 */
class RoleColumnController extends Controller
{

    /**
     * @Route("/{column}/delete", methods={"DELETE"})
     * @param RoleColumn $column
     *
     * @return JsonResponse
     */
    public function deleteColumnAction(RoleColumn $column)
    {
        $this->container->get('cms.role_column')->removeColumn($column);

        return new JsonResponse("ok");
    }

}
