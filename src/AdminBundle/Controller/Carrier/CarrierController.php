<?php

namespace AdminBundle\Controller\Carrier;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Carrier\CarrierDatatableType;
use AdminBundle\Datatable\Carrier\DeliveryMethodDatatableType;
use AdminBundle\Form\Carrier\CarrierFormType;
use AdminBundle\Form\Carrier\DeliveryMethodFormType;
use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/transporteurs")
 * Class CarrierController
 * @package AdminBundle\Controller\Carrier
 */
class CarrierController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => Carrier::class,
            'form_type' => CarrierFormType::class,
            'datatable' => [
                'entity_class' => Carrier::class,
                'form_type' => CarrierFormType::class,
                'datatable_type' => CarrierDatatableType::class,
                'allow_read' => true,
                'allow_edit' => false,
                'allow_delete' => true,
            ],
            'view' => [
                'label_subject' => '%%shortName%%',
                'panels' => [
                    [
                        'id' => 'information_panel',
                        'type' => 'template',
                        'template' => '@Admin/Company/information-panel.html.twig',
                        'template_parameters' => [
                            'showConnector' => true,
                            'company' => (function () {
                                if ($this->getRequest()->attributes->has('id')) {
                                    $carrier = $this->getDoctrine()->getRepository(Carrier::class)->find($this->getRequest()->attributes->get('id'));
                                    if ($carrier !== null) {
                                        return $carrier->getCompany();
                                    }
                                }
                                return null;
                            })(),
                        ],
                    ],
                    [
                        'id' => 'tab_panel',
                        'type' => 'tabs',
                        'tabs' => [
                            [
                                'action' => 'leverings_wijzen',
                                'label' => 'Beschikbare leveringswijzen',
                                'datatable' => [
                                    'alias' => 'dlme',
                                    'datatable_type' => DeliveryMethodDatatableType::class,
                                    'form_type' => DeliveryMethodFormType::class,
                                    'entity_class' => DeliveryMethod::class,
                                    'base_route' => 'admin_carrier_deliverymethod',
                                    'allow_add' => false,
                                    'allow_delete' => false,
                                    'allow_read' => false,
                                    'allow_edit' => true,
                                    'query_builder' => static function (QueryBuilder $qb, Request $request = null) {
                                        if (null !== $request) {
                                            return $qb
                                                ->leftJoin('dlme.carriers', 'carr')
                                                ->where('carr.id = :value')
                                                ->setParameters(['value' => $request->attributes->get('id')]);
                                        }

                                        return $qb;
                                    },
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-delete',
                                            'route' => 'admin_carrier_carrier_deletedeliverymethod',
                                            'route_parameters' => [
                                                'carrier' => $this->getRequest()->get('id'),
                                                'deliveryMethod' => 1,
                                            ],
                                            'method' => 'DELETE',
                                            'type' => 'delete',
                                            'label' => 'Verwijderen',
                                        ],
                                    ],
                                    'actions' => [
                                        [
                                            'icon' => 'mdi mdi-plus',
                                            'route' => 'admin_carrier_carrier_adddeliverymethod',
                                            'route_parameters' => [
                                                'carrier' => $this->getRequest()->get('id'),
                                            ],
                                            'method' => 'POST',
                                            'label' => 'Toevoegen',
                                            'xhr' => true,
                                            'action' => 'pre_confirm',
                                            'className' => 'btn-success',
                                            'type' => 'add-delivery-method',
                                            'pre_confirm' => [
                                                'title' => 'Transportwijze toevoegen',
                                                'text' => 'Voeg een bestaande transportwijze toe of maak een nieuwe.',
                                                'type' => null,
                                                'confirmButtonText' => 'Toevoegen',
                                                'showConfirmButton' => true,
                                                'cancelButtonText' => 'Annuleren',
                                                'showCancelButton' => true,
                                                'reverseButtons' => true,
                                                'input' => 'select',
                                                'confirmButtonColor' => '#5cb85c',
                                                'inputOptions' => (function () {
                                                    /** @var EntityManagerInterface $entityManager */
                                                    $entityManager = $this->getDoctrine()->getManager();
                                                    /** @var QueryBuilder $qb */
                                                    $qb = $entityManager->createQueryBuilder();
                                                    $qb
                                                        ->select(['dlme.id', 'dlme.name'])
                                                        ->from(DeliveryMethod::class, 'dlme')
                                                        ->leftJoin('dlme.carriers', 'carr')
                                                        ->where('carr.id IS NULL');
                                                    $options = [
                                                        //Disable new option
                                                        //'' => 'Nieuwe transportwijze aanmaken',
                                                    ];
                                                    foreach ($qb->getQuery()->getScalarResult() as $result) {
                                                        $options[$result['id']] = $result['name'];
                                                    }
                                                    return $options;
                                                })(),
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/{carrier}/leveringswijze/toevoegen", methods={"POST"})
     * @Breadcrumb()
     *
     * @param Carrier         $carrier
     * @param Request         $request
     * @param RouterInterface $router
     * @return JsonResponse
     */
    public function addDeliveryMethod(Carrier $carrier, Request $request, RouterInterface $router): JsonResponse
    {
        $deliveryMethodId = $request->request->get('data');
        if (null !== $deliveryMethodId && $deliveryMethodId !== '') {
            /** @var DeliveryMethod $deliveryMethod */
            $deliveryMethod = $this->getDoctrine()->getRepository(DeliveryMethod::class)->find($deliveryMethodId);
            if (null !== $deliveryMethod) {
                $deliveryMethod->addCarrier($carrier);
                $this->getDoctrine()->getManager()->flush();
                return $this->json([
                    'message' => [
                        'type' => 'success',
                        'text' => sprintf('Transportwijze %s is toegevoegd aan transporteur %s',
                            $deliveryMethod->getName(), $carrier->getShortName()),
                        'title' => 'Transportwijze toegevoegd',
                        'callback' => 'CrudRefresh',
                    ],
                ]);
            }
            return $this->json([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is iets mis gegaan, geen transportwijze toegevoegd',
                    'title' => 'Fout',
                ],
            ]);
        }
        return $this->json([
            'message' => [
                'type' => 'success',
                'text' => 'Transportwijze toevoegen',
                'title' => 'Toevoegen',
                'redirect' => $router->generate('admin_carrier_deliverymethod_addtocarrier', [
                    'carrier' => $carrier->getId(),
                ]),
            ],
        ]);
    }

    /**
     * @Route("/{carrier}/leveringswijze/{deliveryMethod}/verwijderen", methods={"DELETE"})
     * @Breadcrumb()
     *
     * @param Carrier        $carrier
     * @param DeliveryMethod $deliveryMethod
     * @param Request        $request
     * @return JsonResponse
     */
    public function deleteDeliveryMethod(
        Carrier $carrier,
        DeliveryMethod $deliveryMethod,
        Request $request
    ): JsonResponse {
        $carrier->removeDeliveryMethod($deliveryMethod);
        $this->getDoctrine()->getManager()->flush();
        return $this->json([
            'message' => [
                'type' => 'success',
                'text' => sprintf('Transportwijze %s is verwijderd van transporteur %s',
                    $deliveryMethod->getName(), $carrier->getShortName()),
                'title' => 'Transportwijze verwijderd',
                'callback' => 'CrudRefresh',
            ],
        ]);
    }

}
