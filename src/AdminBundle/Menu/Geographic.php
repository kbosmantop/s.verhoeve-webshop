<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Geographic implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function menu(FactoryInterface $factory, array $options)
    {
        void($options);

        $menu = $factory->createItem('root');

        $menu->addChild('Landen', ['route' => 'admin_settings_geographic_country_index'])->setAttributes([]);

        $menu->addChild('Provincies',
            ['route' => 'admin_settings_geographic_province_index'])->setAttributes([]);

        $menu->addChild('Plaatsen', ['route' => 'admin_settings_geographic_city_index'])->setAttributes([]);

        $menu->addChild('Postcodes',
            ['route' => 'admin_settings_geographic_postcode_index'])->setAttributes([]);

        return $menu;
    }
}