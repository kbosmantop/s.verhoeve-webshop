<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Account implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function menu(FactoryInterface $factory, array $options)
    {
        void($options);

        $menu = $factory->createItem('root');

        $menu->setAttribute('icon', 'home');

        $menu->addChild('Algemeen')->setAttributes([
            "navigation_header" => true,
        ]);

        $menu->addChild('Gegevens wijzigen', ['route' => 'admin_user_profile_edit'])->setAttributes([
            "icon" => "pencil",
        ]);

        $menu->addChild('Wachtwoord wijzigen', ['route' => 'admin_user_change_password'])->setAttributes([
            "icon" => "key",
        ]);

        $menu->addChild('Beveiliging')->setAttributes([
            "navigation_header" => true,
        ]);

        $menu->addChild('Twee stap authenticatie',
            ['route' => 'admin_user_profile_two_step'])->setAttributes([
            "icon" => "shield2",
        ]);

        $menu->addChild('IP-adressen', ['route' => 'admin_user_profile_two_step'])->setAttributes([
            "icon" => "unlocked",
        ]);

        return $menu;
    }
}
