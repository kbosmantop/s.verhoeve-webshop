<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Main
 * @package AdminBundle\Menu
 */
class Main extends AbstractMenu implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     *
     * @return ItemInterface
     */
    public function menu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'navigation navigation-main navigation-xs',
            ],
        ]);

        $menu->setAttribute('icon', 'home');
        $menu->setLabel('Home');

        $this->dashboardMenu($menu);
        $this->customerMenu($menu);
        $this->salesMenu($menu);
        $this->catalogMenu($menu);
        $this->contentMenu($menu);
        $this->marketingMenu($menu);
        $this->financialMenu($menu);

        $this->settingsMenu($menu);

        $this->applyUserRoles($menu);
        $this->applyExtraRoutes($menu);

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     */
    private function dashboardMenu(ItemInterface $menu)
    {
        $label = 'Dashboard';

        $menu->addChild('Algemeen', [])->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild($label, ['route' => 'admin_dashboard_index'])->setAttributes([
            'icon' => 'home-outline',
        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function rulesMenu(ItemInterface $menu)
    {
        $label = 'Regels';
        $menu->addChild($label, ['route' => ''])->setAttributes([
            'icon' => 'chart-timeline',
        ]);

        $menu[$label]->addChild('Regels', ['route' => 'admin_rule_index']);
        $menu[$label]->addChild('Kortingsvelden', ['route' => 'admin_discount_discountproperty_index']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function settingsMenu(ItemInterface $menu)
    {

        $label = 'Configuratie';

        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $label = 'Instellingen';

        $menu->addChild($label)->setAttributes([
            'icon' => 'wrench',
        ]);

        $menu[$label]->addChild('Regels');
        $menu[$label]['Regels']->addChild('Regels', ['route' => 'admin_rule_index']);
        $menu[$label]['Regels']->addChild('Kortingsvelden', ['route' => 'admin_discount_discountproperty_index']);

        $menu[$label]->addChild('Websites', ['route' => 'admin_settings_site_index']);

        $menu[$label]->addChild('Betaalmethoden',
            ['route' => 'admin_settings_paymentmethod_index']);

        $this->orderProcessingMenu($menu[$label]);

        $menu[$label]->addChild('Geografisch',
            ['route' => 'admin_settings_geographic_country_index']);

        $labelCarriers = 'Transporteurs';
        $menu[$label]->addChild($labelCarriers);
        $menu[$label][$labelCarriers]->addChild('Leveringswijzen', ['route' => 'admin_carrier_deliverymethod_index']);
        $menu[$label][$labelCarriers]->addChild('Transporteurs', ['route' => 'admin_carrier_carrier_index']);

        $this->parametersMenu($menu[$label]);
        $this->variablesMenu($menu[$label]);

        $menu[$label]->addChild('Kiyoh', ['route' => 'admin_settings_kiyoh_index']);

        $labelAccess = 'Toegang';

        $menu[$label]->addChild($labelAccess);

        $menu[$label][$labelAccess]->addChild('Gebruikers', ['route' => 'admin_authentication_user_index']);
        $menu[$label][$labelAccess]->addChild('Gebruikers groepen', ['route' => 'admin_authentication_group_index']);
        $menu[$label][$labelAccess]->addChild('Gebruikers rollen', ['route' => 'admin_role_rolecategory_index']);

        $menu[$label]->addChild('Klant authenticatie Log',
            ['route' => 'admin_customer_customerauthenticationlog_index'])->setAttributes([
            'divider_prepend' => true,
        ]);

        $menu[$label]->addChild('WMS', ['route' => 'admin_settings_wmsordertype_index']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function variablesMenu(ItemInterface $menu)
    {
        $label = 'Variabelen';

        $menu->addChild($label, ['route' => '']);

        $menu[$label]->addChild('Type bezorgadres',
            ['route' => 'admin_settings_variables_deliveryaddresstype_index']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function orderProcessingMenu(ItemInterface $menu)
    {
        $label = 'Orderverwerking';

        $menu->addChild($label, ['route' => '']);

        $menu[$label]->addChild('Prioriteitsregels', ['route' => 'admin_settings_orderpriorityrule_index']);

        $menu[$label]->addChild('Fraude detectie', ['route' => 'admin_fraud_fraud_index']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function parametersMenu(ItemInterface $menu)
    {
        $label = 'Parameters';

        $menu->addChild($label, ['route' => '']);

        $menu[$label]->addChild('Parameters', [
            'route' => 'admin_parameters_index',
        ]);

        $menu[$label]->addChild('Toegewezen parameters', [
            'route' => 'admin_parameter_parameterentity_index',
        ]);

        $menu[$label]->addChild('Systeem parameters', [
            'route' => 'admin_parameters_system',
        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function customerMenu(ItemInterface $menu)
    {
        $label = 'Relaties';

        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild('Contacten')->setAttributes([
            'icon' => 'account-key',
        ]);

        $menu['Contacten']->addChild('Contacten',
            ['route' => 'admin_customer_customer_index']);

        $menu['Contacten']->addChild('Groepen',
            ['route' => 'admin_customer_customergroup_index']);

        $menu->addChild('Bedrijven', ['route' => 'admin_customer_company_index'])->setAttributes([
            'icon' => 'domain',
        ]);

        $menu->addChild('Leveranciers')->setAttributes([
            'icon' => 'truck',
        ]);

        $menu['Leveranciers']->addChild('Leveranciers', ['route' => 'admin_supplier_supplier_index']);

        $menu['Leveranciers']
            ->addChild('Bezorggebieden', [
                'route' => 'admin_customer_deliveryarea_index',
            ]);

        $menu['Leveranciers']->addChild('Groepen',
            ['route' => 'admin_supplier_suppliergroup_index']);

    }

    /**
     * @param ItemInterface $menu
     */
    private function salesMenu(ItemInterface $menu)
    {
        $label = 'Orderverwerking';
        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild('Bestellingen', ['route' => 'admin_sales_order_index'])->setAttributes([
            'icon' => 'store',
        ]);


//        $menu->addChild('Abonnementen',
//            ['route' => 'admin_customer_subscription_index'])->setAttributes([
//            'icon' => 'repeat',
//        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function catalogMenu(ItemInterface $menu)
    {
        $label = 'Catalogus';
        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $productIcons = [
            'flower',
            'cake-variant',
            'food-apple',
            'glass-wine',
        ];

        $productIcon = $productIcons[array_rand($productIcons)];

        $labelProducts = 'Producten';

        $menu->addChild($labelProducts, [])->setAttributes([
            'icon' => $productIcon,
        ]);

        $menu[$labelProducts]->addChild('Producten', ['route' => 'admin_catalog_product_index']);

        $menu[$labelProducts]->addChild('Productgroepen', ['route' => 'admin_catalog_productgroup_index']);

        $menu[$labelProducts]->addChild('Verpakkingseenheden',
            ['route' => 'admin_catalog_product_packagingunit_index']);

        $menu[$labelProducts]->addChild('Kenmerken', []);
        $menu[$labelProducts]['Kenmerken']->addChild('Sets',
            ['route' => 'admin_catalog_productgrouppropertyset_index']);

        $menu[$labelProducts]['Kenmerken']->addChild('Productkenmerken',
            ['route' => 'admin_catalog_productgroupproperty_index']);

        $menu[$labelProducts]->addChild('Stickers',
            ['route' => 'admin_catalog_product_productsticker_index']);

        $menu->addChild('Kaartjes', ['route' => 'admin_catalog_productcard_index'])->setAttributes([
            'icon' => 'cards-playing-outline',
        ]);

        $menu->addChild('Personalisaties', [])->setAttributes([
            'icon' => 'message-draw',
        ]);

        $labelPersonalization = 'Personalisaties';

        $menu[$labelPersonalization]->addChild('Producten', ['route' => 'admin_catalog_productpersonalization_index']);

        $menu->addChild('Verpakkingseenheden',
            ['route' => 'admin_catalog_product_packagingunit_index'])->setAttributes([
            'icon' => 'wall',
        ]);

        $menu[$labelPersonalization]->addChild('Designs', []);

        $labelDesigns = 'Designs';

        $menu[$labelPersonalization][$labelDesigns]->addChild('Templates',
            ['route' => 'admin_settings_designer_designertemplate_index']);

        $menu[$labelPersonalization][$labelDesigns]->addChild('Vormen',
            ['route' => 'admin_settings_designer_designercanvas_index']);

        $menu[$labelPersonalization][$labelDesigns]->addChild('Collages',
            ['route' => 'admin_settings_designer_designercollage_index']);

        $menu[$labelPersonalization][$labelDesigns]->addChild('Lettertypen',
            ['route' => 'admin_settings_designer_designerfont_index']);

        $menu[$labelPersonalization][$labelDesigns]->addChild('Labels',
            ['route' => 'admin_settings_designer_tag_index']);


        $menu->addChild('Brieven', ['route' => 'admin_catalog_productletter_index'])->setAttributes([
            'icon' => 'upload',
        ]);

        $menu->addChild('Bezorgkosten', ['route' => 'admin_catalog_producttransporttype_index'])->setAttributes([
            'icon' => 'truck',
        ]);

        $menu->addChild('Assortimenten',
            ['route' => 'admin_catalog_assortment_index'])->setAttributes([
            'icon' => 'apps',
        ]);

        $menu->addChild('Voorraadbeheer', ['route' => 'admin_stock_index'])->setAttributes([
            'icon' => 'buffer',
        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function contentMenu(ItemInterface $menu)
    {
        $label = 'Content';
        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild('Pagina\'s', ['route' => 'admin_site_page_index'])->setAttributes([
            'icon' => 'library-books',
        ]);

        $menu->addChild('Banners', ['route' => 'admin_site_banner_index'])->setAttributes([
            'icon' => 'image-filter',
        ]);

        $menu->addChild('USP\'s', ['route' => 'admin_site_usp_index'])->setAttributes([
            'icon' => 'check-all',
        ]);

        $menu->addChild('SEO Redirects', ['route' => 'admin_seo_redirect_index'])->setAttributes([
            'icon' => 'directions-fork',
        ]);

        $this->navigationMenu($menu);
    }

    /**
     * @param ItemInterface $menu
     */
    private function marketingMenu(ItemInterface $menu)
    {
        $label = 'Marketing';
        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild('Kortingen',
            ['route' => 'admin_discount_discount_index'])->setAttributes([
            'icon' => 'sale',
        ]);

        $menu->addChild('Vouchers')->setAttributes([
            'icon' => 'ticket-percent',
        ]);

        $menu['Vouchers']->addChild('Vouchers', [
            'route' => 'admin_discount_voucher_index',
        ]);

        $menu['Vouchers']->addChild('Batches', [
            'route' => 'admin_discount_voucherbatch_index',
        ]);

        $menu->addChild('Rapportages', ['route' => 'admin_report_report_index'])->setAttributes([
            'icon' => 'file-chart',
        ]);
    }

    /**
     * @param ItemInterface $menu
     */
    private function navigationMenu(ItemInterface $menu)
    {
        $label = 'Navigatie';
        $menu->addChild($label)->setAttributes([
            'icon' => 'file-tree',
        ]);;

        $menu[$label]->addChild('Instellingen',
            ['route' => 'admin_site_navigation_menu_index']);

        $menu[$label]->addChild('Menu\'s', ['route' => 'admin_site_navigation_index']);
    }

    /**
     * @param ItemInterface $menu
     */
    private function financialMenu(ItemInterface $menu)
    {
        $label = 'Financieel';
        $menu->addChild($label)->setAttributes([
            'navigation_header' => true,

        ]);

        $menu->addChild('BTW')->setAttributes([
            'icon' => 'coin',
        ]);

        $menu['BTW']->addChild('BTW Groepen', ['route' => 'admin_finance_vat_vatgroup_index']);
        $menu['BTW']->addChild('BTW Niveaus', ['route' => 'admin_finance_vat_vatlevel_index']);

        //TODO implement in WEB-2948
        //$menu['BTW']->addChild('BTW Regels', ['route' => 'admin_finance_vat_vatrule_index']);

        $menu->addChild('Exports')->setAttributes([
            'icon' => 'file-export',
        ]);

        $menu['Exports']->addChild('Facturatie (Twinfield)', ['route' => 'admin_finance_export_index']);
        $menu['Exports']->addChild('Facturatie (Exact Globe)', ['route' => 'admin_finance_invoiceexport_index']);
        $menu['Exports']->addChild('Maandafrekeningen', ['route' => 'admin_finance_commissioninvoice_index']);
        $menu['Exports']->addChild('Batches', ['route' => 'admin_finance_commissionbatch_index']);

        $menu->addChild('Grootboeken', ['route' => 'admin_finance_ledger_index'])->setAttributes([
            'icon' => 'book-open',
        ]);
    }
}
