<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Settings implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function menu(FactoryInterface $factory, array $options)
    {
        void($options);

        $menu = $factory->createItem('root');
        $menu->setAttribute('icon', 'home');
        $menu->addChild('Toegang')->setAttributes([
            "navigation_header" => true,
        ]);

        $menu->addChild('Groepen', ['route' => 'admin_authentication_group_index']);
        $menu->addChild('Gebruikers', ['route' => 'admin_authentication_user_index']);
        $menu->addChild('Whitelist IP-addressen', ['route' => 'admin_authentication_ipwhitelist_index']);

        return $menu;
    }
}
