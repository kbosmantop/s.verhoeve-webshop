<?php

namespace RuleBundle\Command;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Column;
use RuleBundle\Annotation as Rule;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Property;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PropertyCommand
 */
class PropertyCommand extends ContainerAwareCommand
{
    /** @var EntityManagerInterface $em */
    protected $em;

    /** @var AnnotationReader $annotationReader */
    protected $annotationReader;

    /** @var array  */
    protected $currentProperties = [];

    /**
     * PropertyCommand constructor.
     * @throws AnnotationException
     */
    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();

        parent::__construct();
    }

    /**
     * Configures the command
     */
    protected function configure()
    {
        $this->setName('topgeschenken:entities:properties')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \TypeError
     * @throws \ReflectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        /**
         * @var Entity $entity
         */
        $entityId = (int)$input->getArgument('id');
        $entity = $this->em->getRepository(Entity::class)->find($entityId);

        if(null !== $entity) {
            $reflection = new \ReflectionClass($entity->getFullyQualifiedName());

            if($reflection->isInterface()) {
                $this->loopMethods($entity, $reflection);
            } else {
                $this->loopProperties($entity, $reflection);
            }

            $propertyIds = [];
            //remove all old references
            /** @var Property $property */
            foreach($this->currentProperties as $property) {
                $propertyIds[] = $property->getId();
            }

            $qb = $this->em->createQueryBuilder();
            $qb->delete(Property::class, 'ep')
                ->andWhere('ep.id NOT IN(:ids)')
                ->andWhere('ep.entity = :entityId')
                ->setParameters([
                   'ids' => $propertyIds,
                   'entityId' => $entityId
                ]);

            $qb->getQuery()->execute();
        }

        return 0;
    }

    /**
     * @param Entity           $entity
     * @param \ReflectionClass $reflection
     * @throws \TypeError
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function loopProperties(Entity $entity, \ReflectionClass $reflection) {
        foreach ($reflection->getProperties() as $property) {
            $annotation = $this->annotationReader->getPropertyAnnotation($property, Rule\Property::class);

            if ($annotation !== null) {
                $criteria = [
                    'entity' => $entity,
                    'name' => $property->getName(),
                ];

                /**
                 * @var Property $object
                 */
                $object = $this->em->getRepository(Property::class)->findOneOrCreate($criteria);
                $object->setType($this->getType($property));
                $object->setDescription($annotation->getDescription());

                $this->currentProperties[] = $object;
            }
        }

        $this->em->flush();
    }

    /**
     * @param Entity           $entity
     * @param \ReflectionClass $reflection
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \TypeError
     * @throws \Exception
     */
    private function loopMethods(Entity $entity, \ReflectionClass $reflection) {
        foreach ($reflection->getMethods() as $method) {
            /** @var Rule\Method $annotation */
            $annotation = $this->annotationReader->getMethodAnnotation($method, Rule\Method::class);

            if (!is_null($annotation)) {
                $criteria = [
                    'entity' => $entity,
                    'name' => lcfirst(str_replace('get', null, $method->getName())),
                ];

                /**
                 * @var Property $object
                 */
                $object = $this->em->getRepository(Property::class)->findOneOrCreate($criteria);

                if($annotation->getRelatedEntity()) {
                    $object->setType($annotation->getRelatedEntity());
                } else {
                    $type = $this->getMethodReturnType($method);

                    $object->setType($type);
                }

                $object->setDescription($annotation->getDescription());

                $this->currentProperties[] = $object;
            }
        }

        $this->em->flush();
    }

    /**
     * @param \ReflectionProperty $property
     * @return string
     */
    protected function getType(\ReflectionProperty $property)
    {
        $annotation = $this->annotationReader->getPropertyAnnotation($property, Column::class);

        if ($annotation) {
            return $annotation->type;
        }

        $em = $this->getContainer()->get('doctrine')->getManager();
        $classMetaData = $em->getMetadataFactory()->getMetadataFor($property->getDeclaringClass()->getName());

        return $classMetaData->getAssociationTargetClass($property->getName());
    }

    /**
     * @param \ReflectionMethod $method
     * @return mixed
     * @throws \Exception
     */
    private function getMethodReturnType(\ReflectionMethod $method) {
        $doc = $method->getDocComment();
        preg_match_all('#@(.*?)\n#s', $doc, $annotations);

        if(isset($annotations[1])) {
            foreach($annotations[1] as $annotation) {
                $parts = explode(' ', $annotation);
                if($parts[0] == 'return') {
                    return $parts[1];
                }
            }
        }

        throw new \Exception(sprintf("No return type found for method '%s' in class '%s'", $method->getName(), $method->getDeclaringClass()->getName()));
    }
}
