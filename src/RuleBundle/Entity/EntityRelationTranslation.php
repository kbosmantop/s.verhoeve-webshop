<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;

/**
 * Class EntityRelationTranslation
 * @package RuleBundle\Entity
 *
 * @ORM\Entity
 */
class EntityRelationTranslation
{
    use Translation;

    /**
     * @var string $label
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $label;

    /**
     * Set label
     *
     * @param string $label
     *
     * @return EntityRelationTranslation
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}
