<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Field
 * @package RuleBundle\Entity
 *
 * todo: remove baseRepo from appbundle
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\Table("entity_property")
 */
class Property
{
    use TimestampableEntity;

    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Entity $entity
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Entity", inversedBy="properties")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $entity;

    /**
     * @var string $fieldName
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $type;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $description;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescription() . ' ('.$this->getEntity()->getFullyQualifiedName().')';
    }

    /**
     * @param Entity $entity
     * @return $this
     */
    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
