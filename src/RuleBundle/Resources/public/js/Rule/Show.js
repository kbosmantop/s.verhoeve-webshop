function RuleShow(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.3 + 'px',
        zIndex: 2000,
    });

    this.open(url);
}

RuleShow.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    $.get(this.url).done(function (data) {
        self.contentWindow.setHtml(data);
    }).fail(function () {
        console.error('Something went wrong, please reload the page or try again');
    });
};
