$(function () {
    $('[data-action="rule_create"]').on('click', function (e) {
        e.preventDefault();

        new RuleCreate($(this).data('url'));
    });

    $('[data-action="rule_show"]').on('click', function (e) {
        e.preventDefault();

        new RuleShow($(this).data('url'));
    });

    $('[data-action="rule_edit"]').on('click', function (e) {
        e.preventDefault();

        new RuleEdit($(this).data('url'));
    });

    $('[data-action="rule_delete"]').on('click', function (e) {
        e.preventDefault();

        deleteModal('regel', $(this).data('url'));
    });

    $('body').on('click', '[data-type="rule_edit"]', function (e) {
        e.preventDefault();

        new RuleEdit($(this).attr('href'));
    });

    function deleteModal(type, url) {
        swal({
            type: 'question',
            title: 'Weet je het zeker?',
            text: 'Weet je zeker dat je deze ' + type + ' wilt verwijderen? deze actie kan niet ongedaan gemaakt worden.',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger'
        }).then(function () {
            $.ajax({
                url: url,
                method: 'GET'
            }).done(function () {
                swal({
                    type: 'success',
                    text: type + ' verwijderd'
                });
            }).fail(function () {
                swal({
                    type: 'warning',
                    text: 'Er is iets fout gegaan, probeer het later opnieuw!'
                });
            });
        }, function (dismiss) {
        });
    }
});
