/**
 * @class TsBootstrapSwitch
 * @description Applies Bootstrap Switch on checkbox.
 * @param {object} [options]
 * @param {string} [options.showWeekNumbers=true]
 * @throws MissingLibraryError
 */
$.fn.queryBuilder.define('ts-bootstrap-switch', function (options) {
    if (!$.fn.bootstrapSwitch) {
        throw Error('Bootstrap bootstrapSwitch is required to use "ts-bootstrap-switch" plugin.');
    }

    this.on('afterCreateRuleInput', function (e, rule) {
        var input = $(rule.$el, '.rule-input-container').find('input:checkbox');
        var newInput = input.clone(false);

        input.hide();

        newInput.attr('data-original-name', newInput.attr('name'));
        newInput.attr('name', newInput.attr('name') + '_clone');

        $(newInput).insertAfter(input);

        options.state = input.prop('checked');

        newInput
        .bootstrapSwitch(options)
        .on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                input
                .attr('checked', true)
                .prop('checked', true)
                .trigger('change');
            } else {
                input
                .removeAttr('checked')
                .prop('checked', false)
                .trigger('change');
            }
        });
        ;


    }).on('afterUpdateRuleValue', function (e, rule) {
        var input = $(rule.$el, '.rule-input-container').find('input:checkbox');
        var newInput = $('[data-original-name="' + input.attr('name') + '"]');

        if (rule._updating_value) {
            $(newInput).bootstrapSwitch('state', input.prop('checked'), true);
        }
    });
}, {
    onText: 'Ja',
    offText: 'Nee',
    size: 'mini',
    onColor: 'success',
    offColor: 'danger'
});
