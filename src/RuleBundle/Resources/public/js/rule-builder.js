function RuleBuilder(element, options) {
    if (!element) {
        throw new Error('You must pass a DOM-element or identifier in order to initialize a RuleBuilder');
    }

    if (!(element instanceof jQuery)) {
        element = $(element);
    }

    this.element = element;
    this.queryBuilderObject = null;
    this.deleteConfirmed = false;

    this.filters = null;
    this.rules = null;

    this.options = options;
}

RuleBuilder.prototype.getOption = function (key) {
    if (!this.options.hasOwnProperty(key)) {
        return false;
    }

    return decodeURI(this.options[key]);
};

RuleBuilder.prototype.check = function () {
    if (!this.filters) {
        throw new Error('You must set filters in order to create a RuleBuilder');
    }
};

RuleBuilder.prototype.create = function () {
    this.check();

    this.createQueryBuilder();
};

RuleBuilder.prototype.setFilters = function (filters, update) {
    let self = this;

    this.filters = filters;

    this.filters.map(function (filter) {
        if (
            filter.hasOwnProperty('valueSetter')
            && filter.hasOwnProperty('data')
            && filter.data.hasOwnProperty('ajax')
        ) {
            filter.valueSetter = function (rule, value) {
                self.processFilterValueSelect(rule, value, filter);
            }
        }
    });

    if (update) {
        this.queryBuilderObject.queryBuilder('setFilters', true, this.filters);
    }
};

RuleBuilder.prototype.getRules = function () {
    return this.queryBuilderObject.queryBuilder('getRules');
};

RuleBuilder.prototype.setRules = function (rules, update) {
    this.rules = rules;

    if (update) {
        this.queryBuilderObject.queryBuilder('setRules', rules);
    }
};

RuleBuilder.prototype.createQueryBuilder = function () {
    let self = this;

    self.queryBuilderObject = this.element.queryBuilder({
        allow_empty: true,
        select_placeholder: 'Geen selectie',
        plugins: [
            'bt-tooltip-errors',
            'ts-daterange-picker',
            'ts-bootstrap-switch'
        ],
        lang: {
            conditions: {
                'AND': 'Alle',
                'OR': 'Minstens een'
            },
            add_rule: 'voorwaarde',
            add_group: 'voorwaarden groep',
            operators: {
                equal: 'is',
                not_equal: 'is niet',
                date_between: 'Tussen',
                date_not_between: 'Niet tussen'
            }
        },
        lang_code: 'nl',
        icons: {
            add_rule: 'icon-insert-template',
            add_group: 'icon-align-left',
            remove_rule: 'icon-cross3',
            remove_group: 'icon-cross3',
            error: 'icon-warning'
        },
        inputs_separator: '<span class="input-separator">EN</span>',
        operators: [
            'equal',
            'not_equal',
            'less',
            'less_or_equal',
            'greater',
            'greater_or_equal',
            'begins_with',
            'ends_with',
            'is_null',
            'is_not_null',
            {type: 'between', nb_inputs: 2, apply_to: ['integer', 'double']},
            {type: 'not_between', nb_inputs: 2, apply_to: ['integer', 'double']},
            {type: 'date_between', nb_inputs: 2, apply_to: ['date', 'datetime']},
            {type: 'date_not_between', nb_inputs: 2, apply_to: ['date', 'datetime']}
        ],
        templates: {
            group: self.getRuleBuilderGroupTemplate(),
            rule: self.getRuleBuilderRuleTemplate()
        },
        filters: self.filters
    })
    .on('beforeDeleteGroup.queryBuilder', function (e, group, error, value) {
        self.unbindDeleteRowListener();

        self.deleteGroup(e, group, error, value);
    })
    .on('afterDeleteGroup.queryBuilder', function () {
        self.bindDeleteRowListener();
    })
    .on('afterAddRule.queryBuilder afterAddGroup.queryBuilder', function (e, item) {
        if (!item.hasOwnProperty('$el')) {
            return;
        }
    })
    .on('afterCreateRuleInput.queryBuilder afterCreateRuleOperators.queryBuilder afterCreateRuleFilters.queryBuilder', function (e, item) {
        if (!item.hasOwnProperty('$el')) {
            return;
        }

        switch (e.type) {
            case 'afterCreateRuleFilters':
                self.initSelect(item, '.rule-filter-container', false);

                break;

            case 'afterCreateRuleOperators':
                self.initSelect(item, '.rule-operator-container', false);

                break;

            case 'afterCreateRuleInput':
                switch (item.filter.input) {
                    case 'select':
                        self.initSelect(item, '.rule-value-container', true);

                        break;
                    default:
                        if (['between', 'not_between', 'date_between', 'date_not_between'].indexOf(item.operator.type) !== -1) {
                            $('.rule-value-container', item.$el).addClass('rule-value-container-between');
                        } else {
                            $('.rule-value-container', item.$el).removeClass('rule-value-container-between');
                        }

                        break;
                }

                break;
        }
    })
    .on('afterUpdateRuleFilter.queryBuilder afterUpdateRuleOperator.queryBuilder', function (e, item) {
        var container;

        if (e.type === 'afterUpdateRuleFilter') {
            container = '.rule-filter-container';
        } else if (e.type === 'afterUpdateRuleOperator') {
            container = '.rule-operator-container';
        }

        if (container) {
            $(container, item.$el).find('select.form-control').trigger('change.select2');
        }
    })
    ;

    if (self.rules) {
        // Only call setRules when they are present
        self.queryBuilderObject.queryBuilder('setRules', self.rules);
    } else {
        // If there are no rules set, manually convert the filter select to a select2();
        self.initSelect('#builder_rule_0', '.rule-filter-container', true);
    }

    self.bindDeleteRowListener();
};

RuleBuilder.prototype.initTooltip = function (item) {
    $('[data-tooltip="true"]', item).tooltip();
};

RuleBuilder.prototype.initSelect = function (item, container, liveSearch) {
    var self = this;

    var options = {
        language: 'nl',
        placeholder: 'Typ hier uw zoekterm'
    };

    var filter = item.filter;

    if (filter && filter.hasOwnProperty('data')) {
        if (liveSearch && filter.data.hasOwnProperty('ajax')) {
            options.minimumInputLength = 3;
            options.ajax = {};

            options.ajax.data = function (params) {
                var queryParameters = filter.data.ajax;

                queryParameters.term = params.term;

                return queryParameters;
            };

            options.ajax.method = 'POST';
            options.ajax.url = self.getOption('autocompleteUrl');
            options.ajax.delay = 250;
            options.ajax.processResults = function (data, params) {
                return {
                    results: data
                };
            };
        }
    }

    if (container === '.rule-filter-container') {
        options.templateSelection = function (item) {
            if (item.id === '-1' || item.id === '') {
                return $('<span class="filter-property-placeholder text-grey-300">Deze regel is inactief</span><span class="filter-property-path"></span>');
            }

            var label = $(item.element).parent().attr('label');

            return $('<span class="filter-property-name">' + item.text + '</span><span class="filter-property-path">' + label + '</span>');
        };
    }

    $(container, item.$el).find('select.form-control:not(.hide)').select2(options).trigger('change.select2');
};

RuleBuilder.prototype.deleteGroup = function (e, group, error, value) {
    var self = this;
    var rulesCount = group.rules.length;

    if (self.deleteConfirmed) {
        self.deleteConfirmed = false;

        return;
    }

    e.preventDefault();

    var confirmMessage = 'Dat deze groep met ' + rulesCount + ' voorwaarde' + ((rulesCount !== 1) ? 'n' : '') + ' verwijderd mag worden.';

    swal({
        type: 'question',
        title: 'Weet je het zeker',
        html: confirmMessage,
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ja',
        cancelButtonText: 'Annuleren'
    }).then(function () {
        self.deleteConfirmed = true;

        self.queryBuilderObject.queryBuilder('deleteGroup', group);
    }, function () {
        self.deleteConfirmed = false;
    });
};

RuleBuilder.prototype.deleteRule = function (e, rule, error, value) {
    var self = this;

    if (self.deleteConfirmed) {
        self.deleteConfirmed = false;

        return;
    }

    e.preventDefault();

    var confirmMessage = 'Dat deze voorwaarde verwijderd mag worden';

    swal({
        type: 'question',
        title: 'Weet je het zeker',
        html: confirmMessage,
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ja',
        cancelButtonText: 'Annuleren'
    }).then(function () {
        self.deleteConfirmed = true;

        self.queryBuilderObject.queryBuilder('deleteRule', rule);
    }, function () {
        self.deleteConfirmed = false;
    });
};

RuleBuilder.prototype.bindDeleteRowListener = function () {
    var self = this;

    self.queryBuilderObject.on('beforeDeleteRule.queryBuilder', function (e, rule, error, value) {
        self.deleteRule(e, rule, error, value);
    });
};

RuleBuilder.prototype.unbindDeleteRowListener = function () {
    this.queryBuilderObject.off('beforeDeleteRule.queryBuilder');
};

RuleBuilder.prototype.processFilterValueSelect = function (rule, value, filter) {
    var self = this;

    if (!value) {
        return;
    }

    var select = $('.rule-value-container select.form-control', rule.$el);

    $.ajax({
        type: 'POST',
        url: self.getOption('autocompleteLookupUrl').replace('%%IDENTIFIER%%', value),
        data: filter.data.ajax
    }).then(function (data) {
        select
        .append(
            new Option(data.text, data.id, true, true)
        )
        .trigger('change')
        .trigger({
            type: 'select2:select',
            params: {
                data: data
            }
        });
    });
};

RuleBuilder.prototype.getRuleBuilderGroupTemplate = function () {
    return '<dl id="{{= it.group_id }}" class="rules-group-container"> \
             <dt class="rules-group-header">\
               <div class="btn-group group-conditions">\
                 {{~ it.conditions: condition }}\
                   <label class="btn btn-xs btn-flat">\
                     <input type="radio" name="{{= it.group_id }}_cond" value="{{= condition }}"> {{= it.translate("conditions", condition) }}\
                   </label>\
                 {{~}}\
                 <span class="group-description">van onderstaande voorwaarden</span>\
               </div>\
                <div class="btn-group group-actions">\
                        <button type="button" class="btn btn-xs btn-flat" data-add="rule">\
                        <i class="{{= it.icons.add_rule }}"></i>\
                        <i class="icon-plus3 add-item-stack"></i>\
                        {{= it.translate("add_rule") }}\
                        </button> \
                    {{? it.settings.allow_groups===-1 || it.settings.allow_groups>=it.level }}\
                <button type="button" class="btn btn-xs btn-flat" data-add="group">\
                        <i class="{{= it.icons.add_group }}"></i>\
                        <i class="icon-plus3 add-item-stack"></i>\
                        {{= it.translate("add_group") }}\
                        </button>\
                    {{?}}\
                    {{? it.level>1 }}\
                <button type="button" class="btn btn-xs btn-flat text-danger-300" data-delete="group" data-tooltip="true" title="Verwijder voorwaarden groep">\
                        <i class="{{= it.icons.remove_group }}"></i>\
                        </button>\
                    {{?}}\
                </div>\
               {{? it.settings.display_errors }} \
                 <div class="error-container"><i class="{{= it.icons.error }}"></i></div> \
               {{?}} \
             </dt> \
             <dd class="rules-group-body"> \
               <ul class="rules-list"></ul> \
             </dd> \
           </dl>';
};

RuleBuilder.prototype.getRuleBuilderRuleTemplate = function () {
    return '<li id="{{= it.rule_id }}" class="rule-container"> \
                {{? it.settings.display_errors }} \
                <div class="error-container"><i class="{{= it.icons.error }}"></i></div> \
                {{?}} \
                <div class="rule-filter-container"></div> \
                <div class="rule-operator-container"></div> \
                <div class="rule-value-container"></div> \
                <div class="rule-header">\
                    <div class="btn-group rule-actions"> \
                        <button type="button" class="btn btn-xs btn-flat text-danger-300" data-delete="rule" data-tooltip="true" title="Verwijder voorwaarde"> \
                            <i class="{{= it.icons.remove_rule }}"></i> \
                        </button> \
                    </div> \
                </div> \
            </li>';
};
