<?php

namespace RuleBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"METHOD"})
 * Class MappedProperty
 * @package RuleBundle\Annotation
 */
class MappedProperty {

    /**
     * @var string
     */
    public $property;

    /**
     * @return string
     */
    public function getProperty() {
        return $this->property;
    }

}