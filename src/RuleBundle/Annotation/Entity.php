<?php

namespace RuleBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"CLASS"})
 * Class Entity
 * @package RuleBundle\Annotation
 */
class Entity
{
    /**
     * @var string $description
     */
    public $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}