<?php

namespace RuleBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class Property
 * @package RuleBundle\Annotation
 */
class Property
{
    /** @var string $description */
    public $description;

    /** @var array $autoComplete */
    public $autoComplete;

    /** @var string $relation */
    public $relation;

    /** @var array $definedRelations */
    private $definedRelations = ['parent', 'child', 'root'];

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getAutoComplete()
    {
        return $this->autoComplete;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getRelation() {
        if(!is_null($this->relation) && !in_array($this->relation, $this->definedRelations)) {
            throw new \Exception(sprintf('Invalid relationship given available options are "parent" and "child". "%s" found', $this->relation));
        }
        return $this->relation;
    }
}