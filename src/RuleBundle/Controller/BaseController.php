<?php

namespace RuleBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseController
 * @package RuleBundle\Controller
 */
class BaseController extends Controller
{
    /** @var EntityManagerInterface $em */
    protected $em;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->em = $container->get('doctrine.orm.entity_manager');
    }
}
