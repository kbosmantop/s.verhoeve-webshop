<?php

namespace RuleBundle\Target;

use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use RulerZ\Context\ExecutionContext;
use RulerZ\Result\IteratorTools;

/**
 * Trait CustomFilterTrait
 * @package RuleBundle\Target
 */
trait CustomFilterTrait
{
    /**
     * @param       $target
     * @param array $operators
     * @param array $parameters
     * @return mixed
     */
    abstract protected function execute($target, array $operators, array $parameters);

    /**
     *
     * @param QueryBuilder     $target
     * @param array            $parameters
     * @param array            $operators
     * @param ExecutionContext $context
     * @return QueryBuilder
     */
    public function applyFilter($target, array $parameters, array $operators, ExecutionContext $context)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        foreach ($this->detectedJoins as $join) {
            $target->leftJoin(sprintf('%s.%s', $join['root'], $join['column']), $join['as']);
        }

        // this will return DQL code
        $dql = $this->execute($target, $operators, $parameters);

        // so we apply it to the query builder
        $target->andWhere($dql);

        // now we define the parameters
        foreach ($parameters as $name => $value) {
            if($value instanceof Parameter) {
                $target->setParameter($value->getName(), $value->getValue(), $value->getType());
            } else {
                $target->setParameter($name, $value);
            }
        }

        return $target;
    }

    /**
     * {@inheritdoc}
     */
    public function filter($target, array $parameters, array $operators, ExecutionContext $context)
    {
        /* @var QueryBuilder $target */

        $this->applyFilter($target, $parameters, $operators, $context);

        $dql = $target->getQuery()->getDQL();
        $dql = str_replace(['!= NULL', '= NULL'], ['IS NOT NULL', 'IS NULL'], $dql);

        $q = $target->getEntityManager()->createQuery();
        $q->setDQL($dql);

        foreach ($parameters as $parameter) {
            if($parameter instanceof Parameter) {
                $q->setParameter($parameter->getName(), $parameter->getValue(), $parameter->getType());
            }
        }

        // execute the query
        $result = $q->getResult();

        // and return the appropriate result type
        if ($result instanceof \Traversable) {
            return $result;
        }

        if (\is_array($result)) {
            return IteratorTools::fromArray($result);
        }

        throw new \RuntimeException(sprintf('Unhandled result type: "%s"', \get_class($result)));
    }
}
