<?php

namespace AppBundle\Helper\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Exceptions\OrderStatusNotFoundException;

/**
 * Class OrderStatusHelper
 * @package AppBundle\Helper\Order
 */
class OrderStatusHelper
{
    /**
     * @var array
     */
    private const DESCRIPTIONS = [
        'draft' => 'Concept',
        'archived' => 'Gearchiveerd',
        'cancelled' => 'Geannuleerd',
        'complete' => 'Voltooid (volledig afgerond)',
        'new' => 'Nieuw (nog te verwerken)',
        'on_hold' => 'In de wacht',
        'payment_pending' => 'Wacht op betaling',
        'pending' => 'Verstuurd (naar leverancier)',
        'processed' => 'Verwerkt (door leverancier)',
        'sent' => 'Verzonden (naar klant)',
    ];

    /**
     * @var array
     */
    private const LABEL_CLASSES = [
        'draft' => 'grey-300',
        'archived' => 'slate-300',
        'cancelled' => 'slate-800',
        'complete' => 'success-800',
        'new' => 'orange-300',
        'on_hold' => 'warning-300',
        'payment_pending' => 'info-300',
        'pending' => 'orange-600',
        'processed' => 'success-300',
        'sent' => 'success-600',
    ];

    /**
     * @param Order $order
     * @return mixed|null
     * @throws OrderStatusNotFoundException
     */
    public static function getDescription(Order $order): string
    {
        $status = $order->getStatus();

        if (!\array_key_exists($status, self::DESCRIPTIONS)) {
            throw new OrderStatusNotFoundException(
                sprintf('Order status with the key "%s" was not found', $status)
            );
        }

        return self::DESCRIPTIONS[$status];
    }

    /**
     * @param string $status
     * @return mixed|null
     * @throws OrderStatusNotFoundException
     */
    public static function getDescriptionByKey(string $status): string
    {
        if (!\array_key_exists($status, self::DESCRIPTIONS)) {
            throw new OrderStatusNotFoundException(
                sprintf('Order status with the key "%s" was not found', $status)
            );
        }

        return self::DESCRIPTIONS[$status];
    }

    /**
     * @param string $status
     * @return mixed|null
     * @throws OrderStatusNotFoundException
     */
    public static function getLabelClass(string $status): string
    {
        if (!\array_key_exists($status, self::LABEL_CLASSES)) {
            throw new OrderStatusNotFoundException(
                sprintf('Order status with the key "%s" was not found', $status)
            );
        }

        return self::LABEL_CLASSES[$status];
    }

    /**
     * @return array
     */
    public static function getDescriptions(): array
    {
        return self::DESCRIPTIONS;
    }

    /**
     * @return array
     */
    public static function getLabelClasses(): array
    {
        return self::LABEL_CLASSES;
    }
}