<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Order\Order;
use function dump;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class InvalidLineCombinationValidator
 */
class InvalidLineCombinationValidator extends ConstraintValidator
{
    /**
     * Checks if the order contains products with invalid personalizations
     *
     * @param Order $order
     * @param Constraint|InvalidLineCombination $constraint The constraint for the validation
     *
     * @throws Exception
     */
    public function validate($order, Constraint $constraint)
    {
        foreach($order->getLines() as $orderLine) {
            $personalizationLine = $orderLine->getPersonalizationLine();
            if (null !== $personalizationLine) {
                $product = $orderLine->getProduct();

                if (!$product->isProductPersonalizationAvailable()) {
                    $this->context->buildViolation($constraint->invalidCombination)
                        ->addViolation();
                }

                try {
                    $personalization = $product->findProductProperty('personalization');
                } catch(Exception $e) {
                    $personalization = null;
                }

                if ((null !== $personalization && $personalization->getProductVariation() !== $personalizationLine->getProduct()) && !$product->getPersonalizations()->contains($personalizationLine->getProduct())) {
                    $this->context->buildViolation($constraint->invalidPersonalization)
                        ->addViolation();
                }
            }
        }
    }
}