<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ChamberOfCommerceValidator
 * @package AppBundle\Validator\Constraints
 *
 */
class ChamberOfCommerceValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $inValid = false;

        if (!empty($value)) {
            try {
                if (!$constraint->getWebservicesNL()->isValidChamberOfCommerceNumber($value)) {
                    $inValid = true;
                }
            } catch (\Exception $e) {
                $inValid = true;
            }

            if ($inValid) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}