<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Services\Webservices\VatInformationExchangeSystem;
use Symfony\Component\Validator\Constraint;

/**
 * Class VatNumber
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class VatNumber extends Constraint
{
    public $message = 'vat_number.not_valid';

    private $webservice_vat;

    /**
     *
     * @param VatInformationExchangeSystem $webservice_vat
     * @param null                         $options
     */
    public function __construct(VatInformationExchangeSystem $webservice_vat, $options = null)
    {
        $this->webservice_vat = $webservice_vat;

        parent::__construct($options);
    }

    /**
     * @return VatInformationExchangeSystem
     */
    public function getWebserviceVat(): VatInformationExchangeSystem
    {
        return $this->webservice_vat;
    }

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return 'vat_number';
    }
}
