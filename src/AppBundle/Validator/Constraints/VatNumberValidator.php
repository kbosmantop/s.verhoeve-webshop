<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class VatNumberValidator
 * @package AppBundle\Validator\Constraints
 *
 */
class VatNumberValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $inValid = false;

        if (!empty($value)) {

            try {
                if (!$constraint->getWebserviceVat()->isValidVatNumber($value)) {
                    $inValid = true;
                }
            } catch (\Exception $e) {
                $inValid = true;
            }

            if ($inValid) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}