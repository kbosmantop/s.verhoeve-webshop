<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Relation\CompanyDomain;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CustomerEmailValidator
 * @package AppBundle\Validator\Constraints
 *
 */
class CustomerEmailValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var CustomerEmail $constraint */

        $inValid = false;

        if (!empty($value)) {
            try {
                [$username, $domain] = explode('@', $value);

                void($username);

                $domains = $constraint->getEntityManager()->getRepository(CompanyDomain::class)->findBy([
                    'domain' => $domain,
                    'allowRegistration' => false,
                ]);

                if ($domains) {
                    $inValid = true;
                }
            } catch (\Exception $e) {
                $inValid = true;
            }

            if ($inValid) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}
