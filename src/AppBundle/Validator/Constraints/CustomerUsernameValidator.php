<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CustomerUsernameValidator
 * @package AppBundle\Validator\Constraints
 *
 */
class CustomerUsernameValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var CustomerUsername $constraint */

        $inValid = false;

        if (!empty($value)) {
            try {
                $checkUserExistance = $constraint->getEntityManager()->getRepository(Customer::class)->findOneBy([
                    'username' => $value,
                ]);

                if ($checkUserExistance) {
                    $inValid = true;
                }
            } catch (\Exception $e) {
                $inValid = true;
            }

            if ($inValid) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}
