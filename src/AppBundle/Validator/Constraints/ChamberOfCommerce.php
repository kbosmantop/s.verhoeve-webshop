<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Services\Webservices\WebservicesNL;
use Symfony\Component\Validator\Constraint;

/**
 * Class ChamberOfCommerce
 * @package AppBundle\Validator\Constraints
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class ChamberOfCommerce extends Constraint
{
    public $message = 'chamber_of_commerce.not_valid';

    private $webservices_nl;

    /**
     *
     * @param WebservicesNL $webservices_nl
     */
    public function __construct(WebservicesNL $webservices_nl)
    {
        parent::__construct();

        $this->webservices_nl = $webservices_nl;
    }

    /**
     * @return WebservicesNL
     */
    public function getWebservicesNL(): WebservicesNL
    {
        return $this->webservices_nl;
    }

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return 'chamber_of_commerce';
    }
}