<?php

namespace AppBundle\Interfaces\Seo;

interface SlugInterface
{
    /**
     * @param $slug
     * @return mixed
     */
    public function setSlug($slug);

    /**
     * @return mixed
     */
    public function getSlug();
}