<?php

namespace AppBundle\Interfaces\DeliveryArea;

use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Services\DeliveryArea\DeliveryAreaService;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Interface DatasetInterface
 * @package AppBundle\Interfaces\DeliveryArea
 */
interface DatasetInterface
{
    /**
     * DatasetInterface constructor.
     *
     * @param DeliveryAreaService $deliveryAreaService
     * @param EntityManagerInterface       $em
     */
    public function __construct(DeliveryAreaService $deliveryAreaService, EntityManagerInterface $em);

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getEvent();

    /**
     * @param \stdClass|null $filter
     *
     * @return DeliveryArea[]
     */
    public function getDeliveryAreas(\stdClass $filter = null);

    /**
     * @return array
     */
    public function getLegend();

    /**
     * @return array
     */
    public function getSuppliers();

    /**
     * @return array
     */
    public function getSupplierGroups();

    /**
     * @return array
     */
    public function getSupplierConnectors();

    /**
     * @return array
     */
    public function getOrder();
}
