<?php

namespace AppBundle\Interfaces\Order;

use AppBundle\Entity\Finance\Tax\VatRate;

/**
 * Interface VatLineInterface
 */
interface VatLineInterface
{
    /**
     * @return VatRate
     */
    public function getVatRate(): VatRate;

    /**
     * @return float
     */
    public function getAmount(): float;

    /**
     * @return LineInterface
     */
    public function getLine(): LineInterface;
}