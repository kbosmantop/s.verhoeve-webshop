<?php

namespace AppBundle\Interfaces\Order;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use Doctrine\Common\Collections\Collection;

/**
 * Interface LineInterface
 * @package AppBundle\Interfaces\Order
 */
interface LineInterface
{
    /**
     * @return string
     */
    public function getUuid();

    /**
     * @return Product
     */
    public function getProduct();

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param $quantity
     * @return $this
     */
    public function setQuantity($quantity);

    /**
     * @return float
     */
    public function getPrice();

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * @return array
     */
    public function getMetadata();

    /**
     * @param array $metadata
     * @return $this
     */
    public function setMetadata(array $metadata);

    /**
     * @param float $discountAmount
     * @return $this
     */
    public function setDiscountAmount($discountAmount);

    /**
     * @return float
     */
    public function getDiscountAmount();
    /**
     * @return float
     */
    public function getTotalPrice();
}