<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ConnectorInterface
 * @package AppBundle\Interfaces
 */
interface ConnectorInterface
{

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function process(SupplierOrder $supplierOrder);

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function cancel(SupplierOrder $supplierOrder);

    /**
     * @return bool
     */
    public function getCommissionable();

    /**
     * @return boolean
     */
    public function requireForwardingPrices();

    /**
     * @return boolean
     */
    public function alwaysShowForwardingPrices();

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form);

    /**
     * @param Company $company
     * @return Response|null
     */
    public function login(Company $company);

    /**
     * @return FormBuilderInterface|null
     */
    public function getConnectorParametersFormBuilder();
}