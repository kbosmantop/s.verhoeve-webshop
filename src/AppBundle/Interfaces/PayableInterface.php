<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Site\Site;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface PayableInterface
 * @package AppBundle\Interfaces
 */
interface PayableInterface
{
    /**
     * @return float
     */
    public function getPaymentAmount();

    /**
     * @return string
     */
    public function getPaymentDescription();

    /**
     * @return PaymentStatus
     */
    public function getPaymentStatus();

    /**
     * @return Site
     */
    public function getSite();

    /**
     * @return Customer
     */
    public function getCustomer();

    /**
     * @return ArrayCollection|Payment[]
     */
    public function getPayments();

    /**
     * @return Country
     */
    public function getInvoiceAddressCountry();
}
