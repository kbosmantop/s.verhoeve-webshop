<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Payment\Payment;
use Symfony\Component\HttpFoundation\Response;

interface GatewayInterface
{
    /**
     * @param Payment $payment
     * @return bool|Response
     */
    public function processPayment(Payment $payment);
}