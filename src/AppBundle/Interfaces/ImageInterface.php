<?php

namespace AppBundle\Interfaces;

/**
 * Interface ImageInterface
 * @package AppBundle\Interfaces
 */
interface ImageInterface extends FileInterface
{
    /**
     * @return string
     */
    public function generateName();
}