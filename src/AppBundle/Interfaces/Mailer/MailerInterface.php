<?php

namespace AppBundle\Interfaces\Mailer;

/**
 * Interface MailerInterface
 */
interface MailerInterface
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     */
    public function send(array $parameters, callable $callback = null): bool;

    /**
     * @param array $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array;
}