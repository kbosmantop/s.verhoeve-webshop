<?php

namespace AppBundle\Interfaces\PageView;

/**
 * Interface PageViewControllerInterface
 * @package AppBundle\Interfaces\PageView
 */
interface PageViewControllerInterface
{
    /**
     * @return array
     */
    public function getPageviewSettings();
}
