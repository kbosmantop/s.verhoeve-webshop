<?php

namespace AppBundle\Interfaces\Preview;

use AppBundle\Entity\Site\Site;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

interface PreviewableControllerInterface
{
    /**
     * Method for retreiving the possible sites which this item can be previewed on
     *
     * @Route("/preview/{entity}/sites")
     * @ParamConverter("entity", class="[INSERT_ENTITY_CLASS_HERE]")
     * @Method("GET")
     *
     * @param PreviewableEntityInterface $entity
     * @param Request                    $request
     *
     * @return JsonResponse
     */
    public function previewableSitesAction(PreviewableEntityInterface $entity, Request $request);

    /**
     * Redirect to the proper url of the item that needs to be previewed
     *
     * @Route("/preview/{entity}/{site}", defaults={"site" = null})
     * @ParamConverter("entity", class="[INSERT_ENTITY_CLASS_HERE]")
     * @Method("GET")
     *
     * @param PreviewableEntityInterface $entity
     * @param Site|null                  $site
     * @param Request                    $request
     *
     * @return RedirectResponse
     */
    public function previewAction(PreviewableEntityInterface $entity, Site $site = null, Request $request);
}
