<?php

namespace AppBundle\Interfaces\Commission;

use AppBundle\Entity\Finance\CommissionInvoice;
use Symfony\Component\HttpFoundation\Response;

interface InvoiceCalculatorInterface
{
    /**
     * @param int $version
     */
    public function setVersion($version);

    /**
     * @param CommissionInvoice $commissionInvoice
     */
    public function setCommissionInvoice(CommissionInvoice $commissionInvoice);

    /**
     * @return Response
     */
    public function renderView();
}