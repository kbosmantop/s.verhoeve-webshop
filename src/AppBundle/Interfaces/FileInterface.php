<?php

namespace AppBundle\Interfaces;

/**
 * Interface FileInterface
 * @package AppBundle\Interfaces
 */
interface FileInterface
{
    /**
     * @return string
     */
    public function getPath();

    /**
     * @param $path
     *
     * @return FileInterface
     */
    public function setPath($path);

    /**
     * @return string
     */
    public function getPathColumn();

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getStoragePath();
}