<?php

namespace AppBundle\Interfaces\Sales;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Order\LineInterface;
use Doctrine\Common\Collections\Collection;
use RuleBundle\Annotation as Rule;

/**
 * Interface OrderLineInterface
 * @package AppBundle\Interfaces\Sales
 * @Rule\EntityInterface(description="(Concept-)orderregels")
 */
interface OrderLineInterface extends LineInterface
{
    /**
     * @return Product
     * @Rule\Method(relatedEntity="AppBundle\Entity\Catalog\Product\Product", relation="child", autoComplete={"id"="product.id", "label"="product.translate.title"}, description="Product")
     */
    public function getProduct();

    /**
     * @return float
     * @Rule\Method(description="Kortingsbedrag")
     */
    public function getDiscountAmount();

    /**
     * @return float
     * @Rule\Method(description="Aantal")
     */
    public function getQuantity();

    /**
     * @return float
     * @Rule\Method(description="Totaal bedrag (Excl. BTW)")
     */
    public function getPrice();

    /**
     * @return OrderInterface
     * @Rule\Method(relatedEntity="AppBundle\Interfaces\Sales\OrderInterface", relation="parent", autoComplete={"id"="order.id", "label"="order.number"}, description="(Concept-)order")
     */
    public function getOrder();

    /**
     * @return array
     */
    public function getMetadata();

    /**
     * @param array $metadata
     */
    public function setMetadata(array $metadata);

    /**
     * @return OrderLineInterface|null
     */
    public function getParent();

    /**
     * @return OrderLineInterface[]|Collection
     */
    public function getChildren();
}