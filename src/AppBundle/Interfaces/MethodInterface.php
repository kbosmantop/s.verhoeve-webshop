<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Method\AbstractMethod;

interface MethodInterface
{
    /**
     * @return string
     */
    public function getCode();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getFormClass();

    /**
     * @var GatewayInterface
     */
    public function getGateway();

    /**
     * @return Paymentmethod|AbstractMethod
     */
    public function getEntity();
}