<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\MenuRepositoryInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductRepository
 * @package AppBundle\Repository
 */
class ProductRepository extends EntityRepository implements MenuRepositoryInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param mixed $id
     * @param null  $lockMode
     * @param null  $lockVersion
     * @return Product||CompanyProductDecorator|null
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        /** @var Product $product */
        $product = parent::find($id, $lockMode, $lockVersion);

        if (null === $product) {
            return null;
        }

        //disable admin from decorating products
        $request = $this->container->get('request_stack')->getMasterRequest();

        if(null !== $request && false !== strpos($request->getRequestUri(), '/admin')) {
            return $product;
        }

        $product = $this->container->get(ProductManager::class)->decorateProduct($product);

        return $product;
    }

    /**
     * @param      $slug
     * @param null $locale
     * @return Product
     * @throws NonUniqueResultException
     */
    public function getBySlug($slug, $locale = null)
    {
        if (!$locale) {
            $locale = 'nl_NL';
        }

        $qb = $this->getEntityManager()->getRepository(Product::class)->createQueryBuilder('p');
        $qb->leftJoin('p.translations', 'pt');

        $qb->andWhere('pt.slug = :slug');
        $qb->andWhere('pt.locale = :locale');

        $qb->setParameters([
            'slug' => $slug,
            'locale' => $locale,
        ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Site $site
     * @param      $keyword
     * @return ArrayCollection|Product[]
     */
    public function searchMenu(Site $site, $keyword)
    {

        $qb = $this->getEntityManager()->getRepository(Product::class)->createQueryBuilder('p');
        $qb
            ->select('p')
            ->leftJoin('p.translations', 'pt', 'WITH', 'p.id = pt.translatable AND pt.locale = :locale')
            ->leftJoin('p.assortmentProducts', 'pap')
            ->leftJoin('pap.assortment', 'a')
            ->leftJoin('a.sites', 's')
            ->andWhere('p.parent IS NULL')
            ->andWhere('s.id = :site')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('pt.title', ':search'),
                $qb->expr()->like('pt.shortDescription', ':search'),
                $qb->expr()->like('pt.description', ':search')
            ))
            ->setParameters([
                'site' => $site,
                'locale' => 'nl_NL',
                'search' => '%' . $keyword . '%',
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Company $company
     * @param         $keyword
     * @return array
     */
    public function searchMenuByCompany(Company $company, $keyword)
    {
        $qb = $this->getEntityManager()->getRepository(Product::class)->createQueryBuilder('p');
        $qb
            ->select('p')
            ->leftJoin('p.translations', 'pt', 'WITH', 'p.id = pt.translatable AND pt.locale = :locale')
            ->leftJoin('p.assortmentProducts', 'pap')
            ->leftJoin('pap.assortment', 'a')
            ->leftJoin('a.companies', 'c')
            ->andWhere('p.parent IS NULL')
            ->andWhere('c.id = :company')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('pt.title', ':search'),
                $qb->expr()->like('pt.shortDescription', ':search'),
                $qb->expr()->like('pt.description', ':search')
            ))
            ->setParameters([
                'company' => $company,
                'locale' => 'nl_NL',
                'search' => '%' . $keyword . '%',
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Assortment $assortment
     * @return Product[]
     */
    public function getProductsByAssortment(Assortment $assortment): array
    {
        $qb = $this->getEntityManager()->getRepository(Product::class)->createQueryBuilder('product');
        $qb->leftJoin('product.assortmentProducts', 'assortment_product');
        $qb->leftJoin('assortment_product.assortment', 'assortment');

        $qb->leftJoin('product.translations', 'translation');
        $qb->addSelect('translation');
        $qb->andWhere('translation.locale = :locale');

        $qb->leftJoin('product.productImages', 'product_image');
        $qb->addSelect('product_image');

        $qb->leftJoin('product.combinations', 'product_combination');
        $qb->addSelect('product_combination');

        $qb->andWhere('assortment = :assortment');
        $qb->setParameter('assortment', $assortment);

        $qb->leftJoin('product.variations', 'variation');
        $qb->addSelect('variation');

        $qb->leftJoin('variation.translations', 'variation_translation');
        $qb->addSelect('variation_translation');
        $qb->andWhere('variation_translation.locale = :locale');

        $qb->setParameter('locale', $this->container->get('translator')->getLocale());

        $qb->orderBy('assortment_product.position', 'ASC');

        $qb->andWhere('product.deletedAt IS NULL');
        $qb->andWhere('assortment_product.deletedAt IS NULL');
        $qb->andWhere('assortment.deletedAt IS NULL');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $supplierGroupIds
     * @param $searchTerm
     * @param $existingProducts
     * @return array
     */
    public function getSupplierGroupProducts($supplierGroupIds, $searchTerm, $existingProducts): array
    {
        $qb = $this->createQueryBuilder('product');
        $qb->leftJoin('product.variations', 'variation');
        $qb->leftJoin('variation.supplierGroupProducts', 'supplier_group_product');
        $qb->andWhere('supplier_group_product.supplierGroup IN (:supplierGroups)');
        $qb->setParameter('supplierGroups', $supplierGroupIds);

        $qb = $this->filterCombinationSelectionResults($qb, $searchTerm, $existingProducts);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $supplierIds
     * @param $searchTerm
     * @param $existingProducts
     * @return array
     */
    public function getSupplierProducts($supplierIds, $searchTerm, $existingProducts): array
    {
        $qb = $this->createQueryBuilder('product');
        $qb->leftJoin('product.variations', 'variation');
        $qb->leftJoin('variation.supplierProducts', 'supplier_product');
        $qb->andWhere('supplier_product.supplier IN (:suppliers)');
        $qb->setParameter('suppliers', $supplierIds);

        $qb = $this->filterCombinationSelectionResults($qb, $searchTerm, $existingProducts);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param string       $searchTerm
     * @param array        $existingProducts
     * @return QueryBuilder
     */
    public function filterCombinationSelectionResults(
        QueryBuilder $qb,
        string $searchTerm,
        array $existingProducts
    ): QueryBuilder
    {
        $result = $qb->leftJoin(
            ProductTranslation::class,
            'pt',
            'WITH',
            'product.id = pt.translatable AND pt.locale = :locale'
        );
        $qb->setParameter('locale', 'nl_NL');

        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->like('pt.title', ':search'),
            $qb->expr()->like('pt.shortDescription', ':search'),
            $qb->expr()->like('pt.description', ':search')
        ));
        $qb->setParameter('search', '%' . $searchTerm . '%');

        $qb->andWhere('product NOT IN (:existingProducts) AND product.parent IS NULL');
        $qb->setParameter('existingProducts', array_values($existingProducts));

        return $result;
    }

    /**
     * @param int $siteId
     *
     * @param array $supplierIds
     *
     * @param null $query
     *
     * @param bool $groupedByParent
     *
     * @return QueryBuilder
     */
    public function queryVariationsBySiteAndSuppliers(int $siteId, ?array $supplierIds = null, $query = null)
    {
        $qb = $this->getEntityManager()->getRepository(GenericProduct::class)->createQueryBuilder('product');
        $qb->select('product.id id, product.name, pp.id parent_id, pp.name as parent_name');
        $qb->leftJoin('product.parent', 'pp');

        $qb->andWhere('product.deletedAt IS NULL');
        $qb->andWhere('product.publish != 0');
        $qb->andWhere('pp.deletedAt IS NULL');
        $qb->andWhere('pp.publish != 0');

        if (null !== $supplierIds) {
            $qb->leftJoin('product.supplierGroupProducts', 'supplier_group_products');
            $qb->leftJoin('product.supplierProducts', 'supplier_products');
            $qb->leftJoin('supplier_group_products.supplierGroup', 'supplier_group');
            $qb->leftJoin('supplier_group.suppliers', 'supplier');

            $orX = $qb->expr()->orX(
                $qb->expr()->in('supplier.id', ':supplierIds'),
                $qb->expr()->in('supplier_products.supplier', ':supplierIds')
            );
            $qb->andWhere($orX);
            $qb->setParameter('supplierIds', $supplierIds);
        }

        if ($query) {
            $qb->andWhere('product.name LIKE :name');
            $qb->setParameter('name', '%' . $query . '%');
        }

        $qb->addGroupBy('product.id');

        return $qb;
    }

    /**
     * @param int $siteId
     *
     * @return mixed
     */
    public function findBySite(int $siteId)
    {
        return $this->queryBySite($siteId)->getQuery()->getResult();
    }
}
