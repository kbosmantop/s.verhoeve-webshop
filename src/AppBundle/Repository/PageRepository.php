<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\MenuRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class PageRepository
 * @package AppBundle\Repository
 */
class PageRepository extends EntityRepository implements MenuRepositoryInterface
{
    /**
     * @param      $slug
     * @param null $locale
     * @return Page
     * @throws NonUniqueResultException
     */
    public function getBySlug($slug, $locale = null)
    {
        if (!$locale) {
            $locale = 'nl_NL';
        }

        $qb = $this->getEntityManager()->getRepository(Page::class)->createQueryBuilder('p');
        $qb->leftJoin('p.translations', 'pt');

        $qb->andWhere('pt.slug = :slug');
        $qb->andWhere('pt.locale = :locale');

        $qb->setParameters([
            'slug' => $slug,
            'locale' => $locale,
        ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     *
     * @param Site $site
     * @param      $keyword
     *
     * @return ArrayCollection|Page[]
     */
    public function searchMenu(Site $site, $keyword)
    {
        $qb = $this->getEntityManager()->getRepository(Page::class)->createQueryBuilder('p');
        $qb
            ->select('p')
            ->leftJoin('p.translations', 'pt', 'WITH', 'p.id = pt.translatable AND pt.locale = :locale')
            ->andWhere('p.site = :site')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('pt.title', ':search'),
                $qb->expr()->like('pt.content', ':search')
            ))
            ->setParameters([
                'site' => $site,
                'locale' => 'nl_NL',
                'search' => '%' . $keyword . '%',
            ]);

        return $qb->getQuery()->getResult();
    }
}
