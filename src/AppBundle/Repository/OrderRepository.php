<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Site\Site;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class OrderRepository
 * @package AppBundle\Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param string $invoiceReference
     * @return null|OrderCollection|object
     */
    public function findOneByInvoiceReference(string $invoiceReference)
    {
        return $this->findOneBy([
            'invoiceReference' => $invoiceReference,
        ]);
    }

    /**
     * @param string $orderNumber
     * @return null|OrderCollection|object
     */
    public function findOneByOrderNumber(string $orderNumber)
    {
        if (!preg_match('/^\\d{8}-\\d{4}$/', $orderNumber)) {
            return null;
        }

        $qb = $this->getEntityManager()->getRepository(Order::class)->createQueryBuilder('o')
            ->select('o')
            ->andWhere('o.orderCollection = :orderCollectionId')
            ->andWhere('o.number = :orderNumber')
            ->setParameters([
                'orderCollectionId' => (int)substr($orderNumber, 0, 8) - 10000000,
                'orderNumber' => (int)substr($orderNumber, 9, 4),
            ]);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            // Shall never happen, but catching it to prevent a @throws NonUniqueResultException in the DocBlock
            return null;
        }
    }

    /**
     * @param string $type
     * @return OrderCollection[]
     */
    public function findExpiredOrders(string $type)
    {
        $tomorrow = date('Y-m-d', strtotime('+1 day'));

        if (date('N', strtotime($tomorrow)) === '7') {
            $tomorrow = date('Y-m-d', strtotime('+2 day'));
        }

        $qb = $this->getEntityManager()->getRepository(Order::class)->createQueryBuilder('o');
        $qb->leftJoin('o.supplier', 's');
        $qb->leftJoin('o.orderCollection', 'oc')->addSelect('oc');
        $qb->andWhere('o.deliveryDate <= :tomorrow');
        $qb->andWhere('o.deliveryDate >= :past');
        $qb->andWhere('o.supplier is not null');
        $qb->andWhere('s.supplierConnector = :bakery or s.supplierConnector = :bakeryMail');

        if ($type === 'unprocessed') {
            $qb->andWhere('o.status = :status');
        } else {
            $qb->andWhere('o.deliveryStatus is null or o.deliveryStatus = :status');
        }

        $qb->addOrderBy('o.deliveryDate', 'ASC');

        // Query pre-fetching optimalization
        $qb->leftJoin('o.autoOrder', 'auto_order')->addSelect('auto_order');
        $qb->leftJoin('o.cartOrder', 'cart_order')->addSelect('cart_order');
        $qb->leftJoin('oc.cart', 'cart')->addSelect('cart');
        $qb->leftJoin('oc.review', 'review')->addSelect('review');

        $qb->setParameters([
            'tomorrow' => $tomorrow,
            'past' => date('Y-m-d', strtotime('-7 day')),
            'bakery' => 'Bakker',
            'bakeryMail' => 'BakkerMail',
            'status' => $type === 'unprocessed' ? 'pending' : 'unknown',
        ]);

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $siteId
     * @param int $limit
     * @return Order[]
     */
    public function findRecentOrders($siteId = null, $limit = 5)
    {
        $site = null;
        if ($siteId) {
            $site = $this->getEntityManager()->getRepository(Site::class)->find($siteId);
        }

        $startToday = date('Y-m-d H:i:s', strtotime('midnight'));
        $endToday = date('Y-m-d H:i:s', strtotime('tomorrow') - 1);

        $qb = $this->getEntityManager()->getRepository(Order::class)->createQueryBuilder('o');
        $qb->leftJoin('o.orderCollection', 'oc')->addSelect('oc');
        $qb->andWhere('o.createdAt >= :startToday');
        $qb->andWhere('o.createdAt <= :endToday');
        $qb->andWhere("o.status != :paymentStatus AND o.status != 'cancelled'");
        $qb->addOrderBy('o.createdAt', 'DESC');

        // Query pre-fetching optimalization
        $qb->leftJoin('o.autoOrder', 'auto_order')->addSelect('auto_order');
        $qb->leftJoin('o.cartOrder', 'cart_order')->addSelect('cart_order');
        $qb->leftJoin('oc.cart', 'cart')->addSelect('cart');
        $qb->leftJoin('oc.review', 'review')->addSelect('review');

        $parameters = [
            'startToday' => $startToday,
            'endToday' => $endToday,
            'paymentStatus' => 'payment_pending',
        ];

        if ($site) {
            $qb->andWhere('oc.site = :site');
            $parameters['site'] = $site;
        }

        $qb->setParameters($parameters);

        $orders = $qb->getQuery()->getResult();
        $recentOrders = \array_slice($orders, 0, $limit);

        return ['count' => \count($orders), 'recent' => $recentOrders];
    }

    /**
     * @param int  $requestid
     * @param string  $groupby
     * @return array
     *
     * getRevenueToday2
     * getRevenueByCategory
     */
    public function getRevenueToday($requestid = null, $groupby = 'site') : array
    {
        $qb = $this->getEntityManager()->getConnection()->createQueryBuilder();

        if($groupby === 'site'){
            $qb->select('SUM(o.total_price_incl) as price, COUNT(*) as `count`, MAX(o.total_price_incl) as `maxprice`, oc.site_id as id, DATE(o.created_at) as date')
                ->from('`order`', 'o')
                ->leftJoin('o', 'order_collection', 'oc', 'o.order_collection_id = oc.id')
                ->andWhere('o.status <> \'payment_pending\'')
                ->andWhere('o.status <> \'cancelled\'')
                ->andWhere('o.created_at > NOW() - INTERVAL 1 WEEK');

            if ($requestid) {
                $qb->andWhere('oc.site_id = :requestid');
                $qb->setParameter('requestid', $requestid);
            }

            $qb->groupBy('DATE(o.created_at), oc.site_id');

        } elseif($groupby === 'productgroup') {
            $qb->select('SUM((ol.price * ol.quantity) - IFNULL(ol.discount_amount,0)) as price, COUNT(*) as `count`, MAX((ol.price * ol.quantity) - IFNULL(ol.discount_amount,0)) as maxprice, DATE(o.created_at) as date')
                ->from('order_line', 'ol')
                ->leftJoin('ol', '`order`', 'o', 'ol.order_id = o.id')
                ->leftJoin('ol', 'product', 'p1', 'ol.product_id = p1.id')
                ->leftJoin('p1', 'product', 'p2', 'p1.parent_id = p2.id')
                ->leftJoin('p1', 'productgroup', 'pg', 'IFNULL(p1.productgroup_id, p2.productgroup_id) = pg.id')
                ->where('o.created_at > NOW() - INTERVAL 1 WEEK');

            if ($requestid) {
                $qb->andWhere('pg.id = :requestid');
                $qb->setParameter('requestid', $requestid);
            }

            $qb->groupBy('DATE(o.created_at), pg.id');
        }
        $orders = $qb->execute()->fetchAll();

        $revenuePerDayArray = [];

        for ($i = 7; $i >= 0; $i--) {
            $revenuePerDayArray[date('d-m-Y', strtotime('-' . $i . ' days'))] = [
                'revenue' => 0,
                'count' => 0,
            ];
        }

        $total = 0;
        $max = 0;
        $ordersToday = 0;

        foreach ($orders as $order) {
            $orderdate = date('d-m-Y', strtotime( $order['date'] ));

            //check if order is created today
            if ($order['date'] == date('Y-m-d')) {
                $total += $order['price'];
                $max = $order['maxprice'] > $max ? $order['maxprice'] : $max;
                $ordersToday += $order['count'];
            }

            if (isset($revenuePerDayArray[$orderdate])) {
                $revenuePerDayArray[$orderdate]['revenue'] += $order['price'];
                $revenuePerDayArray[$orderdate]['count'] += $order['count'];
            } else {
                $revenuePerDayArray[$orderdate]['revenue'] = $order['price'];
                $revenuePerDayArray[$orderdate]['count'] = 1;
            }
        }

        $chartLabels = array_keys($revenuePerDayArray);

        foreach($revenuePerDayArray as $revenueDay){
            $chartValues[] = $revenueDay['revenue'];
            $tooltips[] = $revenueDay['count'];
        }

        return [
            'total' => round($total, 2),
            'max' => round($max, 2),
            'chartLabels' => $chartLabels,
            'chartValues' => $chartValues,
            'tooltips' => $tooltips,
            'count' => $ordersToday,
        ];
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return mixed
     */
    public function findFraudCheckableOrdersByCollection(OrderCollection $orderCollection)
    {
        return $this->getEntityManager()->getRepository(OrderCollection::class)->findFraudCheckableOrders($orderCollection);
    }
}
