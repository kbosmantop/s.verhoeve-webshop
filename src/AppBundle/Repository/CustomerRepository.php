<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CustomerRepository
 * @package AppBundle\Repository
 */
class CustomerRepository extends EntityRepository
{
    use ContainerAwareTrait;

    /**
     * find RecentCustomers
     *
     * @param bool $orderByLogin
     * @param int  $limit
     * @return mixed
     */
    public function findRecentCustomers($orderByLogin = false, $limit = 5)
    {
        $qb = $this->getEntityManager()->getRepository(Customer::class)->createQueryBuilder('c');
        $qb->leftJoin('c.company', 'co');
        $qb->andWhere('c.username is not null');
        $qb->addOrderBy($orderByLogin ? 'c.lastLogin' : 'c.createdAt', 'DESC');
        $qb->setMaxResults($limit);

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function findRecentActiveCustomers($limit = 5)
    {
        return $this->findRecentCustomers(true, $limit);
    }

    /**
     * @param null $username
     * @return null|Customer
     */
    public function findByUsername($username = null)
    {
        return $this->container->get('doctrine')->getRepository(Customer::class)->findOneBy([
            'username' => $username,
        ]);
    }

    /**
     * @param Company $company
     * @return array
     */
    public function getUsersByCompany(Company $company)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.company = :company')
            ->setParameter('company', $company->getId())
            ->getQuery()
            ->getResult();
    }

}
