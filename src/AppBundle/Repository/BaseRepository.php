<?php namespace AppBundle\Repository;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\ORMException;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class BaseRepository
 * @package AppBundle\Repository
 */
class BaseRepository extends EntityRepository
{
    /**
     * Find the first result or create a persisted instance
     *
     * @param array $criteria
     * @return object
     * @throws ORMException
     */
    public function findOneOrCreate(array $criteria = [])
    {
        $object = $this->findOneBy($criteria);

        if (null === $object) {
            $object = $this->createPersist($criteria);
        }

        return $object;
    }

    /**
     * Find the first result or create an unpersisted instance
     *
     * @param array $criteria
     * @return object
     * @throws ORMException
     */
    public function findOneOrNew(array $criteria = [])
    {
        $object = $this->findOneBy($criteria);

        if (null === $object) {
            $object = $this->new($criteria);
        }

        return $object;
    }

    /**
     * Creates and persists an object to the database
     *
     * @param array $criteria
     * @return object
     * @throws ORMException
     */
    public function create(array $criteria = [])
    {
        try {
            $className = $this->getClassName();
            $reflectionClass = new ReflectionClass($className);
            $annotationReader = new AnnotationReader();
            $entityAnnotation = $annotationReader->getClassAnnotation($reflectionClass, Entity::class);
            if($entityAnnotation !== null && $entityAnnotation->readOnly){
                throw new ORMException(sprintf('Can not create "%s" entity is readonly', $className));
            }
        } catch (AnnotationException $e) {
            //If annotations can not be read do not check readonly
        } catch (ReflectionException $e) {
            throw new ORMException($e);
        }
        return $this->createPersist($criteria);
    }

    /**
     * Creates and persists an object to the database
     *
     * @param array $criteria
     * @return object
     * @throws ORMException
     */
    protected function createPersist(array $criteria = [])
    {
        $object = $this->new($criteria);

        $this->getEntityManager()->persist($object);

        return $object;
    }

    /**
     * Create an unpersisted object
     *
     * @param array $criteria
     *
     * @return object
     * @throws ORMException
     * @throws ReflectionException
     */
    protected function new(array $criteria = [])
    {
        $className = $this->getClassName();
        try {
            $reflectionClass = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            throw new ORMException($e);
        }
        $objectArgs = [];

        if(($constructor = $reflectionClass->getConstructor()) !== null) {
            foreach ($constructor->getParameters() as $constructorParameter) {
                $keyExists = array_key_exists($constructorParameter->getName(), $criteria);
                if ($keyExists) {
                    $objectArgs[] = $criteria[$constructorParameter->getName()];
                    unset($criteria[$constructorParameter->getName()]);
                } elseif (!$keyExists && $constructorParameter->isOptional()) {
                    $objectArgs[] = $constructorParameter->getDefaultValue();
                } else {
                    throw new ORMException(sprintf('Can not create new "%s" parameter "%s" is required', $className,
                        $constructorParameter->getName()));
                }
            }
        }

        $object = $reflectionClass->newInstanceArgs($objectArgs);
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($criteria as $key => $value) {
            if (null === $value) {
                continue;
            }

            $accessor->setValue($object, $key, $value);
        }

        return $object;
    }
}
