<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @method SupplierGroupProductTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 * @Rule\Entity(description="Leveranciersgroep")
 */
class SupplierGroup
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Rule\Property(description="Naam")
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company", mappedBy="supplierGroups")
     * @Rule\Property(description="Leveranciers", autoComplete={"id"="supplier.id", "label"="supplier.name"},
     *                                            relation="parent")
     */
    protected $suppliers;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3, nullable=true)
     */
    protected $deliveryCost;

    /**
     * @deprecated
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Vat")
     * @Rule\Property(description="BTW percentage", autoComplete={"id"="vat.id", "label"="vat.description"},
     *                                 relation="child")
     */
    protected $deliveryVAT;

    /**
     * @ORM\Column(type="string")
     */
    protected $deliveryInterval;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierGroupCommission", mappedBy="supplierGroup",
     *                                                                         cascade={"persist"})
     */
    protected $supplierCommissions;

    /**
     * @var VatGroup[]|Collection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Finance\Tax\VatGroup")
     * @Rule\Property(description="BTW Groep", autoComplete={"id"="vatGroup.id", "label"="vatGroup.level.description"},
     *                                 relation="child")
     */
    protected $deliveryVatGroups;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierGroupProduct", mappedBy="supplierGroup")
     */
    protected $supplierGroupProducts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->suppliers = new ArrayCollection();
        $this->supplierCommissions = new ArrayCollection();
        $this->deliveryVatGroups = new ArrayCollection();
        $this->supplierGroupProducts = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * Get deliveryCost
     *
     * @return float
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

    /**
     * Set deliveryCost
     *
     * @param float $deliveryCost
     *
     * @return SupplierGroup
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SupplierGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add supplier
     *
     * @param Company $supplier
     *
     * @return SupplierGroup
     */
    public function addSupplier(Company $supplier)
    {
        $this->suppliers[] = $supplier;

        return $this;
    }

    /**
     * Remove supplier
     *
     * @param Company $supplier
     */
    public function removeSupplier(Company $supplier)
    {
        $this->suppliers->removeElement($supplier);
    }

    /**
     * Get suppliers
     *
     * @return Collection
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

    /**
     * Add supplier commission
     *
     * @param SupplierGroupCommission $commission
     *
     * @return SupplierGroup
     */
    public function addSupplierCommission(SupplierGroupCommission $commission)
    {
        $commission->setSupplierGroup($this);

        $this->supplierCommissions[] = $commission;

        return $this;
    }

    /**
     * Remove supplier commission
     *
     * @param SupplierGroupCommission $commission
     */
    public function removeSupplierCommission(SupplierGroupCommission $commission)
    {
        $this->supplierCommissions->removeElement($commission);
    }

    /**
     * Get supplier commissions
     *
     * @return Collection
     */
    public function getSupplierCommissions()
    {
        return $this->supplierCommissions;
    }

    /**
     * Set deliveryInterval
     *
     * @param string $deliveryInterval
     *
     * @return SupplierGroup
     */
    public function setDeliveryInterval($deliveryInterval)
    {
        $this->deliveryInterval = $deliveryInterval;

        return $this;
    }

    /**
     * Get deliveryInterval
     *
     * @return string
     */
    public function getDeliveryInterval()
    {
        return $this->deliveryInterval;
    }

    /**
     * Set deliveryVAT
     * @deprecated
     * @param Vat $deliveryVAT
     *
     * @return SupplierGroup
     */
    public function setDeliveryVAT(Vat $deliveryVAT = null)
    {
        $this->deliveryVAT = $deliveryVAT;

        return $this;
    }

    /**
     * Get deliveryVAT
     * @deprecated
     * @return Vat
     */
    public function getDeliveryVAT()
    {
        return $this->deliveryVAT;
    }

    /**
     * @return VatGroup[]|Collection
     */
    public function getDeliveryVatGroups(): Collection
    {
        return $this->deliveryVatGroups;
    }

    /**
     * @param VatGroup[]|Collection $vatGroups
     * @return SupplierGroup
     */
    public function setDeliveryVatGroups(Collection $vatGroups): SupplierGroup
    {
        $this->deliveryVatGroups = $vatGroups;

        return $this;
    }

    /**
     * @param VatGroup $vatGroup
     * @return SupplierGroup
     */
    public function addDeliveryVatGroup(VatGroup $vatGroup): SupplierGroup
    {
        if (!$this->deliveryVatGroups->contains($vatGroup)) {
            $this->deliveryVatGroups->add($vatGroup);
        }

        return $this;
    }

    /**
     * @param VatGroup $vatGroup
     * @return $this
     */
    public function removeDeliveryVatGroup(VatGroup $vatGroup): SupplierGroup
    {
        if ($this->deliveryVatGroups->contains($vatGroup)) {
            $this->deliveryVatGroups->removeElement($vatGroup);
        }

        return $this;
    }

    /**
     * @param Country $country
     * @return VatGroup|null
     */
    public function getDeliveryVatGroupForCountry(Country $country) {
        $groups = $this->deliveryVatGroups->filter(function(VatGroup $vatGroup) use($country) {
            return $vatGroup->getCountry()->getId() === $country->getId();
        });

        return !$groups->isEmpty() ? $groups->current() : null;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $executionContext, $payload)
    {
        $countries = [];
        /** @var VatGroup $vatGroup */
        foreach ($this->getDeliveryVatGroups() as $vatGroup) {
            if (\in_array($vatGroup->getCountry(), $countries, true)) {
                $executionContext->buildViolation('Er mogen niet meerdere BTW groepen uit hetzelfde land op een supplier group zitten.')
                    ->atPath('deliveryVatGroups')
                    ->addViolation();

                break;
            }

            $countries[] = $vatGroup->getCountry();
        }
    }

    /**
     * @return mixed
     */
    public function getSupplierGroupProducts()
    {
        return $this->supplierGroupProducts;
    }
}
