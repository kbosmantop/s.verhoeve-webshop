<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Relation\Company as Supplier;
use AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder;
use AppBundle\Entity\Order\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class SupplierOrder
 * @package AppBundle\Entity\Supplier
 *
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"status"})
 * })
 */
class SupplierOrder
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    protected $reference;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $status;

    /**
     * @var Supplier
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $supplier;

    /**
     * @var null|Order
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $order;

    /**
     * @var SupplierOrderLine[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierOrderLine", cascade={"persist"},
     *                                                                            mappedBy="supplierOrder")
     */
    protected $lines;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $commissionable;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $recommission = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $connector;

    /**
     * @var ArrayCollection|CommissionInvoiceSupplierOrder[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder", mappedBy="supplierOrder")
     */
    protected $commissionInvoiceSupplierOrders;

    /**
     * SupplierOrder constructor.
     */
    public function __construct()
    {
        $this->lines = new ArrayCollection();
        $this->status = 'pending';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return SupplierOrder
     */
    public function setStatus(string $status): SupplierOrder
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return SupplierOrder
     */
    public function setReference(string $reference): SupplierOrder
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return ArrayCollection|SupplierOrderLine[]
     */
    public function getLines(): Collection
    {
        return $this->lines;
    }

    /**
     * Add supplierOrderLine
     *
     * @param SupplierOrderLine $supplierOrderLine
     */
    public function addLine(SupplierOrderLine $supplierOrderLine)
    {
        $supplierOrderLine->setSupplierOrder($this);

        $this->lines[] = $supplierOrderLine;
    }

    /**
     * @return bool
     */
    public function isCommissionable(): bool
    {
        return $this->commissionable;
    }

    /**
     * @param bool $commissionable
     */
    public function setCommissionable(bool $commissionable): void
    {
        $this->commissionable = $commissionable;
    }

    /**
     * Get recommission
     *
     * @return boolean
     */
    public function getRecommission()
    {
        return $this->recommission;
    }

    /**
     * Set recommission
     *
     * @param boolean $recommission
     */
    public function setRecommission($recommission)
    {
        $this->recommission = $recommission;
    }

    /**
     * @return string
     */
    public function getConnector(): string
    {
        return $this->connector;
    }

    /**
     * @param string $connector
     */
    public function setConnector(string $connector): void
    {
        $this->connector = $connector;
    }

    /**
     * @return ArrayCollection|CommissionInvoiceSupplierOrder[]
     */
    public function getCommissionInvoiceSupplierOrders(): Collection
    {
        return $this->commissionInvoiceSupplierOrders;
    }

    /**
     * @param CommissionInvoiceSupplierOrder[] $commissionInvoiceSupplierOrders
     */
    public function setCommissionInvoiceSupplierOrders(array $commissionInvoiceSupplierOrders): void
    {
        $this->commissionInvoiceSupplierOrders = $commissionInvoiceSupplierOrders;
    }

    /**
     * @return SupplierOrderLine[]
     */
    public function getRootLines(): array
    {
        $lines = [];

        foreach ($this->getLines() as $line) {
            if ($line->getOrderLine()->hasParent()) {
                continue;
            }

            $lines[] = $line;
        }

        return $lines;
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return SupplierOrderLine[]
     */
    public function getChildLines(SupplierOrderLine $supplierOrderLine): array
    {
        $lines = [];

        foreach ($this->getLines() as $line) {
            $orderLine = $line->getOrderLine();
            if (!$orderLine->hasParent() || $orderLine->getParent() !== $supplierOrderLine->getOrderLine()) {
                continue;
            }

            // Assign childs only to main combination line.
            if(null !== $orderLine->getParent()) {
                $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct()->getProduct();
                $orderLineProduct = $orderLine->getParent()->getProduct();
                if(null !== $orderLineProduct->getMainCombination()) {
                    if($orderLineProduct->getMainCombination()->getProduct()->getId() !== $supplierProduct->getId()) {
                        continue;
                    }
                }
            }

            $lines[] = $line;
        }

        return $lines;
    }
}