<?php

namespace AppBundle\Entity\Security\Customer;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;


/**
 * @ORM\Entity
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class CustomerGroup
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Customer\Customer", mappedBy="customerGroup", cascade={"persist"})
     * @Rule\Property(description="Klanten", autoComplete={"id"="customer.id", "label"="customer.fullname"}, relation="parent")
     */
    protected $customers;

    /**
     * @ORM\Column(type="text", length=100)
     *
     */
    protected $description;

    /**
     * @ORM\Column(type="text", length=100, nullable=true)
     */
    protected $teamleaderTag;

    public function __toString()
    {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CustomerGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customers = new ArrayCollection();
//        $this->companies = new ArrayCollection();
    }

    /**
     * Add customer
     *
     * @param Customer $customer
     *
     * @return CustomerGroup
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param Customer $customer
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Set teamleaderTag
     *
     * @param string $teamleaderTag
     *
     * @return CustomerGroup
     */
    public function setTeamleaderTag($teamleaderTag)
    {
        $this->teamleaderTag = $teamleaderTag;

        return $this;
    }

    /**
     * Get teamleaderTag
     *
     * @return string
     */
    public function getTeamleaderTag()
    {
        return $this->teamleaderTag;
    }

    /**
     * Add company
     *
     * @param Company $company
     *
     * @return $this
     */
    public function addCompany(Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }
}
