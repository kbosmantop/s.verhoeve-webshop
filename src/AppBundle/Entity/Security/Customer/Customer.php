<?php

namespace AppBundle\Entity\Security\Customer;

use AppBundle\DBAL\Types\GenderType;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AppBundle\Model\Security\Customer\CustomerModel;
use AppBundle\Traits\NonDeletableEntity;
use AppBundle\Traits\NonEditableEntity;
use AppBundle\Traits\UuidEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use RuleBundle\Annotation as Rule;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @ORM\Table(name="`customer`")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="username", message="username.not_unique")
 * @ORM\EntityListeners({"AppBundle\EventListener\Teamleader\CustomerListener"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Rule\Entity(description="Contactpersoon")
 */
class Customer extends CustomerModel implements UserInterface, AuditEntityInterface
{
    public const ADDRESS_TYPE_MAIN = 'main';
    public const ADDRESS_TYPE_INVOICE = 'invoice';
    public const ADDRESS_TYPE_DELIVERY = 'delivery';

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use NonEditableEntity;
    use NonDeletableEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\GenderType")
     * @ORM\Column(type="GenderType", nullable=true)
     * @Assert\NotBlank(message = "customer.gender.not_blank", groups={"customer_registration", "company_registration",
     *                          "cart_sender", "Default"})
     *
     */
    protected $gender;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    private $fullname;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\NotBlank(message = "customer.firstname.not_blank", groups={"company_registration", "cart_sender",
     *                          "Default"})
     *
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     */
    private $lastnamePrefix;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\NotBlank(message = "customer.lastname.not_blank", groups={"company_registration", "cart_sender",
     *                          "Default"})
     *
     */
    private $lastname;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     */
    private $birthday;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="customers")
     *
     * @Assert\NotBlank(
     *     message="Bedrijfs id kan niet leeg zijn",
     *     groups={"uploadUsers"}
     * )
     * @Rule\Property(description="Bedrijf", autoComplete={"id"="company.id", "label"="company.name"},
     *                                       relation="parent")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $facebookAccessToken;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     * @Assert\NotBlank(message = "customer.phoneNumber.not_blank", groups={"customer_registration",
     *                          "user_registration"})
     *
     * @Rule\Property(description="Telefoonnummer")
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     *
     */
    protected $mobilenumber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup", inversedBy="customers")
     *
     * @Rule\Property(description="Klantgroep", autoComplete={"id"="customergroup.id",
     *                                          "label"="customergroup.description"}, relation="child")
     */
    private $customerGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    private $default_delivery_address;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     * @Assert\Valid()
     */
    private $default_invoice_address;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\Address", mappedBy="customer", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $addresses;

    /**
     * @ORM\Column(type="string" , length=255, nullable=true, unique=true)
     *
     */
    protected $username;

    /**
     * @ORM\Column(type="string" , length=255, nullable=true, unique=true)
     *
     */
    protected $usernameCanonical;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "customer.email.not_blank", groups={"customer_registration", "user_registration",
     *                          "cart_sender", "uploadUsers"})
     * @Assert\Email(message = "customer.email.not_valid", groups={"customer_registration", "user_registration",
     *                       "cart_sender", "uploadUsers"})
     * @Rule\Property(description="Emailadres")
     */
    protected $email;

    /**
     * @ORM\Column(type="string" , length=255)
     *
     */
    protected $emailCanonical;

    /**
     * @ORM\Column(type="boolean")
     * @Rule\Property(description="Newsletter")
     */
    protected $newsletter = 0;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Rule\Property(description="Actief")
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $passwordRequestedAt;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $locked;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $expired;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $expiresAt;

    /**
     * @ORM\Column(type="array")
     *
     */
    protected $roles;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $credentialsExpireAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderCollection", mappedBy="customer", cascade={"persist"})
     * @ORM\OrderBy({"id" = "DESC"})
     * @Rule\Property(description="Ordercollection", autoComplete={"id"="order.id", "label"="order.number"},
     *                                               relation="parent")
     */
    protected $orders;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\Paymentmethod")
     */
    protected $preferredPaymentmethod;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    protected $preferredIdealIssuer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="registeredCustomers")
     */
    protected $registeredOnSite;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $locale = 'nl_NL';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $internalRemark;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerInternalRemark", mappedBy="customer")
     */
    protected $customerInternalRemarks;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function doFullname()
    {
        if ($this->firstname && $this->lastname) {
            $this->fullname = $this->firstname . (null !== $this->lastnamePrefix ? ' '.$this->lastnamePrefix : '') . ' ' . $this->lastname;
        }
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function doUsernameToEmail()
    {

        // Bakery hack
        $prefix = 'b' . $this->getId() . '@';
        if (0 === strpos($this->getUsername(), $prefix)) {
            return;
        }

        if ($this->getUsername()) {
            $this->setEmail($this->getUsername());
            $this->setEmailCanonical($this->getUsernameCanonical());
        }

    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->customerInternalRemarks = new ArrayCollection();
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->enabled = false;
        $this->locked = false;
        $this->expired = false;
        $this->roles = [];
        $this->credentialsExpired = false;
    }

    /**
     * @param string $role
     * @return $this|static
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!\in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used during check for
     * changes and the id.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->expiresAt,
            $this->credentialsExpireAt,
            $this->email,
            $this->emailCanonical,
        ]);
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized, []);
        // add a few extra elements in the array to ensure that we have enough keys when unserializing
        // older data which does not include all properties.
        $data = array_merge($data, array_fill(0, 2, null));

        [
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->expiresAt,
            $this->credentialsExpireAt,
            $this->email,
            $this->emailCanonical,
        ] = $data;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * @return null|string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Gets the encrypted password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Gets the last login time.
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @return null|string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     *         $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role
     *
     * @return boolean
     */
    public function hasRole($role)
    {
        return \in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        if (true === $this->expired) {
            return false;
        }

        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        if (true === $this->credentialsExpired) {
            return false;
        }

        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return !$this->isAccountNonExpired();
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * @param string $role
     * @return $this|UserInterface
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @param string $username
     * @return $this|UserInterface
     */
    public function setUsername($username)
    {
        $this->username = $username;

        if (empty($this->email)) {
            $this->email = $username;
        }

        return $this;
    }

    /**
     * @param string $usernameCanonical
     * @return $this|UserInterface
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;

        if (empty($this->emailCanonical)) {
            $this->emailCanonical = $usernameCanonical;
        }

        return $this;
    }

    /**
     * @param \DateTime|null $date
     *
     * @return $this
     */
    public function setCredentialsExpireAt(\DateTime $date = null)
    {
        $this->credentialsExpireAt = $date;

        return $this;
    }

    /**
     * @param $boolean
     *
     * @return $this
     */
    public function setCredentialsExpired($boolean)
    {
        $this->credentialsExpired = $boolean;

        return $this;
    }

    /**
     * @param string $emailCanonical
     * @return $this|UserInterface
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * @param bool $boolean
     * @return $this|UserInterface
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean)$boolean;

        return $this;
    }

    /**
     * Sets this user to expired.
     *
     * @param Boolean $boolean
     *
     * @return $this
     */
    public function setExpired($boolean)
    {
        $this->expired = (Boolean)$boolean;

        return $this;
    }

    /**
     * @param \DateTime|null $date
     *
     * @return $this
     */
    public function setExpiresAt(\DateTime $date = null)
    {
        $this->expiresAt = $date;

        return $this;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param bool $boolean
     *
     * @return $this
     */
    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    /**
     * @param \DateTime|null $time
     *
     * @return $this
     */
    public function setLastLogin(\DateTime $time = null)
    {
        $this->lastLogin = $time;

        return $this;
    }

    /**
     * @param Boolean $boolean
     *
     * @return $this
     */
    public function setLocked($boolean)
    {
        $this->locked = $boolean;

        return $this;
    }

    /**
     * @param null|string $confirmationToken
     * @return $this|UserInterface
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @param \DateTime|null $date
     * @return $this|UserInterface
     */
    public function setPasswordRequestedAt(\DateTime $date = null)
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|\DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param int $ttl
     * @return bool
     */
    public function isPasswordRequestNonExpired($ttl): bool
    {
        return null !== $this->getPasswordRequestedAt() && $this->getPasswordRequestedAt() instanceof \DateTime &&
            $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * @param array $roles
     * @return $this|UserInterface
     */
    public function setRoles(array $roles)
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * @param string $password
     *
     * @return Customer
     *
     * Little hack to force updating User entity
     */
    public function setPlainPassword($password): Customer
    {
        $this->setUpdatedAt(new \DateTime());

        $this->plainPassword = $password;

        return $this;
    }

    /**
     * Set gender
     *
     * @param GenderType $gender
     *
     * @return Customer
     */
    public function setGender($gender): Customer
    {
        $this->gender = $gender;

        return $this;

    }

    /**
     * Get gender
     *
     * @return GenderType
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get initals (extracted from firstname)
     */
    public function getInitials()
    {
        $parts = explode(' ', $this->firstname);

        return implode(' ', array_map(function ($part) {
            return $part[0];
        }, $parts));
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Customer
     */
    public function setFirstname($firstname): Customer
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Customer
     */
    public function setLastname($lastname): Customer
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     * @deprecated Moved into CustomerManager
     */
    public function getSalutation(): string
    {
        $salutation = 'Geachte';

        switch ($this->gender) {
            case 'male':
                $salutation .= ' heer';
                break;
            case 'female':
                $salutation .= ' mevrouw';
                break;
            default:
                $salutation .= ' heer/mevrouw';
        }

        $salutation .= ' ' . $this->lastname;

        return $salutation;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Customer
     */
    public function setBirthday($birthday = null): Customer
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return Customer
     */
    public function setFacebookId($facebookId): Customer
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return Customer
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Check if Customer can login.
     *
     * @return bool
     */
    public function canLogin(): bool
    {
        return ($this->getUsername() && $this->isEnabled() && !$this->isLocked() && !$this->isExpired());
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Customer
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $email
     * @return $this|UserInterface
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set phoneNumber
     *
     * @param $phoneNumber
     *
     * @return Customer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int
     *
     * @return null|string
     */
    public function getFormattedPhoneNumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->phoneNumber) {
            return null;
        }

        try {
            if ($this->getMainAddress()) {
                $countryCode = $this->getMainAddress()->getCountry()->getCode();
            } elseif ($this->getDefaultInvoiceAddress()) {
                $countryCode = $this->getDefaultInvoiceAddress()->getCountry()->getCode();
            } else {
                throw new \RuntimeException('No country detected');
            }

            $phoneNumber = PhoneNumberUtil::getInstance()->parse($this->phoneNumber, $countryCode);

            return PhoneNumberUtil::getInstance()->format($phoneNumber, $format);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Customer
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        if (!$this->fullname) {
            $this->doFullname();
        }

        return $this->fullname;
    }

    /**
     * Set lastnamePrefix
     *
     * @param string $lastnamePrefix
     *
     * @return Customer
     */
    public function setLastnamePrefix($lastnamePrefix)
    {
        $this->lastnamePrefix = $lastnamePrefix;

        return $this;
    }

    /**
     * Get lastnamePrefix
     *
     * @return string
     */
    public function getLastnamePrefix()
    {
        return $this->lastnamePrefix;
    }

    /**
     * Set mobilenumber
     *
     * @param string $mobilenumber
     *
     * @return Customer
     */
    public function setMobilenumber($mobilenumber)
    {
        $this->mobilenumber = $mobilenumber;

        return $this;
    }

    /**
     * Get mobilenumber
     *
     * @return string
     */
    public function getMobilenumber()
    {
        return $this->mobilenumber;
    }

    /**
     * @param int $format
     *
     * @return null|string
     */
    public function getFormattedMobileNumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->mobilenumber) {
            return null;
        }

        try {
            if ($this->getMainAddress()) {
                $countryCode = $this->getMainAddress()->getCountry()->getCode();
            } else {
                throw new \RuntimeException('No country detected');
            }

            $phoneNumber = PhoneNumberUtil::getInstance()->parse($this->mobilenumber, $countryCode);

            return PhoneNumberUtil::getInstance()->format($phoneNumber, $format);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Set customerGroup
     *
     * @param CustomerGroup $customerGroup
     *
     * @return Customer
     */
    public function setCustomerGroup(CustomerGroup $customerGroup = null)
    {
        $this->customerGroup = $customerGroup;

        return $this;
    }

    /**
     * Get customerGroup
     *
     * @return CustomerGroup
     */
    public function getCustomerGroup()
    {
        return $this->customerGroup;
    }

    /**
     * Add address
     *
     * @param Address $address
     *
     * @return Customer $this
     */
    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return ArrayCollection|Address[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Get delivery addresses
     *
     * @return Address|null
     */
    public function getMainAddress(): ?Address
    {
        if ($this->company) {
            return $this->getCompany()->getMainAddress();
        }

        $addresses = $this->filterAddresses(static::ADDRESS_TYPE_MAIN);

        if (!$addresses) {
            return null;
        }

        $addresses = iterator_to_array($addresses);

        $address = reset($addresses);

        if (!$address) {
            return null;
        }

        return $address;
    }

    /**
     * Get delivery addresses
     *
     * @return Address|null
     */
    public function getInvoiceAddress(): ?Address
    {
        if(null !== $this->getCompany()) {
            return $this->getCompany()->getInvoiceAddress();
        }

        if ($this->getDefaultInvoiceAddress()) {
            return $this->getDefaultInvoiceAddress();
        }

        $addresses = $this->filterAddresses(static::ADDRESS_TYPE_INVOICE);

        if (!$addresses) {
            return null;
        }

        $addresses = iterator_to_array($addresses);

        $address = reset($addresses);

        if (!$address) {
            return null;
        }

        return $address;
    }

    /**
     * Get delivery addresses
     *
     * @return Collection
     */
    public function getDeliveryAddresses()
    {
        return $this->filterAddresses(static::ADDRESS_TYPE_DELIVERY);
    }

    /**
     * @param $type
     *
     * @return ArrayCollection|Address[]|bool
     */
    private function filterAddresses($type)
    {
        $addresses = $this->getAddresses() ?: [];
        if (null === $addresses || empty($addresses)) {
            return false;
        }

        /** @var ArrayCollection $addresses */
        return $addresses->filter(function (Address $address) use ($type) {
            switch ($type) {
                case static::ADDRESS_TYPE_MAIN:
                    return $address->isMainAddress();
                case static::ADDRESS_TYPE_INVOICE:
                    return $address->isInvoiceAddress();
                case static::ADDRESS_TYPE_DELIVERY:
                    return $address->isDeliveryAddress();
                default:
                    return false;
            }
        });
    }


    /**
     * Set defaultDeliveryAddress
     *
     * @param Address|null $defaultDeliveryAddress
     *
     * @return Customer
     */
    public function setDefaultDeliveryAddress(Address $defaultDeliveryAddress = null)
    {
        $this->default_delivery_address = $defaultDeliveryAddress;

        return $this;
    }

    /**
     * Get defaultDeliveryAddress
     *
     * @return Address
     */
    public function getDefaultDeliveryAddress()
    {
        return $this->default_delivery_address;
    }

    /**
     * Set defaultInvoiceAddress
     *
     * @param Address $defaultInvoiceAddress
     *
     * @return Customer $this
     */
    public function setDefaultInvoiceAddress(Address $defaultInvoiceAddress = null)
    {
        $this->default_invoice_address = $defaultInvoiceAddress;

        return $this;
    }

    /**
     * Get defaultInvoiceAddress
     *
     * @return Address
     */
    public function getDefaultInvoiceAddress()
    {
        return $this->default_invoice_address;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCustomerNumber()
    {
        return 'TG' . str_pad($this->getId(), 7, '0', STR_PAD_LEFT);
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Customer
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }


    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return \DateTime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getUsername();
    }

    /**
     * Add order
     *
     * @param OrderCollection $orderCollection
     *
     * @return Customer
     */
    public function addOrder(OrderCollection $orderCollection)
    {
        $this->orders[] = $orderCollection;

        return $this;
    }

    /**
     * Remove order
     *
     * @param OrderCollection $orderCollection
     */
    public function removeOrder(OrderCollection $orderCollection)
    {
        $this->orders->removeElement($orderCollection);
    }

    /**
     * Get orders
     *
     * @return Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set preferredPaymentmethod
     *
     * @param Paymentmethod $preferredPaymentmethod
     *
     * @return Customer
     */
    public function setPreferredPaymentmethod(Paymentmethod $preferredPaymentmethod = null)
    {
        $this->preferredPaymentmethod = $preferredPaymentmethod;

        return $this;
    }

    /**
     * Get preferredPaymentmethod
     *
     * @return null|Paymentmethod
     */
    public function getPreferredPaymentmethod()
    {
        return $this->preferredPaymentmethod;
    }

    /**
     * Set preferredIdealIssuer
     *
     * @param string $preferredIdealIssuer
     *
     * @return Customer
     */
    public function setPreferredIdealIssuer($preferredIdealIssuer): Customer
    {
        $this->preferredIdealIssuer = $preferredIdealIssuer;

        return $this;
    }

    /**
     * Get preferredIdealIssuer
     *
     * @return string
     */
    public function getPreferredIdealIssuer()
    {
        return $this->preferredIdealIssuer;
    }

    /**
     * Set registeredOnSite
     *
     * @param Site $registeredOnSite
     *
     * @return Customer
     */
    public function setRegisteredOnSite(Site $registeredOnSite = null): Customer
    {
        $this->registeredOnSite = $registeredOnSite;

        return $this;
    }

    /**
     * Get registeredOnSite
     *
     * @return null|Site
     */
    public function getRegisteredOnSite()
    {
        return $this->registeredOnSite;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Customer
     */
    public function setLocale($locale): Customer
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        if (!$this->locale) {
            return 'nl_NL';
        }

        return $this->locale;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage(): string
    {
        return substr($this->getLocale(), 0, 2);
    }

    /**
     * Set internalRemark
     *
     * @param string $internalRemark
     *
     * @return Customer
     */
    public function setInternalRemark($internalRemark)
    {
        $this->internalRemark = $internalRemark;

        return $this;
    }

    /**
     * Get internalRemark
     *
     * @return string
     */
    public function getInternalRemark()
    {
        return $this->internalRemark;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Customer
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return bool
     */
    public function isMigrated(): bool
    {
        $metadata = $this->getMetadata();

        return null !== $metadata && isset($metadata['bloemorderCustomerId']);
    }

    /**
     * Add customerInternalRemark
     *
     * @param CustomerInternalRemark $customerInternalRemark
     *
     * @return Customer
     */
    public function addCustomerInternalRemark(CustomerInternalRemark $customerInternalRemark)
    {
        $this->customerInternalRemarks[] = $customerInternalRemark;

        return $this;
    }

    /**
     * Remove customerInternalRemark
     *
     * @param CustomerInternalRemark $customerInternalRemark
     */
    public function removeCustomerInternalRemark(CustomerInternalRemark $customerInternalRemark)
    {
        $this->customerInternalRemarks->removeElement($customerInternalRemark);
    }

    /**
     * Get customerInternalRemarks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerInternalRemarks()
    {
        return $this->customerInternalRemarks;
    }

    /**
     * @return array
     */
    public function getAuditSettings(): array
    {
        return [
            'mapping' => [
                'fullname' => 'Naam',
                'username' => 'Gebruikersnaam',
                'email' => 'E-mailadres',
                'phonenumber' => 'Telefoonnummer',
                'last_login' => 'Laatst ingelogd',
                'deletedAt' => 'Verwijderd op',
                'createdAt' => 'Aaangemaakt op',
                'updatedAt' => 'Geupdate op',
            ],
            'label' => function (Customer $customer) {
                return $customer->getFullname();
            },
            'title' => function (Customer $customer) {
                return $customer->getFullname();
            },
            'route' => function (Customer $customer) {
                return [
                    'route' => 'admin_customer_customer_view',
                    'routeParameters' => [
                        'id' => $customer->getId(),
                    ],
                ];
            },
        ];
    }

    /**
     * @return null|bool
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param bool $newsletter
     *
     * @return Customer
     */
    public function setNewsletter(bool $newsletter): Customer
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * @return array
     */
    public static function getIgnoredColumns(): array
    {
        $defaultColumns = NonEditableEntity::$defaultIgnoredColumns;

        $columns = [
            'lastLogin',
        ];

        return array_merge($columns, $defaultColumns);
    }
}
