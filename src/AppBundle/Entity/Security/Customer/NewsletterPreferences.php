<?php

namespace AppBundle\Entity\Security\Customer;

use AppBundle\Entity\Site\Site;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;


/**
 * @ORM\Entity
 *
 */
class NewsletterPreferences
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer", mappedBy="customer",
     *                                                                           cascade={"persist"})
     * @Rule\Property(description="Klanten Nieuwsbrief voorkeuren", autoComplete={"id"="customer.id",
     *                                     "label"="customer.fullname"}, relation="parent")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     * @var Customer
     */
    protected $customer;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $typeNoMail;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $typeNewsletters;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $typeDiscountActions;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $typeResearch;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $siteTopbloemen;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $siteTopgeschenken;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $siteToptaart;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $siteTopfruit;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @var boolean
     */
    protected $siteSuppliers;

    /**
     * NewsletterPreferences constructor.
     */
    public function __construct()
    {
        $this->typeNoMail = false;
        $this->typeNewsletters = false;
        $this->typeDiscountActions = false;
        $this->typeResearch = false;
        $this->siteTopbloemen = false;
        $this->siteTopgeschenken = false;
        $this->siteToptaart = false;
        $this->siteTopfruit = false;
        $this->siteSuppliers = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return NewsletterPreferences
     */
    public function setCustomer(Customer $customer): NewsletterPreferences
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return NewsletterPreferences
     */
    public function setEmail(string $email): NewsletterPreferences
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return NewsletterPreferences
     */
    public function setName(string $name): NewsletterPreferences
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set TypeNoMail
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setTypeNoMail(bool $set): NewsletterPreferences
    {
        $this->typeNoMail = $set;

        return $this;
    }

    /**
     * Get TypeNoMail
     *
     * @return bool
     */
    public function getTypeNoMail(): bool
    {
        return $this->typeNoMail;
    }

    /**
     * Set TypeNewsletters
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setTypeNewsletters(bool $set): NewsletterPreferences
    {
        $this->typeNewsletters = $set;

        return $this;
    }

    /**
     * Get TypeNewsletters
     *
     * @return bool
     */
    public function getTypeNewsletters(): bool
    {
        return $this->typeNewsletters;
    }

    /**
     * Set TypeDiscountActions
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setTypeDiscountActions(bool $set): NewsletterPreferences
    {
        $this->typeDiscountActions = $set;

        return $this;
    }

    /**
     * Get TypeDiscountActions
     *
     * @return bool
     */
    public function getTypeDiscountActions(): bool
    {
        return $this->typeDiscountActions;
    }

    /**
     * Set TypeResearch
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setTypeResearch(bool $set): NewsletterPreferences
    {
        $this->typeResearch = $set;

        return $this;
    }

    /**
     * Get TypeResearch
     *
     * @return bool
     */
    public function getTypeResearch(): bool
    {
        return $this->typeResearch;
    }

    /**
     * Set SiteTopbloemen
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setSiteTopbloemen(bool $set): NewsletterPreferences
    {
        $this->siteTopbloemen = $set;

        return $this;
    }

    /**
     * Get SiteTopbloemen
     *
     * @return bool
     */
    public function getSiteTopbloemen(): bool
    {
        return $this->siteTopbloemen;
    }

    /**
     * Set SiteTopgeschenken
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setSiteTopgeschenken(bool $set): NewsletterPreferences
    {
        $this->siteTopgeschenken = $set;

        return $this;
    }

    /**
     * Get SiteTopgeschenken
     *
     * @return bool
     */
    public function getSiteTopgeschenken(): bool
    {
        return $this->siteTopgeschenken;
    }

    /**
     * Set SiteToptaart
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setSiteToptaart(bool $set): NewsletterPreferences
    {
        $this->siteToptaart = $set;

        return $this;
    }

    /**
     * Get SiteToptaart
     *
     * @return bool
     */
    public function getSiteToptaart(): bool
    {
        return $this->siteToptaart;
    }

    /**
     * Set SiteTopfruit
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setSiteTopfruit(bool $set): NewsletterPreferences
    {
        $this->siteTopfruit = $set;

        return $this;
    }

    /**
     * Get SiteTopfruit
     *
     * @return bool
     */
    public function getSiteTopfruit(): bool
    {
        return $this->siteTopfruit;
    }

    /**
     * Set SiteSuppliers
     *
     * @param bool $set
     * @return NewsletterPreferences
     */
    public function setSiteSuppliers(bool $set): NewsletterPreferences
    {
        $this->siteSuppliers = $set;

        return $this;
    }

    /**
     * Get SiteSuppliers
     *
     * @return bool
     */
    public function getSiteSuppliers(): bool
    {
        return $this->siteSuppliers;
    }

    /**
     * @param Site $site
     * @return NewsletterPreferences
     */
    public function setSitesByDetermenSite(Site $site): NewsletterPreferences
    {
        $themeName = $site->getTheme();
        if ($themeName === 'topbloemen') {
            $this->setSiteTopbloemen(true);
        }
        if ($themeName === 'topgeschenken') {
            $this->setSiteTopgeschenken(true);
        }
        if ($themeName === 'toptaarten') {
            $this->setSiteToptaart(true);
        }
        if ($themeName === 'topfruit') {
            $this->setSiteTopfruit(true);
        }
        if ($themeName === 'suppliers') {
            $this->setSiteSuppliers(true);
        }

        return $this;
    }

}
