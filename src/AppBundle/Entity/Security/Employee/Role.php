<?php

namespace AppBundle\Entity\Security\Employee;

use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\RoleColumn;
use AppBundle\Model\Role\RoleModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\Table(name="role")
 * @UniqueEntity(fields={"name"})
 * Class Role
 *
 * @package AppBundle\Entity\Role
 */
class Role extends RoleModel
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\User", mappedBy="roles", cascade={"persist"})
     * @ORM\JoinTable(name="role_roles_users",
     *     joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="adiuvo_user_id", referencedColumnName="id")}
     *     )
     */
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\Group", mappedBy="roles", cascade={"persist"})
     * @ORM\JoinTable(name="role_roles_groups",
     *     joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="adiuvo_user_group_id", referencedColumnName="id")}
     *     )
     */
    protected $groups;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\RoleCategory", inversedBy="roles")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\RoleColumn", inversedBy="roles", cascade={"persist"})
     */
    protected $column;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->category = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Role
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\Security\Employee\User $user
     *
     * @return Role
     */
    public function addUser(\AppBundle\Entity\Security\Employee\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\Security\Employee\User $user
     */
    public function removeUser(\AppBundle\Entity\Security\Employee\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set category
     *
     * @param RoleCategory $category
     *
     * @return Role
     */
    public function setCategory(RoleCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return ArrayCollection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add group
     *
     * @param Group $group
     *
     * @return Role
     */
    public function addGroup(Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param Group $group
     */
    public function removeGroup(Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set column
     *
     * @param RoleColumn $column
     *
     * @return Role
     */
    public function setColumn(RoleColumn $column = null)
    {
        $this->column = $column;

        return $this;
    }

    /**
     * Get column
     *
     * @return RoleColumn
     */
    public function getColumn()
    {
        return $this->column;
    }
}
