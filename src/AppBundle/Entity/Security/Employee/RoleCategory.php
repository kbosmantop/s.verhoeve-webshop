<?php

namespace AppBundle\Entity\Security\Employee;

use AppBundle\Model\Role\RoleCategoryModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\Table(name="role_category")
 * @UniqueEntity(fields={"name"})
 * Class RoleCategory
 * @package AppBundle\Entity\Role
 */
class RoleCategory extends RoleCategoryModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Employee\Role", mappedBy="category", cascade={"persist"})
     */
    protected $roles;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Employee\RoleColumn", mappedBy="category",
     *                                                                              cascade={"persist"})
     */
    protected $columns;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->columns = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RoleCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return RoleCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add role
     *
     * @param Role $role
     *
     * @return RoleCategory
     */
    public function addRole(Role $role)
    {
        $role->setCategory($this);

        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add column
     *
     * @param RoleColumn $column
     *
     * @return RoleCategory
     */
    public function addColumn(RoleColumn $column)
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * Remove column
     *
     * @param RoleColumn $column
     */
    public function removeColumn(RoleColumn $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return Collection
     */
    public function getColumns()
    {
        return $this->columns;
    }
}
