<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @link http://future500.nl/articles/2013/09/doctrine-2-how-to-handle-join-tables-with-extra-columns/
 */

/**
 * @ORM\Entity
 */
class SubscriptionProduct
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var integer $id
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Subscription", inversedBy="subscriptionProducts")
     */
    protected $subscription;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     */
    private $quantity = 1;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    protected $product;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $forwardPrice;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set subscription
     *
     * @param Subscription $subscription
     *
     * @return SubscriptionProduct
     */
    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return SubscriptionProduct
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return SubscriptionProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return SubscriptionProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set forwardPrice
     *
     * @param float $forwardPrice
     *
     * @return SubscriptionProduct
     */
    public function setForwardPrice($forwardPrice)
    {
        $this->forwardPrice = $forwardPrice;

        return $this;
    }

    /**
     * Get forwardPrice
     *
     * @return float
     */
    public function getForwardPrice()
    {
        return $this->forwardPrice;
    }
}
