<?php

namespace AppBundle\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class OrderDeliveryStatus
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $paazlStatus;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return OrderDeliveryStatus
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OrderDeliveryStatus
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set paazlStatus
     *
     * @param string $paazlStatus
     *
     * @return OrderDeliveryStatus
     */
    public function setPaazlStatus($paazlStatus)
    {
        $this->paazlStatus = $paazlStatus;

        return $this;
    }

    /**
     * Get paazlStatus
     *
     * @return string
     */
    public function getPaazlStatus()
    {
        return $this->paazlStatus;
    }
}
