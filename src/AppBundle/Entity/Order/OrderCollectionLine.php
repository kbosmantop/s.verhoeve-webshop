<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Order\LineInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class OrderCollectionLine
 * @package AppBundle\Entity\Order\OrderCollection
 */
class OrderCollectionLine implements LineInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    protected $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="id")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $product;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $discountAmount;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="lines")
     */
    protected $orderCollection;

    /**
     * @var Order|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order")
     */
    protected $relatedOrder;

    /**
     * @var Collection|BillingItem[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\BillingItem", mappedBy="orderCollectionLine")
     */
    protected $billingItems;

    /**
     * @var OrderCollectionLineVat|ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderCollectionLineVat", mappedBy="orderCollectionLine", cascade={"persist"})
     */
    protected $vatLines;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * Set uuid
     *
     * @param uuid $uuid
     *
     * @return OrderCollectionLine
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->billingItems = new ArrayCollection();
        $this->vatLines = new ArrayCollection();
        $this->status = 'actual';
    }

    /**
     * Add billingItem
     *
     * @param BillingItem $billingItem
     *
     * @return OrderCollectionLine
     */
    public function addBillingItem(BillingItem $billingItem)
    {
        if (!$this->billingItems->contains($billingItem)) {
            $this->billingItems[] = $billingItem;
        }

        return $this;
    }

    /**
     * Remove billingItem
     *
     * @param BillingItem $billingItem
     */
    public function removeBillingItem(BillingItem $billingItem)
    {
        if ($this->billingItems->contains($billingItem)) {
            $this->billingItems->removeElement($billingItem);
        }
    }

    /**
     * Get billingItems
     *
     * @return Collection
     */
    public function getBillingItems()
    {
        return $this->billingItems;
    }

    /**
     * @return OrderCollectionLineVat[]|ArrayCollection
     */
    public function getVatLines(): Collection
    {
        return $this->vatLines;
    }

    /**
     * @param OrderCollectionLineVat $orderCollectionLineVat
     */
    public function addVatLine(OrderCollectionLineVat $orderCollectionLineVat)
    {
        if(!$this->vatLines->contains($orderCollectionLineVat)) {
            $this->vatLines->add($orderCollectionLineVat);
        }
    }

    /**
     * @param OrderCollectionLineVat $orderCollectionLineVat
     */
    public function removeVatLine(OrderCollectionLineVat $orderCollectionLineVat)
    {
        if($this->vatLines->contains($orderCollectionLineVat)) {
            $this->vatLines->removeElement($orderCollectionLineVat);
        }
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getTotalPriceIncl()
    {
        return round(array_sum($this->getVatLines()->map(function(OrderCollectionLineVat $line) {
            return round($line->getAmount() * (1 + $line->getVatRate()->getFactor()), 2);
        })->toArray()), 2);
    }

    /**
     * @param int $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * @return float|int
     */
    public function getDiscountAmountExcl()
    {
        /** @var OrderCollectionLineVat $vatLine */
        $vatLine = $this->getVatLines()->current();

        return $this->discountAmount / (1 + $vatLine->getVatRate()->getFactor());
    }

    /**
     * @param int $discountAmount
     */
    public function setDiscountAmount($discountAmount): void
    {
        $this->discountAmount = $discountAmount;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }

    /**
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * @param OrderCollection $orderCollection
     */
    public function setOrderCollection(OrderCollection $orderCollection): void
    {
        $this->orderCollection = $orderCollection;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return Order|null
     */
    public function getRelatedOrder(): ?Order
    {
        return $this->relatedOrder;
    }

    /**
     * @param Order|null $relatedOrder
     */
    public function setRelatedOrder(?Order $relatedOrder): void
    {
        $this->relatedOrder = $relatedOrder;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return round($this->getTotalPrice() * $this->getQuantity(), 3);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}
