<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\BillingItemGroup;
use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyCustomOrderFieldValue;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AppBundle\Interfaces\PayableInterface;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Model\OrderCollectionModel;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohReview;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Solr\Document(index="order_collection")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderCollectionRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="number", columns={"number"})
 * })
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\EntityListeners({"AppBundle\EventListener\Order\OrderCollectionListener"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Rule\Entity(description="Bestelling")
 */
class OrderCollection extends OrderCollectionModel implements PayableInterface, OrderCollectionInterface, AuditEntityInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @Solr\Id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\Cart", mappedBy="orderCollection", fetch="EXTRA_LAZY")
     */
    protected $cart;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\ThirdParty\Kiyoh\Entity\KiyohReview", mappedBy="orderCollection",
     *                                                                             fetch="EXTRA_LAZY")
     */
    private $review;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="integer", nullable=true)
     * @Rule\Property(description="Ordernummer")
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site")
     * @Rule\Property(description="Site", autoComplete={"id"="site.id", "label"="site.translate.description"},
     *                                    relation="parent")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="orders", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     *
     * @Rule\Property(description="Bedrijf", autoComplete={"id"="company.id", "label"="company.name"}, relation="parent")
     */
    protected $company;

    /**
     * @Solr\Field(type="text", getter="getEmail")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer", inversedBy="orders",
     *                                                                            cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     *
     * @Rule\Property(description="Contactpersoon", autoComplete={"id"="customer.id", "label"="customer.fullname"},
     *                                     relation="child")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     *
     * @Rule\Property(description="Medewerker", autoComplete={"id"="user.id", "label"="user.username"},
     *                                          relation="parent")
     */
    protected $employee;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    protected $invoiceAddress;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=200, nullable=true)
     *
     */
    protected $invoiceAddressCompanyName;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=200, nullable=true)
     *
     */
    protected $invoiceAddressAttn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Rule\Property(description="(Factuur) Straat")
     */
    protected $invoiceAddressStreet;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Rule\Property(description="(Factuur) Huisnummer")
     */
    protected $invoiceAddressNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Rule\Property(description="(Factuur) Huisnummer toevoeging")
     */
    protected $invoiceAddressNumberAddition;

    /**
     * @Solr\Field(type="text")
     *
     */
    protected $invoiceAddressStreetAndNumber;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Rule\Property(description="(Factuur) Postcode")
     *
     */
    protected $invoiceAddressPostcode;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Rule\Property(description="(Factuur) Stad")
     */
    protected $invoiceAddressCity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="invoice_address_country", referencedColumnName="id", nullable=true)
     * @Rule\Property(description="Land", autoComplete={"id"="country.id", "label"="country.translate.name"})
     * @Assert\Valid()
     *
     */
    protected $invoiceAddressCountry;

    /**
     * @Solr\Field(type="string")
     * @ORM\Column(type="string", length=35, nullable=true)
     *
     */
    protected $invoiceAddressPhoneNumber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Subscription")
     *
     */
    protected $subscription;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     */
    protected $subscriptionDate;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=50, nullable=true);
     *
     */
    protected $ip;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\Order", mappedBy="orderCollection", cascade={"persist"})
     * @Rule\Property(description="Order", autoComplete={"id"="order.id", "label"="order.number"}, relation="child")
     */
    protected $orders;

    /**
     * @ORM\Column(type="integer")
     *
     */
    protected $priority;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment\Payment", mappedBy="orderCollection", cascade={"persist"})
     * @Rule\Property(description="Betalingen", autoComplete={"id"="payment.id", "label"="payment.id"})
     */
    protected $payments;

    /**
     * @ORM\Column(type="float")
     *
     * @Rule\Property(description="Totaal prijs (excl. BTW)")
     */
    protected $totalPrice;

    /**
     * @ORM\Column(type="float")
     *
     * @Rule\Property(description="Totaal prijs (incl. BTW)")
     */
    protected $totalPriceIncl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Rule\Property(description="Fraude score")
     */
    protected $fraudScore;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderActivity", mappedBy="orderCollection")
     */
    protected $activities;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @Rule\Property(description="Aangemaakt op")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Discount\Voucher", mappedBy="orderCollections")
     */
    protected $vouchers;

    /**
     * @var Collection|BillingItemGroup[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\BillingItemGroup", mappedBy="orderCollection")
     */
    protected $billingItemGroups;

    /**
     * @var Collection|OrderCollectionLine
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderCollectionLine", mappedBy="orderCollection", cascade={"persist"})
     */
    protected $lines;

    /**
     * @var CompanyCustomOrderFieldValue[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyCustomOrderFieldValue", mappedBy="orderCollection")
     */
    protected $companyCustomerOrderFieldValues;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->vouchers = new ArrayCollection();
        $this->billingItemGroups = new ArrayCollection();
        $this->lines = new ArrayCollection();
        $this->companyCustomerOrderFieldValues = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getPaymentDescription()
    {
        return $this->getSite()->translate()->getDescription() . ' order ' . $this->getNumber();
    }

    /**
     * @return void
     */
    public function getPaymentStatus()
    {

    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function calculatePriority()
    {
        $this->priority = 50;
    }

    /**
     * @deprecated use OrderManager::calculateTotals instead
     */
    public function calculateTotals()
    {
        @trigger_error('calculateTotals() Use OrderManager::calculateTotals instead.', E_USER_DEPRECATED);

        $this->totalPrice = 0;
        $this->totalPriceIncl = 0;

        foreach ($this->getOrders() as $order) {
            $this->totalPrice += $order->getTotalPrice();
            $this->totalPriceIncl += $order->getTotalPriceIncl();
        }
    }

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function load()
    {
        /**
         *  Build additional solr indexes
         */
        $this->invoiceAddressStreetAndNumber = trim($this->invoiceAddressStreet . " " . $this->invoiceAddressNumber);

        if (!empty($this->invoiceAddressNumberAddition)) {
            $this->invoiceAddressStreetAndNumber .= '-' . $this->invoiceAddressNumberAddition;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return OrderCollection
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set orders
     *
     * @param Order[] $orders
     *
     * @return OrderCollection
     */
    public function setOrders($orders)
    {
        $this->orders = new ArrayCollection();

        foreach ($orders as $order) {
            $this->addOrder($order);
        }

        return $this;
    }


    /**
     * Add order
     *
     * @param Order $order
     *
     * @return OrderCollection
     */
    public function addOrder(Order $order)
    {
        $order->setOrder($this);

        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
        $order->setDeletedAt(new \DateTime());
    }

    /**
     * Remove orders
     */
    public function removeOrders()
    {
        foreach ($this->orders as $order) {
            $order->removeLines();

            $this->removeOrder($order);
        }
    }

    /**
     * Get orders
     *
     * @return Collection|Order[]
     * @Rule\MappedProperty(property="orders")
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set subscription
     *
     * @param Subscription $subscription
     *
     * @return OrderCollection
     */
    public function setSubscription(Subscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Set subscriptionDate
     *
     * @param \DateTime $subscriptionDate
     *
     * @return OrderCollection
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;

        return $this;
    }

    /**
     * Get subscriptionDate
     *
     * @return \DateTime
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return OrderCollection
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return OrderCollection
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        /**
         * The ordernumber isn't directly available after creating it, so generating it when needed (#1660)
         */
        if (!$this->number && $this->getId()) {
            $this->number = $this->getId() + 10000000;
        }

        return $this->number;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return OrderCollection
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     * @Rule\MappedProperty(property="company")
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return OrderCollection
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     * @Rule\MappedProperty(property="customer")
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return OrderCollection
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     * @Rule\MappedProperty(property="site")
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Add payment
     *
     * @param Payment $payment
     *
     * @return OrderCollection
     */
    public function addPayment(Payment $payment)
    {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param Payment $payment
     */
    public function removePayment(Payment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Get payments
     *
     * @return Collection|Payment[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @return bool
     */
    public function hasPendingPayment()
    {
        $pendingPayments = $this->payments->filter(function (Payment $payment) {
            return $payment->getStatus()->getId() == "pending";
        });

        return !$pendingPayments->isEmpty();
    }

    /**
     * @param $code string
     *
     * @return bool
     */
    public function hasPaymentMethod($code)
    {
        if ($this->getPayments()) {
            foreach ($this->getPayments() as $payment) {
                if (!$payment->getPaymentmethod()) {
                    continue;
                }

                if ($payment->getPaymentmethod()->getCode() == $code) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Set totalPrice
     *
     * @param number $totalPrice
     *
     * @return OrderCollection
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     * @Rule\MappedProperty(property="totalPrice")
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set totalPriceIncl
     *
     * @param float $totalPriceIncl
     *
     * @return OrderCollection
     */
    public function setTotalPriceIncl($totalPriceIncl)
    {
        $this->totalPriceIncl = $totalPriceIncl;

        return $this;
    }

    /**
     * Get totalPriceIncl
     * @Rule\MappedProperty(property="totalPriceIncl")
     *
     * @return float
     */
    public function getTotalPriceIncl()
    {
        return $this->totalPriceIncl;
    }

    /**
     * Set review
     *
     * @param KiyohReview $review
     *
     * @return OrderCollection
     */
    public function setReview(KiyohReview $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return KiyohReview
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set invoiceAddressCompanyName
     *
     * @param string $invoiceAddressCompanyName
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressCompanyName($invoiceAddressCompanyName)
    {
        $this->invoiceAddressCompanyName = $invoiceAddressCompanyName;

        return $this;
    }

    /**
     * Get invoiceAddressCompanyName
     *
     * @return string
     */
    public function getInvoiceAddressCompanyName()
    {
        return $this->invoiceAddressCompanyName;
    }

    /**
     * Set invoiceAddressAttn
     *
     * @param string $invoiceAddressAttn
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressAttn($invoiceAddressAttn)
    {
        $this->invoiceAddressAttn = $invoiceAddressAttn;

        return $this;
    }

    /**
     * Get invoiceAddressAttn
     *
     * @return string
     */
    public function getInvoiceAddressAttn()
    {
        return $this->invoiceAddressAttn;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;

        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressNumber
     *
     * @param string $invoiceAddressNumber
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressNumber($invoiceAddressNumber)
    {
        $this->invoiceAddressNumber = $invoiceAddressNumber;

        return $this;
    }

    /**
     * Get invoiceAddressNumber
     *
     * @return string
     */
    public function getInvoiceAddressNumber()
    {
        return $this->invoiceAddressNumber;
    }

    /**
     * Set invoiceAddressNumberAddition
     *
     * @param string $invoiceAddressNumberAddition
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressNumberAddition($invoiceAddressNumberAddition)
    {
        $this->invoiceAddressNumberAddition = $invoiceAddressNumberAddition;

        return $this;
    }

    /**
     * Get invoiceAddressNumber
     *
     * @return string
     */
    public function getInvoiceAddressNumberAddition()
    {
        return $this->invoiceAddressNumberAddition;
    }

    /**
     * Get the invoice address street, number and number addition as one string
     *
     * @return string
     */
    public function getInvoiceAddressStreetAndNumber()
    {
        return $this->invoiceAddressStreetAndNumber;
    }

    /**
     * Set invoiceAddressPostcode
     *
     * @param string $invoiceAddressPostcode
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressPostcode($invoiceAddressPostcode)
    {
        $this->invoiceAddressPostcode = $invoiceAddressPostcode;

        return $this;
    }

    /**
     * Get invoiceAddressPostcode
     *
     * @return string
     */
    public function getInvoiceAddressPostcode()
    {
        return $this->invoiceAddressPostcode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;

        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressPhoneNumber
     *
     * @param string $invoiceAddressPhoneNumber
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressPhoneNumber($invoiceAddressPhoneNumber)
    {
        $this->invoiceAddressPhoneNumber = $invoiceAddressPhoneNumber;

        return $this;
    }

    /**
     * Get invoiceAddressPhoneNumber
     *
     * @return string
     */
    public function getInvoiceAddressPhoneNumber()
    {
        return $this->invoiceAddressPhoneNumber;
    }

    /**
     * Get formatted invoiceAddressPhoneNumber
     *
     * @param int $format
     * @return string
     */
    public function getInvoiceAddressFormattedPhoneNumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->invoiceAddressPhoneNumber) {
            return null;
        }

        try {
            $country = $this->getInvoiceAddressCountry()->getCode();

            $phoneNumber = PhoneNumberUtil::getInstance()->parse($this->invoiceAddressPhoneNumber, $country);

            return PhoneNumberUtil::getInstance()->format($phoneNumber, $format);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Set invoiceAddress
     *
     * @param Address $invoiceAddress
     *
     * @return OrderCollection
     */
    public function setInvoiceAddress(Address $invoiceAddress = null)
    {
        if ($invoiceAddress && $this->invoiceAddress != $invoiceAddress) {
            $this->invoiceAddressCompanyName = $invoiceAddress->getCompanyName();
            $this->invoiceAddressAttn = $invoiceAddress->getAttn();
            $this->invoiceAddressStreet = $invoiceAddress->getStreet();
            $this->invoiceAddressNumber = $invoiceAddress->getNumber();
            $this->invoiceAddressNumberAddition = $invoiceAddress->getNumberAddition();
            $this->invoiceAddressPostcode = $invoiceAddress->getPostcode();
            $this->invoiceAddressCity = $invoiceAddress->getCity();
            $this->invoiceAddressCountry = $invoiceAddress->getCountry();
            $this->invoiceAddressPhoneNumber = $invoiceAddress->getPhoneNumber();
        }

        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * Get invoiceAddress
     *
     * @return Address
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * Set invoiceAddressCountry
     *
     * @param Country $invoiceAddressCountry
     *
     * @return OrderCollection
     */
    public function setInvoiceAddressCountry(Country $invoiceAddressCountry = null)
    {
        $this->invoiceAddressCountry = $invoiceAddressCountry;

        return $this;
    }

    /**
     * Get invoiceAddressCountry
     *
     * @return Country
     */
    public function getInvoiceAddressCountry()
    {
        return $this->invoiceAddressCountry;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     *
     * @return OrderCollection
     */
    public function setCart(Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Add activity
     *
     * @param OrderActivity $activity
     *
     * @return OrderCollection
     */
    public function addActivity(OrderActivity $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param OrderActivity $activity
     */
    public function removeActivity(OrderActivity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set employee
     *
     * @param User $employee
     *
     * @return OrderCollection
     */
    public function setEmployee(User $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return User
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param $fraudScore
     * @return $this
     */
    public function setFraudScore($fraudScore)
    {
        $this->fraudScore = $fraudScore;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFraudScore()
    {
        return $this->fraudScore;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return OrderCollection
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return array
     */
    public function getAuditSettings(): array
    {
        return [
            'mapping' => [
                'invoiceAddress' => 'Factuur adress',
                'invoiceAddressCompanyName' => 'Factuur bedrijf',
                'invoiceAddressAttn' => 'Factuur ontvanger',
                'invoiceAddressStreet' => 'Factuur straat',
                'invoiceAddressNumber' => 'Factuur huisnummer',
                'invoiceAddressNumberAddition' => 'Factuur huisnummer toevoeging',
                'invoiceAddressStreetAndNumber' => 'Factuur straat en nummer',
                'invoiceAddressPostcode' => 'Factuur postcode',
                'invoiceAddressCity' => 'Factuur plaats',
                'invoiceAddressCountry' => 'Factuur land ',
                'invoiceAddressPhoneNumber' => 'Telefoonnummer ',
                'deletedAt' => 'Verwijderd op',
                'createdAt' => 'Aaangemaakt op',
                'updatedAt' => 'Geupdate op',
            ],
            'label' => function (OrderCollection $orderCollection) {
                /** @var OrderCollection $orderCollection */
                if ($orderCollection->getInvoiceAddressCompanyName()) {
                    return $orderCollection->getInvoiceAddressCompanyName();
                }

                return $orderCollection->getInvoiceAddressAttn();
            },
            'title' => function (OrderCollection $orderCollection) {
                /** @var OrderCollection $orderCollection */
                $prefix = $orderCollection->getNumber();

                if ($orderCollection->getInvoiceAddressCompanyName()) {
                    return $prefix . ' ' . $orderCollection->getInvoiceAddressCompanyName();
                }

                return $prefix . ' ' . $orderCollection->getInvoiceAddressAttn();
            },
            'route' => function (OrderCollection $orderCollection) {
                return [
                    'popup' => true,
                    'route' => 'admin_sales_order_order',
                    'routeParameters' => [
                        'orderCollection' => $orderCollection->getId(),
                    ],
                ];
            },
        ];
    }

    /**
     * Add voucher
     *
     * @param Voucher $voucher
     *
     * @return OrderCollection
     */
    public function addVoucher(Voucher $voucher)
    {
        if (false === $this->vouchers->contains($voucher)) {
            $this->vouchers->add($voucher);
            $voucher->addOrderCollection($this);
        }

        return $this;
    }

    /**
     * Remove voucher
     *
     * @param Voucher $voucher
     */
    public function removeVoucher(Voucher $voucher)
    {
        if ($this->vouchers->contains($voucher)) {
            $this->vouchers->removeElement($voucher);
        }

        $voucher->removeOrderCollection($this);
    }

    /**
     * @return OrderCollection
     */
    public function removeAllVouchers()
    {
        foreach ($this->vouchers as $voucher) {
            $this->removeVoucher($voucher);
        }

        return $this;
    }

    /**
     * Get vouchers
     *
     * @return Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * Add billingItemGroup
     *
     * @param BillingItemGroup $billingItemGroup
     *
     * @return OrderCollection
     */
    public function addBillingItemGroup(BillingItemGroup $billingItemGroup)
    {
        if (!$this->billingItemGroups->contains($billingItemGroup)) {
            $this->billingItemGroups[] = $billingItemGroup;
        }

        return $this;
    }

    /**
     * Remove billingItemGroup
     *
     * @param BillingItemGroup $billingItemGroup
     */
    public function removeBillingItemGroup(BillingItemGroup $billingItemGroup)
    {
        if ($this->billingItemGroups->contains($billingItemGroup)) {
            $this->billingItemGroups->removeElement($billingItemGroup);
        }
    }

    /**
     * Get billingItemGroups
     *
     * @return Collection
     */
    public function getBillingItemGroups()
    {
        return $this->billingItemGroups;
    }

    /**
     * Add line
     *
     * @param OrderCollectionLine $line
     *
     * @return OrderCollection
     */
    public function addLine(OrderCollectionLine $line)
    {
        if (!$this->lines->contains($line)) {
            $line->setOrderCollection($this);

            $this->lines[] = $line;
        }

        return $this;
    }

    /**
     * Remove line
     *
     * @param OrderCollectionLine $line
     */
    public function removeLine(OrderCollectionLine $line)
    {
        if ($this->lines->contains($line)) {
            $this->lines->removeElement($line);
        }
    }

    /**
     * Get lines
     *
     * @return Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @return CompanyCustomOrderFieldValue[]|Collection
     */
    public function getCompanyCustomerOrderFieldValues()
    {
        return $this->companyCustomerOrderFieldValues;
    }
}
