<?php

namespace AppBundle\Entity\Order;

use AppBundle\Annotation\GeneralListener;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Ledger;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Model\OrderLineModel;
use AppBundle\Traits\UuidEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Recurr\Exception;
use RuleBundle\Annotation as Rule;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use stdClass;

/**
 * @ORM\Entity(repositoryClass="OrderLineRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @GeneralListener(parent="OrderOrder", parentGetter="getOrder", get="updatedAt", set="updatedAt")
 * @Rule\Entity(description="Orderregel")
 */
class OrderLine extends OrderLineModel implements OrderLineInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\CartOrderLine", mappedBy="orderLine")
     */
    protected $cartLine;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="lines")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @Rule\Property(description="Order", autoComplete={"id"="order.id", "label"="order.number"}, relation="parent")
     */
    protected $order;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderLine", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderLine", mappedBy="parent", cascade={"persist"})
     * @Rule\Property(description="Sub-orderregel", autoComplete={ "id"="order.id", "label"="order.id"})
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     *
     * @Rule\Property(description="Product", autoComplete={"id"="product.id", "label"="product.translate.title"},
     *                                       relation="child")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     *
     * @Rule\Property(description="Aantal")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @Rule\Property(description="titel")
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Ledger")
     * @ORM\JoinColumn(name="ledger_id", referencedColumnName="id")
     */
    protected $ledger;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagePath;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Rule\Property(description="Prijs")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Rule\Property(description="Kortingsbedrag")
     */
    private $discountAmount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Vat")
     *
     * @Rule\Property(description="BTW percentage", autoComplete={"id"="vat.id", "label"="vat.description"},
     *                                 relation="child")
     */
    private $vat;

    /**
     * @var OrderLineVat[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderLineVat", mappedBy="orderLine", cascade={"persist", "remove"})
     */
    private $vatLines;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\DiscountType")
     * @ORM\Column(type="DiscountType", nullable=true)
     */
    private $discountType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->quantity = 1;
        $this->vatLines = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get metadata
     *
     * @return array|null
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set metadata
     *
     * @param array|null $metadata
     *
     * @return OrderLine
     */
    public function setMetadata(?array $metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     * @Rule\MappedProperty(property="product")
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return $this
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        $total = 0;
        foreach($this->getVatLines() as $vatLine) {
            $total += $vatLine->getAmount();
        }
        return round($total, 3);
    }

    /**
     * Get price
     *
     * @return float
     * @Rule\MappedProperty(property="price")
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     * @Rule\MappedProperty(property="Aantal")
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return OrderLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get totalPrice including vat
     * @deprecated
     * @return float
     */
    public function getTotalPriceIncl($withDiscount = true)
    {
        $price = round($this->getPriceIncl(), 2) * $this->getQuantity();

        if (true === $withDiscount) {
            $price -= $this->getDiscountAmount();
        }

        return $price;
    }

    /**
     * Get price including vat
     * @deprecated
     * @return float
     */
    public function getPriceIncl()
    {
        $percentage = $this->getVat()->getPercentage();

        return $this->getPrice() * (1 + ($percentage / 100));
    }

    /**
     * Get vat
     * @deprecated
     * @return Vat
     * @Rule\MappedProperty(property="vat")
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set vat
     *
     * @param Vat $vat
     *
     * @return $this
     */
    public function setVat(Vat $vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get ledger
     *
     * @return Ledger
     */
    public function getLedger()
    {
        return $this->ledger;
    }

    /**
     * Set ledger
     *
     * @param Ledger $ledger
     *
     * @return $this
     */
    public function setLedger(Ledger $ledger)
    {
        $this->ledger = $ledger;

        return $this;
    }

    /**
     * Set discountAmount
     *
     * @param float $discountAmount
     *
     * @return $this
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float
     * @Rule\MappedProperty(property="discountAmount")
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Get discountAmount including vat
     *
     * @return float
     * @deprecated
     */
    public function getDiscountAmountIncl()
    {
        return $this->getDiscountAmount();
    }

    /**
     * Set parent
     *
     * @param OrderLine $parent
     *
     * @return $this
     */
    public function setParent(OrderLine $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return OrderLine
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get parent
     *
     * @return bool
     */
    public function hasParent()
    {
        return (bool) $this->parent;
    }

    /**
     * Add child
     *
     * @param OrderLine $child
     *
     * @return $this
     */
    public function addChild(OrderLine $child)
    {
        if(!$this->children->contains($child)) {
            $child->setParent($this);

            $this->children->add($child);
        }

        return $this;
    }

    /**
     * Get order
     *
     * @deprecated
     * @return Order
     */
    public function getOrderOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param Order $order
     * @deprecated
     *
     * @return $this
     */
    public function setOrderOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return OrderLine
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Remove child
     *
     * @param OrderLine $child
     */
    public function removeChild(OrderLine $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection|OrderLine[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getProcessStatus()
    {
        if ($this->getOrder()->getSupplierOrder()=== null) {
            return '';
        }

        $supplier = $this->getOrder()->getSupplierOrder()->getSupplier();

        if ($supplier && \in_array($supplier->getSupplierConnector(), ['Bakker', 'Bakkermail'])) {
            $process = $this->getProcess();

            if ($process === null) {
                return '';
            }

            $message = '';

            if (\in_array($process->status, ['DELIVERED', 'DELIVEREDBB'])) {
                $icon = 'fa fa-truck';
                $color = '#81C784';
            } elseif ($process->status === 'DeliveryFailed') {
                $icon = 'fa fa-truck';
                $color = '#ff0000';
            } elseif ($process->processed) {
                $icon = 'glyphicon glyphicon-ok';
                $color = '#81C784';
            } elseif ($process->status === 'Error') {
                $icon = 'glyphicon glyphicon-remove';
                $color = '#e7e7e7';
            } else {
                $icon = 'glyphicon glyphicon-ok';
                $color = '#e7e7e7';
            }

            if ($icon !== null && $color !== null) {
                $message = '<i class="' . $icon . '" data-popup="tooltip" data-placement="left" data-original-title="' . $process->status . '" style="color: ' . $color . ';"></i>';
            }

            return $message;
        }

        return '';
    }

    /**
     * @return null|\stdClass
     * @throws Exception
     */
    public function getProcess()
    {
        global $kernel;

        $order = $this->getOrder();
        $orderCollection = $order->getOrderCollection();

        $referenceNumber = $orderCollection->getNumber() . $order->getNumber();
        $key = $order->getNumber() - 1;

        $url = $kernel->getContainer()->getParameter('connector_bakker_api_url');

        try {
            $process = @file_get_contents("{$url}/tgapi/export/delivery-status/{$referenceNumber}");

            if ($process === false) {
                return (object)['processed' => '0', 'status' => 'Error'];
            }

            $process = json_decode($process);

            // Occurs on development where connector_bakker_api_url hasn't been configured
            if ($process === null) {
                return null;
            }

            if (array_key_exists($key, $process)) {
                return $process[$key];
            }

            return null;

        } catch (\Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Set cartLine
     *
     * @param CartOrderLine $cartLine
     *
     * @return OrderLine
     */
    public function setCartLine(CartOrderLine $cartLine = null)
    {
        $this->cartLine = $cartLine;

        return $this;
    }

    /**
     * Get cartLine
     *
     * @return CartOrderLine
     */
    public function getCartLine()
    {
        return $this->cartLine;
    }

    /**
     * @return null|string
     */
    public function getCardText(): ?string
    {
        $product = $this->getProduct();
        $metadata = $this->getMetadata();

        if (null === $product || !$product->isCard() || !isset($metadata['text'])) {
            return null;
        }

        return $metadata['text'];
    }

    /**
     * Set discountType
     *
     * @param string $discountType
     *
     * @return OrderLine
     */
    public function setDiscountType( $discountType)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType
     *
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * @return OrderLineVat[]|Collection
     */
    public function getVatLines(): Collection
    {
        return $this->vatLines;
    }

    /**
     * @param OrderLineVat $vatLine
     * @return OrderLine
     */
    public function addVatLine(OrderLineVat $vatLine): OrderLine
    {
        if(!$this->vatLines->contains($vatLine)) {
            $this->vatLines->add($vatLine);
        }

        return $this;
    }

    /**
     * @param OrderLineVat[] $vatLines
     * @return OrderLine
     */
    public function setVatLines(array $vatLines): OrderLine
    {
        $this->vatLines = new ArrayCollection();

        foreach ($vatLines as $vatLine) {
            $this->addVatLine($vatLine);
        }

        return $this;
    }

    /**
     * @return float|int
     */
    public function getVatAmount()
    {
        $total = 0;
        foreach($this->vatLines as $vatLine) {
            $total += $vatLine->getAmount();
        }

        return $total;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return bool
     */
    public function isPersonalizationLine()
    {
        return ($this->getMetadata() && !empty($this->getMetadata()['designer']));
    }
}
