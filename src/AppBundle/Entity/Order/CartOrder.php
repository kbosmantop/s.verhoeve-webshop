<?php

namespace AppBundle\Entity\Order;

use AppBundle\Annotation\GeneralListener;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Interfaces\CartOrderInterface;
use AppBundle\Interfaces\Sales\OrderInterface;
use AppBundle\Model\CartOrderModel;
use AppBundle\Traits\UuidEntity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @GeneralListener(parent="Cart", parentGetter="getCart", get="updatedAt", set="updatedAt")
 * @Rule\Entity(description="Conceptorder")
 */
class CartOrder extends CartOrderModel implements CartOrderInterface, OrderInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\Order", cascade={"persist"}, inversedBy="cartOrder", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Cart", inversedBy="orders", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Rule\Property(description="Winkelwagen", autoComplete={"id"="cart.id", "label"="cart.id"}, relation="parent")
     */
    protected $cart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(message="delivery_date.not_blank", groups={"cart_delivery_date"})
     * @Rule\Property(description="Bezorgdatum")
     */
    protected $deliveryDate;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\DeliveryAddressType")
     * @Assert\NotBlank(message="delivery_address_type.not_blank", groups={"cart_delivery"})
     */
    protected $deliveryAddressType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $deliveryAddress;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     * @Assert\NotBlank(message="pickup_address.not_blank", groups={"cart_pickup"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $pickupAddress;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $deliveryAddressCompanyName;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $deliveryAddressAttn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="address.street.not_blank", groups={"cart_delivery"})
     */
    protected $deliveryAddressStreet;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\NotBlank(message="address.number.not_blank", groups={"cart_delivery"})
     */
    protected $deliveryAddressNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $deliveryAddressNumberAddition;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\NotBlank(message="address.postcode.not_blank", groups={"cart_delivery"})
     */
    protected $deliveryAddressPostcode;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="address.city.not_blank", groups={"cart_delivery"})
     */
    protected $deliveryAddressCity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="delivery_address_country", referencedColumnName="id", nullable=true)
     * @Assert\Valid()
     */
    protected $deliveryAddressCountry;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    protected $deliveryAddressPhoneNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $supplierRemark;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $deliveryRemark;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\CartOrderLine", mappedBy="cartOrder", cascade={"persist",
     *                                                               "remove"})
     * @Rule\Property(description="Winkelwagen Orderregels", autoComplete={"id"="line.id", "label"="line.id"}, relation="child")
     */
    protected $lines;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Totaalprijs (excl. BTW)")
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Totaalprijs (incl. BTW)")
     */
    private $totalPriceIncl;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     * @Assert\Regex("/^[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}$/")
     */
    protected $timeslot;

    /**
     * Clone CartOrder
     */
    public function __clone()
    {
        $this->id = null;
        $this->uuid = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supplierRemark
     *
     * @param string $supplierRemark
     *
     * @return CartOrder
     */
    public function setSupplierRemark($supplierRemark)
    {
        $this->supplierRemark = $supplierRemark;

        return $this;
    }

    /**
     * Get supplierRemark
     *
     * @return string
     */
    public function getSupplierRemark()
    {
        return $this->supplierRemark;
    }

    /**
     * Set deliveryRemark
     *
     * @param string $deliveryRemark
     *
     * @return CartOrder
     */
    public function setDeliveryRemark($deliveryRemark)
    {
        $this->deliveryRemark = $deliveryRemark;

        return $this;
    }

    /**
     * Get deliveryRemark
     *
     * @return string
     */
    public function getDeliveryRemark()
    {
        return $this->deliveryRemark;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     *
     * @return CartOrder
     */
    public function setCart(Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @return Cart
     * @Rule\MappedProperty(property="cart")
     */
    public function getCollection()
    {
        return $this->cart;
    }

    /**
     * Set deliveryAddress
     *
     * @param Address $deliveryAddress
     *
     * @return CartOrder
     */
    public function setDeliveryAddress(Address $deliveryAddress = null)
    {
        if ($deliveryAddress && $this->deliveryAddress !== $deliveryAddress) {
            $this->deliveryAddressCompanyName = $deliveryAddress->getCompanyName();
            $this->deliveryAddressAttn = $deliveryAddress->getAttn();
            $this->deliveryAddressStreet = $deliveryAddress->getStreet();
            $this->deliveryAddressNumber = $deliveryAddress->getNumber();
            $this->deliveryAddressNumberAddition = $deliveryAddress->getNumberAddition();
            $this->deliveryAddressPostcode = $deliveryAddress->getPostcode();
            $this->deliveryAddressCity = $deliveryAddress->getCity();
            $this->deliveryAddressCountry = $deliveryAddress->getCountry();
            $this->deliveryAddressPhoneNumber = $deliveryAddress->getPhoneNumber();
        }

        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return Address
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lines = new ArrayCollection();
    }

    /**
     * Add line
     *
     * @param CartOrderLine $line
     *
     * @return CartOrder
     */
    public function addLine(CartOrderLine $line)
    {
        if (false === $this->lines->contains($line)) {
            $line->setCartOrder($this);

            $this->lines[] = $line;
        }

        return $this;
    }

    /**
     * Remove line
     *
     * @param CartOrderLine $line
     * @return CartOrder
     */
    public function removeLine(CartOrderLine $line)
    {
        if (true === $this->lines->contains($line)) {
            $this->lines->removeElement($line);
        }

        return $this;
    }

    /**
     * Get lines
     *
     * @return Collection|CartOrderLine[]
     * @Rule\MappedProperty(property="lines")
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * Set deliveryAddressCompanyName
     *
     * @param string $deliveryAddressCompanyName
     *
     * @return CartOrder
     */
    public function setDeliveryAddressCompanyName($deliveryAddressCompanyName)
    {
        $this->deliveryAddressCompanyName = $deliveryAddressCompanyName;

        return $this;
    }

    /**
     * Get deliveryAddressCompanyName
     *
     * @return string
     */
    public function getDeliveryAddressCompanyName()
    {
        return $this->deliveryAddressCompanyName;
    }

    /**
     * Set deliveryAddressAttn
     *
     * @param string $deliveryAddressAttn
     *
     * @return CartOrder
     */
    public function setDeliveryAddressAttn($deliveryAddressAttn)
    {
        $this->deliveryAddressAttn = $deliveryAddressAttn;

        return $this;
    }

    /**
     * Get deliveryAddressAttn
     *
     * @return string
     */
    public function getDeliveryAddressAttn()
    {
        return $this->deliveryAddressAttn;
    }

    /**
     * Set deliveryAddressStreet
     *
     * @param string $deliveryAddressStreet
     *
     * @return CartOrder
     */
    public function setDeliveryAddressStreet($deliveryAddressStreet)
    {
        $this->deliveryAddressStreet = $deliveryAddressStreet;

        return $this;
    }

    /**
     * Get deliveryAddressStreet
     *
     * @return string
     */
    public function getDeliveryAddressStreet()
    {
        return $this->deliveryAddressStreet;
    }

    /**
     * Set deliveryAddressNumber
     *
     * @param string $deliveryAddressNumber
     *
     * @return CartOrder
     */
    public function setDeliveryAddressNumber($deliveryAddressNumber)
    {
        $this->deliveryAddressNumber = $deliveryAddressNumber;

        return $this;
    }

    /**
     * Get deliveryAddressNumber
     *
     * @return string
     */
    public function getDeliveryAddressNumber()
    {
        return $this->deliveryAddressNumber;
    }

    /**
     * Set deliveryAddressPostcode
     *
     * @param string $deliveryAddressPostcode
     *
     * @return CartOrder
     */
    public function setDeliveryAddressPostcode($deliveryAddressPostcode)
    {
        $this->deliveryAddressPostcode = $deliveryAddressPostcode;

        return $this;
    }

    /**
     * Get deliveryAddressPostcode
     *
     * @return string
     */
    public function getDeliveryAddressPostcode()
    {
        return $this->deliveryAddressPostcode;
    }

    /**
     * Set deliveryAddressCity
     *
     * @param string $deliveryAddressCity
     *
     * @return CartOrder
     */
    public function setDeliveryAddressCity($deliveryAddressCity)
    {
        $this->deliveryAddressCity = $deliveryAddressCity;

        return $this;
    }

    /**
     * Get deliveryAddressCity
     *
     * @return string
     */
    public function getDeliveryAddressCity()
    {
        return $this->deliveryAddressCity;
    }

    /**
     * Set deliveryAddressPhoneNumber
     *
     * @param string $deliveryAddressPhoneNumber
     *
     * @return CartOrder
     */
    public function setDeliveryAddressPhoneNumber($deliveryAddressPhoneNumber)
    {
        $this->deliveryAddressPhoneNumber = $deliveryAddressPhoneNumber;

        return $this;
    }

    /**
     * Get deliveryAddressPhoneNumber
     *
     * @return string
     */
    public function getDeliveryAddressPhoneNumber()
    {
        return $this->deliveryAddressPhoneNumber;
    }

    /**
     * Set deliveryAddressCountry
     *
     * @param Country $deliveryAddressCountry
     *
     * @return CartOrder
     */
    public function setDeliveryAddressCountry(Country $deliveryAddressCountry)
    {
        $this->deliveryAddressCountry = $deliveryAddressCountry;

        return $this;
    }

    /**
     * Get deliveryAddressCountry
     *
     * @return Country
     */
    public function getDeliveryAddressCountry()
    {
        return $this->deliveryAddressCountry;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     *
     * @return CartOrder
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     * @Rule\MappedProperty(property="deliveryDate")
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set totalPriceIncl
     *
     * @param float $totalPriceIncl
     *
     * @return CartOrder
     */
    public function setTotalPriceIncl($totalPriceIncl)
    {
        $this->totalPriceIncl = $totalPriceIncl;

        return $this;
    }

    /**
     * Get totalPriceIncl
     *
     * @return float
     * @Rule\MappedProperty(property="deliveryDate")
     */
    public function getTotalPriceIncl()
    {
        return $this->totalPriceIncl;
    }

    /**
     * Set deliveryDate
     *
     * @param DateTime|null $deliveryDate
     *
     * @return CartOrder
     */
    public function setDeliveryDate(DateTime $deliveryDate = null)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return DateTime
     * @Rule\MappedProperty(property="deliveryDate")
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return CartOrder
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set deliveryAddressType
     *
     * @param DeliveryAddressType $deliveryAddressType
     *
     * @return CartOrder
     */
    public function setDeliveryAddressType(DeliveryAddressType $deliveryAddressType = null)
    {
        $this->deliveryAddressType = $deliveryAddressType;

        return $this;
    }

    /**
     * Get deliveryAddressType
     *
     * @return DeliveryAddressType
     */
    public function getDeliveryAddressType()
    {
        return $this->deliveryAddressType;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return CartOrder
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set pickupAddress
     *
     * @param Address $pickupAddress
     *
     * @return CartOrder
     */
    public function setPickupAddress(Address $pickupAddress = null)
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * Get pickupAddress
     *
     * @return Address
     */
    public function getPickupAddress()
    {
        return $this->pickupAddress;
    }

    /**
     * Set deliveryAddressNumberAddition
     *
     * @param string $deliveryAddressNumberAddition
     *
     * @return CartOrder
     */
    public function setDeliveryAddressNumberAddition($deliveryAddressNumberAddition)
    {
        $this->deliveryAddressNumberAddition = $deliveryAddressNumberAddition;

        return $this;
    }

    /**
     * Get deliveryAddressNumberAddition
     *
     * @return string
     */
    public function getDeliveryAddressNumberAddition()
    {
        return $this->deliveryAddressNumberAddition;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(): float
    {
        $discountAmount = 0;
        foreach($this->getLines() as $line) {
            $discountAmount += $line->getDiscountAmount();
        }

        return $discountAmount;
    }

    /**
     * @param string $timeslot
     * @return CartOrder
     */
    public function setTimeslot(string $timeslot): CartOrder
    {
        $this->timeslot = $timeslot;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTimeslot(): ?string
    {
        return $this->timeslot;
    }
}
