<?php

namespace AppBundle\Entity\Order;

use AppBundle\ThirdParty\Exact\Entity\Invoice;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class BillingItemGroup
 * @package AppBundle\Entity\Order
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *          name="colection_sequence_unique",
 *          columns={"order_collection_id", "sequence"}
 *     )
 * })
 */
class BillingItemGroup
{
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var OrderCollection
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="billingItemGroups")
     */
    protected $orderCollection;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $sequence;

    /**
     * @var Collection|BillingItem[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\BillingItem", mappedBy="billingItemGroup",
     *                                                                   cascade={"persist", "remove"})
     */
    protected $billingItems;

    /**
     * @var Invoice|null
     * @ORM\ManyToOne(targetEntity="AppBundle\ThirdParty\Exact\Entity\Invoice", inversedBy="billingItemGroups")
     */
    protected $invoice;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return BillingItemGroup
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return BillingItemGroup
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->billingItems = new ArrayCollection();
    }

    /**
     * Add billingItem
     *
     * @param BillingItem $billingItem
     *
     * @return BillingItemGroup
     */
    public function addBillingItem(BillingItem $billingItem)
    {
        if (!$this->billingItems->contains($billingItem)) {
            $this->billingItems[] = $billingItem;
            $billingItem->setBillingItemGroup($this);
        }

        return $this;
    }

    /**
     * Remove billingItem
     *
     * @param BillingItem $billingItem
     */
    public function removeBillingItem(BillingItem $billingItem)
    {
        if ($this->billingItems->contains($billingItem)) {
            $this->billingItems->removeElement($billingItem);
        }
    }

    /**
     * Get billingItems
     *
     * @return Collection
     */
    public function getBillingItems()
    {
        return $this->billingItems;
    }

    /**
     * @return Invoice|null
     */
    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }

    /**
     * @param Invoice|null $invoice
     */
    public function setInvoice(?Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }
}
