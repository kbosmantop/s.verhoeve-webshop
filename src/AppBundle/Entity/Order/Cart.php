<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\PayableInterface;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Model\CartModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"AppBundle\EventListener\Order\CartListener"})
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Rule\Entity(description="Winkelwagen")
 */
class Cart extends CartModel implements OrderCollectionInterface, PayableInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Uuid
     * @ORM\Column(type="uuid", unique=true, nullable=true)
     */
    protected $uuid;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="cart")
     * @ORM\JoinColumn(nullable=true)
     * @Rule\Property(description="orderCollection")
     */
    protected $orderCollection;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     * @ORM\JoinColumn(nullable=true)
     *
     * @Rule\Property(description="Bedrijf", autoComplete={ "id"="company.id", "label"="company.name" },
     *                                       relation="child")
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid()
     */
    protected $customer;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $sessionId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\CartStatus")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $invoiceAddress;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $invoiceAddressCompanyName;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $invoiceAddressAttn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $invoiceAddressStreet;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $invoiceAddressNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $invoiceAddressNumberAddition;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $invoiceAddressPostcode;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $invoiceAddressCity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="delivery_address_country", referencedColumnName="id", nullable=true)
     * @Assert\Valid()
     */
    protected $invoiceAddressCountry;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    protected $invoiceAddressPhoneNumber;

    /**
     * @ORM\Column(type="string", length=50, nullable=true);
     */
    protected $ip;

    /**
     * @ORM\Column(type="text", nullable=true);
     */
    protected $userAgent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\CartOrder", mappedBy="cart", cascade={"persist"})
     * @Rule\Property(description="Winkelwagen Orders", autoComplete={"id"="order.id", "label"="order.number"},
     *                                         relation="child")
     */
    protected $orders;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Totaal (excl. BTW)")
     */
    protected $totalPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Totaal (incl. BTW)")
     */
    protected $totalPriceIncl;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Rule\Property(description="Tijd / Datum")
     */
    protected $expiredAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\CartLine", mappedBy="cart", cascade={"persist"})
     */
    protected $lines;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Discount\Voucher", mappedBy="carts")
     */
    protected $vouchers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->vouchers = new ArrayCollection();

        if (!$this->uuid) {
            $this->uuid = (string)Uuid::uuid4();
        }
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @ORM\PreRemove
     */
    public function calculateTotals()
    {
        $this->totalPrice = 0;
        $this->totalPriceIncl = 0;

        foreach ($this->getOrders() as $order) {
            $this->totalPrice += $order->getTotalPrice();
            $this->totalPriceIncl += $order->getTotalPriceIncl();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     *
     * @return Cart
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Cart
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     *
     * @return Cart
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Add order
     *
     * @param CartOrder $order
     *
     * @return Cart
     */
    public function addOrder(CartOrder $order)
    {
        $order->setCart($this);

        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param CartOrder $order
     */
    public function removeOrder(CartOrder $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return Collection
     * @Rule\MappedProperty(property="orders")
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set status
     *
     * @param CartStatus $status
     *
     * @return Cart
     */
    public function setStatus(CartStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return CartStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     * @param bool     $setCompany
     *
     * @return Cart
     */
    public function setCustomer(Customer $customer = null, $setCompany = false)
    {
        $this->customer = $customer;

        if ($setCompany) {
            $this->setCompany($customer->getCompany());
        }

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     * @Rule\MappedProperty(property="customer")
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Cart
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     * @Rule\MappedProperty(property="company")
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return Cart
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     * @Rule\MappedProperty(property="site")
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set invoiceAddressCompanyName
     *
     * @param string $invoiceAddressCompanyName
     *
     * @return Cart
     */
    public function setInvoiceAddressCompanyName($invoiceAddressCompanyName)
    {
        $this->invoiceAddressCompanyName = $invoiceAddressCompanyName;

        return $this;
    }

    /**
     * Get invoiceAddressCompanyName
     *
     * @return string
     */
    public function getInvoiceAddressCompanyName()
    {
        return $this->invoiceAddressCompanyName;
    }

    /**
     * Set invoiceAddressAttn
     *
     * @param string $invoiceAddressAttn
     *
     * @return Cart
     */
    public function setInvoiceAddressAttn($invoiceAddressAttn)
    {
        $this->invoiceAddressAttn = $invoiceAddressAttn;

        return $this;
    }

    /**
     * Get invoiceAddressAttn
     *
     * @return string
     */
    public function getInvoiceAddressAttn()
    {
        return $this->invoiceAddressAttn;
    }

    /**
     * Set invoiceAddressStreet
     *
     * @param string $invoiceAddressStreet
     *
     * @return Cart
     */
    public function setInvoiceAddressStreet($invoiceAddressStreet)
    {
        $this->invoiceAddressStreet = $invoiceAddressStreet;

        return $this;
    }

    /**
     * Get invoiceAddressStreet
     *
     * @return string
     */
    public function getInvoiceAddressStreet()
    {
        return $this->invoiceAddressStreet;
    }

    /**
     * Set invoiceAddressNumber
     *
     * @param string $invoiceAddressNumber
     *
     * @return Cart
     */
    public function setInvoiceAddressNumber($invoiceAddressNumber)
    {
        $this->invoiceAddressNumber = $invoiceAddressNumber;

        return $this;
    }

    /**
     * Get invoiceAddressNumber
     *
     * @return string
     */
    public function getInvoiceAddressNumber()
    {
        return $this->invoiceAddressNumber;
    }

    /**
     * Set invoiceAddressPostcode
     *
     * @param string $invoiceAddressPostcode
     *
     * @return Cart
     */
    public function setInvoiceAddressPostcode($invoiceAddressPostcode)
    {
        $this->invoiceAddressPostcode = $invoiceAddressPostcode;

        return $this;
    }

    /**
     * Get invoiceAddressPostcode
     *
     * @return string
     */
    public function getInvoiceAddressPostcode()
    {
        return $this->invoiceAddressPostcode;
    }

    /**
     * Set invoiceAddressCity
     *
     * @param string $invoiceAddressCity
     *
     * @return Cart
     */
    public function setInvoiceAddressCity($invoiceAddressCity)
    {
        $this->invoiceAddressCity = $invoiceAddressCity;

        return $this;
    }

    /**
     * Get invoiceAddressCity
     *
     * @return string
     */
    public function getInvoiceAddressCity()
    {
        return $this->invoiceAddressCity;
    }

    /**
     * Set invoiceAddressPhoneNumber
     *
     * @param string $invoiceAddressPhoneNumber
     *
     * @return Cart
     */
    public function setInvoiceAddressPhoneNumber($invoiceAddressPhoneNumber)
    {
        $this->invoiceAddressPhoneNumber = $invoiceAddressPhoneNumber;

        return $this;
    }

    /**
     * Get invoiceAddressPhoneNumber
     *
     * @return string
     */
    public function getInvoiceAddressPhoneNumber()
    {
        return $this->invoiceAddressPhoneNumber;
    }

    /**
     * Set invoiceAddress
     *
     * @param Address $invoiceAddress
     *
     * @return Cart
     */
    public function setInvoiceAddress(Address $invoiceAddress = null)
    {
        if ($invoiceAddress && $this->invoiceAddress != $invoiceAddress) {
            $this->invoiceAddressCompanyName = $invoiceAddress->getCompanyName();
            $this->invoiceAddressStreet = $invoiceAddress->getStreet();
            $this->invoiceAddressNumber = $invoiceAddress->getNumber();
            $this->invoiceAddressNumberAddition = $invoiceAddress->getNumberAddition();
            $this->invoiceAddressPostcode = $invoiceAddress->getPostcode();
            $this->invoiceAddressCity = $invoiceAddress->getCity();
            $this->invoiceAddressCountry = $invoiceAddress->getCountry();
            $this->invoiceAddressPhoneNumber = $invoiceAddress->getPhoneNumber();
        }

        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * Get invoiceAddress
     *
     * @return Address
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * Set invoiceAddressCountry
     *
     * @param Country $invoiceAddressCountry
     *
     * @return Cart
     */
    public function setInvoiceAddressCountry(Country $invoiceAddressCountry)
    {
        $this->invoiceAddressCountry = $invoiceAddressCountry;

        return $this;
    }

    /**
     * Get invoiceAddressCountry
     *
     * @return Country
     */
    public function getInvoiceAddressCountry()
    {
        return $this->invoiceAddressCountry;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     *
     * @return Cart
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set totalPriceIncl
     *
     * @param float $totalPriceIncl
     *
     * @return Cart
     */
    public function setTotalPriceIncl($totalPriceIncl)
    {
        $this->totalPriceIncl = $totalPriceIncl;

        return $this;
    }

    /**
     * Get totalPriceIncl
     *
     * @return float
     */
    public function getTotalPriceIncl()
    {
        return $this->totalPriceIncl;
    }

    /**
     * Set order
     *
     * @param \AppBundle\Entity\Order\OrderCollection|null $orderCollection
     *
     * @return Cart
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $orderCollection->setCart($this);

        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get order
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set order
     *
     * @param OrderCollection $orderCollection
     *
     * @return Cart
     */
    public function setOrder(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get order
     *
     * @return OrderCollection
     */
    public function getOrder()
    {
        return $this->orderCollection;
    }

    /**
     * Set uuid
     *
     * @param uuid $uuid
     *
     * @return Cart
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return Uuid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     *
     * @return Cart
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Add line
     *
     * @param CartLine $line
     *
     * @return Cart
     */
    public function addLine(CartLine $line)
    {
        $line->setCart($this);

        $this->lines[] = $line;

        return $this;
    }

    /**
     * Remove line
     *
     * @param CartLine $line
     */
    public function removeLine(CartLine $line)
    {
        $this->lines->removeElement($line);
    }

    /**
     * Get lines
     *
     * @return Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->getTotalPriceIncl();
    }

    /**
     * @return string
     */
    public function getPaymentDescription()
    {
        return "";
    }

    /**
     * @return PaymentStatus|null
     */
    public function getPaymentStatus()
    {
        return null;
    }

    /**
     * @return ArrayCollection|Payment[]
     */
    public function getPayments()
    {
        return new ArrayCollection();
    }

    /**
     * Set invoiceAddressNumberAddition
     *
     * @param string $invoiceAddressNumberAddition
     *
     * @return Cart
     */
    public function setInvoiceAddressNumberAddition($invoiceAddressNumberAddition)
    {
        $this->invoiceAddressNumberAddition = $invoiceAddressNumberAddition;

        return $this;
    }

    /**
     * Get invoiceAddressNumberAddition
     *
     * @return string
     */
    public function getInvoiceAddressNumberAddition()
    {
        return $this->invoiceAddressNumberAddition;
    }

    /**
     * @param Voucher $voucher
     * @return Cart
     */
    public function addVoucher(Voucher $voucher)
    {
        if (!$this->vouchers->contains($voucher)) {
            $this->vouchers->add($voucher);
            $voucher->addCart($this);
        }

        return $this;
    }

    /**
     * @param Voucher $voucher
     * @return Cart
     */
    public function removeVoucher(Voucher $voucher)
    {
        if ($this->vouchers->contains($voucher)) {
            $this->vouchers->removeElement($voucher);
        }

        $voucher->removeCart($this);

        return $this;
    }

    /**
     * @return Voucher[]|ArrayCollection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(): float
    {
        $discountAmount = 0;

        /** @var CartOrder $order */
        foreach ($this->getOrders() as $order) {
            $discountAmount += $order->getDiscountAmount();
        }

        return $discountAmount;
    }
}
