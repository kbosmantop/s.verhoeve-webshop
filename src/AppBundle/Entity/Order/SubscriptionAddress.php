<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Relation\Address;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @link http://future500.nl/articles/2013/09/doctrine-2-how-to-handle-join-tables-with-extra-columns/
 */

/**
 * @ORM\Entity
 */
class SubscriptionAddress
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var Integer $id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Subscription", inversedBy="subscriptionAddresses")
     */
    protected $subscription;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     */
    protected $address;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $deliveryCosts;

    /**
     * @ORM\Column(type="text")
     */
    protected $deliveryRemark;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscription
     *
     * @param Subscription $subscription
     *
     * @return SubscriptionAddress
     */
    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return SubscriptionAddress
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set deliveryCosts
     *
     * @param float $deliveryCosts
     *
     * @return SubscriptionAddress
     */
    public function setDeliveryCosts($deliveryCosts)
    {
        $this->deliveryCosts = $deliveryCosts;

        return $this;
    }

    /**
     * Get deliveryCosts
     *
     * @return float
     */
    public function getDeliveryCosts()
    {
        return $this->deliveryCosts;
    }

    /**
     * Set deliveryRemark
     *
     * @param string $deliveryRemark
     *
     * @return SubscriptionAddress
     */
    public function setDeliveryRemark($deliveryRemark)
    {
        $this->deliveryRemark = $deliveryRemark;

        return $this;
    }

    /**
     * Get deliveryRemark
     *
     * @return string
     */
    public function getDeliveryRemark()
    {
        return $this->deliveryRemark;
    }
}
