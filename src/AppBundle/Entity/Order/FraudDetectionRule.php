<?php

namespace AppBundle\Entity\Order;

use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RuleBundle\Entity\Rule;

/**
 * @ORM\Entity()
 *
 * Class FraudDetectionRule
 * @package AppBundle\Entity\Fraud
 */
class FraudDetectionRule
{

    use PublishableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     * @ORM\JoinColumn(name="rule_id", referencedColumnName="id")
     */
    protected $rule;

    /**
     * @ORM\Column(type="integer")
     */
    protected $score;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return FraudDetectionRule
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set rule
     *
     * @param Rule $rule
     *
     * @return FraudDetectionRule
     */
    public function setRule(Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
}
