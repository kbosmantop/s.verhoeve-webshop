<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Interfaces\Order\LineInterface;
use AppBundle\Interfaces\Order\VatLineInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class OrderCollectionLineVat
 * @package AppBundle\Entity\Order
 */
class OrderCollectionLineVat implements VatLineInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var OrderCollectionLine
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollectionLine", inversedBy="vatLines")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $orderCollectionLine;

    /**
     * @var VatRate
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatRate")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $vatRate;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $amount;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return VatRate
     */
    public function getVatRate(): VatRate
    {
        return $this->vatRate;
    }

    /**
     * @param VatRate $vatRate
     * @return OrderCollectionLineVat
     */
    public function setVatRate(VatRate $vatRate): OrderCollectionLineVat
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return OrderCollectionLineVat
     */
    public function setAmount(float $amount): OrderCollectionLineVat
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return OrderCollectionLine
     */
    public function getOrderCollectionLine(): OrderCollectionLine
    {
        return $this->orderCollectionLine;
    }

    /**
     * @param OrderCollectionLine $orderCollectionLine
     */
    public function setOrderCollectionLine(OrderCollectionLine $orderCollectionLine): void
    {
        $this->orderCollectionLine = $orderCollectionLine;
    }

    /**
     * @return LineInterface
     */
    public function getLine(): LineInterface
    {
        return $this->orderCollectionLine;
    }
}