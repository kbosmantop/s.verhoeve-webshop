<?php

namespace AppBundle\Entity\Payment;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Geography\Country;
use AppBundle\Interfaces\GatewayInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 * @ORM\Table(name="paymentmethod")
 *
 * @method PaymentmethodTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 * @Rule\Entity(description="Betaalmethode")
 */
class Paymentmethod
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\PaymentGatewayType")
     * @ORM\Column(type="PaymentGatewayType", nullable=true)
     */
    protected $gateway;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="paymentmethod_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="productgroup_id", referencedColumnName="id")}
     * )
     */
    private $productgroups;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="paymentmethod_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country", referencedColumnName="id")}
     * )
     */
    private $countries;

    /**
     * @ORM\Column(type="string", length=30)
     * @Rule\Property(description="Code")
     */
    private $code;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\PaymentmethodType")
     * @Gedmo\SortableGroup
     */
    private $paymentmethodType;

    /**
     * @ORM\ManyToone(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    private $product;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Maximum toegestaan bedrag")
     */
    private $maxAmount = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Paymentmethod
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productgroups = new ArrayCollection();
        $this->countries = new ArrayCollection();
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Paymentmethod
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add productgroup
     *
     * @param ProductGroup $productgroup
     *
     * @return Paymentmethod
     */
    public function addProductgroup(ProductGroup $productgroup)
    {
        $this->productgroups[] = $productgroup;

        return $this;
    }

    /**
     * Remove productgroup
     *
     * @param ProductGroup $productgroup
     */
    public function removeProductgroup(ProductGroup $productgroup)
    {
        $this->productgroups->removeElement($productgroup);
    }

    /**
     * Get productgroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductgroups()
    {
        return $this->productgroups;
    }

    /**
     * Set paymentmethodType
     *
     * @param PaymentmethodType $paymentmethodType
     *
     * @return Paymentmethod
     */
    public function setPaymentmethodType(PaymentmethodType $paymentmethodType = null)
    {
        $this->paymentmethodType = $paymentmethodType;

        return $this;
    }

    /**
     * Get paymentmethodType
     *
     * @return PaymentmethodType
     */
    public function getPaymentmethodType()
    {
        return $this->paymentmethodType;
    }

    /**
     * Add country
     *
     * @param Country $country
     *
     * @return Paymentmethod
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set gateway
     *
     * @param GatewayInterface $gateway
     *
     * @return Paymentmethod
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return GatewayInterface
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return Paymentmethod
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set maxAmount
     *
     * @param float $maxAmount
     *
     * @return Paymentmethod
     */
    public function setMaxAmount($maxAmount)
    {
        $this->maxAmount = $maxAmount;

        return $this;
    }

    /**
     * Get maxAmount
     *
     * @return float
     */
    public function getMaxAmount()
    {
        return $this->maxAmount;
    }
}
