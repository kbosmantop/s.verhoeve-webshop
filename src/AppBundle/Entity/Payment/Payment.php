<?php

namespace AppBundle\Entity\Payment;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Utils\Adyen\FraudCheckResult;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Ramsey\Uuid\Uuid;
use RuleBundle\Annotation as Rule;
use FS\SolrBundle\Doctrine\Annotation as Solr;

/**
 * @Solr\Document(index="payment")
 * @ORM\Entity
 * @ORM\EntityListeners({"AppBundle\EventListener\Payment\PaymentListener"})
 * @ORM\HasLifecycleCallbacks
 * @Rule\Entity(description="Betaling")
 */
class Payment
{
    /**
     * @Solr\Id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    protected $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\Payment")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $related;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\PaymentTypeType")
     * @ORM\Column(type="PaymentTypeType", nullable=true)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\Paymentmethod")
     * @Rule\Property(description="Betaalmethode", autoComplete={"id"="paymentmethod.id", "label"="paymentmethod.translate.description"}, relation="child")
     */
    protected $paymentmethod;

    /**
     * @Solr\Field(type="integer", getter="getId")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="payments", cascade={"persist"})
     */
    protected $orderCollection;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Voldaan bedrag")
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\PaymentStatus")
     * @Rule\Property(description="Betaalstatus", autoComplete={"id"="paymentstatus.id", "label"="paymentstatus.description"})
     */
    protected $status;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=40, nullable=true, unique=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="datetime")
     * @Rule\Property(description="Datum")
     */
    protected $datetime;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $lastErrors = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Rule\Property(description="Fraude score")
     */
    protected $fraudScore;

    public function __construct()
    {
        if (!$this->uuid) {
            $this->uuid = (string)Uuid::uuid4();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Order ' . $this->getOrderCollection()->getId();
    }

    /**
     * Get order collection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return Payment
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->getStatus()->getCountable();
    }

    /**
     * Get status
     *
     * @return PaymentStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param PaymentStatus $status
     *
     * @return Payment
     */
    public function setStatus(PaymentStatus $status = null)
    {
        $this->status = $status;

        if (null !== $status && $status->getId() === 'authorized') {
            $this->setLastErrors(null);
        }

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Set order
     *
     * @param OrderCollection $orderCollection
     *
     * @return Payment
     */
    public function setOrder(OrderCollection $orderCollection = null)
    {
        if (null !== $orderCollection) {
            $orderCollection->addPayment($this);
        }

        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get order
     *
     * @deprecated
     * @return OrderCollection
     */
    public function getOrder()
    {
        return $this->orderCollection;
    }

    /**
     * Get paymentmethod
     *
     * @return Paymentmethod
     */
    public function getPaymentmethod()
    {
        return $this->paymentmethod;
    }

    /**
     * Set paymentmethod
     *
     * @param Paymentmethod $paymentmethod
     *
     * @return Payment
     */
    public function setPaymentmethod(Paymentmethod $paymentmethod = null)
    {
        $this->paymentmethod = $paymentmethod;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Payment
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get related
     *
     * @return Payment
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set related
     *
     * @param Payment $related
     *
     * @return Payment
     */
    public function setRelated(Payment $related = null)
    {
        $this->related = $related;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Payment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Payment
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Payment
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get lastErrors
     *
     * @return array
     */
    public function getLastErrors()
    {
        return $this->lastErrors;
    }

    /**
     * Set lastErrors
     *
     * @param array $lastErrors
     *
     * @return Payment
     */
    public function setLastErrors($lastErrors)
    {
        $this->lastErrors = $lastErrors;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastErrorsAsString(): string
    {
        if (empty($this->lastErrors)) {
            return '';
        }

        return implode(PHP_EOL, array_unique(array_map(function($lastError) {
            return $lastError;
        }, $this->lastErrors)));
    }

    /**
     * Get uuid
     *
     * @return string|Uuid uuid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set uuid
     *
     * @param uuid $uuid
     *
     * @return Payment
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get fraudScore
     *
     * @return integer
     */
    public function getFraudScore()
    {
        return $this->fraudScore;
    }

    /**
     * Set fraudScore
     *
     * @param integer $fraudScore
     *
     * @return Payment
     */
    public function setFraudScore($fraudScore)
    {
        $this->fraudScore = $fraudScore;

        return $this;
    }

    /**
     * @return null|array
     */
    public function getFraudCheckResults()
    {
        if (!array_key_exists('fraudResult', $this->metadata)) {
            return null;
        }

        if (!array_key_exists('results', $this->metadata['fraudResult'])) {
            return null;
        }

        $fraudCheckResults = [];

        foreach ($this->metadata['fraudResult']['results'] as $fraudCheckResult) {
            if ($fraudCheckResult['FraudCheckResult']['accountScore'] === 0) {
                continue;
            }

            $fraudCheckResults[] = new FraudCheckResult($fraudCheckResult['FraudCheckResult']);
        }

        return $fraudCheckResults;
    }
}
