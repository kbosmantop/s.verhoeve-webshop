<?php

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 */
class BannerTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $textBlockContent;

    /**
     * Set textBlockContent
     *
     * @param string $textBlockContent
     *
     * @return BannerTranslation
     */
    public function setTextBlockContent($textBlockContent)
    {
        $this->textBlockContent = $textBlockContent;

        return $this;
    }

    /**
     * Get textBlockContent
     *
     * @return string
     */
    public function getTextBlockContent()
    {
        return $this->textBlockContent;
    }
}
