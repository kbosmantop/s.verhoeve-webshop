<?php

namespace AppBundle\Entity\Site;

use AppBundle\Interfaces\MenuSourceInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use AppBundle\Traits\AccessibleEntity;
use AppBundle\Traits\PublishableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(name="accessibleCustomerGroups",
 *          joinTable=@ORM\JoinTable(
 *              name="page_accessible_customer_group"
 *          )
 *      )
 * })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 *
 * @method PageTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class Page implements MenuSourceInterface, PreviewableEntityInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use PublishableEntity;
    use AccessibleEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="pages")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Banner", inversedBy="pages")
     */
    protected $banner;

    /**
     * @var string
     */
    private $renderedContent;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTitle();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->translate()->getTitle() . ' (' . $this->getSite() . ')';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set rendered content
     *
     * @param string $renderedContent
     *
     * @return Page
     */
    public function setRenderedContent($renderedContent): Page
    {
        $this->renderedContent = $renderedContent;

        return $this;
    }

    /**
     * Get rendered content
     *
     * @return string|null
     */
    public function getRenderedContent(): ?string
    {
        return $this->renderedContent;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return Page
     */
    public function setSite(Site $site = null): Page
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set banner
     *
     * @param Banner $banner
     *
     * @return Page
     */
    public function setBanner(Banner $banner = null): Page
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return Banner
     */
    public function getBanner(): ?Banner
    {
        return $this->banner;
    }

    /**
     *
     * @return array
     */
    public static function getMenuSourceConfig(): array
    {
        return [
            'labels' => [
                'description' => "Pagina's",
                'singular' => "Pagina",
            ],
        ];
    }

    /**
     * @return string
     */
    public function getMenuTitle(): ?string
    {
        return $this->translate()->getTitle();
    }

    /**
     * @return string
     */
    public function getMenuType(): string
    {
        return 'Page';
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return 'AppBundle\Controller\PageController::indexAction';
    }

    /**
     * @return string
     */
    public function getControllerParam(): string
    {
        return 'page';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accessibleCustomerGroups = new ArrayCollection();
    }

}
