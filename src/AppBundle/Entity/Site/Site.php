<?php

namespace AppBundle\Entity\Site;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use AppBundle\DBAL\Types\DeliveryDateMethodType;
use AppBundle\DBAL\Types\DisplayPriceType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Interfaces\Seo\SlugInterface;
use AppBundle\Model\Site\SiteModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @Rule\Entity(description="Site")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @method SiteTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class Site extends SiteModel implements SlugInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Page", mappedBy="site")
     */
    protected $pages;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Menu", mappedBy="site")
     */
    protected $menus;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Domain", mappedBy="site", cascade={"persist"})
     * @Rule\Property(description="Domein", autoComplete={"id"="domain.id", "label"="domain.domain"}, relation="child")
     */
    protected $domains;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $httpScheme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $httpPort;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", mappedBy="sites")
     */
    protected $assortments;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Geography\DeliveryAddressType", mappedBy="sites")
     */
    protected $deliveryAddressTypes;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     * @Rule\Property(description="Land", autoComplete={"id"="country.id", "label"="country.translate.name"},
     *                                    relation="child")
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="phone_number", nullable=true)
     */
    protected $phoneNumber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $homepage;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     */
    protected $homepageAssortment;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $homepageAbout;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $orderSuccessPage;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $customerServicePopupPage;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $termsAndConditionsPage;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    protected $paymentPendingPage;

    /**
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\SiteUsp", mappedBy="site", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Assert\Count(max="4", maxMessage="Er kunnen maximaal 4 USP's aan een website gekoppeld worden.")
     */
    protected $usps;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\SiteHomepageAssortment", mappedBy="site", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $homepageAssortments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\SiteCustomHtml", mappedBy="site", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $siteCustomHtml;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\OpeningHours", mappedBy="site", cascade={"persist"})
     * @ORM\OrderBy({"day" = "ASC"})
     */
    protected $siteOpeningHours;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ThemeType")
     * @ORM\Column(type="ThemeType", nullable=true)
     */
    protected $theme;

    /**
     * @ORM\Column(type="boolean", nullable=true);
     */
    protected $customerServiceMenuDisabled;

    /**
     * @ORM\Column(type="boolean", nullable=true);
     */
    protected $paymentMethodLogosHidden;

    /**
     * @ORM\Column(type="string", length=15, nullable=true);
     */
    protected $googleAnalyticsTrackingId;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\DisplayPriceType")
     * @ORM\Column(type="DisplayPriceType", nullable=true)
     */
    protected $defaultDisplayPrice;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\SitePaymentMethodSettings", mappedBy="site",
     *                                                                                cascade={"persist"})
     */
    protected $paymentMethodSettings;

    /**
     * @ORM\Column(type="string", length=50, nullable=true);
     */
    protected $adyenMerchantAccount;

    /**
     * @ORM\Column(type="string", length=50, nullable=true);
     */
    protected $adyenUsername;

    /**
     * @Encrypted
     * @ORM\Column(type="text", nullable=true);
     */
    protected $adyenPassword;

    /**
     * @ORM\Column(type="string", length=50, nullable=true);
     */
    protected $adyenCseToken;

    /**
     * @ORM\Column(type="string", length=50, nullable=true);
     */
    protected $adyenSkinCode;

    /**
     * @Encrypted
     * @ORM\Column(type="text", nullable=true);
     */
    protected $adyenCseKey;

    /**
     * @Encrypted
     * @ORM\Column(type="text", nullable=true);
     */
    protected $adyenHmacKey;

    /**
     * @ORM\Column(type="integer", nullable=true);
     */
    protected $afterpayMerchantId;

    /**
     * @ORM\Column(type="integer", nullable=true);
     */
    protected $afterpayPortfolioId;

    /**
     * @Encrypted
     * @ORM\Column(type="text", nullable=true);
     */
    protected $afterpayPassword;

    /**
     * @ORM\Column(type="integer", nullable=true);
     */
    protected $afterpayCaptureDelay;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Customer\Customer", mappedBy="registeredOnSite")
     */
    protected $registeredCustomers;

    /**
     * @ORM\Column(type="boolean");
     */
    protected $authenticationRequired;

    /**
     * @ORM\Column(type="boolean");
     */
    protected $disableCustomerLogin;

    /**
     * @ORM\Column(type="boolean");
     */
    protected $disableCustomerRegistration;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup")
     */
    protected $customerGroups;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\DeliveryDateMethodType")
     * @ORM\Column(type="DeliveryDateMethodType", nullable=true)
     */
    protected $deliveryDateMethod = 'required';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $deliveryDateMethodDescription;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $orderPickupEnabled = true;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $pickupMethodDescription;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     */
    protected $pickupAddress;

    /**
     * @deprecated
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\TransportType")
     */
    protected $defaultProductTransportType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $addToCartModalEnabled;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="children")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Site", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="sites")
     */
    protected $company;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $availableLocales;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->translate()->getDescription();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->domains = new ArrayCollection();
        $this->usps = new ArrayCollection();
        $this->homepageAssortments = new ArrayCollection();
        $this->siteCustomHtml = new ArrayCollection();
        $this->siteOpeningHours = new ArrayCollection();
        $this->assortments = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->deliveryAddressTypes = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->registeredCustomers = new ArrayCollection();
        $this->customerGroups = new ArrayCollection();
        $this->paymentMethodSettings = new ArrayCollection();
        $this->availableLocales = [];
    }

    public function syncWithParent(){
        $parent = $this->getParent();
        if (null === $parent) {
            throw new \RuntimeException('Parent must be set first');
        }

        $this->setHttpScheme($parent->getHttpScheme());
        $this->setAuthenticationRequired($parent->getAuthenticationRequired());
        $this->setDisableCustomerLogin($parent->getDisableCustomerLogin());
        $this->setDisableCustomerRegistration($parent->getDisableCustomerRegistration());
        $this->setDefaultDisplayPrice($parent->getDefaultDisplayPrice());
        $this->setPhoneNumber($parent->getPhoneNumber());
        $this->setEmail($parent->getEmail());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     * @deprecated
     * @param string $description
     *
     * @return Site
     */
    public function setDescription($description)
    {
        void($description);

        return $this;
    }

    /**
     * Get description
     * @deprecated
     * @return string
     */
    public function getDescription()
    {
        return $this->translate()->getDescription();
    }

    /**
     * Add domain
     *
     * @param Domain $domain
     *
     * @return Site
     */
    public function addDomain(Domain $domain)
    {
        $domain->setSite($this);

        $this->domains[] = $domain;

        return $this;
    }

    /**
     * Remove domain
     *
     * @param Domain $domain
     */
    public function removeDomain(Domain $domain)
    {
        $this->domains->removeElement($domain);
    }

    /**
     * Get domains
     *
     * @return Collection
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     *
     * Get Primary domain
     *
     * @return Domain
     */
    public function getPrimaryDomain()
    {
        $domainCollection = $this->getDomains() ?: new ArrayCollection();

        $domains = $domainCollection->filter(function (Domain $domain) {
            return $domain->getMain();
        });

        $domains = iterator_to_array($domains);

        return reset($domains);
    }

    /**
     *
     * Get additional domains
     *
     * @return Collection
     */
    public function getAdditionalDomains()
    {
        $domains = $this->getDomains() ?: new ArrayCollection();

        return $domains->filter(function (Domain $domain) {
            return !$domain->getMain();
        });
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Site
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set theme
     *
     * @param $theme
     *
     * @return Site
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Site
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return PhoneNumber
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $format
     * @return null|string
     */
    public function getFormattedPhonenumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->phoneNumber) {
            return null;
        }

        return PhoneNumberUtil::getInstance()->format($this->phoneNumber, $format);
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Site
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add siteCustomHtml
     *
     * @param SiteCustomHtml $siteCustomHtml
     *
     * @return Site
     */
    public function addSiteCustomHtml(SiteCustomHtml $siteCustomHtml)
    {
        $siteCustomHtml->setSite($this);

        $this->siteCustomHtml[] = $siteCustomHtml;

        return $this;
    }

    /**
     * Remove siteCustomHtml
     *
     * @param SiteCustomHtml $siteCustomHtml
     */
    public function removeSiteCustomHtml(SiteCustomHtml $siteCustomHtml)
    {
        $this->siteCustomHtml->removeElement($siteCustomHtml);
    }

    /**
     * Get siteCustomHtml
     *
     * @param null $location
     * @param null $manipulation
     * @return Collection
     */
    public function getSiteCustomHtml($location = null, $manipulation = null)
    {
        if (!$location && !$manipulation) {
            return $this->siteCustomHtml;
        }

        return $this->siteCustomHtml->filter(function (SiteCustomHtml $siteCustomHtml) use ($location, $manipulation) {
            return $location && !$manipulation ? $siteCustomHtml->getLocation() === $location : (!$location && $siteCustomHtml->getManipulation() === $manipulation) || (($siteCustomHtml->getLocation() === $location) && ($siteCustomHtml->getManipulation() === $manipulation));
        });
    }

    /**
     * Add assortment
     *
     * @param Assortment $assortment
     *
     * @return Site
     */
    public function addAssortment(Assortment $assortment)
    {
        $this->assortments[] = $assortment;

        return $this;
    }

    /**
     * Remove assortment
     *
     * @param Assortment $assortment
     */
    public function removeAssortment(Assortment $assortment)
    {
        $this->assortments->removeElement($assortment);
    }

    /**
     * Get assortments
     *
     * @return Collection
     */
    public function getAssortments()
    {
        return $this->assortments;
    }

    /**
     * Add page
     *
     * @param Page $page
     *
     * @return Site
     */
    public function addPage(Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param Page $page
     */
    public function removePage(Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add usp
     *
     * @param SiteUsp $siteUsp
     *
     * @return Site
     */
    public function addUsp(SiteUsp $siteUsp)
    {
        $siteUsp->setSite($this);

        $this->usps[] = $siteUsp;

        return $this;
    }

    /**
     * Remove usp
     *
     * @param SiteUsp $siteUsp
     */
    public function removeUsp(SiteUsp $siteUsp)
    {
        $this->usps->removeElement($siteUsp);
    }

    /**
     * Get usps
     *
     * @return Collection
     */
    public function getUsps()
    {
        return $this->usps;
    }

    /**
     * Add homepageAssortment
     *
     * @param SiteHomepageAssortment $siteHomepageAssortment
     *
     * @return Site
     */
    public function addHomepageAssortment(SiteHomepageAssortment $siteHomepageAssortment)
    {
        if (false === $this->homepageAssortments->contains($siteHomepageAssortment)) {
            $this->homepageAssortments[] = $siteHomepageAssortment;

            $siteHomepageAssortment->setSite($this);
        }

        return $this;
    }

    /**
     * Remove homepageAssortment
     *
     * @param SiteHomepageAssortment $siteHomepageAssortment
     */
    public function removeHomepageAssortment(SiteHomepageAssortment $siteHomepageAssortment)
    {
        if (true === $this->homepageAssortments->contains($siteHomepageAssortment)) {
            $this->homepageAssortments->removeElement($siteHomepageAssortment);
        }
    }

    /**
     * Get homepageAssortments
     *
     * @return ArrayCollection|SiteHomepageAssortment[]
     */
    public function getHomepageAssortments()
    {
        return $this->homepageAssortments;
    }

    /**
     * Set homepageAssortment
     *
     * @param Assortment $homepageAssortment
     *
     * @return Site
     */
    public function setHomepageAssortment(Assortment $homepageAssortment = null)
    {
        $this->homepageAssortment = $homepageAssortment;

        return $this;
    }

    /**
     * Get homepageAssortment
     *
     * @return Assortment
     */
    public function getHomepageAssortment()
    {
        return $this->homepageAssortment;
    }

    /**
     * Set homepage
     *
     * @param Page $homepage
     *
     * @return Site
     */
    public function setHomepage(Page $homepage = null)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return Page
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Set googleAnalyticsTrackingId
     *
     * @param string $googleAnalyticsTrackingId
     *
     * @return Site
     */
    public function setGoogleAnalyticsTrackingId($googleAnalyticsTrackingId)
    {
        $this->googleAnalyticsTrackingId = $googleAnalyticsTrackingId;

        return $this;
    }

    /**
     * Get googleAnalyticsTrackingId
     *
     * @return string
     */
    public function getGoogleAnalyticsTrackingId()
    {
        return $this->googleAnalyticsTrackingId;
    }

    /**
     * Set httpScheme
     *
     * @param string $httpScheme
     *
     * @return Site
     */
    public function setHttpScheme($httpScheme)
    {
        $this->httpScheme = $httpScheme;

        return $this;
    }

    /**
     * Get httpScheme
     *
     * @return string
     */
    public function getHttpScheme()
    {
        return $this->httpScheme;
    }

    /**
     * Set httpPort
     *
     * @param integer $httpPort
     *
     * @return Site
     */
    public function setHttpPort($httpPort)
    {
        $this->httpPort = $httpPort;

        return $this;
    }

    /**
     * Get httpPort
     *
     * @return integer
     */
    public function getHttpPort()
    {
        return $this->httpPort;
    }

    /**
     * Get full url based on information present
     *
     * @return string
     */
    public function getUrl()
    {
        $parts = [];

        $parts[] = $this->getHttpScheme();
        $parts[] = '://';
        $parts[] = $this->getPrimaryDomain()->getDomain();

        if ($this->getHttpPort() && !\in_array($this->getHttpPort(), [80, 443], true)) {
            $parts[] = ':';
            $parts[] = $this->getHttpPort();
        }

        return implode(null, $parts);
    }

    /**
     * Add deliveryAddressType
     *
     * @param DeliveryAddressType $deliveryAddressType
     *
     * @return Site
     */
    public function addDeliveryAddressType(DeliveryAddressType $deliveryAddressType)
    {
        $this->deliveryAddressTypes[] = $deliveryAddressType;

        return $this;
    }

    /**
     * Remove deliveryAddressType
     *
     * @param DeliveryAddressType $deliveryAddressType
     */
    public function removeDeliveryAddressType(DeliveryAddressType $deliveryAddressType)
    {
        $this->deliveryAddressTypes->removeElement($deliveryAddressType);
    }

    /**
     * Get deliveryAddressTypes
     *
     * @return Collection
     */
    public function getDeliveryAddressTypes()
    {
        return $this->deliveryAddressTypes;
    }

    /**
     * Add menu
     *
     * @param Menu $menu
     *
     * @return Site
     */
    public function addMenu(Menu $menu)
    {
        $this->menus[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param Menu $menu
     */
    public function removeMenu(Menu $menu)
    {
        $this->menus->removeElement($menu);
    }

    /**
     * Get menus
     *
     * @return Collection
     */
    public function getMenus()
    {
        return $this->menus;
    }

    /**
     * Set adyenUsername
     *
     * @param string $adyenUsername
     *
     * @return Site
     */
    public function setAdyenUsername($adyenUsername)
    {
        $this->adyenUsername = $adyenUsername;

        return $this;
    }

    /**
     * Get adyenUsername
     *
     * @return string
     */
    public function getAdyenUsername()
    {
        return $this->adyenUsername;
    }

    /**
     * Set adyenPassword
     *
     * @param string $adyenPassword
     *
     * @return Site
     */
    public function setAdyenPassword($adyenPassword)
    {
        $this->adyenPassword = $adyenPassword;

        return $this;
    }

    /**
     * Get adyenPassword
     *
     * @return string
     */
    public function getAdyenPassword()
    {
        return $this->adyenPassword;
    }

    /**
     * Set adyenCseToken
     *
     * @param string $adyenCseToken
     *
     * @return Site
     */
    public function setAdyenCseToken($adyenCseToken)
    {
        $this->adyenCseToken = $adyenCseToken;

        return $this;
    }

    /**
     * Get adyenCseToken
     *
     * @return string
     */
    public function getAdyenCseToken()
    {
        return $this->adyenCseToken;
    }

    /**
     * Set adyenCseKey
     *
     * @param string $adyenCseKey
     *
     * @return Site
     */
    public function setAdyenCseKey($adyenCseKey)
    {
        $this->adyenCseKey = $adyenCseKey;

        return $this;
    }

    /**
     * Get adyenCseKey
     *
     * @return string
     */
    public function getAdyenCseKey()
    {
        return $this->adyenCseKey;
    }

    /**
     * Set adyenMerchantAccount
     *
     * @param string $adyenMerchantAccount
     *
     * @return Site
     */
    public function setAdyenMerchantAccount($adyenMerchantAccount)
    {
        $this->adyenMerchantAccount = $adyenMerchantAccount;

        return $this;
    }

    /**
     * Get adyenMerchantAccount
     *
     * @return string
     */
    public function getAdyenMerchantAccount()
    {
        return $this->adyenMerchantAccount;
    }

    /**
     * Set adyenSkinCode
     *
     * @param string $adyenSkinCode
     *
     * @return Site
     */
    public function setAdyenSkinCode($adyenSkinCode)
    {
        $this->adyenSkinCode = $adyenSkinCode;

        return $this;
    }

    /**
     * Get adyenSkinCode
     *
     * @return string
     */
    public function getAdyenSkinCode()
    {
        return $this->adyenSkinCode;
    }

    /**
     * Set adyenHmacKey
     *
     * @param string $adyenHmacKey
     *
     * @return Site
     */
    public function setAdyenHmacKey($adyenHmacKey)
    {
        $this->adyenHmacKey = $adyenHmacKey;

        return $this;
    }

    /**
     * Get adyenHmacKey
     *
     * @return string
     */
    public function getAdyenHmacKey()
    {
        return $this->adyenHmacKey;
    }

    /**
     * Add registeredCustomer
     *
     * @param Customer $registeredCustomer
     *
     * @return Site
     */
    public function addRegisteredCustomer(Customer $registeredCustomer)
    {
        $this->registeredCustomers[] = $registeredCustomer;

        return $this;
    }

    /**
     * Remove registeredCustomer
     *
     * @param Customer $registeredCustomer
     */
    public function removeRegisteredCustomer(Customer $registeredCustomer)
    {
        $this->registeredCustomers->removeElement($registeredCustomer);
    }

    /**
     * Get registeredCustomers
     *
     * @return Collection
     */
    public function getRegisteredCustomers()
    {
        return $this->registeredCustomers;
    }

    /**
     * @return array
     */
    public function getAvailableLocales(): array
    {
        return $this->availableLocales ?? [];
    }

    /**
     * @param array $locales
     * @return Site
     */
    public function setAvailableLocales(array $locales): Site{
        $this->availableLocales = $locales;
        return $this;
    }

    /**
     * @param string $locale
     * @return Site
     */
    public function addAvailableLocale(string $locale): Site{
        if(!isset($this->availableLocales[$locale])) {
            $this->availableLocales[] = $locale;
        }
        return $this;
    }

    /**
     * Add customerGroup
     *
     * @param CustomerGroup $customerGroup
     *
     * @return Site
     */
    public function addCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->customerGroups[] = $customerGroup;

        return $this;
    }

    /**
     * Remove customerGroup
     *
     * @param CustomerGroup $customerGroup
     */
    public function removeCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->customerGroups->removeElement($customerGroup);
    }

    /**
     * Get customerGroups
     *
     * @return Collection
     */
    public function getCustomerGroups()
    {
        return $this->customerGroups;
    }

    /**
     * Set authenticationRequired
     *
     * @param boolean $authenticationRequired
     *
     * @return Site
     */
    public function setAuthenticationRequired($authenticationRequired)
    {
        $this->authenticationRequired = $authenticationRequired;

        return $this;
    }

    /**
     * Get authenticationRequired
     *
     * @return boolean
     */
    public function getAuthenticationRequired()
    {
        return $this->authenticationRequired;
    }

    /**
     * Set disableCustomerLogin
     *
     * @param boolean $disableCustomerLogin
     *
     * @return Site
     */
    public function setDisableCustomerLogin($disableCustomerLogin)
    {
        $this->disableCustomerLogin = $disableCustomerLogin;

        return $this;
    }

    /**
     * Get disableCustomerLogin
     *
     * @return boolean
     */
    public function getDisableCustomerLogin()
    {
        return $this->disableCustomerLogin;
    }

    /**
     * Set disableCustomerRegistration
     *
     * @param boolean $disableCustomerRegistration
     *
     * @return Site
     */
    public function setDisableCustomerRegistration($disableCustomerRegistration)
    {
        $this->disableCustomerRegistration = $disableCustomerRegistration;

        return $this;
    }

    /**
     * Get disableCustomerRegistration
     *
     * @return boolean
     */
    public function getDisableCustomerRegistration()
    {
        return $this->disableCustomerRegistration;
    }

    /**
     * Set defaultDisplayPrice
     *
     * @param DisplayPriceType $defaultDisplayPrice
     *
     * @return Site
     */
    public function setDefaultDisplayPrice($defaultDisplayPrice)
    {
        $this->defaultDisplayPrice = $defaultDisplayPrice;

        return $this;
    }

    /**
     * Get defaultDisplayPrice
     *
     * @return DisplayPriceType
     */
    public function getDefaultDisplayPrice()
    {
        return $this->defaultDisplayPrice;
    }

    /**
     * Set deliveryDateMethod
     *
     * @param DeliveryDateMethodType $deliveryDateMethod
     *
     * @return Site
     */
    public function setDeliveryDateMethod($deliveryDateMethod)
    {
        $this->deliveryDateMethod = $deliveryDateMethod;

        return $this;
    }

    /**
     * Get deliveryDateMethod
     *
     * @return string
     */
    public function getDeliveryDateMethod()
    {
        return $this->deliveryDateMethod;
    }

    /**
     * Set deliveryDateMethodDescription
     *
     * @param string $deliveryDateMethodDescription
     *
     * @return Site
     */
    public function setDeliveryDateMethodDescription($deliveryDateMethodDescription)
    {
        $this->deliveryDateMethodDescription = $deliveryDateMethodDescription;

        return $this;
    }

    /**
     * Get deliveryDateMethodDescription
     *
     * @return string
     */
    public function getDeliveryDateMethodDescription()
    {
        return $this->deliveryDateMethodDescription;
    }

    /**
     * Set orderPickupEnabled
     *
     * @param boolean $orderPickupEnabled
     *
     * @return Site
     */
    public function setOrderPickupEnabled($orderPickupEnabled)
    {
        $this->orderPickupEnabled = $orderPickupEnabled;

        return $this;
    }

    /**
     * Get orderPickupEnabled
     *
     * @return boolean
     */
    public function getOrderPickupEnabled()
    {
        return $this->orderPickupEnabled;
    }

    /**
     * Set pickupMethodDescription
     *
     * @param string $pickupMethodDescription
     *
     * @return Site
     */
    public function setPickupMethodDescription($pickupMethodDescription)
    {
        $this->pickupMethodDescription = $pickupMethodDescription;

        return $this;
    }

    /**
     * Get pickupMethodDescription
     *
     * @return string
     */
    public function getPickupMethodDescription()
    {
        return $this->pickupMethodDescription;
    }

    /**
     * Set pickupAddress
     *
     * @param Address $pickupAddress
     *
     * @return Site
     */
    public function setPickupAddress(Address $pickupAddress = null)
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * Get pickupAddress
     *
     * @return Address
     */
    public function getPickupAddress()
    {
        return $this->pickupAddress;
    }

    /**
     * Set addToCartModalEnabled
     *
     * @param boolean $addToCartModalEnabled
     *
     * @return Site
     */
    public function setAddToCartModalEnabled($addToCartModalEnabled)
    {
        $this->addToCartModalEnabled = $addToCartModalEnabled;

        return $this;
    }

    /**
     * Get addToCartModalEnabled
     *
     * @return boolean
     */
    public function getAddToCartModalEnabled()
    {
        return $this->addToCartModalEnabled;
    }

    /**
     * Set homepageAbout
     *
     * @param Page $homepageAbout
     *
     * @return Site
     */
    public function setHomepageAbout(Page $homepageAbout = null)
    {
        $this->homepageAbout = $homepageAbout;

        return $this;
    }

    /**
     * Get homepageAbout
     *
     * @return Page
     */
    public function getHomepageAbout()
    {
        return $this->homepageAbout;
    }

    /**
     * Set customerServiceMenuDisabled
     *
     * @param boolean $customerServiceMenuDisabled
     *
     * @return Site
     */
    public function setCustomerServiceMenuDisabled($customerServiceMenuDisabled)
    {
        $this->customerServiceMenuDisabled = $customerServiceMenuDisabled;

        return $this;
    }

    /**
     * Get customerServiceMenuDisabled
     *
     * @return boolean
     */
    public function getCustomerServiceMenuDisabled()
    {
        return $this->customerServiceMenuDisabled;
    }

    /**
     * Set paymentMethodLogosHidden
     *
     * @param boolean $paymentMethodLogosHidden
     *
     * @return Site
     */
    public function setPaymentMethodLogosHidden($paymentMethodLogosHidden)
    {
        $this->paymentMethodLogosHidden = $paymentMethodLogosHidden;

        return $this;
    }

    /**
     * Get paymentMethodLogosHidden
     *
     * @return boolean
     */
    public function getPaymentMethodLogosHidden()
    {
        return $this->paymentMethodLogosHidden;
    }

    /**
     * Set orderSuccessPage
     *
     * @param Page $orderSuccessPage
     *
     * @return Site
     */
    public function setOrderSuccessPage(Page $orderSuccessPage = null)
    {
        $this->orderSuccessPage = $orderSuccessPage;

        return $this;
    }

    /**
     * Get orderSuccessPage
     *
     * @return Page
     */
    public function getOrderSuccessPage()
    {
        return $this->orderSuccessPage;
    }

    /**
     * Set paymentPendingPage
     *
     * @param Page $paymentPendingPage
     *
     * @return Site
     */
    public function setPaymentPendingPage(Page $paymentPendingPage = null)
    {
        $this->paymentPendingPage = $paymentPendingPage;

        return $this;
    }

    /**
     * Get paymentPendingPage
     *
     * @return Page
     */
    public function getPaymentPendingPage()
    {
        return $this->paymentPendingPage;
    }

    /**
     * Set defaultProductTransportType
     *
     * @param TransportType $defaultProductTransportType
     * @deprecated set transporttypes on productgroup instead
     * @return Site
     */
    public function setDefaultProductTransportType(TransportType $defaultProductTransportType = null)
    {
        $this->defaultProductTransportType = $defaultProductTransportType;

        return $this;
    }

    /**
     * Get defaultProductTransportType
     * @deprecated
     * @return TransportType
     */
    public function getDefaultProductTransportType()
    {
        return $this->defaultProductTransportType;
    }

    /**
     * Set afterpayMerchantId
     *
     * @param integer $afterpayMerchantId
     *
     * @return Site
     */
    public function setAfterpayMerchantId($afterpayMerchantId)
    {
        $this->afterpayMerchantId = $afterpayMerchantId;

        return $this;
    }

    /**
     * Get afterpayMerchantId
     *
     * @return integer
     */
    public function getAfterpayMerchantId()
    {
        return $this->afterpayMerchantId;
    }

    /**
     * Set afterpayPortfolioId
     *
     * @param integer $afterpayPortfolioId
     *
     * @return Site
     */
    public function setAfterpayPortfolioId($afterpayPortfolioId)
    {
        $this->afterpayPortfolioId = $afterpayPortfolioId;

        return $this;
    }

    /**
     * Get afterpayPortfolioId
     *
     * @return integer
     */
    public function getAfterpayPortfolioId()
    {
        return $this->afterpayPortfolioId;
    }

    /**
     * Set afterpayPassword
     *
     * @param string $afterpayPassword
     *
     * @return Site
     */
    public function setAfterpayPassword($afterpayPassword)
    {
        $this->afterpayPassword = $afterpayPassword;

        return $this;
    }

    /**
     * Get afterpayPassword
     *
     * @return string
     */
    public function getAfterpayPassword()
    {
        return $this->afterpayPassword;
    }

    /**
     * Add paymentMethodSetting
     *
     * @param SitePaymentMethodSettings $paymentMethodSetting
     *
     * @return Site
     */
    public function addPaymentMethodSetting(SitePaymentMethodSettings $paymentMethodSetting)
    {
        $this->paymentMethodSettings[] = $paymentMethodSetting;

        return $this;
    }

    /**
     * Remove paymentMethodSetting
     *
     * @param SitePaymentMethodSettings $paymentMethodSetting
     */
    public function removePaymentMethodSetting(SitePaymentMethodSettings $paymentMethodSetting)
    {
        $this->paymentMethodSettings->removeElement($paymentMethodSetting);
    }

    /**
     * Get paymentMethodSettings
     *
     * @return Collection
     */
    public function getPaymentMethodSettings()
    {
        return $this->paymentMethodSettings;
    }

    /**
     * Set afterpayCaptureDelay
     *
     * @param integer $afterpayCaptureDelay
     *
     * @return Site
     */
    public function setAfterpayCaptureDelay($afterpayCaptureDelay)
    {
        $this->afterpayCaptureDelay = $afterpayCaptureDelay;

        return $this;
    }

    /**
     * Get afterpayCaptureDelay
     *
     * @return integer
     */
    public function getAfterpayCaptureDelay()
    {
        return $this->afterpayCaptureDelay;
    }

    /**
     * Set customerServicePopupPage
     *
     * @param Page $customerServicePopupPage
     *
     * @return Site
     */
    public function setCustomerServicePopupPage(Page $customerServicePopupPage = null)
    {
        $this->customerServicePopupPage = $customerServicePopupPage;

        return $this;
    }

    /**
     * Get customerServicePopupPage
     *
     * @return Page
     */
    public function getCustomerServicePopupPage()
    {
        return $this->customerServicePopupPage;
    }

    /**
     * Set termsAndConditionsPage
     *
     * @param Page $termsAndConditionsPage
     *
     * @return Site
     */
    public function setTermsAndConditionsPage(Page $termsAndConditionsPage = null)
    {
        $this->termsAndConditionsPage = $termsAndConditionsPage;

        return $this;
    }

    /**
     * Get termsAndConditionsPage
     *
     * @return Page
     */
    public function getTermsAndConditionsPage()
    {
        return $this->termsAndConditionsPage;
    }


    /**
     * Add siteOpeningHour
     *
     * @param OpeningHours $siteOpeningHour
     *
     * @return Site
     */
    public function addSiteOpeningHour(OpeningHours $siteOpeningHour)
    {
        $siteOpeningHour->setSite($this);

        $this->siteOpeningHours[] = $siteOpeningHour;

        return $this;
    }

    /**
     * Remove siteOpeningHour
     *
     * @param OpeningHours $siteOpeningHour
     */
    public function removeSiteOpeningHour(OpeningHours $siteOpeningHour)
    {
        $this->siteOpeningHours->removeElement($siteOpeningHour);
    }

    /**
     * Get siteOpeningHours
     *
     * @return Collection
     */
    public function getSiteOpeningHours()
    {
        return $this->siteOpeningHours;
    }

    /**
     * Set slug.
     *
     * @param string|null $slug
     *
     * @return Site
     */
    public function setSlug($slug = null)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set parent.
     *
     * @param Site|null $parent
     *
     * @return Site
     */
    public function setParent(Site $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return Site|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param Site $child
     *
     * @return Site
     */
    public function addChild(Site $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param Site $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(Site $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return Collection|Site[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Site
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
