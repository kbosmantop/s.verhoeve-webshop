<?php

namespace AppBundle\Entity\Site;

use AppBundle\Entity\Payment\Paymentmethod;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"site_id", "payment_method_id"})
 * })
 */
class SitePaymentMethodSettings
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="paymentMethodSettings")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payment\Paymentmethod")
     */
    protected $paymentMethod;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return SitePaymentMethodSettings
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return \true
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return SitePaymentMethodSettings
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set paymentMethod
     *
     * @param Paymentmethod $paymentMethod
     *
     * @return SitePaymentMethodSettings
     */
    public function setPaymentMethod(Paymentmethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return Paymentmethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
}
