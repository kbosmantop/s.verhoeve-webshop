<?php


namespace AppBundle\Entity\Site;

use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\EntityListeners({"AppBundle\EventListener\MenuListener"})
 */
class Menu
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="menus")
     */
    protected $site;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\MenuItem", mappedBy="menu", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $items;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxLevels;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxRootItems;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxChildItems;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\MenuLocationType")
     * @ORM\Column(type="MenuLocationType", nullable=true)
     */
    protected $location;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\Company", mappedBy="menu")
     */
    protected $companies;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'menu' => $this->menu,
            'menuItem' => $this->menuItem,
            'items' => $this->items,
            'position' => $this->position,
        ]);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return Menu
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Add item
     *
     * @param MenuItem $item
     *
     * @return Menu
     */
    public function addItem(MenuItem $item)
    {
        $this->items[] = $item;

        $item->setMenu($this);

        return $this;
    }

    /**
     * Remove item
     *
     * @param MenuItem $item
     */
    public function removeItem(MenuItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set location
     *
     * @param MenuLocationType $location
     *
     * @return Menu
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return MenuLocationType
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set maxLevels
     *
     * @param integer $maxLevels
     *
     * @return Menu
     */
    public function setMaxLevels($maxLevels)
    {
        $this->maxLevels = $maxLevels;

        return $this;
    }

    /**
     * Get maxLevels
     *
     * @return integer
     */
    public function getMaxLevels()
    {
        return $this->maxLevels;
    }

    /**
     * Set maxRootItems
     *
     * @param integer $maxRootItems
     *
     * @return Menu
     */
    public function setMaxRootItems($maxRootItems)
    {
        $this->maxRootItems = $maxRootItems;

        return $this;
    }

    /**
     * Get maxRootItems
     *
     * @return integer
     */
    public function getMaxRootItems()
    {
        return $this->maxRootItems;
    }

    /**
     * Set maxChildItems
     *
     * @param $maxChildItems
     *
     * @return Menu
     */
    public function setMaxChildItems($maxChildItems)
    {
        $this->maxChildItems = $maxChildItems;

        return $this;
    }

    /**
     * Get maxChildItems
     *
     * @return integer
     */
    public function getMaxChildItems()
    {
        return $this->maxChildItems;
    }

    /**
     * Add company
     *
     * @param Company $company
     *
     * @return Menu
     */
    public function addCompany(Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }
}
