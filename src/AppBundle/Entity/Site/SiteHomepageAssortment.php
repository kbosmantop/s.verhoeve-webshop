<?php

namespace AppBundle\Entity\Site;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class SiteHomepageAssortment
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="homepageAssortments")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     */
    protected $assortment;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SiteHomepageAssortment
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return SiteHomepageAssortment
     */
    public function setSite(Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set assortment
     *
     * @param Assortment $assortment
     *
     * @return SiteHomepageAssortment
     */
    public function setAssortment(?Assortment $assortment)
    {
        $this->assortment = $assortment;

        return $this;
    }

    /**
     * Get assortment
     *
     * @return Assortment
     */
    public function getAssortment(): ?Assortment
    {
        return $this->assortment;
    }
}
