<?php

namespace AppBundle\Entity\Site;

use AppBundle\Interfaces\Seo\SlugInterface;
use AppBundle\Traits\SeoEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 */
class PageTranslation implements SlugInterface
{
    use ORMBehaviors\Translatable\Translation;
    use SeoEntity;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $slug;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PageTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return PageTranslation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return PageTranslation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
