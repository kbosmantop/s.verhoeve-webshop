<?php

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table("site_custom_html");
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class SiteCustomHtml
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const LOCATION_HEAD = 'head';
    const LOCATION_BODY = 'body';

    const MANIPULATION_APPEND = 'append';
    const MANIPULATION_PREPEND = 'prepend';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="siteCustomHtml")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $site;

    /**
     * @ORM\Column(type="string", length=4)
     *
     */
    protected $location;

    /**
     * @ORM\Column(type="text")
     *
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=16)
     *
     */
    protected $manipulation;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     *
     */
    protected $position;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return SiteCustomHtml
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SiteCustomHtml
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return SiteCustomHtml
     */
    public function setSite(Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set manipulation
     *
     * @param string $manipulation
     *
     * @return SiteCustomHtml
     */
    public function setManipulation($manipulation)
    {
        $this->manipulation = $manipulation;

        return $this;
    }

    /**
     * Get manipulation
     *
     * @return string
     */
    public function getManipulation()
    {
        return $this->manipulation;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return SiteCustomHtml
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
}
