<?php

namespace AppBundle\Entity\Site;

use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * Class SiteLogo
 * @package AppBundle\Entity\Relation\Company
 */
class SiteLogo implements ImageInterface
{

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return SiteLogo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SiteLogo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'path';
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return "images/site-logo/" . $this->getId();
    }

    /**
     * @return string
     */
    public function generateName()
    {
        $name = $this->getDescription();

        return $name;
    }
}
