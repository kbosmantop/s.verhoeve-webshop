<?php

namespace AppBundle\Entity\Site;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use AppBundle\Traits\PublishableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class Banner implements ImageInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ImageEntity;
    use PublishableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $image;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\BannerTextBlockPositionType")
     * @ORM\Column(type="BannerTextBlockPositionType")
     */
    protected $textBlockPosition;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Page", mappedBy="banner")
     */
    protected $pages;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", mappedBy="banner")
     */
    protected $assortments;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Banner
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set textBlockPosition
     *
     * @param string $textBlockPosition
     *
     * @return Banner
     */
    public function setTextBlockPosition($textBlockPosition)
    {
        $this->textBlockPosition = $textBlockPosition;

        return $this;
    }

    /**
     * Get textBlockPosition
     *
     * @return string
     */
    public function getTextBlockPosition()
    {
        return $this->textBlockPosition;
    }

    public function getPathColumn()
    {
        return 'image';
    }

    public function getPath()
    {
        return $this->{$this->getPathColumn()};
    }

    public function setPath($path)
    {
        $this->{$this->getPathColumn()} = $path;

        return $this;
    }

    public function generateName()
    {
        return Urlizer::urlize($this->getName());
    }

    public function getStoragePath()
    {
        return "images/banner/" . $this->getId();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->assortments = new ArrayCollection();
    }

    /**
     * Add page
     *
     * @param Page $page
     *
     * @return Banner
     */
    public function addPage(Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param Page $page
     */
    public function removePage(Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add assortment
     *
     * @param Assortment $assortment
     *
     * @return Banner
     */
    public function addAssortment(Assortment $assortment)
    {
        $this->assortments[] = $assortment;

        return $this;
    }

    /**
     * Remove assortment
     *
     * @param Assortment $assortment
     */
    public function removeAssortment(Assortment $assortment)
    {
        $this->assortments->removeElement($assortment);
    }

    /**
     * Get assortments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssortments()
    {
        return $this->assortments;
    }
}
