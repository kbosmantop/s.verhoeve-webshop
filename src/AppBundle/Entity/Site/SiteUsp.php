<?php

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"site_id", "usp_id"})})
 */
class SiteUsp
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="usps")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Usp", inversedBy="sites")
     */
    protected $usp;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * Set id
     *
     * @param $id
     * @return SiteUsp
     */
    public function setId($id)
    {

        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SiteUsp
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return SiteUsp
     */
    public function setSite(Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set usp
     *
     * @param Usp $usp
     *
     * @return SiteUsp
     */
    public function setUsp(Usp $usp)
    {
        $this->usp = $usp;

        return $this;
    }

    /**
     * Get usp
     *
     * @return Usp
     */
    public function getUsp()
    {
        return $this->usp;
    }
}
