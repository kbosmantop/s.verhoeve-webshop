<?php

namespace AppBundle\Entity\Site;

use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @Rule\Entity(description="Domein")
 */
class Domain
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="domains")
     * @Rule\Property(description="Site", autoComplete={"id"="site.id", "label"="site.translate.description"},
     *                                    relation="parent")
     */
    protected $site;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $main;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $domain;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany", mappedBy="domain")
     */
    protected $kiyohCompany;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $mailFromName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $mailFromAddress;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $defaultLocale;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDomain();
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function defaultLocaleConstraint(ExecutionContextInterface $executionContext, $payload)
    {
        if ($this->getSite() === null || false === \in_array($this->getDefaultLocale(),
                $this->getSite()->getAvailableLocales(), true)) {
            $executionContext->buildViolation(sprintf('Standaardtaal %s voor domein %s is niet beschikbaar op de site %s',
                $this->getDefaultLocale(), $this->getDomain(), $this->getSite()))
                ->atPath('defaultLocale')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        $site = $this->getSite();
        $uri = $site->getHttpScheme() . '://' . $this->getDomain();

        if ($site->getHttpPort()) {
            $uri .= ':' . $site->getHttpPort();
        }
        return $uri;

    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set defaultLocale
     *
     * @param string $defaultLocale
     *
     * @return Domain
     */
    public function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;

        return $this;
    }

    /**
     * Get defaultLocale
     *
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return Domain
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set mailFromName
     *
     * @param string $mailFromName
     *
     * @return Domain
     */
    public function setMailFromName($mailFromName)
    {
        $this->mailFromName = $mailFromName;

        return $this;
    }

    /**
     * Get mailFromName
     *
     * @return string
     */
    public function getMailFromName()
    {
        return $this->mailFromName;
    }

    /**
     * Set mailFromAddress
     *
     * @param string $mailFromAddress
     *
     * @return Domain
     */
    public function setMailFromAddress($mailFromAddress)
    {
        $this->mailFromAddress = $mailFromAddress;

        return $this;
    }

    /**
     * Get mailFromAddress
     *
     * @return string
     */
    public function getMailFromAddress()
    {
        return $this->mailFromAddress;
    }

    /**
     * Set kiyohCompany
     *
     * @param KiyohCompany $kiyohCompany
     *
     * @return Domain
     */
    public function setKiyohCompany(KiyohCompany $kiyohCompany = null)
    {
        $this->kiyohCompany = $kiyohCompany;

        return $this;
    }

    /**
     * Get kiyohCompany
     *
     * @return KiyohCompany
     */
    public function getKiyohCompany()
    {
        return $this->kiyohCompany;
    }

    /**
     * Returns if kiyohCompany is present
     *
     * @return boolean
     */
    public function hasKiyohCompany()
    {
        return (bool)$this->kiyohCompany;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return Domain
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }
}
