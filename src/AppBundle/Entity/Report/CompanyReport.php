<?php

namespace AppBundle\Entity\Report;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class CompanyReport
 * @package AppBundle\Entity\Report
 */
class CompanyReport
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report\Report", inversedBy="companyReports", cascade={"persist"})
     */
    protected $report;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="reports")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report\CompanyReportCustomer", mappedBy="companyReport", cascade={"persist"})
     */
    protected $customers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set report
     *
     * @param Report $report
     *
     * @return CompanyReport
     */
    public function setReport(Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyReport
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add customer
     *
     * @param CompanyReportCustomer $customer
     *
     * @return CompanyReport
     */
    public function addCustomer(CompanyReportCustomer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param CompanyReportCustomer $customer
     */
    public function removeCustomer(CompanyReportCustomer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }
}
