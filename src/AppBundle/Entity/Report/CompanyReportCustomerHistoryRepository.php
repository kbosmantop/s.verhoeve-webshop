<?php

namespace AppBundle\Entity\Report;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\ORM\EntityRepository;

/**
 * Class CompanyReportCustomerHistoryRepository
 * @package AppBundle\Entity\Report
 */
class CompanyReportCustomerHistoryRepository extends EntityRepository
{

    /**
     * @param Customer $customer
     * @return array
     */
    public function findByCustomer(Customer $customer)
    {
        $qb = $this->createQueryBuilder('crch');
        $qb
            ->leftJoin('crch.companyReportCustomer', 'crc')
            ->leftJoin('crc.customer', 'customer')
            ->andWhere('customer.id = :customer')
            ->setParameter('customer', $customer->getId());

        return $qb->getQuery()->getResult();
    }

}
