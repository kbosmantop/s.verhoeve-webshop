<?php

namespace AppBundle\Entity\Report;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Entity\Rule;

/**
 * @Entity()
 * Class Report
 * @package AppBundle\Entity\Report
 */
class Report
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report\ReportColumn", mappedBy="report", cascade={"persist"})
     */
    protected $columns;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report\CompanyReport", mappedBy="report")
     */
    protected $companyReports;

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->columns = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Report
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set rule
     *
     * @param Rule $rule
     *
     * @return Report
     */
    public function setRule(Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Add column
     *
     * @param ReportColumn $column
     *
     * @return Report
     */
    public function addColumn(ReportColumn $column)
    {
        $column->setReport($this);

        $this->columns[] = $column;

        return $this;
    }

    /**
     * Remove column
     *
     * @param ReportColumn $column
     */
    public function removeColumn(ReportColumn $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return Collection
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Add companyReport
     *
     * @param CompanyReport $companyReport
     *
     * @return Report
     */
    public function addCompanyReport(CompanyReport $companyReport)
    {
        $this->companyReports[] = $companyReport;

        return $this;
    }

    /**
     * Remove companyReport
     *
     * @param CompanyReport $companyReport
     */
    public function removeCompanyReport(CompanyReport $companyReport)
    {
        $this->companyReports->removeElement($companyReport);
    }

    /**
     * Get companyReports
     *
     * @return Collection
     */
    public function getCompanyReports()
    {
        return $this->companyReports;
    }
}
