<?php

namespace AppBundle\Entity\Report;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners({"AppBundle\EventListener\Report\CompanyReportCustomerListener"})
 * Class CompanyReportCustomer
 * @package AppBundle\Entity\Report
 */
class CompanyReportCustomer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report\CompanyReport", inversedBy="customers")
     */
    protected $companyReport;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $customer;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $startDate;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyReportCustomerFrequencyType")
     * @ORM\Column(type="CompanyReportCustomerFrequencyType")
     */
    protected $frequency;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Report\CompanyReportCustomerHistory",
     *     mappedBy="companyReportCustomer",
     *     cascade={"persist", "remove"}
     *  )
     */
    protected $histories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->histories = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (null === $this->getCustomer()) {
            return '';
        }

        return $this->getCustomer()->getFullname();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set frequency
     *
     * @param $frequency
     *
     * @return CompanyReportCustomer
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set companyReport
     *
     * @param CompanyReport $companyReport
     *
     * @return CompanyReportCustomer
     */
    public function setCompanyReport(CompanyReport $companyReport = null)
    {
        $this->companyReport = $companyReport;

        return $this;
    }

    /**
     * Get companyReport
     *
     * @return CompanyReport
     */
    public function getCompanyReport()
    {
        return $this->companyReport;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return CompanyReportCustomer
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return CompanyReportCustomer
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Add history
     *
     * @param CompanyReportCustomerHistory $history
     *
     * @return CompanyReportCustomer
     */
    public function addHistory(CompanyReportCustomerHistory $history)
    {
        $this->histories[] = $history;

        return $this;
    }

    /**
     * Remove history
     *
     * @param CompanyReportCustomerHistory $history
     */
    public function removeHistory(CompanyReportCustomerHistory $history)
    {
        $this->histories->removeElement($history);
    }

    /**
     * Get histories
     *
     * @return Collection
     */
    public function getHistories()
    {
        return $this->histories;
    }
}
