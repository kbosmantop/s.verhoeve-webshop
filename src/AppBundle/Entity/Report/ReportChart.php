<?php

namespace AppBundle\Entity\Report;

use AppBundle\DBAL\Types\ReportChartType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class ReportChart
 * @package AppBundle\Entity\Report
 * @ORM\Entity()
 */
class ReportChart {

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report\Report")
     */
    protected $report;

    /**
     * @ORM\Column(type="string")
     */
    protected $dateFilter;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report\ReportChartData", mappedBy="reportChart", cascade={"persist", "remove"})
     */
    protected $reportChartData;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ReportChartType")
     * @ORM\Column(type="ReportChartType", nullable=true)
     */
    protected $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ReportChart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param ReportChartType $type
     *
     * @return ReportChart
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return ReportChartType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set report
     *
     * @param Report $report
     *
     * @return ReportChart
     */
    public function setReport(Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reportChartData = new ArrayCollection();
    }

    /**
     * Add reportChartDatum
     *
     * @param ReportChartData $reportChartDatum
     *
     * @return ReportChart
     */
    public function addReportChartDatum(ReportChartData $reportChartDatum)
    {
        $this->reportChartData[] = $reportChartDatum;

        $reportChartDatum->setReportChart($this);

        return $this;
    }

    /**
     * Remove reportChartDatum
     *
     * @param ReportChartData $reportChartDatum
     */
    public function removeReportChartDatum(ReportChartData $reportChartDatum)
    {
        $this->reportChartData->removeElement($reportChartDatum);
    }

    /**
     * Get reportChartData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportChartData()
    {
        return $this->reportChartData;
    }

    /**
     * Set dateFilter
     *
     * @param string $dateFilter
     *
     * @return ReportChart
     */
    public function setDateFilter($dateFilter)
    {
        $this->dateFilter = $dateFilter;

        return $this;
    }

    /**
     * Get dateFilter
     *
     * @return string
     */
    public function getDateFilter()
    {
        return $this->dateFilter;
    }
}
