<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Supplier\SupplierOrder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class CommissionInvoiceSupplierOrder
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommissionInvoice", inversedBy="commissionInvoiceSupplierOrders")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $commissionInvoice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierOrder", inversedBy="commissionInvoiceSupplierOrders")
     */
    protected $supplierOrder;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Finance\CommissionInvoiceSupplierOrderLine", mappedBy="commissionInvoiceSupplierOrder", cascade={"persist"})
     */
    protected $commissionInvoiceSupplierOrderLines;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $recommissioned = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commissionInvoiceSupplierOrderLines = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commission
     *
     * @param CommissionInvoice $commissionInvoice
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function setCommissionInvoice(CommissionInvoice $commissionInvoice = null)
    {
        $this->commissionInvoice = $commissionInvoice;

        return $this;
    }

    /**
     * Get commission
     *
     * @return CommissionInvoice
     */
    public function getCommissionInvoice()
    {
        return $this->commissionInvoice;
    }

    /**
     * Set supplier order
     *
     * @param SupplierOrder $supplierOrder
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function setSupplierOrder(SupplierOrder $supplierOrder = null)
    {
        $this->supplierOrder = $supplierOrder;

        return $this;
    }

    /**
     * Get supplier order
     *
     * @return SupplierOrder
     */
    public function getSupplierOrder()
    {
        return $this->supplierOrder;
    }

    /**
     * Add commissionInvoiceSupplierOrderLine
     *
     * @param CommissionInvoiceSupplierOrderLine $commissionSupplierOrderLine
     * @return CommissionInvoiceSupplierOrder
     */
    public function addCommissionSupplierOrderLine(CommissionInvoiceSupplierOrderLine $commissionSupplierOrderLine)
    {
        $commissionSupplierOrderLine->setCommissionSupplierOrder($this);

        $this->commissionInvoiceSupplierOrderLines[] = $commissionSupplierOrderLine;

        return $this;
    }

    /**
     * Remove commissionInvoiceSupplierOrderLine
     *
     * @param CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine
     */
    public function removeCommissionSupplierOrderLine(CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine)
    {
        $this->commissionInvoiceSupplierOrderLines->removeElement($commissionInvoiceSupplierOrderLine);
    }

    /**
     * Get commissionInvoiceSupplierOrderLines
     *
     * @return Collection|CommissionInvoiceSupplierOrderLine[]
     */
    public function getCommissionInvoiceSupplierOrderLines()
    {
        return $this->commissionInvoiceSupplierOrderLines;
    }

    /**
     * @return float
     */
    public function getSubtotalIncl()
    {
        $subtotalIncl = 0;

        foreach ($this->getCommissionInvoiceSupplierOrderLines() as $commissionInvoiceSupplierOrderLine) {
            $subtotalIncl += $commissionInvoiceSupplierOrderLine->getOrderLineTotalPriceIncl();
        }

        return $subtotalIncl;
    }

    /**
     * @return float
     */
    public function getDiscountIncl()
    {
        $totalDiscountIncl = 0;

        foreach ($this->getCommissionInvoiceSupplierOrderLines() as $commissionInvoiceSupplierOrderLine) {
            $totalDiscountIncl += ($commissionInvoiceSupplierOrderLine->getOrderLineTotalPriceIncl() / 100 * $commissionInvoiceSupplierOrderLine->getPercentage());
        }

        return $totalDiscountIncl;
    }

    /**
     * @return float
     */
    public function getTotalIncl()
    {
        return $this->getSubtotalIncl() - $this->getDiscountIncl();
    }

    /**
     * Add commissionInvoiceSupplierOrderLine
     *
     * @param CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function addCommissionInvoiceSupplierOrderLine(CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine)
    {
        $this->commissionInvoiceSupplierOrderLines[] = $commissionInvoiceSupplierOrderLine;

        return $this;
    }

    /**
     * Remove commissionInvoiceSupplierOrderLine
     *
     * @param CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine
     */
    public function removeCommissionInvoiceSupplierOrderLine(CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine)
    {
        $this->commissionInvoiceSupplierOrderLines->removeElement($commissionInvoiceSupplierOrderLine);
    }

    /**
     * Set recommissioned
     *
     * @param boolean $recommissioned
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function setRecommissioned($recommissioned)
    {
        $this->recommissioned = $recommissioned;

        return $this;
    }

    /**
     * Get recommissioned
     *
     * @return boolean
     */
    public function getRecommissioned()
    {
        return $this->recommissioned;
    }
}
