<?php

namespace AppBundle\Entity\Finance\Tax;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Geography\Country;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Annotation\Entity;
use RuleBundle\Annotation\Property;

/**
 * Class VatGroup
 * @package AppBundle\Entity\Finance\Tax
 * @ORM\Entity()
 * @Entity(description="BTW Groep")
 */
class VatGroup
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(nullable=false)
     * @Property(description="Land", autoComplete={"id"="country.id", "label"="country.translate.description"})
     */
    protected $country;

    /**
     * @var VatLevel
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatLevel", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $level;

    /**
     * @var VatRate[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Finance\Tax\VatRate", mappedBy="group")
     */
    protected $rates;

    /**
     * @var Product[]|Collection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @Property(description="Product", autoComplete={"id"="product.id", "label"="product.translate.title"},
     *                                  relation="child")
     */
    protected $products;

    /**
     * VatGroup constructor.
     */
    public function __construct()
    {
        $this->rates = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLevel()->getTitle() . ' (' . $this->getCountry()->translate()->getName() . ')';
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return VatGroup
     */
    public function setCountry(Country $country): VatGroup
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return null|VatLevel
     */
    public function getLevel(): ?VatLevel
    {
        return $this->level;
    }

    /**
     * @param VatLevel $level
     * @return VatGroup
     */
    public function setLevel(VatLevel $level): VatGroup
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return VatRate[]|Collection
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param VatRate $rate
     * @return VatGroup
     */
    public function addRate(VatRate $rate): VatGroup
    {
        if (!$this->rates->contains($rate)) {
            $this->rates->add($rate);
        }

        return $this;
    }

    /**
     * @param VatRate $rate
     * @return VatGroup
     */
    public function removeRate(VatRate $rate): VatGroup
    {
        if ($this->rates->contains($rate)) {
            $this->rates->removeElement($rate);
        }

        return $this;
    }

    /**
     * @param \DateTime $dateTime
     * @param bool      $purchaseRate
     * @return VatRate|null
     * @throws \Exception
     */
    public function getRateByDate(\DateTime $dateTime = null, bool $purchaseRate = false): ?VatRate
    {
        if (null === $dateTime) {
            $dateTime = new \DateTime();
        }
        $currentTime = $dateTime->getTimestamp();

        foreach ($this->rates as $rate) {
            if (null !== $rate->getValidFrom() && null !== $rate->getValidUntil()) {
                $validFromTimestamp = $rate->getValidFrom()->getTimestamp();
                $validUntilTimestamp = $rate->getValidUntil()->getTimestamp();
                $isPurchase = $rate->isPurchase();

                if ((null === $isPurchase || $isPurchase === $purchaseRate) && $validFromTimestamp < $currentTime && $validUntilTimestamp >= $currentTime) {
                    return $rate;
                }
            }
        }

        return null;
    }

    /**
     * @return Product[]|Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }
}