<?php

namespace AppBundle\Entity\Finance\Tax;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VatLevel
 * @package AppBundle\Entity\Finance\Tax
 * @ORM\Entity()
 */
class VatLevel
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $special;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->title;
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VatLevel
     */
    public function setTitle(string $title): VatLevel
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return null|bool
     */
    public function isSpecial(): ?bool
    {
        return $this->special;
    }

    /**
     * @param bool $special
     * @return VatLevel
     */
    public function setSpecial(bool $special): VatLevel
    {
        $this->special = $special;

        return $this;
    }


}