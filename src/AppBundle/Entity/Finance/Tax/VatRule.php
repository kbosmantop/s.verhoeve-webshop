<?php

namespace AppBundle\Entity\Finance\Tax;

use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Rule;

/**
 * @ORM\Entity()
 * Class VatRule
 * @package AppBundle\Entity\Finance\Tax
 */
class VatRule
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $position;

    /**
     * @var Rule
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $allowSplit;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return VatRule
     */
    public function setPosition(int $position): VatRule
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Rule
     */
    public function getRule(): ?Rule
    {
        return $this->rule;
    }

    /**
     * @param Rule $rule
     * @return VatRule
     */
    public function setRule(Rule $rule): VatRule
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowSplit(): ?bool
    {
        return $this->allowSplit;
    }

    /**
     * @param bool $allowSplit
     * @return VatRule
     */
    public function setAllowSplit(bool $allowSplit): VatRule
    {
        $this->allowSplit = $allowSplit;

        return $this;
    }
}