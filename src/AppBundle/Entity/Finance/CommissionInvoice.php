<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Relation\Company;
use AppBundle\Utils\Commission\Invoice\VatSpecification;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 */
class CommissionInvoice
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="id")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommissionBatch", inversedBy="commissionInvoices")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $commissionBatch;

    /**
     * @ORM\OneToMany(targetEntity="CommissionInvoiceLine", mappedBy="commissionInvoice", cascade={"persist"})
     */
    protected $lines;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="CommissionInvoiceSupplierOrder", mappedBy="commissionInvoice", cascade={"persist"})
     */
    protected $commissionInvoiceSupplierOrders;

    /**
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * CommissionInvoice constructor.
     */
    public function __construct()
    {
        $this->commissionInvoiceSupplierOrders = new ArrayCollection();
        $this->lines = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return CommissionInvoice
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return 1000000 + $this->id;
    }

    /**
     * Set batch
     *
     * @param CommissionBatch $commissionBatch
     *
     * @return CommissionInvoice
     */
    public function setCommissionBatch(CommissionBatch $commissionBatch = null)
    {
        $this->commissionBatch = $commissionBatch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return CommissionBatch
     */
    public function getCommissionBatch()
    {
        return $this->commissionBatch;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CommissionInvoice
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add order
     *
     * @param CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrders
     *
     * @return CommissionInvoice
     */
    public function addCommissionInvoiceSupplierOrder(CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrders)
    {
        $commissionInvoiceSupplierOrders->setCommissionInvoice($this);

        $this->commissionInvoiceSupplierOrders[] = $commissionInvoiceSupplierOrders;

        return $this;
    }

    /**
     * Remove order
     *
     * @param CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrders
     */
    public function removeCommissionInvoiceSupplierOrder(CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrders)
    {
        $this->commissionInvoiceSupplierOrders->removeElement($commissionInvoiceSupplierOrders);
    }

    /**
     * Get orders
     *
     * @return Collection|CommissionInvoiceSupplierOrder[]
     */
    public function getCommissionInvoiceSupplierOrders()
    {
        return $this->commissionInvoiceSupplierOrders;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CommissionInvoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get due date
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        /** @var \DateTime $dueDate */
        $dueDate = clone $this->date;
        $dueDate->add(new \DateInterval("P14D"));

        return $dueDate;
    }

    /**
     * @return float
     */
    public function getOrderTotalIncl()
    {
        $subtotalIncl = 0;

        foreach ($this->getCommissionInvoiceSupplierOrders() as $commissionInvoiceSupplierOrder) {
            $subtotalIncl += $commissionInvoiceSupplierOrder->getTotalIncl();
        }

        return $subtotalIncl;
    }

    /**
     * @return float
     */
    public function getTotalIncl()
    {
        $totalIncl = 0;
        $totalIncl += $this->getOrderTotalIncl();

        foreach ($this->getLines() as $line) {
            $totalIncl += $line->getPriceIncl();
        }

        return round($totalIncl, 2);
    }

    /**
     * @return ArrayCollection|VatSpecification[]
     */
    public function getVatSpecifications()
    {
        $vatSpecifications = new ArrayCollection();

        foreach ($this->getCommissionInvoiceSupplierOrders() as $commissionInvoiceSupplierOrder) {
            foreach ($commissionInvoiceSupplierOrder->getCommissionInvoiceSupplierOrderLines() as $commissionInvoiceSupplierOrderLine) {
                if (!$commissionInvoiceSupplierOrderLine->getOrderLineTotalPrice()) {
                    continue;
                }

                $vatRate = $commissionInvoiceSupplierOrderLine->getVatRate();

                if (!$vatSpecifications->containsKey($vatRate->getId())) {
                    $vatSpecifications->set($vatRate->getId(),
                        new VatSpecification($commissionInvoiceSupplierOrderLine->getVatRate()));
                }

                $vatSpecification = $vatSpecifications->get($vatRate->getId());

                $price = $commissionInvoiceSupplierOrderLine->getOrderLineTotalPrice() * ((100 - $commissionInvoiceSupplierOrderLine->getPercentage()) / 100);

                $vatSpecification->add($price);
            }
        }


        return $vatSpecifications;
    }

    /**
     * Add line
     *
     * @param CommissionInvoiceLine $line
     *
     * @return CommissionInvoice
     */
    public function addLine(CommissionInvoiceLine $line)
    {
        $line->setCommissionInvoice($this);

        $this->lines[] = $line;

        return $this;
    }

    /**
     * Remove line
     *
     * @param CommissionInvoiceLine $line
     */
    public function removeLine(CommissionInvoiceLine $line)
    {
        $this->lines->removeElement($line);
    }

    /**
     * Get lines
     *
     * @return Collection|CommissionInvoiceLine[]
     */
    public function getLines()
    {
        return $this->lines;
    }
}
