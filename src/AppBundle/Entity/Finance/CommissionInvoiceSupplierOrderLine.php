<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class CommissionInvoiceSupplierOrderLine
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder", inversedBy="commissionInvoiceSupplierOrderLines")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $commissionInvoiceSupplierOrder;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierOrderLine")
     */
    protected $supplierOrderLine;

    /**
     * @ORM\Column(type="integer")
     */
    protected $percentage;

    /**
     * @ORM\Column(type="float")
     */
    protected $orderLineTotalPrice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatRate")
     */
    protected $vatRate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     *
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set commission
     *
     * @param CommissionInvoiceSupplierOrder|null $commissionInvoiceSupplierOrder
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setCommissionSupplierOrder(CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrder = null)
    {
        $this->commissionInvoiceSupplierOrder = $commissionInvoiceSupplierOrder;

        return $this;
    }

    /**
     * Get commissionInvoiceSupplierOrder
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function getCommissionSupplierOrder()
    {
        return $this->commissionInvoiceSupplierOrder;
    }

    /**
     * Set supplierOrderLine
     *
     * @param SupplierOrderLine|null $supplierOrderLine
     *
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setSupplierOrderLine(SupplierOrderLine $supplierOrderLine = null)
    {
        $this->supplierOrderLine = $supplierOrderLine;

        return $this;
    }

    /**
     * Get supplierOrderLine
     *
     * @return SupplierOrderLine
     */
    public function getSupplierOrderLine()
    {
        return $this->supplierOrderLine;
    }

    /**
     * Set orderLineTotal
     *
     * @param float $orderLineTotalPrice
     *
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setOrderLineTotalPrice($orderLineTotalPrice)
    {
        $this->orderLineTotalPrice = $orderLineTotalPrice;

        return $this;
    }

    /**
     * Get orderLineTotal
     *
     * @return float
     */
    public function getOrderLineTotalPrice()
    {
        return $this->orderLineTotalPrice;
    }

    /**
     * Get orderLineTotal
     *
     * @return float
     */
    public function getOrderLineTotalPriceIncl()
    {
        return $this->getOrderLineTotalPrice() * (1 + $this->vatRate->getFactor());
    }

    /**
     * Set commissionInvoiceSupplierOrder
     *
     * @param CommissionInvoiceSupplierOrder|null $commissionInvoiceSupplierOrder
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setCommissionInvoiceSupplierOrder(CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrder = null)
    {
        $this->commissionInvoiceSupplierOrder = $commissionInvoiceSupplierOrder;

        return $this;
    }

    /**
     * Get commissionInvoiceSupplierOrder
     *
     * @return CommissionInvoiceSupplierOrder
     */
    public function getCommissionInvoiceSupplierOrder()
    {
        return $this->commissionInvoiceSupplierOrder;
    }

    /**
     * Set vat
     *
     * @param VatRate|null $vatRate
     * @return CommissionInvoiceSupplierOrderLine
     */
    public function setVatRate(VatRate $vatRate = null)
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * Get vat
     *
     * @return VatRate
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }
}
