<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Ledger
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", length=32)
     */
    protected $name;

    /**
     * @ORM\Column(type="smallint", unique=true, options={ "unsigned"=true })
     */
    protected $number;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", mappedBy="ledger")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $productgroups;

    public function __toString()
    {
        return $this->getName() . " (" . $this->getNumber() . ")";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ledger
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Ledger
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productgroups = new ArrayCollection();
    }

    /**
     * Add productgroup
     *
     * @param Productgroup $productgroup
     *
     * @return Ledger
     */
    public function addProductgroup(Productgroup $productgroup)
    {
        $this->productgroups[] = $productgroup;

        return $this;
    }

    /**
     * Remove productgroup
     *
     * @param Productgroup $productgroup
     */
    public function removeProductgroup(Productgroup $productgroup)
    {
        $this->productgroups->removeElement($productgroup);
    }

    /**
     * Get productgroups
     *
     * @return Collection
     */
    public function getProductgroups()
    {
        return $this->productgroups;
    }
}
