<?php

namespace AppBundle\Entity\Geography;

use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Media\Image\ImageInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class DeliveryAddressTypeField
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\DeliveryAddressType", inversedBy="fields")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $deliveryAddressType;

    /**
     * @ORM\Column(type="string", length=30)
     *
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $required;

    /**
     * @ORM\Column(type="integer")
     *
     */
    protected $position;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DeliveryAddressTypeField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return DeliveryAddressTypeField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return DeliveryAddressTypeField
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set deliveryAddressType
     *
     * @param DeliveryAddressType $deliveryAddressType
     *
     * @return DeliveryAddressTypeField
     */
    public function setDeliveryAddressType(DeliveryAddressType $deliveryAddressType)
    {
        $this->deliveryAddressType = $deliveryAddressType;

        return $this;
    }

    /**
     * Get deliveryAddressType
     *
     * @return DeliveryAddressType
     */
    public function getDeliveryAddressType()
    {
        return $this->deliveryAddressType;
    }
}
