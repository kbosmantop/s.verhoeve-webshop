<?php

namespace AppBundle\Entity\Geography;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity
 * @Rule\Entity(description="Land")
 * @method CountryTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class Country
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=2)
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Geography\Province", mappedBy="country")
     */
    private $provinces;

    public function __toString()
    {
        $translation = $this->translate()->getName();

        if (!$translation) {
            return (string)$this->getId();
        }

        return (string)$translation;
    }

    public function getSlug($language)
    {
        return Urlizer::urlize($this->translate($language)->getName());
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->id = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->provinces = new ArrayCollection();
    }

    /**
     * Add province
     *
     * @param Province $province
     *
     * @return Country
     */
    public function addProvince(Province $province)
    {
        $this->provinces[] = $province;

        return $this;
    }

    /**
     * Remove province
     *
     * @param Province $province
     */
    public function removeProvince(Province $province)
    {
        $this->provinces->removeElement($province);
    }

    /**
     * Get provinces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProvinces()
    {
        return $this->provinces;
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Country
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
