<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CityTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    private $slug;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CityTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CityTranslation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @ORM\PrePersist
     */
    public function generateSlug()
    {
        if ($this->slug) {
            return;
        }

        /** @var City $city */
        $city = $this->getTranslatable();

        $province = $city->getProvince()->translate($this->getLocale());

        if (!$province->getSlug()) {
            return;
        }

        $this->setSlug($province->getSlug() . "/" . Urlizer::urlize($this->name));
    }
}
