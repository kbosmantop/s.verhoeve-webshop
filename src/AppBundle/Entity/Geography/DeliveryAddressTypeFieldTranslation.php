<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 */
class DeliveryAddressTypeFieldTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     */
    protected $label;

    /**
     * Set label
     *
     * @param string $label
     *
     * @return DeliveryAddressTypeFieldTranslation
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}
