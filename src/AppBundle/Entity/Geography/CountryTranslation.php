<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Locale;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"slug", "locale"})
 * })
 * @ORM\HasLifecycleCallbacks
 *
 * @method Country getTranslatable
 */
class CountryTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(length=128)
     */
    private $slug;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CountryTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        if (!$this->name) {
            return Locale::getDisplayRegion('sl-Latn-' . $this->getTranslatable()->getCode() . '-nedis',
                $this->getLocale());
        }

        return $this->name;
    }

    /**
     * @ORM\PrePersist
     */
    public function generateSlug()
    {
        $this->setSlug(Urlizer::urlize($this->name));
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CountryTranslation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
