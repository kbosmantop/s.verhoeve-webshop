<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class PostcodeRepository
 * @package AppBundle\Entity\Geography
 */
class PostcodeRepository extends EntityRepository
{
    /**
     * @param string $country
     * @param string $postcode
     * @return Postcode|null
     */
    public function findOneByCountryAndPostcode(string $country, string $postcode): ?Postcode
    {
        $qb = $this->createQueryBuilder('postcode');
        $qb->leftJoin('postcode.province', 'province');
        $qb->leftJoin('province.country', 'country');
        $qb->andWhere('country.id = :country');
        $qb->setParameter('country', $country);
        $qb->andWhere('postcode.postcode = :postcode');
        $qb->setParameter('postcode', (int) $postcode);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // this won't happen because of database constraints
            return null;
        }
    }
}
