<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 */
class DeliveryAddressTypeTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     */
    protected $companyLabel;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     */
    protected $recipientLabel;

    /**
     * Set companyLabel
     *
     * @param string $companyLabel
     *
     * @return DeliveryAddressTypeTranslation
     */
    public function setCompanyLabel($companyLabel)
    {
        $this->companyLabel = $companyLabel;

        return $this;
    }

    /**
     * Get companyLabel
     *
     * @return string
     */
    public function getCompanyLabel()
    {
        return $this->companyLabel;
    }

    /**
     * Set recipientLabel
     *
     * @param string $recipientLabel
     *
     * @return DeliveryAddressTypeTranslation
     */
    public function setRecipientLabel($recipientLabel)
    {
        $this->recipientLabel = $recipientLabel;

        return $this;
    }

    /**
     * Get recipientLabel
     *
     * @return string
     */
    public function getRecipientLabel()
    {
        return $this->recipientLabel;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DeliveryAddressTypeTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
