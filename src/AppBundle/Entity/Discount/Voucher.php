<?php

namespace AppBundle\Entity\Discount;

use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\OrderCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\DateTime;
use FS\SolrBundle\Doctrine\Annotation as Solr;

/**
 * Class Voucher
 * @package AppBundle\Entity
 *
 * @Solr\Document(index="voucher")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Discount\VoucherRepository")
 * @UniqueEntity(fields={"code"})
 */
class Voucher
{
    use TimestampableEntity;

    /**
     * @var int $id
     *
     * @Solr\Id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Discount $discount
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discount\Discount", inversedBy="vouchers")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $discount;

    /**
     * @Solr\Field(type="integers", getter="getId")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="vouchers")
     */
    protected $orderCollections;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Order\Cart", inversedBy="vouchers")
     */
    protected $carts;

    /**
     * @var $description
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $description;

    /**
     * @var string $code
     *
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=32)
     */
    protected $code;

    /**
     * @var $validFrom
     *
     * @ORM\Column(type="datetime")
     */
    protected $validFrom;

    /**
     * @var $validUntil
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $validUntil;

    /**
     * @var $usage
     *
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\VoucherUsageType")
     * @ORM\Column(name="`usage`", type="VoucherUsageType", nullable=false)
     */
    protected $usage;

    /**
     * @var int $limit
     *
     * @ORM\Column(name="`limit`", type="integer", length=3, nullable=true)
     */
    protected $limit;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discount\VoucherBatch", inversedBy="vouchers")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $batch;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderCollections = new ArrayCollection();
        $this->carts = new ArrayCollection();
        $this->usage = 'once';
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Discount $discount
     * @return Voucher
     */
    public function setDiscount(Discount $discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @return Discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param $validFrom
     * @return $this
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * @param $validUntil
     * @return $this
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param $usage
     * @return $this
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * @param $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set batch
     *
     * @param VoucherBatch $batch
     *
     * @return Voucher
     */
    public function setBatch(VoucherBatch $batch = null)
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return VoucherBatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * Add orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return Voucher
     */
    public function addOrderCollection(OrderCollection $orderCollection)
    {
        $this->orderCollections[] = $orderCollection;

        return $this;
    }

    /**
     * Remove orderCollection
     *
     * @param OrderCollection $orderCollection
     */
    public function removeOrderCollection(OrderCollection $orderCollection)
    {
        $this->orderCollections->removeElement($orderCollection);
    }

    /**
     * Get orderCollections
     *
     * @return Collection
     */
    public function getOrderCollections()
    {
        return $this->orderCollections;
    }

    /**
     * Add cart
     *
     * @param Cart $cart
     *
     * @return Voucher
     */
    public function addCart(Cart $cart)
    {
        $this->carts[] = $cart;

        return $this;
    }

    /**
     * Remove cart
     *
     * @param Cart $cart
     */
    public function removeCart(Cart $cart)
    {
        $this->carts->removeElement($cart);
    }

    /**
     * Get carts
     *
     * @return Collection
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * @return int
     */
    public function getUsedVouchersCount(): int
    {
        return $this->getOrderCollections()->count();
    }
}
