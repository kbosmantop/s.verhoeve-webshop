<?php

namespace AppBundle\Entity\Discount;

use AppBundle\Entity\Security\Employee\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * Class VoucherBatch
 * @package AppBundle\Entity\Discount
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Discount\VoucherBatchRepository")
 */
class VoucherBatch
{

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=5)
     * @Assert\Length(max="5", maxMessage="Maximaal 5 tekens toegestaan.")
     */
    protected $prefix;

    /**
     * @var $validFrom
     *
     * @ORM\Column(type="datetime")
     */
    protected $validFrom;

    /**
     * @var $validUntil
     *
     * @ORM\Column(type="datetime")
     */
    protected $validUntil;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Discount\Voucher", mappedBy="batch", cascade={"persist", "remove"})
     */
    protected $vouchers;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     */
    protected $user;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vouchers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return VoucherBatch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     *
     * @return VoucherBatch
     */
    public function setPrefix($prefix = null)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Add voucher
     *
     * @param Voucher $voucher
     *
     * @return VoucherBatch
     */
    public function addVoucher(Voucher $voucher)
    {
        $this->vouchers[] = $voucher;

        return $this;
    }

    /**
     * Remove voucher
     *
     * @param Voucher $voucher
     */
    public function removeVoucher(Voucher $voucher)
    {
        $this->vouchers->removeElement($voucher);
    }

    /**
     * Get vouchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * Set validFrom
     *
     * @param \DateTime $validFrom
     *
     * @return VoucherBatch
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * Get validFrom
     *
     * @return \DateTime
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * Set validUntil
     *
     * @param \DateTime $validUntil
     *
     * @return VoucherBatch
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    /**
     * Get validUntil
     *
     * @return \DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return VoucherBatch
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }
}
