<?php

namespace AppBundle\Entity\Designer;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DesignerCollageObject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Designer\DesignerCollage", inversedBy="objects")
     */
    protected $collage;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $x;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $y;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $width;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $height;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DesignerCollageObject
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set x
     *
     * @param float $x
     *
     * @return DesignerCollageObject
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param float $y
     *
     * @return DesignerCollageObject
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set width
     *
     * @param float $width
     *
     * @return DesignerCollageObject
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param float $height
     *
     * @return DesignerCollageObject
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set collage
     *
     * @param DesignerCollage $collage
     *
     * @return DesignerCollageObject
     */
    public function setCollage(DesignerCollage $collage = null)
    {
        $this->collage = $collage;

        return $this;
    }

    /**
     * Get collage
     *
     * @return DesignerCollage
     */
    public function getCollage()
    {
        return $this->collage;
    }
}
