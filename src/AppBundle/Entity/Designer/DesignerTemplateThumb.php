<?php

namespace AppBundle\Entity\Designer;

use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class DesignerTemplateThumb implements ImageInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $image;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Designer\DesignerTemplate", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    protected $template;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return "images/designer/" . $this->template->getId();
    }

    /**
     * @return string
     */
    public function generateName()
    {
        return 'thumb.jpg';
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param $path
     * @return mixed|void
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'path';
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DesignerTemplateThumb
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return DesignerTemplateThumb
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get template
     *
     * @return DesignerTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set template
     *
     * @param DesignerTemplate $template
     *
     * @return DesignerTemplateThumb
     */
    public function setTemplate(DesignerTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }
}
