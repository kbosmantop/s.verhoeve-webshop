<?php

namespace AppBundle\Entity\Designer;

use AppBundle\Entity\Relation\Company;
use AppBundle\Interfaces\ImageInterface;
use AppBundle\Model\Designer\TemplateModel;
use AppBundle\Traits\ImageEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @method DesignerTemplateTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class DesignerTemplate extends TemplateModel implements ImageInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=36, nullable=false)
     */
    protected $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Designer\DesignerCanvas")
     */
    protected $canvas;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $image;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Designer\Tag")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $tags;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagePath;

    /**
     * @var Company|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="templates")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $company;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DesignerTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return DesignerTemplate
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get canvas
     *
     * @return DesignerCanvas
     */
    public function getCanvas()
    {
        return $this->canvas;
    }

    /**
     * Set canvas
     *
     * @param DesignerCanvas $canvas
     *
     * @return DesignerTemplate
     */
    public function setCanvas(DesignerCanvas $canvas = null)
    {
        $this->canvas = $canvas;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return DesignerTemplate
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     *
     * @return DesignerTemplate
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return Collection|Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function generateName()
    {
        $name = $this->translate("nl_NL")->getTitle();

        return Urlizer::urlize($name);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->getImagePath();
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'imagePath';
    }

    /**
     * @param $path
     * @return mixed|void
     */
    public function setPath($path)
    {
        $this->{$this->getPathColumn()} = $path;
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return "images/designer_template/" . $this->getId();
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return DesignerTemplate
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     */
    public function setCompany(?Company $company): void
    {
        $this->company = $company;
    }

}
