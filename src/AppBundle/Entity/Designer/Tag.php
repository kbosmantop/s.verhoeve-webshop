<?php

namespace AppBundle\Entity\Designer;

use AppBundle\Model\Designer\TagModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="designer_tag")
 */
class Tag extends TagModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Designer\DesignerTemplate", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @ORM\OrderBy({"name" = "ASC"})
     */
    protected $templates;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->templates = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Add template
     *
     * @param DesignerTemplate $template
     *
     * @return Tag
     */
    public function addTemplate(DesignerTemplate $template)
    {
        $this->templates[] = $template;

        return $this;
    }

    /**
     * Remove template
     *
     * @param DesignerTemplate $template
     */
    public function removeTemplate(DesignerTemplate $template)
    {
        $this->templates->removeElement($template);
    }

    /**
     * Get templates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }
}
