<?php

namespace AppBundle\Entity\Common;

/**
 * Interface TimeslotEntityInterface
 * @package AppBundle\Entity\Common
 */
interface TimeslotEntityInterface{
    /**
     * Set timeslots
     *
     * @param Timeslot[] $timeslots
     */
    public function setTimeslots($timeslots);


    /**
     * Add timeslot
     *
     * @param Timeslot $timeslot
     */
    public function addTimeslot(Timeslot $timeslot);

    /**
     * Remove timeslot
     *
     * @param Timeslot $timeslot
     */
    public function removeTimeslot(Timeslot $timeslot);

    /**
     * Remove timeslots
     */
    public function removeTimeslots();

    /**
     * Get timeslots
     */
    public function getTimeslots();
}