<?php

namespace AppBundle\Entity\Common\DataSet;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DataSetFilterFieldOption
 * @package AppBundle\Entity\Common\DataSet
 *
 * @ORM\Entity()
 */
class DataSetFilterFieldOption
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Common\DataSet\DataSetFilterField", inversedBy="dataSetFilterFieldOptions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $dataSetFilterField;

    /**
     * @Assert\NotBlank(message = "dataset.option.operator.not_blank")
     * @ORM\Column(type="string", nullable=false)
     */
    protected $operator;

    /**
     * @Assert\NotBlank(message = "dataset.option.label.not_blank")
     * @ORM\Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $value;

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return DataSetFilterFieldOption
     */
    public function setId($id): DataSetFilterFieldOption
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DataSetFilterField
     */
    public function getDataSetFilterField(): DataSetFilterField
    {
        return $this->dataSetFilterField;
    }

    /**
     * @param mixed $dataSetFilterField
     *
     * @return DataSetFilterFieldOption
     */
    public function setDataSetFilterField(DataSetFilterField $dataSetFilterField): DataSetFilterFieldOption
    {
        $this->dataSetFilterField = $dataSetFilterField;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     *
     * @return DataSetFilterFieldOption
     */
    public function setOperator(string $operator): DataSetFilterFieldOption
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return DataSetFilterFieldOption
     */
    public function setValue($value): DataSetFilterFieldOption
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     *
     * @return DataSetFilterFieldOption
     */
    public function setLabel(string $label): DataSetFilterFieldOption
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @param mixed $position
     *
     * @return DataSetFilterFieldOption
     */
    public function setPosition(?int $position): DataSetFilterFieldOption
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }
}
