<?php

namespace AppBundle\Entity\Common\DataSet;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RuleBundle\Entity\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DataSetFilterField
 * @package AppBundle\Entity\Common\DataSet
 *
 * @ORM\Entity()
 */
class DataSetFilterField
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Common\DataSet\DataSetFilterFieldOption")
     */
    protected $parentOption;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Common\DataSet\DataSetFilterFieldOption", mappedBy="dataSetFilterField",
     *                                                                                         cascade={"persist",
     *                                                                                         "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Assert\Valid(traverse=true)
     */
    protected $dataSetFilterFieldOptions;

    /**
     * @Assert\NotBlank(message = "dataset.entity.not_blank")
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Entity", inversedBy="dataSetFilterFields")
     */
    protected $entity;

    /**
     * @Assert\NotBlank(message = "dataset.form_type.not_blank")
     * @ORM\Column(type="string")
     */
    protected $formType;

    /**
     * @Assert\NotBlank(message = "dataset.path.not_blank")
     * @ORM\Column(type="string")
     */
    protected $path;

    /**
     * @Assert\NotBlank(message = "dataset.label.not_blank")
     * @ORM\Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $placeholder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $multiple;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pinned;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    protected $position;

    /**
     * DataSetFilterField constructor.
     */
    public function __construct()
    {
        $this->dataSetFilterFieldOptions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param DataSetFilterFieldOption $dataSetFilterFieldOption
     *
     * @return DataSetFilterField
     */
    public function addDataSetFilterFieldOption(DataSetFilterFieldOption $dataSetFilterFieldOption): DataSetFilterField
    {
        if (!$this->dataSetFilterFieldOptions->contains($dataSetFilterFieldOption)) {
            $dataSetFilterFieldOption->setDataSetFilterField($this);

            $this->dataSetFilterFieldOptions->add($dataSetFilterFieldOption);
        }

        return $this;
    }

    /**
     * @param DataSetFilterFieldOption $dataSetFilterFieldOption
     *
     * @return DataSetFilterField
     */
    public function removeDataSetFilterFieldOption(DataSetFilterFieldOption $dataSetFilterFieldOption
    ): DataSetFilterField {
        if (!$this->dataSetFilterFieldOptions->contains($dataSetFilterFieldOption)) {
            $this->dataSetFilterFieldOptions->removeElement($dataSetFilterFieldOption);
        }

        return $this;
    }

    /**
     * @return Collection|DataSetFilterFieldOption[]
     */
    public function getDataSetFilterFieldOptions(): Collection
    {
        return $this->dataSetFilterFieldOptions;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return DataSetFilterField
     */
    public function setPath(string $path): DataSetFilterField
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return DataSetFilterField
     */
    public function setLabel(string $label): DataSetFilterField
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param null|string $placeholder
     *
     * @return DataSetFilterField
     */
    public function setPlaceholder(?string $placeholder): DataSetFilterField
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * Get placeholder
     *
     * @return null|string
     */
    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    /**
     * Set pinned
     *
     * @param boolean $pinned
     *
     * @return DataSetFilterField
     */
    public function setPinned(bool $pinned): DataSetFilterField
    {
        $this->pinned = $pinned;

        return $this;
    }

    /**
     * @return null|bool
     */
    public function getPinned(): ?bool
    {
        return $this->pinned;
    }

    /**
     * Is pinned
     *
     * @return bool
     */
    public function isPinned(): bool
    {
        return (bool)$this->getPinned();
    }

    /**
     * Set position
     *
     * @param null|int $position
     *
     * @return DataSetFilterField
     */
    public function setPosition(?int $position): DataSetFilterField
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return null|int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * Set parentOption
     *
     * @param null|DataSetFilterFieldOption $parentOption
     *
     * @return DataSetFilterField
     */
    public function setParentOption(?DataSetFilterFieldOption $parentOption): DataSetFilterField
    {
        $this->parentOption = $parentOption;

        return $this;
    }

    /**
     * Get parentOption
     *
     * @return DataSetFilterFieldOption
     */
    public function getParentOption(): ?DataSetFilterFieldOption
    {
        return $this->parentOption;
    }

    /**
     * Set entity
     *
     * @param Entity $entity
     *
     * @return DataSetFilterField
     */
    public function setEntity(Entity $entity): DataSetFilterField
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @return string|null
     */
    public function getFormType(): ?string
    {
        return $this->formType;
    }

    /**
     * @param string $formType
     *
     * @return DataSetFilterField
     */
    public function setFormType(string $formType): DataSetFilterField
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMultiple(): ?bool
    {
        return $this->multiple;
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return (bool)$this->getMultiple();
    }

    /**
     * @param int $multiple
     *
     * @return DataSetFilterField
     */
    public function setMultiple($multiple): DataSetFilterField
    {
        $this->multiple = $multiple;

        return $this;
    }
}
