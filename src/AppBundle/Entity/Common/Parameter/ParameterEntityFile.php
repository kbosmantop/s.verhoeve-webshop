<?php

namespace AppBundle\Entity\Common\Parameter;

use AppBundle\Interfaces\FileInterface;
use AppBundle\Traits\UuidEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class ParameterEntityFile
 *
 * @ORM\Entity
 * @package AppBundle\Entity\Common\Parameter
 */
class ParameterEntityFile implements FileInterface
{
    use TimestampableEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $description;

    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'path';
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        $uuid = $this->getUuid()->toString();

        return '/common/files/' . substr($uuid, 0, 8) . '/' . $uuid;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ParameterEntityFile
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ParameterEntityFile
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
