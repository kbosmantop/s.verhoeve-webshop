<?php

namespace AppBundle\Entity\Carrier;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class DeliveryMethod
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Carrier\Carrier", mappedBy="deliveryMethods")
     */
    private $carriers;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Length(min=1, max=20, minMessage="Naam mag niet leeg zijn", maxMessage="Naam maximum lengte: {{ limit }}")
     */
    protected $name;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->name = '';
        $this->carriers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Add carrier
     *
     * @param Carrier $carrier
     *
     * @return DeliveryMethod
     */
    public function addCarrier(Carrier $carrier): DeliveryMethod
    {
        if (!$this->carriers->contains($carrier)) {
            $this->carriers->add($carrier);
            $carrier->addDeliveryMethod($this);
        }

        return $this;
    }

    /**
     * Remove carrier
     *
     * @param Carrier $carrier
     * @return DeliveryMethod
     */
    public function removeCarrier(Carrier $carrier): DeliveryMethod
    {
        if ($this->carriers->contains($carrier)) {
            $this->carriers->removeElement($carrier);
            $carrier->removeDeliveryMethod($this);
        }

        return $this;
    }

    /**
     * @ORM\PreRemove()
     * @return DeliveryMethod
     */
    public function removeAllDeliveryMethods(): DeliveryMethod
    {
        foreach($this->carriers as $carrier){
            $this->carriers->removeElement($carrier);
            $carrier->removeDeliveryMethod($this);
        }
        return $this;
    }

    /**
     * Get carriers
     *
     * @return Collection
     */
    public function getCarriers(): Collection
    {
        return $this->carriers;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DeliveryMethod
     */
    public function setName(string $name): DeliveryMethod
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
