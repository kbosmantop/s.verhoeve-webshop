<?php

namespace AppBundle\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Order\Order;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class OrderGiftCard
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class OrderGiftCard
{
    use TimestampableEntity;

    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Order $orderId
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $order;

    /**
     * @var Product $product
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $product;

    /**
     * @Encrypted
     * @ORM\Column(type="text", nullable=true)
     */
    protected $number;

    /**
     * @var float $value
     * @ORM\Column(type="float", length=32, nullable=true)
     */
    protected $value;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return OrderGiftCard
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get maskedNumber
     *
     * @return string
     */
    public function getMaskedNumber()
    {
        return str_repeat('*', strlen($this->number) - 4) . substr($this->number, -4);
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return OrderGiftCard
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set orderId
     *
     * @param Order $order
     *
     * @return OrderGiftCard
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set productId
     *
     * @param Product $product
     *
     * @return OrderGiftCard
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get productId
     *
     * @return Product
     */
    public function getProductId()
    {
        return $this->product;
    }
}
