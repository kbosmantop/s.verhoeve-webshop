<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SerialNumberMeta
 * @package AppBundle\Entity\Catalog\Product
 * @ORM\Entity()
 */
class SerialNumberMeta {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level;

    /**
     * @ORM\Column(type="string")
     */
    protected $fixSer;

    /**
     * @ORM\Column(type="string")
     */
    protected $mask;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return SerialNumberMeta
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set fixSer
     *
     * @param string $fixSer
     *
     * @return SerialNumberMeta
     */
    public function setFixSer($fixSer)
    {
        $this->fixSer = $fixSer;

        return $this;
    }

    /**
     * Get fixSer
     *
     * @return string
     */
    public function getFixSer()
    {
        return $this->fixSer;
    }

    /**
     * Set mask
     *
     * @param string $mask
     *
     * @return SerialNumberMeta
     */
    public function setMask($mask)
    {
        $this->mask = $mask;

        return $this;
    }

    /**
     * Get mask
     *
     * @return string
     */
    public function getMask()
    {
        return $this->mask;
    }
}
