<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PackagingUnit
 *
 * @ORM\Entity()
 * @package AppBundle\Entity\Catalog\Product\Product
 */
class PackagingUnit
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=3)
     * @Assert\Length(min=2, max=3)
     */
    protected $code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    public function __toString()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->code;
    }

    /**
     * Get GDSN packaging unit code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set GDSN packaging unit code
     *
     * @param string $gdsnCode
     *
     * @return PackagingUnit
     */
    public function setCode($gdsnCode)
    {
        $this->code = $gdsnCode;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PackagingUnit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
