<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class letter
 * @package AppBundle\Entity\Catalog\Product
 *
 * @ORM\Entity()
 */
class Letter extends Product
{
    use PublishableEntity;

    /**
     * @var boolean
     */
    protected $purchasable = true;
}
