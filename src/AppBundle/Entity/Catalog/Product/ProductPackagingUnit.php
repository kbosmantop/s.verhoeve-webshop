<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class ProductPackagingUnit
 * @package AppBundle\Entity\Catalog\Product\Product
 */
class ProductPackagingUnit
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="productPackagingUnits")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\PackagingUnit")
     * @ORM\JoinColumn(referencedColumnName="code")
     */
    protected $packagingUnit;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    protected $dimensionsLength;

    /**
     * @ORM\Column(type="integer")
     */
    protected $dimensionsWidth;

    /**
     * @ORM\Column(type="integer")
     */
    protected $dimensionsHeight;

    /**
     * @ORM\Column(type="integer")
     */
    protected $weight;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductPackagingUnit
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get dimensionsLength
     *
     * @return float
     */
    public function getDimensionsLength()
    {
        return $this->dimensionsLength;
    }

    /**
     * Set dimensionsLength
     *
     * @param float $dimensionsLength
     *
     * @return ProductPackagingUnit
     */
    public function setDimensionsLength($dimensionsLength)
    {
        $this->dimensionsLength = $dimensionsLength;

        return $this;
    }

    /**
     * Get dimensionsWidth
     *
     * @return float
     */
    public function getDimensionsWidth()
    {
        return $this->dimensionsWidth;
    }

    /**
     * Set dimensionsWidth
     *
     * @param float $dimensionsWidth
     *
     * @return ProductPackagingUnit
     */
    public function setDimensionsWidth($dimensionsWidth)
    {
        $this->dimensionsWidth = $dimensionsWidth;

        return $this;
    }

    /**
     * Get dimensionsHeight
     *
     * @return float
     */
    public function getDimensionsHeight()
    {
        return $this->dimensionsHeight;
    }

    /**
     * Set dimensionsHeight
     *
     * @param float $dimensionsHeight
     *
     * @return ProductPackagingUnit
     */
    public function setDimensionsHeight($dimensionsHeight)
    {
        $this->dimensionsHeight = $dimensionsHeight;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductPackagingUnit
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get packagingUnit
     *
     * @return PackagingUnit
     */
    public function getPackagingUnit()
    {
        return $this->packagingUnit;
    }

    /**
     * Set packagingUnit
     *
     * @param PackagingUnit $packagingUnit
     *
     * @return ProductPackagingUnit
     */
    public function setPackagingUnit(PackagingUnit $packagingUnit = null)
    {
        $this->packagingUnit = $packagingUnit;

        return $this;
    }

    /**
     * @return integer
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * @param $weight
     * @return $this
     */
    public function setWeight($weight) {
        $this->weight = $weight;

        return $this;
    }
}
