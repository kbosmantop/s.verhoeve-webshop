<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ExpirationMeta
 * @package AppBundle\Entity\Catalog\Product
 * @ORM\Entity()
 */
class ExpirationMeta {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $period;

    /**
     * @ORM\Column(type="integer")
     */
    protected $guarantee;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set period
     *
     * @param integer $period
     *
     * @return ExpirationMeta
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return integer
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set guarantee
     *
     * @param $guarantee
     * @return ExpirationMeta
     */
    public function setGuarantee($guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee
     *
     * @return integer
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }
}
