<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Rule\Entity(description="BTW")
 */
class Vat
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", length=32)
     * @Rule\Property(description="BTW percentage")
     */
    protected $description;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->description . ' (' . $this->getPercentage() . '%)';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Vat
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPercentage()
    {
        switch ($this->id) {
            case 1:
                return 0;
            case 2:
                return 6;
            case 3:
                return 21;
            case 4: //todo VAT WEB-2869
                return 0;
            default:
                return null;
        }
    }
}
