<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\DBAL\Types\ProductgroupPropertyTargetType;
use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @UniqueEntity(fields={"key"})
 * @method ProductgroupPropertyTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class ProductgroupProperty
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="`key`", type="string", length=40, nullable=true, unique=true)
     *
     */
    protected $key;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ProductgroupPropertyTypeType")
     * @ORM\Column(type="ProductgroupPropertyTypeType", nullable=false)
     *
     */
    protected $formType;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     *
     */
    protected $formTypeOptions;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     *
     */
    protected $suffix;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $configurable;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $filterable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isUnique;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $onFrontend;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption", mappedBy="productgroupProperty", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $options;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $assortment;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $decorator_service;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertySet", mappedBy="productgroupProperties")
     */
    protected $productgroupPropertySets;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", inversedBy="productgroupProperties")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $productgroups;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->translate()->getDescription();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productgroups = new ArrayCollection();
        $this->options = new ArrayCollection();
        $this->productgroupPropertySets = new ArrayCollection();
    }

    /**
     * Add productgroup
     *
     * @param Productgroup $productgroup
     *
     * @return ProductgroupProperty
     */
    public function addProductgroup(Productgroup $productgroup)
    {
        $this->productgroups[] = $productgroup;

        return $this;
    }

    /**
     * Remove productgroup
     *
     * @param Productgroup $productgroup
     */
    public function removeProductgroup(Productgroup $productgroup)
    {
        $this->productgroups->remove($productgroup);
    }

    /**
     * Get productgroups
     *
     * @return Collection
     */
    public function getProductgroups()
    {
        return $this->productgroups;
    }

    /**
     * Add option
     *
     * @param ProductgroupPropertyOption $option
     *
     * @return ProductgroupProperty
     */
    public function addOption(ProductgroupPropertyOption $option)
    {
        $option->setProductgroupProperty($this);

        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param ProductgroupPropertyOption $option
     */
    public function removeOption(ProductgroupPropertyOption $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return Collection
     */
    public function getOptions()
    {
        return $this->options;

//        switch ($this->formType) {
//            case ProductgroupPropertyTypeType::ENTITY:
//                if ($this->getProduct()) {
//                    return $this->getProduct()->getVariations();
//                }
//            default:
//                return $this->options;
//        }
    }

    /**
     * @deprecated
     * Set target
     *
     * @param ProductgroupPropertyTargetType $target
     *
     * @return ProductgroupProperty
     */
    public function setTarget($target)
    {
        return $this;
    }

    /**
     * @deprecated
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return 'product';
    }

    /**
     * Set formType
     *
     * @param ProductgroupPropertyTypeType $formType
     *
     * @return ProductgroupProperty
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * Get formType
     *
     * @return ProductgroupPropertyTypeType
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * Set formTypeOptions
     *
     * @param array $formTypeOptions
     *
     * @return ProductgroupProperty
     */
    public function setFormTypeOptions($formTypeOptions)
    {
        $this->formTypeOptions = $formTypeOptions;

        return $this;
    }

    /**
     * Get formTypeOptions
     *
     * @return array
     */
    public function getFormTypeOptions()
    {
        return $this->formTypeOptions;
    }

    /**
     * Set configurable
     *
     * @param boolean $configurable
     *
     * @return ProductgroupProperty
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable
     *
     * @return boolean
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductgroupProperty
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Catalog\Product\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set suffix
     *
     * @param string $suffix
     *
     * @return ProductgroupProperty
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    /**
     * Get suffix
     *
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Set decoratorService
     *
     * @param string $decoratorService
     *
     * @return ProductgroupProperty
     */
    public function setDecoratorService($decoratorService)
    {
        $this->decorator_service = $decoratorService;

        return $this;
    }

    /**
     * Get decoratorService
     *
     * @return string
     */
    public function getDecoratorService()
    {
        return $this->decorator_service;
    }

    /**
     * Set assortment
     *
     * @param Assortment $assortment
     *
     * @return ProductgroupProperty
     */
    public function setAssortment(Assortment $assortment = null)
    {
        $this->assortment = $assortment;

        return $this;
    }

    /**
     * Get assortment
     *
     * @return Assortment
     */
    public function getAssortment()
    {
        return $this->assortment;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return ProductgroupProperty
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Add productgroupPropertySet
     *
     * @param ProductgroupPropertySet $productgroupPropertySet
     *
     * @return ProductgroupProperty
     */
    public function addProductgroupPropertySet(
        ProductgroupPropertySet $productgroupPropertySet
    ) {
        $this->productgroupPropertySets[] = $productgroupPropertySet;

        return $this;
    }

    /**
     * Remove productgroupPropertySet
     *
     * @param ProductgroupPropertySet $productgroupPropertySet
     */
    public function removeProductgroupPropertySet(
        ProductgroupPropertySet $productgroupPropertySet
    ) {
        $this->productgroupPropertySets->removeElement($productgroupPropertySet);
    }

    /**
     * Get productgroupPropertySets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductgroupPropertySets()
    {
        return $this->productgroupPropertySets;
    }

    /**
     * Set filterable
     *
     * @param boolean $filterable
     *
     * @return ProductgroupProperty
     */
    public function setFilterable($filterable)
    {
        $this->filterable = $filterable;

        return $this;
    }

    /**
     * Get filterable
     *
     * @return boolean
     */
    public function getFilterable()
    {
        return $this->filterable;
    }

    /**
     * Set isUnique
     *
     * @param boolean $isUnique
     *
     * @return ProductgroupProperty
     */
    public function setIsUnique($isUnique)
    {
        $this->isUnique = $isUnique;

        return $this;
    }

    /**
     * Get isUnique
     *
     * @return boolean
     */
    public function getIsUnique()
    {
        return $this->isUnique;
    }

    /**
     * Set onFrontend
     *
     * @param boolean $onFrontend
     *
     * @return ProductgroupProperty
     */
    public function setOnFrontend($onFrontend)
    {
        $this->onFrontend = $onFrontend;

        return $this;
    }

    /**
     * Get onFrontend
     *
     * @return boolean
     */
    public function getOnFrontend()
    {
        return $this->onFrontend;
    }
}
