<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Site\Usp;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class ProductUsp
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\GenericProduct", inversedBy="usps")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Usp")
     */
    protected $usp;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;


    /**
     * Set position
     *
     * @param integer $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set usp
     *
     * @param Usp $usp
     *
     * @return $this
     */
    public function setUsp(Usp $usp)
    {
        $this->usp = $usp;

        return $this;
    }

    /**
     * Get usp
     *
     * @return Usp
     */
    public function getUsp()
    {
        return $this->usp;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductUsp
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
