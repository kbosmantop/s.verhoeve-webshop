<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\ThirdParty\Google\Entity\Shopping\Taxonomy;
use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use AppBundle\Traits\PublishableEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 *
 */
class ProductCard extends Product implements ImageInterface
{
    use TimestampableEntity;
    use PublishableEntity;

    use ImageEntity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagePath;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->translate()->getTitle() ?? '';
    }

    /**
     * @return string
     */
    public function getEntityType()
    {
        return 'Kaartje';
    }

    /**
     * @return string
     */
    public function generateName()
    {
        $name = $this->translate('nl_NL')->getTitle();

        return Urlizer::urlize($name);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->getImagePath();
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'imagePath';
    }

    /**
     * @param $path
     * @return mixed|void
     */
    public function setPath($path)
    {
        $this->{$this->getPathColumn()} = $path;
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return 'images/card/' . $this->getId();
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return ProductCard
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Add assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     *
     * @return ProductCard
     */
    public function addAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProducts[] = $assortmentProduct;

        return $this;
    }

    /**
     * Remove assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     */
    public function removeAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProducts->removeElement($assortmentProduct);
    }

    /**
     * Get assortmentProducts
     *
     * @return Collection
     */
    public function getAssortmentProducts()
    {
        return $this->assortmentProducts;
    }

    /**
     * Set googleShoppingTaxonomy
     *
     * @param Taxonomy $googleShoppingTaxonomy
     *
     * @return ProductCard
     */
    public function setGoogleShoppingTaxonomy(Taxonomy $googleShoppingTaxonomy = null)
    {
        $this->googleShoppingTaxonomy = $googleShoppingTaxonomy;

        return $this;
    }

    /**
     * Get googleShoppingTaxonomy
     * @return Taxonomy
     */
    public function getGoogleShoppingTaxonomy()
    {
        return $this->googleShoppingTaxonomy;
    }

    /**
     * Add supplierGroupProduct
     *
     * @param SupplierGroupProduct $supplierGroupProduct
     *
     * @return ProductCard
     */
    public function addSupplierGroupProduct(SupplierGroupProduct $supplierGroupProduct)
    {
        $supplierGroupProduct->setProduct($this);

        $this->supplierGroupProducts[] = $supplierGroupProduct;

        return $this;
    }

    /**
     * Remove supplierGroupProduct
     *
     * @param SupplierGroupProduct $supplierGroupProduct
     */
    public function removeSupplierGroupProduct(SupplierGroupProduct $supplierGroupProduct)
    {
        $this->supplierGroupProducts->removeElement($supplierGroupProduct);
    }

    /**
     * Get supplierGroupProducts
     *
     * @return Collection
     */
    public function getSupplierGroupProducts()
    {
        return $this->supplierGroupProducts;
    }

    /**
     * Add combination
     *
     * @param Combination $combination
     *
     * @return ProductCard
     */
    public function addCombination(Combination $combination)
    {
        $this->combinations[] = $combination;

        return $this;
    }

    /**
     * Remove combination
     *
     * @param Combination $combination
     */
    public function removeCombination(Combination $combination)
    {
        $this->combinations->removeElement($combination);
    }

    /**
     * Get combinations
     *
     * @return Collection
     */
    public function getCombinations()
    {
        return $this->combinations;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return ProductCard
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set configurable
     *
     * @param boolean $configurable
     *
     * @return ProductCard
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable
     *
     * @return boolean
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }

    /**
     * Set showOptions
     *
     * @param boolean $showOptions
     *
     * @return ProductCard
     */
    public function setShowOptions($showOptions)
    {
        $this->showOptions = $showOptions;

        return $this;
    }

    /**
     * Get showOptions
     *
     * @return boolean
     */
    public function getShowOptions()
    {
        return $this->showOptions;
    }
}
