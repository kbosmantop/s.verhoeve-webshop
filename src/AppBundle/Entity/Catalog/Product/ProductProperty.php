<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"product_id", "productgroup_property_id"})})
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ProductProperty
{

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="productProperties",
     *                                                                         cascade={"persist"})
     *
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     *
     */
    protected $productVariation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupProperty")
     *
     */
    protected $productgroupProperty;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $productgroupPropertyOption;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $value;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption")
     */
    protected $productgroupPropertyOptions;

    public function __construct()
    {
        $this->productgroupPropertyOptions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            switch ($this->getProductgroupProperty()->getFormType()) {
                case ProductgroupPropertyTypeType::TEXT:
                    if ($this->value) {
                        return $this->value . ' ' . $this->getProductgroupProperty()->getSuffix();
                    }
                    break;
                case ProductgroupPropertyTypeType::CHOICE:
                    $values = [];

                    if (\in_array('multiple', $this->getProductgroupProperty()->getFormTypeOptions(), true)) {
                        foreach ($this->getProductgroupPropertyOptions() as $productgroupPropertyOption) {
                            $values[] = $productgroupPropertyOption->getValue();
                        }
                    } elseif ($this->getProductgroupPropertyOption()) {
                        $values[] = $this->getProductgroupPropertyOption()->getValue();
                    }

                    return implode(', ', $values);
                case ProductgroupPropertyTypeType::ENTITY:
                    //                return $this->getProductVariation();
            }
        } catch (\Exception $e) {

        }

        return '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ProductProperty
     */
    public function setValue($value)
    {
        switch ($this->getProductgroupProperty()->getFormType()) {
            case ProductgroupPropertyTypeType::CHOICE:
                $this->value = null;

                if ($value === null) {
                    $this->setProductgroupPropertyOption(null);
                } else {
                    foreach ($this->getProductgroupProperty()->getOptions() as $option) {
                        if ($option->getId() === $value) {
                            $this->setProductgroupPropertyOption($option);
                        }
                    }
                }

                break;
            case ProductgroupPropertyTypeType::ENTITY:
                $this->value = null;
                $this->setProductgroupPropertyOption(null);
                $this->setProductVariation($value);

                break;

            case ProductgroupPropertyTypeType::DATE:
                /** @var \DateTime $value */
                $this->value = $value->format('Y-m-d');
                $this->setProductgroupPropertyOption(null);

                break;

            default:
                $this->value = $value;
                $this->setProductgroupPropertyOption(null);
                break;
        }

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        switch ($this->getProductgroupProperty()->getFormType()) {
            case ProductgroupPropertyTypeType::CHOICE:
                if ($this->getProductgroupPropertyOption()) {
                    return $this->getProductgroupPropertyOption()->getId();
                }

                return false;
            case ProductgroupPropertyTypeType::ENTITY:
                return $this->getProductVariation();

            case ProductgroupPropertyTypeType::DATE:
                return new \DateTime($this->value);

            default:
                return $this->value;
        }
    }

    /**
     * @param $data
     * @return Product|ProductProperty|bool
     */
    public function setData($data)
    {
        switch ($this->getProductgroupProperty()->getFormType()) {
            case ProductgroupPropertyTypeType::TEXT:
                return $this->value = $data;
                break;
            case ProductgroupPropertyTypeType::CHOICE:
                if (\in_array('multiple', $this->getProductgroupProperty()->getFormTypeOptions(), true)) {
                    // Symfony is handling the data internally :-)
                    $this->setProductgroupPropertyOptions($data);
                } else {
                    return $this->setProductgroupPropertyOption($data);
                }
                break;
            case ProductgroupPropertyTypeType::ENTITY:
                return $this->getProductVariation();
                break;
        }

        return false;
    }

    /**
     * @return Product|ProductgroupPropertyOption|bool|Collection
     */
    public function getData()
    {
        switch ($this->getProductgroupProperty()->getFormType()) {
            case ProductgroupPropertyTypeType::TEXT:
                return $this->value;
            case ProductgroupPropertyTypeType::CHOICE:
                if (\in_array('multiple', $this->getProductgroupProperty()->getFormTypeOptions(), true)) {
                    return $this->getProductgroupPropertyOptions();
                }

                return $this->getProductgroupPropertyOption();
            case ProductgroupPropertyTypeType::ENTITY:
                return $this->getProductVariation();
        }

        return false;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductProperty
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set productgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     *
     * @return ProductProperty
     */
    public function setProductgroupProperty(ProductgroupProperty $productgroupProperty = null)
    {
        $this->productgroupProperty = $productgroupProperty;

        return $this;
    }

    /**
     * Get productgroupProperty
     *
     * @return ProductgroupProperty
     */
    public function getProductgroupProperty()
    {
        return $this->productgroupProperty;
    }

    /**
     * Set productgroupPropertyOption
     *
     * @param ProductgroupPropertyOption $productgroupPropertyOption
     *
     * @return ProductProperty
     */
    public function setProductgroupPropertyOption(ProductgroupPropertyOption $productgroupPropertyOption = null)
    {
        $this->productgroupPropertyOption = $productgroupPropertyOption;

        return $this;
    }

    /**
     * Get productgroupPropertyOption
     *
     * @return ProductgroupPropertyOption
     */
    public function getProductgroupPropertyOption()
    {
        return $this->productgroupPropertyOption;
    }

    /**
     * Set productVariation
     *
     * @param Product $productVariation
     *
     * @return ProductProperty
     */
    public function setProductVariation(Product $productVariation = null)
    {
        $this->productVariation = $productVariation;

        return $this;
    }

    /**
     * Get productVariation
     *
     * @return Product
     */
    public function getProductVariation()
    {
        return $this->productVariation;
    }

    /**
     * Add productgroupPropertyOption
     *
     * @param ProductgroupPropertyOption $productgroupPropertyOption
     *
     * @return ProductProperty
     */
    public function addProductgroupPropertyOption(ProductgroupPropertyOption $productgroupPropertyOption)
    {
        $this->productgroupPropertyOptions[] = $productgroupPropertyOption;

        return $this;
    }

    /**
     * Remove productgroupPropertyOption
     *
     * @param ProductgroupPropertyOption $productgroupPropertyOption
     */
    public function removeProductgroupPropertyOption(ProductgroupPropertyOption $productgroupPropertyOption)
    {
        $this->productgroupPropertyOptions->removeElement($productgroupPropertyOption);
    }

    /**
     * @param $productgroupPropertyOptions
     */
    public function setProductgroupPropertyOptions($productgroupPropertyOptions)
    {
        if (!$this->productgroupPropertyOptions) {
            $this->productgroupPropertyOptions = $productgroupPropertyOptions;
        }
    }

    /**
     * Get productgroupPropertyOptions
     *
     * @return Collection
     */
    public function getProductgroupPropertyOptions()
    {
        return $this->productgroupPropertyOptions;
    }
}
