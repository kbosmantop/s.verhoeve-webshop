<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Security\Customer\CustomerGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table("product_bulk_price");
 *
 */
class ProductBulkPrice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="bulkPrices")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $quantity;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup")
     */
    protected $customergroups;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    protected $productVariations;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $price;

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductBulkPrice
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ProductBulkPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductBulkPrice
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customergroups = new ArrayCollection();
    }

    /**
     * Add customergroup
     *
     * @param CustomerGroup $customergroup
     *
     * @return ProductBulkPrice
     */
    public function addCustomergroup(CustomerGroup $customergroup)
    {
        $this->customergroups[] = $customergroup;

        return $this;
    }

    /**
     * Remove customergroup
     *
     * @param CustomerGroup $customergroup
     */
    public function removeCustomergroup(CustomerGroup $customergroup)
    {
        $this->customergroups->removeElement($customergroup);
    }

    /**
     * Get customergroups
     *
     * @return Collection
     */
    public function getCustomergroups()
    {
        return $this->customergroups;
    }

    /**
     * Add productVariation
     *
     * @param Product $productVariation
     *
     * @return ProductBulkPrice
     */
    public function addProductVariation(Product $productVariation)
    {
        $this->productVariations[] = $productVariation;

        return $this;
    }

    /**
     * Remove productVariation
     *
     * @param Product $productVariation
     */
    public function removeProductVariation(Product $productVariation)
    {
        $this->productVariations->removeElement($productVariation);
    }

    /**
     * Get productVariations
     *
     * @return Collection
     */
    public function getProductVariations()
    {
        return $this->productVariations;
    }
}
