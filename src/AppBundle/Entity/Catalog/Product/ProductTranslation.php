<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Interfaces\Seo\SlugInterface;
use AppBundle\Traits\SeoEntity;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @Solr\Document(index="product_translation")
 * @Solr\SynchronizationFilter(callback="shouldBeIndexed")
 * @ORM\Entity
 *
 * @method Product getTranslatable()
 */
class ProductTranslation implements SlugInterface
{
    use ORMBehaviors\Translatable\Translation;
    use SeoEntity;

    /**
     * @return boolean
     */
    public function shouldBeIndexed()
    {
        return (bool)$this->getTranslatable();
    }

    /**
     * @Solr\Id
     */
    protected $id;

    /**
     * @Solr\Field(type="integer", getter="getId")
     */
    protected $translatable;

    /**
     * @Solr\Field(type="text")
     */
    protected $locale;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $title;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $description;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $shortDescription;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $imagePath;

    public function __clone()
    {
        $this->id = null;
        $this->translatable = null;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProductTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return ProductTranslation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return ProductTranslation
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return ProductTranslation
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }
}
