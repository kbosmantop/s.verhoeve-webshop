<?php

namespace AppBundle\Entity\Catalog\Assortment;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Model\Assortment\AssortmentProductModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;

/**
 * @link http://future500.nl/articles/2013/09/doctrine-2-how-to-handle-join-tables-with-extra-columns/
 */

/**
 * @ORM\Entity()
 * @Rule\Entity(description="Assortiment product")
 */
class AssortmentProduct extends AssortmentProductModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", inversedBy="assortmentProducts")
     * @Rule\Property(description="Assortiment", autoComplete={"id"="assortment.id", "label"="assortment.translate.title"}, relation="parent")
     */
    protected $assortment;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="assortmentProducts")
     * @Rule\Property(description="Product", autoComplete={"id"="product.id", "label"="product.translate.title"}, relation="child")
     */
    protected $product;

    /**
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\AssortmentProductImage", mappedBy="assortmentProduct",
     *                                                                        cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $assortmentProductImages;

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return AssortmentProduct
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set assortment
     *
     * @param Assortment $assortment
     *
     * @return AssortmentProduct
     */
    public function setAssortment(Assortment $assortment)
    {
        $this->assortment = $assortment;

        return $this;
    }

    /**
     * Get assortment
     *
     * @return Assortment
     */
    public function getAssortment()
    {
        return $this->assortment;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return AssortmentProduct
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assortmentProductImages = new ArrayCollection();
    }

    /**
     * Add assortmentProductImage
     *
     * @param AssortmentProductImage $assortmentProductImage
     *
     * @return AssortmentProduct
     */
    public function addAssortmentProductImage(AssortmentProductImage $assortmentProductImage)
    {
        $this->assortmentProductImages[] = $assortmentProductImage;

        return $this;
    }

    /**
     * Remove assortmentProductImage
     *
     * @param AssortmentProductImage $assortmentProductImage
     */
    public function removeAssortmentProductImage(AssortmentProductImage $assortmentProductImage)
    {
        $this->assortmentProductImages->removeElement($assortmentProductImage);
    }

    /**
     * Get assortmentProductImages
     *
     * @return Collection|AssortmentProductImage[]
     */
    public function getAssortmentProductImages()
    {
        return $this->assortmentProductImages;
    }
}
