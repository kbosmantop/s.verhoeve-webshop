<?php

namespace AppBundle\Entity\Relation;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 * @method CompanyCustomField getTranslatable()
 */
class CompanyCustomFieldTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $label;

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return CompanyCustomFieldTranslation
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }
}
