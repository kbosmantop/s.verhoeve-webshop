<?php

namespace AppBundle\Entity\Relation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 *
 *
 */
class CompanyDomain
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $domain;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="companyDomains")
     *
     */
    protected $company;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $allowRegistration;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return CompanyDomain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set allowRegistration
     *
     * @param boolean $allowRegistration
     *
     * @return CompanyDomain
     */
    public function setAllowRegistration($allowRegistration)
    {
        $this->allowRegistration = $allowRegistration;

        return $this;
    }

    /**
     * Get allowRegistration
     *
     * @return boolean
     */
    public function getAllowRegistration()
    {
        return $this->allowRegistration;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyDomain
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Relation\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
