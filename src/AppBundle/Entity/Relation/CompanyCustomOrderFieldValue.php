<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AssertionError;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class CompanyCustomOrderFieldValue
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\CompanyCustomField")
     * @Assert\NotNull()
     */
    private $companyCustomField;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="companyCustomerOrderFieldValues")
     */
    private $orderCollection;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="companyCustomerOrderFieldValues")
     */
    private $order;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotNull()
     */
    private $value;

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set companyCustomField
     *
     * @param CompanyCustomField $companyCustomField
     *
     * @return CompanyCustomOrderFieldValue
     */
    public function setCompanyCustomField(CompanyCustomField $companyCustomField): CompanyCustomOrderFieldValue
    {
        $this->companyCustomField = $companyCustomField;

        return $this;
    }

    /**
     * Get companyCustomField
     *
     * @return CompanyCustomField|null
     */
    public function getCompanyCustomField(): ?CompanyCustomField
    {
        return $this->companyCustomField;
    }

    /**
     * Set order
     *
     * @param Order|null $order
     *
     * @return CompanyCustomOrderFieldValue
     */
    public function setOrder(?Order $order): CompanyCustomOrderFieldValue
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection|null $orderCollection
     *
     * @return CompanyCustomOrderFieldValue
     */
    public function setOrderCollection(?OrderCollection $orderCollection): CompanyCustomOrderFieldValue
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection|null
     */
    public function getOrderCollection(): ?OrderCollection
    {
        return $this->orderCollection;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return CompanyCustomOrderFieldValue
     */
    public function setValue(string $value): CompanyCustomOrderFieldValue
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @Assert\Callback()
     */
    public function validateQuantitySteps()
    {
        if($this->order === null && $this->orderCollection === null){
            throw new AssertionError('Order or OrderCollection must be set');
        }
    }
}