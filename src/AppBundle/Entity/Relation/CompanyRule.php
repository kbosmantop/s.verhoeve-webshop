<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Rule;

/**
 * @ORM\Entity()
 * Class CompanyRule
 * @package AppBundle\Entity\Relation\Company
 */
class CompanyRule
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="rules")
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule", cascade={"persist", "remove"})
     */
    protected $rule;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyRule
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set rule
     *
     * @param Rule $rule
     *
     * @return CompanyRule
     */
    public function setRule(Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
}
