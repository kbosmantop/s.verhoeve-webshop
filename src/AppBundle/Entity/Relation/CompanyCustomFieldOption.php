<?php

namespace AppBundle\Entity\Relation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CompanyCustomFieldOption
 * @package AppBundle\Entity\Relation
 * @ORM\Entity()
 */
class CompanyCustomFieldOption
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\Regex("([A-Za-z0-9\-\_]+)")
     */
    protected $optionKey;

    /**
     * @ORM\Column(type="string")
     */
    protected $optionValue;

    /**
     * @ORM\Column(type="integer")
     */
    protected $position;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\CompanyCustomField", inversedBy="options")
     */
    protected $companyCustomField;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionKey
     *
     * @param string $optionKey
     *
     * @return CompanyCustomFieldOption
     */
    public function setOptionKey($optionKey)
    {
        $this->optionKey = $optionKey;

        return $this;
    }

    /**
     * Get optionKey
     *
     * @return string
     */
    public function getOptionKey()
    {
        return $this->optionKey;
    }

    /**
     * Set optionValue
     *
     * @param string $optionValue
     *
     * @return CompanyCustomFieldOption
     */
    public function setOptionValue($optionValue)
    {
        $this->optionValue = $optionValue;

        return $this;
    }

    /**
     * Get optionValue
     *
     * @return string
     */
    public function getOptionValue()
    {
        return $this->optionValue;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CompanyCustomFieldOption
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set companyCustomField
     *
     * @param CompanyCustomField $companyCustomField
     *
     * @return CompanyCustomFieldOption
     */
    public function setCompanyCustomField(CompanyCustomField $companyCustomField = null)
    {
        $this->companyCustomField = $companyCustomField;

        return $this;
    }

    /**
     * Get companyCustomField
     *
     * @return CompanyCustomField
     */
    public function getCompanyCustomField()
    {
        return $this->companyCustomField;
    }
}
