<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Site\SiteLogo;
use AppBundle\Model\Company\CompanySiteModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class CompanySite
 * @package AppBundle\Entity\Relation\Company
 *
 * @ORM\Entity()
 */
class CompanySite extends CompanySiteModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $locale;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site")
     */
    protected $defaultSite;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\SiteLogo", cascade={"persist"})
     */
    protected $logo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customStyle;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $registrationEnabled;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $allowedExtensions;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $domainName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $orderConfirmationUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $orderConfirmationCCUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $deliveryStatusUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $deliveryStatusCCUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $confirmOrderUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $confirmUserUser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $loginText;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $registrationText;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $orderSuccessText;

    /**
     * ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="sites")
     */
    protected $companies;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return CompanySite
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get registrationEnabled
     *
     * @return boolean
     */
    public function getRegistrationEnabled()
    {
        return $this->registrationEnabled;
    }

    /**
     * Set registrationEnabled
     *
     * @param boolean $registrationEnabled
     *
     * @return CompanySite
     */
    public function setRegistrationEnabled($registrationEnabled)
    {
        $this->registrationEnabled = $registrationEnabled;

        return $this;
    }

    /**
     * Get domainName
     *
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * Set domainName
     *
     * @param string $domainName
     *
     * @return CompanySite
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CompanySite
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get loginText
     *
     * @return string
     */
    public function getLoginText()
    {
        return $this->loginText;
    }

    /**
     * Set loginText
     *
     * @param string $loginText
     *
     * @return CompanySite
     */
    public function setLoginText($loginText)
    {
        $this->loginText = $loginText;

        return $this;
    }

    /**
     * Get registrationText
     *
     * @return string
     */
    public function getRegistrationText()
    {
        return $this->registrationText;
    }

    /**
     * Set registrationText
     *
     * @param string $registrationText
     *
     * @return CompanySite
     */
    public function setRegistrationText($registrationText)
    {
        $this->registrationText = $registrationText;

        return $this;
    }

    /**
     * Get orderConfirmationUser
     *
     * @return Customer
     */
    public function getOrderConfirmationUser()
    {
        return $this->orderConfirmationUser;
    }

    /**
     * Set orderConfirmationUser
     *
     * @param Customer $orderConfirmationUser
     *
     * @return CompanySite
     */
    public function setOrderConfirmationUser(Customer $orderConfirmationUser = null)
    {
        $this->orderConfirmationUser = $orderConfirmationUser;

        return $this;
    }

    /**
     * Get orderConfirmationCCUser
     *
     * @return Customer
     */
    public function getOrderConfirmationCCUser()
    {
        return $this->orderConfirmationCCUser;
    }

    /**
     * Set orderConfirmationCCUser
     *
     * @param Customer $orderConfirmationCCUser
     *
     * @return CompanySite
     */
    public function setOrderConfirmationCCUser(Customer $orderConfirmationCCUser = null)
    {
        $this->orderConfirmationCCUser = $orderConfirmationCCUser;

        return $this;
    }

    /**
     * Get deliveryStatusUser
     *
     * @return Customer
     */
    public function getDeliveryStatusUser()
    {
        return $this->deliveryStatusUser;
    }

    /**
     * Set deliveryStatusUser
     *
     * @param Customer $deliveryStatusUser
     *
     * @return CompanySite
     */
    public function setDeliveryStatusUser(Customer $deliveryStatusUser = null)
    {
        $this->deliveryStatusUser = $deliveryStatusUser;

        return $this;
    }

    /**
     * Get DeliveryStatusCCUser
     *
     * @return Customer
     */
    public function getDeliveryStatusCCUser()
    {
        return $this->deliveryStatusCCUser;
    }

    /**
     * Set DeliveryStatusCCUser
     *
     * @param Customer $deliveryStatusCCUser
     *
     * @return CompanySite
     */
    public function setDeliveryStatusCCUser(Customer $deliveryStatusCCUser = null)
    {
        $this->deliveryStatusCCUser = $deliveryStatusCCUser;

        return $this;
    }

    /**
     * Get confirmOrderUser
     *
     * @return Customer
     */
    public function getConfirmOrderUser()
    {
        return $this->confirmOrderUser;
    }

    /**
     * Set confirmOrderUser
     *
     * @param Customer $confirmOrderUser
     *
     * @return CompanySite
     */
    public function setConfirmOrderUser(Customer $confirmOrderUser = null)
    {
        $this->confirmOrderUser = $confirmOrderUser;

        return $this;
    }

    /**
     * Get confirmUserUser
     *
     * @return Customer
     */
    public function getConfirmUserUser()
    {
        return $this->confirmUserUser;
    }

    /**
     * Set confirmUserUser
     *
     * @param Customer $confirmUserUser
     *
     * @return CompanySite
     */
    public function setConfirmUserUser(Customer $confirmUserUser = null)
    {
        $this->confirmUserUser = $confirmUserUser;

        return $this;
    }

    /**
     * Get allowedExtensions
     *
     * @return string
     */
    public function getAllowedExtensions()
    {
        return $this->allowedExtensions;
    }

    /**
     * Set allowedExtensions
     *
     * @param string $allowedExtensions
     *
     * @return CompanySite
     */
    public function setAllowedExtensions($allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;

        return $this;
    }

    /**
     * Get orderSuccessText
     *
     * @return string
     */
    public function getOrderSuccessText()
    {
        return $this->orderSuccessText;
    }

    /**
     * Set orderSuccessText
     *
     * @param string $orderSuccessText
     *
     * @return CompanySite
     */
    public function setOrderSuccessText($orderSuccessText)
    {
        $this->orderSuccessText = $orderSuccessText;

        return $this;
    }

    /**
     * Set defaultSite
     *
     * @param Site $defaultSite
     *
     * @return CompanySite
     */
    public function setDefaultSite(Site $defaultSite = null)
    {
        $this->defaultSite = $defaultSite;

        return $this;
    }

    /**
     * Get defaultSite
     *
     * @return Site
     */
    public function getDefaultSite()
    {
        return $this->defaultSite;
    }

    /**
     * Set customStyle
     *
     * @param string $customStyle
     *
     * @return CompanySite
     */
    public function setCustomStyle($customStyle)
    {
        $this->customStyle = $customStyle;

        return $this;
    }

    /**
     * Get customStyle
     *
     * @return string
     */
    public function getCustomStyle()
    {
        return $this->customStyle;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies() {
        return $this->companies;
    }

    /**
     * Set logo
     *
     * @param SiteLogo $logo
     *
     * @return CompanySite
     */
    public function setLogo(SiteLogo $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return SiteLogo
     */
    public function getLogo()
    {
        return $this->logo;
    }
}
