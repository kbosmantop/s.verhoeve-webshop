<?php

namespace AppBundle\Entity\Relation;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity()
 * Class CompanyInvoiceSettings
 */
class CompanyInvoiceSettings
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Company $company
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $company;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyInvoiceFrequencyType")
     * @ORM\Column(type="company_invoice_frequency_type", options={"default" : "collective"})
     */
    protected $invoiceFrequency;

    /**
     * @var int $paymentTerm;
     * @ORM\Column(type="integer", length=3, nullable=false, options={"default" : 14})
     */
    protected $paymentTerm;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyInvoiceDiscountType")
     * @ORM\Column(type="company_invoice_discount_type", options={"default" : "separate"})
     */
    protected $invoiceDiscount;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType")
     * @ORM\Column(type="company_invoice_incl_personal_data_type", options={"default" : "none"})
     */
    protected $invoiceInclPersonalData;

    /**
     * @var boolean $invoiceZeroValue
     * @ORM\Column(type="boolean")
     */
    protected $invoiceZeroValue;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyInvoiceTypeType")
     * @ORM\Column(type="company_invoice_type_type", options={"default" : "pdf_mail"})
     */
    protected $invoiceType;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $invoiceReminderEmail;

    /**
     * CompanyInvoiceSettings constructor.
     */
    public function __construct()
    {
        $this->setInvoiceFrequency('collective');
        $this->setPaymentTerm(14);
        $this->setInvoiceDiscount('separate');
        $this->setInvoiceInclPersonalData('all');
        $this->setInvoiceZeroValue(true);
        $this->setInvoiceType('pdf_mail');
    }

    /**
     * @return string
     */
    public function getInvoiceFrequency(): string
    {
        return $this->invoiceFrequency;
    }

    /**
     * @param string $invoiceFrequency
     */
    public function setInvoiceFrequency(string $invoiceFrequency): void
    {
        $this->invoiceFrequency = $invoiceFrequency;
    }

    /**
     * @return integer
     */
    public function getPaymentTerm(): int
    {
        return $this->paymentTerm;
    }

    /**
     * @param integer $paymentTerm
     */
    public function setPaymentTerm($paymentTerm): void
    {
        $this->paymentTerm = $paymentTerm;
    }

    /**
     * @return string
     */
    public function getInvoiceDiscount(): string
    {
        return $this->invoiceDiscount;
    }

    /**
     * @param string $invoiceDiscount
     */
    public function setInvoiceDiscount($invoiceDiscount): void
    {
        $this->invoiceDiscount = $invoiceDiscount;
    }

    /**
     * @return string
     */
    public function getInvoiceInclPersonalData(): string
    {
        return $this->invoiceInclPersonalData;
    }

    /**
     * @param string $invoiceInclPersonalData
     */
    public function setInvoiceInclPersonalData(string $invoiceInclPersonalData): void
    {
        $this->invoiceInclPersonalData = $invoiceInclPersonalData;
    }

    /**
     * @return bool
     */
    public function isInvoiceZeroValue(): bool
    {
        return $this->invoiceZeroValue;
    }

    /**
     * @param bool $invoiceZeroValue
     */
    public function setInvoiceZeroValue(bool $invoiceZeroValue): void
    {
        $this->invoiceZeroValue = $invoiceZeroValue;
    }

    /**
     * @return string
     */
    public function getInvoiceType(): string
    {
        return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     */
    public function setInvoiceType($invoiceType): void
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    /**
     * Set invoiceReminder email
     *
     * @param string $invoiceReminderEmail
     *
     * @return CompanyInvoiceSettings
     */
    public function setInvoiceReminderEmail(?string $invoiceReminderEmail): self
    {
        $this->invoiceReminderEmail = $invoiceReminderEmail;

        return $this;
    }

    /**
     * Get invoiceReminder email
     *
     * @return string
     */
    public function getInvoiceReminderEmail(): ?string
    {
        if($this->invoiceReminderEmail === ''){
            return null;
        }
        return $this->invoiceReminderEmail;
    }
}