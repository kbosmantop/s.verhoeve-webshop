<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Common\TimeslotEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Annotation as Rule;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model\Loggable\Loggable;

/**
 * Class CompanyEstablishment
 * @package AppBundle\Entity\Relation\Company
 *
 * @ORM\Entity()
 */
class CompanyEstablishment implements TimeslotEntityInterface
{

    use TimestampableEntity;
    use Loggable;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string $establishmentNumber
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $establishmentNumber;

    /**
     * @var \DateTime|null $verifiedOn
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $verifiedOn;

    /**
     * @var Address $address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    protected $address;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="establishments")
     */
    protected $company;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Common\Timeslot", cascade={"persist"})
     */
    protected $openingTimeslots;

    /**
     * CompanyEstablishment constructor.
     */
    public function __construct()
    {
        $this->openingTimeslots = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CompanyEstablishment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set establishmentNumber
     *
     * @param string $establishmentNumber
     *
     * @return CompanyEstablishment
     */
    public function setEstablishmentNumber($establishmentNumber)
    {
        $this->establishmentNumber = $establishmentNumber;

        return $this;
    }

    /**
     * Get establishmentNumber
     *
     * @return string
     */
    public function getEstablishmentNumber()
    {
        return $this->establishmentNumber;
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return CompanyEstablishment
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyEstablishment
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        if (null !== $this->getCompany()->getCompanyRegistration()) {
            return $this->getEstablishmentNumber() === $this->getCompany()->getCompanyRegistration()->getEstablishmentNumber();
        } else {
            // @todo: Remove when all florists have a valid establishment number.
            if ($this->address->isMainAddress() === 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param bool $isVerified
     *
     * @return CompanyEstablishment
     */
    public function setIsVerified(bool $isVerified): CompanyEstablishment
    {
        if ($isVerified === true) {
            try {
                $this->verifiedOn = new \DateTime();
            } catch (\Exception $e) {
                $this->verifiedOn = null;
                $this->establishmentNumber = null;
            }
        } else {
            $this->verifiedOn = null;
            $this->establishmentNumber = null;
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verifiedOn !== null;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getVerifiedOn(): ?\DateTime
    {
        return new $this->verifiedOn;
    }

    /**
     * Set openingTimeslots
     *
     * @param Timeslot[] $openingTimeslots
     *
     * @return CompanyEstablishment
     */
    public function setTimeslots($openingTimeslots): CompanyEstablishment
    {
        $this->openingTimeslots = new ArrayCollection();

        foreach ($openingTimeslots as $openingTimeslot) {
            $this->addTimeslot($openingTimeslot);
        }

        return $this;
    }


    /**
     * Add openingTimeslot
     *
     * @param Timeslot $openingTimeslot
     *
     * @return CompanyEstablishment
     */
    public function addTimeslot(Timeslot $openingTimeslot): CompanyEstablishment
    {
        if (!$this->openingTimeslots->contains($openingTimeslot)) {
            $this->openingTimeslots->add($openingTimeslot);
        }

        return $this;
    }

    /**
     * Remove openingTimeslot
     *
     * @param Timeslot $openingTimeslot
     *
     * @return CompanyEstablishment
     */
    public function removeTimeslot(Timeslot $openingTimeslot): CompanyEstablishment
    {
        if ($this->openingTimeslots->contains($openingTimeslot)) {
            $this->openingTimeslots->removeElement($openingTimeslot);
        }

        return $this;
    }

    /**
     * Remove openingTimeslots
     * @return CompanyEstablishment
     */
    public function removeTimeslots(): CompanyEstablishment
    {
        foreach ($this->openingTimeslots as $openingTimeslot) {
            $this->removeTimeslot($openingTimeslot);
        }

        return $this;
    }

    /**
     * Get openingTimeslots
     *
     * @return ArrayCollection|Timeslot[]
     * Rule\MappedProperty(property="openingTimeslots")
     * @throws \Exception
     */
    public function getTimeslots()
    {
        return $this->openingTimeslots;
    }

}
