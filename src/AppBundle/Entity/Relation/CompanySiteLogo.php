<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * Class CompanySiteLogo
 * @package AppBundle\Entity\Relation\Company
 */
class CompanySiteLogo implements ImageInterface
{

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return CompanySiteLogo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CompanySiteLogo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'path';
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return "images/company-logo/" . $this->getCompanySite()->getId();
    }

    /**
     * @return mixed
     */
    public function generateName()
    {
        $name = $this->getCompanySite()->getSlug();

        return $name;
    }
}
