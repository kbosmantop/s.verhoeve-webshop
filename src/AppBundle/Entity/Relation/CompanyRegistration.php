<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Geography\Country;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class CompanyRegistration
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @UniqueEntity(fields="registrationNumber")
 * @UniqueEntity(fields="vatNumber")
 */
class CompanyRegistration
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     */
    protected $country;

    /**
     * @var string $registrationNumber
     *
     * @ORM\Column(type="string", length=64, nullable=true, unique=true)
     */
    protected $registrationNumber;

    /**
     * @var string $establishmentNumber
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $establishmentNumber;

    /**
     * @var string $vatNumber
     *
     * @ORM\Column(type="string", length=64, nullable=true, unique=true)
     */
    protected $vatNumber;

    /**
     * @var Address $mainAddress
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    protected $mainAddress;

    /**
     * @var Address $postAddress
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    protected $postAddress;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\Company", mappedBy="companyRegistration")
     */
    protected $companies;

    /**
     * CompanyRegistration constructor.
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return string
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this->name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Country $country
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $registrationNumber
     * @return $this
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get the registration number of the company.
     *
     * This registration number is valid in the country referenced by the 'country' property.
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Get the Dutch Chamber of Commerce (KvK) number.
     *
     * Returns null if this is not a Dutch company.
     *
     * @return string
     */
    public function getKvKNumber()
    {
        if ($this->getCountry()->getCode() === 'NL') {
            return $this->getRegistrationNumber();
        }

        return null;
    }

    /**
     * @param $establishmentNumber
     * @return $this
     */
    public function setEstablishmentNumber($establishmentNumber)
    {
        $this->establishmentNumber = $establishmentNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstablishmentNumber()
    {
        return $this->establishmentNumber;
    }

    /**
     * @param $vatNumber
     * @return $this
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param $mainAddress
     * @return $this
     */
    public function setMainAddress(Address $mainAddress)
    {
        $this->mainAddress = $mainAddress;

        return $this;
    }

    /**
     * @return Address
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }

    /**
     * @param $postAddress
     * @return $this
     */
    public function setPostAddress(Address $postAddress)
    {
        $this->postAddress = $postAddress;

        return $this;
    }

    /**
     * @return Address
     */
    public function getPostAddress()
    {
        return $this->postAddress;
    }

    /**
     * Add company
     *
     * @param Company $company
     *
     * @return CompanyRegistration
     */
    public function addCompany(Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return Collection|Company[]
     */
    public function getCompanies()
    {
        return $this->companies;
    }
}
