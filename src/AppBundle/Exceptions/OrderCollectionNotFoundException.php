<?php

namespace AppBundle\Exceptions;

/**
 * Class OrderCollectionNotFoundException
 * @package AppBundle\Exceptions
 */
class OrderCollectionNotFoundException extends \Exception
{
}