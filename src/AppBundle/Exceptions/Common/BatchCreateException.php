<?php

namespace AppBundle\Exceptions\Common;

/**
 * Class BatchCreateException
 * @package AppBundle\Exceptions\Common
 */
class BatchCreateException extends \Exception
{
}