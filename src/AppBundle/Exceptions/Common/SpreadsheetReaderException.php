<?php

namespace AppBundle\Exceptions\Common;

/**
 * Class SpreadsheetReaderException
 * @package AppBundle\Exceptions\Common
 */
class SpreadsheetReaderException extends \Exception
{

}
