<?php

namespace AppBundle\Exceptions;

use Exception;

class TempApiException extends Exception
{
    /**
     * @var int
     */
    protected $code = 422;
}