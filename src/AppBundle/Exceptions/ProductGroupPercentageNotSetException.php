<?php

namespace AppBundle\Exceptions;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Catalog\Product\Productgroup;

class ProductGroupPercentageNotSetException extends \Exception
{
    /**
     * @var Productgroup
     */
    private $productGroup;

    /**
     * @var Company
     */
    private $supplier;

    private function message()
    {
        $this->message = vsprintf("Geen commissie percentage ingesteld voor leverancier '%s' op productgroep '%s'", [
            $this->supplier ? $this->supplier->getName() : null,
            $this->productGroup ? $this->productGroup->getTitle() : null,
        ]);
    }

    /**
     * @param Productgroup $productGroup
     */
    public function setProductGroup(Productgroup $productGroup)
    {
        $this->productGroup = $productGroup;

        $this->message();
    }

    /**
     * @param Company $supplier
     */
    public function setSupplier(Company $supplier)
    {
        $this->supplier = $supplier;

        $this->message();
    }
}
