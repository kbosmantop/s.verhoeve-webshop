<?php

namespace AppBundle\Exceptions\Customer;

/**
 * Class EmailAddressExistsException
 * @package AppBundle\Exceptions\Customer
 */
class EmailAddressExistsException extends \Exception
{
}