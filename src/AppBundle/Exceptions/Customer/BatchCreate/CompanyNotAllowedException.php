<?php

namespace AppBundle\Exceptions\Customer\BatchCreate;

/**
 * Class BatchCreateException
 * @package AppBundle\Exceptions\Customer\BatchCreate
 */
class CompanyNotAllowedException extends \Exception
{
}