<?php

namespace AppBundle\Exceptions\Customer\BatchCreate;

/**
 * Class InvalidMimeTypeException
 * @package AppBundle\Exceptions\Customer\BatchCreate
 */
class InvalidMimeTypeException extends \Exception
{
}