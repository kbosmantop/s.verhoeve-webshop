<?php

namespace AppBundle\Exceptions\Customer;

/**
 * Class UserAlreadyExistException
 * @package AppBundle\Exceptions\Customer
 */
class UsernameUsedException extends \Exception
{
}