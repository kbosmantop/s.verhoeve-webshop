<?php

namespace AppBundle\Exceptions\Sales;

class CartInvalidSupplierCombinationException extends \Exception
{
    protected $message = "There are no suppliers available that deliver the combination of products for the cart.";
}