<?php

namespace AppBundle\Exceptions;

/**
 * Class FloristSyncException
 * @package AppBundle\Exceptions
 */
class FloristSyncException extends \Exception
{
}