<?php

namespace AppBundle\Exceptions;

class NonEditableException extends \Exception
{
}