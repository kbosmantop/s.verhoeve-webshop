<?php

namespace AppBundle\Exceptions;

use Exception;

/**
 * Class ChoosePasswordException
 * @package AppBundle\Exceptions
 */
class ChoosePasswordException extends Exception
{
}