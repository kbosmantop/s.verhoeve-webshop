<?php

namespace AppBundle\Exceptions;

class CardTextExceedsMaximumLengthException extends \Exception
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var integer
     */
    private $maximumLength;

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param int $maximumLength
     */
    public function setMaximumLength(int $maximumLength)
    {
        $this->maximumLength = $maximumLength;
    }

    /**
     * @return int
     */
    public function getMaximumLength()
    {
        return $this->maximumLength;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return strlen($this->text);
    }
}