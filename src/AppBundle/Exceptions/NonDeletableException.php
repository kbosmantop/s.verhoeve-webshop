<?php

namespace AppBundle\Exceptions;

class NonDeletableException extends \Exception
{
}