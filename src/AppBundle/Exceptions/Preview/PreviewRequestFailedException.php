<?php

namespace AppBundle\Exceptions\Preview;

class PreviewRequestFailedException extends \Exception
{
    protected $message = "Preview request failed";
}