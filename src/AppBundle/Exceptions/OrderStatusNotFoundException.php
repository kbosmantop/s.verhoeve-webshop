<?php

namespace AppBundle\Exceptions;

class OrderStatusNotFoundException extends \Exception
{
    protected $code = 422;
    protected $message = 'OrderStatus not found';
}
