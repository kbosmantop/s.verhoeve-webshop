<?php

namespace AppBundle\Exceptions;

class LinkedPreferredSuppliersException extends \Exception
{
    public function __construct($message = '')
    {
        $this->message = $message;
    }
}
