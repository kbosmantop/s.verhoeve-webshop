<?php

namespace AppBundle\Exceptions;

/**
 * Class InvalidHashException
 * @package AppBundle\Exceptions
 */
class InvalidHashException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 400;

    /**
     * @var string
     */
    protected $message = 'Invalid hash';
}
