<?php

namespace AppBundle\Exceptions;

/**
 * Class PasswordResetException
 * @package AppBundle\Exceptions
 */
class PasswordResetException extends \Exception
{
}