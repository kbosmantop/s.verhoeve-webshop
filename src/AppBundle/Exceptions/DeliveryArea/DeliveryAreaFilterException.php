<?php

namespace AppBundle\Exceptions\DeliveryArea;

/**
 * Class DeliveryAreaFilterException
 * @package AppBundle\Exceptions\DeliveryArea
 */
class DeliveryAreaFilterException extends \RuntimeException
{
}