<?php

namespace AppBundle\Services;

use AppBundle\Entity\Site\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Menu
 *
 * @package AppBundle\Services
 */
class Menu
{
    use ContainerAwareTrait;

    /**
     * Menu constructor.
     *
     */
    public function __construct()
    {
    }

    /**
     * @param MenuItem $menuItem
     *
     * @return boolean|\stdClass
     */
    public function getMenuItemEntity(MenuItem $menuItem)
    {
        if (!$menuItem->getEntityName() || !$menuItem->getEntityId()) {
            return false;
        }

        return $this->container->get('doctrine')->getRepository($menuItem->getEntityName())->find($menuItem->getEntityId());
    }
}