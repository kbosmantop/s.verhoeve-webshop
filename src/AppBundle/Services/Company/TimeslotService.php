<?php

namespace AppBundle\Services\Company;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Geography\HolidayCountry;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Supplier\PickupLocation;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PDO;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;

/**
 * Class TimeslotService
 * @package AppBundle\Services\Company
 */
class TimeslotService
{
    /**
     * @var ArrayTransformer
     */
    protected $transformer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array 
     */
    private $holidays = [];

    /**
     * TimeslotService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->transformer = new ArrayTransformer();
    }

    /**
     * @param Collection $timeslots
     * @param DateTime|null $preparationTime
     * @param DateTime|null $dateToCheck
     * @param PickupLocation|null $pickupLocation
     *
     * @return Collection
     * @throws \Exception
     */
    public function getAvailableTimeslots(
        Collection $timeslots,
        ?\DateTime $preparationTime = null,
        ?\DateTime $dateToCheck = null,
        ?PickupLocation $pickupLocation = null
    ): Collection {
        if ($dateToCheck === null) {
            $dateToCheck = new \DateTime();
        }

        $fixedSlots = $this->getFixedSlots();

        $timeslots = $timeslots->filter(function ($slot) use ($dateToCheck, $preparationTime, $pickupLocation) {
            return $this->isOpenOnDay($slot, $preparationTime, $dateToCheck, $pickupLocation);
        });

        $fixedSlots = $fixedSlots->filter(function ($slot) use ($dateToCheck, $preparationTime, $pickupLocation) {
            return $this->isOpenOnDay($slot, $preparationTime, $dateToCheck, $pickupLocation);
        });

        $bitsToday = $this->timeslotBinaryEncode($timeslots);
        $bitsFixedSlots = $this->timeslotBinaryEncode($fixedSlots);
        return $this->timeslotBinaryDecode($bitsToday & $bitsFixedSlots);
    }

    /**
     * TODO implement max block size instead of longest continues run
     * @param $bits
     * @return ArrayCollection
     */
    private function timeslotBinaryDecode($bits)
    {
        $timeslots = new ArrayCollection();
        $from = null;
        for ($time = 0; $time <= 24; $time++) {
            $nextBit = $bits & 1;
            $bits >>= 1;
            if (($nextBit === 0 && $from === null) || ($nextBit === 1 && $from !== null)) {
                continue;
            }

            if ($nextBit === 1 && $from === null) {
                $from = $time;
            }

            if ($nextBit === 0 && $from !== null) {
                try {
                    $timeslots->add(new Timeslot(
                        (new \DateTime())->setDate(0, 0, 0)->setTime($from, 0),
                        (new \DateTime())->setDate(0, 0, 0)->setTime($time, 0)
                    ));
                } catch (\Exception $e) {
                    //Catch \DateTime creation date error, and dont add a timeslot
                }
                $from = null;
            }
        }
        return $timeslots;
    }

    /**
     * @param $timeslots
     * @return int
     */
    private function timeslotBinaryEncode($timeslots)
    {
        $bitsToday = 0;
        /** @var Timeslot $timeslot */
        foreach ($timeslots as $timeslot) {
            $bits = 0b0;
            $from = (int)$timeslot->getFrom()->format('H');
            $till = (int)$timeslot->getTill()->format('H');
            for ($time = 24; $time >= 0; $time--) {
                if ($time >= $from && $time < $till) {
                    $bits = ($bits << 1) | 1;
                } else {
                    $bits <<= 1;
                }
            }
            $bitsToday |= $bits;
        }
        return $bitsToday;
    }

    /**
     * @param Timeslot            $timeslot
     * @param DateTime|null       $preparationTime
     * @param DateTime|null       $dateToCheck
     * @param PickupLocation|null $pickupLocation
     * @return bool
     * @throws \Exception
     */
    public function isOpenOnDay(
        Timeslot $timeslot,
        ?\DateTime $preparationTime = null,
        ?\DateTime $dateToCheck = null,
        ?PickupLocation $pickupLocation = null
    ): bool {
        if ($dateToCheck === null) {
            $dateToCheck = new \DateTime();
        }

        if ($pickupLocation !== null && in_array($dateToCheck->format('Y-M-d'),
                $this->getExcludedDates($timeslot, $pickupLocation), true)) {
            return false;
        }

        //NOTE This check supports day of the week only, for example bi weekly opening hours are currently not supported
        $rule = $timeslot->getRrule();
        if ($rule !== null && !in_array((int)$dateToCheck->format('N'), $timeslot->getAvailableOnDaysOfTheWeek(),
                true)) {
            return false;
        }

        if ($preparationTime === null) {
            return true;
        }

        $date = (clone $dateToCheck)->setTime(0, 0);
        $prepDate = (clone $preparationTime)->setTime(0, 0);

        //Check if day is later than preparation time
        if ($date > $prepDate) {
            return true;
        }

        /** @noinspection TypeUnsafeComparisonInspection NOTE Object is never the same but this compares the date */
        if ($date == $prepDate) {
            //If preparation time is later than the start / from time of the timeslot, it is not available
            return (clone $preparationTime)->setDate(0, 0, 0) < $timeslot->getFrom();
        }

        //IF $date < $prepDate
        return false;
    }

    /**
     * @param Timeslot       $timeslot
     * @param PickupLocation $pickupLocation
     * @return array
     * @throws \Exception
     */
    public function getExcludedDates(Timeslot $timeslot, PickupLocation $pickupLocation)
    {
        $country = $pickupLocation->getCompanyEstablishment()->getAddress()->getCountry();
        $excludedDates = [];
        if ($timeslot->getExcludeHolidays()) {
            if(isset($this->holidays[$country->getId()])){
                $excludedDates = $this->holidays[$country->getId()];
            } else {
                /** @var EntityRepository $repository */
                $repository = $this->entityManager->getRepository(HolidayCountry::class);
                $queryBuilder = $repository->createQueryBuilder('holiday_country');
                $queryBuilder->where('holiday_country.country = :country');

                $queryBuilder->leftJoin('holiday_country.holiday', 'holiday');
                $queryBuilder->andWhere('holiday.date >= :date');

                $queryBuilder->select('holiday.date');
                $queryBuilder->setParameters([
                    'country' => $country,
                    'date' => (new \DateTime())->modify('-1 day'),
                ]);
                $excludedDates = $queryBuilder->getQuery()->getScalarResult();
                $this->holidays[$country->getId()] = $excludedDates;
            }
        }
        //if($timeslot->getExcludeCompanyClosingDates()){
        //TODO Get company Excluded dates
        //}
        return $excludedDates;
    }

    /**
     * @param PickupLocation $pickupLocation
     * @param CartOrder      $cartOrder
     * @return DateTime
     * @throws \Exception
     */
    public function getCalculatedPreparationTime(PickupLocation $pickupLocation, CartOrder $cartOrder)
    {
        /** @var Connection $connection */
        $connection = $this->entityManager->getConnection();

        $sql = "
            SELECT MAX(prep.preparation_time) AS max_preparation_time
            FROM
            (
                (
                    SELECT pilo.preparation_time

                    FROM pickup_location AS pilo

                    WHERE pilo.id = :pickupLocationId1
                )
                UNION
                (
                    SELECT paev.value AS preparation_time
                    FROM cart_order_line AS caol
                    LEFT JOIN supplier_product AS supr ON supr.product_id = caol.product_id AND supr.pickup_enabled = 1
                    INNER JOIN  parameter_entity_value AS paev ON paev.entity = supr.supplier_id AND paev.parameter_entity_id = (
                        SELECT id
                        FROM parameter_entity
                        WHERE parameter_id = 'supplier_product_preparation_time'
                    )

                    LEFT JOIN company_establishment AS coes ON coes.company_id = supr.supplier_id
                    INNER JOIN pickup_location AS pilo ON pilo.company_establishment_id = coes.id

                    WHERE pilo.id = :pickupLocationId2
                    AND caol.cart_order_id = :cartOrderId1
                )
                UNION
                (
                    SELECT paev.value AS preparation_time
                    FROM cart_order_line AS caol
                    LEFT JOIN supplier_group_product AS sugp ON caol.product_id = sugp.product_id AND sugp.pickup_enabled = 1
                    LEFT JOIN company_supplier_group AS cosg ON cosg.supplier_group_id = sugp.supplier_group_id
                    INNER JOIN  parameter_entity_value AS paev ON paev.entity = cosg.supplier_group_id AND paev.parameter_entity_id = (
                        SELECT id
                        FROM parameter_entity
                        WHERE parameter_id = 'supplier_group_product_preparation_time'
                    )

                    LEFT JOIN company_establishment AS coes ON coes.company_id = cosg.company_id
                    INNER JOIN pickup_location AS pilo ON pilo.company_establishment_id = coes.id

                    WHERE pilo.id = :pickupLocationId3
                    AND caol.cart_order_id = :cartOrderId2
                )
            ) AS prep
        ";

        $statement = $connection->prepare($sql);

        $statement->bindValue(':pickupLocationId1', $pickupLocation->getId(), PDO::PARAM_INT);
        $statement->bindValue(':pickupLocationId2', $pickupLocation->getId(), PDO::PARAM_INT);
        $statement->bindValue(':pickupLocationId3', $pickupLocation->getId(), PDO::PARAM_INT);
        $statement->bindValue(':cartOrderId1', $cartOrder->getId(), PDO::PARAM_INT);
        $statement->bindValue(':cartOrderId2', $cartOrder->getId(), PDO::PARAM_INT);

        $statement->execute();
        $maxPreperationTime = $statement->fetch()['max_preparation_time'] ?: 1;

        return (new \DateTime())->modify(sprintf('+%d hour', $maxPreperationTime));
    }

    /**
     * TODO Temp Fixed Slots fixtures
     *
     * @return ArrayCollection
     * @throws \Exception
     */
    public function getFixedSlots(): Collection
    {
        return new ArrayCollection([
            new Timeslot(
                new \DateTime('09:00:00'),
                new \DateTime('12:00:00'),
                'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA;INTERVAL=1;DTSTART=20190101',
                true,
                true
            ),
            new Timeslot(
                new \DateTime('13:00:00'),
                new \DateTime('18:00:00'),
                'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;INTERVAL=1;DTSTART=20190101',
                true,
                true
            ),
        ]);
//        return new Collection([
//            new Timeslot(
//                new DateTime('09:00:00'),
//                new DateTime('12:00:00'),
//                'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101',
//                true,
//                true
//            ),
//            new Timeslot(
//                new DateTime('12:00:00'),
//                new DateTime('15:00:00'),
//                'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101',
//                true,
//                true
//            ),
//            new Timeslot(
//                new DateTime('15:00:00'),
//                new DateTime('18:00:00'),
//                'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101',
//                true,
//                true
//            ),
//        ]);
    }
}
