<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\Common\Cache\RedisCache;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderLineService
 * @package AppBundle\Services
 */
class OrderLineService
{
    protected $em;

    use ContainerAwareTrait;

    /**
     * @return array
     */
    public function getRecentOrderLinesByGroup()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        //setup and retrieve cache if available
        $redis = new \Redis();
        $redis->connect($this->container->getParameter('redis_host'), $this->container->getParameter('redis_port'));
        $cacheDriver = new RedisCache();
        $cacheDriver->setRedis($redis);

        if (!$cacheDriver->contains('orderLines')) {
            $start = date('Y-m-d H:i:s', strtotime('-7 days', time()));

            $qb = $em->getRepository(OrderLine::class)->createQueryBuilder('ol');
            $qb->select('pg.id as productgroup, count(ol.product) as orders, pg.title, identity(pg.parent) as parent');
            $qb->leftJoin("ol.product", "p");
            $qb->leftJoin("p.productgroup", "pg");
            $qb->andWhere('ol.createdAt > :start');
            $qb->groupBy("pg.id");

            $qb->setParameter('start', $start);

            $queryResults = $qb->getQuery()->execute();

            $results = [];
            foreach ($queryResults as $result) {
                //add orders to the product group ID
                if ($result['productgroup']) {
                    $results[$result['productgroup']] = $result['orders'];
                }

                //check if parent is available and add orders to the parent too.
                if ($result['parent']) {
                    if (isset($results[$result['parent']])) {
                        $results[$result['parent']] += $result['orders'];
                    } else {
                        $results[$result['parent']] = $result['orders'];
                    }
                }
            }

            //save cache and keep cache alive for 15 minutes
            $cacheDriver->save('orderLines', $results, 60 * 15);
        } else {
            $results = $cacheDriver->fetch('orderLines');
        }

        //return all orders and a list of all product groups
        return [
            'orders' => $results,
            'productGroups' => $em->getRepository(Productgroup::class)->findBy(['parent' => null]),
        ];
    }
}
