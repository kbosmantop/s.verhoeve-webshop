<?php

namespace AppBundle\Services\RegionData;

use AppBundle\Client\DefaultClient;
use AppBundle\Entity\Geography\City;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Geography\Province;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Luxembourg
 * @package AppBundle\Services\RegionData
 */
class Luxembourg {
    /**
     * @var string
     */
    private $source = 'https://raw.githubusercontent.com/TrustChainEG/postal-codes-json-xml-csv/master/data/LU/zipcodes.lu.json';

    /**
     * @var Country
     */
    private $country;

    /**
     * @var ArrayCollection
     */
    private $provinces;

    /**
     * @var ArrayCollection
     */
    private $cities;

    /**
     * @var ArrayCollection
     */
    private $postcodes;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Belgium constructor.
     * @param DefaultClient          $client
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(DefaultClient $client, EntityManagerInterface $entityManager)
    {
        $this->client = $client;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ConnectionException
     * @throws GuzzleException
     */
    public function process(): void
    {
        $this->init();
        $this->read();
        $this->cleanup();
    }

    private function init(): void
    {
        set_time_limit(-1);
        ini_set('memory_limit', -1);

        $this->country = $this->entityManager->getRepository(Country::class)->find('LU');

        if (!$this->country) {
            return;
        }

        $this->loadProvinces();
        $this->loadPostcodes();
        $this->loadCities();
    }

    private function loadProvinces(): void
    {
        $this->provinces = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Province::class);

        $qb = $repository->createQueryBuilder('p');
        $qb->addSelect('translations');
        $qb->andWhere('p.country = :country');
        $qb->leftJoin('p.translations', 'translations');

        $qb->setParameters([
            'country' => $this->country,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Province $province * */
        foreach ($results as $province) {
            $this->provinces->set($province->translate('lu_FR')->getName(), $province);
        }
    }

    private function loadPostcodes(): void
    {
        $this->postcodes = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Postcode::class);

        $qb = $repository->createQueryBuilder('p');
        $qb->leftJoin('p.province', 'provinces');
        $qb->andWhere('provinces.country = :country');
        $qb->andWhere('provinces IN (:provinces)');

        $qb->setParameters([
            'country' => $this->country,
            'provinces' => $this->provinces,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Postcode $postcode * */
        foreach ($results as $postcode) {
            $this->postcodes->set($postcode->getPostcode(), $postcode);
        }
    }

    private function loadCities(): void
    {
        $this->cities = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(City::class);

        $qb = $repository->createQueryBuilder('city');

        $qb->addSelect('postcodes');
        $qb->leftJoin('city.postcodes', 'postcodes');
        $qb->andWhere('postcodes IN (:postcodes)');

        $qb->setParameters([
            'postcodes' => $this->postcodes,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var City $city * */
        foreach ($results as $city) {
            $this->cities->set($city->translate('lu_FR')->getName() . '|' . $city->getProvince()->translate('lu_FR')->getName(),
                $city);
        }
    }

    /**
     * @param $name string
     * @return string
     */
    private function generateKey($name): string
    {
        return str_replace(' ', '-', strtolower($name));
    }

    /**
     * @throws ConnectionException
     * @throws GuzzleException
     */
    private function read(): void
    {
        $doctrineConnection = $this->entityManager->getConnection();
        $doctrineConnection->beginTransaction();

        try {
            $data = json_decode($this->client->request('GET', $this->source)->getBody()->getContents());

            foreach ($data as $row) {
                $province = $this->getProvince($row->state);
                $postcode = $this->getPostcode(substr($row->zipcode, 2), $province);
                $this->getCity($row->place, $postcode, $province);
            }

            $this->entityManager->flush();

            $doctrineConnection->commit();
        } catch (ConnectionException | GuzzleException $e) {
            $doctrineConnection->rollBack();

            throw $e;
        }
    }

    /**
     * @param $name
     * @return Province
     */
    private function getProvince($name): Province
    {
        $province = $this->provinces->get($name);

        if (!$province) {
            $province = new Province();
            $province->setCountry($this->country);
            $province->translate('lu_FR')->setName($name);
            $province->mergeNewTranslations();

            $this->entityManager->persist($province);

            $this->provinces->set($name, $province);
        }

        return $province;
    }

    /**
     * @param          $number
     * @param Province $province
     * @return Postcode
     */
    private function getPostcode($number, Province $province): Postcode
    {
        $postcode = $this->postcodes->get($number);

        if (!$postcode) {
            $postcode = new Postcode();
            $postcode->setProvince($province);
            $postcode->setPostcode($number);

            $this->entityManager->persist($postcode);

            $this->postcodes->set($number, $postcode);
        }

        return $postcode;
    }

    /**
     * @param          $name
     * @param Postcode $postcode
     * @param Province $province
     * @return City
     */
    private function getCity($name, Postcode $postcode, Province $province): City
    {
        $city = $this->cities->get($this->generateKey($name) . '|' . $province->translate('lu_FR')->getName());

        if (!$city) {
            $city = new City();
            $city->setProvince($province);

            $this->entityManager->persist($city);

            $this->cities->set($this->generateKey($name) . '|' . $province->translate('lu_FR')->getName(), $city);
        }

        $city->translate('lu_FR')->setName($name);
        $city->mergeNewTranslations();

        $collection = new ArrayCollection();

        foreach ($city->getPostcodes() as $postcodeC) {
            $collection->set($postcodeC->getPostcode(), $postcodeC);
        }

        if (!$collection->get($postcode->getPostcode())) {
            $city->getPostcodes()->set($postcode->getPostcode(), $postcode);
        }

        return $city;
    }

    private function cleanup(): void
    {
        $this->provinces = null;
        $this->postcodes = null;
        $this->cities = null;
    }
}