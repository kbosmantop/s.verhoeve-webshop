<?php

namespace AppBundle\Services\RegionData;

use AppBundle\Client\DefaultClient;
use AppBundle\Entity\Geography\City;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Geography\Province;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Client;

/**
 * Class Netherlands
 * @package AppBundle\Services\RegionData
 */
class Netherlands
{
    private const URI = 'http://geodata.nationaalgeoregister.nl/locatieserver/v3/free?q=*&fq=type:postcode&start=%d&rows=%d&fl=postcode,woonplaatsnaam,provincienaam&sort=postcode%%20asc';

    /**
     * @var Country
     */
    private $country;

    /**
     * @var ArrayCollection
     */
    private $provinces;

    /**
     * @var ArrayCollection
     */
    private $cities;

    /**
     * @var ArrayCollection
     */
    private $postcodes;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Netherlands constructor.
     * @param DefaultClient          $client
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(DefaultClient $client, EntityManagerInterface $entityManager)
    {
        $this->client = $client;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Exception
     */
    public function process(): void
    {
        $this->init();
        $this->read();
        $this->cleanup();
    }

    private function init(): void
    {
        set_time_limit(-1);
        ini_set('memory_limit', -1);

        $this->country = $this->entityManager->getRepository(Country::class)->find('NL');

        $this->loadProvinces();
        $this->loadPostcodes();
        $this->loadCities();
    }

    private function loadProvinces(): void
    {
        $this->provinces = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Province::class);

        $qb = $repository->createQueryBuilder('province');
        $qb->addSelect('translations');
        $qb->leftJoin('province.translations', 'translations');
        $qb->andWhere('province.country = :country');

        $qb->setParameters([
            'country' => $this->country,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Province $province * */
        foreach ($results as $province) {
            $this->provinces->set($province->translate('nl_NL')->getName(), $province);
        }
    }

    private function loadPostcodes(): void
    {
        $this->postcodes = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Postcode::class);

        $qb = $repository->createQueryBuilder('p');
        $qb->leftJoin('p.province', 'provinces');
        $qb->andWhere('provinces.country = :country');
        $qb->andWhere('provinces IN (:provinces)');

        $qb->setParameters([
            'country' => $this->country,
            'provinces' => $this->provinces,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Postcode $postcode * */
        foreach ($results as $postcode) {
            $this->postcodes->set($postcode->getPostcode(), $postcode);
        }
    }

    private function loadCities(): void
    {
        $this->cities = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(City::class);

        $qb = $repository->createQueryBuilder('c');
        $qb->addSelect('translations');
        $qb->addSelect('postcodes');
        $qb->addSelect('province');
        $qb->leftJoin('c.translations', 'translations');
        $qb->leftJoin('c.postcodes', 'postcodes');
        $qb->leftJoin('c.province', 'province');
        $qb->andWhere('postcodes IN (:postcodes)');

        $qb->setParameters([
            'postcodes' => $this->postcodes,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var City $city * */
        foreach ($results as $city) {
            $indexedName = $this->generateKey($city->translate('nl_NL')->getName());

            $this->cities->set($indexedName . '|' . $city->getProvince()->translate('nl_NL')->getName(), $city);
        }
    }

    /**
     * @param $name string
     * @return string
     */
    private function generateKey($name): string
    {
        return str_replace(' ', '-', strtolower($name));
    }

    /**
     * @param int   $start
     * @param int   $limit
     */
    private function fetchData($start = 0, $limit = 50000): void
    {
        $response = $this->client->get(sprintf(self::URI, $start, $limit));

        $results = json_decode($response->getBody()->getContents());

        foreach ($results->response->docs as $i => $result) {
            $postcode = (int) substr($result->postcode, 0, 4);

            if ($this->postcodes->containsKey($postcode)) {
                continue;
            }

            $province = $this->getProvince($result->provincienaam);
            $postcode = $this->getPostcode($postcode, $province);
            $this->getCity($result->woonplaatsnaam, $postcode, $province);
        }

        if ($results->response->numFound > $start + $limit) {
            $this->fetchData($start + $limit, $limit);
        }
    }

    /**
     * @throws \Exception
     */
    private function read(): void
    {
        $connection = $this->entityManager->getConnection();
        $connection->beginTransaction();

        try {
            $this->fetchData();

            $this->entityManager->flush();

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();

            throw $e;
        }
    }

    /**
     * @param $name
     * @return Province
     */
    private function getProvince($name): Province
    {
        $province = $this->provinces->get($name);

        if (!$province) {
            $province = new Province();
            $province->setCountry($this->country);
            $province->translate('nl_NL')->setName($name);
            $province->mergeNewTranslations();

            $this->entityManager->persist($province);

            $this->provinces->set($name, $province);
        }

        return $province;
    }

    /**
     * @param          $number
     * @param Province $province
     * @return Postcode
     */
    private function getPostcode($number, Province $province): Postcode
    {
        $postcode = $this->postcodes->get($number);

        if (!$postcode) {
            $postcode = new Postcode();
            $postcode->setProvince($province);
            $postcode->setPostcode($number);

            $this->entityManager->persist($postcode);

            $this->postcodes->set($number, $postcode);
        }

        return $postcode;
    }

    /**
     * @param          $name
     * @param Postcode $postcode
     * @param Province $province
     * @return City
     */
    private function getCity($name, Postcode $postcode, Province $province): City
    {
        $city = $this->cities->get($this->generateKey($name) . '|' . $province->translate('nl_NL')->getName());

        if (!$city) {
            $city = new City();
            $city->setProvince($province);

            $this->entityManager->persist($city);

            $this->cities->set($this->generateKey($name) . '|' . $province->translate('nl_NL')->getName(), $city);
        }

        $city->translate('nl_NL')->setName($name);
        $city->mergeNewTranslations();

        $collection = new ArrayCollection();

        foreach ($city->getPostcodes() as $postcodeC) {
            $collection->set($postcodeC->getPostcode(), $postcodeC);
        }

        if (!$collection->get($postcode->getPostcode())) {
            $city->getPostcodes()->set($postcode->getPostcode(), $postcode);
        }

        return $city;
    }

    private function cleanup(): void
    {
        $this->provinces = null;
        $this->postcodes = null;
        $this->cities = null;
    }
}
