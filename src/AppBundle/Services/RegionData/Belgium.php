<?php

namespace AppBundle\Services\RegionData;

use AppBundle\Client\DefaultClient;
use AppBundle\Entity\Geography\City;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Geography\Province;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPExcel_Exception;
use PHPExcel_IOFactory;

/**
 * Class Belgium
 * @package AppBundle\Services\RegionData
 */
class Belgium
{
    /**
     * @see http://www.bpost.be/site/nl/verzenden/adressering/zoek-een-postcode
     */
    private $source_nl = 'http://www.bpost2.be/zipcodes/files/zipcodes_num_nl.xls';
    private $source_fr = 'http://www.bpost2.be/zipcodes/files/zipcodes_num_fr.xls';

    /**
     * @var Country
     */
    private $country;

    /**
     * @var ArrayCollection
     */
    private $provinces;

    /**
     * @var ArrayCollection
     */
    private $cities;

    /**
     * @var ArrayCollection
     */
    private $postcodes;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Belgium constructor.
     * @param DefaultClient          $client
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(DefaultClient $client, EntityManagerInterface $entityManager)
    {
        $this->client = $client;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ConnectionException
     * @throws GuzzleException
     * @throws PHPExcel_Exception
     */
    public function process(): void
    {
        $this->init();
        $this->read();
        $this->cleanup();
    }

    private function init(): void
    {
        set_time_limit(-1);
        ini_set('memory_limit', -1);

        $this->country = $this->entityManager->getRepository(Country::class)->find('BE');

        $this->loadProvinces();
        $this->loadPostcodes();
        $this->loadCities();
    }

    private function loadProvinces(): void
    {
        $this->provinces = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Province::class);

        $qb = $repository->createQueryBuilder('p');
        $qb->addSelect('translations');
        $qb->andWhere('p.country = :country');
        $qb->leftJoin('p.translations', 'translations');

        $qb->setParameters([
            'country' => $this->country,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Province $province * */
        foreach ($results as $province) {
            $this->provinces->set($province->translate('nl_BE')->getName(), $province);
        }
    }

    private function loadPostcodes(): void
    {
        $this->postcodes = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(Postcode::class);

        $qb = $repository->createQueryBuilder('p');
        $qb->leftJoin('p.province', 'provinces');
        $qb->andWhere('provinces.country = :country');
        $qb->andWhere('provinces IN (:provinces)');

        $qb->setParameters([
            'country' => $this->country,
            'provinces' => $this->provinces,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var Postcode $postcode * */
        foreach ($results as $postcode) {
            $this->postcodes->set($postcode->getPostcode(), $postcode);
        }
    }

    private function loadCities(): void
    {
        $this->cities = new ArrayCollection();

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(City::class);

        $qb = $repository->createQueryBuilder('city');

        $qb->addSelect('postcodes');
        $qb->leftJoin('city.postcodes', 'postcodes');
        $qb->andWhere('postcodes IN (:postcodes)');

        $qb->setParameters([
            'postcodes' => $this->postcodes,
        ]);

        $results = $qb->getQuery()->getResult();

        /** @var City $city * */
        foreach ($results as $city) {
            $this->cities->set($city->translate('nl_BE')->getName() . '|' . $city->getProvince()->translate('nl_BE')->getName(),
                $city);
        }
    }

    /**
     * @throws ConnectionException
     * @throws GuzzleException
     * @throws PHPExcel_Exception
     */
    private function read(): void
    {
        $doctrineConnection = $this->entityManager->getConnection();
        $doctrineConnection->beginTransaction();

        try {
            $path_nl = tempnam('/tmp', 'nl');
            $path_fr = tempnam('/tmp', 'fr');

            $this->client->request('GET', $this->source_nl, [
                'sink' => $path_nl,
            ]);

            $this->client->request('GET', $this->source_fr, [
                'sink' => $path_fr,
            ]);

            $worksheet_nl = PHPExcel_IOFactory::load($path_nl)->getActiveSheet();
            $worksheet_fr = PHPExcel_IOFactory::load($path_fr)->getActiveSheet();

            foreach ($worksheet_nl->getRowIterator() as $row) {
                if ($row->getRowIndex() === 1) {
                    continue;
                }

                $postcode = $worksheet_nl->getCellByColumnAndRow(0, $row->getRowIndex())->getValue();

                // Ignore postcode for 'Sinterklaas', solves SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry 'belgique/saint-nicolas' for key 'UNIQ_274F5EBF989D9B62'
                if ((int) $postcode === 612) {
                    continue;
                }

                $city_nl = $worksheet_nl->getCellByColumnAndRow(1, $row->getRowIndex())->getValue();
                $city_fr = $worksheet_fr->getCellByColumnAndRow(1, $row->getRowIndex())->getValue();

                $province_nl = trim($worksheet_nl->getCellByColumnAndRow(3, $row->getRowIndex())->getValue());
                $province_fr = trim($worksheet_fr->getCellByColumnAndRow(3, $row->getRowIndex())->getValue());

                if ($city_nl === mb_strtoupper($city_nl)) {
                    $city_nl = ucfirst(mb_strtolower($city_nl));
                }

                if ($city_fr === mb_strtoupper($city_fr)) {
                    $city_fr = ucfirst(mb_strtolower($city_fr));
                }

                $province_nl = preg_replace("|\s\([a-z0-9\s]*\)|i", '', $province_nl);
                $province_fr = preg_replace("|\s\([a-z0-9\s]*\)|i", '', $province_fr);

//                if (!$postcode || !($city_nl || $city_fr) || !($province_nl || $province_fr)) {
//                    continue;
//                }

                if (!$postcode || !$city_nl || !$province_nl) {
                    continue;
                }

                $province = (object)[
                    'nl' => $province_nl,
                    'fr' => $province_fr,
                ];

                $city = (object)[
                    'nl' => $city_nl,
                    'fr' => $city_fr,
                ];

                $province = $this->getProvince($province);
                $postcode = $this->getPostcode($postcode, $province);

                $this->getCity($city, $postcode, $province, $row->getRowIndex());
            }

            unlink($path_nl);
            unlink($path_fr);

            $this->entityManager->flush();

            $doctrineConnection->commit();
        } catch (ConnectionException | GuzzleException | PHPExcel_Exception $e) {
            $doctrineConnection->rollBack();

            throw $e;
        }
    }

    /**
     * @param $name
     * @return Province
     */
    private function getProvince($name): Province
    {
        $province = $this->provinces->get($name->nl);

        if (!$province) {
            $province = new Province();
            $province->setCountry($this->country);
            $province->translate('nl_BE')->setName($name->nl);
            $province->translate('fr_BE')->setName($name->fr);
            $province->mergeNewTranslations();

            $this->entityManager->persist($province);

            $this->provinces->set($name->nl, $province);
        }

        return $province;
    }

    /**
     * @param          $number
     * @param Province $province
     * @return Postcode
     */
    public function getPostcode($number, Province $province): Postcode
    {
        $postcode = $this->postcodes->get($number);

        if (!$postcode) {
            $postcode = new Postcode();
            $postcode->setProvince($province);
            $postcode->setPostcode($number);

            $this->entityManager->persist($postcode);

            $this->postcodes->set($number, $postcode);
        }

        return $postcode;
    }

    /**
     * @param          $name
     * @param Postcode $postcode
     * @param Province $province
     * @param          $line
     * @return City
     */
    private function getCity($name, Postcode $postcode, Province $province, $line): City
    {
        void($line);

        $city = $this->cities->get($name->nl . '|' . $province->translate('nl_BE')->getName());

        if (!$city) {
            $city = new City();
            $city->setProvince($province);
            $city->translate('nl_BE')->setName($name->nl);
            $city->translate('fr_BE')->setName($name->fr);
            $city->mergeNewTranslations();

            $this->entityManager->persist($city);

            $this->cities->set($name->nl . '|' . $province->translate('nl_BE')->getName(), $city);
        }

        $collection = new ArrayCollection();

        foreach ($city->getPostcodes() as $postcodeC) {
            $collection->set($postcodeC->getPostcode(), $postcodeC);
        }

        if (!$collection->get($postcode->getPostcode())) {
            $city->getPostcodes()->set($postcode->getPostcode(), $postcode);
        }

        return $city;
    }

    private function cleanup(): void
    {
        $this->provinces = null;
        $this->postcodes = null;
        $this->cities = null;
    }
}