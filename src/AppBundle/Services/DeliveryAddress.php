<?php

namespace AppBundle\Services;

use AppBundle\Entity\Geography\DeliveryAddressType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Domain
 *
 * @package AppBundle\Services
 */
class DeliveryAddress
{
    use ContainerAwareTrait;

    /**
     *
     * @return \AppBundle\Entity\Site\Domain[]
     */
    public function getTypes()
    {

        $types = $this->container->get('doctrine')->getRepository(DeliveryAddressType::class)->findAll();
        $site = $this->container->get(SiteService::class)->determineSite();
        $order = $this->container->get(CartService::class)->getCurrentOrder();

        /**
         * Check for excluded sites
         *
         * @var DeliveryAddressType $type
         */
        foreach ($types as $key => $type) {
            // If site is not found, remove item from results
            if (!$type->getSites()->contains($site)) {
                unset ($types[$key]);

                continue;
            }
        }

        /**
         * Check for excluded assortments
         *
         * @var DeliveryAddressType $type
         */
        foreach ($types as $key => $type) {
            foreach ($type->getExcludedAssortments() as $excludedAssortment) {
                foreach ($order->getLines() as $line) {
                    if (!$line->getProduct()->getPurchasable()) {
                        continue;
                    }

                    if ($line->getProduct()->getParent()) {
                        $assortments = $line->getProduct()->getParent()->getAssortments();
                    } else {
                        $assortments = $line->getProduct()->getAssortments();
                    }

                    if ($assortments->contains($excludedAssortment)) {
                        unset ($types[$key]);

                        continue;
                    }
                }
            }
        }

        return $types;
    }
}