<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Services\Sales\Order\OrderActivityService;
use AppBundle\Entity\Relation\Company as Supplier;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Interfaces\ConnectorInterface;
use AppBundle\Services\Finance\Tax\VatGroupService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Workflow\Registry;
use Psr\Log\LoggerInterface;
use function sprintf;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use function in_array;

/**
 * @todo rename to SupplierOrderManager since its only used for supplier Orders
 * Class OrderManagerService
 * @package AppBundle\Services
 */
class OrderManagerService
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var ConnectorHelper $conectorHelper
     */
    private $connectorHelper;

    /**
     * @var OrderActivityService
     */
    private $orderActivity;

    /**
     * @var SupplierManagerService
     */
    private $supplierManager;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * OrderManagerService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TokenStorage           $tokenStorage
     * @param ConnectorHelper        $connectorHelper
     * @param SupplierManagerService $supplierManager
     * @param Registry               $registry
     */
    public function __construct(
        TokenStorage $tokenStorage,
        ConnectorHelper $connectorHelper,
        SupplierManagerService $supplierManager,
        EntityManagerInterface $entityManager,
        Registry $registry,
        VatGroupService $vatGroupService,
        LoggerInterface $logger,
        OrderActivityService $orderActivityService
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->connectorHelper = $connectorHelper;
        $this->supplierManager = $supplierManager;
        $this->entityManager = $entityManager;
        $this->registry = $registry;
        $this->vatGroupService = $vatGroupService;
        $this->orderActivity = $orderActivityService;
        $this->logger = $logger;
    }

    /**
     * @param Order $order
     * @param bool  $preventFlush
     *
     * @todo Move to OrderManager
     */
    public function cancel(Order $order, bool $preventFlush = false): void
    {
        $this->registry->get($order)->apply($order, 'cancel');

        if (false === $preventFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param Order $order
     * @param Supplier $supplier
     *
     * @throws Exception
     * @deprecated Replaced by forwardToSupplier method, which uses the Workflow transitions
     */
    public function forwardOrder(Order $order, Supplier $supplier)
    {
        $supplierOrder = $this->createSupplierOrder($order, $supplier);

        $this->forwardToSupplier($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     */
    public function forwardToSupplier(SupplierOrder $supplierOrder): void
    {
        $order = $supplierOrder->getOrder();
        $supplier = $supplierOrder->getSupplier();

        try {
            $connector = $this->connectorHelper->getConnector($supplier);

            if (null === $connector) {
                throw new \RuntimeException(sprintf('Kan geen connector vinden voor leverancier %s', $supplier));
            }

            if ($connector->requireForwardingPrices()) {
                foreach ($supplierOrder->getLines() as $line) {
                    if ($line->getPrice() === null) {
                        throw new \RuntimeException('Deze order bevat één of meerdere regels zonder doorgeefprijs');
                    }
                }
            }

            $connector->process($supplierOrder);
        } catch (\Exception $e) {
            $this->registry->get($supplierOrder)->apply($supplierOrder, 'fail');
            $this->entityManager->flush();

            throw $e;
        }

        $this->registry->get($supplierOrder)->apply($supplierOrder, 'transmit');

        try {
            $this->registerActivity($order, 'order_processed');
        } catch (\Exception $e) {
            $this->logger->addWarning(sprintf('Failed to write activity for supplier order: %s',
                $supplierOrder->getId()));
        }

        $this->entityManager->flush();
    }

    /**
     * @param Order $order
     * @param Supplier $supplier
     *
     * @return SupplierOrder
     */
    public function createSupplierOrder(Order $order, Supplier $supplier)
    {
        if ($order->getStatus() !== 'new') {
            throw new \RuntimeException(sprintf("Order '%s' is not processable", $order->getOrderNumber()));
        }

        $supOrder = $order->getSupplierOrder();
        if (null !== $supOrder && !in_array($supOrder->getStatus(), ['cancelled', 'failed'], true)) {
            throw new \RuntimeException(sprintf('There is an existing supplierOrder, cancel the supplierOrder first (order: %s)',
                $order->getNumber()));
        }

        // TODO: De connector is op dit moment nodig om te bepalen of de order gecommisioneerd moet worden of niet,
        // TODO: dit ook op basis van configuratie op de supplier entity gedaan kunnen worden.
        $connector = $this->connectorHelper->getConnector($supplier);

        if ($connector === null) {
            throw new \RuntimeException(sprintf("No connector configured for '%s'", $supplier->getName()));
        }

        $supplierOrder = new SupplierOrder();
        $supplierOrder->setSupplier($supplier);
        $supplierOrder->setOrder($order);
        $order->setSupplierOrder($supplierOrder);
        $supplierOrder->setConnector($supplier->getSupplierConnector());
        $supplierOrder->setCommissionable($connector->getCommissionable());

        foreach ($order->getLines() as $orderLine) {
            $supplierOrderLines = $this->createSupplierOrderLines($orderLine, $supplier, $connector);

            foreach ($supplierOrderLines as $supplierOrderLine) {
                $supplierOrder->addLine($supplierOrderLine);
            }
        }

        $this->entityManager->persist($supplierOrder);

        return $supplierOrder;
    }

    /**
     * @param OrderLine $orderLine
     * @param Supplier $supplier
     * @param ConnectorInterface $connector
     *
     * @return SupplierOrderLine[]
     * @throws Exception
     */
    public function createSupplierOrderLines(
        OrderLine $orderLine,
        Supplier $supplier,
        ConnectorInterface $connector
    ): array {
        $supplierOrderLines = [];

        if ($orderLine->getProduct()->isCombination()) {
            foreach ($orderLine->getProduct()->getCombinations() as $combination) {
                $product = $combination->getProduct();

                $supplierOrderLines[] = $this->createSupplierOrderLineFromProduct($orderLine, $product, $supplier,
                    $connector);
            }
        } else {
            $product = $orderLine->getProduct();

            $supplierOrderLines[] = $this->createSupplierOrderLineFromProduct($orderLine, $product, $supplier,
                $connector);
        }

        return $supplierOrderLines;
    }

    /**
     * @param OrderLine $orderLine
     * @param Product $product
     * @param Supplier $supplier
     * @param ConnectorInterface $connector
     *
     * @return SupplierOrderLine
     * @throws Exception
     */
    private function createSupplierOrderLineFromProduct(
        OrderLine $orderLine,
        Product $product,
        Supplier $supplier,
        ConnectorInterface $connector
    ) {
        $supplierProduct = $this->supplierManager->getSupplierProduct($product, $supplier);

        $supplierOrderLine = new SupplierOrderLine();
        $supplierOrderLine->setOrderLine($orderLine);

        switch (\get_class($supplierProduct)) {
            case SupplierProduct::class:
                $supplierOrderLine->setSupplierProduct($supplierProduct);
                break;
            case SupplierGroupProduct::class:
                $supplierOrderLine->setSupplierGroupProduct($supplierProduct);
                break;
        }

        $supplierOrderLine->setQuantity($orderLine->getQuantity());

        if ($connector->requireForwardingPrices()) {
            $supplierOrderLine->setPrice((float)$supplierProduct->getForwardPrice());

            if ($product instanceof TransportType) {
                $deliveryArea = $this->supplierManager->getSupplierDeliveryArea($supplier,
                    $orderLine->getOrder());

                if ($deliveryArea) {
                    $supplierOrderLine->setPrice((float)$deliveryArea->getDeliveryPrice());
                }
            }
        }

        $country =  $supplier->getInvoiceAddress() ? $supplier->getInvoiceAddress()->getCountry() : $supplier->getCompanyRegistration()->getCountry();
        $vatGroup = $this->vatGroupService->determineVatGroupForProduct($orderLine->getProduct(), $country);
        $vatRate = $vatGroup->getRateByDate($orderLine->getOrder()->getDeliveryDate(), true);

        if (null === $vatRate) {
            throw new \RuntimeException(sprintf("Couldn't determine vatRate for orderLine %s",
                $orderLine->getId()));
        }

        $supplierOrderLine->setVatRate($vatRate);

        return $supplierOrderLine;
    }

    /**
     * @param Order $order
     *
     * @todo remove
     */
    public function processBySupplier(Order $order): void
    {
        $this->registry->get($order)->apply($order, 'process_by_supplier');

        $this->entityManager->flush();
    }

    /**
     * @param Order $order
     *
     * @todo remove
     */
    public function sendToCustomer(Order $order): void
    {
        $this->registry->get($order)->apply($order, 'send_to_customer');

        $this->entityManager->flush();
    }

    /**
     * @param Order  $order
     * @param string $activity
     */
    public function registerActivity(Order $order, string $activity)
    {
        $this->orderActivity->add($activity, $order);
    }
}
