<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Report\CompanyReportCustomerHistory;
use AppBundle\Entity\Site\Site;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class CompanyReportCustomerMailer
 * @package AppBundle\Services\Mailer
 */
class CompanyReportCustomerMailer extends AbstractMailer
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $parameters = $this->resolveParameters($parameters);

        /** @var CompanyReportCustomerHistory $companyReportCustomerHistory */
        $companyReportCustomerHistory = $parameters['companyReportCustomerHistory'];

        $customer = $companyReportCustomerHistory->getCompanyReportCustomer()->getCustomer();

        $parameters['to'] = [$customer->getEmail() => $customer->getFullname()];
        $parameters['url'] = $this->router->generate(
            'app_customer_account_report_index',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        /** @var Site $site */
        $site = $customer->getRegisteredOnSite();
        $parameters['site'] = $site;
        $parameters['customer'] = $customer;

        $template = '@AppBundle/Resources/views/Emails/company-report.html.twig';

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver->setRequired([
            'companyReportCustomerHistory',
        ]);

        return $resolver->resolve($parameters);
    }

}
