<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Order\Collo;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;

/**
 * Class OrderDeliveryStatusMailer
 * @package AppBundle\Services\Mailer
 */
class OrderDeliveryStatusMailer extends AbstractMailer
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        if (PHP_SAPI !== 'cli' && !isset($parameters['preview'])) {
            throw new \LogicException('Should always be done via command.');
        }

        $parameters = $this->resolveParameters($parameters);

        /** @var Order $order */
        $this->order = $order = $parameters['order'];

        /** @var OrderCollection $orderCollection */
        $orderCollection = $order->getOrderCollection();

        // Translate mail/assets into user defined locale.
        $locale = $orderCollection->getLocale();
        $this->translator->setLocale($locale);

        $site = $orderCollection->getSite();

        $to = $orderCollection->getCustomer()->getEmail();

        $template = $this->determineTemplate($site, 'order-delivery-status');

        $trackAndTraceInfo = '';
        if (!empty($this->getTrackAndTraceInfo())) {
            $trackAndTraceInfo .= PHP_EOL;
            $trackAndTraceInfo .= $this->translator->trans('mailer.order_delivery_status.track_and_trace_info') . PHP_EOL;
            $trackAndTraceInfo .= trim(implode('<br />', $this->getTrackAndTraceInfo())) . PHP_EOL;
        }

        $parameters['to'] = $to;
        $parameters['site'] = $site;
        $parameters['locale'] = $locale;
        $parameters['customer'] = $orderCollection->getCustomer();
        $parameters['trackAndTraceInfo'] = $trackAndTraceInfo;
        $parameters['customerServiceEmail'] = $orderCollection->getSite()->getEmail();
        $parameters['orderNumber'] = $order->getOrderNumber();
        $parameters['utm'] = [
            'utm_campaign' => 'order_status_mail',
        ];

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver->setDefaults([
            'preview' => false,
        ]);

        $resolver->setRequired('order');
        $resolver->setAllowedTypes('order', [Order::class]);

        $resolver->setAllowedTypes('preview', ['bool']);

        return $resolver->resolve($parameters);
    }

    /**
     * @return array
     */
    private function getTrackAndTraceInfo()
    {
        $info = [];

        $countryCode = $this->order->getDeliveryAddressCountry()->getCode();
        $postcode = $this->order->getDeliveryAddressPostcode();

        /** @var Collo $collo */
        foreach ($this->order->getSentOrderCollos() as $collo) {
            $link = sprintf('https://jouw.postnl.nl/#!/track-en-trace/zoeken/%s/%s/%s', $collo->getCode(), $countryCode,
                $postcode);

            $info[$collo->getId()] = sprintf('<a href="%s" target="_blank">%s</a>', $link, $link);
        }

        return $info;
    }
}