<?php
namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\ChoosePasswordException;
use AppBundle\Interfaces\Mailer\MailerInterface;
use AppBundle\Services\Mailer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CompanyChoosePasswordMailer
 * @package AppBundle\Services\Mailer
 */
class CompanyChoosePasswordMailer extends AbstractMailer
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $parameters = $this->resolveParameters($parameters);

        /** @var Customer $customer */
        $customer = $parameters['customer'];

        /** @var Site $site */
        $site = $parameters['site'];

        // Translate mail/assets into user defined locale.
        $locale = $customer->getLocale() ?: 'nl_NL';
        $this->translator->setLocale($locale);

        $parameters['to'] = [$customer->getEmail() => $customer->getFullname()];
        $parameters['url'] = $this->generateUrl($customer);
        $parameters['locale'] = $locale;

        $template = $this->determineTemplate($site, 'company-choose-password');

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver->setRequired([
            'site',
            'company',
            'customer',
        ]);

        $resolver->setAllowedTypes('site', Site::class);
        $resolver->setAllowedTypes('company', Company::class);
        $resolver->setAllowedTypes('customer', Customer::class);

        return $resolver->resolve($parameters);
    }

    /**
     * @param Customer $customer
     * @return string
     * @throws ChoosePasswordException
     */
    private function generateUrl(Customer $customer): string
    {
        $token = $customer->getConfirmationToken();

        if(null === $token) {
            throw new ChoosePasswordException('No choose password token defined');
        }

        return $this->router->generate(
            'app_customer_choosepassword_index',
            [
                'token' => $token
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}