<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Sales\Order\Invoice;
use Psr\Log\LoggerInterface;
use Swift_Attachment;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OrderConfirmation
 * @package AppBundle\Services\Mailer
 */
class OrderConfirmationMailer extends AbstractMailer
{
    /**
     * @var Invoice
     */
    protected $invoiceService;

    /**
     * InvoiceMailer constructor.
     * @param Swift_Mailer        $mailer
     * @param TwigEngine          $twig
     * @param TranslatorInterface $translator
     * @param RouterInterface     $router
     * @param LoggerInterface     $logger
     * @param Invoice             $invoiceService
     */
    public function __construct(
        Swift_Mailer $mailer,
        TwigEngine $twig,
        TranslatorInterface $translator,
        RouterInterface $router,
        LoggerInterface $logger,
        Invoice $invoiceService
    ) {
        parent::__construct($mailer, $twig, $translator, $router, $logger);

        // @todo: moves the code that uses these services to own classes.
        $this->invoiceService = $invoiceService;
    }

    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        if (PHP_SAPI !== 'cli' && !isset($parameters['preview'])) {
            throw new \LogicException('Should always be done via command.');
        }

        $parameters = $this->resolveParameters($parameters);

        /** @var OrderCollection $orderCollection */
        $orderCollection = $parameters['orderCollection'];

        // Translate mail/assets into user defined locale.
        $locale = $orderCollection->getLocale();
        $this->translator->setLocale($locale);

        $site = $orderCollection->getSite();

        $to = $orderCollection->getCustomer()->getEmail();

        $template = $this->determineTemplate($site, 'order-confirmation');

        $parameters['to'] = $to;
        $parameters['site'] = $site;
        $parameters['locale'] = $locale;
        $parameters['utm'] = [
            'utm_campaign' => 'orderbevestiging',
        ];

        $attachment = $this->generateInvoiceAttachment($orderCollection);

        if (null !== $attachment) {
            $parameters['attachments'][] = $attachment;
        }

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param OrderCollection $orderCollection
     * @return null|Swift_Attachment
     */
    private function generateInvoiceAttachment(OrderCollection $orderCollection): ?Swift_Attachment
    {
        if (!$orderCollection->hasPaymentMethod('invoice')) {
            try {
                $pdf = $this->invoiceService->setItem($orderCollection);

                $invoiceAttachment = new \Swift_Attachment();
                $invoiceAttachment->setBody($pdf->getContents());
                $invoiceAttachment->setContentType('application/pdf');
                $invoiceAttachment->setFilename($pdf->getFilename());

                return $invoiceAttachment;
            } catch (\Exception $e) {
                $message = sprintf("Generating of invoice '%s' failed (%s)", $orderCollection->getNumber(),
                    $e->getMessage());

                $this->logger->alert($message, ['exception' => $e]);
            }
        }

        return null;
    }

    /**
     * @param $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver->setDefaults([
            'preview' => false,
        ]);

        $resolver->setRequired([
            'orderCollection',
        ]);

        $resolver->setAllowedTypes('orderCollection', [OrderCollection::class]);

        return $resolver->resolve($parameters);
    }
}