<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Sales\Order\Invoice;
use Psr\Log\LoggerInterface;
use Swift_Attachment;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class InvoiceMailer
 * @package AppBundle\Services
 */
class InvoiceMailer extends AbstractMailer
{
    /** @var Invoice */
    private $invoiceService;

    /**
     * InvoiceMailer constructor.
     * @param Swift_Mailer        $mailer
     * @param TwigEngine          $twig
     * @param TranslatorInterface $translator
     * @param RouterInterface     $router
     * @param LoggerInterface     $logger
     * @param Invoice             $invoiceService
     */
    public function __construct(
        Swift_Mailer $mailer,
        TwigEngine $twig,
        TranslatorInterface $translator,
        RouterInterface $router,
        LoggerInterface $logger,
        Invoice $invoiceService
    ) {
        parent::__construct($mailer, $twig, $translator, $router, $logger);

        $this->invoiceService = $invoiceService;
    }

    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $parameters = $this->resolveParameters($parameters);

        /** @var OrderCollection $orderCollection */
        $orderCollection = $parameters['orderCollection'];

        // Translate mail/assets into user defined locale.
        $this->translator->setLocale($orderCollection->getCustomer()->getLocale() ?: 'nl_NL');

        if ($orderCollection->hasPaymentMethod('invoice')) {
            $this->logger->alert(sprintf('
		        Sending an invoice PDF for order %s with payment method "invoice" is not allowed
	        ', $orderCollection->getNumber()));

            return false;
        }

        // @todo: use site of supplier
        $site = $orderCollection->getSite();

        $template = $this->determineTemplate($site, 'invoice');

        $attachment = $this->generateInvoiceAttachment($orderCollection);

        $parameters['site'] = $site;

        if (null !== $attachment) {
            $parameters['attachments'][] = $attachment;
        }

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param OrderCollection $orderCollection
     * @return null|Swift_Attachment
     */
    private function generateInvoiceAttachment(OrderCollection $orderCollection): ?Swift_Attachment
    {
        try {
            $pdf = $this->invoiceService->setItem($orderCollection);

            $invoiceAttachment = new Swift_Attachment();
            $invoiceAttachment->setBody($pdf->getContents());
            $invoiceAttachment->setContentType('application/pdf');
            $invoiceAttachment->setFilename($pdf->getFilename());

            return $invoiceAttachment;
        } catch (\Exception $e) {
            $message = sprintf("Generating of invoice '%s' failed (%s)", $orderCollection->getNumber(), $e->getMessage());

            $this->logger->alert($message, ['exception' => $e]);
        }

        return null;
    }

    /**
     * @param $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();
        $resolver->setRequired([
            'orderCollection',
            'to',
        ]);

        $resolver->setAllowedTypes('orderCollection', [OrderCollection::class]);

        return $resolver->resolve($parameters);
    }
}