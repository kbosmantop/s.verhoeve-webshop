<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;

/**
 * Class RegistrationMailer
 * @package AppBundle\Services\Mailer
 */
class RegistrationMailer extends AbstractMailer
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $this->resolveParameters($parameters);

        $site = $parameters['site'];

        /** @var Customer $customer */
        $customer = $parameters['customer'];

        // Translate mail/assets into user defined locale.
        $this->translator->setLocale($customer->getLocale());

        $parameters['to'] = [$customer->getEmail() => $customer->getFullname()];

        $template = $this->determineTemplate($site, 'registration');

        if (!empty($parameters['company']) && $parameters['company'] instanceof Company) {
            $template = $this->determineTemplate($site, 'company-registration');
        }

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();
        $resolver->setRequired([
            'customer',
            'site',
        ]);

        $resolver->setDefaults([
            'company' => null,
        ]);

        return $resolver->resolve($parameters);
    }
}