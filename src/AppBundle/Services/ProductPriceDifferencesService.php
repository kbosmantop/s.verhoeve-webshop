<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Product\PriceInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\Finance\Tax\VatGroupService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class PriceService
 * @package AppBundle\Services
 */
class ProductPriceDifferencesService
{
    /**
     * @var PriceService
     */
    private $priceService;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * ProductPriceDifferencesService constructor.
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService, VatGroupService $vatGroupService, ProductManager $productManager)
    {
        $this->priceService = $priceService;
        $this->vatGroupService = $vatGroupService;
        $this->productManager = $productManager;
    }

    /**
     * Calculate price differences between the first product and the other collection products
     *
     * @param Collection $collection
     * @param bool       $useCombinationMainProduct
     * @return array
     * @throws \Exception
     */
    public function getDifferences(Collection $collection, bool $useCombinationMainProduct)
    {
        $priceIndex = [];

        if ($useCombinationMainProduct) {
            $collection = $this->getMainProducts($collection);
        }

        $sortedCollection = $this->sortCollectionOnPrice($collection);
        /** @var Product $firstProduct */
        $firstProduct = $this->productManager->decorateProduct($sortedCollection->first());
        $vatGroup = $this->vatGroupService->determineVatGroupForProduct($firstProduct);

        $firstProductPrice = $this->priceService->getRawDisplayPrice($firstProduct->calculatePrice(), $vatGroup->getRateByDate());

        /** @var Product $product */
        foreach ($sortedCollection as $product) {
            $product = $this->productManager->decorateProduct($product);

            $vatGroup = $this->vatGroupService->determineVatGroupForProduct($product);
            $productPrice = $this->priceService->getRawDisplayPrice($product->calculatePrice(), $vatGroup->getRateByDate());

            $priceIndex[$product->getId()] = $this->priceService->getDisplayPrice($productPrice - $firstProductPrice);
        }

        return $priceIndex;
    }

    /**
     * @param Collection $collection
     * @return ArrayCollection|Product[]
     */
    private function sortCollectionOnPrice(Collection $collection): ArrayCollection
    {
        $iterator = $collection->getIterator();
        $iterator->uasort(function (PriceInterface $a, PriceInterface $b) {
            return $a->getPrice() <=> $b->getPrice();
        });

        return new ArrayCollection(iterator_to_array($iterator));
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    private function getMainProducts(Collection $collection)
    {
        return $collection->map(function (Product $variation) {
            if (null === $variation->getMainCombination()) {
                throw new \RuntimeException('Main product in combination not found.');
            }

            return $variation->getMainCombination()->getProduct();
        });
    }

}