<?php

namespace AppBundle\Services\Discount;

use AppBundle\DBAL\Types\VoucherUsageType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Discount\VoucherBatch;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Exceptions\Discount\InvalidVoucherCodeException;
use AppBundle\Exceptions\Discount\VoucherExpiredException;
use AppBundle\Exceptions\Discount\VoucherUsedException;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Services\Parameter\ParameterService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use function sprintf;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class VoucherService
 * @package AppBundle\Services\Discount
 */
class VoucherService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var DiscountService
     */
    private $discountService;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * VoucherService constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface  $tokenStorage
     * @param DiscountService        $discountService
     * @param \Swift_Mailer          $mailer
     * @param ParameterService       $parameterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TokenStorageInterface $tokenStorage,
        DiscountService $discountService,
        \Swift_Mailer $mailer,
        ParameterService $parameterService
    ) {

        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->discountService = $discountService;
        $this->mailer = $mailer;
        $this->parameterService = $parameterService;
    }

    /**
     * @param VoucherBatch $batch
     * @param int          $quantity
     * @param array        $voucherData
     * @return bool
     * @throws Exception
     */
    public function generateBatch(VoucherBatch $batch)
    {
        $conn = $this->entityManager->getConnection();

        $conn->beginTransaction();

        $metadata = $batch->getMetadata();
        for ($i = 0; $i < $metadata['quantity']; $i++) {
            $currentTime = new DateTime();

            $conn->insert('voucher', [
                '`description`' => $batch->getName(),
                'discount_id' => $metadata['discount'],
                '`usage`' => $metadata['usage'],
                '`limit`' => $metadata['limit'],
                'valid_from' => $metadata['validFrom'],
                'valid_until' => $metadata['validUntil'],
                'code' => $this->generateUniqueVoucherCode($batch->getPrefix()),
                'batch_id' => $batch->getId(),
                'created_at' => $currentTime->format('Y-m-d H:i:s'),
                'updated_at' => $currentTime->format('Y-m-d H:i:s')
            ]);
        }

        $conn->commit();

        if (isset($metadata['user'])) {
            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->find($metadata['user']);

            $notificationEmailAddress = $this->parameterService->setEntity()->getValue('default_sender_notification_email');

            $message = (new \Swift_Message())
                ->setSubject('Voucher batch gegenereerd.')
                ->setFrom($notificationEmailAddress, 'Topgeschenken')
                ->setTo($user->getEmail())
                ->setBody('Je voucher batch is succesvol gegenereerd.');

            $this->mailer->send($message);
        }

        return true;
    }

    /**
     * @param Voucher $voucher
     * @return bool
     */
    public function invalidateVoucher(Voucher $voucher): bool
    {
        if (($limit = $voucher->getLimit()) === null || $voucher->getOrderCollections()->count() < $limit) {
            try {
                $voucher->setValidUntil((new DateTime())->modify('-1 day'));
            } catch (Exception $e) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param Order    $order
     * @param Discount $discount
     * @return Voucher
     * @throws Exception
     */
    public function generateRestitutionVoucher(Order $order, Discount $discount)
    {
        $voucher = new Voucher();
        $voucher->setDescription(sprintf('Restitution voucher for order %s', $order->getOrderNumber()));
        $voucher->setDiscount($discount);
        $voucher->setUsage(VoucherUsageType::ONCE);
        $voucher->setLimit(1);
        $voucher->setValidFrom(new DateTime());
        $voucher->setValidUntil((new DateTime())->modify('+1 month'));
        $voucher->setCode($this->generateUniqueVoucherCode('CS'));
        $order->setRestitutionVoucher($voucher);

        $this->entityManager->persist($voucher);
        return $voucher;
    }

    /**
     * @param string $prefix
     * @param int    $codeLength
     * @return string
     * @throws Exception
     */
    public function generateUniqueVoucherCode(?string $prefix = '', $codeLength = 12)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = \strlen($characters);
        $start = \strlen($prefix);
        if ($start >= $codeLength) {
            throw new \RuntimeException('Prefix cannot be longer than code length');
        }
        $qb = $this->entityManager->getConnection()->createQueryBuilder();
        $checkLimit = $length ^ ($codeLength - $start);

        while ($checkLimit-- > 0) {
            $randomString = $prefix;
            for ($i = $start; $i < $codeLength; $i++) {
                $randomString .= $characters[random_int(0, $length - 1)];
            }

            $qb->
            select('id')
                ->from('voucher')
                ->where('code = :code')
                ->setParameter('code', $randomString);
            if ($qb->execute()->rowCount() === 0) {
                return $randomString;
            }
        }

        throw new \RuntimeException('No unique code found');
    }

    /**
     * @param VoucherBatch $batch
     * @param array        $voucherData
     * @return bool
     * @throws Exception
     */
    public function editBatch(VoucherBatch $batch, array $voucherData = [])
    {
        $this->resolveBatchOptions($voucherData);

        if(is_int($voucherData['discount'])) {
            $voucherData['discount'] = $this->entityManager->getRepository(Discount::class)->find($voucherData['discount']);
        }

        $qb = $this->entityManager->getRepository(Voucher::class)->createQueryBuilder('voucher');
        $qb->update()
            ->set('voucher.limit', ':limit')
            ->set('voucher.discount', ':discount')
            ->set('voucher.usage', ':usage')
            ->set('voucher.validFrom', ':validFrom')
            ->set('voucher.validUntil', ':validUntil')
            ->where('voucher.batch = :batch')
            ->setParameter('limit', $voucherData['limit'])
            ->setParameter('discount', $voucherData['discount'])
            ->setParameter('usage', $voucherData['usage'])
            ->setParameter('validFrom', $voucherData['validFrom'])
            ->setParameter('validUntil', $voucherData['validUntil'])
            ->setParameter('batch', $batch->getId())
            ->getQuery()
            ->execute();

        return true;
    }

    /**
     * @param array $options
     */
    private function resolveBatchOptions(array $options)
    {
        $resolver = new OptionsResolver();

        $resolver->setRequired([
            'discount',
            'usage',
            'limit',
            'validFrom',
            'validUntil',
        ]);

        $resolver->setAllowedTypes('discount', [Discount::class, 'int']);
        $resolver->setAllowedTypes('usage', 'string');
        $resolver->setAllowedTypes('limit', 'int');
        $resolver->setAllowedTypes('validFrom', 'string');
        $resolver->setAllowedTypes('validUntil', 'string');

        $resolver->resolve($options);
    }

    /**
     * @param string                   $voucherCode
     * @param OrderCollectionInterface $orderCollectionInterface
     * @param bool                     $alreadyApplied
     * @return OrderCollectionInterface
     * @throws Exception
     */
    public function applyVoucherForOrderCollectionInterface(
        $voucherCode,
        OrderCollectionInterface $orderCollectionInterface,
        $alreadyApplied = false
    ) {
        /** @var Voucher $voucher */
        $voucher = $this->entityManager->getRepository(Voucher::class)->findOneBy(['code' => $voucherCode]);

        if (null === $voucher) {
            throw new InvalidVoucherCodeException(sprintf("The voucher with code '%s' couldn't be found", $voucherCode));
        }

        if($this->validateVoucher($voucher, $alreadyApplied)) {
            $orderCollectionInterface = $this->discountService->applyDiscountForOrderCollectionInterface($voucher->getDiscount(),
                $orderCollectionInterface);

            //clear all previous vouchers from orderCollectionInterface
            foreach ($orderCollectionInterface->getVouchers() as $v) {
                $orderCollectionInterface->removeVoucher($v);
            }

            $orderCollectionInterface->addVoucher($voucher);
        }

        return $orderCollectionInterface;
    }

    /**
     * @param string $voucherCode
     * @param OrderCollection $orderCollection
     *
     * @return OrderCollectionInterface
     * @throws VoucherExpiredException
     * @throws VoucherUsedException
     * @throws Exception
     */
    public function applyVoucherForOrderCollection(
        $voucherCode,
        OrderCollection $orderCollection
    ) {
        /** @var Voucher $voucher */
        $voucher = $this->entityManager->getRepository(Voucher::class)->findOneBy(['code' => $voucherCode]);

        if(null === $voucher) {
            throw new InvalidVoucherCodeException('Voucher not valid');
        }

        if($this->validateVoucher($voucher)) {
            $orderCollection = $this->discountService->applyDiscountForOrderCollection($voucher->getDiscount(),
                $orderCollection);


            $orderCollection->addVoucher($voucher);
        }

        return $orderCollection;
    }

    /**
     * @param Voucher $voucher
     * @param bool $alreadyApplied
     *
     * @return bool
     * @throws InvalidVoucherCodeException
     * @throws VoucherExpiredException
     * @throws VoucherUsedException
     */
    private function  validateVoucher(Voucher $voucher, $alreadyApplied = false): bool {
        if(null === $voucher->getDiscount() || null === $voucher->getDiscount()->getRule()) {
            throw new Exception(sprintf("The voucher with code '%s' is invalid", $voucher->getCode()));
        }

        //only calculate discount if discount is orderLine based since its already calculated on assortment level
        if ($voucher->getDiscount()->getRule()->getStart() !== Product::class) {
            if (!$alreadyApplied) {
                return $this->checkVoucherAvailability($voucher);
            }

            return true;
        }

        return false;
    }

    /**
     * @param Voucher $voucher
     * @param bool    $throwExeptions
     * @return bool
     * @throws VoucherExpiredException
     * @throws VoucherUsedException
     */
    public function checkVoucherAvailability(Voucher $voucher, $throwExeptions = true){
        if ($voucher->getUsage() === 'once' && !$voucher->getOrderCollections()->isEmpty()) {
            if($throwExeptions) {
                throw new VoucherUsedException('This voucher is used');
            }

            return false;
        }

        try {
            $now = new DateTime();
            $validUntil = $voucher->getValidUntil();
        } catch (Exception $e) {
            if($throwExeptions) {
                throw new VoucherExpiredException('This voucher is expired');
            }
            return false;
        }

        if(null !== $validUntil) {
            $validUntil->setTime(23, 59, 59);
        }

        if ((null !== $validUntil && ($now < $voucher->getValidFrom() || $now > $validUntil))
            || ($voucher->getUsage() === 'limited' && $voucher->getOrderCollections()->count() >= $voucher->getLimit())) {
            if($throwExeptions) {
                throw new VoucherExpiredException('This voucher is expired');
            }
            return false;
        }

        return true;
    }

}
