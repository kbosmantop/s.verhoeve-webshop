<?php

namespace AppBundle\Services\Discount;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Order\OrderCollectionLineVat;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Order\OrderLineVat;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Interfaces\Sales\OrderInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Manager\Order\CartOrderLineManager;
use AppBundle\Manager\Order\OrderCollectionLineManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\SiteService;
use function array_sum;
use Doctrine\ORM\EntityManagerInterface;
use function round;
use RuleBundle\Service\ResolverService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class DiscountService
 * @package AppBundle\Services\Discount
 */
class DiscountService
{
    /**
     * @var string
     */
    protected $orderCollectionInterfaceRuleSuffix = '.collection.id';

    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * @var CartOrderLineManager
     */
    protected $cartOrderLineManager;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var ResolverService
     */
    protected $ruleResolver;

    /**
     * @var SiteService
     */
    protected $siteService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderCollectionLineManager
     */
    private $orderCollectionLineManager;

    /**
     * DiscountService constructor.
     *
     * @param OrderLineManager $orderLineManager
     * @param CartOrderLineManager $cartOrderLineManager
     * @param VatGroupService $vatGroupService
     * @param ResolverService $resolverService
     * @param SiteService $siteService
     * @param EntityManagerInterface $entityManager
     * @param OrderCollectionLineManager $orderCollectionLineManager
     */
    public function __construct(
        OrderLineManager $orderLineManager,
        CartOrderLineManager $cartOrderLineManager,
        VatGroupService $vatGroupService,
        ResolverService $resolverService,
        SiteService $siteService,
        EntityManagerInterface $entityManager,
        OrderCollectionLineManager $orderCollectionLineManager
    ) {
        $this->orderLineManager = $orderLineManager;
        $this->cartOrderLineManager = $cartOrderLineManager;
        $this->vatGroupService = $vatGroupService;
        $this->ruleResolver = $resolverService;
        $this->siteService = $siteService;
        $this->entityManager = $entityManager;
        $this->orderCollectionLineManager = $orderCollectionLineManager;
    }

    /**
     * @param Discount $discount
     * @param OrderCollectionInterface $orderCollectionInterface
     *
     * @return OrderCollectionInterface
     * @throws \Exception
     */
    public function applyLocalDiscount(Discount $discount, OrderCollectionInterface $orderCollectionInterface)
    {
        $lines = [];

        /** @var OrderInterface $order */
        foreach ($orderCollectionInterface->getOrders() as $order) {
            /** @var OrderLineInterface $line */
            foreach ($order->getLines() as $line) {
                if ($this->ruleResolver->satisfies($line, $discount->getRule())) {
                    if ($discount->getType() === 'percentage') {
                        $line->setDiscountAmount($this->calculatePercentageDiscount($this->getTotalPriceIncl($line),
                            $discount->getValue()));
                    } else {
                        $lines[] = $line;
                    }
                }
            }
        }

        if ($discount->getType() === 'amount') {
            $this->calculateFixedDiscount($discount->getValue(), $lines);
        }

        return $orderCollectionInterface;
    }

    /**
     * @param Discount $discount
     * @param OrderCollectionInterface $orderCollectionInterface
     *
     * @return OrderCollectionInterface
     * @throws \Exception
     */
    public function applyDiscountForOrderCollectionInterface(
        Discount $discount,
        OrderCollectionInterface $orderCollectionInterface
    ) {
        $lines = [];
        //reset all discount lines
        foreach ($orderCollectionInterface->getOrders() as $order) {
            /** @var OrderLineInterface $line */
            foreach ($order->getLines() as $line) {
                $line->setDiscountAmount(0);

                if ($this->ruleResolver->satisfies($line, $discount->getRule())) {
                    $lines[] = $line;
                }
            }
        }

        if ($discount->getType() === 'percentage') {
            /** @var OrderLine $line */
            foreach ($lines as $line) {
                $this->applyDiscountForOrderLineInterface($discount, $line);
            }
        } else {
            $this->calculateFixedDiscount($discount, $lines);
        }

        return $orderCollectionInterface;
    }

    /**
     * @param Discount $discount
     * @param OrderLineInterface $orderLine
     * @param bool $applyForOrderCollectionLine
     *
     * @return float|int
     * @throws \Exception
     */
    public function applyDiscountForOrderLineInterface(
        Discount $discount,
        OrderLineInterface $orderLine,
        $applyForOrderCollectionLine = false
    ) {
        if ($discount->getType() !== 'percentage') {
            throw new \LogicException('Only percentage discounts can be directly aplied to an order line.');
        }

        $discountAmount = $this->calculatePercentageDiscount($this->getTotalPriceIncl($orderLine),
            $discount->getValue());

        //only apply discount if its more favourable
        if (!$applyForOrderCollectionLine && $orderLine->getDiscountAmount() < $discountAmount) {
            $orderLine->setDiscountAmount($discountAmount);

            $metadata = $orderLine->getMetadata();
            $metadata['discount'] = $discount->getId();
            $orderLine->setMetadata($metadata);
        }

        return $discountAmount;
    }

    /**
     * @param OrderLineInterface $orderLine
     *
     * @return float
     * @throws \Exception
     */
    private function getTotalPriceIncl(OrderLineInterface $orderLine)
    {
        $totalPriceIncl = 0;

        if ($orderLine instanceof OrderLine) {
            $totalPriceIncl = $this->orderLineManager->getTotalPriceIncl($orderLine);
        } elseif ($orderLine instanceof CartOrderLine) {
            $totalPriceIncl = $this->cartOrderLineManager->getTotalPriceIncl($orderLine);
        }

        return $totalPriceIncl;
    }

    /**
     * @param $price
     * @param $percentage
     *
     * @return float|int
     */
    private function calculatePercentageDiscount($price, $percentage)
    {
        return round($price * ($percentage / 100), 2);
    }

    /**
     * @param Discount $discount
     * @param array | OrderLineInterface[] $orderLineInterfaces
     * @param bool $applyForOrderCollectionLine
     *
     * @return array
     * @throws \Exception
     */
    private function calculateFixedDiscount(
        Discount $discount,
        array $orderLineInterfaces,
        $applyForOrderCollectionLine = false
    ) {
        $totalPrice = 0;
        $vatDivision = [];

        foreach ($orderLineInterfaces as $orderLineInterface) {
            $totalPrice += $this->getTotalPriceIncl($orderLineInterface);
        }

        foreach ($orderLineInterfaces as $orderLineInterface) {
            $site = $this->siteService->determineSite();

            if (null === $site) {
                $site = $orderLineInterface->getOrder()->getCollection()->getSite();
            }
            $linePrice = $site->getDefaultDisplayPrice() === 'exclusive' ? $orderLineInterface->getTotalPrice() : $this->getTotalPriceIncl($orderLineInterface);

            $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($orderLineInterface);
            $vatRate = $vatGroup->getRateByDate($orderLineInterface->getOrder()->getDeliveryDate());
            $vatId = $vatRate->getId();

            $vatDivision[$vatId]['total'] = round(isset($vatDivision[$vatId]) ? $vatDivision[$vatId]['total'] += $linePrice : $linePrice,
                3);
            $vatDivision[$vatId]['percentage'] = round(($vatDivision[$vatId]['total'] / $totalPrice) * 100,
                2);
            $vatDivision[$vatId]['applicableDiscount'] = round($discount->getValue() / 100 * $vatDivision[$vatId]['percentage'],
                3);
        }

        $totalDiscount = [];

        foreach ($orderLineInterfaces as $orderLineInterface) {
            $site = $this->siteService->determineSite();

            if (null === $site) {
                $site = $orderLineInterface->getOrder()->getCollection()->getSite();
            }
            $linePrice = $site->getDefaultDisplayPrice() === 'exclusive' ? $orderLineInterface->getTotalPrice() : $this->getTotalPriceIncl($orderLineInterface);

            $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($orderLineInterface);
            $vatRate = $vatGroup->getRateByDate($orderLineInterface->getOrder()->getDeliveryDate());
            $vatId = $vatRate->getId();

            $lineDiscountPercentage = $linePrice / $vatDivision[$vatId]['total'] * 100;
            $discountPrice = round($vatDivision[$vatId]['applicableDiscount'] / 100 * $lineDiscountPercentage,
                2);

            $discountAmount = $discountPrice > $linePrice ? $linePrice : $discountPrice;

            if (!$applyForOrderCollectionLine) {
                $orderLineInterface->setDiscountAmount($discountAmount);

                $metadata = $orderLineInterface->getMetadata();
                $metadata['discount'] = $discount->getId();
                $orderLineInterface->setMetadata($metadata);
            }

            $totalDiscount[$vatId] = isset($totalDiscount[$vatId]) ? $totalDiscount[$vatId] += $discountAmount : $discountAmount;
        }

        if ($applyForOrderCollectionLine) {
            return $totalDiscount;
        }

        return $orderLineInterfaces;
    }

    /**
     * This discount will apply the discounted values on a new OrderCollectionLine, not on the Orderlines within the
     * Orders of the OrderCollection
     *
     * The discountAmount is also calculated excluding vat since it's stored in the price column on the OrderCollectionLine
     *
     * @param Discount $discount
     * @param OrderCollection $orderCollection
     *
     * @return OrderCollection
     * @throws \Exception
     */
    public function applyDiscountForOrderCollection(Discount $discount, OrderCollection $orderCollection)
    {
        $lines = [];
        //reset all discount lines
        foreach ($orderCollection->getOrders() as $order) {
            if ($order->getStatus() === 'cancelled') {
                continue;
            }

            /** @var OrderLineInterface $line */
            foreach ($order->getLines() as $line) {
                if ($this->ruleResolver->satisfies($line, $discount->getRule())) {
                    $lines[] = $line;
                }
            }
        }

        if (!empty($lines)) {
            if ($discount->getType() === 'percentage') {
                $discounts = [];
                $orders = [];

                /** @var OrderLine $line */
                foreach ($lines as $line) {
                    /** @var OrderLineVat $vatLine */
                    $vatLine = $line->getVatLines()->current();
                    $rateId = $vatLine->getVatRate()->getId();

                    $discountAmount = $this->applyDiscountForOrderLineInterface($discount, $line, true);
                    $discountAmount /= (1 + $vatLine->getVatRate()->getFactor());

                    $orderId = $line->getOrder()->getId();
                    $discounts[$orderId][$rateId] = isset($discounts[$orderId][$rateId]) ? $discounts[$orderId][$rateId] += $discountAmount : $discountAmount;
                    $orders[$orderId] = $line->getOrder();
                }

                foreach ($discounts as $orderId => $discountAmountInfo) {
                    $orderCollectionLine = new OrderCollectionLine();
                    $orderCollectionLine->setTitle($discount->getDescription());
                    $orderCollectionLine->setQuantity(1);
                    $orderCollectionLine->setPrice(-round(array_sum($discountAmountInfo), 3));
                    $orderCollectionLine->setOrderCollection($orderCollection);
                    $orderCollectionLine->setRelatedOrder($orders[$orderId]);

                    foreach ($discountAmountInfo as $vatRateId => $discountAmount) {
                        $vatRate = $this->entityManager->getRepository(VatRate::class)->find($vatRateId);
                        $this->orderCollectionLineManager->createVatLines($orderCollectionLine, $vatRate, -round($discountAmount, 3));
                    }

                    $orderCollection->addLine($orderCollectionLine);
                }
            } else {
                $orderCollectionLine = new OrderCollectionLine();
                $orderCollectionLine->setTitle($discount->getDescription());
                $orderCollectionLine->setQuantity(1);

                $discounts = $this->calculateFixedDiscount($discount, $lines, true);

                $totalDiscount = 0;
                foreach ($discounts as $vatRateId => $discountAmount) {
                    $vatRate = $this->entityManager->getRepository(VatRate::class)->find($vatRateId);
                    $discountAmount /= (1 + $vatRate->getFactor());

                    $totalDiscount += $discountAmount;
                    $discounts[$vatRateId] = $discountAmount;
                }

                $orderCollectionLine->setPrice(-round($totalDiscount, 3));

                foreach ($discounts as $vatRateId => $discountAmount) {
                    $vatRate = $this->entityManager->getRepository(VatRate::class)->find($vatRateId);
                    $this->orderCollectionLineManager->createVatLines($orderCollectionLine, $vatRate, -round($discountAmount, 3));
                }

                $metadata = $orderCollectionLine->getMetadata();
                $metadata['discount_lines'] = array_map(function (OrderLine $orderline) {
                    return $orderline->getId();
                }, $lines);
                $orderCollectionLine->setMetadata($metadata);

                $orderCollection->addLine($orderCollectionLine);
            }
        }

        return $orderCollection;
    }
}