<?php

namespace AppBundle\Services;

use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use NumberFormatter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PriceService
 * @package AppBundle\Services
 */
class PriceService
{

    /**
     * @var SiteService
     */
    protected $siteService;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var NumberFormatter
     */
    protected $numberFormatter;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * PriceService constructor.
     * @param SiteService  $siteService
     * @param RequestStack $requestStack
     * @param TokenStorage $tokenStorage
     */
    public function __construct(SiteService $siteService, RequestStack $requestStack, TokenStorage $tokenStorage)
    {
        $this->siteService = $siteService;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->numberFormatter = null;
    }

    /**
     * Gets site specific view value including optional vat with locale conversion
     *
     * @param float        $price
     * @param VatRate|null $vatRate
     * @return string
     */
    public function getDisplayPrice(float $price, VatRate $vatRate = null): string
    {
        return $this->calcPriceFormatted($price, $this->determineViewableVat($vatRate));
    }

    /**
     * Gets site specific view value including optional vat
     *
     * @param float        $price
     * @param VatRate|null $vatRate
     * @param bool|null    $showVat
     * @return float
     * @throws \Exception
     */
    public function getRawDisplayPrice(float $price, VatRate $vatRate = null, ?bool $showVat = null): float
    {
        return round($this->calcPrice($price, $this->determineViewableVat($vatRate, $showVat)), 2);
    }

    /**
     * @param VatRate|null $vatRate
     * @param bool|null    $showVat
     * @return VatRate|null
     */
    private function determineViewableVat(VatRate $vatRate = null, ?bool $showVat = null)
    {
        if ($showVat) {
            return $vatRate;
        }

        if (false === $showVat) {
            return null;
        }

        /** depends on site and user settings */
        $site = $this->siteService->determineSite();

        $siteDefaultDisplayPrice = $site ? $site->getDefaultDisplayPrice() : 'inclusive';
        /** @var Company $company */
        if (($token = $this->tokenStorage->getToken()) !== null
            && ($token instanceof AnonymousToken) === false
            && ($user = $token->getUser()) !== null
            && ($user instanceof Customer) === true
            && ($company = $user->getCompany()) !== null
            && ($override = $company->getOverrideDefaultPriceView()) !== null
        ) {
            $siteDefaultDisplayPrice = $override;
        }
        return ($siteDefaultDisplayPrice === 'exclusive') ? null : $vatRate;
    }

    /**
     * Calculates the price with optional vat
     *
     * @param float        $price
     * @param VatRate|null $vatRate
     * @return float|int
     */
    public function calcPrice(float $price, VatRate $vatRate = null)
    {
        if (null === $vatRate) {
            return $price;
        }

        return $price * (1 + $vatRate->getFactor());
    }

    /**
     * @param float   $price
     * @param VatRate $vatRate
     * @return float
     */
    public function calcVatAmount(float $price, VatRate $vatRate): float
    {
        return $price * $vatRate->getFactor();
    }

    /**
     * @internal
     *
     * @param float   $price
     * @param VatRate $vat
     * @return string
     */
    public function calcPriceFormatted(float $price, VatRate $vat = null)
    {
        $calculatedPrice = $this->calcPrice($price, $vat);
        return $this->getNumberFormatter()->format($calculatedPrice);
    }

    /**
     * @return NumberFormatter
     */
    private function getNumberFormatter(): NumberFormatter
    {
        if (null === $this->numberFormatter) {
            $locale = $this->requestStack->getCurrentRequest()->getLocale();
            $this->numberFormatter = new NumberFormatter($locale, NumberFormatter::PATTERN_DECIMAL);
            $this->numberFormatter->setAttribute(NumberFormatter::MIN_INTEGER_DIGITS, 1);
            $this->numberFormatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
        }

        return $this->numberFormatter;
    }
}
