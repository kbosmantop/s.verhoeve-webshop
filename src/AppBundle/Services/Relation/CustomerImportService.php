<?php

namespace AppBundle\Services\Relation;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Exceptions\Common\SpreadsheetInvalidExtensionException;
use AppBundle\Exceptions\Common\SpreadsheetReaderException;
use AppBundle\Exceptions\Customer\BatchCreate\CompanyNotAllowedException;
use AppBundle\Exceptions\Common\BatchCreateException;
use AppBundle\Exceptions\Customer\EmailAddressExistsException;
use AppBundle\Exceptions\Customer\UsernameUsedException;
use AppBundle\Manager\CustomerManager;
use AppBundle\Services\Common\SpreadsheetReaderService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use FOS\UserBundle\Doctrine\UserManager;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CustomerImportService
 * @package AppBundle\Services\Relation
 */
class CustomerImportService
{
    /** @var UserManager */
    private $fosUserManager;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SpreadsheetReaderService */
    private $spreadsheetReaderService;

    /** @var CustomerManager */
    private $customerManager;

    /** @var array */
    private $emailsChecked;

    /** @var array */
    private $usernamesChecked;

    private const COLUMN_COMPANY_ID_LABEL = 'Bedrijf_ID';
    private const COLUMN_FIRSTNAME_LABEL = 'Voornaam';
    private const COLUMN_LASTNAME_PREFIX_LABEL = 'Tussenvoegsel';
    private const COLUMN_LASTNAME_LABEL = 'Achternaam';
    private const COLUMN_PHONE_LABEL = 'Telefoon';
    private const COLUMN_MOBILE_LABEL = 'Mobiel';
    private const COLUMN_GENDER_LABEL = 'Geslacht';
    private const COLUMN_USERNAME_LABEL = 'Gebruikersnaam (email)';
    private const COLUMN_ENABLED_LABEL = 'Account_Actief';
    private const COLUMN_PLAIN_PASSWORD_LABEL = 'Wachtwoord';
    private const COLUMN_TG_PASSWORD_LABEL = 'Wachtwoord_TG';

    /**
     * CustomerImportService constructor.
     *
     * @param UserManager              $fosUserManager
     * @param EntityManagerInterface            $entityManager
     * @param SpreadsheetReaderService $spreadsheetReaderService
     * @param CustomerManager   $customerManager
     */
    public function __construct(
        UserManager $fosUserManager,
        EntityManagerInterface $entityManager,
        SpreadsheetReaderService $spreadsheetReaderService,
        CustomerManager $customerManager
    ) {
        $this->fosUserManager = $fosUserManager;
        $this->entityManager = $entityManager;
        $this->spreadsheetReaderService = $spreadsheetReaderService;
        $this->customerManager = $customerManager;
    }

    /**
     * @return array
     */
    public function getUserUploadColumns(): array
    {
        return [
            self::COLUMN_FIRSTNAME_LABEL,
            self::COLUMN_LASTNAME_PREFIX_LABEL,
            self::COLUMN_LASTNAME_LABEL,
            self::COLUMN_GENDER_LABEL,
            self::COLUMN_PHONE_LABEL,
            self::COLUMN_MOBILE_LABEL,
            self::COLUMN_USERNAME_LABEL,
            self::COLUMN_PLAIN_PASSWORD_LABEL,
            self::COLUMN_ENABLED_LABEL,
        ];
    }

    /**
     * @param string $column
     * @return string
     */
    public function getUserUploadHeaderMapping(string $column): ?string
    {
        switch ($column) {
            case self::COLUMN_COMPANY_ID_LABEL:
                return 'company';
            case self::COLUMN_FIRSTNAME_LABEL:
                return 'firstname';
            case self::COLUMN_LASTNAME_PREFIX_LABEL:
                return 'lastnamePrefix';
            case self::COLUMN_LASTNAME_LABEL:
                return 'lastname';
            case self::COLUMN_PHONE_LABEL:
                return 'phoneNumber';
            case self::COLUMN_MOBILE_LABEL:
                return 'mobilenumber';
            case self::COLUMN_ENABLED_LABEL:
                return 'enabled';
            case self::COLUMN_USERNAME_LABEL:
                return 'username';
            case self::COLUMN_PLAIN_PASSWORD_LABEL:
                return 'plainPassword';
            case self::COLUMN_TG_PASSWORD_LABEL:
                return 'password';
            case self::COLUMN_GENDER_LABEL:
                return 'gender';
            default:
                return false;
        }
    }

    /**
     * @param array    $row
     * @param int|null $rowNumber
     * @param Company  $requestCompany
     * @return array
     */
    public function getUserUploadResolver(array $row = [], int $rowNumber = null, Company $requestCompany): array
    {
        $fosUserManager = $this->fosUserManager;

        $resolver = new OptionsResolver();

        $companies = [];

        /** @var EntityRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(Company::class);

        $resolver
            ->setDefined([
                'company',
                'firstname',
                'lastnamePrefix',
                'lastname',
                'phoneNumber',
                'mobilenumber',
                'username',
                'enabled',
                'plainPassword',
                'password',
                'gender',
            ])
            ->setDefaults([
                'enabled' => 0,
                'lastnamePrefix' => null,
                'phoneNumber' => null,
                'mobilenumber' => null,
                'plainPassword' => null,
                'password' => null,
                'gender' => null,
            ])
            ->setRequired([
                'firstname',
                'lastname',
                'username',
            ])
            ->setAllowedTypes('company', ['int', 'double', 'string'])
            ->setAllowedTypes('firstname', ['string'])
            ->setAllowedTypes('lastnamePrefix', ['null', 'string'])
            ->setAllowedTypes('lastname', ['string'])
            ->setAllowedTypes('phoneNumber', ['null', 'string', 'double'])
            ->setAllowedTypes('mobilenumber', ['null', 'string', 'double'])
            ->setAllowedTypes('username', ['string'])
            ->setAllowedTypes('enabled', ['null', 'double', 'int', 'bool'])
            ->setAllowedTypes('plainPassword', ['null', 'string', 'double'])
            ->setAllowedTypes('password', ['null', 'string'])
            ->setAllowedTypes('gender', ['null', 'string'])
            ->setNormalizer('gender', function (Options $options, $value) {
                void($options);

                if (!empty($value)) {
                    switch (\strtolower($value)) {
                        case 'male':
                        case 'man':
                        case 'm':
                            return 'male';
                        case 'female':
                        case 'f':
                        case 'vrouw':
                        case 'v':
                            return 'female';
                    }
                }

                return null;
            })
            ->setNormalizer('phoneNumber', function (Options $options, $phoneNumber) {
                void($options);

                return (string)$phoneNumber;
            })
            ->setNormalizer('mobilenumber', function (Options $options, $mobilenumber) {
                void($options);

                return (string)$mobilenumber;
            })
            ->setNormalizer('username', function (Options $options, $username) use ($fosUserManager, $rowNumber) {
                void($options);

                if (empty($username)) {
                    throw new \RuntimeException('Username mag niet leeg zijn. (regel: ' . $rowNumber . ')');
                }

                if (isset($this->usernamesChecked[$username]) || $fosUserManager->findUserBy(['username' => $username])) {
                    throw new UsernameUsedException('Gebruikersnaam (' . $username . ') bestaat al (regel: ' . $rowNumber . ')');
                }

                if (isset($this->entityManagerailsChecked[$username]) || $fosUserManager->findUserBy(['email' => $username])) {
                    throw new EmailAddressExistsException('Email (' . $username . ') bestaat al (regel: ' . $rowNumber . ')');
                }

                $this->entityManagerailsChecked[$username] = 1;

                return $username;
            })
            ->setNormalizer('company', function (Options $options, $value) use ($companyRepository, $companies, $requestCompany) {
                void($options);

                if ($value instanceof Company === false) {
                    $value = (int)$value;

                    if (isset($companies[$value])) {
                        return $companies[$value];
                    }

                    /** @var Company $company */
                    $company = $companyRepository->findOneBy(['id' => $value]);

                    /** @var Company[] $requestChildCompanies */
                    $requestChildCompanies = $companyRepository->findBy(['parent' => $requestCompany->getId()]);

                    if (!$company) {
                        return ($companies[$value] = false);
                    }

                    $validCompany = false;
                    if($requestCompany->getId() === $company->getId()) {
                        $validCompany = true;
                    }

                    foreach($requestChildCompanies as $requestChildCompany) {
                        if($requestChildCompany->getId() === $company->getId()) {
                            $validCompany = true;
                            break;
                        }
                    }

                    if (false === $validCompany){
                        throw new CompanyNotAllowedException(sprintf('Importeren van klanten kan alleen voor het bedrijf %s of een onderliggend bedrijf)', $requestCompany->getName()));
                    }

                    return ($companies[$value] = $company);
                }

                return $value;
            })
            ->setNormalizer('enabled', function (Options $options, $value) {
                void($options);

                return (int)$value;
            });

        $row = $resolver->resolve($row);

        // Assign username to email column.
        $row['email'] = $row['username'];

        if(empty($row['company'])) {
            $row['company'] = $requestCompany->getId();
        }

        return $row;
    }

    /**
     * @param UploadedFile $file
     * @param Company      $company
     * @return ArrayCollection
     * @throws BatchCreateException
     * @throws ConnectionException
     * @throws Exception
     * @throws OptimisticLockException
     * @throws SpreadsheetInvalidExtensionException
     * @throws SpreadsheetReaderException
     * @throws \AppBundle\Exceptions\Customer\BatchCreate\InvalidMimeTypeException
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function processFile(UploadedFile $file, Company $company): ArrayCollection
    {
        $this->spreadsheetReaderService
            ->readFile($file)
            ->setResolver([$this, 'getUserUploadResolver'])
        ;

        $this->spreadsheetReaderService->getHeaders([$this, 'getUserUploadHeaderMapping']);

        return $this->batchCreate($this->spreadsheetReaderService->getData($company));
    }

    /**
     * @param array $data
     * @return ArrayCollection|Customer[]
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws BatchCreateException
     */
    public function batchCreate(array $data = []): ArrayCollection
    {
        $collection = new ArrayCollection();
        $errors = [];

        $connection = $this->entityManager->getConnection();
        $connection->beginTransaction();

        $this->customerManager->disableAutoFlush();

        $lastRow = 0;

        try {
            foreach ($data as $row => $userData) {
                ++$lastRow;

                // Create base customer
                $customer = $this->fosUserManager->createUser();

                // Check if given properties are valid ones
                $properties = $this->resolveCustomerProperties($userData);

                // Set all properties
                $this->setCustomerProperties($customer, $properties);

                /** @var Customer $customer */
                $customer = $this->customerManager->create($customer, null, ['uploadUsers']);

                $collection->add($customer);
            }

            $connection->commit();
        } catch (\Exception $e) {
            $errors[] = $lastRow;

            // Reset database changes.
            if ($connection->getTransactionNestingLevel() > 0) {
                $connection->rollBack();
            }
        }

        if(!empty($errors)) {
            throw new BatchCreateException(sprintf('Fout tijdens batch import op regel(s): %s', implode(',', $errors)));
        }

        $this->entityManager->flush();

        return $collection;
    }

    /**
     * @param $customer
     * @param $properties
     */
    private function setCustomerProperties($customer, $properties): void
    {
        $customerPropertyAccessor = PropertyAccess::createPropertyAccessor();

        foreach ($properties as $property => $value) {
            $customerPropertyAccessor->setValue($customer, $property, $value);
        }
    }

    /**
     * @param array $properties
     *
     * @return array
     */
    private function resolveCustomerProperties(array $properties): array
    {
        $resolver = new OptionsResolver();

        $resolver
            ->setDefined([
                'company',
                'lastnamePrefix',
                'birthday',
                'phoneNumber',
                'mobilenumber',
                'customerGroup',
                'preferredPaymentMethod',
                'preferredIdealIssuer',
                'registerOnSite',
                'locale',
                'internalRemark',
                'metadata',
                'username',
                'email',
                'password',
                'plainPassword',
                'registeredOnSite',
                'gender',
            ])
            ->setDefaults([
                'enabled' => false,
                'locked' => false,
                'expired' => false,
                'credentialsExpired' => false,
                'lastnamePrefix' => false,
                'roles' => [],
            ])
            ->setRequired([
                'username',
                'enabled',
                'firstname',
                'lastname',
            ])
            ->setAllowedTypes('company', [Company::class, 'int', 'double'])
            ->setAllowedTypes('firstname', ['string'])
            ->setAllowedTypes('lastnamePrefix', ['null', 'string'])
            ->setAllowedTypes('lastname', ['string'])
            ->setAllowedTypes('phoneNumber', ['null', 'string'])
            ->setAllowedTypes('email', ['string'])
            ->setAllowedTypes('enabled', ['bool', 'int', 'double'])
            ->setAllowedTypes('username', ['null', 'string'])
            ->setAllowedTypes('plainPassword', ['null', 'string'])
            ->setAllowedTypes('password', ['null', 'string'])
            ->setAllowedTypes('gender', ['null', 'string']);

        $companies = [];
        /** @var EntityRepository $companyRepository */
        $companyRepository = $this->entityManager->getRepository(Company::class);

        $self = $this;
        $resolver
            ->setNormalizer('email', function (Options $options, $value) use ($self) {
                void($options);

                if (empty($value)) {
                    throw new \RuntimeException('Email mag niet leeg zijn');
                }

                if ($self->findByEmail($value)) {
                    throw new \RuntimeException('Email bestaat al');
                }

                return $value;
            })
            ->setNormalizer('company', function (Options $options, $value) use ($companyRepository, $companies) {
                void($options);

                if (!$value instanceof Company) {
                    if (isset($companies[$value])) {
                        return $companies[$value];
                    }

                    $company = $companyRepository->findOneBy(['id' => (int)$value]);
                    if (!$company) {
                        return ($companies[$value] = false);
                    }

                    return ($companies[$value] = $company);
                }

                return $value;
            });

        return $resolver->resolve($properties);
    }

    /**
     * @param string $email
     *
     * @return UserInterface
     */
    public function findByEmail(string $email): ?UserInterface
    {
        return $this->fosUserManager->findUserBy(['email' => $email]);
    }
}
