<?php

namespace AppBundle\Services;

use AppBundle\Utils\Browsershot\Browsershot;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PdfService
 * @package AppBundle\Services
 */
class PdfService
{
    /**
     * @param Response $response
     * @param array    $headers
     * @return BinaryFileResponse|Response
     * @throws \Exception
     */
    public function renderFromResponse(Response $response, array $headers): BinaryFileResponse
    {
        $path = tempnam(sys_get_temp_dir(), 'pdf');

        $this->saveFromResponse($response, $path);

        $headers = array_merge($response->headers->all(), $headers);
        $headers['Content-Type'] = 'application/pdf';

        $response = new BinaryFileResponse($path, 200, $headers);
        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @param Response $response
     * @param string   $path
     * @throws \Exception
     */
    public function saveFromResponse(Response $response, string $path): void
    {
        Browsershot::html($response->getContent())
            ->paperSize(210, 297)
            ->savePdf($path);

        if (!file_exists($path)) {
            throw new \RuntimeException('PDF rendering failed (reason unknown)');
        }
    }
}
