<?php

namespace AppBundle\Services;

use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class UuidGeneratorService
 * @package AppBundle\Services
 */
class UuidGeneratorService
{

    use ContainerAwareTrait;

    /**
     * @param $entityClass
     * @return bool
     */
    public function migrate($entityClass)
    {
        if (method_exists($entityClass, 'getUuid') && method_exists($entityClass, 'setUuid')) {

            $em = $this->container->get('doctrine')->getManager();

            $entities = $em->getRepository($entityClass)->findBy(['uuid' => null]);
            foreach ($entities as $entity) {
                $entity->setUuid(Uuid::uuid4());
            }

            $em->flush();

            return true;
        }

        return false;
    }

}