<?php

namespace AppBundle\Services\Convert;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Exceptions\CouldNotCreateException;
use AppBundle\Repository\CompanyRepository;
use AppBundle\Services\Company\CompanyRegistrationService;
use AppBundle\Services\Webservices\WebservicesNL;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyService
 * @package AppBundle\Services\Convert
 */
class CompanyService
{

    /**
     * @var WebservicesNL
     */
    private $webservicesNL;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var AddressService
     */
    private $addressService;

    /**
     * @var Company $company
     */
    private $company;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var CompanyRegistrationService
     */
    private $companyRegistrationService;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * CompanyService constructor.
     * @param EntityManagerInterface     $entityManager
     * @param CompanyRegistrationService $companyRegistrationService
     * @param WebservicesNL              $webservicesNL
     * @param CustomerService            $customerService
     * @param AddressService             $addressService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CompanyRegistrationService $companyRegistrationService,
        WebservicesNL $webservicesNL,
        CustomerService $customerService,
        AddressService $addressService
    ) {
        $this->entityManager = $entityManager;
        $this->companyRegistrationService = $companyRegistrationService;
        $this->webservicesNL = $webservicesNL;
        $this->customerService = $customerService;
        $this->addressService = $addressService;
        $this->companyRepository = $this->entityManager->getRepository(Company::class);
    }

    /**
     * @param null|\stdClass $rawData
     * @return object
     * @throws \Exception
     */
    public function validateRawData(\stdClass $rawData = null)
    {

        if (isset($rawData->metadata)) {
            $rawData->metadata = (array)$rawData->metadata;
        }

        /* Validate data */
        $optionResolver = new OptionsResolver();

        $optionResolver->setDefined([
            'id',
            'parentId',
            'metadata',
            'name',
            'twinfieldRelationNumber',
            'phoneNumber',
            'chamberOfCommerceNumber',
            'customerGroups',
            'email',
            'internalRemark',
            'vatNumber',
            'customers',
            'verified',
            'addresses',
            'defaultDeliveryAddressId',
            'isCustomer',
            'isSupplier',
            'supplierConnector',
            'handleChildOrders',
            'billOnParent',
        ]);

        // Set defaults for optional paramaters
        $optionResolver->setDefaults([
            'verified' => true,
        ]);

        $optionResolver->addAllowedTypes('id', ['int', 'null']);
        $optionResolver->addAllowedTypes('parentId', ['int', 'null']);
        $optionResolver->addAllowedTypes('defaultDeliveryAddressId', ['int', 'null']);
        $optionResolver->addAllowedTypes('verified', ['boolean', 'null']);
        $optionResolver->addAllowedTypes('metadata', ['array', 'null']);
        $optionResolver->addAllowedTypes('customers', ['array', 'null']);
        $optionResolver->addAllowedTypes('addresses', ['array', 'null']);
        $optionResolver->addAllowedTypes('customerGroups', ['array', 'null']);

        $optionResolver->setRequired([
            "chamberOfCommerceNumber",
        ]);

        try {
            $rawData = (object)$optionResolver->resolve((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Company data fields missing.');
        }

        if (!$this->webservicesNL
            ->isValidChamberOfCommerceNumber($rawData->chamberOfCommerceNumber)) {
            throw new \Exception('Invalid chamber of commerce number');
        }

        return $rawData;

    }

    /**
     * @param Company $company
     * @param bool    $withRelations
     * @return \stdClass
     */
    public function getJsonData(Company $company, bool $withRelations = false)
    {

        if ($withRelations) {
            $customers = $company->getCustomers()->map(function (Customer $customer) {
                return $this->customerService->getJsonData($customer);
            })->toArray();

            $addresses = $company->getAddresses()->map(function (Address $address) {
                return $this->addressService->getJsonData($address);
            })->toArray();

            $childeren = $company->getChildren()->map(function (Company $company) {
                return $company->getId();
            })->toArray();
        }

        $data = new \stdClass();
        $data->id = $company->getId();
        $data->metadata = ($company->getMetadata() ? $company->getMetadata() : null);
        $data->parent = ($company->getParent() ? $company->getParent()->getId() : null);
        $data->twinfieldRelationNumber = $company->getTwinfieldRelationNumber();
        $data->name = $company->getName();
        $data->phoneNumber = $company->getPhoneNumber();
        $data->email = $company->getEmail();
        $data->isCustomer = $company->getIsCustomer();
        $data->isSupplier = $company->getIsSupplier();
        $data->internalRemark = $company->getInternalRemark();
        $data->handleChildOrders = $company->getHandleChildOrders();
        $data->chamberOfCommerceNumber = $company->getChamberOfCommerceNumber();
        $data->chamberOfCommerceEstablishmentNumber = $company->getChamberOfCommerceEstablishmentNumber();
        $data->vatNumber = $company->getVatNumber();
        $data->customers = (isset($customers) ? $customers : null);
        $data->addresses = (isset($addresses) ? $addresses : null);;
        $data->childeren = (isset($childeren) ? $childeren : null);;

        return $data;

    }

    /**
     * @param array $attributes
     * @return Company
     * @throws CouldNotCreateException
     */
    public function createOrUpdate(array $attributes)
    {
        $persist = false;

        if (!isset($attributes['id']) || !$this->company = $this->companyRepository->find($attributes['id'])) {
            $this->company = new Company();
            $persist = true;
        }

        if (!empty($attributes['customerGroups']) && is_array($attributes['customerGroups'])) {
            foreach ($attributes['customerGroups'] as $customerGroup) {
                $customerGroup = $this->entityManager->getRepository(CustomerGroup::class)->findOneBy([
                    'description' => $customerGroup,
                ]);

                if (!is_null($customerGroup) && !$this->company->getCustomerGroups()->contains($customerGroup)) {
                    $this->company->addCustomerGroup($customerGroup);
                }
            }
        }

        $this->setAttributes($attributes);

        // Add company registration if not set for the company.
        if (!$this->company->getCompanyRegistration()) {
            if (!empty($attributes['chamberOfCommerceNumber'])) {
                $companyRegistration = $this->companyRegistrationService
                    ->createByRegistrationNumber($attributes['chamberOfCommerceNumber']);
            } else {
                if ($attributes['vatNumber']) {
                    $companyRegistration = $this->companyRegistrationService
                        ->createByVatNumber($attributes['vatNumber']);
                }
            }

            if (!$companyRegistration) {
                throw new CouldNotCreateException("Could not create a `companyRegistration`");
            }

            $this->company->setCompanyRegistration($companyRegistration);
        }

        if ($persist) {
            $this->entityManager->persist($this->company);
        }

        return $this->company;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    private function setAttributes(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $method = 'set' . ucwords($key);
            if (method_exists($this->company, $method) && !is_null($value)) {
                $this->company = call_user_func_array([$this->company, $method], [$value]);
            }
        }

        return $this;
    }

}
