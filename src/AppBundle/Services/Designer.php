<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Designer\DesignerCollage;
use AppBundle\Entity\Designer\DesignerCollageObject;
use AppBundle\Entity\Designer\DesignerFont;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Entity\Designer\Tag;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Exceptions\Designer\ProcessDesignException;
use AppBundle\Services\Filesystem\RackspaceTempUrlPlugin;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Utils\Browsershot\Browsershot;
use AppBundle\Utils\Designer\SVG;
use AppBundle\Utils\Network;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\MountManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class Designer
 * @package AppBundle\Services
 */
class Designer
{
    use ContainerAwareTrait;

    public const ALLOWED_IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'gif'];

    public const PROCESS_FILE_FIELD_NAME = 'file';

    /** @var string */
    public const SOURCE_LOCAL = 'local';

    /** @var string */
    public const SOURCE_OBJECT_STORE = 'object_store';

    public const FILE_PRINT = 'print.png';
    public const FILE_MANIPULATED_PRINT = 'manipulated_print.png';
    public const FILE_PREVIEW = 'thumb.jpg';
    public const FILE_SVG = 'design.svg';
    public const FILE_JSON = 'design.json';

    /** @var bool */
    private $debugMode = false;

    /** @var bool */
    private $screenshotModus = false;

    /** @var bool */
    private $convertOnProcess = false;

    /** @var int */
    private $screenshotDelay = 250;

    /** @var string */
    private $uuid;

    /** @var string */
    private $templateUuid;

    /** DesignerTemplate */
    private $template;

    /** @var Product */
    private $product;

    /** @var Personalization */
    private $personalization;

    /** @var string */
    private $assetsPath;

    /** @var string */
    private $uploadPath;

    /** @var string */
    private $returnUrl;

    /** @var string */
    private $cancelUrl;

    /** @var string */
    private $apiUrl;

    /** @var RequestStack */
    private $requestStack;

    /** @var $parameterBag */
    private $parameterBag;

    /** @var Filesystem */
    private $fs;

    /** @var MountManager */
    private $mountManager;

    /** @var ParameterService */
    private $parameterService;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var RouterInterface */
    private $router;

    /** @var bool */
    private $isPreDesign = false;

    /** @var string */
    private $callback;

    /** @var array */
    private $settings = [];

    /** @var string */
    private $isUrlDesign;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var string */
    private $convertUrl;

    /** Designer constructor.
     *
     * @param RequestStack $requestStack
     * @param ParameterBagInterface $parameterBag
     * @param EntityManagerInterface $entityManager
     * @param MountManager $mountManager
     * @param ParameterService $parameterService
     * @param RouterInterface $router
     * @param TokenStorageInterface $tokenStorage
     *
     * @throws Exception
     */
    public function __construct(
        RequestStack $requestStack,
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $entityManager,
        MountManager $mountManager,
        ParameterService $parameterService,
        RouterInterface $router,
        TokenStorageInterface $tokenStorage
    ) {
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
        $this->mountManager = $mountManager;
        $this->parameterService = $parameterService;
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;

        $this->uuid = Uuid::uuid4();
        $this->fs = new Filesystem();
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $callback
     *
     * @return $this
     */
    public function setCallback($callback): self
    {
        $this->callback = $callback;

        return $this;
    }

    /**
     * @throws Exception
     *
     * @return \stdClass
     */
    public function getConfig(): \stdClass
    {
        $request = $this->requestStack->getMasterRequest();

        $session = null;

        if($request !== null) {
            $session = $request->getSession();

            if (!$this->uploadPath) {

                // Set fallback for file upload
                $this->uploadPath = $request->getRequestUri();
            }
        }

        $this->settings = $this->getDesignSettings();

        $config = new \stdClass();
        $config->debug = $this->inDebugMode();
        $config->origin = $this->parameterBag->get('openstack_object_store');
        $config->uuid = $this->getUuid();

        $config->uploadPath = $this->uploadPath;

        $config->returnUrl = $this->returnUrl;

        $config->cancelUrl = $this->getCancelUrl();

        $config->callback = $this->callback;

        $config->apiUrl = $this->getApiUrl();

        if($this->getProduct() !== null) {
            $config->title = $this->getProduct()->translate()->getTitle();
        }

        $config->translations = $this->router->generate('app_translations_jsonoutput', [
            'domain' => 'designer',
            'locale' => $session ? $session->get('_locale', 'nl_NL') : 'nl_NL',
        ], RouterInterface::ABSOLUTE_URL);

        $config->screenshotModus = $this->getScreenshotModus();
        $config->screenshotDelay = $this->getScreenshotDelay();

        $config->dimensions = new \stdClass();
        $config->dimensions->type = 'rectangle';

        $widthPercentage = 1;
        $heightPercentage = 1;

        if ($this->hasTemplateUuid() && $designerTemplate = $this->getTemplate()) {
            $widthPercentage = $designerTemplate->getCanvas()->getWidth();
            $heightPercentage = $designerTemplate->getCanvas()->getHeight();

            $config->templateUuid = $this->getTemplateUuid();
        }

        // set default scaling size (cm) for predesigns.
        $config->dimensions->width = 30 * $widthPercentage;
        $config->dimensions->height = 30 * $heightPercentage;

        $personalization = $this->getPersonalization();

        if (null !== $personalization) {
            $config->dimensions->width = $personalization->findProductProperty('design.width')->getValue();
            $config->dimensions->height = $personalization->findProductProperty('design.height')->getValue();
        }

        $config = $this->assignDesignSettings($config);

        if (empty($config->fonts)) {
            $fonts = $this->entityManager->getRepository(DesignerFont::class)->findAll();
            $config->fonts = $this->createFontList($fonts);
        }

        if (empty($config->layouts)) {
            $collages = $this->entityManager->getRepository(DesignerCollage::class)->findAll();
            $config->layouts = $this->createCollageList($collages);
        }

        $config->json = $this->getDesignJson($config->dimensions);

        return $config;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getDesignSettings(): array
    {
        if (!$this->hasTemplateUuid()) {
            return [];
        }

        $designerTemplate = $this->getTemplate();

        if (null === $designerTemplate) {
            return [];
        }

        return $this->parameterService
            ->setEntity($designerTemplate)
            ->getEntityValues();
    }

    /**
     * @return bool
     */
    public function hasTemplateUuid(): bool
    {
        return (bool)$this->getTemplateUuid();
    }

    /**
     * @return string
     */
    public function getTemplateUuid(): ?string
    {
        return $this->templateUuid;
    }

    /**
     * @param $templateUuid
     *
     * @return $this
     */
    public function setTemplateUuid(string $templateUuid): self
    {
        $this->templateUuid = $templateUuid;

        // Clear template data when templateUuid changed.
        $this->template = null;

        return $this;
    }

    /**
     * @return DesignerTemplate|null
     */
    public function getTemplate(): ?DesignerTemplate
    {
        if (null !== $this->template) {
            return $this->template;
        }

        if (!$this->hasTemplateUuid()) {
            return null;
        }

        $this->template = $this->entityManager->getRepository(DesignerTemplate::class)->findOneBy([
            'uuid' => $this->getTemplateUuid(),
        ]);

        return $this->template;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param $uuid
     *
     * @return $this
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->apiUrl;
    }

    /**
     * @param $apiUrl
     *
     * @return $this
     */
    public function setApiUrl(string $apiUrl): self
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * @todo: move to manager
     * @param \stdClass|null $dimensions
     *
     * @return null|\stdClass
     * @throws FileNotFoundException
     */
    public function getDesignJson(\stdClass $dimensions = null): ?\stdClass
    {
        $json = $this->getJson();

        // Get temporary predesign
        if (null === $json && false === $this->hasTemplateUuid()) {
            try {
                $json = $this->getTemporaryPredesignJson($dimensions);
            } catch (Exception $e) {
                $json = null;
            }
        }

        // Get JSON of predesign if there is no current design.
        if (null === $json && $this->hasTemplateUuid()) {
            $designerTemplateService = $this->container->get('app.designer');
            $designerTemplateService->setIsPreDesign(true);
            $designerTemplateService->setUuid($this->getTemplateUuid());
            $designerTemplateService->setUuid($this->getTemplateUuid());

            $json = $designerTemplateService->getJson();
        }

        return $json;
    }

    /**
     * @param \stdClass|null $dimensions
     *
     * @return null|\stdClass
     */
    private function getTemporaryPredesignJson(\stdClass $dimensions = null): ?\stdClass
    {
        $personalization = $this->getPersonalization();

        $image = null;

        if (null !== $personalization) {
            $image = $personalization->getMainImage();
        }

        if (null === $image || null === $dimensions || null === $image->getPath()) {
            return null;
        }

        /** @var CacheManager */
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');

        /** @var string */
        $resolvedPath = $imagineCacheManager->getBrowserPath($image->getPath(), 'product_image_optimized');

        return $this->getTmpPreDesignJson($dimensions->width, $dimensions->height, $resolvedPath);
    }

    /**
     * @param float $cmSize
     * @param int $dpi
     *
     * @return float
     */
    public function convertCmToPx(float $cmSize, int $dpi = 150): float
    {
        return ($cmSize / 2.54) * $dpi;
    }

    /**
     * Get temp json with only a image box.
     *
     * @param float $canvasWidth
     * @param float $canvasHeight
     * @param string $imgUrl
     *
     * @return \stdClass
     */
    public function getTmpPreDesignJson(float $canvasWidth, float $canvasHeight, string $imgUrl): \stdClass
    {
        $boxX = 0;
        $boxY = 0;
        $boxWidth = 100;
        $boxHeight = 100;

        $canvasRealWidth = $this->convertCmToPx($canvasWidth);
        $canvasRealHeight = $this->convertCmToPx($canvasHeight);

        $imgWidth = ($canvasRealWidth / 100) * $boxWidth;
        $imgHeight = ($canvasRealHeight / 100) * $boxHeight;
        $imgLeft = (($canvasRealWidth / 100) * $boxX) + ($imgWidth / 2);
        $imgTop = (($canvasRealHeight / 100) * $boxY) + ($imgHeight / 2);

        $json = '{"objects":[null,{"type":"image","originX":"center","originY":"center","left":%%imgLeft%%,"top":%%imgTop%%,"width":%%imgWidth%%,"height":%%imgHeight%%,"fill":"rgb(0,0,0)","stroke":null,"strokeWidth":0,"strokeDashArray":null,"strokeLineCap":"butt","strokeLineJoin":"miter","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","globalCompositeOperation":"source-over","transformMatrix":null,"skewX":0,"skewY":0,"crossOrigin":"anonymous","alignX":"none","alignY":"none","meetOrSlice":"meet","class":"free","fileId":1,"selectable":false,"lockRotation":false,"lockUniScaling":false,"hasBorders":true,"hasControls":true,"src":"%%url%%","filters":[],"resizeFilters":[]}],"background":"white","oldDimensions":{"type":"rectangle","width":"%%width%%","height":"%%height%%"}}';

        $json = str_replace([
            '%%url%%',
            '%%width%%',
            '%%height%%',
            '%%imgLeft%%',
            '%%imgWidth%%',
            '%%imgTop%%',
            '%%imgHeight%%',
        ], [
            $imgUrl,
            $canvasWidth,
            $canvasHeight,
            round($imgLeft, 3),
            round($imgWidth, 3),
            round($imgTop, 3),
            round($imgHeight, 3),
        ], $json);

        return json_decode($json);
    }

    /**
     * @throws FileNotFoundException
     *
     * @return \stdClass
     */
    public function getJson(): ?\stdClass
    {
        $filesystem = $this->getPrivateFilesystem();

        $path = $this->getUploadPath(self::FILE_JSON);

        if (!$filesystem->has($path)) {
            return null;
        }

        $content = $filesystem->read($path);
        $content = $this->regexReplace($content, true);

        return json_decode($content);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getUploadPath($path = ''): string
    {
        if ($this->isUrlDesign()) {
            return '/designer_url_images/' . $this->uuid . '/' . $path;
        }

        if ($this->isPreDesign()) {
            return '/designer_layouts/' . $this->uuid . '/' . $path;
        }

        return '/designer/' . $this->uuid . '/' . $path;
    }

    /**
     * @param $path
     *
     * @return $this
     */
    public function setUploadPath($path): self
    {
        $this->uploadPath = $path;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPreDesign(): bool
    {
        return $this->isPreDesign;
    }

    /**
     * @param bool $bool
     *
     * @return $this
     */
    public function setIsPreDesign($bool = false): self
    {
        $this->isPreDesign = (bool)$bool;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUrlDesign()
    {
        return $this->isUrlDesign;
    }

    /**
     * @param bool $isUrlDesign
     *
     * @return $this
     */
    public function setIsUrlDesign($isUrlDesign = false)
    {
        $this->isUrlDesign = (bool)$isUrlDesign;

        return $this;
    }

    /**
     * @param string $convertUrl
     *
     * @return $this
     */
    public function setConvertUrl(string $convertUrl)
    {
        $this->convertUrl = $convertUrl;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getConvertUrl(): ?string
    {
        return $this->convertUrl;
    }

    /**
     * @param bool $screenshotModus
     *
     * @return $this
     */
    public function setScreenshotModus(bool $screenshotModus)
    {
        $this->screenshotModus = $screenshotModus;

        return $this;
    }

    /**
     * @return bool
     */
    public function getScreenshotModus(): bool
    {
        return $this->screenshotModus;
    }

    /**
     * @param int $screenshotDelay
     *
     * @return $this
     */
    public function setScreenshotDelay(int $screenshotDelay)
    {
        $this->screenshotDelay = $screenshotDelay;

        return $this;
    }

    /**
     * @return int
     */
    public function getScreenshotDelay(): int
    {
        return $this->screenshotDelay;
    }

    /**
     * @param string $content
     * @param bool $useLocalPath
     * @param bool $useEscapeUrl
     *
     * @return string
     */
    private function regexReplace($content = '', $useLocalPath = false, $useEscapeUrl = false): string
    {
        // Old regex. temporarily kept for backup
        //$pattern = "|https?://[\w]*(?:\.[\w]*)*(?:\:\d*)?/designer/([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})/(.*)\?temp_url_sig=(\w*)&(?:amp;)?temp_url_expires=(\d*?)|iU";

        $pattern = "|https?\:.*designer\/([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})\/(.*)\?temp_url_sig=(\w*)&(?:amp;)?temp_url_expires=(\d*?)|iU";

        $content = preg_replace_callback($pattern, function ($matches) use ($useLocalPath, $useEscapeUrl) {
            if ($useLocalPath) {
                $url = $this->getLocalUrl($matches[2]);
            } else {
                $url = $this->getObjectStoreUrl($matches[2]);
            }

            if ($useEscapeUrl) {
                return htmlspecialchars($url);
            }

            return $url;

        }, $content);

        if ($useLocalPath) {
            $pattern = "|https?:\/\/[\w]*(?:\.[\w]*)*(?:\:\d*)?(/media/cache(/resolve)?/(.*)/images/product/[\d]*/[\w]*.[\w]*)|i";
            $content = preg_replace_callback($pattern, static function ($matches) {
                return $matches[1];
            }, $content);
        }

        return $content;
    }

    /**
     * Create a local path (translated in DesignerController::proxyAction)
     *
     * @param     $filename
     * @param int $expiresIn
     *
     * @return string
     */
    public function getLocalUrl($filename, $expiresIn = 60): string
    {
        $request = $this->requestStack->getMasterRequest();

        if (null === $request) {
            return '';
        }

        return $this->buildTempUrl($request->getSchemeAndHttpHost(), $filename, $expiresIn);
    }

    /**
     * @param $url
     * @param $filename
     * @param $expiresIn
     *
     * @return string
     */
    private function buildTempUrl($url, $filename, $expiresIn): string
    {
        // Calculate experices time.
        $expires = time() + $expiresIn;

        // Get path file path.
        $path = $this->getUploadPath($filename);

        // Get openstack key.
        $openstackTempUrlKey = $this->container->getParameter('openstack_temp_url_key');

        $params = RackspaceTempUrlPlugin::calculateQueryParams(
            trim($path, '/'),
            $expires,
            $openstackTempUrlKey
        );

        return $url . $path . '?' . http_build_query($params);
    }

    /**
     * @param     $filename
     * @param int $expiresIn
     *
     * @return string
     */
    public function getObjectStoreUrl($filename, $expiresIn = 60): string
    {
        $url = $this->container->getParameter('openstack_object_store') . '/private';

        return $this->buildTempUrl($url, $filename, $expiresIn);
    }

    /**
     * @return bool
     */
    public function hasPersonalization(): bool
    {
        return (bool)$this->getPersonalization();
    }

    /**
     * @return Personalization
     */
    public function getPersonalization(): ?Personalization
    {
        return $this->personalization;
    }

    /**
     * @param $personalization
     *
     * @return $this
     * @throws Exception
     */
    public function setPersonalization($personalization): self
    {
        if ($personalization instanceof Personalization) {
            $this->personalization = $personalization;
        } else {
            throw new \RuntimeException(sprintf("personalization must be a '%s'", Personalization::class));
        }

        return $this;
    }

    /**
     * @param \stdClass $config
     *
     * @return \stdClass
     */
    private function assignDesignSettings(\stdClass $config): \stdClass
    {
        if (!$this->settings) {
            return $config;
        }

        if ($this->getProduct()) {
            $config->title = $this->getProduct()->translate()->getTitle();
        }

        $config->layouts = $config->collages = $this->getCollages();
        $config->fonts = $this->getFonts();

        $config->extensions = [];

        $config->enableLayoutsTab = (!empty($this->settings['designer_ext_collage_enabled']) && $this->settings['designer_ext_collage_enabled']);

        $config->enableBgImageTab = (!empty($this->settings['designer_ext_background_image_enabled']) && $this->settings['designer_ext_background_image_enabled']);

        $config->enableBgPatternTab = (!empty($this->settings['designer_ext_background_pattern_enabled']) && $this->settings['designer_ext_background_pattern_enabled']);

        $config->enableEmojiTab = (!empty($this->settings['designer_ext_emoji_enabled']) && $this->settings['designer_ext_emoji_enabled']);

        $config->enableImageTab = (!empty($this->settings['designer_ext_image_enabled']) && $this->settings['designer_ext_image_enabled']);

        $config->enableTextTab = (!empty($this->settings['designer_ext_text_enabled']) && $this->settings['designer_ext_text_enabled']);

        return $config;
    }

    /**
     * @return Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param $product
     *
     * @return $this
     * @throws Exception
     */
    public function setProduct($product): self
    {
        if ($product instanceof Product) {
            $this->product = $product;
        } else {
            throw new \RuntimeException(sprintf("personalization must be a '%s'", Product::class));
        }

        return $this;
    }

    /**
     * @return array
     */
    private function getCollages(): array
    {
        if (empty($this->settings['designer_ext_collage_items'])) {
            return [];
        }

        return $this->createCollageList($this->settings['designer_ext_collage_items']);
    }

    /**
     * @param array $collages
     *
     * @return array
     */
    private function createCollageList(array $collages = []): array
    {
        return array_map(static function (DesignerCollage $collage) {
            return (object)[
                'name' => 'template_' . $collage->getId(),
                'active' => false,
                'boxes' => array_map(static function (DesignerCollageObject $collageObject) {
                    return [
                        'type' => $collageObject->getType(),
                        'x' => $collageObject->getX() * 100,
                        'y' => $collageObject->getY() * 100,
                        'width' => $collageObject->getWidth() * 100,
                        'height' => $collageObject->getHeight() * 100,
                    ];
                }, $collage->getObjects()->toArray()),
            ];
        }, $collages);
    }

    /**
     * @return DesignerFont[]
     */
    private function getFonts(): array
    {
        if (empty($this->settings['designer_ext_text_fonts'])) {
            return [];
        }

        return $this->createFontList($this->settings['designer_ext_text_fonts']);
    }

    /**
     * @param array $fonts
     *
     * @return array
     */
    private function createFontList(array $fonts = []): array
    {
        return array_map(static function (DesignerFont $font) {
            return (object)[
                'family' => $font->getFamily(),
                'type' => $font->getType(),
            ];

        }, $fonts);
    }

    /**
     * @return bool
     */
    public function hasProduct(): bool
    {
        return (bool)$this->getProduct();
    }

    /**
     * @return bool
     * @throws FileNotFoundException
     */
    public function convertJsonIntoImage()
    {
        $convertUrl = $this->getConvertUrl();

        if(!$convertUrl) {
            throw new \RuntimeException('Convert url not defined.');
        }

        if (null === $this->getJson()) {
            throw new \RuntimeException('JSON data not found');
        }

        $tmpFile = $this->tmpFile(self::FILE_PRINT);

        Browsershot::url($convertUrl)
            ->select('#screenshot')
            ->waitForFunction('window.designerReady == true')
            ->save($tmpFile);

        // Move PNG to objectstore.
        $this->moveFileToObjectStore($tmpFile, self::FILE_PRINT);

        // Create thumb of local tmp file.
        $this->convertPngToJpg($tmpFile);

        $this->fs->remove(\dirname($tmpFile));

        return $this->getFileUrl();
    }


    /**
     * @param string $inputName
     *
     * @return bool|null|string
     * @throws Exception
     */
    public function processUpload()
    {
        $request = $this->requestStack->getMasterRequest();

        if (null === $request) {
            throw new ProcessDesignException('Request not found.');
        }

        $jsonData = $request->get('json');

        $uploadedFile = $request->files->get(self::PROCESS_FILE_FIELD_NAME);

        if ($jsonData) {
            // Remove existing images
            $this->removeFileFromObjectStore(self::FILE_PRINT);
            $this->removeFileFromObjectStore(self::FILE_PREVIEW);
            $this->removeFileFromObjectStore(self::FILE_MANIPULATED_PRINT);

            $resultJson = $this->moveFileDataToObjectStore($jsonData, self::FILE_JSON);

            if (!$resultJson) {
                throw new ProcessDesignException('Process of JSON data failed');
            }

            if($this->convertOnProcessEnabled()) {
                $this->convertJsonIntoImage();
            }

            return $this->getFileUrl(self::FILE_PREVIEW);
        }

        // todo: Move to own method
        if ($uploadedFile) {
            if (!$this->validateFileExtension($uploadedFile)) {
                throw new \RuntimeException(sprintf("Invalid file type '%s'.",
                    $uploadedFile->getClientOriginalExtension()));
            }

            $filename = $this->generateFilename($uploadedFile);

            if (filesize($uploadedFile->getRealPath()) === 0) {
                throw new \RuntimeException('Empty file');
            }

            $this->moveFileToObjectStore($uploadedFile->getRealPath(), $filename);

            return $this->getLocalUrl($filename);
        }

        return null;
    }

    /**
     * @param string $filename
     * @return mixed
     */
    public function removeFileFromObjectStore(string $filename)
    {
        $filesystem = $this->getPrivateFilesystem();

        $path = $this->getUploadPath($filename);

        if ($filesystem->has($path)) {
            return $filesystem->delete($path);
        }

        return false;
    }

    /**
     * @param $data
     * @param $filename
     *
     * @return bool
     */
    public function moveFileDataToObjectStore($data, $filename): ?bool
    {
        $filesystem = $this->getPrivateFilesystem();

        try {
            $stream = fopen('php://temp', 'rb+');
            fwrite($stream, $data);
            rewind($stream);

            $uploadPath = $this->getUploadPath($filename);

            return $filesystem->putStream($uploadPath, $stream);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Generate flipped / rotated image from printUrl
     *
     * @param array $manipulations
     *
     * @return bool
     */
    public function applyImageManipulation(array $manipulations = [])
    {
        if(
            !array_key_exists('flip', $manipulations) && !array_key_exists('rotation', $manipulations)
        ) {
            return false;
        }

        if (
            $this->fileExist(self::FILE_MANIPULATED_PRINT)
        ) {
            return true;
        }

        $printUrl = $this->getFileUrl();

        try {
            $tmpFile = $this->tmpFile(self::FILE_MANIPULATED_PRINT);

            $manager = new ImageManager(['driver' => 'imagick']);

            $image = $manager->make($printUrl);

            if (array_key_exists('flip', $manipulations) && $manipulations['flip'] === true) {
                $image = $image->flip();
            }

            if (array_key_exists('rotation', $manipulations) && (int)$manipulations['rotation'] !== 0) {
                $image = $image->rotate((int)$manipulations['rotation']);
            }

            $image->save($tmpFile);

            $this->moveFileToObjectStore($tmpFile, self::FILE_MANIPULATED_PRINT);

            // Remove tmp file.
            $this->fs->remove(basename($tmpFile));

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Convert svg to png and jpg
     * @deprecated
     * @return bool
     */
    public function convertSVGtoPngAndJpg(): ?bool
    {
        try {
            if (null === $this->getSvg()) {
                throw new \RuntimeException('SVG data not found');
            }

            // Load SVG data.
            $svg = SVG::createFromData($this->getSvg());

            if (!$svg->getWidth() || !$svg->getHeight()) {
                throw new \RuntimeException('SVG size is unknown');
            }

            $tmpFile = $this->tmpFile(self::FILE_PRINT);

            /* Create screenshot (PNG) of SVG file */
            try {
                if(null === $svg->getSvg()) {
                    throw new \RuntimeException('SVG element not found.');
                }

                Browsershot::html('<html lang="nl"><head><title>SVG</title><meta charset="UTF-8"></head><body style="margin:0">' . $svg->getSvg()->asXML() . '</body></html>')
                    ->windowSize($svg->getWidth(), $svg->getHeight())
                    ->save($tmpFile);

            } catch (Exception $exception) {
                throw $exception;
            }

            // Move PNG to objectstore.
            $this->moveFileToObjectStore($tmpFile, self::FILE_PRINT);

            // Create thumb of local tmp file.
            $this->convertPngToJpg($tmpFile);

            $this->fs->remove(\dirname($tmpFile));

            return true;

        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Convert to pdf
     *
     * Print Url can be fetched by $this->getPrintUrl(self::OPTION)
     *
     * @param string $printUrl
     * @return string
     * @throws FileNotFoundException
     */
    public function getPdf(string $printUrl): string
    {
        if($this->getUuid() === null){
            throw new \RuntimeException('Uuid must be set first');
        }

        Network::isExternalResourceAvailable($printUrl, true, 'Print url does not seem to exist');

        $width = ($this->getJson()->oldDimensions->width * 10);
        $height = ($this->getJson()->oldDimensions->height * 10);

        return Browsershot::html('<html><head><meta charset="UTF-8"></head><body style="margin:0;"><img id="image" style="width: 100%; height: 100%; position: absolute; display: block; top: 0; left: 0;" src="' . $printUrl. '"/></body></html>')
            ->paperSize($width, $height)
            ->pdf();
    }

    /**
     * @throws FileNotFoundException
     *
     * @return null|string
     */
    public function getSvg(): ?string
    {
        $filesystem = $this->getPrivateFilesystem();

        $svgPath = $this->getUploadPath(self::FILE_SVG);

        if (!$filesystem->has($svgPath)) {
            return null;
        }

        $content = $filesystem->read($svgPath);
        $content = $this->regexReplace($content, false, true);

        return $content;
    }

    /**
     * @param $file
     * @param $filename
     *
     * @return bool
     */
    private function moveFileToObjectStore($file, $filename): ?bool
    {
        $filesystem = $this->getPrivateFilesystem();

        try {
            if (!file_exists($file)) {
                throw new \RuntimeException('Tmp designer image not found.');
            }

            return $filesystem->putStream($this->getUploadPath($filename), fopen($file, 'rb+'));
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param string $filename
     * @param string $content
     *
     * @return string
     */
    private function tmpFile(string $filename, string $content = '')
    {
        $tmpFile = implode(DIRECTORY_SEPARATOR, [
            sys_get_temp_dir(),
            $this->getUuid(),
            $filename
        ]);

        $this->fs->dumpFile($tmpFile, $content);

        return $tmpFile;
    }

    /**
     * Create thumb image of big png file.
     *
     * @param     $url
     * @param int $width
     *
     * @return bool
     */
    private function convertPngToJpg($url, int $width = 300): ?bool
    {
        try {
            $tmpFile = $this->tmpFile(self::FILE_PREVIEW);

            $manager = new ImageManager(['driver' => 'imagick']);
            $manager
                ->make($url)
                ->resize($width, null, static function (Constraint $constraint) {
                    $constraint->aspectRatio();
                })
                ->save($tmpFile);

            $this->moveFileToObjectStore($tmpFile, self::FILE_PREVIEW);

            // Remove tmp file.
            $this->fs->remove(basename($tmpFile));

            return true;

        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @param $uploadedFile UploadedFile
     *
     * @return bool
     */
    private function validateFileExtension(UploadedFile $uploadedFile): bool
    {
        if (!strpos($uploadedFile->getMimeType(), 'image')) {
            if (!\in_array($uploadedFile->guessExtension(), self::ALLOWED_IMAGE_EXTENSIONS, true)) {
                return false;
            }
        } elseif (!\in_array($uploadedFile->getClientOriginalExtension(), ['svg', 'json'])) {
            return false;
        }

        return true;
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return string
     */
    private function generateFilename(UploadedFile $uploadedFile): string
    {
        return uniqid('', true) . '.' . $uploadedFile->getClientOriginalExtension();
    }

    /**
     * @param $returnUrl
     *
     * @return $this
     */
    public function setReturnUrl(string $returnUrl): self
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }

    /**
     * @param $cancelUrl
     *
     * @return $this
     */
    public function setCancelUrl(string $cancelUrl): self
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }

    /**
     * @param bool $convertOnProcess
     */
    public function setConvertOnProcess(bool $convertOnProcess)
    {
        $this->convertOnProcess = $convertOnProcess;
    }

    /**
     * @return bool
     */
    public function convertOnProcessEnabled()
    {
        return $this->convertOnProcess;
    }

    /**
     * @return string|null
     */
    public function getCancelUrl(): ?string
    {
        return $this->cancelUrl;
    }

    /**
     * @param $assetsPath
     *
     * @return $this
     */
    public function setAssetsPath(string $assetsPath): self
    {
        $this->assetsPath = $assetsPath;

        return $this;
    }

    /**
     * @param bool $debugMode
     *
     * @return Designer
     */
    public function setDebugMode(bool $debugMode = false): self
    {
        $this->debugMode = $debugMode;

        return $this;
    }

    /**
     * @return bool
     */
    public function inDebugMode(): bool
    {
        return $this->debugMode;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        $filesystem = $this->getPrivateFilesystem();
        $path = $this->getUploadPath(self::FILE_JSON);

        return $filesystem->has($path);
    }

    /**
     * @deprecated Legacy function use getFileUrl(self::FILE_PRINT) instead
     * @see getFileUrl
     * @param string $filename
     * @param int $expires
     * @param string $source
     *
     * @return null|string
     */
    public function getPrintUrl(
        $filename = self::FILE_PRINT,
        $expires = 60,
        $source = self::SOURCE_OBJECT_STORE
    ): ?string {
        return $this->getFileUrl($filename, $expires, $source);
    }

    /**
     * @param int $expires
     * @param string $source
     *
     * @return null|string
     */
    public function getPreviewUrl($expires = 60, $source = self::SOURCE_OBJECT_STORE): ?string
    {
        return $this->getFileUrl(self::FILE_PREVIEW, $expires, $source);
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function fileExist($filename = self::FILE_PRINT): bool
    {
        return $this->getPrivateFilesystem()->has($this->getUploadPath($filename));
    }

    /**
     * @param string $filename
     * @param int    $expires
     * @param string $source
     *
     * @return null|string
     */
    public function getFileUrl(
        $filename = self::FILE_PRINT,
        $expires = 60,
        $source = self::SOURCE_OBJECT_STORE
    ): ?string {
        if (false === $this->fileExist($filename)) {
            return null;
        }

        switch ($source) {
            case self::SOURCE_LOCAL:
                return $this->getLocalUrl($filename, $expires);
            case self::SOURCE_OBJECT_STORE:
                return $this->getObjectStoreUrl($filename, $expires);
            default:
                return null;
        }
    }

    /**
     * @param string $templateUuid
     *
     * @return array
     * @throws FileNotFoundException
     */
    public function getAlternativeTemplates(string $templateUuid): array
    {
        /** @var DesignerTemplate $mainDesignerTemplate */
        $mainDesignerTemplate = $this->entityManager->getRepository(DesignerTemplate::class)->findOneBy([
            'uuid' => $templateUuid,
        ]);

        $mainDesignerTemplateTags = [];

        /** @var Tag $tag */
        foreach ($mainDesignerTemplate->getTags() as $tag) {
            $mainDesignerTemplateTags[] = $tag->getId();
        }

        $qb = $this->entityManager
            ->getRepository(DesignerTemplate::class)
            ->createQueryBuilder('dt');

        $qb
            ->leftJoin('dt.tags', 'dtt')
            ->andWhere('dt.canvas = :canvas')
            ->andWhere($qb->expr()->in('dtt.id', $mainDesignerTemplateTags))
            ->setParameter('canvas', $mainDesignerTemplate->getCanvas()->getId());

        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        if ($user instanceof Customer && null !== $user->getCompany()) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->isNull('dt.company'),
                    $qb->expr()->eq('dt.company', $user->getCompany()->getId())
                ));
        }

        $designerTemplates = $qb->getQuery()->getResult();

        $oldUuid = $this->uuid;
        $oldIsPreDesign = $this->isPreDesign;
        $this->isPreDesign = true;

        $results = [];
        /** @var DesignerTemplate $designerTemplate */
        foreach ($designerTemplates as $designerTemplate) {

            $this->uuid = $designerTemplate->getUuid();

            // Check if we got an template preview
            if ($getTemplatePreview = $this->getTemplatePreview()) {
                $previewUrl = $getTemplatePreview;
            } else {
                $previewUrl = $this->getFileUrl(self::FILE_PREVIEW);
            }

            // Only show when we got an image
            if ($previewUrl) {
                $results[] = [
                    'title' => $designerTemplate->translate()->getTitle(),
                    'uuid' => $designerTemplate->getUuid(),
                    'imgSrc' => $previewUrl,
                ];
            }
        }

        $this->uuid = $oldUuid;
        $this->isPreDesign = $oldIsPreDesign;

        return $results;
    }

    /**
     * @return null|string
     */
    public function getTemplatePreview(): ?string
    {
        if ($this->uuid) {
            $filesystem = $this->getPublicFilesystem();

            $designerTemplate = $this->entityManager->getRepository(DesignerTemplate::class)->findOneBy([
                'uuid' => $this->uuid,
            ]);

            if (null === $designerTemplate) {
                return null;
            }

            $imagePath = $designerTemplate->getPath();

            if ($filesystem->has($imagePath)) {
                /** @noinspection PhpUndefinedMethodInspection */
                return $filesystem->getPublicUrl($imagePath);
            }
        }

        return null;
    }

    /**
     * @return FilesystemInterface
     */
    private function getPrivateFilesystem()
    {
        return $this->mountManager->getFilesystem('filesystem_private');
    }

    /**
     * @return FilesystemInterface
     */
    private function getPublicFilesystem()
    {
        return $this->mountManager->getFilesystem('filesystem_public');
    }

    /**
     * @return string|null
     * @throws FileNotFoundException
     * @throws FileExistsException
     * @throws Exception
     */
    public function cloneDesign(){
        if($this->uuid === null){
            return null;
        }

        $cloneUuid = (string)Uuid::uuid4();
        $oldPath = $this->getUploadPath();
        $newPath = str_replace($this->uuid, $cloneUuid, $oldPath);
        $newJson = $newPath . self::FILE_JSON;

        $jsonContent = $this->getPrivateFilesystem()->read($oldPath . self::FILE_JSON);

        $contents = $this->getPrivateFilesystem()->listContents($oldPath);
        $fileError = !($jsonContent !== false);
        foreach($contents as $object){
            if(false === $this->getPrivateFilesystem()->copy($object['path'], $newPath . $object['basename'])){
                $fileError = true;
            }
        }

        $jsonContent = str_replace($this->uuid, $cloneUuid, $jsonContent);
        if(false === $fileError && $this->getPrivateFilesystem()->put($newJson, $jsonContent) === false){
            $fileError = true;
        }

        if($fileError === true){
            $this->getPrivateFilesystem()->deleteDir($newPath);
            throw new FileNotFoundException(sprintf('No design with UUID %s found', $this->uuid));
        }

        return $cloneUuid;
    }
}
