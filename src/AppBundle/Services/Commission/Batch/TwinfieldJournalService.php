<?php

namespace AppBundle\Services\Commission\Batch;

use AppBundle\Entity\Finance\CommissionBatch;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class TwinfieldJournalService
{
    /**
     * @var CommissionBatch
     */
    private $commissionBatch;

    public function setCommissionBatch(CommissionBatch $commissionBatch)
    {
        $this->commissionBatch = $commissionBatch;
    }

    /**
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function export()
    {
        set_time_limit(0);

        \PHPExcel_Settings::setLocale("nl");

        $excel = new \PHPExcel();

        $sheet = $excel->getActiveSheet();

        $sheet->setCellValue("A1", "Code");
        $sheet->setCellValue("B1", "Currency");
        $sheet->setCellValue("C1", "Date");
        $sheet->setCellValue("D1", "Period");
        $sheet->setCellValue("E1", "Invoicenumber");
        $sheet->setCellValue("F1", "Duedate");
        $sheet->setCellValue("G1", "Number");
        $sheet->setCellValue("H1", "GL-account");
        $sheet->setCellValue("I1", "AP");
        $sheet->setCellValue("J1", "Project");
        $sheet->setCellValue("K1", "Amount");
        $sheet->setCellValue("L1", "Debitcredit");
        $sheet->setCellValue("M1", "Description");
        $sheet->setCellValue("N1", "Vatcode");

        $rowIndex = 2;

        foreach ($this->commissionBatch->getCommissionInvoices() as $commissionInvoice) {
            $formattedPeriod = $commissionInvoice->getDate()->format("Y") . "/" . $commissionInvoice->getDate()->format("n");

            $sheet->setCellValue("A" . $rowIndex, "VRK");
            $sheet->setCellValue("B" . $rowIndex, "EUR");
            $sheet->setCellValue("C" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDate()));
            $sheet->getStyle("C" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

            $sheet->setCellValue("D" . $rowIndex, $formattedPeriod);
            $sheet->setCellValue("E" . $rowIndex, $commissionInvoice->getNumber());
            $sheet->setCellValue("F" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDueDate()));
            $sheet->getStyle("F" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

            $sheet->setCellValue("G" . $rowIndex, null);
            $sheet->setCellValue("H" . $rowIndex, 1400);
            $sheet->setCellValue("I" . $rowIndex, $commissionInvoice->getCompany()->getTwinfieldRelationNumber());
            $sheet->setCellValue("J" . $rowIndex, null);
            $sheet->setCellValue("K" . $rowIndex, $commissionInvoice->getTotalIncl());
            $sheet->getStyle("K" . $rowIndex)->getNumberFormat()->setFormatCode('#,##0.00');

            $sheet->setCellValue("L" . $rowIndex, "credit");
            $sheet->setCellValue("M" . $rowIndex, $commissionInvoice->getNumber());
            $sheet->setCellValue("N" . $rowIndex, null);

            $rowIndex++;

            foreach ($commissionInvoice->getVatSpecifications() as $vatSpecification) {
                $sheet->setCellValue("A" . $rowIndex, "VRK");
                $sheet->setCellValue("B" . $rowIndex, "EUR");
                $sheet->setCellValue("C" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDate()));
                $sheet->getStyle("C" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                $sheet->setCellValue("D" . $rowIndex, $formattedPeriod);
                $sheet->setCellValue("E" . $rowIndex, $commissionInvoice->getNumber());
                $sheet->setCellValue("F" . $rowIndex,
                    \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDueDate()));
                $sheet->getStyle("F" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                $sheet->setCellValue("G" . $rowIndex, null);

                switch ($vatSpecification->getVatRate()->getFactor() * 100) {
                    case 0:
                        $sheet->setCellValue("H" . $rowIndex, 3632);
                        break;
                    case 6:
                    case 9:
                        $sheet->setCellValue("H" . $rowIndex, 3612);
                        break;
                    case 21:
                        $sheet->setCellValue("H" . $rowIndex, 3622);
                        break;
                }

                $sheet->setCellValue("I" . $rowIndex, null);
                $sheet->setCellValue("J" . $rowIndex, null);
                $sheet->setCellValue("K" . $rowIndex, $vatSpecification->getPrice());
                $sheet->getStyle("K" . $rowIndex)->getNumberFormat()->setFormatCode('#,##0.00');

                $sheet->setCellValue("L" . $rowIndex, "debit");
                $sheet->setCellValue("M" . $rowIndex, $commissionInvoice->getNumber());
                $sheet->setCellValue("N" . $rowIndex, $vatSpecification->getVatRate()->getCode());

                $rowIndex++;
            }

            foreach ($commissionInvoice->getLines() as $line) {
                $sheet->setCellValue("A" . $rowIndex, "VRK");
                $sheet->setCellValue("B" . $rowIndex, "EUR");
                $sheet->setCellValue("C" . $rowIndex, \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDate()));
                $sheet->getStyle("C" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                $sheet->setCellValue("D" . $rowIndex, $formattedPeriod);
                $sheet->setCellValue("E" . $rowIndex, $commissionInvoice->getNumber());
                $sheet->setCellValue("F" . $rowIndex,
                    \PHPExcel_Shared_Date::PHPToExcel($commissionInvoice->getDueDate()));
                $sheet->getStyle("F" . $rowIndex)->getNumberFormat()->setFormatCode("d-m-yyyy");

                $sheet->setCellValue("G" . $rowIndex, null);
                $sheet->setCellValue("H" . $rowIndex, 8911);
                $sheet->setCellValue("I" . $rowIndex, null);
                $sheet->setCellValue("J" . $rowIndex, null);
                $sheet->setCellValue("K" . $rowIndex, abs($line->getPrice()));
                $sheet->getStyle("K" . $rowIndex)->getNumberFormat()->setFormatCode('#,##0.00');

                if ($line->getPrice() > 0) {
                    $sheet->setCellValue("L" . $rowIndex, "debet");
                } else {
                    $sheet->setCellValue("L" . $rowIndex, "credit");
                }
                $sheet->setCellValue("M" . $rowIndex, $commissionInvoice->getNumber());
                $sheet->setCellValue("N" . $rowIndex, "VH");

                $rowIndex++;
            }
        }

        $path = tempnam(sys_get_temp_dir(), "export");

        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->save($path);

        $name = "Twinfield - Debiteuren (Batch " . $this->commissionBatch->getNumber() . ")";

        $response = new BinaryFileResponse($path, 200, [
//            "Content-type" => "application/vnd.ms-excel",
            "Content-type" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "Content-Disposition" => 'attachment; filename="' . $name . '.xlsx"',
        ]);

        $response->deleteFileAfterSend(true);

        return $response;
    }
}
