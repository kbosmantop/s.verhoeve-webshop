<?php

namespace AppBundle\Services\Sync;

use AppBundle\Client\FloristClient;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Exceptions\FloristSyncException;
use AppBundle\Exceptions\InvalidBuildingNumberException;
use AppBundle\Services\Company\CompanyRegistrationService;
use AppBundle\Services\Sync\AbstactFloristSync;
use AppBundle\Utils\BuildingNumber;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Florists
 * @package AppBundle\Services\Sync
 */
class Florists extends AbstractFloristSync
{
    use ContainerAwareTrait;

    /**
     * @var ArrayCollection|Customer[]
     */
    private $currentCustomers;

    /**
     * @var array
     */
    private $duplicateEmailAddresses = [];

    /**
     * @var CustomerGroup
     */
    private $customerGroup;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var array
     */
    private $messages;

    /**
     * @var CompanyRegistrationService
     */
    private $companyRegistrationService;

    /**
     * @var array
     */
    protected $companyRegistrations;

    /**
     * Florists constructor.
     * @param FloristClient              $floristClient
     * @param EntityManagerInterface     $entityManager
     * @param CompanyRegistrationService $companyRegistrationService
     * @throws FloristSyncException
     */
    public function __construct(
        FloristClient $floristClient,
        EntityManagerInterface $entityManager,
        CompanyRegistrationService $companyRegistrationService
    ){
        parent::__construct($floristClient, $entityManager);

        $this->companyRegistrationService = $companyRegistrationService;
    }

    /**
     * @throws FloristSyncException
     */
    private function lookUpCustomerGroup()
    {
        $customerGroup = $this->entityManager
            ->getRepository(CustomerGroup::class)
            ->findOneBy([
            'description' => 'Bloemisten',
        ]);

        if (!$customerGroup) {
            throw new FloristSyncException(sprintf('Customergroup "%s" must exists', 'Bloemisten'));
        }

        return $customerGroup;
    }

    /**
     * @return Customer[]|array
     */
    private function lookUpCustomers()
    {
        return $this->entityManager->getRepository(Customer::class)->findBy([
            'customerGroup' => $this->customerGroup,
        ]);
    }

    /**
     * @throws FloristSyncException
     */
    private function getFlorists()
    {
        $response = $this->floristClient->get('/export/exportPartners');

        if (strpos($response->getHeaderLine('Content-Type'), 'application/json') === false) {
            throw new FloristSyncException(sprintf("Expected 'application/json', got '%s'",
                $response->getHeaderLine('Content-Type')));
        }

        $florists = json_decode($response->getBody());

        if(false === $florists) {
            throw new FloristSyncException('Invalid json');
        }

        return $florists;
    }

    /**
     * @param OutputInterface $output
     *
     * @return $this
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        if ($output && $output->isVerbose()) {
            $this->progressBar = new ProgressBar($this->output);
        }

        return $this;
    }

    /**
     * @return bool
     * @throws FloristSyncException
     */
    public function import()
    {
        $this->customerGroup = $this->lookUpCustomerGroup();
        $this->currentCustomers = new ArrayCollection($this->lookUpCustomers());

        $florists = $this->getFlorists();

        $emailAddresses = [];


        foreach ($florists as $florist) {
            if (!$florist->email) {
                continue;
            }

            if (empty($florist->addressing_company_name)) {
                $florist->addressing_company_name = $florist->company_name;
            }

            if (!array_key_exists($florist->email, $emailAddresses)) {
                $emailAddresses[$florist->email] = [];
            }

            $emailAddresses[$florist->email][] = $florist;
        }

        $this->duplicateEmailAddresses = array_filter($emailAddresses,
            function ($floristsWithDuplicateEmailAddresses) {
                return count($floristsWithDuplicateEmailAddresses) > 1;
            });

        if ($this->output && $this->output->isVerbose()) {
            foreach ($this->duplicateEmailAddresses as $duplicateEmailAddress => $floristsWithDuplicateEmailAddresses) {
                $this->output->writeln($duplicateEmailAddress);

                foreach ($floristsWithDuplicateEmailAddresses as $floristsWithDuplicateEmailAddress) {
                    $this->output->writeln($floristsWithDuplicateEmailAddress->company_florist_id . ' (' . $floristsWithDuplicateEmailAddress->country . ')');
                }
            }
        }

        if ($this->progressBar) {
            $this->progressBar->start(count($florists));
        }

        $this->messages = [];

        foreach ($florists as $florist) {
            $this->processFlorist($florist);
        }

        foreach ($this->currentCustomers as $currentCustomer) {
            $currentCustomer->setEnabled(false);
        }

        $this->entityManager->flush();

        if ($this->progressBar) {
            $this->progressBar->finish();
            $this->output->writeln('');
        }

        if (!empty($this->messages)) {
            $this->output->write($this->messages);
        }

        return true;
    }

    /**
     * @param $florist
     * @return bool
     * @throws FloristSyncException
     */
    private function processFlorist($florist)
    {
        if (!$florist->email) {
            $this->advance();

            return false;
        }

        if (array_key_exists($florist->email, $this->duplicateEmailAddresses)) {
            $this->advance();

            return false;
        }

        $country = $this->entityManager->find(Country::class, $florist->country);

        $twinfieldRelationNumber = $this->determineTwinfieldRelationNumber($florist);

        $phoneNumber = null;

        if ($florist->phone_number) {
            try {
                $phoneNumber = PhoneNumberUtil::getInstance()->format(PhoneNumberUtil::getInstance()->parse($florist->phone_number,
                    $florist->country), PhoneNumberFormat::INTERNATIONAL);
            } catch (\Exception $e) {
            }
        }

        $mobileNumber = null;

        if ($florist->mobilenumber) {
            try {
                $mobileNumber = PhoneNumberUtil::getInstance()->format(PhoneNumberUtil::getInstance()->parse($florist->mobilenumber,
                    $florist->country), PhoneNumberFormat::INTERNATIONAL);
            } catch (\Exception $e) {
            }
        }

        $company = $this->entityManager->getRepository(Company::class)->findOneBy([
            'twinfieldRelationNumber' => $twinfieldRelationNumber,
        ]);

        if (!$company) {
            $company = new Company();
            $company->setTwinfieldRelationNumber($twinfieldRelationNumber);

            $this->entityManager->persist($company);
        }

        $company->setName($florist->company_name);
        $company->setPhoneNumber($phoneNumber);
        $company->setEmail($florist->email);

        $vatNumber = preg_replace('/[^0-9A-Z]+/', '', strtoupper($florist->company_vat_number));

        $company->setVatNumber(!empty($vatNumber) ? $vatNumber : null);

        $company->setVerified(true);
        $company->setIsCustomer(true);

        $company->setIsSupplier(null);
        $company->setSupplierConnector(null);

        if (isset($florist->is_supplier) && (int)$florist->is_supplier === 1) {
            $company->setIsSupplier(true);
            $company->setSupplierConnector('Topbloemen');
        }

        $company->addSupplierGroup($this->getFloristSupplierGroup());

        if (!$company->getCustomerGroups()->contains($this->customerGroup)) {
            $company->addCustomerGroup($this->customerGroup);
        }

        if ($florist->country === 'NL' && (empty($company->getChamberOfCommerceNumber()) || $company->getChamberOfCommerceNumber() === '99999999')) {
            $company->setChamberOfCommerceNumber('-' . $florist->company_florist_id);
        } else if ($florist->country === 'BE' && !empty($company->getChamberOfCommerceNumber())) {
            // Reset KVK column for Belgium
            $company->setChamberOfCommerceNumber(null);
        }

        if (!$company->getMainAddress()) {
            $mainAddress = new Address();
            $mainAddress->setCompany($company);

            $company->setDefaultDeliveryAddress($mainAddress);
        } else {
            $mainAddress = $company->getMainAddress();
        }

        $mainAddress->setCountry($country);
        $mainAddress->setDescription('Vestigingsadres');
        $mainAddress->setCompanyName($florist->addressing_company_name);
        $mainAddress->setAttn($florist->firstname . ' ' . ($florist->lastname_prefix ? $florist->lastname_prefix . ' ' : '') . $florist->lastname);
        $mainAddress->setEmail($florist->email);
        $mainAddress->setMainAddress(true);
        $mainAddress->setDeliveryAddress(true);
        $mainAddress->setInvoiceAddress(false);
        $mainAddress->setStreet($florist->street);

        try {
            $numberParts = BuildingNumber::split($florist->number);
        } catch (InvalidBuildingNumberException $exception) {
            throw new \RuntimeException(sprintf('Could not parse building number of florist %s).',
                $florist->addressing_company_name));
        }

        if (isset($numberParts['number'])) {
            $mainAddress->setNumber($numberParts['number']);
        }

        if (isset($numberParts['numberAddition'])) {
            $mainAddress->setNumberAddition($numberParts['numberAddition']);
        }

        $mainAddress->setPostcode(str_replace(' ', '', $florist->postcode));
        $mainAddress->setCity($florist->city);
        $mainAddress->setPhoneNumber($phoneNumber);

        // Create a Establishment if there is no company registration
        if (
            null === $company->getCompanyRegistration()
            && true === $company->getEstablishments()->isEmpty()
        ) {
            $establishment = new CompanyEstablishment();

            $establishment->setCompany($company);
            $establishment->setAddress($mainAddress);
            $establishment->setName($company->getName());

            $company->addEstablishment($establishment);

            $this->entityManager->persist($establishment);
        }

        if (null === $mainAddress->getId()) {
            $this->entityManager->persist($mainAddress);
        }

        $invoiceAddress = $company->getInvoiceAddress();

        if (null === $invoiceAddress) {
            $invoiceAddress = new Address();
            $invoiceAddress->setCompany($company);
            $company->setInvoiceAddress($invoiceAddress);
        }

        $invoiceAddress->setDescription('Factuuradres');
        $invoiceAddress->setCountry($country);
        $invoiceAddress->setCompanyName($florist->addressing_company_name);
        $invoiceAddress->setAttn($florist->firstname . ' ' . ($florist->lastname_prefix ? $florist->lastname_prefix . ' ' : '') . $florist->lastname);
        $invoiceAddress->setEmail($florist->email);
        $invoiceAddress->setMainAddress(false);
        $invoiceAddress->setDeliveryAddress(false);
        $invoiceAddress->setInvoiceAddress(true);
        $invoiceAddress->setStreet($florist->street);

        if (isset($numberParts['number'])) {
            $invoiceAddress->setNumber($numberParts['number']);
        }

        if (isset($numberParts['numberAddition'])) {
            $invoiceAddress->setNumberAddition($numberParts['numberAddition']);
        }

        $invoiceAddress->setPostcode(str_replace(' ', '', $florist->postcode));
        $invoiceAddress->setCity($florist->city);
        $invoiceAddress->setPhoneNumber($phoneNumber);

        if (null === $invoiceAddress->getId()) {
            $this->entityManager->persist($invoiceAddress);
        }

        if ($company->getCustomers()->isEmpty()) {
            $customer = new Customer();
            $customer->setCompany($company);
            $customer->setCustomerGroup($this->customerGroup);

            $company->addCustomer($customer);
        } else {
            $customer = $company->getCustomers()->current();

            $this->currentCustomers->removeElement($customer);
        }

        $customer->setEnabled(true);
        $customer->setGender($florist->gender);
        $customer->setFirstname($florist->firstname);
        $customer->setLastname(trim($florist->lastname_prefix . ' ' . $florist->lastname));
        $customer->setPhoneNumber($phoneNumber);
        $customer->setMobilenumber($mobileNumber);
        $customer->setUsername($florist->email);
        $customer->setUsernameCanonical($florist->email);
        $customer->setLocale($florist->locale);
        $customer->setDefaultInvoiceAddress($invoiceAddress);

        $this->entityManager->persist($customer);

        $this->advance();

        try {
            if (null === $company->getCompanyRegistration()) {
                if(!isset($this->companyRegistrations[$vatNumber])) {
                    $companyRegistration = $this->companyRegistrationService->migrateCompany($company);
                    $this->companyRegistrations[$vatNumber] = $companyRegistration;
                } else {
                    $company->setCompanyRegistration($this->companyRegistrations[$vatNumber]);
                }
            }
        } catch (\Exception $exception) {
            $this->messages[] = '<info>' . $exception->getMessage() . '</info>' . PHP_EOL;
        }

        return true;
    }

    private function advance()
    {
        if ($this->progressBar) {
            $this->progressBar->advance();
        }
    }

}
