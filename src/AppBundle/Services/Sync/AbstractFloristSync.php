<?php

namespace AppBundle\Services\Sync;

use AppBundle\Client\FloristClient;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Exceptions\FloristSyncException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractFloristSync
 */
abstract class AbstractFloristSync
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var FloristClient
     */
    protected $floristClient;

    /**
     * AbstractFloristSync constructor.
     * @param FloristClient              $floristClient
     * @param EntityManagerInterface     $entityManager
     */
    public function __construct(
        FloristClient $floristClient,
        EntityManagerInterface $entityManager
    ){
        $this->floristClient = $floristClient;
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $data
     * @return string
     * @throws FloristSyncException
     */
    protected function determineTwinfieldRelationNumber(object $data): string
    {
        switch ($data->country) {
            case 'NL':
                $prefix = 'BD';
                break;
            case 'BE':
            case 'LU':
                $prefix = 'BX';
                break;
            default:
                throw new FloristSyncException(sprintf("Country '%s' not expected", $data->country));
        }

        return $prefix . str_pad($data->company_florist_id, 7, '0', STR_PAD_LEFT);
    }

    protected function getFloristSupplierGroup(): SupplierGroup
    {
        $supplierGroup = $this->entityManager->getRepository(SupplierGroup::class)->findOneBy([
            'name' => 'Bloemisten',
        ]);

        if (null === $supplierGroup) {
            throw new FloristSyncException("Supplier group for florists not found");
        }

        return $supplierGroup;
    }
}
