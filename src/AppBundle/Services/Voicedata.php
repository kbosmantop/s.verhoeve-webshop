<?php

namespace AppBundle\Services;

use AppBundle\ThirdParty\Firebase\Entity\FirebaseToken;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\Common\Collections\ArrayCollection;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use Sabre\Xml as Sabre;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Voicedata
 * @package AppBundle\Services
 */
class Voicedata
{
    use ContainerAwareTrait;

    public const KEY = 'QU^MMWmR4I5KusYmtAo7NQJxejHRqKr9';

    public const FIREBASE_URL = 'https://android.googleapis.com/gcm/send';

    /**
     * @param string $xml
     * @throws NumberParseException
     */
    public function processXml($xml)
    {
        try {
            $data = new \SimpleXMLElement($xml);
        } catch (\Exception $e) {
            return;
        }

        $calledNumber = $data->xpath('/message/messagebody/did')[0];
        $callerId = $data->xpath('/message/messagebody/clip')[0];
        $calleeId = $data->xpath('/message/messagebody/extension')[0];

        if ($callerId == 'anonymous') {
            $callerId = null;
        }

        if ($calleeId) {
            if (\strlen($callerId) <= 4) {
                return;
            }

            $site = $this->getSite($calledNumber);

            if (!$site) {
                return;
            }

            $this->sendNotification($callerId, $calleeId, $site);
        }
    }

    /**
     * @param $calledNumber
     * @return Site|null
     * @throws NumberParseException
     */
    private function getSite($calledNumber)
    {
        $em = $this->container->get('doctrine')->getManager();

        $site = $em->getRepository(Site::class)->findOneBy([
            'phoneNumber' => PhoneNumberUtil::getInstance()->parse($calledNumber, 'NL'),
        ]);

        return $site;
    }

    /**
     * @param      $callerId
     * @param      $calleeId
     * @param Site $site
     */
    private function sendNotification($callerId, $calleeId, Site $site)
    {
        $tokens = $this->getTokens($calleeId);

        if (!$tokens) {
            return;
        }

        $fields = [
            'registration_ids' => $tokens,
            'data' => [
                'title' => 'Oproep van ' . ($callerId ?? 'anonieme beller'),
                'body' => 'Klik hier om ROBIN te openen…',
                'target' => $this->getTarget($callerId),
                'icon' => '/bundles/app/img/theme/' . $site->getTheme() . '/apple-touch-icon-76x76.png',
            ],
        ];

        $guzzleClient = $this->container->get('eight_points_guzzle.client.default');

        $guzzleClient->post(self::FIREBASE_URL, [
            'headers' => [
                'Authorization' => 'key=' . $this->container->getParameter('firebase_token'),
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode($fields),
        ]);
    }

    /**
     * @param $calleeId string
     * @return array
     */
    private function getTokens($calleeId)
    {
        $em = $this->container->get('doctrine')->getManager();

        $users = $em->getRepository(User::class)->findBy([
            'voipExtension' => $calleeId,
        ]);

        $tokens = new ArrayCollection($em->getRepository(FirebaseToken::class)->findBy([
            'user' => $users,
        ]));

        return $tokens->map(function (FirebaseToken $firebaseToken) {
            return $firebaseToken->getToken();
        })->toArray();
    }

    /**
     * @param $callerId string
     * @return string
     */
    private function getTarget($callerId)
    {
        $url = sprintf('https://app.robinhq.com/?startcode=searchphonenumber&startdata=%s', $callerId);

        return $this->encrypt(self::KEY, $url);
    }

    /**
     * @param $passphrase string
     * @param $value string
     * @return string
     */
    private function encrypt($passphrase, $value)
    {
        $salt = openssl_random_pseudo_bytes(8);

        $salted = '';
        $dx = '';

        while (\strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }

        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);

        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);

        $data = ['ct' => base64_encode($encrypted_data), 'iv' => bin2hex($iv), 's' => bin2hex($salt)];

        return json_encode($data);
    }

    /**
     * @param $ext
     * @param $number
     * @return bool
     * @throws \Exception
     */
    public function outgoingCall($ext, $number)
    {
        $response = $this->container->get('eight_points_guzzle.client.voicedata_api')->post('/xml/mvapi', [
            'body' => $this->generateOutgoingCallXML($ext, $number)
        ]);

        $xml = new \SimpleXMLElement($response->getBody()->getContents());

        if (((int) $xml->xpath('//message/messageheader/errorcode')[0]) > 0) {
            throw new \RuntimeException($xml->xpath('//message/messageheader/errorcodedescription')[0], 500);
        }

        return true;
    }

    /**
     * @param $extension
     * @param $number
     * @return string
     * @throws \Exception
     */
    private function generateOutgoingCallXML($extension, $number): string
    {
        $parameters = $this->container->get('app.parameter')->setEntity();

        if ($parameters->getValue('voicedata_api_account_id') === null) {
            throw new \RuntimeException('Voicedata API Account ID must be set');
        }

        if ($parameters->getValue('voicedata_api_password') === null) {
            throw new \RuntimeException('Voicedata API Password must be set');
        }

        $xml = new Sabre\Service();

        $xml = $xml->write('message', function (Sabre\Writer $writer) use ($extension, $number, $parameters) {
            $writer->writeElement('messageheader', function () use ($writer) {
                $writer->writeElement('debug', 'false');
                $writer->writeElement('msgtype', 'originate');
                $writer->writeElement('msgversion', '1.0');
                $writer->writeElement('msgidentifier', time());
                $writer->writeElement('errorcode', 0);
                $writer->writeElement('errorcodedescription', 'ok');
                $writer->writeElement('msgdatetime', date("Y-m-d\Th:i:s"));
            });

            $writer->writeElement('messagebody', function () use ($writer, $extension, $number, $parameters) {
                $writer->writeElement('accountid', $parameters->getValue('voicedata_api_account_id'));
                $writer->writeElement('xmlpass', $parameters->getValue('voicedata_api_password'));
                $writer->writeElement('extension', $extension);
                $writer->writeElement('destination', $this->sanitizeNumber($number));
            });
        });

        $xml = str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="utf-8"?>', $xml);

        return $xml;
    }

    /**
     * @param string $number
     * @return string
     */
    public function sanitizeNumber(string $number): string
    {
        $number = str_replace('+', '00', $number);
        $number = preg_replace('/\D/', '', $number);

        return $number;
    }

}
