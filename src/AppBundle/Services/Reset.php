<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Manager\Supplier\DeliveryAreaManager;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use League\Flysystem\MountManager;
use RuntimeException;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Class Reset
 * @package AppBundle\Services
 */
class Reset
{
    /** @var InputInterface */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var bool $verbose
     */
    private $verbose = false;

    /**
     * @var bool
     */
    private $execFrontend = false;

    /**
     * @var bool
     */
    private $execPostcodes = false;

    /**
     * @var bool
     */
    private $execJobs = false;

    /**
     * @var DeliveryAreaManager
     */
    private $deliveryAreaManager;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var MountManager
     */
    private $mountManager;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Reset constructor.
     * @param ParameterBagInterface $parameterBag
     * @param EventDispatcherInterface $eventDispatcher
     * @param Registry $doctrine
     * @param MountManager $mountManager
     * @param Filesystem $filesystem
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        EventDispatcherInterface $eventDispatcher,
        Registry $doctrine,
        MountManager $mountManager,
        Filesystem $filesystem
    )
    {
        $this->parameterBag = $parameterBag;
        $this->eventDispatcher = $eventDispatcher;
        $this->doctrine = $doctrine;
        $this->mountManager = $mountManager;
        $this->filesystem = $filesystem;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @param QuestionHelper $helper
     * @return Reset
     */
    public function init(InputInterface $input, OutputInterface $output, QuestionHelper $helper): Reset
    {
        $this->input = $input;
        $this->output = $output;

        $this->io = new SymfonyStyle($input, $output);
        $this->verbose = $input->getOption('verbose');

        $question = new ConfirmationQuestion('Execute `Frontend` commands y/n (default: false)?', false);

        if ($answer = $helper->ask($input, $output, $question)) {
            $this->execFrontend = $answer;
        }

        $question = new ConfirmationQuestion('Execute `Postcode` commands y/n (default: false)?', false);

        if ($answer = $helper->ask($input, $output, $question)) {
            $this->execPostcodes = $answer;
        }

        $question = new ConfirmationQuestion('Execute `Jobs` commands y/n (default: false)?', false);

        if ($answer = $helper->ask($input, $output, $question)) {
            $this->execJobs = $answer;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function listMethods()
    {
        if ($this->input->hasOption('list-methods')) {
            $this->io->text('list methods');
            $this->io->listing($this->getAllMethods());

            return true;
        }

        return false;
    }

    /**
     * @param bool $verbose
     */
    public function setVerbose($verbose = false): void
    {
        $this->verbose = $verbose;
    }

    /**
     * @return string
     */
    private function getVerboseArgument(): string
    {
        if ($this->verbose) {
            return ' -vvv';
        }

        return ' --quiet';
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function execute(): bool
    {
        $environment = $this->parameterBag->get('kernel.environment');

        if ($environment === 'prod') {
            throw new RuntimeException('The reset command cant be run in production');
        }

        $excludedDispatchListeners = [];

        try {
            foreach ($this->eventDispatcher->getListeners() as $listenerName => $listener) {
                if (false !== stripos($listenerName, 'solr')) {
                    $excludedDispatchListeners[$listenerName] = $listener;
                    $this->eventDispatcher->removeListener($listenerName, $listener);
                }
            }
            $this->setTextMessage(sprintf('Event listeners (%s) are disabled', implode(', ', array_keys($excludedDispatchListeners))));
        } catch (Exception $e) {
            $this->setTextMessage('Exception while disableing event listeners: ' . $e->getMessage());
            //Do not remove listeners, just continue
        }

        $this->dropDatabase();
        $this->createDatabase();
        $this->runDatabaseCommands();

        try {
            foreach ($excludedDispatchListeners as $listenerName => $listener) {
                $this->eventDispatcher->addListener($listenerName, $listener);
            }
            $this->setTextMessage('Event listeners are enabled');
        } catch (Exception $e) {
            $this->setTextMessage('Exception while enableing event listeners: ' . $e->getMessage());
            //Do not remove listeners, just continue
        }

        $this->runCommands();
        $this->createSupplierDeliveryArea();
        $this->createUserRoles();
        $this->executeJobs();
        $this->postReset();

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function executeJobs(): bool
    {
        if (!$this->execJobs) {
            return false;
        }

        $this->setSection('Execute jobs');
        $this->setTextMessage(' -> run jobs');

        return $this->runCommand('php bin/console jms-job-queue:run --idle-time=1 --max-runtime=15');
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function postReset(): bool
    {
        $this->setSection('Post Reset');
        $this->setTextMessage(' -> clear cache');

        return $this->runCommand('php bin/console cache:clear');
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function dropDatabase(): bool
    {
        $this->setTextMessage('Drop database');

        $connection = $this->doctrine->getConnection();
        $dbName = $this->parameterBag->get('database_name');

        try {
            $connection->executeQuery(sprintf('DROP DATABASE %s', $dbName));
            $this->setSuccessMessage('Done');

        } catch (Exception $e) {
            $this->setErrorMessage('Error: ' . $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createDatabase(): bool
    {
        $this->setSection('create database');

        $connection = $this->doctrine->getConnection();
        $dbName = $this->parameterBag->get('database_name');

        try {
            $connection->executeQuery(sprintf('CREATE DATABASE %s', $dbName));
            $connection->executeQuery(sprintf('USE %s', $dbName));

            $this->setSuccessMessage('Done');
        } catch (Exception $e) {
            $this->setErrorMessage('Error: ' . $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param string $cmd
     * @param bool $useVerbose
     * @param bool $disableJobs
     * @return bool
     * @throws Exception
     */
    private function runCommand(string $cmd, bool $useVerbose = true, bool $disableJobs = false): bool
    {
        $this->setTextMessage(sprintf(' -> Run command `%s`', $cmd));

        $cmd = trim(sprintf('%s %s', $cmd, $useVerbose ? $this->getVerboseArgument() : ''));

        $process = new Process($cmd);
        $process->setTimeout(null);
        $process->setEnv([
            'DISABLEJOBS' => $disableJobs ? '1' : '0',
        ]);
        $process->run();

        if (!$process->isSuccessful()) {
            $this->setErrorMessage(sprintf('Error code: %s, %s', $process->getExitCode(), $process->getErrorOutput()));
        }

        $this->setSuccessMessage('Done');

        return true;
    }

    /**
     * @throws Exception
     */
    private function runDatabaseCommands(): void
    {
        $this->setSection('Run commands');

        $this->setSection('General');
        $this->runCommand('php bin/console doctrine:schema:create');
        $this->runCommand('php bin/console topgeschenken:entities:generate');
        $this->runCommand('php bin/console hautelook:fixtures:load --append', true, true);
        $this->setProductImages();
        $this->runCommand('php bin/console holiday:generate');
        $this->runCommand('php bin/console app:roles:generate');
    }

    /**
     * @throws Exception
     */
    private function runCommands(): void
    {
        $this->runCommand('php bin/console solr:index:populate');

        $this->setSection('Doctrine cache');
        $this->setTextMessage(' -> Clears all metadata cache');
        $this->runCommand('php bin/console doctrine:cache:clear-metadata');
        $this->runCommand('php bin/console doctrine:cache:clear-query');

        $this->setSection('Redis cache');
        $this->setTextMessage(' -> Clears redis cache');
        $this->runCommand('php bin/console redis:flushall');

        $this->setSection('Assets');
        $this->setTextMessage(' -> Assets install / dump');
        $this->runCommand('php bin/console assets:install --symlink');
        $this->runCommand('php bin/console assetic:dump');

        if ($this->execFrontend) {
            $this->setSection('Frontend');
            $this->setTextMessage(' -> yarn install / build');
            $this->runCommand('yarn install', false);
            $this->runCommand('yarn build-dev', false);
        }

        if ($this->execPostcodes) {
            $this->setSection('Regions');
            $this->setTextMessage(' -> Load postcodes / regions');
            $this->runCommand('php bin/console app:geography:update-region-data');
            // todo: shapes files inladen
        }
    }

    /**
     * @param string $name
     * @return $this
     */
    private function setSection($name = ''): self
    {
        if ($this->io) {
            $this->io->section($name);
        }

        return $this;
    }

    /**
     * @param string $text
     * @return void
     */
    private function setTextMessage($text = ''): void
    {
        if ($this->io) {
            $this->io->text(sprintf($text . ' at %s', $this->getTime()));
        }
    }

    /**
     * @return string
     */
    private function getTime(): string
    {
        return (new DateTime())->format('H:i:s');
    }

    /**
     * @param string $text
     * @return void
     */
    private function setSuccessMessage($text = ''): void
    {
        if ($this->io) {
            $this->io->success(sprintf('%s at %s', $text, $this->getTime()));
        }
    }

    /**
     * @param string $text
     * @throws Exception
     */
    private function setErrorMessage($text = ''): void
    {
        if ($this->io) {
            $this->io->error($text);
        }

        throw new RuntimeException('Reset killed');
    }

    /**
     * @return $this
     */
    public function createUserRoles()
    {
        $this->setTextMessage(' -> Insert user roles');

        $em = $this->doctrine->getManager();

        $users = $em->getRepository(User::class)->findAll();
        $roles = $em->getRepository(Role::class)->findAll();

        /** @var User $user */
        foreach ($users as $user) {
            if (\in_array('ROLE_SUPER_ADMIN', $user->getRoles(), true)) {
                continue;
            }

            /** @var Role $role */
            foreach ($roles as $role) {
                if ($role->getName() === 'ROLE_SUPER_ADMIN') {
                    continue;
                }

                $role->addUser($user);
                $user->addRole($role);

                $em->persist($role);
            }
        }

        $em->flush();

        $this->setSuccessMessage('Done');

        return $this;
    }

    /**
     * todo: Rewrite for test purposes
     *
     * @return bool
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createSupplierDeliveryArea(): bool
    {
        if ($this->execPostcodes === false) {
            return false;
        }
        $this->setTextMessage(' -> insert supplier delivery area\'s');

        $postcodes = $this->doctrine->getRepository(Postcode::class)->findAll();

        if (\count($postcodes) === 0) {
            $this->setErrorMessage("Error: Postcodes ain't inserted");
            return false;
        }

        /** @var Company[] $companies */
        $companies = $this->doctrine
            ->getRepository(Company::class)
            ->findAllSuppliers();

        if (!$companies) {
            $this->setErrorMessage("Error: Companies ain't inserted");
            return false;
        }

        /** @var Postcode[] $zipcodes */
        $zipcodes = $this->doctrine->getRepository(Postcode::class)->findAll();

        if (!$zipcodes) {
            $this->setErrorMessage("Error: Zipcodes ain't inserted");
            return false;
        }

        $deliveryAreaCollection = new ArrayCollection();

        foreach ($companies as $company) {
            if (null === $company->getCompanyRegistration()) {
                continue;
            }

            // Wat is het juiste adres om op te checken...
            $mainAddress = $company->getCompanyRegistration()->getMainAddress();

            if (null === $mainAddress) {
                continue;
            }

            foreach ($zipcodes as $zipcode) {
                $postcodeNumbers = substr($mainAddress->getPostcode(), 0, 3);
                $postcodeNumbersCheck = substr($zipcode->getPostcode(), 0, 3);

                if ($postcodeNumbers !== $postcodeNumbersCheck) {
                    continue;
                }

                $deliveryArea = new DeliveryArea();
                $deliveryArea->setCompany($company);
                $deliveryArea->setPostcode($zipcode);

                $deliveryArea->setDeliveryPrice(10 / 1.09);
                $deliveryArea->setDeliveryInterval('P1DT7H');

                $deliveryAreaCollection->add($deliveryArea);
            }
        }

        foreach ($deliveryAreaCollection as $deliveryArea) {
            $this->deliveryAreaManager->create($deliveryArea);
        }

        $this->doctrine->getManager()->flush();

        return true;
    }

    /**
     * @param string $method
     * @return bool
     * @throws Exception
     */
    public function executeMethod(string $method): bool
    {
        // Check if option arg is valid
        if (method_exists($this, $method)) {
            $this->{$method}();

            return true;
        }

        $this->setErrorMessage('Method does\'t exist');
        return false;
    }

    /**
     * @return array
     */
    public function getAllMethods(): array
    {
        return [
            'dropDatabase',
            'createDatabase',
            'runCommands',
            'callServices',
            'createUserRoles',
            'createSupplierDeliveryArea',
            'executeJobs',
        ];
    }

    private function setProductImages(): void
    {
        $em = $this->doctrine->getManager();

        $mount = $this->mountManager->getFilesystem('filesystem_public');

        $slugs = [
            'fruitmand-basis',
            'fruitmand-jongen',
            'feestelijke-slagroomtaart',
            'feestelijke-slagroomtaart-met-opdruk',
            'boeket-sanne-standaard',
            'boeket-sanne-groot',
            'boeket-sanne-xl',
            'boeket-joyce-standaard',
            'boeket-joyce-groot',
            'boeket-joyce-xl',
            'boeket-jaimy-standaard',
            'boeket-jaimy-groot',
            'boeket-jaimy-xl',
            'boeket-hanna-standaard',
            'boeket-hanna-groot',
            'boeket-hanna-xl',
            'glazen-vaas',
            'bloemencadeaubon',
            'polo',
            'boeket-hanna',
            'boeket-joyce',
            'leffe-pakket',
            'bolcadeaukaart',
            'guilhem-rouge',
            'additional-tijdschriften-vrouw',
        ];

        foreach ($slugs as $slug) {

            /** @var Product $product */
            try {
                $product = $em->getRepository(Product::class)->getBySlug($slug);
            } catch (NonUniqueResultException $e) {
            }

            if (!$product) {
                throw new RuntimeException(sprintf("Product with slug '%s' not found", $slug));
            }

            $path = 'src/AppBundle/Resources/fixtures/public/img/' . $slug . '.png';

            if (!$this->filesystem->exists($path)) {
                throw new FileNotFoundException(null, 0, null, 'public/img/' . $slug . '.png');
            }

            $url = 'images/product/' . $product->getId() . '/' . substr(md5($slug), 0, 13) . '.png';

            if (!$mount->has($url)) {
                $mount->putStream($url, fopen($path, 'rb'));
            }

            $productImage = new ProductImage();
            $productImage->setProduct($product);
            $productImage->setPath($url);
            $productImage->setMain(true);
            $productImage->setPosition(1);

            $em->persist($productImage);

        }
        $em->flush();
    }
}