<?php

namespace AppBundle\Services\Google\Shopping;

use AppBundle\ThirdParty\Google\Entity\Shopping\Taxonomy;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class TaxonomyImporter
{
    use ContainerAwareTrait;

    private $source = 'https://www.google.com/basepages/producttype/taxonomy-with-ids.nl-NL.txt';

    public function import()
    {
        $response = $this->container->get("eight_points_guzzle.client.default")->get($this->source);

        $this->processResult((string)$response->getBody());
    }

    /**
     * @param $result string
     */
    private function processResult($result)
    {
        $lines = explode(PHP_EOL, trim($result));

        array_shift($lines);

        foreach ($lines as $line) {
            $this->processLine($line);
        }
    }

    /**
     * @param $line string
     */
    private function processLine($line)
    {
        $id = (int)substr($line, 0, strpos($line, " - "));
        $productCategory = substr($line, strpos($line, " - ") + 3);

        $googleShoppingTaxonomy = $this->getDoctrine()->getManager()->find(Taxonomy::class, $id);

        if (!$googleShoppingTaxonomy) {
            $googleShoppingTaxonomy = new Taxonomy();
            $googleShoppingTaxonomy->setId($id);
            $googleShoppingTaxonomy->setProductCategory($productCategory);

            $this->getDoctrine()->getManager()->persist($googleShoppingTaxonomy);
        }

        $googleShoppingTaxonomy->setProductCategory($productCategory);

        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->container->get("doctrine");
    }
}
