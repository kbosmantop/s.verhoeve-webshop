<?php

namespace AppBundle\Services\Robin;

use AppBundle\Entity\Order\Order;
use AppBundle\Manager\Order\OrderLineManager;
use libphonenumber\PhoneNumberFormat;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

/**
 * Class OrderTransformer
 * @package AppBundle\Services\Robin
 */
class OrderTransformer
{
    public const EMPTY_KEY = '<div style="position: absolute; width: 15px; height: 15px; background-color: white; visibility: visible !important;"></div>';

    /** @var Order */
    private $order;
    /** @var Router */
    private $router;

    /** @var OrderLineManager  */
    private $orderLineManager;

    /**
     * OrderTransformer constructor.
     * @param Router $router
     */
    public function __construct(Router $router, OrderLineManager $orderLineManager)
    {
        $this->router = $router;
        $this->orderLineManager = $orderLineManager;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return \stdClass
     */
    public function transform(): \stdClass
    {
        $dateFormatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::MEDIUM, \IntlDateFormatter::NONE);
        $timeFormatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::NONE, \IntlDateFormatter::SHORT);

        $lines = [];

        foreach ($this->order->getLines() as $line) {
            if ($line->getParent()) {
                continue;
            }

            $lines[] = (object)[
                'Product' => $line->getTitle(),
                'Aantal' => (!$line->getProduct() || $line->getProduct()->getPurchasable()) ? $line->getQuantity() : '',
                'Prijs' => '€ ' . number_format($this->orderLineManager->getTotalPriceIncl($line), 2, ',', '.'),
            ];

            if ($line->getDiscountAmount()) {
                $lines[] = (object)[
                    'Product' => '<em> ∟ Korting</em>',
                    'Aantal' => '',
                    'Prijs' => '<em>€ ' . number_format($line->getDiscountAmount(), 2, ',', '.') . '</em>',
                ];
            }

            foreach ($line->getChildren() as $child) {
                $lines[] = (object)[
                    'Product' => ' ∟ ' . $child->getTitle(),
                    'Aantal' => $child->getQuantity(),
                    'Prijs' => '€ ' . number_format($this->orderLineManager->getTotalPriceIncl($child), 2, ',', '.'),
                ];
            }
        }

        $lines[] = (object)[
            'Product' => '<strong>Totaal</strong>',
            'Aantal' => '',
            'Prijs' => '<strong>€ ' . number_format($this->order->getTotalPrice(), 2, ',', '.') . '</strong>',
        ];

//        $lines[] = (object) [
//            "Product" => "Waarvan BTW",
//            "Aantal" => "" ,
//            "Prijs" => "€ ". number_format($this->order->getTotalPriceIncl() - $this->order->getTotalPrice(), 2, ",", "."),
//        ];

        $relatedOrders = [];

        if ($this->order->getOrderCollection()->getOrders()->count() > 1) {
            foreach ($this->order->getOrderCollection()->getOrders() as $relatedOrder) {
                /**
                 * @var $relatedOrder Order
                 */
                if ($relatedOrder === $this->order) {
                    continue;
                }

                $relatedOrders[] = (object)[
                    'Ordernummer' => $relatedOrder->getOrderNumber(),
                    '<div style="white-space: nowrap; margin-left: -30px; position: absolute; width: 84px;">Bez. dat.</div>' => '<div style="white-space: nowrap; margin-left: -30px; position: absolute; width: 84px;">' . $dateFormatter->format($relatedOrder->getDeliveryDate()) . '</div>',
                    'Bedrag' => '€ ' . number_format($relatedOrder->getTotalPriceIncl(), 2, ',', '.'),
                ];
            }

            $relatedOrdersObject = (object)[
                'display_as' => 'columns',
                'caption' => 'Gerelateerde orders',
                'data' => $relatedOrders,
            ];
        } else {
            $relatedOrdersObject = (object)[
                'display_as' => 'rows',
                'caption' => 'Gerelateerde orders',
                'data' => [
                    (object)[
                        '<div style="position: absolute;">Geen gerelateerde orders</div>' => '',
                    ],
                ],
            ];
        }

        $payments = [];

        foreach ($this->order->getOrderCollection()->getPayments() as $payment) {
            switch ($payment->getStatus()->getId()) {
                case 'authorized':
                    $icon = '<div style="position: absolute;margin-left: 178px; margin-top: -2px; font-size: 24px; font-weight: bold; color: limegreen;">&#9679;</div>';
                    break;
                case 'refunded':
                    $icon = '<div style="position: absolute;margin-left: 178px; margin-top: -2px; font-size: 24px; font-weight: bold; color: dimgrey;">&#9679;</div>';
                    break;
                case 'cancelled':
                case 'failed':
                case 'pending':
                case 'refused':
                default:
                    $icon = '<div style="position: absolute;margin-left: 178px; margin-top: -2px; font-size: 24px; font-weight: bold; color: red;">&#9679;</div>';
                    break;
            }

            $payments[] = (object)[
                '€ ' . number_format($payment->getAmount(), 2, ',', '.') =>
                    $icon .
                    $dateFormatter->format($payment->getDatetime()) . ' om ' . $timeFormatter->format($payment->getDatetime())
                ,
                self::EMPTY_KEY => $payment->getPaymentmethod()->translate()->getDescription(),
            ];
        }

        $url = $this->router->generate('admin_sales_order_index', [],
            UrlGeneratorInterface::ABSOLUTE_URL);

        $orders = (object)[
            'orders' => [
                (object)[
                    'order_number' => $this->order->getOrderNumber(),
                    'email_address' => $this->order->getOrderCollection()->getCustomer()->getEmail(),
                    'url' => $url . '?ordernummer=' . $this->order->getOrderNumber(),
                    'order_by_date' => $this->order->getCreatedAt()->format('c'),
                    'revenue' => $this->order->getTotalPriceIncl(),
                    'list_view' => (object)[
                        'Site' => $this->order->getOrderCollection()->getSite()->translate()->getDescription(),
                        'Ordernummer' => $this->order->getOrderNumber(),
                        'Orderstatus' => $this->order->getStatusDescription(),
                        'Bezorgdatum' => $dateFormatter->format($this->order->getDeliveryDate()),
                    ],
                    'details_view' => [
                        (object)[
                            'display_as' => 'details',
                            'data' => (object)[
                                'Site' => $this->order->getOrderCollection()->getSite()->translate()->getDescription(),
                                'Bezorgdatum' => $dateFormatter->format($this->order->getDeliveryDate()),
                                'Orderstatus' => $this->order->getStatusDescription(),
                                'Bezorgstatus' => '<i>Niet bekend</i>',
                                'Besteld op' => $dateFormatter->format($this->order->getCreatedAt()) . ' om ' . $timeFormatter->format($this->order->getCreatedAt()),
                                'Leverancier' => $this->getSupplierData(),
                                'Ontvanger' => $this->order->getDeliveryAddressAttn() . ($this->order->getDeliveryAddressCompanyName() ? '(' . $this->order->getDeliveryAddressCompanyName() . ')' : ''),
                                'Bezorgadres' =>
                                    $this->order->getDeliveryAddressStreetAndNumber() . ' ' .
                                    $this->order->getDeliveryAddressPostcode() . ' ' . $this->order->getDeliveryAddressCity() . '',
                                '' .
                                $this->order->getDeliveryAddressCountry()->translate('nl_NL')->getName(),
                                '<div style="height: 30px;">
                                    <div class="panel_button" style="position: absolute;"><div><a target="robin" href="' . $url . '">Bewerk order</a></div></div>
                                </div>
                                <script type="text/javascript">
                                    $(".panel.button").addClass("panel"+ " button").removeClass("panel").removeClass("button");
                                </script>' => '',
                            ],
                        ],
                        (object)[
                            'display_as' => 'columns',
                            'caption' => 'Producten',
                            'data' => $lines,
                        ],
                        $relatedOrdersObject,
                        (object)[
                            'display_as' => 'rows',
                            'caption' => 'Betalingen',
                            'data' => $payments,
                        ],
                    ],
                ],
            ],
        ];

        return $orders;
    }

    /**
     * @return null|string
     */
    private function getSupplierData(): ?string
    {
        if ($this->order->getSupplierOrder() === null) {
            return null;
        }

        $supplier = $this->order->getSupplierOrder()->getSupplier();

        if (!$supplier) {
            return null;
        }

        $data = $supplier->getName() . '<br />' . $supplier->getEmail() . $supplier->getFormattedPhoneNumber(PhoneNumberFormat::E164) . '<br />';

        return $data;
    }
}
