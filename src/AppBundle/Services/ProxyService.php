<?php

namespace AppBundle\Services;

use AppBundle\Client\DefaultClient;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class ProxyService
 * @package AppBundle\Services
 */
class ProxyService
{
    /**
     * @var DefaultClient
     */
    private $defaultClient;

    /**
     * ProxyService constructor.
     * @param DefaultClient $defaultClient
     */
    public function __construct(DefaultClient $defaultClient)
    {
        $this->defaultClient = $defaultClient;
    }

    /**
     * @param $url
     * @return StreamedResponse
     */
    public function process($url)
    {
        if (ob_get_level()) {
            ob_end_clean();
            ob_implicit_flush(true);
        }

        $streamedResponse = new StreamedResponse();
        $streamedResponse->setCallback(function () use ($url) {
            try {
                $response = $this->defaultClient->get($url, [
                    'on_headers' => static function (ResponseInterface $response) {
                        foreach ($response->getHeaders() as $name => $value) {
                            if (strpos(current($value), 'attachment') !== false) {
                                continue;
                            }

                            if($name !== 'Location') {
                                header($name . ':' . current($value));
                            }
                        }
                    },
                ]);

                $body = $response->getBody();
                $body->seek(0);
                while (!$body->eof()) {
                    print $body->read(1024);
                    flush();
                }
            } catch (ClientException $e) {
                throw $e;
            }
        });

        return $streamedResponse;
    }
}
