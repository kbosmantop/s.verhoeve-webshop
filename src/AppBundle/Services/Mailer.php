<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Site\Domain;
use AppBundle\Services\Mailer\AbstractMailer;

/**
 * Class Mailer
 * @package AppBundle\Services
 * @deprecated
 */
class Mailer extends AbstractMailer
{
    /**
     * @var Domain
     */
    protected $domain;

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function sendTestMail($parameters, $callback = null)
    {
        $resolver = $this->getOptionsResolver();
        $resolver->setRequired([
            'to',
        ]);

        $parameters = $resolver->resolve($parameters);

        return $this->sendEmailMessage('AppBundle:Emails:test.html.twig', $parameters, $callback);
    }

    /**
     * @deprecated Functionality moved to OrderConfirmationMailer
     *
     * @param OrderCollection $orderCollection
     * @param callable|null   $callback
     *
     * @throws \Exception
     */
    public function sendOrderConfirmation(OrderCollection $orderCollection, callable $callback = null): void
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use OrderConfirmationMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @deprecated Functionality moved to InvoiceMailer
     *
     * @param               $parameters
     * @param callable|null $callback
     *
     * @throws \Exception
     */
    public function sendInvoice($parameters, callable $callback = null): void
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use InvoiceMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return void
     */
    public function sendRegistrationMail($parameters, $callback = null): void
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use RegistrationMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return void
     */
    public function sendResetPasswordMail($parameters, $callback = null): void
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use PasswordResetMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return void
     */
    public function sendCompanyChoosePasswordMail($parameters, $callback = null): void
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use CompanyChoosePasswordMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @deprecated Functionality moved to CompanyReportCustomerMailer
     *
     * @param      $parameters
     * @param null $callback
     * @return void
     */
    public function sendCompanyReportCustomerEmail($parameters, $callback = null)
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use CompanyReportCustomerMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @deprecated Functionality moved to PaymentRequestMailer
     *
     * @param array $parameters
     * @param null  $callback
     * @return bool
     * @throws \Exception
     */
    public function sendPaymentRequestMail($parameters, $callback = null): bool
    {
        trigger_error('The ' . __CLASS__ . '::' . __METHOD__ . ' is deprecated, use PaymentRequestMailer::send instead.',
            E_USER_DEPRECATED);
    }

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return bool
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        return false;
    }
}
