<?php

namespace AppBundle\Services\Designer;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Exceptions\Designer\DesignGeneratorException;
use AppBundle\Services\Designer;
use Exception;
use stdClass;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignGeneratorService
 * @package AppBundle\Services\Designer
 */
class DesignGeneratorService
{
    private const BASE_SCALE_SIZE = 30;

    /**
     * @var null|string
     */
    private $json;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * DesignGeneratorService constructor.
     *
     * @param Designer $designer
     *
     * @throws Exception
     */
    public function __construct(Designer $designer)
    {
        $this->designer = $designer;
    }

    /**
     * @param Personalization $personalization
     *
     * @throws Exception
     */
    public function setPersonalization(Personalization $personalization): void
    {
        $this->designer->setPersonalization($personalization);
    }

    /**
     * @return Personalization|null
     */
    public function getPersonalization(): ?Personalization
    {
        return $this->designer->getPersonalization();
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->designer->setUuid($uuid);
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->designer->getUuid();
    }

    /**
     * @return null|string
     */
    public function getJson(): ?string
    {
        return $this->json;
    }

    /**
     * @param array $objects
     *
     * @return bool
     * @throws DesignGeneratorException
     * @throws Exception
     */
    public function generate(array $objects = []): bool
    {
        $personalization = $this->getPersonalization();

        if(null === $personalization) {
            throw new DesignGeneratorException('Required personalization not found.');
        }

        try {
            $canvasWidth = $personalization->findProductProperty('design.width')->getValue();
            $canvasHeight = $personalization->findProductProperty('design.height')->getValue();
        } catch (Exception $exception) {
            throw new DesignGeneratorException('Incorrect personalization dimensions');
        }

        $canvasRealWidth = $this->designer->convertCmToPx($canvasWidth);
        $canvasRealHeight = $this->designer->convertCmToPx($canvasHeight);

        $objectList = [];
        foreach ($objects as $type => $properties) {
            $properties['left'] = round(($canvasRealWidth / 100) * $properties['left'], 3);
            $properties['top'] = round(($canvasRealHeight / 100) * $properties['top'], 3);

            if ($type === 'text') {
                $scaleX = round($canvasWidth / self::BASE_SCALE_SIZE, 3);
                $scaleY = round($canvasWidth / self::BASE_SCALE_SIZE, 3);

                if (isset($properties['scaleX'])) {
                    $scaleX = (int)$properties['scaleX'];
                }

                if (isset($properties['scaleY'])) {
                    $scaleY = (int)$properties['scaleY'];
                }

                $properties['scaleX'] = $scaleX;
                $properties['scaleY'] = $scaleY;
                $properties['width'] = round(($canvasRealWidth / 100) * $properties['width'], 3);
                $properties['height'] = round(($canvasRealWidth / 100) * $properties['height'], 3);
            }

            if ($type === 'image') {
                if (isset($properties['src'])) {
                    $filename = $this->downloadAndMoveImageToObjectStore($this->designer->getUuid(),
                        $properties['src']);

                    $properties['src'] = $this->designer->getObjectStoreUrl($filename);
                } else {
                    throw new DesignGeneratorException('Fabric image object required an image url');
                }

                $maxWidth = round(($canvasRealWidth / 100) * $properties['width'], 3);
                $maxHeight = round(($canvasRealWidth / 100) * $properties['height'], 3);

                [$imageWidth, $imageHeight] = $this->calcImageSize($properties['src'], $maxWidth, $maxHeight);

                $properties['width'] = $imageWidth;
                $properties['height'] = $imageHeight;
            }

            $obj = $this->generateFabricObject($type, $properties);

            $objectList[] = $obj;
        }

        $jsonObject = new stdClass();
        $jsonObject->objects = $objectList;
        $jsonObject->background = 'white';

        $oldDimensions = new stdClass();
        $oldDimensions->type = 'rectangle';
        $oldDimensions->width = $canvasWidth;
        $oldDimensions->height = $canvasHeight;

        $jsonObject->oldDimensions = $oldDimensions;

        $this->json = json_encode($jsonObject);

        return $this->designer->moveFileDataToObjectStore($this->json, 'design.json');
    }

    /**
     * @param string $type
     * @param array $properties
     *
     * @return array
     * @throws DesignGeneratorException
     */
    private function generateFabricObject(string $type, array $properties = []): array
    {
        if (!in_array($type, ['text', 'image'], true)) {
            throw new DesignGeneratorException('Invalid FarbicJs object type');
        }

        return $this->resolveProperties($type, $properties);
    }

    /**
     * @param string $type
     * @param array $properties
     *
     * @return array
     */
    private function resolveProperties(string $type, array $properties = []): array
    {
        $resolver = new OptionsResolver();

        $defaults = [
            'type'                     => $type,
            'originX'                  => 'center',
            'originY'                  => 'center',
            'scaleX'                   => '1',
            'scaleY'                   => '1',
            'flipX'                    => false,
            'flipY'                    => false,
            'lockMovementX'            => false,
            'lockMovementY'            => false,
            'lockScalingX'             => false,
            'lockScalingY'             => false,
            'skewX'                    => 0,
            'skewY'                    => 0,
            'alignX'                   => 'none',
            'alignY'                   => 'none',
            'meetOrSlice'              => 'meet',
            'crossOrigin'              => 'anonymous',
            'opacity'                  => 1,
            'visible'                  => true,
            'clipTo'                   => null,
            'backgroundColor'          => '',
            'fillRule'                 => 'nonzero',
            'globalCompositeOperation' => 'source-over',
            'transformMatrix'          => null,
            'shadow'                   => null,
            'angle'                    => 0,
            'fill'                     => 'rgb(0,0,0)',
            'stroke'                   => null,
            'strokeWidth'              => $type === 'text' ? 50 : 0,
            'strokeDashArray'          => null,
            'strokeLineCap'            => $type === 'text' ? 'strokeLineCap' : null,
            'strokeLineJoin'           => 'miter',
            'strokeMiterLimit'         => 10,
            'selectable'               => true,
            'lockRotation'             => false,
            'lockUniScaling'           => false,
            'hasBorders'               => true,
            'hasControls'              => true,
            'filters'                  => [],
            'resizeFilters'            => [],
        ];

        if ($type === 'text') {
            $defaults['fontFamily'] = 'Roboto';
            $defaults['fontSize'] = '200';
            $defaults['fontWeight'] = 'normal';
            $defaults['fontStyle'] = '';
            $defaults['textDecoration'] = '';
            $defaults['lineHeight'] = 1;
            $defaults['textAlign'] = 'center';
            $defaults['textBackgroundColor'] = '';
            $defaults['backgroundColor'] = 'rgba(0, 0, 0, .005)';
        }

        $resolver->setDefaults($defaults);

        $required = [
            'type',
            'originX',
            'originY',
            'left',
            'top',
            'width',
            'height',
            'class',
        ];

        switch ($type) {
            case 'text':
                $required[] = 'text';
                break;
            case 'image':
                $required[] = 'fileId';
                $required[] = 'src';
                break;
        }

        $resolver->setRequired($required);

        return $resolver->resolve($properties);
    }

    /**
     * @param        $uuid
     * @param string $imageUrl
     * @param bool $isUrlDesign
     *
     * @return string
     * @throws DesignGeneratorException
     */
    public function downloadAndMoveImageToObjectStore($uuid, $imageUrl, $isUrlDesign = false): string
    {
        if (!$data = file_get_contents($imageUrl)) {
            throw new DesignGeneratorException(sprintf("Can't download image (%s) data", $imageUrl));
        }

        $filename = sha1($imageUrl);
        $ext = pathinfo($imageUrl, PATHINFO_EXTENSION);

        if (!in_array($ext, Designer::ALLOWED_IMAGE_EXTENSIONS, true)) {
            throw new DesignGeneratorException(sprintf('Extension %s for personalisation not supported.',
                $ext));
        }

        $this->designer->setIsUrlDesign($isUrlDesign);
        $this->designer->setUuid($uuid);

        $filename .= '.' . $ext;

        if ($isUrlDesign) {
            $filename = 'print.' . $ext;
        }

        $this->designer->moveFileDataToObjectStore($data, $filename);

        return $filename;
    }

    /**
     * @param $imageUrl
     * @param $maxWidth
     * @param $maxHeight
     *
     * @return array
     */
    public function calcImageSize($imageUrl, $maxWidth, $maxHeight): array
    {
        [$imageWidth, $imageHeight] = getimagesize($imageUrl);

        if ($imageWidth >= $imageHeight) {
            $ratio = $maxWidth / $imageWidth;

            $imageWidth = $maxWidth;
            $imageHeight *= $ratio;
        } else {
            $ratio = $maxHeight / $imageHeight;

            $imageHeight = $maxHeight;
            $imageWidth *= $ratio;
        }

        if ($imageWidth > $maxWidth) {
            $ratio = $maxWidth / $imageWidth;

            $imageWidth = $maxWidth;
            $imageHeight *= $ratio;
        }

        if ($imageHeight > $maxHeight) {
            $ratio = $maxHeight / $imageHeight;

            $imageHeight = $maxHeight;
            $imageWidth *= $ratio;
        }

        return [$imageWidth, $imageHeight];
    }
}