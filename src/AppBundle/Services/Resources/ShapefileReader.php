<?php

namespace AppBundle\Services\Resources;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Geography\PostcodeGeometry;
use AppBundle\Exceptions\Resources\ShapefileException;
use CrEOF\Geo\WKB\Parser;
use CrEOF\Spatial\PHP\Types\Geometry\MultiPolygon;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Monolog\Logger;
use org\majkel\dbase\Record;
use org\majkel\dbase\Table;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ShapefileReader
 * @package AppBundle\Services\Resources
 */
class ShapefileReader
{
    private const FORMAT_SPOTZI = 'spotzi';
    private const FORMAT_GEODAN = 'geodan';

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var float
     */
    private static $X0 = 155E3;

    /**
     * @var float
     */
    private static $Y0 = 463E3;

    /**
     * @var float
     */
    private static $lat0 = 52.1551744;

    /**
     * @var float
     */
    private static $lng0 = 5.38720621;

    /**
     * @var string
     */
    private $format;

    /**
     * ShapefileReader constructor.
     * @param Registry $doctrine
     * @param Logger   $logger
     */
    public function __construct(
        Registry $doctrine,
        LoggerInterface $logger
    ) {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
    }

    /**
     * @param string  $filename
     * @param Country $country
     *
     * @throws \Exception
     */
    public function read($filename, Country $country)
    {
        $connection = $this->doctrine->getConnection();

        $connection->beginTransaction();

        $dbfFilename = './var/resources/shapefiles/' . $filename . '.dbf';
        $shpFilename = './var/resources/shapefiles/' . $filename . '.shp';

        $filesystem = new Filesystem();

        if (!$filesystem->exists($dbfFilename)) {
            throw new ShapefileException(sprintf("'%s' doesn't exist", $filename . '.dbf'));
        }

        if (!$filesystem->exists($shpFilename)) {
            throw new ShapefileException(sprintf("'%s' doesn't exist", $filename . '.shp'));
        }

        /** @var Record[] $dbfRecords */
        $dbfRecords = Table::fromFile($dbfFilename);

        $shpHandle = fopen($shpFilename, 'rb');

        $fileHeader = unpack('NFileCode/NUnused4/NUnused8/NUnused12/NUnused16/NUnused20/NFileLength/IVersion/IShapeType/dXmin/dYmin/dXmax/dYmax/dZmin/dZmax/dMmin/dMmax',
            fread($shpHandle, 100));

        if ($fileHeader['ShapeType'] != Parser::WKB_TYPE_MULTILINESTRING) {
            throw new ShapefileException(sprintf("Can't parse ShapeType '%s'", $fileHeader['ShapeType']));
        }

        $shpFileSize = filesize($shpFilename);

        $count = 0;

        while (!feof($shpHandle) && (ftell($shpHandle) + 8) < $shpFileSize) {
            if ($dbfRecords[$count]->offsetExists('postalcode')) {
                $this->format = self::FORMAT_SPOTZI;
            } elseif ($dbfRecords[$count]->offsetExists('PC4CODE')) {
                $this->format = self::FORMAT_GEODAN;
            } else {
                throw new \RuntimeException("Can't determine format");
            }

            switch ($this->format) {
                case self::FORMAT_SPOTZI:
                    $postcode = $this->getPostcode($country, $dbfRecords[$count]['postalcode']);
                    break;
                case self::FORMAT_GEODAN:
                    $postcode = $this->getPostcode($country, $dbfRecords[$count]['PC4CODE']);
                    break;
                default:
                    throw new \RuntimeException('Unkown format');
            }

            $recordHeader = unpack('NRecordNumber/NContentLength/IShapeType', fread($shpHandle, 12));

            switch ($recordHeader['ShapeType']) {
                case Parser::WKB_TYPE_MULTILINESTRING:
                    $multiPolygon = $this->readPolygon($shpHandle);

                    if ($postcode) {
                        $geometry = $postcode->getGeometry();

                        if (!$geometry) {
                            $geometry = new PostcodeGeometry();

                            $this->doctrine->getManager()->persist($geometry);

                            $postcode->setGeometry($geometry);
                        }

                        $geometry->setMultipolygon($multiPolygon);
                        $geometry->setPoint($this->centroid($multiPolygon));
                    }
                    break;
                default:
                    throw new ShapefileException(sprintf('Unsupported shapetype %d', $recordHeader['ShapeType']));
            }

            $count++;
        }

        $this->doctrine->getManager()->flush();
        $connection->commit();
    }

    /**
     * @param $shpHandle
     *
     * @return MultiPolygon
     */
    private function readPolygon($shpHandle)
    {
        $header = unpack('dXmin/dYmin/dXmax/dYmax/InumParts/InumPoints/', fread($shpHandle, 40));

        $parts = [];

        for ($i = 1; $i <= $header['numParts']; $i++) {
            $parts[] = unpack('IIndex', fread($shpHandle, 4))['Index'];
        }

        $points = [];

        for ($i = 1; $i <= $header['numPoints']; $i++) {
            $point = unpack('dX/dY', fread($shpHandle, 16));

            switch ($this->format) {
                case self::FORMAT_SPOTZI:
                    $points[] = [
                        $point['X'],
                        $point['Y']
                    ];
                    break;
                case self::FORMAT_GEODAN:
                    // GEODAN format bevat Rijksdriehoekscoördinaten, omrekenen naar GPS coördinaten
                    $points[] = [
                        $this->RD2lng($point['X'], $point['Y']),
                        $this->RD2lat($point['X'], $point['Y']),
                    ];
                    break;
                default:
                    throw new \RuntimeException('Unkown format');
            }
        }

        $rings = [];

        foreach ($parts as $i => $partIndex) {
            if (array_key_exists($i + 1, $parts)) {
                $nextIndex = $parts[$i + 1];
            } else {
                $nextIndex = count($points);
            }

            $list = array_slice($points, $partIndex, $nextIndex - $partIndex);

            $rings[] = $list;
        }

        return $this->createMultiPolygon($rings);
    }

    /**
     * @param array $rings
     *
     * @return MultiPolygon
     */
    private function createMultiPolygon(array $rings)
    {
        $multiPolygon = [];
        $polygon = [];

        foreach ($rings as $ring) {
            $polygon[] = $ring;
        }

        if ($polygon) {
            $multiPolygon[] = $polygon;
        }

        return new MultiPolygon($multiPolygon);
    }

    /**
     * @param MultiPolygon $multiPolygon
     *
     * @return Point
     */
    private function centroid(MultiPolygon $multiPolygon)
    {
        // This won't work with negative points
        $longitude = [];
        $latitude = [];

        foreach ($multiPolygon->getPolygons() as $polygon) {
            foreach ($polygon->getRing(0)->getPoints() as $point) {
                $longitude[] = $point->getLongitude();
                $latitude[] = $point->getLatitude();
            }
        }

        return new Point(
            array_sum($longitude) / count($longitude),
            array_sum($latitude) / count($latitude)
        );
    }

    /**
     * @param Country $country
     * @param string $postcode
     *
     * @return Postcode|null
     */
    private function getPostcode(Country $country, string $postcode)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->doctrine->getRepository(Postcode::class);

        $qb = $entityRepository->createQueryBuilder('postcode');
        $qb->leftJoin('postcode.province', 'province');
        $qb->leftJoin('province.country', 'country');
        $qb->andWhere('country = :country');
        $qb->setParameter('country', $country);
        $qb->andWhere('postcode.postcode = :number');
        $qb->setParameter('number', $postcode);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $exception) {
            $this->logger->notice(sprintf('Postcode %s not found in DB while reading shapes',
                $postcode));

            return null;
        } catch (NonUniqueResultException $e) {
            // Will never happen

            return null;
        }
    }

    /**
     * @param $X
     * @param $Y
     *
     * @return float
     */
    public function RD2lat($X, $Y)
    {
        $latpqK = [
            1 => ['p' => 0, 'q' => 1, 'K' => 3235.65389],
            2 => ['p' => 2, 'q' => 0, 'K' => -32.58297],
            3 => ['p' => 0, 'q' => 2, 'K' => -0.2475],
            4 => ['p' => 2, 'q' => 1, 'K' => -0.84978],
            5 => ['p' => 0, 'q' => 3, 'K' => -0.0665],
            6 => ['p' => 2, 'q' => 2, 'K' => -0.01709],
            7 => ['p' => 1, 'q' => 0, 'K' => -0.00738],
            8 => ['p' => 4, 'q' => 0, 'K' => 0.0053],
            9 => ['p' => 2, 'q' => 3, 'K' => -3.9E-4],
            10 => ['p' => 4, 'q' => 1, 'K' => 3.3E-4],
            11 => ['p' => 1, 'q' => 1, 'K' => -1.2E-4],
        ];

        $a = 0;
        $dX = 1E-5 * ($X - self::$X0);
        $dY = 1E-5 * ($Y - self::$Y0);

        for ($i = 1; 12 > $i; $i++) {
            $a += $latpqK[$i]['K'] * pow($dX, $latpqK[$i]['p']) * pow($dY, $latpqK[$i]['q']);
        }

        return self::$lat0 + $a / 3600;
    }

    /**
     * @param $X
     * @param $Y
     *
     * @return float
     */
    public function RD2lng($X, $Y)
    {
        $lngpqL = [
            1 => ['p' => 1, 'q' => 0, 'K' => 5260.52916],
            2 => ['p' => 1, 'q' => 1, 'K' => 105.94684],
            3 => ['p' => 1, 'q' => 2, 'K' => 2.45656],
            4 => ['p' => 3, 'q' => 0, 'K' => -0.81885],
            5 => ['p' => 1, 'q' => 3, 'K' => 0.05594],
            6 => ['p' => 3, 'q' => 1, 'K' => -0.05607],
            7 => ['p' => 0, 'q' => 1, 'K' => 0.01199],
            8 => ['p' => 3, 'q' => 2, 'K' => -0.00256],
            9 => ['p' => 1, 'q' => 4, 'K' => 0.00128],
            10 => ['p' => 0, 'q' => 2, 'K' => 2.2E-4],
            11 => ['p' => 2, 'q' => 0, 'K' => -2.2E-4],
            12 => ['p' => 5, 'q' => 0, 'K' => 2.6E-4],
        ];

        $a = 0;
        $dX = 1E-5 * ($X - self::$X0);
        $dY = 1E-5 * ($Y - self::$Y0);

        for ($i = 1; 13 > $i; $i++) {
            $a += $lngpqL[$i]['K'] * pow($dX, $lngpqL[$i]['p']) * pow($dY, $lngpqL[$i]['q']);
        }

        return self::$lng0 + $a / 3600;
    }
}
