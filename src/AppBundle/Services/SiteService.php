<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanySite;
use AppBundle\Entity\Site\Domain as DomainEntity;
use AppBundle\Entity\Site\Site;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use AppBundle\Utils\Date\DayUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SiteService
 * @package AppBundle\Services
 */
class SiteService
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var bool */
    private $isMasterRequest = false;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /** @var string */
    private $hostname;

    /** @var string  */
    private $requestUri;

    /** @var DomainEntity */
    private $domain;

    /** @var CompanySite */
    private $companySite;

    /** @var DayUtil */
    private $dayUtil;

    /**
     * @var SessionInterface
     */
    private $session;

    /** Site */
    private $currentSite;

    /** @var Site */
    private $subsite;

    /**
     * Domain constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $requestStack
     * @param DayUtil                $dayUtil
     * @param SessionInterface       $session
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, DayUtil $dayUtil, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->dayUtil = $dayUtil;
        $this->session = $session;
        $this->subsite = false;
    }

    /**
     * @return Site|null
     */
    public function determineSite()
    {
        $this->setupRequest();

        if ($this->isMasterRequest) {
            if($this->currentSite !== null){
                return $this->currentSite;
            }

            if ($companySite = $this->getCompanySite($this->hostname)) {
                return $companySite->getDefaultSite();
            }

            $uriParts = explode('/', $this->requestUri);
            $slug = $uriParts[1];

            if($this->subsite === false){
                $this->subsite = $this->entityManager->getRepository(Site::class)->findOneBy(['slug' => $slug]);
            }

            if(null !== $this->subsite) {
                $this->currentSite = $this->subsite;
                return $this->subsite;
            }

            if ($domain = $this->getDomain($this->hostname)) {
                $site = $domain->getSite();
                $this->currentSite = $site;
                return $site;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function determineSiteTheme(){
        if($this->session->has('site_id')){
            $site = $this->entityManager->getRepository(Site::class)->find($this->session->get('site_id'));
        } else {
            $site = $this->determineSite();
        }
        return $site? $site->getTheme(): '';
    }

    /**
     * @return DomainEntity|null
     */
    public function determineDomain() {
        $this->setupRequest();

        if ($domain = $this->getDomain($this->hostname)) {
            return $domain;
        }

        return null;
    }
    /**
     * @return ArrayCollection|Collection
     */
    public function determineAssortments()
    {
        $this->setupRequest();

        $assortments = new ArrayCollection();

        if ($this->isMasterRequest) {
            if ($companySite = $this->getCompanySite($this->hostname)) {
                /** @var Company $company */
                $company = $companySite->getCompanies()->current();
                return $company->getAssortments()->filter(function (Assortment $assortment) {
                    return $assortment->getAssortmentType() === null;
                });
            }

            if ($domain = $this->getDomain($this->hostname)) {
                return $domain->getSite()->getAssortments();
            }
        }

        return $assortments;
    }

    /**
     * @param $hostname
     *
     * @return DomainEntity|bool|null|\object
     */
    private function getDomain($hostname)
    {
        $this->setupRequest();

        if (null !== $this->domain) {
            return $this->domain;
        }

        $domain = $this->entityManager->getRepository(DomainEntity::class)->findOneBy([
            'domain' => $hostname,
        ]);

        if ($domain) {
            return ($this->domain = $domain);
        }

        return ($this->domain = false);
    }

    /**
     * @param $hostname
     *
     * @return CompanySite|bool|null|\object
     */
    private function getCompanySite($hostname)
    {
        if (null !== $this->companySite) {
            return $this->companySite;
        }

        $possibleCompanyDomainName = explode('.', $hostname)[0];

        $companySite = $this->entityManager->getRepository(CompanySite::class)->findOneBy([
            'domainName' => $possibleCompanyDomainName,
        ]);

        if ($companySite) {
            return ($this->companySite = $companySite);
        }

        return ($this->companySite = false);
    }

    /**
     * @return bool|null|KiyohCompany
     */
    public function hasKiyohCompany()
    {
        $this->setupRequest();

        if (null === $this->domain && null === $this->getDomain($this->hostname)) {
            throw new \RuntimeException('No domain found');
        }

        if ($this->domain->getKiyohCompany()) {
            return $this->domain->getKiyohCompany();
        }

        return false;
    }


    /**
     * @param Site|null $site
     * @return array
     */
    public function getOpeningHours(?Site $site): array
    {
        if(null === $site) {
            $site = $this->determineSite();
        }

        if (null === $site) {
            throw new \RuntimeException('No site received');
        }

        $siteOpeningHours = $site->getSiteOpeningHours();

        if ($siteOpeningHours->isEmpty() !== false) {
            $allOpeningHours = [
                'Ma-Do' => [
                    'dayName' => 'Ma-Do',
                    'fromHours' => '08:30',
                    'tillHours' => '20:00',
                ],
                'Vrij-Za' => [
                    'dayName' => 'Vrij-Za',
                    'fromHours' => '08:30',
                    'tillHours' => '17:30',
                ],
            ];
        } else {
            $allOpeningHours = [];

            $days = $this->dayUtil->getDayNames(true);

            foreach ($siteOpeningHours as $siteOpeninghour) {
                if (($dayName = $days[$siteOpeninghour->getDay()]) === null) {
                    continue;
                }

                $newOpeningHourKey = $dayName;

                foreach ($allOpeningHours as $openingHourKey => $allOpeningHour) {
                    if ($allOpeningHour['fromHours'] === $siteOpeninghour->getFromHour() &&
                        $allOpeningHour['tillHours'] === $siteOpeninghour->getTillHour()
                    ) {
                        [$newOpeningHourKey] = \explode('-', $openingHourKey, 2);
                        unset($allOpeningHours[$openingHourKey]);
                        $newOpeningHourKey = $newOpeningHourKey . '-' . $dayName;
                        break;
                    }
                }

                $allOpeningHours[$newOpeningHourKey] = [
                    'dayName' => $newOpeningHourKey,
                    'fromHours' => $siteOpeninghour->getFromHour(),
                    'tillHours' => $siteOpeninghour->getTillHour(),
                ];
            }
        }

        return $allOpeningHours;
    }

    private function setupRequest() {
        if (null !== $this->requestStack->getMasterRequest()) {
            $this->isMasterRequest = true;
            $this->hostname = $this->requestStack->getMasterRequest()->getHost();
            $this->requestUri = $this->requestStack->getMasterRequest()->getRequestUri();
        }
    }
}
