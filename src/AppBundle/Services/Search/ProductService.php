<?php

namespace AppBundle\Services\Search;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\SiteService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method Product[] getResult()
 */
class ProductService extends AbstractSearch
{
    /**
     * @var SiteService
     */
    protected $siteService;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * ProductService constructor.
     * @param SiteService            $siteService
     * @param TokenStorageInterface  $tokenStorage
     * @param EntityManagerInterface $entityManager
     * @param ProductManager         $productManager
     */
    public function __construct(SiteService $siteService, TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager, ProductManager $productManager)
    {
        $this->entity = Product::class;
        $this->translationEntity = ProductTranslation::class;
        $this->siteService = $siteService;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->productManager = $productManager;
    }

    /**
     * @param string $q
     * @param int    $limit
     * @return $this
     * @throws \Exception
     */
    public function search($q, $limit = null)
    {
        return $this->searchBy([
            "title",
            "short_description",
            "description",
        ], $q, $limit);
    }

    /**
     * @param string $q
     * @param int    $limit
     * @return $this
     * @throws \Exception
     */
    public function searchByTitle($q, $limit = null)
    {
        return $this->searchBy("title", $q, $limit);
    }

    /**
     * @param $q
     * @return array
     * @throws \Exception
     */
    public function getProductsByTitle($q)
    {
        /** @var Customer $user */
        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Product::class)->createQueryBuilder('product');
        $qb->leftJoin('product.translations', 'product_translation')
            ->leftJoin('product.assortmentProducts', 'assortment_products')
            ->leftJoin('assortment_products.assortment', 'assortment')
            ->leftJoin('assortment.sites', 'sites');

        $qb->andWhere($qb->expr()->orX(
          $qb->expr()->like('product_translation.title', ':searchQuery'),
          $qb->expr()->like('product_translation.description', ':searchQuery')
        ));

        $qb->setParameter('searchQuery', '%'.$q.'%');


        if($user instanceof Customer && $user->getCompany() && false === $user->getCompany()->getAssortments()->isEmpty()) {
            $assortmentIds = [];
            $assortments =  $user->getCompany()->getAssortments()->filter(function (Assortment $assortment) {
                $assortmentKey = $assortment->getAssortmentType() ? $assortment->getAssortmentType()->getKey() : false;
                return !\in_array($assortmentKey,
                    [AssortmentType::TYPE_ADDITIONAL_PRODUCTS, AssortmentType::TYPE_CARDS], true);
            });

            foreach($assortments as $assortment) {
                $assortmentIds[] = $assortment->getId();
            }

            $qb->andWhere('assortment.id IN (:assormentIds)')
                ->setParameter('assormentIds', $assortmentIds);
        } else {
            $site = $this->siteService->determineSite();
            $qb->andWhere('sites.id = :site');
            $qb->setParameter('site', $site->getId());
        }

        $products = $qb->getQuery()->getResult();
        $products = array_map(function(Product $product) {
            return $this->productManager->decorateProduct($product);
        }, $products);

        return $products;
    }
}
