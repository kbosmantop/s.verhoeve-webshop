<?php

namespace AppBundle\Services;

use function in_array;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductFormValidationService
 * @package AppBundle\Services
 */
class ProductFormValidationService
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Form    $form
     * @param Request $request
     * @return bool
     */
    public function validate(Form $form, Request $request): bool
    {
        $hasError = false;

        $this->form = $form;
        $this->request = $request;

        if ($form->has('variation') && $form->get('variation')->getData() === null) {
            $hasError = true;
            $form->addError(new FormError('product.variation.not_blank'));
        }

        if (!$this->validateCard()) {
            $hasError = true;
        }

        return !$hasError;
    }

    /**
     * @return bool
     */
    private function validateCard(): bool
    {
        $cardError = false;
        if ($this->request->request->get('add_card')) {
            if (!$this->request->request->get('card')) {
                $cardError = true;

                $this->form->addError(new FormError('product.card.not_blank'));
            }

            if (!$this->request->request->get('card_text')) {
                $cardError = true;

                $this->form->addError(new FormError('product.card_text.not_blank'));
            }
        }

        return !$cardError;
    }

    private function validateCustomPrice(): void
    {
        if ($this->form->has('price')) {
            if ($this->form->get('price')->getData() === null) {
                $this->form->addError(new FormError('product.manual_price.not_blank'));
            } else {
                $product = $this->form->get('product');
                $minMaxErrors = $product->checkMinMaxPrice($this->form->get('price')->getData());

                if ($minMaxErrors) {
                    if (in_array('min_max', $minMaxErrors, true)) {
                        $this->form->addError(new FormError('product.manual_price.not_valid', null, [
                            '%min%' => $product->getDisplayMinPrice(),
                            '%max%' => $product->getDisplayMaxPrice(),
                        ]));
                    }

                    if (in_array('step', $minMaxErrors, true)) {
                        $this->form->addError(new FormError('product.manual_price.step_not_valid', null, [
                            '%step%' => $product->getDisplayStepMinMaxPrice(),
                        ]));
                    }
                }
            }
        }
    }
}
