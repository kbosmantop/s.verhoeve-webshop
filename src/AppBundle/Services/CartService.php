<?php

namespace AppBundle\Services;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Order\CartStatus;
use AppBundle\Entity\Order\Order;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\Order\CartManager;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\Sales\Order\DeliveryService;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Services\Discount\VoucherService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Workflow\Registry;

/**
 * Class CartService
 * @package AppBundle\Services
 */
class CartService
{

    public $autoFlush = true;

    public const SOURCE_USER = 'user';
    public const SOURCE_UUID = 'uuid';

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var DeliveryService
     */
    private $deliveryService;

    /**
     * @var EntityManagerInterface;
     */
    protected $em;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /** @var SupplierManagerService */
    protected $supplierManager;

    /** @var VoucherService */
    protected $voucherService;

    /** @var SiteService */
    protected $siteService;

    /** @var ProductManager */
    protected $productManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var CartManager
     */
    protected $cartManager;

    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * @var OrderCollectionManager
     */
    protected $orderCollectionManager;

    /**
     * @var Registry
     */
    private $workflowRegistry;

    /**
     * CartService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack $requestStack
     * @param Session $session
     * @param TokenStorage $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     * @param SupplierManagerService $supplierManager
     * @param VoucherService $voucherService
     * @param SiteService $siteService
     * @param ProductManager $productManager
     * @param CartManager $cartManager
     * @param OrderLineManager $orderLineManager
     * @param OrderCollectionManager $orderCollectionManager
     * @param DeliveryService $deliveryService
     * @param ContainerInterface $container
     * @param Registry $workflowRegistry
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack,
        Session $session,
        TokenStorage $tokenStorage,
        AuthorizationChecker $authorizationChecker,
        SupplierManagerService $supplierManager,
        VoucherService $voucherService,
        SiteService $siteService,
        ProductManager $productManager,
        CartManager $cartManager,
        OrderLineManager $orderLineManager,
        OrderCollectionManager $orderCollectionManager,
        DeliveryService $deliveryService,
        ContainerInterface $container,
        Registry $workflowRegistry
    ) {
        $this->em = $entityManager;
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->supplierManager = $supplierManager;
        $this->voucherService = $voucherService;
        $this->siteService = $siteService;
        $this->productManager = $productManager;
        $this->cartManager = $cartManager;
        $this->orderLineManager = $orderLineManager;
        $this->orderCollectionManager = $orderCollectionManager;
        $this->requestStack = $requestStack;
        $this->deliveryService = $deliveryService;
        $this->container = $container;
        $this->workflowRegistry = $workflowRegistry;
    }

    /**
     * @param bool $flush
     */
    public function setAutoFlush(bool $flush)
    {
        $this->autoFlush = $flush;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequest(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param string|null $source
     * @param boolean     $autoCreate
     * @return Cart|mixed
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getCart($source = null, $autoCreate = true)
    {
        if (!$this->cart) {
            $qb = $this->em->getRepository(Cart::class)->createQueryBuilder('c');
            $qb->leftJoin('c.status', 'cs');

            $qb->andWhere("cs.id IS NULL OR cs.id = 'pending'");

            $token = $this->tokenStorage->getToken();

            if (!$source) {
                if ($token && $this->authorizationChecker->isGranted('ROLE_USER') && !$this->session->get('migrate_cart')) {
                    $source = self::SOURCE_USER;
                } else {
                    $source = self::SOURCE_UUID;
                }
            }

            if ($source === self::SOURCE_USER) {
                $qb->andWhere('c.customer = :customer');
                $qb->setParameter('customer', $token->getUser());
            } elseif ($source === self::SOURCE_UUID && $this->request) {
                $qb->andWhere('c.uuid = :uuid');
                $qb->setParameter('uuid', $this->request->cookies->get('cart'));
            }

            $qb->andWhere('c.expiredAt IS NULL');
            $qb->andWhere('c.deletedAt IS NULL');

            $site = $this->siteService->determineSite();
            if(null !== $site) {
                $qb->andWhere('c.site = :site')
                    ->setParameter('site', $site->getId());
            }

            $qb->setMaxResults(1);

            $this->cart = $qb->getQuery()->getOneOrNullResult();

            if (!$this->cart && $autoCreate) {
                $this->cart = $this->createCart();
            }
        }

        return $this->cart;
    }

    /**
     * @param boolean $forceAnonymous
     *
     * @return Cart
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function createCart($forceAnonymous = false)
    {
        $cart = new Cart();
        $request = $this->request;

        if ($request) {
            $cart->setSite($this->siteService->determineSite());
            $cart->setIp($request->getClientIp());
            $cart->setUserAgent($request->headers->get('User-Agent'));
        }

        if ($this->session && $this->session->getId()) {
            $cart->setSessionId($this->session->getId());
        }

        if (!$forceAnonymous && (null !== ($token = $this->tokenStorage->getToken())) && ($user = $token->getUser()) instanceof Customer && $this->authorizationChecker->isGranted('ROLE_USER')) {
            $cart->setCustomer($user, true);
        }

        $this->em->persist($cart);

        if ($this->autoFlush) {
            $this->em->flush();
        }

        return $cart;
    }

    /**
     * @param Cart|null $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * Migrates the cart to the customer
     *
     * @param Customer $user
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function migrate(Customer $user)
    {
        $this->getCart(self::SOURCE_UUID, false);

        if ($this->count() > 0) {
            $this->expirePreviousCarts();

            $this->session->set('migrate_cart', true);

            $cart = $this->getCart();
            $cart->setCustomer($user, true);

            $this->em->flush($cart);

            $this->session->remove('migrate_cart');
        } else {
            $this->session->remove('migrate_cart');

            $this->setCart(null);
            $cart = $this->getCart(self::SOURCE_USER, false);

            if (!$cart) {
                $cart = $this->getCart(self::SOURCE_UUID, false);

                if ($cart) {
                    $cart->setCustomer($user, true);
                }
            }

            $this->em->flush($cart);
        }
    }

    /**
     * @todo: Maybe move this to the customer entity
     * Expires previous carts for current customer
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function expirePreviousCarts()
    {
        $token = $this->tokenStorage->getToken();
        $qb = $this->em->getRepository(Cart::class)->createQueryBuilder('c');
        $qb->leftJoin('c.status', 'cs');
        $qb->andWhere("cs.id IS NULL OR cs.id = 'pending'");
        $qb->andWhere('c.customer = :customer');

        if (null === $token) {
            throw new \RuntimeException('No token received');
        }

        $qb->setParameter('customer', $token->getUser());
        $qb->andWhere('c.deletedAt IS NULL');

        $carts = $qb->getQuery()->getResult();

        /**
         * @var Cart $cart
         */
        foreach ($carts as $cart) {
            $cart->setExpiredAt(new \DateTime());
            $cart->setStatus($this->em->getRepository(CartStatus::class)->find('expired'));

            $this->em->flush($cart);
        }
    }

    /**
     * @return CartOrder
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function createOrder()
    {
        $order = new CartOrder();
        $order->setCart($this->getCart());

        $this->getCart()->addOrder($order);

        $this->em->persist($order);

        if ($this->autoFlush) {
            $this->em->flush();
        }

        if (null !== $this->container) {
            $order->setContainer($this->container);
        }

        return $order;
    }

    /**
     * @param CartOrder $cartOrder
     * @return $this
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function removeOrder(CartOrder $cartOrder)
    {
        $this->getCart()->removeOrder($cartOrder);

        $this->em->remove($cartOrder);

        if ($this->autoFlush) {
            $this->em->flush();
        }

        return $this;
    }

    /**
     * @param CartOrder $cartOrder
     *
     * @return CartOrder|null
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getOrder(CartOrder $cartOrder): ?CartOrder
    {
        $result = $this->getCart()->getOrders()->filter(function (CartOrder $cartOrder1) use ($cartOrder) {
            return ($cartOrder === $cartOrder1);
        })->current();

        if ($result) {
            return $result;
        }

        return null;
    }

    /**
     * @return Collection
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getOrders()
    {
        return $this->getCart()->getOrders();
    }

    /**
     * @return CartOrder|null
     *
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getCurrentOrder(): ?CartOrder
    {
        if ($this->getCart()->getOrders()->isEmpty()) {
            return null;
        }

        if ($this->session && $this->session->getId() && $this->session->get('cart_' . $this->getCart()->getId() . '.current_order')) {
            $currentOrder = $this->getCart()->getOrders()->filter(function (CartOrder $cartOrder) {
                return !$cartOrder->getLines()->isEmpty() && $cartOrder->getId() === $this->session->get('cart_' . $this->getCart()->getId() . '.current_order');
            })->first();

            if ($currentOrder) {
                return $currentOrder;
            }
        }

        /** @var \ArrayIterator $iterator */
        $iterator = $this->getCart()->getOrders()->getIterator();
        $iterator->uasort(function (CartOrder $cartOrder1, CartOrder $cartOrder2) {
            if ($cartOrder1->getUpdatedAt() == $cartOrder2->getUpdatedAt()) {
                return 0;
            }

            return ($cartOrder1->getUpdatedAt() > $cartOrder2->getUpdatedAt() ? -1 : 1);
        });

        return $iterator->current();
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return CartOrder
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getCurrentOrderByLine(CartOrderLine $cartOrderLine)
    {
        $this->setCurrentOrder($cartOrderLine->getCartOrder());

        return $this->getCurrentOrder();
    }

    /**
     * @param CartOrder $cartOrder
     * @return $this
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function setCurrentOrder(CartOrder $cartOrder)
    {
        $this->session->set('cart_' . $this->getCart()->getId() . '.current_order', $cartOrder->getId());

        return $this;
    }

    /**
     * @return $this
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function clear()
    {
        $this->getCart()->setStatus($this->em->getRepository(CartStatus::class)->find('cancelled'));

        if ($this->autoFlush) {
            $this->em->flush();
        }

        $this->cart = null;

        return $this;
    }

    /**
     * @param Cart|null $cart
     * @param bool      $flush
     * @return OrderCollection
     * @throws ConnectionException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function finalize(Cart $cart = null, $flush = true): OrderCollection
    {
        if ($cart) {
            $this->cart = $cart;
        }

        $this->autoFlush = false;

        $this->em->getConnection()->beginTransaction();

        $status = 'payment_pending';// @todo: workflow refactor

        $orderCollection = $this->getCart()->getOrderCollection();

        if (!$this->getCart()->getOrderCollection()) {
            $orderCollection = new OrderCollection();

            $this->getCart()->setOrderCollection($orderCollection);
        } else {

            // Cleanup orders from the orderCollection when the order has been removed from the cart
            foreach ($orderCollection->getOrders() as $order) {
                $cartOrder = $order->getCartOrder();

                try {
                    if ($cartOrder && $cartOrder->isDeleted()) {
                        $cartOrder = null;
                    }
                } catch (EntityNotFoundException $exception) {
                    // handling for soft-deletes
                    $cartOrder = null;
                }

                if (!$cartOrder) {
                    $workflow = $this->workflowRegistry->get($order);

                    if($workflow->can($order, 'cancel')) {
                        $workflow->apply($order, 'cancel');
                    }
                } else {
                    foreach ($cartOrder->getLines() as $cartLine) {
                        // Cleanup lines from the order when the order line been removed from the cartOrder
                        foreach ($cartOrder->getOrder()->getLines() as $orderLine) {
                            $cartLine = $orderLine->getCartLine();

                            try {
                                if ($cartLine && $cartLine->isDeleted()) {
                                    $cartLine = null;
                                }
                            } catch (EntityNotFoundException $exception) {
                                // handling for soft-deletes
                                $cartLine = null;
                            }

                            if (!$cartLine) {
                                $cartOrder->getOrder()->removeLine($orderLine);
                            }
                        }
                    }
                }
            }
        }

        $orderCollection->setSite($this->getCart()->getSite());
        $orderCollection->setIp($this->getCart()->getIp());
        $orderCollection->setCompany($this->getCart()->getCompany());
        $orderCollection->setCustomer($this->getCart()->getCustomer());
        $orderCollection->setInvoiceAddress($this->getCart()->getInvoiceAddress());
        $orderCollection->setInvoiceAddressAttn($this->getCart()->getInvoiceAddressAttn());
        $orderCollection->setInvoiceAddressStreet($this->getCart()->getInvoiceAddressStreet());
        $orderCollection->setInvoiceAddressNumber($this->getCart()->getInvoiceAddressNumber());
        $orderCollection->setInvoiceAddressNumberAddition($this->getCart()->getInvoiceAddressNumberAddition());
        $orderCollection->setInvoiceAddressPostcode($this->getCart()->getInvoiceAddressPostcode());
        $orderCollection->setInvoiceAddressCity($this->getCart()->getInvoiceAddressCity());
        $orderCollection->setInvoiceAddressCountry($this->getCart()->getInvoiceAddressCountry());
        $orderCollection->setInvoiceAddressPhoneNumber($this->getCart()->getInvoiceAddressPhoneNumber());

        foreach ($this->getCart()->getVouchers() as $voucher) {
            $orderCollection->addVoucher($voucher);
        }

        if ($this->session->has('employee_login_as_customer') && null !== $this->session->get('employee_id')) {
            $employee = $this->em->getRepository(User::class)->find($this->session->get('employee_id'));

            $orderCollection->setEmployee($employee);
        }

        if ($this->getOrders()->isEmpty()) {
            throw new \RuntimeException("Can't convert Cart with no orders");
        }

        foreach ($this->getOrders() as $cartOrder) {
            /**
             * @var CartOrder $cartOrder
             */
            if ($cartOrder->getOrder()) {
                $order = $cartOrder->getOrder();
            } else {
                $order = new Order();
                $order->setOrderCollection($orderCollection);
                $order->setStatus($status); // @todo: workflow refactor

                $orderCollection->addOrder($order);
            }

            $order->setPickupAddress($cartOrder->getPickupAddress());

            $order->setDeliveryAddress($cartOrder->getDeliveryAddress());
            $order->setDeliveryAddressCompanyName($cartOrder->getDeliveryAddressCompanyName());
            $order->setDeliveryAddressAttn($cartOrder->getDeliveryAddressAttn());
            $order->setDeliveryAddressStreet($cartOrder->getDeliveryAddressStreet());
            $order->setDeliveryAddressNumber($cartOrder->getDeliveryAddressNumber());
            $order->setDeliveryAddressNumberAddition($cartOrder->getDeliveryAddressNumberAddition());
            $order->setDeliveryAddressPostcode($cartOrder->getDeliveryAddressPostcode());
            $order->setDeliveryAddressCity($cartOrder->getDeliveryAddressCity());
            $order->setDeliveryAddressCountry($cartOrder->getDeliveryAddressCountry());
            $order->setDeliveryAddressPhoneNumber($cartOrder->getDeliveryAddressPhoneNumber());
            $order->setDeliveryDate($cartOrder->getDeliveryDate());
            $order->setTimeslot($cartOrder->getTimeslot());

            $order->setSupplierRemark($cartOrder->getSupplierRemark());
            $order->setDeliveryRemark($cartOrder->getDeliveryRemark());
            $order->setDeliveryAddressType($cartOrder->getDeliveryAddressType());
            $order->setMetadata($cartOrder->getMetadata());

            $cartOrder->setOrder($order);

            foreach ($cartOrder->getLines() as $cartOrderLine) {
                /**
                 * @var $cartOrderLine CartOrderLine|CartOrderLineDecorator
                 */
                if ($cartOrderLine->getOrderLine()) {
                    $orderOrderLine = $cartOrderLine->getOrderLine();
                } else {
                    $orderOrderLine = new OrderLine();
                    $orderOrderLine->setOrder($order);

                    $order->addLine($orderOrderLine);
                }

                $product = $cartOrderLine->getProduct();
                $orderOrderLine->setTitle($product->getDisplayName());

                //convert companyProducts to regular products and add id of companyProduct to metadata
                if ($product instanceof CompanyProduct) {
                    $metadata = $cartOrderLine->getMetadata();
                    $metadata['companyProduct'] = $cartOrderLine->getProduct()->getId();

                    $product = $product->getRelatedProduct();
                    $cartOrderLine->setMetadata($metadata);
                }

                $orderOrderLine->setProduct($product);
                $orderOrderLine->setQuantity($cartOrderLine->getQuantity());

                //decorate product
                $product = $this->productManager->decorateProduct($product);

                if (null !== ($productGroup = $product->getProductgroup(true))) {
                    $orderOrderLine->setLedger($productGroup->getLedger());
                }

                $orderOrderLine->setMetadata(($cartOrderLine->getMetadata() ?: null));

                if ($cartOrderLine->getParent()) {
                    $orderOrderLine->setParent($cartOrderLine->getParent()->getOrderLine());
                }

                if ($cartOrderLine->getPrice() !== null) {
                    $orderOrderLine->setPrice($cartOrderLine->getPrice());
                } else {
                    $orderOrderLine->setPrice($cartOrderLine->getProduct()->getPrice());
                }

                $this->orderLineManager->createVatLines($orderOrderLine);

                $orderOrderLine->setDiscountAmount($cartOrderLine->getDiscountAmount());

                $cartOrderLine->setOrderLine($orderOrderLine);
            }

            if ($orderCollection->getId()) {
                $this->em->persist($order);
                $this->em->flush();
            }
        }

        $this->orderCollectionManager->calculateTotals($orderCollection);

        if ($flush) {
            $this->em->persist($orderCollection);
            $this->em->flush();

            $this->em->getConnection()->commit();
        }

        $this->session->set('RecentOrderCollectionId', $orderCollection->getId());

        return $orderCollection;
    }

    /**
     * @return int
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function count(): int
    {
        $cart = $this->getCart();

        $totalProducts = 0;
        foreach ($cart->getOrders() as $cartOrder) {
            /** @var CartOrderLine $line */
            foreach ($cartOrder->getLines() as $line) {
                if (null === $line->getParent() && !($line->getProduct() instanceof TransportType)) {
                    $totalProducts += (int)$line->getQuantity();
                }
            }
        }

        return $totalProducts;
    }

    /**
     * @param Product      $product
     * @param Product|null $card
     * @param Product|null $personalization
     * @param array        $options
     * @return CartOrderLine
     * @throws CartInvalidSupplierCombinationException
     * @throws \Exception
     */
    public function addProduct(
        ProductInterface $product,
        ProductInterface $card = null,
        ProductInterface $personalization = null,
        array $options = []
    ) {
        $em = $this->em;

        $options = $this->resolveProductOptions($options);

        $currentOrder = $this->getCurrentOrder();

        if (!$currentOrder) {
            $currentOrder = $this->createOrder();
        }

        if ($product instanceof CompanyProductDecorator) {
            $productToAdd = $product->getEntity();
        } else {
            $productToAdd = $product;
        }

        $productLine = $currentOrder->addProduct($productToAdd, $options['quantity'], null, true);

        // @todo: hier rekening houden met inclusief / exclusief btw ingave van de bezoeker o.b.v. site instellingen
        // Berekenen o.b.v. price service.
        if ($options['price']) {
            $price = number_format($options['price'] / (100 + $product->getVat()->getPercentage()) * 100, 3);

            $productLine->setPrice($price);
        }

        if ($card) {
            $currentOrder->addProduct($card, $options['card_quantity'],
                $productLine)->setMetadata([
                'is_card' => true,
                'text' => $options['card_text'],
            ]);

            $em->flush();
        }

        if ($personalization) {
            $metadata = [];
            if (isset($options['designer'])) {
                $metadata['designer'] = $options['designer'];
            } elseif (isset($options['personalization_complete_meta'])) {
                $metadata = $options['personalization_complete_meta'];
            }

            /** @var Product $productToAddis */
            if($productToAdd->hasPersonalization()) {
                $metadata = array_merge($productLine->getMetadata(), $metadata);
                $productLine->setMetadata($metadata);
            } else {
                $currentOrder->addProduct($personalization, null, $productLine)->setMetadata($metadata);
            }

            $em->flush();
        }

        if (null !== $options['letter']) {
            $letter = $em->getRepository(Letter::class)->find($options['letter']['product']);

            $currentOrder
                ->addProduct($letter, 1, $productLine, true)
                ->setMetadata([
                    'letter' => [
                        'uuid' => $options['letter']['uuid'],
                        'filename' => $options['letter']['filename'],
                        'path' => $options['letter']['path'],
                    ],
                ]);

            $em->flush();
        }

        $this->setCurrentOrder($currentOrder);
        $this->mergeOrder($currentOrder);

        $this->applyVouchers();
        $em->flush();

        return $productLine;
    }

    /**
     * @param CartOrder $currentOrder
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function mergeOrder(CartOrder $currentOrder)
    {
        $em = $this->em;
        $supplierManager = $this->supplierManager;

        /** @var CartOrder $order */
        foreach ($this->cart->getOrders() as $order) {
            $mergeError = false;

            if ($order->getId() === $currentOrder->getId()) {
                continue;
            }

            if ($order->getDeliveryAddressPostcode() === $currentOrder->getDeliveryAddressPostcode()
                && $order->getDeliveryAddressNumber() === $currentOrder->getDeliveryAddressNumber()
                && $order->getDeliveryAddressNumberAddition() === $currentOrder->getDeliveryAddressNumberAddition()
                && $order->getDeliveryAddressCountry() === $currentOrder->getDeliveryAddressCountry()
                && $order->getDeliveryDate() == $currentOrder->getDeliveryDate()
                && $order->getPickupAddress() === $currentOrder->getPickupAddress()
                && $order->getTimeslot() === $currentOrder->getTimeslot()) {

                $linesToMerge = [];

                /** @var CartOrderLine $currentOrderLine */
                foreach ($currentOrder->getLines() as $currentOrderLine) {
                    /** @var CartOrderLine $cartOrderLine */
                    foreach ($order->getLines() as $cartOrderLine) {
                        if (!$supplierManager->canCombineProductWith($currentOrderLine->getProduct(),
                            $cartOrderLine->getProduct())) {
                            $mergeError = true;

                            break 2;
                        }

                        $linesToMerge[] = $currentOrderLine;
                    }
                }

                if (!$mergeError) {
                    foreach ($linesToMerge as $lineToMerge) {
                        $order->addLine($lineToMerge);
                        $currentOrder->removeLine($lineToMerge);
                    }

                    $this->deliveryService->updateDeliveryProduct($order);

                    if ($currentOrder->getLines()->filter(function (CartOrderLine $line) {
                        return !$line->getProduct()->isTransportType();
                    })->isEmpty()) {
                        $em->remove($currentOrder);
                        $currentOrder = null;
                        break;
                    }
                }
            }
        }

        if (null !== $currentOrder) {
            $this->deliveryService->updateDeliveryProduct($currentOrder);
        }

        $this->applyVouchers();

        $em->flush();
    }

    /**
     * Duplicatie the given CartOrder and save it to the Cart
     *
     * @param CartOrder $cartOrder
     * @throws \Exception
     * @throws FileNotFoundException
     * @return CartOrder
     */
    public function duplicateOrderForAnotherAddress(CartOrder $cartOrder)
    {
        $duplicate = clone $cartOrder;
        $em = $this->em;

        // Clear all delivery related information
        $duplicate
            ->setDeliveryAddressType(null)
            ->setDeliveryAddress(null)
            ->setDeliveryDate(null)
            ->setDeliveryAddressCompanyName(null)
            ->setDeliveryAddressAttn(null)
            ->setDeliveryAddressStreet(null)
            ->setDeliveryAddressNumber(null)
            ->setDeliveryAddressNumberAddition(null)
            ->setDeliveryAddressPhoneNumber(null)
            ->setDeliveryAddressPostcode(null)
            ->setDeliveryAddressCity(null);

        foreach ($cartOrder->getLines() as $line) {
            $duplicate->removeLine($line);

            // Skip delivery product
            if ($line->getProduct()->isTransportType()) {
                continue;
            }

            // Skip children
            if (null !== $line->getParent()) {
                continue;
            }

            $duplicateLine = clone $line;
            $duplicateLine->setChildren(null);
            $duplicateLine->setCartOrder($duplicate);

            if ($line->getChildren()) {
                foreach ($line->getChildren() as $childLine) {
                    $childLine = clone $childLine;
                    $childLine->setCartOrder($duplicate);
                    $childLine->setParent($duplicateLine);

                    $duplicateLine->addChild($childLine);

                    $meta = $childLine->getMetadata();
                    if($meta !== null && $meta !== [] && isset($meta['designer'])) {
                        $newUuid = $this->container->get(Designer::class)->setUuid($meta['designer'])->cloneDesign();
                        if ($newUuid === null) {
                            unset($meta['designer']);
                        } else {
                            $meta['designer'] = $newUuid;
                        }
                        $childLine->setMetadata($meta);
                    }
                }
            }

            $duplicate->addLine($duplicateLine);
        }

        $em->persist($duplicate);
        $em->flush();

        $this->setCurrentOrder($duplicate);

        return $duplicate;
    }

    /**
     * @param $options
     * @return array
     */
    private function resolveProductOptions($options)
    {
        $resolver = new OptionsResolver();

        $resolver->setDefined([
            'quantity',
            'card_quantity',
            'card_text',
            'price',
            'designer',
            'letter',
            'override',
            'personalization_complete_meta',
        ]);

        $resolver->setDefaults([
            'quantity' => 1,
            'card_quantity' => null,
            'price' => null,
            'letter' => null,
        ]);

        return $resolver->resolve($options);
    }

    /**
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    private function applyVouchers()
    {
        //check if cart has applied voucher and reapply it for new products
        if (!$this->getCart()->getVouchers()->isEmpty()) {
            foreach ($this->getCart()->getVouchers() as $voucher) {
                $this->voucherService->applyVoucherForOrderCollectionInterface($voucher->getCode(), $this->getCart(),
                    true);
            }
        }
    }

    /**
     * @return CartManager
     */
    public function getCartManager()
    {
        return $this->cartManager;
    }
}
