<?php

namespace AppBundle\Services\Filesystem;

use AdminBundle\Filesystem\AbstractRackspaceUrlPlugin;
use League\Flysystem\Rackspace\RackspaceAdapter;

class RackspaceTempUrlPlugin extends AbstractRackspaceUrlPlugin
{
    /**
     * @param string|null $path
     * @param int         $expiresIn
     *
     * @return string;
     * @throws \Exception
     */
    public function handle($path = null, $expiresIn = 30)
    {
        if (!($this->adapter instanceof RackspaceAdapter)) {
            throw new \Exception();
        }

        $expires = time() + $expiresIn;

        $privateUrl = $this->filesystem->get($path)->getPrivateUrl();

        $openstackTempUrlKey = $this->container->getParameter("openstack_temp_url_key");

        return $privateUrl . "?" . http_build_query(self::calculateQueryParams($path, $expires, $openstackTempUrlKey));
    }

    /**
     * @param $path
     * @param $expires
     * @param $openstackTempUrlKey
     *
     * @return array
     */
    public static function calculateQueryParams($path, $expires, $openstackTempUrlKey)
    {
        return [
            "temp_url_sig" => self::calculateSignature($path, $expires, 'GET', $openstackTempUrlKey),
            "temp_url_expires" => $expires,
        ];
    }

    /**
     * Gets the method name.
     *
     * @return string
     */
    public function getMethod()
    {
        return 'getTempUrl';
    }

    /**
     * @param        $path
     * @param        $expires
     * @param string $method
     * @param        $openstackTempUrlKey
     *
     * @return string
     */
    private static function calculateSignature($path, $expires, $method = "GET", $openstackTempUrlKey)
    {

        return hash_hmac('sha1', implode("\n", [
            $method,
            $expires,
            "/private/" . $path,
        ]), $openstackTempUrlKey);

    }
}
