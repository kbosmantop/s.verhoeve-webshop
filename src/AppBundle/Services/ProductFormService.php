<?php

namespace AppBundle\Services;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ProductFormService
 * @package AppBundle\Services
 */
class ProductFormService
{
    /**
     * @var ProductPriceManager
     */
    private $productPriceManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var Product|null
     */
    private $product;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * ProductFormService constructor.
     * @param ProductPriceManager    $productPriceManager
     * @param ProductManager         $productManager
     * @param TranslatorInterface    $translator
     * @param EntityManagerInterface $entityManager
     * @param CacheManager           $cacheManager
     */
    public function __construct(ProductPriceManager $productPriceManager, ProductManager $productManager, TranslatorInterface $translator, EntityManagerInterface $entityManager, CacheManager $cacheManager)
    {
        $this->productPriceManager = $productPriceManager;
        $this->productManager = $productManager;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function showLetterForm(ProductInterface $product): bool
    {
        return null !== $product->getLetter();
    }

    /**
     * @param Product $product
     * @return array
     * @throws \Exception
     */
    public function getLetterSettings(Product $product): array
    {
        if(null !== $product->getLetter()) {
            /** @var ProductTranslation $productTranslate */
            $productTranslate = $product->getLetter()->translate();
            return [
                'active' => true,
                'show' => false,
                'title' => $productTranslate->getTitle(),
                'description' => $productTranslate->getDescription(),
                'price' => $this->productPriceManager->getPrice($product->getLetter()),
            ];
        }

        return [];
    }

    /**
     * @param ProductInterface $product
     * @return array
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getOrderableQuantities(ProductInterface $product): array
    {
        if (!$product->hasVariations()) {
            $variation = $product->getVariations()->first();
            $orderableQuantities = $variation->getOrderableQuantities();
        } else {
            $orderableQuantities = [];

            /** @var GenericProduct $variation */
            foreach ($product->getVariations() as $variation) {
                $orderableQuantities += $variation->getOrderableQuantities();
            }
        }

        return $orderableQuantities;
    }

    /**
     * @param ProductInterface $product
     * @return array
     */
    public function getProperties(ProductInterface $product): array
    {
        /** @var Productgroup $productGroup */
        $productGroup = $product->getProductgroup();
        $productGroupPropertyTree[] = $productGroup->getId();
        $properties = [];

        if ($productGroup->getParent()) {
            $productGroupPropertyTree[] = $productGroup->getParent()->getId();
        }

        /** @var ProductProperty $productProperty */
        foreach ($product->getProductProperties() as $productProperty) {
            $addToProperties = $productProperty->getProductgroupProperty()->getProductgroups()->exists(function (
                $key,
                Productgroup $productGroup
            ) use ($productGroupPropertyTree) {
                void($key);
                return \in_array($productGroup->getId(), $productGroupPropertyTree, true);
            });

            if (!$addToProperties) {
                continue;
            }

            $properties[] = [
                'id' => $productProperty->getProductgroupProperty()->getId(),
                'description' => $productProperty->getProductgroupProperty()->translate()->getDescription(),
                'type' => 'select',
            ];
        }

        return array_reverse($properties);
    }

    /**
     * @param ProductInterface $product
     * @return array
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getVariationChoices(ProductInterface $product): array
    {
        $choices = [];

        foreach($product->getVariations() as $variation) {
            $variation = $this->productManager->decorateProduct($variation);

            $labelProduct = $variation;
            if ($variation->isCombination() && $variation->isConfigurable() && null !== $variation->getMainCombination()) {
                $labelProduct = $variation->getMainCombination()->getProduct();
            }

            $label = $labelProduct->getDisplayName();
            if (null !== $labelProduct->getPrice()) {
                $labelProductPrice = number_format($this->productPriceManager->getPrice($labelProduct), 2, ',',
                    '.');
                if ($labelProduct->getPrice()) {
                    $label .= ' á ' . $labelProductPrice;
                }
            }

            if (false === $variation->hasSupplier() || null === $labelProduct->getPrice()) {
                $label .= ' (' . $this->translator->trans('label.not-available-at-the-moment') . ')';
            } elseif (false === $variation->isOrderable()) {
                $label .= ' (' . $this->translator->trans('label.out-of-stock') . ')';
            }

            $choices[$variation->getId()] = $label;
        }

        return array_flip($choices);
    }

    /**
     * @param int $variationId
     * @return array
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getVariationChoicesAttributes(int $variationId)
    {
        if(null !== $this->product) {
            /** @var Product $productVariation */
            $productVariation = $this->product->getVariations()->filter(function (Product $product) use (
                $variationId
            ) {
                return $product->getId() === $variationId;
            })->current();
        } else {
            $productVariation = $this->entityManager->getRepository(Product::class)->find($variationId);
            $this->product = $productVariation->getParent();
        }

        if(!$productVariation instanceof CompanyProductDecorator) {
            $productVariation = $this->productManager->decorateProduct($productVariation);
        }

        $orderable = $productVariation->isOrderable();

        $attr = [
            'data-price' => number_format($this->productPriceManager->getPrice($productVariation), 2, ',',
                '.'),
            'data-original-price' => number_format($this->productPriceManager->getPrice($productVariation,
                false), 2, ',', '.'),
            'data-orderable' => $orderable ? 'true' : 'false',
            'data-has-supplier' => $productVariation->hasSupplier(),
            'data-personalization' => $productVariation->isProductPersonalizationAvailable() ? 'true' : 'false',
            'data-personalization-required' => $productVariation->isProductPersonalizationAvailable() && $productVariation->isProductPersonalizationRequired() ? 'true' : 'false',
        ];

        $attr['data-orderable-quantities'] = implode(',', $productVariation->getOrderableQuantities());

        if (false === $orderable || false === $productVariation->hasSupplier() || null === $productVariation->getPrice()) {
            $attr['disabled'] = 'disabled';
        }

        /** @var ProductProperty $productProperty */
        foreach ($productVariation->getProductProperties() as $productProperty) {
            $attr['data-property-' . $productProperty->getProductgroupProperty()->getId()] = (string)$productProperty;
        }

        if ($productVariation->isProductPersonalizationAvailable()) {
            $personalization = $productVariation->getFirstPersonalization();

            $attr['data-personalization-product-id'] = $personalization ? $personalization->getId() : null;
            $attr['data-personalization-product-price'] = $personalization ? number_format($this->productPriceManager->getPrice($personalization),
                2, ',', '') : null;
        }

        $productImage = $productVariation->getMainImage(true);

        if(null !== $productImage) {
            $attr['data-image'] = $this->cacheManager->getBrowserPath($productImage->getPath(), 'product');
        }

        return $attr;
    }

    /**
     * @param ProductInterface $product
     * @return int|null
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getDefaultVariationId(ProductInterface $product)
    {
        $defaultVariation = $product->getDefaultVariation();

        if (null !== $defaultVariation && $defaultVariation->isOrderable() && $defaultVariation->hasSupplier()) {
            return $defaultVariation->getId();
        }

        return null;
    }
}
