<?php

namespace AppBundle\Services;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AccessibilityService
 * @package AppBundle\Services
 */
class AccessibilityService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AccessibilityService constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {

        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Assortment $assortment
     * @return bool
     */
    public function isAssortmentAccessibleByUser(Assortment $assortment): bool
    {
        $customer = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;
        $company = $customer instanceof Customer ? $customer->getCompany() : null;

        if (null !== $company && $assortment->getOwner() === null && $company->getAssortments()->isEmpty()) {
            return true;
        }

        if (null === $company && $assortment->getOwner() === null) {
            return true;
        }

        return $company && $assortment->getCompanies()->contains($company);
    }

    /**
     * @param ProductInterface $product
     * @return bool
     */
    public function isProductAccessibleByUser(ProductInterface $product): bool
    {
        $customer = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;
        $company = $customer instanceof Customer ? $customer->getCompany() : null;

        if($product instanceof CompanyProductDecorator) {
            $product = $product->getEntity();
        }

        if($product instanceof CompanyProduct) {
            if(null === $company) {
                return false;
            }

            return $company->getProducts()->contains($product);
        }

        if(null !== $company && !$company->getAssortments()->isEmpty()) {
            return $company->getAssortments()->exists(function($key, Assortment $assortment) use($product) {
                /** @var AssortmentProduct $assortmentProduct */
                foreach($assortment->getAssortmentProducts() as $assortmentProduct) {
                    if($assortmentProduct->getProduct() === $product) {
                        return true;
                    }
                }

                return false;
            });
        }

        foreach($product->getAssortments() as $assortment) {
            if($this->isAssortmentAccessibleByUser($assortment)) {
                return true;
            }
        }

        return false;
    }
}