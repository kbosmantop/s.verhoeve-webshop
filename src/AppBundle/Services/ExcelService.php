<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class ExcelService
 * @package AppBundle\Services
 *
 * TODO: Change PHPEXCEL into PHPSpreadsheet.
 * https://topgeschenken.atlassian.net/browse/WEB-2058
 */
class ExcelService
{
    /** @var array $options */
    private $options;

    /** @var \PHPExcel $excel */
    private $excel;

    /**
     * @param array $options
     * @throws \PHPExcel_Exception
     */
    public function build(array $options)
    {
        $this->resolve($options);

        $this->excel = new \PHPExcel();
        $sheet = $this->excel->getActiveSheet();

        //build first row and set titles
        $col = 'A';

        foreach ($this->options['columns'] as $field => $name) {
            $sheet->setCellValue($col . '1', $name);
            $sheet->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }

        //fill sheet with data
        $propertyAccessor = new PropertyAccessor();

        $index = 2;

        foreach ($this->options['dataSet'] as $entity) {
            $col = 'A';

            foreach ($this->options['columns'] as $field => $name) {
                $sheet->setCellValue($col . $index, $propertyAccessor->getValue($entity, $field));
                $col++;
            }

            $index++;
        }
    }

    /**
     * @return BinaryFileResponse
     * @throws \PHPExcel_Writer_Exception
     */
    public function download()
    {
        $path = tempnam(sys_get_temp_dir(), 'export');

        $writer = new \PHPExcel_Writer_Excel5($this->excel);
        $writer->save($path);

        $response = new BinaryFileResponse($path, 200, [
            'Content-type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="' . $this->options['filename'] . '.xls"',
        ]);

        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @param array $data
     */
    private function resolve(array $data)
    {
        $resolver = new OptionsResolver();

        $resolver->setDefined([
            'dataSet',
            'columns',
            'filename',
        ]);

        $resolver->setRequired([
            'dataSet',
            'columns',
        ]);

        $resolver->setAllowedTypes('columns', ['array']);
        $resolver->setAllowedTypes('filename', ['string']);

        $resolver->setDefaults([
            'filename' => 'Export',
        ]);

        $this->options = $resolver->resolve($data);
    }

}