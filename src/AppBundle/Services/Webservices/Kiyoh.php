<?php


namespace AppBundle\Services\Webservices;

use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohReview;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp;
use GuzzleHttp\Psr7\Response;

class Kiyoh
{
    const SYNC_TYPE_INCREMENTAL = "incremental";
    const SYNC_TYPE_FULL = "all";

    /**
     * @var int $limit
     */
    private $limit = 25;

    /**
     * @var array $yesNoOptions
     */
    private $yesNoOptions = ['ja', 'yes', 'oui'];

    /**
     * @var GuzzleHttp\Client $guzzleClient
     */
    private $guzzleClient;

    /**
     * @var GuzzleHttp\Client $guzzleClient
     */
    private $entityManager;

    public function __construct(GuzzleHttp\Client $guzzleClient, EntityManagerInterface $entityManager)
    {
        $this->guzzleClient = $guzzleClient;
        $this->entityManager = $entityManager;
    }

    /**
     * @param KiyohCompany $kiyohCompany
     * @param string       $syncType
     */
    public function synchronize(KiyohCompany $kiyohCompany, $syncType = self::SYNC_TYPE_INCREMENTAL)
    {
        if ($syncType == self::SYNC_TYPE_FULL) {
            $this->limit = 'all';
        }

        $xmlFeed = $this->getXml($kiyohCompany);

        $kiyohCompany->setTotalScore($xmlFeed->company->total_score);
        $kiyohCompany->setTotalReviews($xmlFeed->company->total_reviews);
        $kiyohCompany->setTotalViews($xmlFeed->company->total_views);

        if ((bool)$xmlFeed->review_list->children()->count() === false) {
            return false;
        }

        $totalRecommendationScore = 0;

        foreach ($xmlFeed->review_list->children() as $review) {
            $kiyohReview = $this->entityManager->getRepository(KiyohReview::class)->findOneBy([
                'identifier' => (int)$review->id,
                'kiyohCompany' => $kiyohCompany,
            ]);

            if (!$kiyohReview) {
                $kiyohReview = new KiyohReview();

                if ($review->extra_options->productID) {
                    $product = $this->entityManager->getRepository(Product::class)->find($review->extra_options->productID);

                    $kiyohReview->setProduct($product);
                }

                if ($review->extra_options->reference) {
                    $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find($review->extra_options->reference);

                    $kiyohReview->setOrderCollection($orderCollection);
                }
            }

            $kiyohReview->setIdentifier((int)$review->id);
            $kiyohReview->setKiyohCompany($kiyohCompany);
            $kiyohReview->setName((string)$review->customer->name);
            $kiyohReview->setEmail((string)$review->customer->email);
            $kiyohReview->setDatetime(new \DateTime((string)$review->customer->date));
            $kiyohReview->setCity((string)$review->customer->place);
            $kiyohReview->setScore((float)$review->total_score);
            $kiyohReview->setPositiveFeedback((string)$review->positive);
            $kiyohReview->setNegativeFeedback((string)$review->negative);
            $kiyohReview->setRecommended(in_array(strtolower((string)$review->recommendation), $this->yesNoOptions));

            // Saving company details, if success proceed with company reviews
            $this->entityManager->persist($kiyohReview);

            $totalRecommendationScore += (int)(in_array(strtolower((string)$review->recommendation),
                $this->yesNoOptions));
        }

        if ($this->getLimit() == "all") {
            $totalReviews = count($xmlFeed->review_list->children());

            // Calculate total recommendation score, after all reviews are processed
            if ($totalRecommendationScore) {
                $totalRecommendationScore = (int)$totalRecommendationScore / $totalReviews * 100;
            }

            $kiyohCompany->setTotalReviews((int)$totalReviews);
            $kiyohCompany->setTotalRecommendationScore((float)$totalRecommendationScore);
        }

        $this->entityManager->flush();

        return $xmlFeed->review_list->children()->count();
    }


    private function getXml(KiyohCompany $kiyohCompany)
    {

        /** @var Response $response */
        $response = $this->guzzleClient->get($kiyohCompany->getXmlUrl(), [
            'query' => [
                'connectorcode' => $kiyohCompany->getConnectorCode(),
                'company_id' => $kiyohCompany->getIdentifier(),
                'reviewcount' => $this->getLimit(),
                'showextraquestions' => 0,
            ],
        ]);

        return new \SimpleXMLElement((string)$response->getBody());
    }

    private function getLimit()
    {
        return $this->limit;
    }
}
