<?php

namespace AppBundle\Services\Teamleader;

use AppBundle\Entity\Relation\Company;

/**
 * Class CompanyTransformer
 * @package AppBundle\Services\Teamleader
 */
class CompanyTransformer
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return array
     */
    public function transform(): array
    {
        return [
            'name' => $this->company->getName(),
            'email' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getEmail() : null,
            'vat_code' => $this->company->getVatNumber(),
            'country' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getCountry()->getCode() : null,
            'zipcode' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getPostcode() : null,
            'city' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getCity() : null,
            'street' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getStreet() : null,
            'number' => $this->company->getMainAddress() ? $this->company->getMainAddress()->getNumber() : null,
            'local_business_number' => $this->company->getChamberOfCommerceNumber(),
            'add_tag_by_string' => implode(',', $this->getTags()),
        ];
    }

    /**
     * @return array
     */
    private function getTags(): array
    {
        $tags = [];

        foreach ($this->company->getCustomers() as $customer) {
            if ($customer->getCustomerGroup() && $customer->getCustomerGroup()->getTeamleaderTag()) {
                if (\in_array($customer->getCustomerGroup()->getTeamleaderTag(), $tags, true)) {
                    continue;
                }

                $tags[] = $customer->getCustomerGroup()->getTeamleaderTag();
            }
        }

        return $tags;
    }
}
