<?php

namespace AppBundle\Services\Teamleader;

use AppBundle\Entity\Catalog\Product\Product;

/**
 * Class ProductTransformer
 * @package AppBundle\Services\Teamleader
 */
class ProductTransformer
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function transform(): array
    {
        return [
            'name' => $this->product->getDisplayName(),
            'price' => $this->product->getPrice(),
            'external_id' => $this->product->getSku(),
            'vat' => $this->getVatCode($this->product),
            'purchase_price' => $this->product->getPurchasePrice(),
        ];
    }

    /**
     * @param Product $product
     * @return string
     * @throws \Exception
     */
    private function getVatCode(Product $product): string
    {
        if ($product->getParent()) {
            $product = $product->getParent();
        }

        switch ($product->getVat()->getPercentage()) {
            case 0:
                return '00';
            case 6:
                return '06';
            case 21:
                return '21';
        }

        throw new \RuntimeException(sprintf("Unknown BTW percentage '%d'", $product->getVat()->getPercentage()));
    }
}
