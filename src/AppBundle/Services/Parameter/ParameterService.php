<?php

namespace AppBundle\Services\Parameter;

use AppBundle\Entity\Common\Parameter\ParameterEntity;
use AppBundle\Entity\Common\Parameter\ParameterEntityValue;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\YesNoType;
use BadMethodCallException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RuntimeException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ParameterService
 *
 * @package AppBundle\Services\Parameter
 */
class ParameterService
{
    use ContainerAwareTrait;

    /**
     * @var string $entityClass
     */
    private $entityClass;

    /**
     * @var int $entityId
     */
    private $entityId;

    /**
     * @var ParameterEntity[]
     */
    private $parameters;

    /**
     * @var array
     */
    private $parameterIndex = [];

    /**
     * @var ParameterEntityValue[]
     */
    private $values;

    /**
     * Accepts null for system parameters, Class reference for multiple entities or an entity for single entity use
     *
     * @param mixed|null $entity
     *
     * @return $this
     */
    public function setEntity($entity = null): self
    {
        if (null === $entity || \is_string($entity)) {
            if ($this->entityClass === $entity && $this->entityId === null && !empty($this->parameters)) {
                return $this;
            }

            $this->entityClass = $entity;
            $this->entityId = null;
        } else {
            if (!method_exists($entity, 'getId')) {
                throw new BadMethodCallException(sprintf('The method %s does not exist for this entity', 'getId()'));
            }

            $entityId = $entity->getId();
            $className = $this->getClassName($entity);
            if ($this->entityClass === $className && $this->entityId === $entityId) {
                return $this;
            }

            $this->entityClass = $className;
            $this->entityId = $entityId;
        }

        $this->clean();

        $parameters = $this->container->get('doctrine')->getRepository(ParameterEntity::class)->findBy([
            'entity' => $this->entityClass,
        ]);

        /** @var ParameterEntity $parameter */
        foreach ($parameters as $parameter) {
            $this->parameters[$parameter->getParameter()->getKey()] = $parameter;
        }

        return $this;
    }

    private function clean(): void
    {
        $this->parameters = [];
        $this->values = [];
    }

    /**
     * @param $object
     *
     * @return string
     */
    private function getClassName($object): string
    {
        return str_replace('Proxies\\__CG__\\', '', get_class($object));
    }

    /**
     * @param string $key
     * @param          $value
     * @param int|null $entityId
     *
     * @return string
     * @throws Exception
     */
    public function setValue(string $key, $value, int $entityId = null): string
    {
        if ($entityId) {
            $this->entityId = $entityId;
        }

        if (!$this->entityId && $this->entityClass) {
            throw new RuntimeException('The entityId is not set');
        }

        if ($this->paramsAreSet($key)) {
            $parameterValue = $this->getValueForKey($key);
            $em = $this->container->get('doctrine')->getManager();

            if (!$parameterValue) {
                $parameterValue = new ParameterEntityValue();
                $parameterValue->setEntity($this->entityId);
                $parameterValue->setParameterEntity($this->parameters[$key]);
                $parameterValue->setValue($value);
                $em->persist($parameterValue);
            } else {
                $parameterValue->setValue($value);
            }

            $em->flush();
            $this->values[$key] = $parameterValue;
        }

        return $value;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    private function paramsAreSet($key): bool
    {
        if (!$this->entityClass && $this->entityId) {
            throw new RuntimeException('The entity class is not set. Use SetEntity first.');
        }

        if (!isset($this->parameters[$key])) {
            throw new RuntimeException(sprintf('The parameter "%s" for entity %s does not exist', $key,
                $this->entityClass ?: 'System'));
        }

        return true;
    }

    /**
     * @param $key
     *
     * @return ParameterEntityValue
     */
    private function getValueForKey($key): ?ParameterEntityValue
    {
        if (!isset($this->values[$key])) {
            $this->values[$key] = $this->container->get('doctrine')->getRepository(ParameterEntityValue::class)->findOneBy([
                'parameterEntity' => $this->parameters[$key]->getId(),
                'entity' => $this->entityId,
            ]);
        }

        return $this->values[$key];
    }

    /**
     * @return ParameterEntityValue[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @throws Exception
     */
    public function getEntityValues(): array
    {
        if (null === $this->getParameters()) {
            return [];
        }

        $parameters = [];

        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        foreach ($this->getParameters() as $parameter) {
            $key = $parameter->getParameter()->getKey();
            $formType = $parameter->getParameter()->getFormType();
            $formTypeClass = $parameter->getParameter()->getFormTypeClass();

            if ($formType !== ColumnType::class) {
                $value = $this->getValue($key, $this->entityId);
                if ($formType === YesNoType::class) {
                    $value = (bool)$value;
                } elseif ($formType === EntityType::class) {
                    $value = $em->getRepository($formTypeClass)
                        ->createQueryBuilder('entity')
                        ->where('entity.id IN(:ids)')
                        ->setParameter('ids', explode(',', $value))
                        ->getQuery()
                        ->getResult();
                }
                $parameters[$key] = $value;
            }
        }
        return $parameters;
    }

    /**
     * @return null|ParameterEntity[]
     */
    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    /**
     * @param string $key
     * @param int|null $entityId
     *
     * @return string | null
     */
    public function getValue(string $key, int $entityId = null): ?string
    {
        if ($entityId) {
            $this->entityId = $entityId;
        }

        if ($this->paramsAreSet($key)) {
            $parameterValue = $this->getValueForKey($key);

            return $parameterValue ? $parameterValue->getValue() : null;
        }

        return null;
    }

    /**
     * Creates a parameter via DBAL based on the given array
     *
     * This method is only allowed to be used in migrations, for that reason it uses DBAL instead of the parameter
     * entities
     *
     * @param array $parameter
     *
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createViaArray(array $parameter): bool
    {
        $parameter = $this->resolveParameter($parameter);

        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();
        $conn->beginTransaction();

        /** @var QueryBuilder $qb */
        $qb = $conn->createQueryBuilder();

        $values = [
            '`key`' => $qb->createNamedParameter($parameter['key']),
            'form_type' => $qb->createNamedParameter($parameter['form_type']),
            'multiple' => $qb->createNamedParameter($parameter['multiple']),
        ];

        if (isset($parameter['form_type_class'])) {
            $values['form_type_class'] = $qb->createNamedParameter($parameter['form_type_class']);
        }

        $qb->insert('parameter')
            ->values($values)
            ->execute();


        $parameterEntityValues = [
            'parameter_id' => $qb->createNamedParameter($parameter['key']),
            'required' => $qb->createNamedParameter($parameter['required']),
            'label' => $qb->createNamedParameter($parameter['label']),
            'entity' => $qb->createNamedParameter($parameter['entity']),
        ];

        if (!empty($parameter['parent']) && isset($this->parameterIndex[$parameter['parent']])) {
            $parameterEntityValues['parent_id'] = $this->parameterIndex[$parameter['parent']];
        }
        if (!empty($parameter['default_value']) && isset($parameter['default_value'])) {
            $parameterEntityValues['default_value'] = $parameter['default_value'];
        }

        $qb->insert('parameter_entity')
            ->values($parameterEntityValues)
            ->execute();
        $parameterId = $conn->lastInsertId();

        $this->parameterIndex[$parameter['key']] = $parameterId;

        $conn->commit();

        if (null !== $parameter['value']) {

            $qb->insert('parameter_entity_value')
                ->values([
                    'parameter_entity_id' => $qb->createNamedParameter($parameterId),
                    'value' => $qb->createNamedParameter($parameter['value']),
                ])
                ->execute();
        }

        return true;
    }

    /**
     * Deletes a parameter by its key via DBAL
     *
     * This method is only allowed to be used in migrations, for that reason it uses DBAL instead of the parameter
     * entities
     *
     * @param $key
     *
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteByKey($key): bool
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();
        $conn->beginTransaction();

        $conn->exec("DELETE FROM `parameter_entity_value` WHERE `parameter_entity_id` IN(SELECT `id` FROM `parameter_entity` WHERE parameter_id = '" . $key . "')");
        $conn->commit();

        /** @var QueryBuilder $qb */
        $qb = $conn->createQueryBuilder();

        $qb->delete('parameter_entity')
            ->where('parameter_id = ?')
            ->setParameter(0, $key)
            ->execute();

        $qb->delete('parameter')
            ->where('`key` = ?')
            ->setParameter(0, $key)
            ->execute();

        return true;
    }

    /**
     * @param array $parameter
     *
     * @return array
     */
    private function resolveParameter(array $parameter): array
    {
        $resolver = new OptionsResolver();

        $resolver->setRequired([
            'key',
            'label',
            'entity',
        ]);

        $resolver->setDefaults([
            'required' => 1,
            'entity_id' => null,
            'value' => null,
            'form_type' => TextType::class,
            'form_type_class' => null,
            'parent' => null,
            'multiple' => 0,
            'default_value' => null,
        ]);

        $resolver->setAllowedTypes('required', ['boolean', 'int']);

        $resolver->setNormalizer('required', function (Options $options, $value) {
            void($options);
            return (int)$value;
        });

        return $resolver->resolve($parameter);
    }
}
