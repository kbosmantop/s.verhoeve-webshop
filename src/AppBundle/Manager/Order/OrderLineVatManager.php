<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\OrderLineVat;
use AppBundle\Services\PriceService;

/**
 * Class OrderLineVatManager
 * @package AppBundle\Manager\Order
 */
class OrderLineVatManager
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * OrderLineVatManager constructor.
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }
}