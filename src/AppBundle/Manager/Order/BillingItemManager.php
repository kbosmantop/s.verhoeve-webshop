<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\BillingItem;
use AppBundle\Entity\Order\BillingItemGroup;
use AppBundle\Entity\Order\Order;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BillingItemManager
 * @package AppBundle\Manager\Order
 */
class BillingItemManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * BillingItemManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Order $order
     * @param BillingItemGroup|null $billingItemGroup
     *
     * @return BillingItem|null
     */
    public function createByOrder(
        Order $order,
        ?BillingItemGroup $billingItemGroup = null
    ): ?BillingItem {
        $orderBillingItemGroups = $order->getOrderCollection()->getBillingItemGroups();
        $type = 'debit';

        if ($order->getStatus() === 'cancelled') {
            $type = 'credit';

            if ($order->getBillingItems()->isEmpty()) {
                return null;
            }
        }

        /** @var BillingItemGroup $group */
        foreach ($orderBillingItemGroups as $group) {
            if ($group->getBillingItems()->exists(static function ($key, BillingItem $b) use ($order, $type) {
                return $b->getOrder() === $order && $b->getTransactionType() === $type;
            })) {
                return null;
            }
        }

        $billingItem = new BillingItem();
        $billingItem->setOrder($order);
        $billingItem->setTransactionType($type);

        if ($billingItemGroup) {
            $billingItemGroup->addBillingItem($billingItem);
        }

        $this->entityManager->persist($billingItem);

        return $billingItem;
    }
}