<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\OrderCollectionLineVat;
use AppBundle\Services\PriceService;

/**
 * Class OrderCollectionLineVatManager
 * @package AppBundle\Manager\Order
 */
class OrderCollectionLineVatManager
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * OrderLineVatManager constructor.
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }
}