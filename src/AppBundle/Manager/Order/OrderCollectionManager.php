<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Services\Fraud\OrderFraudCheckService;
use AppBundle\Services\JobManager;
use AppBundle\Services\OrderManagerService;
use AppBundle\Services\Payment\PaymentRequestService;
use AppBundle\Services\PaymentService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use ReflectionException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class OrderCollectionManager
 * @package AppBundle\Manager\Order
 */
class OrderCollectionManager
{
    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * @var BillingItemGroupManager
     */
    protected $billingItemGroupManager;

    /**
     * @var OrderFraudCheckService
     */
    private $orderFraudCheck;

    /**
     * @var OrderManagerService
     */
    protected $orderManagerService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var JobManager
     */
    protected $jobManager;

    /**
     * @var PaymentRequestService
     */
    protected $paymentRequestService;

    /**
     * @var PaymentService
     */
    protected $paymentService;

    /**
     * OrderCollectionManager constructor.
     *
     * @param OrderLineManager $orderLineManager
     * @param OrderManager $orderManager
     * @param BillingItemGroupManager $billingItemGroupManager
     * @param OrderFraudCheckService $orderFraudCheck
     * @param OrderManagerService $orderManagerService
     * @param EntityManagerInterface $entityManager
     * @param JobManager $jobManager
     * @param PaymentRequestService $paymentRequestService
     * @param PaymentService $paymentService
     */
    public function __construct(
        OrderLineManager $orderLineManager,
        OrderManager $orderManager,
        BillingItemGroupManager $billingItemGroupManager,
        OrderFraudCheckService $orderFraudCheck,
        OrderManagerService $orderManagerService,
        EntityManagerInterface $entityManager,
        JobManager $jobManager,
        PaymentRequestService $paymentRequestService,
        PaymentService $paymentService
    )
    {
        $this->orderLineManager = $orderLineManager;
        $this->orderManager = $orderManager;
        $this->billingItemGroupManager = $billingItemGroupManager;
        $this->orderFraudCheck = $orderFraudCheck;
        $this->entityManager = $entityManager;
        $this->jobManager = $jobManager;
        $this->orderManagerService = $orderManagerService;
        $this->paymentRequestService = $paymentRequestService;
        $this->paymentService = $paymentService;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return OrderCollection
     */
    public function calculateTotals(OrderCollection $orderCollection): OrderCollection
    {
        $total = 0;
        $totalIncl = 0;

        foreach($orderCollection->getOrders() as $order) {
            if(!$this->orderManager->isCountable($order)) {
                continue;
            }

            $this->orderManager->calculateTotals($order);
            $total += $order->getTotalPrice();
            $totalIncl += $order->getTotalPriceIncl();
        }

        /** @var OrderCollectionLine $collectionLine */
        foreach($orderCollection->getLines() as $collectionLine) {
            if(null === $collectionLine->getRelatedOrder() || (null !== $collectionLine->getRelatedOrder() && $collectionLine->getRelatedOrder()->getStatus() !== 'cancelled')) {
                $total += ($collectionLine->getPrice() * $collectionLine->getQuantity()) - $collectionLine->getDiscountAmountExcl();
                $totalIncl += $collectionLine->getTotalPriceIncl();
            }
        }

        $orderCollection->setTotalPrice(round($total, 3));
        $orderCollection->setTotalPriceIncl(round($totalIncl, 2));

        return $orderCollection;
    }

    /**
     * @param OrderCollection $orderCollection
     * @return bool
     */
    public function hasCancellableInvoicePayment(OrderCollection $orderCollection): bool
    {
        foreach ($orderCollection->getOrders() as $order) {
            if (
                ! $order->getTwinfieldExportOrders()->isEmpty()
                || in_array($order->getStatus(), ['complete', 'pending', 'processed', 'sent'])
            ) {
                return false;
            }
        }

        return $this->hasAuthorizedInvoice($orderCollection);
    }

    /**
     * @param OrderCollection $orderCollection
     * @return float
     */
    public function calculateOutstanding(OrderCollection $orderCollection): float
    {
        $totalPriceIncl = 0;

        foreach ($orderCollection->getOrders() as $order) {
            if (false === $this->orderManager->isCountable($order)) {
                continue;
            }

            $totalPriceIncl += $order->getTotalPriceIncl();
        }

        /** @var OrderCollectionLine $collectionLine */
        foreach($orderCollection->getLines() as $collectionLine) {
            $totalPriceIncl += $collectionLine->getTotalPriceIncl();
        }

        $paid = 0;

        foreach ($orderCollection->getPayments() as $payment) {
            if (!$payment->getStatus() || !$payment->getStatus()->getCountable()) {
                continue;
            }

            if ($payment->getAmount()) {
                $paid += $payment->getAmount();
            } elseif (\in_array($payment->getPaymentmethod()->getCode(), ['invoice', 'internal'])) {
                $paid += $totalPriceIncl;
            }
        }

        return round($totalPriceIncl - $paid, 2);
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return bool
     */
    public function isFraudCheckable(OrderCollection $orderCollection)
    {
        $orders = $orderCollection->getOrders()->filter(function(Order $order) {
            return $this->orderManager->isFraudCheckable($order);
        });

        return false === $orders->isEmpty();
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return Collection|Order[]
     */
    public function findFraudCheckableOrders(OrderCollection $orderCollection)
    {
        return $orderCollection->getOrders()->filter(function (Order $order) {
            return $this->orderManager->isFraudCheckable($order);
        });
    }

    /**
     * @param OrderCollection $orderCollection
     * @return bool
     */
    public function getPaymentAmount(OrderCollection $orderCollection): bool
    {
        return $this->calculateOutstanding($orderCollection);
    }

    /**
     * @param OrderCollection $orderCollection
     * @return bool
     */
    public function isEditable(OrderCollection $orderCollection): bool
    {
        return $this->orderFraudCheck->canEditOrder($orderCollection);
    }

    /**
     * @param OrderCollection $orderCollection
     * @param bool            $auto
     * @return bool
     * @throws Exception
     */
    public function isProcessable(OrderCollection $orderCollection, bool $auto = false): bool
    {
        return $this->orderFraudCheck->canProcessOrder($orderCollection, $auto);
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return bool
     */
    public function needsFinalization(OrderCollection $orderCollection): bool
    {
        return $this->billingItemGroupManager->checkByOrderCollection($orderCollection);
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return bool
     */
    public function hasDraftOrders(OrderCollection $orderCollection): bool
    {
        return !$orderCollection->getOrders()->filter(static function(Order $order) {
            return $order->getStatus() === 'draft';
        })->isEmpty();
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return Payment|null
     * @throws ReflectionException
     * @throws Exception
     */
    public function createPaymentOrNull(OrderCollection $orderCollection): ?Payment
    {
        $payment = null;
        $outstandingAmount = $this->calculateOutstanding($orderCollection);
        $hasAuthorizedInvoice = $this->hasAuthorizedInvoice($orderCollection);

        if (false === $hasAuthorizedInvoice && $outstandingAmount > 0) {
            $customer = $orderCollection->getCustomer();
            $payment = $this->paymentRequestService->createPayment([
                'amount' => $outstandingAmount,
                'name' => $customer->getFullname(),
                'email' => $customer->getEmail(),
                'methods' => $this->paymentService->getMethods($orderCollection),
                'description' => '',
                'orderCollection' => $orderCollection,
            ]);
        }

        return $payment;
    }

    /**
     * @param OrderCollection $orderCollection
     */
    public function createBillingGroup(OrderCollection $orderCollection): void
    {
        if (false === $this->billingItemGroupManager->checkByOrderCollection($orderCollection)) {
            throw new BadRequestHttpException();
        }

        $this->billingItemGroupManager->createByOrderCollection($orderCollection);
    }

    /**
     * @param OrderCollection $orderCollection
     * @param Payment|null $payment
     *
     * @return bool
     * @throws Exception
     */
    public function sendFinalizeDraftMail(OrderCollection $orderCollection, ?Payment $payment): bool
    {
        $commandParameters = [
            $orderCollection->getId(),
            (null !== $payment) ? $payment->getId() : '0',
        ];

        $job = new Job('app:send-order-changed-mail', $commandParameters);
        $job->addRelatedEntity($orderCollection);
        $this->jobManager->addJob($job);

        return true;
    }

    /**
     * @param Order $order
     *
     * @return bool
     * @throws Exception
     */
    public function sendOrderCancellationMail(Order $order): bool
    {
        $job = new Job('app:send-order-cancellation-mail', [$order->getId()]);
        $job->addRelatedEntity($order);
        $this->jobManager->addJob($job);

        return true;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return array
     */
    public function getDraftOrders(OrderCollection $orderCollection): array
    {
        return $orderCollection->getOrders()->filter(static function(Order $order) {
            return $order->getStatus() === 'draft';
        })->toArray();
    }

    /**
     * @param OrderCollection $orderCollection
     * @param string $metaKey
     * @param string $message
     *
     * @return void
     */
    public function addMetaMessage(OrderCollection $orderCollection, string $metaKey, string $message): void
    {
        /** @var Order $orderCollection|$orderCollectionMetadata */
        $orderCollectionMetadata = $orderCollection->getMetadata();
        $orderCollectionMetadata[$metaKey][date('Y-m-d H:i')] = $message;
        $orderCollection->setMetadata($orderCollectionMetadata);

        $this->entityManager->persist($orderCollection);
        $this->entityManager->flush();
    }

    /**
     * @param OrderCollection $orderCollection
     * @param array $draftOrders
     *
     * @return void
     */
    public function processDraftOrders(OrderCollection $orderCollection, array $draftOrders): void
    {
        $outstandingAmount = $orderCollection->getPaymentAmount();
        $hasAuthorizedInvoice = $this->hasAuthorizedInvoice($orderCollection);

        foreach ($draftOrders as $draftOrderId) {
            /** @var Order $draftOrder */
            $draftOrder = $this->entityManager->getRepository(Order::class)->find($draftOrderId);

            $this->orderManager->submit($draftOrder);

            if ($hasAuthorizedInvoice || $outstandingAmount <= 0) {
                $this->orderManager->acceptPayment($draftOrder);
            }
        }
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return boolean
     */
    public function hasAuthorizedInvoice(OrderCollection $orderCollection): bool
    {
        foreach ($orderCollection->getPayments() as $payment) {
            if (
                $payment->getStatus()->getId() === 'authorized'
                && $payment->getPaymentmethod()->getCode() === 'invoice'
            ) {
                return true;
            }
        }

        return false;
    }
}
