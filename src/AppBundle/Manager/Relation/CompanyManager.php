<?php

namespace AppBundle\Manager\Relation;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Relation\CompanyRegistration;
use AppBundle\Services\Parameter\ParameterService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CompanyManager
 *
 * @package AppBundle\Manager\Relation
 */
class CompanyManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ParameterService  */
    private $parameterService;

    /**
     * CompanyManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ParameterService       $parameterService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterService $parameterService
    ) {
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
    }

    /**
     * TODO This function is not used yet but will replace existing functionality in CompanyRegistrationService and
     * CompanyController
     * @param CompanyRegistration $companyRegistration
     * @return Company
     */
    public function create(CompanyRegistration $companyRegistration): Company
    {
        $company = new Company();
        $company->setCompanyRegistration($companyRegistration);
        $this->createMainEstablishment($company);

        return $company;
    }

    /**
     * @param Company $company
     *
     * @return float
     */
    public function getAvailableCredit(Company $company): float
    {
        // return 0 if there is no limit
        /** @noinspection TypeUnsafeComparisonInspection */
        if (0 == ($creditLimit = $this->getCreditLimit($company))) {
            return $creditLimit;
        }

        return $creditLimit - $this->getOutstandingAmount($company);
    }

    /**
     * @param Company $company
     *
     * @return float
     */
    public function getUsedCredit(Company $company): float
    {
        // return 0 if there is no limit
        if (0 === ($creditLimit = $this->getCreditLimit($company))) {
            return $creditLimit;
        }

        return $this->getOutstandingAmount($company);
    }

    /**
     * @param Company $company
     *
     * @param float   $purchaseAmount
     * @return bool
     */
    public function canPurchaseByCredit(Company $company, float $purchaseAmount): bool
    {
        $availableCredit = $this->getAvailableCredit($company);

        /** @noinspection TypeUnsafeComparisonInspection */
        return (0 == $availableCredit || $purchaseAmount < $availableCredit);
    }

    /**
     * @param Company $company
     *
     * @return object
     */
    public function getCreditBalance(Company $company): object
    {
        return new class($this->getCreditLimit($company), $this->getUsedCredit($company), $this->getAvailableCredit($company)) {

            /** @var float */
            private $limit;

            /** @var float */
            private $used;

            /** @var float */
            private $available;

            /**
             * @param float $limit
             * @param float $used
             * @param float $available
             */
            public function __construct(float $limit, float $used, float $available)
            {
                $this->limit = $limit;
                $this->used = $used;
                $this->available = $available;
            }

            /**
             * @return float
             */
            public function getLimit(): float
            {
                return $this->limit;
            }

            /**
             * @return float
             */
            public function getUsed(): float
            {
                return $this->used;
            }

            /**
             * @return float
             */
            public function getAvailable(): float
            {
                return $this->available;
            }

            /**
             * @return float
             */
            public function getUsedPercentage(): float
            {
                /** @noinspection TypeUnsafeComparisonInspection */
                if ($this->used == 0 || $this->limit == 0) {
                    $percentage = 0;
                } else {
                    $percentage = $this->used / $this->limit * 100;
                }

                return round($percentage, 2);
            }

            /**
             * @return float
             */
            public function getAvailablePercentage(): float
            {
                return 100 - $this->getUsedPercentage();
            }
        };
    }

    /**
     * Get the outstanding amount for the given company
     *
     * @param Company $company
     * @param bool $includingVat
     *
     * @return float
     * @throws NonUniqueResultException
     */
    public function getOutstandingAmount(Company $company, bool $includingVat = false): float
    {
        $outstanding = 0;

        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('ord');
        $qb->leftJoin('ord.orderCollection', 'order_collection');
        $qb->select('COUNT(ord.id)');
        $qb->andWhere('order_collection.company = :company');
        $qb->setParameter('company', $company->getId());

        $orderCount = $qb->getQuery()->getSingleScalarResult();
        if ((int) $orderCount === 0) {
            return $outstanding;
        }

        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('ord');

        if ($includingVat) {
            $qb->select('SUM(ord.totalPriceIncl)');
        } else {
            $qb->select('SUM(ord.totalPrice)');
        }

        $qb->leftJoin('ord.billingItems', 'billing_items');
        $qb->leftJoin('ord.orderCollection', 'order_collection');

        $andX = $qb->expr()->andX(
            $qb->expr()->eq('order_collection.company', $company->getId()),
            $qb->expr()->isNull('billing_items.id')
        );

        $qb->andWhere($andX);

        return (float)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get the credit limit for the given company
     *
     * @param Company $company
     * @return float
     */
    private function getCreditLimit(Company $company): float
    {
        $creditLimitSystem = $this->parameterService->setEntity()->getValue('company_credit_limit');

        // if for some reason no(/invalid) value is set for system parameter fallback
        if (null === $creditLimitSystem || false === \is_numeric($creditLimitSystem)) {
            $creditLimitSystem = Company::CREDIT_LIMIT;
        }

        // If no company specific limit is set, use system limit
        if (null === ($creditLimit = $company->getCreditLimit())) {
            $creditLimit = (float) $creditLimitSystem;
        }

        return $creditLimit;
    }

    /**
     * @param Company $company
     * @return float
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getTotalRevenue(Company $company): float
    {
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        $qb->select('sum(o.totalPriceIncl)')
            ->leftJoin('o.orderCollection', 'oc')
            ->andWhere('oc.company = :company')
            ->setParameter('company', $company->getId());

        return (float)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Company   $company
     * @param int       $limit
     * @return mixed
     */
    public function getRecentOrderCollections(Company $company, $limit = 5)
    {
        $qb = $this->entityManager->getRepository(OrderCollection::class)->createQueryBuilder('oc');
        $qb->andWhere('oc.company = :company')
            ->setMaxResults($limit)
            ->setParameters([
                'company' => $company,
            ])
            ->orderBy('oc.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Get or create main establishments
     *
     * @param Company $company
     * @return CompanyEstablishment
     */
    public function getMainEstablishment(Company $company)
    {
        $mainEstablishments = $company->getEstablishments()->filter(function ($establishment) {
            /** @var CompanyEstablishment $establishment */
            return $establishment->isMain() === true;
        });

        if (!$mainEstablishments->isEmpty()) {
            return $mainEstablishments->first();
        }

        // Fallback functionality

        return $this->createMainEstablishment($company);
    }

    /**
     * @param Company $company
     * @return CompanyEstablishment|null
     */
    private function createMainEstablishment(Company $company): ?CompanyEstablishment
    {
        if ($company->getCompanyRegistration() !== null) {
            $establishment = new CompanyEstablishment();
            $establishment->setName($company->getCompanyRegistration()->getName());
            $establishment->setAddress($company->getCompanyRegistration()->getMainAddress());
            $establishment->setEstablishmentNumber($company->getCompanyRegistration()->getEstablishmentNumber());
            $establishment->setCompany($company);
            $establishment->setIsVerified(true);
            $company->addEstablishment($establishment);
            $this->entityManager->persist($establishment);

            return $establishment;
        }
        return null;
    }
}
