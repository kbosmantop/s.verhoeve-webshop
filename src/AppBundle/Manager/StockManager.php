<?php

namespace AppBundle\Manager;

use AppBundle\Cache\Adapter\TagAwareAdapter;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Entity\Stock\Log;
use AppBundle\Entity\Stock\Stock;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Statement;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class StockManager
 * @package AppBundle\Manager
 */
class StockManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TagAwareAdapter */
    private $cache;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var bool */
    private $cacheEnabled = true;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface   $entityManager
     * @param TagAwareAdapter $cache
     * @param TokenStorageInterface    $tokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TagAwareAdapter $cache, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
        $this->tokenStorage = $tokenStorage;
    }

    public function disableCache(): void
    {
        $this->cacheEnabled = false;
    }

    /**
     * @param Product $product
     *
     * @return int
     * @throws OptimisticLockException
     */
    public function getPhysicalStock(Product $product): ?int
    {
        $productStock = $this->getProductStock($product);

        return $productStock ? $productStock->getPhysicalStock() : null;
    }

    /**
     * @param Product $product
     *
     * @return int
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getVirtualStock(Product $product): int
    {
        // TODO CHANGE CACHE TAG
        $cachedVirtualStock = $this->cache->getItem('stock.virtual.product_' . $product->getId());

        if (!$this->cacheEnabled || !$cachedVirtualStock->isHit()) {
            $virtualStock = $this->getPhysicalStock($product);
            $virtualStock -= $this->calculateCartReservation($product);
            $virtualStock -= $this->calculateOrderReservation($product);

            if ($this->cacheEnabled) {
                $cachedVirtualStock->set($virtualStock);
                $cachedVirtualStock->expiresAfter(60);
                $cachedVirtualStock->tag('stock.product_' . $product->getId());

                $this->cache->save($cachedVirtualStock);
            }
        } else {
            $virtualStock = $cachedVirtualStock->get();
        }

        return $virtualStock;
    }

    /**
     * @param Product $product
     * @return string
     * @throws OptimisticLockException
     */
    public function getStockStatus(Product $product)
    {
        if ($product->getPhysicalStock() < $product->getStockThreshold()) {
            if (
                (null === $product->getParent() && $product->getNoStockBackorder())
                || (null !== $product->getParent() && $product->getParent()->getNoStockBackorder())
            ) {
                return Stock::STATUS_WARNING;
            }

            return Stock::STATUS_DANGER;
        }

        return Stock::STATUS_SUCCESS;
    }

    /**
     * @param Product $product
     * @throws InvalidArgumentException
     */
    public function invalidateCache(Product $product): void
    {
        $this->cache->invalidateTags([
            'stock.product_' . $product->getId(),
        ]);
    }

    /**
     * @param Product $product
     *
     * @return int
     * @throws OptimisticLockException
     */
    public function getThreshold(Product $product): ?int
    {
        $productStock = $this->getProductStock($product);

        return $productStock ? $productStock->getThreshold() : null;
    }

    /**
     * @param Product $product
     * @param int
     * @throws OptimisticLockException
     */
    public function setThreshold(Product $product, $threshold): void
    {
        $productStock = $this->getProductStock($product);
        $productStock->setThreshold($threshold);

        $this->entityManager->flush();
    }

    /**
     * @param Product    $product
     * @param            $amount
     * @param Order|null $relatedOrder
     * @param null       $action
     * @throws \RuntimeException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    public function addPhysicalStock(Product $product, $amount, Order $relatedOrder = null, $action = null): void
    {
        if (!($amount > 0)) {
            throw new \RuntimeException(sprintf('Invalid amount %s, must be positive', $amount));
        }

        $productStock = $this->getProductStock($product);

        $this->setPhysicalStock($product, $productStock->getPhysicalStock() + $amount, $relatedOrder, $action);
    }

    /**
     * @param Product    $product
     * @param            $amount
     * @param Order|null $relatedOrder
     * @param null       $action
     * @throws \RuntimeException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    public function subtractPhysicalStock(Product $product, $amount, Order $relatedOrder = null, $action = null): void
    {
        if (!($amount > 0)) {
            throw new \RuntimeException(sprintf('Invalid amount %s, must be positive', $amount));
        }

        $productStock = $this->getProductStock($product);

        $this->setPhysicalStock($product, $productStock->getPhysicalStock() - $amount, $relatedOrder, $action);
    }

    /**
     * @param Product    $product
     * @param            $physicalStock
     * @param Order|null $relatedOrder
     * @param null       $action
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function setPhysicalStock(Product $product, $physicalStock, Order $relatedOrder = null, $action = null): void
    {
        $productStock = $this->getProductStock($product);

        if (null === $productStock) {
            return;
        }

        $amount = $physicalStock - $productStock->getPhysicalStock();

        $productStock->setPhysicalStock($physicalStock);

        $this->entityManager->flush();

        $this->invalidateCache($product);

        $productStockLog = new Log();
        $productStockLog->setProduct($product);
        $productStockLog->setAmount($amount);
        $productStockLog->setAction($action);

        if ($relatedOrder) {
            $productStockLog->setOrder($relatedOrder);
        } else {
            $productStockLog->setUser($this->getUser());
        }

        $this->entityManager->persist($productStockLog);
        $this->entityManager->flush();
    }

    /**
     * @param Product $product
     *
     * @return Stock|null
     */
    public function getProductStock(Product $product): ?Stock
    {
        if(!$product->getHasStock()) {
            return null;
        }

        $productStock = $this->entityManager->getRepository(Stock::class)->findOneBy([
            'product' => $product,
        ]);

        if (!$productStock) {
            $productStock = new Stock();
            $productStock->setProduct($product);
            $productStock->setPhysicalStock(0);
            $productStock->setThreshold(0);

            $this->entityManager->persist($productStock);
            $this->entityManager->flush();
        }

        return $productStock;
    }

    /**
     * @return ArrayCollection|Product[]
     */
    public function getProducts()
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Product::class)->createQueryBuilder('product');
        $qb->addSelect('COALESCE (stock.physicalStock - stock.threshold, 9999) AS priority');

        $qb->leftJoin('product.parent', 'parent');
        $qb->leftJoin('product.stock', 'stock');

        $qb->where($qb->expr()->andX(
            'product.hasStock = 1',
            'product.type IS NULL'
        ));

        $qb->orWhere($qb->expr()->andX(
            'parent.hasStock = 1',
            "parent.type = 'variable'"
        ));

        $qb->orderBy('priority', 'ASC');

        $stock = $qb->getQuery()->getResult();
        return array_map(function($stock){
            return $stock[0];
        }, $stock);
    }

    /**
     * @param Product $product
     *
     * @return int
     * @throws DBALException
     */
    private function calculateCartReservation(Product $product): int
    {
        /** @var Statement $stmt */
        $stmt = $this->entityManager->getConnection()->prepare('
            SELECT SUM(quantity)
            FROM `cart`
            LEFT JOIN `cart_order` ON cart_order.cart_id = cart.id
            LEFT JOIN `cart_order_line` ON cart_order_line.cart_order_id = cart_order.id
            WHERE product_id = :productId
              AND cart.updated_at >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)      
              AND cart.status_id IS NULL
              AND cart.deleted_at IS NULL
              AND cart_order.deleted_at IS NULL
              AND cart_order_line.deleted_at IS NULL
        ');

        $stmt->execute([
            'productId' => $product->getId(),
        ]);

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param Product $product
     *
     * @return integer
     * @throws DBALException
     */
    private function calculateOrderReservation(Product $product): int
    {
        /** @var Statement $stmt */
        $stmt = $this->entityManager->getConnection()->prepare("
            SELECT SUM(quantity)
            FROM `order_collection`
            LEFT JOIN `order` ON `order`.order_collection_id = `order_collection`.id
            LEFT JOIN `order_line` ON order_line.order_id = `order`.id
            WHERE product_id = :productId
              AND (
               `order_collection`.updated_at >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)                
                OR `order`.status = 'new' 
              )
              AND `order_collection`.deleted_at IS NULL
              AND `order`.deleted_at IS NULL
              AND order_line.deleted_at IS NULL
        ");

        $stmt->execute([
            'productId' => $product->getId(),
        ]);

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param ProductInterface $product
     *
     * @return bool
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function isOrderable(ProductInterface $product): bool
    {
        if($product instanceof CompanyProduct) {
            $product = $product->getRelatedProduct();
        }

        if ($product->isCombination()) {
            $stockManager = $this;

            if (null === $product->getParent() && $product->getVariations()->isEmpty()) {
                return false;
            }

            return !$product->getCombinations()->filter(function (Combination $combination) use (
                $stockManager
            ) {
                return $stockManager->isOrderable($combination->getProduct());
            })->isEmpty();
        }

        if(null !== $product->getParent()) {
            if(!$product->getHasStock()) {
                return true;
            }

            if ($product->getNoStockBackorder()) {
                return true;
            }

            if ($this->getVirtualStock($product) >= $product->getMinQuantity()) {
                return true;
            }
        }

        return false !== $this->hasOrderableVariation($product);
    }

    /**
     * Check wether a variation is orderable and has an available supplier.
     *
     * @param ProductInterface $product
     * @return bool
     */
    public function hasOrderableVariation(ProductInterface $product): bool
    {
        if (false !== $product->getVariations()->isEmpty()) {
            return false;
        }

        foreach ($product->getVariations() as $variation) {
            if ($this->isOrderable($variation)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Product $product
     *
     * @return bool
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function isQuotationAvailable(Product $product): bool
    {
        if (!$product->getHasStock()) {
            return true;
        }

        if ($product->getNoStockQuotation()) {
            return true;
        }

        if ($this->getVirtualStock($product) > 0) {
            return true;
        }

        return false;

    }

    /**
     * @return User|null
     */
    private function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            return null;
        }

        if (!$user = $token->getUser()) {
            return null;
        }

        if (!($user instanceof User)) {
            return null;
        }

        return $user;
    }

    /**
     * @param Product $product
     * @return Log[]|array
     */
    public function getLog(Product $product)
    {
        return $this->entityManager->getRepository(Log::class)->findBy([
            'product' => $product->getId(),
        ], ['createdAt' => 'DESC']);
    }

}
