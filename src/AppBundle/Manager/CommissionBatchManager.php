<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class CommissionBatchManager
 * @package AppBundle\Manager
 */
class CommissionBatchManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CommissionBatchManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return bool
     */
    public function isBatchRunning(): bool
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Job::class)->createQueryBuilder('j');
        $qb->andWhere('j.command = :command')
            ->setParameter('command', 'app:finance:commission:generate-batch')
            ->andWhere('j.state IN(:statuses)')
            ->setParameter('statuses', ['pending', 'running']);

        $result = $qb->getQuery()->getResult();

        return !empty($result);
    }
}