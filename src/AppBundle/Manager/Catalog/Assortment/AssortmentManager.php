<?php

namespace AppBundle\Manager\Catalog\Assortment;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AssortmentManager
 * @package AppBundle\Manager\Catalog\Assortment
 */
class AssortmentManager
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var TokenStorageInterface $tokenStorage
     */
    protected $tokenStorage;

    /**
     * @var ProductPriceManager
     */
    private $productPriceManager;

    /**
     * AssortmentManager constructor.
     * @param EntityManager       $entityManager
     * @param TokenStorage        $tokenStorage
     * @param ProductPriceManager $productPriceManager
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorage $tokenStorage, ProductPriceManager $productPriceManager)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->productPriceManager = $productPriceManager;
    }

    /**
     * @param Assortment $assortment
     * @return array
     */
    public function getFilters(Assortment $assortment)
    {
        if (!$assortment->isShowFilter()) {
            return [];
        }

        $assortmentProducts = $assortment->getAssortmentProducts();

        $propertySets = new ArrayCollection();
        $productIds = [];

        foreach ($assortmentProducts as $assortmentProduct) {
            $product = $assortmentProduct->getProduct();
            if ($product && $product->getPropertySet()) {
                $productIds[] = $product->getId();

                foreach ($product->getVariations() as $variation) {
                    $productIds[] = $variation->getId();
                }

                $propertySet = $product->getPropertySet();

                if (!$propertySets->contains($propertySet)) {
                    $propertySets->add($propertySet);
                }
            }
        }

        $properties = [];
        /**
         * @var $propertySet ProductgroupPropertySet
         */
        foreach ($propertySets as $propertySet) {
            /**
             * @var $property ProductgroupProperty
             */
            foreach ($propertySet->getProductgroupProperties() as $property) {
                if ($property->getFilterable()) {
                    $properties[$property->getId()] = $property->translate()->getDescription();
                }
            }
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->entityManager->getRepository(ProductProperty::class);

        $qb = $entityRepository->createQueryBuilder('p');

        $qb->andWhere('p.productgroupProperty IN(:propertyIds)');
        $qb->andWhere('p.product IN(:productIds)');
        $qb->andWhere('p.productgroupPropertyOption is not null');
        $qb->setParameter('propertyIds', array_keys($properties));
        $qb->setParameter('productIds', $productIds);
        $productProperties = $qb->getQuery()->getResult();

        $filters = [];
        /**
         * @var ProductProperty $productProperty
         */
        foreach ($productProperties as $productProperty) {
            $groupPropertyId = $productProperty->getProductgroupProperty()->getId();

            if (!isset($filters[$groupPropertyId])) {
                $filters[$groupPropertyId] = [
                    'filter' => $properties[$groupPropertyId],
                    'values' => [],
                ];
            }

            $propertyOptionId = $productProperty->getProductgroupPropertyOption()->getId();
            if (!isset($filters[$groupPropertyId]['values'][$propertyOptionId])) {
                $filters[$groupPropertyId]['values'][$propertyOptionId] = [
                    'name' => $productProperty->getProductgroupPropertyOption()->getValue(),
                    'count' => 0,
                ];
            }

            $filters[$groupPropertyId]['values'][$propertyOptionId]['count']++;
        }

        return $filters;
    }

    /**
     * @param Assortment $assortment
     * @return float|null
     * @throws \Exception
     */
    public function getMinPrice(Assortment $assortment): ?float
    {
        $product = null;
        foreach($assortment->getAssortmentProducts() as $assortmentProduct) {
            $currentProduct = $assortmentProduct->getProduct();
            if($currentProduct instanceof CompanyProduct){
                $currentProduct = $currentProduct->getRelatedProduct();
            }

            if($currentProduct !== null) {
                /** @var Product $variation */
                foreach ($currentProduct->getVariations() as $variation) {
                    if (null === $product || $variation->getPrice() < $currentProduct->getPrice()) {
                        $product = $variation;
                    }
                }
            }
        }

        return $this->productPriceManager->getPrice($product);
    }

    /**
     * @param Assortment $assortment
     * @return float|null
     * @throws \Exception
     */
    public function getMaxPrice(Assortment $assortment): ?float
    {
        /** @var Product $product */
        $product = null;
        foreach($assortment->getAssortmentProducts() as $assortmentProduct) {
            $currentProduct = $assortmentProduct->getProduct();
            if($currentProduct instanceof CompanyProduct){
                $currentProduct = $currentProduct->getRelatedProduct();
            }

            if($currentProduct !== null) {
                /** @var Product $variation */
                foreach ($currentProduct->getVariations() as $variation) {
                    if (null === $product || $variation->getPrice() > $currentProduct->getPrice()) {
                        $product = $variation;
                    }
                }
            }
        }

        return $this->productPriceManager->getPrice($product);
    }
}
