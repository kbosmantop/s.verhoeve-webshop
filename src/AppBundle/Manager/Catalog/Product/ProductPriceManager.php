<?php

namespace AppBundle\Manager\Catalog\Product;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Order\CartOrderManager;
use AppBundle\Model\Product\ProductModel;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\PriceService;
use AppBundle\Services\SiteService;
use Nelmio\Alice\Instances\Populator\Methods\Custom;
use RuleBundle\Service\ResolverService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ProductPriceManager
 * @package AppBundle\Manager\Catalog\Product
 */
class ProductPriceManager
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * @var ResolverService
     */
    protected $ruleResolver;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var VoucherService
     */
    protected $voucherService;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var CartOrderManager
     */
    private $cartOrderManager;

    /**
     * ProductPriceManager constructor.
     * @param PriceService          $priceService
     * @param ResolverService       $ruleResolver
     * @param TokenStorageInterface $tokenStorage
     * @param VoucherService        $voucherService
     * @param VatGroupService       $vatGroupService
     * @param ProductManager        $productmanager
     * @param CartOrderManager      $cartOrderManager
     */
    public function __construct(
        PriceService $priceService,
        ResolverService $ruleResolver,
        TokenStorageInterface $tokenStorage,
        VoucherService $voucherService,
        VatGroupService $vatGroupService,
        ProductManager $productmanager,
        CartOrderManager $cartOrderManager
    ) {
        $this->priceService = $priceService;
        $this->ruleResolver = $ruleResolver;
        $this->tokenStorage = $tokenStorage;
        $this->voucherService = $voucherService;
        $this->vatGroupService = $vatGroupService;
        $this->productManager = $productmanager;
        $this->cartOrderManager = $cartOrderManager;
    }

    /**
     *
     * @param ProductInterface $product
     *                                    included
     * @param bool             $withDiscount
     * @param null             $showVat
     * @param CartOrder|null   $cartOrder @todo remove when pricelist is availabe
     * @return float
     * @throws \Exception
     */
    public function getPrice(
        ProductInterface $product,
        $withDiscount = null,
        $showVat = null,
        CartOrder $cartOrder = null
    ): float {
        //get cheapest variation to show get correct display price

        /** @var Product $product */
        if (false === ($product instanceof ProductCard) && null === $product->getParent()) {
            $product = $this->productManager->decorateProduct($product->getCheapestVariation());
        }

        $price = $product->getPrice();

        $company = $this->getCompany();
        if (null === $withDiscount && null !== $company) {
            $withDiscount = $this->getDisplayMode() !== 'original';
        }

        if ($withDiscount && $this->isDiscountApplicable($product)) {
            $discount = $this->getDiscount($product);

            if (null !== $discount) {
                $price *= (1 - $discount->getValue() / 100);
            }
        }

        $vatGroup = $this->vatGroupService->determineVatGroupForProduct($product);
        $vatRate = $vatGroup->getRateByDate();

        //TODO remove when pricelist is available
        if ($product instanceof TransportType && null !== $cartOrder && null !== $company && !$company->getTransportTypes()->isEmpty()) {
            $companyDeliveryPrice = $this->cartOrderManager->getCompanyDeliveryPrice($cartOrder, $product, $company);

            if($companyDeliveryPrice !== null) {
                return $this->priceService->getRawDisplayPrice($companyDeliveryPrice, $vatRate, $showVat);
            }
        }

        return $this->priceService->getRawDisplayPrice($price, $vatRate, $showVat);
    }

    /**
     * Discount excluding VAT
     *
     * @param ProductInterface $product
     * @param float|null       $price
     * @return Discount|null
     * @throws \Exception
     */
    public function getDiscount(ProductInterface $product, float $price = null): ?Discount
    {
        $discountReturn = null;

        $company = $this->getCompany();
        if (null !== $company) {
            foreach ($company->getDiscounts() as $discount) {
                //only percentage is allowed for product discounts
                if (null !== $discount->getRule() && $discount->getType() === 'percentage' && $discount->getRule()->getStart() === Product::class &&
                        $this->ruleResolver->satisfies($product, $discount->getRule())) {
                    if (null === $discountReturn || $discount->getValue() > $discountReturn->getValue()) {
                        $discountReturn = $discount;
                    }
                }
            }
        }

        return $discountReturn;
    }

    /**
     * @param ProductInterface $product
     * @return bool
     */
    public function isDiscountApplicable(ProductInterface $product): bool
    {
        $company = $this->getCompany();
        if (null !== $company) {
            return $company->getDiscounts()->filter(function (Discount $discount) use (
                $product
            ) {
                return null !== $discount->getRule() && $discount->getRule()->getStart() === Product::class &&
                        $this->ruleResolver->satisfies($product, $discount->getRule());
            })->count();
        }

        return false;
    }

    /**
     * @return null|string
     */
    public function getDisplayMode()
    {
        return $this->getCompany() ? $this->getCompany()->getPriceDisplayMode() : null;
    }

    /**
     * @return Company|null
     */
    private function getCompany(): ?Company
    {
        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        if ($user instanceof Customer) {
            return $user->getCompany();
        }

        return null;
    }
}
