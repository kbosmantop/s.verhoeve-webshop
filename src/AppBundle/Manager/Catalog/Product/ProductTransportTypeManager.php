<?php

namespace AppBundle\Manager\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTransportType;
use AppBundle\Services\SiteService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class ProductTransportTypeManager
 * @package Appbundle\Catalog\Product
 */
class ProductTransportTypeManager
{
    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * ProductTransportTypeManager constructor.
     *
     * @param SiteService $siteService
     */
    public function __construct(SiteService $siteService)
    {
        $this->siteService = $siteService;
    }

    /**
     * @param Product $product
     *
     * @return ProductTransportType[]|Collection
     */
    public function getProductTransportTypesForProduct(Product $product): Collection
    {
        if (!$product->getProductTransportTypes()->isEmpty()) {
            return $product->getProductTransportTypes();
        }

        if (null !== $product->getProductgroup()) {
            return $product->getProductgroup()->getProductTransportTypes();
        }

        //TODO remove when productgroups are configured for the transportypes
        $site = $this->siteService->determineSite();

        $transportTypes = $site ? [$site->getDefaultProductTransportType()] : [];
        return new ArrayCollection($transportTypes);
    }
}