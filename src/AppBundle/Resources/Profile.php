<?php

namespace AppBundle\Resources;

/**
 * TODO Copernica code can be trashed
 * Class Profile
 * @package AppBundle\Resources
 */
class Profile extends AbstractResource
{
    private $ID = 17705224;

    public function addInterest()
    {
        global $kernel;

        $this->guzzleClient = $kernel->getContainer()->get('eight_points_guzzle.client.copernica');

        $url = 'https://api.copernica.com/profile/' . $this->ID . '/changeinterests';

        $json = [
            'topfruit_nl' => true,
        ];

        $response = $this->guzzleClient->post($url, [
            "json" => $json,
            "query" => [
                'access_token' => 'f7fa86e3c091ee9d3f3da9baade051dbaa8a8d4a31d4fe27575b3af922edac72b7771e3e29f2570237ba9456b386fec7ec9ed72c88e451bbda3d9ebcb6b26380',
            ],
        ]);

        print $response->getStatusCode();
    }
}
