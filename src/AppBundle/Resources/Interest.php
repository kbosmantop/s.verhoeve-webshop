<?php

namespace AppBundle\Resources;

/**
 * TODO Copernica code can be trashed
 * Class Interest
 * @package AppBundle\Resources
 */
class Interest extends AbstractResource
{
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }
}