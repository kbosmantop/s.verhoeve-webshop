<?php

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\CountryTranslation;
use Symfony\Component\Intl\Intl;

$countryCodes = array_keys(Intl::getRegionBundle()->getCountryNames());

$return = [
    Country::class => [],
    CountryTranslation::class => [],
];

foreach ($countryCodes as $countryCode) {
    $return[Country::class]['country_' . strtolower($countryCode)] = [
        'id' => $countryCode,
    ];

    foreach (['nl_NL', 'nl_BE', 'fr_BE', 'en_US'] as $locale) {
        $return[CountryTranslation::class]['country_' . strtolower($countryCode) . "-" . $locale] = [
            'translatable' => '@country_' . strtolower($countryCode),
            'name' => Locale::getDisplayRegion('-' . $countryCode, substr($locale, 0, 2)),
            'locale' => $locale,
        ];
    }
}

return $return;
