<?php

global $kernel;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use Doctrine\ORM\EntityManagerInterface;

$container = $kernel->getContainer();
$return = [];

$ruleData = [
    'conditionSet' => [
        'condition' => 'AND',
        'rules' => [
            [
                'operator' => 'greater',
                'id' => '__result.order.collection.totalPriceIncl',
                'field' => '__result.order.collection.totalPriceIncl',
                'value' => '10',
                'input' => 'text',
                'type' => 'string',
            ],
        ],
    ],
    'rule' => [
        'description' => '10% winkelwagen korting',
        'start' => OrderLineInterface::class,
    ],
];


/** @var EntityManagerInterface $em */
$em = $container->get('doctrine')->getManager();

$rule = $container->get('rule.transformer')->importData($ruleData);

$return[Discount::class]['discount_cart'] = [
    'type' => 'percentage',
    'value' => 10,
    'rule' => $rule->getId(),
];

$validFrom = new DateTime();
$validUntil = new DateTime();
$validUntil->modify('+10 days');

$return[Voucher::class]['voucher_cart_one'] = [
    'discount' => '@discount_cart',
    'description' => '10% winkelwagen korting',
    'code' => 'AA12345',
    'valid_from' => $validFrom,
    'valid_until' => $validUntil,
    'usage' => 'unlimited',
];

$return[Voucher::class]['voucher_cart_two'] = [
    'discount' => '@discount_cart',
    'description' => 'Eenmalig 10% winkelwagen korting',
    'code' => 'BB12345',
    'valid_from' => $validFrom,
    'valid_until' => $validUntil,
    'usage' => 'once',
];

$return[Voucher::class]['voucher_cart_three'] = [
    'discount' => '@discount_cart',
    'description' => '10% winkelwagen korting (gelimiteerd gebruik)',
    'code' => 'CC12345',
    'valid_from' => $validFrom,
    'valid_until' => $validUntil,
    'usage' => 'limited',
    'limit' => 10,
];

//Restitution discounts
$restitutionRuleData = [
    'conditionSet' => [
        'condition' => 'AND',
        'rules' => [
            [
                'operator' => 'greater',
                'id' => '__result.order.collection.totalPriceIncl',
                'field' => '__result.order.collection.totalPriceIncl',
                'value' => '10',
                'input' => 'text',
                'type' => 'string',
            ],
        ],
    ],
    'rule' => [
        'description' => 'Restitution',
        'start' => OrderLineInterface::class,
    ],
];

$restitutionRule = $container->get('rule.transformer')->importData($restitutionRuleData);

$return[Discount::class]['restitution_amount_5'] = [
    'description' => 'Restitution 5€',
    'type' => 'amount',
    'value' => 5,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_amount_10'] = [
    'description' => 'Restitution 10€',
    'type' => 'amount',
    'value' => 10,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_amount_15'] = [
    'description' => 'Restitution 15€',
    'type' => 'amount',
    'value' => 15,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_amount_25'] = [
    'description' => 'Restitution 25€',
    'type' => 'amount',
    'value' => 25,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_amount_50'] = [
    'description' => 'Restitution 50€',
    'type' => 'amount',
    'value' => 50,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_percentage_5'] = [
    'description' => 'Restitution 5%',
    'type' => 'percentage',
    'value' => 5,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_percentage_10'] = [
    'description' => 'Restitution 10%',
    'type' => 'percentage',
    'value' => 10,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_percentage_15'] = [
    'description' => 'Restitution 15%',
    'type' => 'percentage',
    'value' => 15,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_percentage_25'] = [
    'description' => 'Restitution 25%',
    'type' => 'percentage',
    'value' => 25,
    'rule' => $restitutionRule->getId(),
];
$return[Discount::class]['restitution_percentage_50'] = [
    'description' => 'Restitution 50%',
    'type' => 'percentage',
    'value' => 50,
    'rule' => $restitutionRule->getId(),
];

return $return;