<?php

namespace AppBundle\Resources;

/**
 * TODO Copernica code can be trashed
 *
 * Class AbstractResource
 * @package AppBundle\Resources
 */
abstract class AbstractResource
{
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->ID;
    }
}