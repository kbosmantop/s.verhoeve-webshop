<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Manager\Order\OrderCollectionManager;

/**
 * Class OrderCollectionStatusExtension
 * @package AppBundle\Twig\Extension
 */
class OrderCollectionStatusExtension extends \Twig_Extension
{
    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * OrderStatusExtension constructor.
     * @param OrderCollectionManager $orderCollectionManager
     */
    public function __construct(OrderCollectionManager $orderCollectionManager)
    {
        $this->orderCollectionManager = $orderCollectionManager;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        $options = [];

        return [
            //@todo SF4
            new \Twig_SimpleFunction('order_collection_can', [$this, 'can'], $options),
        ];
    }

    /**
     * @param OrderCollection $orderCollection
     * @param string          $action
     * @return bool
     */
    public function can(OrderCollection $orderCollection, string $action): bool
    {
        switch ($action) {
            case 'edit':
                return $this->orderCollectionManager->isEditable($orderCollection);
            case 'process':
                $auto = false;

                if (\func_num_args() >= 3 && \is_bool(func_get_arg(3))) {
                    $auto = func_get_arg(3);
                }

                try {
                    return $this->orderCollectionManager->isProcessable($orderCollection, $auto);
                } catch (\Exception $e) {

                }
        }

        return false;
    }
}
