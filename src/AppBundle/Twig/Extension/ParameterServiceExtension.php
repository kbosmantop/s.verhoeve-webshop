<?php

namespace AppBundle\Twig\Extension;

use AdminBundle\Form\Parameter\ParameterEntityFileType;
use AppBundle\Entity\Common\Parameter\Parameter;
use AppBundle\Entity\Common\Parameter\ParameterEntityFile;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Services\Domain;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Slug;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ParameterServiceExtension
 * @package AppBundle\Twig\Extension
 */
class ParameterServiceExtension extends \Twig_Extension
{
    /**
     * @var Slug slug
     */
    private $slug;

    /**
     * @var Domain domain
     */
    private $domain;

    /**
     * @var EntityManagerInterface entityManager
     */
    private $entityManager;

    /**
     * @var ParameterService parameterService
     */
    private $parameterService;

    /**
     * ParameterServiceExtension constructor.
     * @param Slug             $slug
     * @param Domain           $domain
     * @param ParameterService $parameterService
     * @param EntityManagerInterface    $entityManager
     */
    public function __construct(
        Slug $slug,
        Domain $domain,
        ParameterService $parameterService,
        EntityManagerInterface $entityManager
    ) {
        $this->slug = $slug;
        $this->domain = $domain;
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => false,
        ];

        $functions = [
            new \Twig_SimpleFunction('app_site_parameter_values', [$this, 'getValues'], $options),
            new \Twig_SimpleFunction('get_system_parameter', [$this, 'getSystemParameter'], $options),
            new \Twig_SimpleFunction('get_entity_parameter', [$this, 'getEntityParameter'], $options),
        ];

        return $functions;
    }

    /**
     * @param                  $key
     * @param null             $type
     *
     * @return null|string
     * @throws \Exception
     */
    public function getValues($key, $type = null): ?string
    {
        if (!$this->getDomainSite() || !$this->getDomainSite()->getId()) {
            return null;
        }

        $siteId = $this->getDomainSite()->getId();

        $value = $this->parameterService
            ->setEntity(Site::class)
            ->getValue($key, $siteId);

        if (null !== $value) {
            if ($type === 'url') {
                /** @var Parameter $parameter */
                $parameter = $this->entityManager
                    ->getRepository(Parameter::class)
                    ->findOneBy(['key' => $key]);

                if ($parameter->getFormType() === ParameterEntityFileType::class) {
                    $parameterEntityFile = $this->entityManager->getRepository(ParameterEntityFile::class)->find($value);

                    if (null !== $parameterEntityFile) {
                        return $parameterEntityFile->getPath();
                    }
                }

                if ($parameter->getFormTypeClass() === Page::class) {
                    $page = $this->entityManager->getRepository(Page::class)->find($value);

                    if (null !== $page) {
                        return $this->slug->getUrl($page);
                    }
                }
            }

            return $value;
        }

        return null;
    }

    /**
     * @param                  $key
     * @param null             $type
     *
     * @return null|string
     * @throws \Exception
     */
    public function getSystemParameter($key, $type = null): ?string
    {
        return $this->parameterService
            ->setEntity(null)
            ->getValue($key, null);
    }

    /**
     * @param      $key
     * @param      $entity
     * @param      $class
     * @return string|null
     */
    public function getEntityParameter($key, $entity, $class): ?string
    {
        return $this->parameterService
            ->setEntity($class)
            ->getValue($key, $entity->getId());
    }

    /**
     * @return Site|null
     */
    private function getDomainSite(): ?Site
    {
        if (
            !$this->domain
            || !$this->domain->getDomain()
            || !$this->domain->getDomain()->getSite()
        ) {
            return null;
        }

        return $this->domain->getDomain()->getSite();
    }
}
