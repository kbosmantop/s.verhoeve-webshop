<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Factory\MenuFactory;
use AppBundle\Services\SiteService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class MobileMenuExtension
 * @package AppBundle\Twig\Extension
 */
class MobileMenuExtension extends \Twig_Extension
{
    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * @var MenuFactory
     */
    private $menuFactory;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * SiteTabsExtension constructor.
     *
     * @param SiteService           $siteService
     * @param MenuFactory           $menuFactory
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(SiteService $siteService, MenuFactory $menuFactory, TokenStorageInterface $tokenStorage)
    {
        $this->siteService = $siteService;
        $this->menuFactory = $menuFactory;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('mobile_menu', [$this, 'get'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $environment
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     * @throws \Twig_Error
     */
    public function get(\Twig_Environment $environment)
    {
        $sites = [];
        $site = $this->siteService->determineSite();

        if (null === $site) {
            return '';
        }

        if (null === ($parentSite = $site->getParent())) {
            $parentSite = $site;
        }

        $menu = null;

        $token = $this->tokenStorage->getToken();
        $customer = null !== $token && $token->getUser() && $token->getUser() instanceof Customer ? $token->getUser() : null;
        if (null !== $customer && $customer->getCompany() && $customer->getCompany()->getMenu()) {
            $menu = $customer->getCompany()->getMenu();

            return $environment->render('@App/blocks/nav-mobile-company-menu.html.twig', [
                'menu' => $this->menuFactory->get($menu)
            ]);
        }

        if (null === $menu && null !== $parentSite && false === $parentSite->getMainMenu()->isEmpty()) {
            $menu = $parentSite->getMainMenu()->current();
        }

        $menu = $this->menuFactory->get($menu);

        $sites[] = [
            'id' => $parentSite->getId(),
            'theme' => $parentSite->getTheme(),
            'slug' => $parentSite->getSlug(),
            'active' => false,
            'menu' => $menu,
        ];

        /** @var Site $childSite */
        foreach ($parentSite->getChildren() as $childSite) {
            if ($childSite->getMainMenu()->isEmpty()) {
                continue;
            }

            $sites[] = [
                'id' => $childSite->getId(),
                'theme' => $parentSite->getTheme(),
                'slug' => $childSite->getSlug(),
                'active' => $site->getSlug() === $childSite->getSlug(), // add session value check
                'menu' => $this->menuFactory->get($childSite->getMainMenu()->current()) ?? null,
            ];
        }

        return $environment->render('AppBundle:blocks:nav-mobile-menu.html.twig', [
            'sites' => $sites,
        ]);
    }
}
