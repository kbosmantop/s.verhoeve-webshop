<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\Domain;
use Symfony\Component\Asset\Packages as Package;
use Twig\Extension\AbstractExtension;
use Twig_Extension_GlobalsInterface;

/**
 * Class CustomerServicePopupExtension
 * @package AppBundle\Twig\Extension
 */
class CustomerServicePopupExtension extends AbstractExtension implements Twig_Extension_GlobalsInterface
{
    /**≤
     * @var Domain
     */
    private $domain;

    /**
     * @var Package
     */
    private $package;

    /**
     * Constructor.
     *
     * @param Domain  $domain
     * @param Package $package
     */
    public function __construct(Domain $domain, Package $package)
    {
        $this->domain = $domain;

        $this->package = $package;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        $content = '';
        $site = null;

        if (
            null !== $this->domain->getDomain() && null !== $this->domain->getDomain()->getSite()
            && ($site = $this->domain->getDomain()->getSite()) && null !== $site->getCustomerServicePopupPage()
        ) {
            $content = str_replace([
                '%siteName%',
                '%customerServiceImage%',
            ], [
                $site->translate()->getDescription(),
                $this->getAssets('build/images/klantenservice.jpg'),
            ],
                $site->getCustomerServicePopupPage()->translate()->getContent()
            );
        }

        return [
            'popup_customer_service_content' => $content,
        ];
    }

    /**
     * @param $path
     * @return string
     */
    private function getAssets($path)
    {
        return $this->package->getUrl($path);
    }

}
