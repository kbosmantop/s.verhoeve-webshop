<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Services\JsonLdGenerator;

/**
 * Class JsonLdExtension
 * @package AppBundle\Twig\Extension
 */
class JsonLdExtension extends \Twig_Extension
{
    /**
     * @var JsonLdGenerator
     */
    private $jsonLdGenerator;

    /**
     * Constructor.
     *
     * @param JsonLdGenerator $jsonLdGenerator
     */
    public function __construct(JsonLdGenerator $jsonLdGenerator)
    {
        $this->jsonLdGenerator = $jsonLdGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('jsonld_product', [$this, 'getJsonLdProduct'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @param Product           $product
     * @return string
     * @internal param array $options
     */
    public function getJsonLdProduct(\Twig_Environment $env, ProductInterface $product)
    {
        void($env);

        return $this->jsonLdGenerator->Product($product);
    }
}
