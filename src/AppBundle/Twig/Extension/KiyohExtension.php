<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\Kiyoh;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * Class KiyohExtension
 * @package AppBundle\Twig\Extension
 */
class KiyohExtension extends \Twig_Extension
{
    private $kiyoh;

    /**
     * Constructor.
     * @param Kiyoh $kiyoh
     */
    public function __construct(Kiyoh $kiyoh)
    {
        $this->kiyoh = $kiyoh;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('kiyoh_company', [$this, 'getKiyohCompany'], $options),
            new \Twig_SimpleFunction('kiyoh_reviews', [$this, 'getKiyohReviews'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @param array             $options
     *
     * @return KiyohCompany
     */
    public function getKiyohCompany(\Twig_Environment $env, array $options = [])
    {
        void($env);

        return $this->kiyoh->getCompany($options);
    }

    /**
     * @param \Twig_Environment $env
     * @param array             $options
     * @return PaginationInterface
     */
    public function getKiyohReviews(\Twig_Environment $env, array $options = [])
    {
        void($env);
        return $this->kiyoh->getReviews($options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_kiyoh';
    }
}
