<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Catalog\Product\ProductCard;
use Doctrine\Common\Collections\Collection;

/**
 * Class CardSizeExtension
 * @package AppBundle\Twig\Extension
 */
class CardSizeExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        return [
            new \Twig_SimpleFunction('card_determine_size', [$this, 'getCardSize'],
                $options),
        ];
    }

    /**
     * @param \Twig_Environment $env
     * @param Collection $cards
     *
     * @return string
     */
    public function getCardSize(\Twig_Environment $env, Collection $cards)
    {
        /** @var ProductCard $card */
        foreach($cards as $card) {
            if($card->hasProductProperty('card_format')) {
                return $card->findProductProperty('card_format')->getProductgroupPropertyOption()->getValue();
            }
        }
    }
}
