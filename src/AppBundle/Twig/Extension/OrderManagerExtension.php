<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Manager\Order\OrderManager;

/**
 * Class OrderLineManagerExtension
 * @package AppBundle\Twig\Extension
 */
class OrderManagerExtension extends \Twig_Extension
{
    /**
     * @var OrderManager
     */
    protected $orderManager;

    /**
     * PriceExtension constructor.
     * @param OrderManager $orderManager
     */
    public function __construct(OrderManager $orderManager)
    {
        $this->orderManager = $orderManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('order_manager_get_totaldiscountamount_excl', [$this, 'getTotalDiscountAmountExcl']),
            new \Twig_SimpleFunction('order_is_editable', [$this, 'isEditable']),
        ];

        return $functions;
    }

    /**
     * @param Order $order
     * @return float|null
     */
    public function getTotalDiscountAmountExcl(Order $order)
    {
        return $this->orderManager->getTotalDiscountAmountExcl($order);
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isEditable(Order $order)
    {
        return $this->orderManager->isEditable($order);
    }
}
