<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\Theme;

/**
 * Class ThemeExtension
 * @package AppBundle\Twig\Extension
 */
class ThemeExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    private $theme;

    /**
     * Constructor.
     *
     * @param Theme $theme
     */
    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        return [
            'theme' => $this->theme,
            'theme_img_base_path' => $this->theme->getImagesDir(),
            'theme_css_dir' => $this->theme->getCssDir(),
            'theme_css' => $this->theme->getCssDir() . 'style.css',
        ];
    }
}
