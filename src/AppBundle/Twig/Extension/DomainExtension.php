<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\Domain;

/**
 * Class DomainExtension
 * @package AppBundle\Twig\Extension
 */
class DomainExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    private $domain;

    /**
     * Constructor.
     * @param Domain $domain
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        return [
            'domain' => $this->domain,
        ];
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'app_domain';
    }
}
