<?php

namespace AppBundle\Twig\Extension;

use AppBundle\DBAL\Types\CompanyPriceDisplayModeType;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Relation\Company;
use AppBundle\Manager\Relation\CompanyManager;
use function array_map;

/**
 * Class CompanyExtension
 * @package AppBundle\Twig\Extension
 */
class CompanyExtension extends \Twig_Extension
{
    /**
     * @var CompanyManager
     */
    private $companyManager;

    /**
     * PriceExtension constructor.
     * @param CompanyManager $companyManager
     */
    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('company_outstanding_amount', [$this, 'getOutstandingAmount']),
            new \Twig_SimpleFunction('company_outstanding_amount_incl', [$this, 'getOutstandingAmountIncl']),
            new \Twig_SimpleFunction('company_available_payment_methods', [$this, 'getAvailablePaymentMethods']),
            new \Twig_SimpleFunction('company_credit_balance', [$this, 'getCreditBalance']),
        ];

        return $functions;
    }

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        $filters = [
            new \Twig_SimpleFilter('company_price_display_mode', [$this, 'getPriceDisplayMode']),
        ];

        return $filters;
    }

    /**
     * @param Company $company
     *
     * @return float
     */
    public function getOutstandingAmount(Company $company)
    {
        return $this->companyManager->getOutstandingAmount($company);
    }

    /**
     * @param Company $company
     *
     * @return float
     */
    public function getOutstandingAmountIncl(Company $company)
    {
        return $this->companyManager->getOutstandingAmount($company, true);
    }

    /**
     * @param Company $company
     * @return object
     */
    public function getCreditBalance(Company $company): object
    {
       return $this->companyManager->getCreditBalance($company);
    }

    /**
     * @param string $mode
     *
     * @return string
     */
    public function getPriceDisplayMode(?string $mode): ?string
    {
        $choices = array_flip(CompanyPriceDisplayModeType::getChoices());

        return $choices[$mode] ?? $mode;
    }

    /**
     * @param Company $company
     * @return string
     */
    public function getAvailablePaymentMethods(Company $company): string
    {
        $methods = $company->getPaymentmethods();

        if($methods->isEmpty()) {
            $methods = ['Online betaalmethodes'];

            if($company->getVerified()) {
                $methods[] = 'Op rekening';
            }
        } else {
            $methods = $methods->map(function(Paymentmethod $paymentmethod) {
                return $paymentmethod->translate()->getDescription();
            })->toArray();
        }

        return implode(', ', $methods);
    }
}
