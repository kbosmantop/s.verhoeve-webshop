<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Manager\Order\OrderLineManager;

/**
 * Class OrderLineManagerExtension
 * @package AppBundle\Twig\Extension
 */
class OrderLineManagerExtension extends \Twig_Extension
{
    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * PriceExtension constructor.
     * @param OrderLineManager $orderLineManager
     */
    public function __construct(OrderLineManager $orderLineManager)
    {
        $this->orderLineManager = $orderLineManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('orderline_manager_get_discountamount_excl', [$this, 'getDiscountAmountExcl']),
            new \Twig_SimpleFunction('orderline_manager_get_price_excl', [$this, 'getPriceExcl']),
            new \Twig_SimpleFunction('orderline_manager_get_price_incl', [$this, 'getPriceIncl']),
            new \Twig_SimpleFunction('orderline_manager_get_discount_description', [$this, 'getDiscountDescription']),
            new \Twig_SimpleFunction('orderline_manager_get_design', [$this, 'getDesign']),
        ];

        return $functions;
    }

    /**
     * @param OrderLine $orderLine
     * @return float|null
     */
    public function getDiscountAmountExcl(OrderLine $orderLine)
    {
        return $this->orderLineManager->getDiscountAmountExcl($orderLine);
    }

    /**
     * @param OrderLine $orderLine
     * @return float
     */
    public function getPriceExcl(OrderLine $orderLine)
    {
        return $orderLine->getTotalPrice();
    }

    /**
     * @param OrderLine $orderLine
     * @return float
     */
    public function getPriceIncl(OrderLine $orderLine)
    {
        return $this->orderLineManager->getTotalPriceIncl($orderLine);
    }

    /**
     * @param OrderLine $orderLine
     * @return string|null
     * @throws \Exception
     */
    public function getDiscountDescription(OrderLine $orderLine)
    {
        return $this->orderLineManager->getDiscountDescription($orderLine);
    }

    /**
     * @param OrderLine $orderLine
     * @return string|null
     */
    public function getDesign(OrderLine $orderLine): ?string
    {
        return $this->orderLineManager->getPersonalizationPreviewImage($orderLine);
    }
}
