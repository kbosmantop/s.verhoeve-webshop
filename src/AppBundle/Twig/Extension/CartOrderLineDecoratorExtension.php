<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Services\CartOrderLineDecorator;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CartOrderLineDecoratorExtension
 * @package AppBundle\Twig\Extension
 */
class CartOrderLineDecoratorExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('topbloemen_cart_order_line_decorator', [$this, 'getCartOrderLineDecorator'],
                $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @param CartOrderLine     $cartOrderLine
     *
     * @return CartOrderLineDecorator
     */
    public function getCartOrderLineDecorator(\Twig_Environment $env, CartOrderLine $cartOrderLine)
    {
        void($env);
        $cartOrderLineDecorator = $this->container->get('app.cart_order_line');
        $cartOrderLineDecorator->setCartOrderLine($cartOrderLine);

        return $cartOrderLineDecorator;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_cart_order_line_decorator';
    }
}
