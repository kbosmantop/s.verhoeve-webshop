<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Helper\Order\OrderStatusHelper;

/**
 * Class OrderStatusExtension
 * @package AppBundle\Twig\Extension
 */
class OrderStatusExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters(): array
    {
        $options = [];

        return [
            new \Twig_SimpleFilter('order_status_label_class', [$this, 'orderStatusLabelClass'], $options),
        ];
    }

    /**
     * @param string $orderStatus
     * @return string
     * @throws \AppBundle\Exceptions\OrderStatusNotFoundException
     */
    public function orderStatusLabelClass(string $orderStatus): string
    {
        return OrderStatusHelper::getLabelClass($orderStatus);
    }
}
