<?php

namespace AppBundle\Twig\Extension;

use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class LocaleNameExtension
 * @package AppBundle\Twig\Extension
 */
class LocaleNameExtension extends \Twig_Extension
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RequestStack          $requestStack
     * @param RouterInterface $router
     */
    public function __construct(
        RequestStack $requestStack,
        RouterInterface $router
    ) {
        $this->requestStack = $requestStack;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('locale_name', [$this, 'getLocaleName']),
            new \Twig_SimpleFunction('locale_path', [$this, 'getLocalePath']),
            new \Twig_SimpleFunction('locale_country', [$this, 'getLocaleCountry']),
        ];
    }

    /**
     * @param string $locale
     * @return string
     */
    public function getLocalePath(string $locale): string
    {
        $request = $this->requestStack->getCurrentRequest();
        $route = $request->get('_route');
        $newRoute = str_replace($request->getLocale(), $locale, $route);
        $routeParams = array_merge($request->get('_route_params') ?? [], ['language' => $locale]);
        unset($routeParams['_locale']);

        try {
            return $this->router->generate($newRoute, $routeParams);
        } catch (\Exception $e) {
            return '/?language='. $locale;
        }
    }

    /**
     * @param string $locale
     * @return string
     */
    public function getLocaleName(string $locale): string
    {
        if ($locale === 'nl_BE') {
            return 'Vlaams';
        }
        if ($locale === 'en_US') {
            return 'English';
        }
        $codes = explode('_', $locale);
        return Intl::getLanguageBundle()->getLanguageName($codes[0], $codes[1] ?? null, $locale) ?? '';
    }

    /**
     * @param string $locale
     * @return string
     */
    public function getLocaleCountry(string $locale): string
    {
        return strtolower(substr($locale, strlen($locale) - 2));
    }
}
