<?php

namespace AppBundle\Twig\Extension;

use Twig\TwigFilter;
use Twig_Extension;

/**
 * Class JsonDecodeExtension
 * @package AppBundle\Twig\Extension
 * @todo sf4: JsonDecodeExtension extends AbstractExtension
 */
class JsonDecodeExtension extends Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('json_decode', [$this, 'jsonDecode'])
        ];
    }

    /**
     * @param string $string
     *
     * @return string
     * @internal param array $options
     */
    public function jsonDecode(string $string, $assoc = true)
    {
        return json_decode($string, $assoc);
    }
}
