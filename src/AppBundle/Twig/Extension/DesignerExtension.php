<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\Designer;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Twig_Environment;

/**
 * Class DesignerExtension
 * @package AppBundle\Twig\Extension
 */
class DesignerExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * Designer
     */
    protected $designer = null;

    /**
     * DesignerExtension constructor.
     * @param Designer $designer
     */
    public function __construct(Designer $designer)
    {
        $this->designer = $designer;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('app_designer_preview', [$this, 'getPreview'], $options),
        ];

        return $functions;
    }

    /**
     * @param Twig_Environment $env
     * @param string           $uuid
     * @param int              $width
     * @param bool             $isPredesign
     * @return string
     */
    public function getPreview(Twig_Environment $env, string $uuid, $width = 200, $isPredesign = false)
    {
        void($env);

        $this->designer->setUuid($uuid);
        $this->designer->setIsPreDesign($isPredesign);
        // Check if we got an template preview
        if ($getTemplatePreview = $this->designer->getTemplatePreview()) {
            $previewUrl = $getTemplatePreview;
        } else {
            $previewUrl = $this->designer->getFileUrl(Designer::FILE_PREVIEW);
        }
        
        if (!$previewUrl) {
            return '';
        }

        /** @var CacheManager */
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');

        /** @var string */
        $previewUrl = $imagineCacheManager->getBrowserPath($previewUrl, 'designer_thumbnail');

        if ($previewUrl) {
            return sprintf('<img src="%s" width="%s" />', $previewUrl, $width);
        }

        return '';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_designer';
    }
}
