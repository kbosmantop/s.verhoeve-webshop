<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\Site;
use AppBundle\Factory\MenuFactory;
use AppBundle\Services\Domain;
use AppBundle\Utils\MenuStructure;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class MenuExtension
 * @package AppBundle\Twig\Extension
 */
class MenuExtension extends \Twig_Extension
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MenuFactory
     */
    private $menuFactory;

    /**
     * @var Domain
     */
    private $domain;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * MenuExtension constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param MenuFactory            $menuFactory
     * @param Domain                 $domain
     * @param Session                $session
     * @param TokenStorage           $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        MenuFactory $menuFactory,
        Domain $domain,
        Session $session,
        TokenStorage $tokenStorage
    ) {
        $this->entityManager = $entityManager;
        $this->menuFactory = $menuFactory;
        $this->domain = $domain;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('topbloemen_menu', [$this, 'get'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment   $env
     * @param string|Menu|integer $menu
     * @param null                $site
     * @return MenuStructure|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \ReflectionException
     */
    public function get(\Twig_Environment $env, $menu, $site = null): ?MenuStructure
    {
        void($env);

        $token = $this->tokenStorage->getToken();
        /** @var Customer $customer */
        $customer = null !== $token && $token->getUser() && $token->getUser() instanceof Customer ? $token->getUser() : null;
        //check if customer has a custom menu, this always overrides other menus
        if (null !== $customer && $customer->getCompany() && $customer->getCompany()->getMenu()) {
            $menu = $customer->getCompany()->getMenu();
        } else {
            $menuRepository = $this->entityManager->getRepository(Menu::class);

            //override menu when id is stored in session
            if ($menu === 'main' && null !== $this->session->get('menu_id')) {
                $menu = $menuRepository->find($this->session->get('menu_id'));
            } else {
                if (\is_int($menu)) {
                    $menu = $menuRepository->find($menu);
                } elseif (\is_string($menu)) {
                    if (null === $site || !$site instanceof Site) {
                        $site = $this->getSite();
                    }

                    if ($menu === 'footer' && null !== $site  && $site->getParent()) {
                        $site = $site->getParent();
                    }

                    $menu = $menuRepository->findOneBy([
                        'location' => $menu,
                        'site' => $site,
                    ]);
                }
            }
        }

        if (!$menu) {
            return null;
        }

        return $this->menuFactory->get($menu);
    }

    /**
     * @return Site|null
     */
    private function getSite(): ?Site
    {
        $domain = $this->domain->getDomain();

        if (!$domain) {
            return null;
        }

        return $domain->getSite();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_menu';
    }
}
