<?php

namespace AppBundle\EventSubscriber\Bloemorder;

use AppBundle\Entity\Relation\Company;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class BloemorderCompanyEventSubscriber
 * @package AppBundle\EventSubscriber\Bloemorder
 */
class BloemorderCompanyEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Company::class, 'sync:bloemorder:companies', 'validateObject', 'bloemorder');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Company::class, 'sync:bloemorder:companies', 'validateObject', 'bloemorder');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Company::class, 'sync:bloemorder:companies', 'validateObject', 'bloemorder');
    }

    /**
     * @param Company $company
     * @param array $changeSet
     *
     * @return bool
     */
    protected function validateObject(Company $company, array $changeSet): bool
    {
        if(!isset($company->getMetadata()['bloemorderCompanyId'])) {
            return false;
        }

        $totalChanges = \count($changeSet);
        return !($totalChanges === 1 && isset($changeSet['updateAt']));
    }
}
