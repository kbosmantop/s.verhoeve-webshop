<?php

namespace AppBundle\EventSubscriber\Catalog;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\UnitOfWork;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductEventSubscriber
 * @package AppBundle\EventSubscriber\Catalog
 */
class ProductEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function checkChange(LifecycleEventArgs $eventArgs)
    {
        $className = Product::class;
        if (($product = $eventArgs->getObject()) instanceof $className) {

            if (!$product->getParent()) {
                return;
            }

            if ($this->processAdditionalProducts($eventArgs)) {
                $job = new Job('router:warmup', [], true, 'routing');
                $this->container->get('job.manager')->addJob($job);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs|EventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(EventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @return bool
     */
    private function processAdditionalProducts(LifecycleEventArgs $eventArgs)
    {
        /** @var UnitOfWork $uow */
        $uow = $eventArgs->getEntityManager()->getUnitOfWork();

        if ($this->checkForAdditionalProductsinChangeset($uow)) {
            return true;
        }

        return false;
    }

    /**
     * @param UnitOfWork $uow
     *
     * @return boolean
     */
    private function checkForAdditionalProductsinChangeset(UnitOfWork $uow)
    {
        $apaInDeletionChangeset = false;
        $apaInUpdateChangeset = false;

        /** @var PersistentCollection $item */
        foreach ($uow->getScheduledCollectionDeletions() as $item) {
            $mapping = $item->getMapping();
            $apaInDeletionChangeset = (isset($mapping['fieldName']) && $mapping['fieldName'] === 'additionalProductAssortments');

            // If found one break the foreach loop
            if ($apaInDeletionChangeset) {
                break;
            }
        }

        // Only search update collections when $apaInDeletionChangeset is false
        if (!$apaInDeletionChangeset) {
            /** @var PersistentCollection $item */
            foreach ($uow->getScheduledCollectionUpdates() as $item) {
                $mapping = $item->getMapping();
                $apaInUpdateChangeset = (isset($mapping['fieldName']) && $mapping['fieldName'] === 'additionalProductAssortments');

                // If found one break the foreach loop
                if ($apaInUpdateChangeset) {
                    break;
                }
            }
        }

        return ($apaInDeletionChangeset || $apaInUpdateChangeset);
    }
}
