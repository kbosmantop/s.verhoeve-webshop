<?php

namespace AppBundle\OAuth\Response;

use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse as BaseClass;

/**
 * Class PathUserResponse
 * @package AppBundle\OAuth\Response
 */
class PathUserResponse extends BaseClass
{
    /**
     * @return null|string
     */
    public function getBirthday(): ?string
    {
        return $this->getValueForPath('birthday');
    }

    /**
     * @return null|string
     */
    public function getFirstname(): ?string
    {
        return $this->getValueForPath('firstname');
    }

    /**
     * @return null|string
     */
    public function getLastname(): ?string
    {
        return $this->getValueForPath('lastname');
    }

    /**
     * @return null|string
     */
    public function getGender(): ?string
    {
        return $this->getValueForPath('gender');
    }
}