<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ThemeType
 * @package AppBundle\DBAL\Types
 */
final class ThemeType extends AbstractEnumType
{
    public const THEME_TOPGESCHENKEN = 'topgeschenken';
    public const THEME_TOPBLOEMEN = 'topbloemen';
    public const THEME_TOPFRUIT = 'topfruit';
    public const THEME_TOPTAARTEN = 'toptaarten';
    public const THEME_SUPPLIERS = 'suppliers';

    protected static $choices = [
        self::THEME_TOPGESCHENKEN => 'topgeschenken',
        self::THEME_TOPBLOEMEN => 'topbloemen',
        self::THEME_TOPFRUIT => 'topfruit',
        self::THEME_TOPTAARTEN => 'toptaarten',
        self::THEME_SUPPLIERS => 'suppliers',
    ];
}