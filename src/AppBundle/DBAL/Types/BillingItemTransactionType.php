<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class DisplayPriceType
 * @package AppBundle\DBAL\Types
 */
final class BillingItemTransactionType extends AbstractEnumType
{
    public const DEBIT = 'debit';
    public const CREDIT = 'credit';

    protected static $choices = [
        self::DEBIT => 'label.debit',
        self::CREDIT => 'label.credit',
    ];
}