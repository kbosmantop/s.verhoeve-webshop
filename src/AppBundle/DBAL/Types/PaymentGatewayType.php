<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class PaymentGatewayType
 * @package AppBundle\DBAL\Types
 */
final class PaymentGatewayType extends AbstractEnumType
{
    public const ADYEN = 'adyen';
    public const AFTERPAY = 'afterpay';

    protected static $choices = [
        self::ADYEN => 'Adyen',
        self::AFTERPAY => 'Afterpay',
    ];
}