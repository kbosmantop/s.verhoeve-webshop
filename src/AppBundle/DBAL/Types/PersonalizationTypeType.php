<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class PersonalizationTypeType
 * @package AppBundle\DBAL\Types
 */
final class PersonalizationTypeType extends AbstractEnumType
{
    public const TYPE_DESIGN = 'design';
    public const TYPE_PDF = 'pdf';
    public const TYPE_TEXT = 'text';

    protected static $choices = [
        self::TYPE_DESIGN => 'design',
        self::TYPE_PDF => 'pdf',
        self::TYPE_TEXT => 'text',
    ];
}
