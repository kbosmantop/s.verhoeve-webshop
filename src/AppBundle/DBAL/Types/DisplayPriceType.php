<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class DisplayPriceType
 * @package AppBundle\DBAL\Types
 */
final class DisplayPriceType extends AbstractEnumType
{
    public const EXCLUSIVE = 'exclusive';
    public const INCLUSIVE = 'inclusive';

    protected static $choices = [
        self::EXCLUSIVE => 'label.exclusive',
        self::INCLUSIVE => 'label.inclusive',
    ];
}