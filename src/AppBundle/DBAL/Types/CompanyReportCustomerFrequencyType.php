<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyReportCustomerFrequencyType
 * @package AppBundle\DBAL\Types
 */
final class CompanyReportCustomerFrequencyType extends AbstractEnumType
{
    const DAILY = 'P1D';
    const WEEKLY = 'P7D';
    const TWOWEEKLY = 'P14D';
    const MONTHLY = 'P1M';
    const QUARTERLY = 'P3M';
    const YEARLY = 'P1Y';

    protected static $choices = [
        self::DAILY => 'Dagelijks',
        self::WEEKLY => 'Wekelijks',
        self::TWOWEEKLY => 'Twee wekelijks',
        self::MONTHLY => 'Maandelijks',
        self::QUARTERLY => 'Elk kwartaal',
        self::YEARLY => 'Jaarlijks',
    ];
}