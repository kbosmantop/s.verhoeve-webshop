<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyPriceDisplayModeType
 * @package AppBundle\DBAL\Types
 */
final class CompanyPriceDisplayModeType extends AbstractEnumType
{
    /** @var string  */
    public const ORIGINAL = 'original';

    /** @var string  */
    public const DISCOUNTED = 'discounted';

    /** @var string  */
    public const BOTH = 'both';

    /** @var array  */
    protected static $choices = [
        self::ORIGINAL => 'Originele prijs',
        self::DISCOUNTED => 'Korting',
        self::BOTH => 'Orginele prijs + korting',
    ];
}