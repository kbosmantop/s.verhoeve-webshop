<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyInvoiceDiscountType
 * @package AppBundle\DBAL\Types
 */
final class CompanyInvoiceDiscountType extends AbstractEnumType
{
    public const INVOICE_DISCOUNT_SEPARATE = 'separate';
    public const INVOICE_DISCOUNT_APPLIED = 'applied';

    protected $name = 'company_invoice_discount_type';

    protected static $choices = [
        self::INVOICE_DISCOUNT_SEPARATE => 'Als regel op factuur',
        self::INVOICE_DISCOUNT_APPLIED => 'Verrekend',
    ];
}