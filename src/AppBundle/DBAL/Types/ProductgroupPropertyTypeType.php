<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ProductgroupPropertyTypeType
 * @package AppBundle\DBAL\Types
 */
final class ProductgroupPropertyTypeType extends AbstractEnumType
{
    public const TEXT = 'text';
    public const TEXTAREA = 'textarea';
    public const DATE = 'date';
    public const CHOICE = 'choice';
    public const PRICE = 'price';
    public const ENTITY = 'entity';
    public const ASSORTMENT = 'assortment';

    protected static $choices = [
        self::TEXT => 'Tekst',
        self::TEXTAREA => 'Tekstveld',
        self::DATE => 'Datum',
        self::CHOICE => 'Keuze',
        self::PRICE => 'Prijs',
        self::ENTITY => 'Productvariatie',
        self::ASSORTMENT => 'Assortment',
    ];
}