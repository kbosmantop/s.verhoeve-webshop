<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class MenuLocationType
 * @package AppBundle\DBAL\Types
 */
final class MenuLocationType extends AbstractEnumType
{
    public const MENU_LOCATION_MAIN = 'main';
    public const MENU_LOCATION_FOOTER = 'footer';
    public const MENU_LOCATION_EMAIL_MAIN = 'email_main';
    public const MENU_LOCATION_EMAIL_FOOTER = 'email_footer';

    /**
     * Use E-mail: as prefix for e-mail locations, this will cause the admin to use this to format the choice labels
     * with an optgroup
     *
     * @var array
     */
    protected static $choices = [
        self::MENU_LOCATION_MAIN => 'Hoofdmenu',
        self::MENU_LOCATION_FOOTER => 'Footer',
        self::MENU_LOCATION_EMAIL_MAIN => 'E-mail: Hoofdmenu',
        self::MENU_LOCATION_EMAIL_FOOTER => 'E-mail: Footer',
    ];
}
