<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class AppliesToOrderType
 * @package AppBundle\DBAL\Types
 */
final class AppliesToOrderType extends AbstractEnumType
{
    public const APPLIES_TO_ORDER_COLLECTION = 'order_collection';
    public const APPLIES_TO_ORDER = 'order';
    public const APPLIES_TO_NONE = null;

    protected static $choices = [
        self::APPLIES_TO_ORDER_COLLECTION => 'Order collection',
        self::APPLIES_TO_ORDER => 'Order',
        self::APPLIES_TO_NONE => 'Alle',
    ];
}