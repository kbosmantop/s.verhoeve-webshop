<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyInvoiceDiscountType
 * @package AppBundle\DBAL\Types
 */
final class CompanyInvoiceInclPersonalDataType extends AbstractEnumType
{
    public const INVOICE_INCL_PERSONAL_DATA_NONE = 'none';
    public const INVOICE_INCL_PERSONAL_DATA_CUSTOMER = 'customer';
    public const INVOICE_INCL_PERSONAL_DATA_RECIPIENT = 'recipient';
    public const INVOICE_INCL_PERSONAL_DATA_ALL = 'all';

    protected $name = 'company_invoice_personal_data';

    protected static $choices = [
        self::INVOICE_INCL_PERSONAL_DATA_NONE => 'Geen persoonsgegevens',
        self::INVOICE_INCL_PERSONAL_DATA_CUSTOMER => 'Alleen besteller',
        self::INVOICE_INCL_PERSONAL_DATA_RECIPIENT => 'Alleen ontvanger',
        self::INVOICE_INCL_PERSONAL_DATA_ALL => 'Besteller en ontvanger',
    ];
}