<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class PriceViewType
 * @package AppBundle\DBAL\Types
 */
final class PriceViewType extends AbstractEnumType
{
    public const PRICE_VIEW_INCLUSIVE = 'inclusive';
    public const PRICE_VIEW_EXCLUSIVE = 'exclusive';

    protected static $choices = [
        self::PRICE_VIEW_INCLUSIVE => 'Inclusief',
        self::PRICE_VIEW_EXCLUSIVE => 'Exclusief',
    ];
}