<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class DeliveryDateMethodType
 * @package AppBundle\DBAL\Types
 */
final class DeliveryDateMethodType extends AbstractEnumType
{
    public const NONE = null;
    public const REQUIRED = 'required';
    public const PREFERENCE = 'preference';

    protected static $choices = [
        self::NONE => 'label.none',
        self::REQUIRED => 'label.required',
        self::PREFERENCE => 'label.preference',
    ];
}