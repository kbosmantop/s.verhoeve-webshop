<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ExactInvoiceExportStatusType
 * @package AppBundle\DBAL\Types
 */
final class ExactInvoiceExportStatusType extends AbstractEnumType
{
    public const NEW = 'new';
    public const IN_PROGRESS = 'in_progress';
    public const EXPORTED = 'exported';
    public const FAILED = 'failed';

    protected static $choices = [
        self::NEW => 'Nieuw',
        self::IN_PROGRESS => 'Wordt gegenereerd',
        self::EXPORTED => 'Geexporteerd',
        self::FAILED => 'Mislukt',
    ];
}