<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class YesNoType
 * @package AppBundle\DBAL\Types
 */
final class YesNoType extends AbstractEnumType
{
    public const YES = true;
    public const NO = false;

    protected static $choices = [
        self::YES => 'Ja', ## label.yes',
        self::NO => 'Nee', ## 'label.no',
    ];
}