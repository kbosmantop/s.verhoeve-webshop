<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class UpsellQuantityType
 * @package AppBundle\DBAL\Types
 */
final class UpsellQuantityType extends AbstractEnumType
{
    public const STRICTLY_FOLLOWING = 'strictly_following';
    public const LOOSELY_FOLLOWING = 'loosely_following';
    public const INDEPENDENT = 'independent';

    protected static $choices = [
        self::STRICTLY_FOLLOWING=> 'Volgt product',
        self::LOOSELY_FOLLOWING => 'Volgt product (aanpasbaar)',
        self::INDEPENDENT => 'Vrij kiesbaar door klant'
    ];
}