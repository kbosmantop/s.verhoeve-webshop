<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class BannerTextBlockPositionType
 * @package AppBundle\DBAL\Types
 */
final class BannerTextBlockPositionType extends AbstractEnumType
{
    public const POSITION_TOP_LEFT = 'tl';
    public const POSITION_TOP_CENTER = 'tc';
    public const POSITION_TOP_RIGHT = 'tr';
    public const POSITION_BOTTOM_LEFT = 'bl';
    public const POSITION_BOTTOM_CENTER = 'bc';
    public const POSITION_BOTTOM_RIGHT = 'br';

    protected static $choices = [
        self::POSITION_TOP_LEFT => 'Links bovenin',
        self::POSITION_BOTTOM_LEFT => 'Links onderin',
        self::POSITION_TOP_CENTER => 'Midden bovenin',
        self::POSITION_BOTTOM_CENTER => 'Midden onderin',
        self::POSITION_TOP_RIGHT => 'Rechts bovenin',
        self::POSITION_BOTTOM_RIGHT => 'Rechts onderin',
    ];
}
