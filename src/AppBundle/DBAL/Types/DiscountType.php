<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class DiscountType
 * @package AppBundle\DBAL\Types
 */
final class DiscountType extends AbstractEnumType
{
    public const DISCOUNT_LINE = 'line';
    public const DISCOUNT_ASSORTMENT = 'assortment';

    /**
     * @var array
     */
    protected static $choices = [
        self::DISCOUNT_LINE => 'Orderregel',
        self::DISCOUNT_ASSORTMENT => 'Assortiment',
    ];
}
