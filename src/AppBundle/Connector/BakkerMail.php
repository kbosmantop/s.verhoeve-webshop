<?php

namespace AppBundle\Connector;

/**
 * Class BakkerMail
 * @package AppBundle\Connector
 */
class BakkerMail extends AbstractMailConnector
{
    protected $color = '#F44336';

    /**
     * @var bool
     */
    protected $requireForwardingPrices = false;

    /**
     * @var bool
     */
    protected $alwaysShowForwardingPrices = false;
}
