<?php

namespace AppBundle\Connector\Client;

/**
 * Class TopbloemenClient
 * @package AppBundle\Connector\Client
 *
 * @method setStrictMode(bool $bool)
 * @method create(string $xml)
 */
class TopbloemenClient extends \SoapClient
{

}