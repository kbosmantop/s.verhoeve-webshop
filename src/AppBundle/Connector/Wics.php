<?php

namespace AppBundle\Connector;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class Wics
 * @package AppBundle\Connector
 */
class Wics extends AbstractConnector
{
    /**
     * @var string
     */
    protected $color = '#621708';

    /**
     * @var bool
     */
    protected $requireForwardingPrices = false;

    /**
     * @var bool
     */
    protected $alwaysShowForwardingPrices = false;

    /**
     * @var bool
     */
    protected $commissionable = false;

    /**
     * @var string
     */
    protected $packingSlipPaperSize = 'A5';

    /**
     * @var WicsService
     */
    private $wicsService;


    /**
     * Wics constructor.
     * @param WicsService            $wicsService
     */
    public function __construct(WicsService $wicsService) {
        $this->wicsService = $wicsService;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $wics = $this->wicsService;

        $wics->exportOrder($supplierOrder, $this->packingSlipPaperSize);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \League\Flysystem\FileExistsException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function cancel(SupplierOrder $supplierOrder)
    {
        $wics = $this->wicsService;

        $wics->exportCancelOrder($supplierOrder);
        parent::cancel($supplierOrder);
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        // TODO: Implement addFormBuilderParameters() method.
    }
}
