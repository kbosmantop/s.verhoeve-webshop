<?php

namespace AppBundle\Connector;

use AppBundle\Client\DefaultClient;
use AppBundle\Connector\Client\TastyPresentClient;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Exceptions\CardTextExceedsMaximumLengthException;
use AppBundle\Services\Sales\Order\PackingSlip;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use League\Flysystem\Adapter\Ftp;
use League\Flysystem\Filesystem;
use Sabre\Xml\Service;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TastyPresent
 * @package AppBundle\Connector
 */
class TastyPresent extends AbstractConnector
{
    /**
     * @var string
     */
    protected $color = '#C10A27';

    /**
     * @var Filesystem
     */
    private $ftp;

    /**
     * @var PackingSlip
     */
    private $packingSlip;

    /**
     * @var DefaultClient
     */
    private $defaultClient;
    
    /**
     * TastyPresent constructor.
     *
     * @param PackingSlip $packingSlip
     * @param DefaultClient $defaultClient
     */
    public function __construct(
        PackingSlip $packingSlip,
        DefaultClient $defaultClient
    ) {
        $this->packingSlip = $packingSlip;
        $this->defaultClient = $defaultClient;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     * @throws GuzzleException
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();

        $xml = $this->buildXml($supplierOrder);

        $soapClient = new TastyPresentClient($this->getParameter('webservice_wsdl'));

        try {
            $data = (object)[
                'xml' => $xml,
                'batchNumber' => $order->getOrderNumber(),
            ];

            $tastyPresentId = $soapClient->ProcessOrdersAsString($data);

            $supplierOrder->setReference($tastyPresentId->ProcessOrdersAsStringResult);
        } catch (\Exception $e) {
            throw new \RuntimeException('Order doorsturen naar Tasty Present mislukt (' . $e->getMessage() . ')');
        }

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return mixed
     * @throws \Exception
     * @throws GuzzleException
     */
    private function buildXml(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();

        $orderNumber = 'TG-' . $order->getOrderNumber();

        $orderLines = [];

        $lineNumber = 1;

        $supplierOrderLine = $supplierOrder->getLines()[0];
        $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

        $telegram = $this->findTelegram($supplierOrder);
        $card = $this->findCard($supplierOrder);
        $design = $this->findDesign($supplierOrder);
        $letter = $this->findLetter($supplierOrder);

        $orderLines[] = [
            'OrderNumber' => $orderNumber,
            'LineNumber' => $lineNumber,
            'ItemCode' => $supplierProduct->getSku(),
            'Description' => $supplierProduct->translate()->getDescription(),
            'Quantity' => $supplierOrderLine->getQuantity(),
            'Price' => $supplierOrderLine->getPriceIncl(),
            '_TelegramText' => $telegram ? $telegram->getOrderLine()->getMetadata()['telegram_text'] : null,
            '_FullName' => $order->getDeliveryAddressAttn(),
            '_Organisation' => (string)$order->getDeliveryAddressCompanyName(),
            '_Address' => $order->getDeliveryAddressStreet() . ' ' . $order->getDeliveryAddressNumber(),
            '_PostalCode' => $order->getDeliveryAddressPostcode(),
            '_City' => $order->getDeliveryAddressCity(),
            '_Country' => $order->getDeliveryAddressCountry()->getCode(),
            '_WishCardText' => $card ? $card->getOrderLine()->getMetadata()['text'] : null,
        ];

        if ($design && !$this->upload($supplierOrder, $design->getOrderLine()->getMetadata()['design_url'],
                $lineNumber)) {
            throw new \RuntimeException(sprintf('Uploading design failed (%s)',
                $design->getOrderLine()->getMetadata()['design_url']));
        }

        $lineNumber++;

        if ($card) {
            $orderLines[] = [
                'OrderNumber' => $orderNumber,
                'LineNumber' => $lineNumber,
                'ItemCode' => 'KAARTTOP',
                'Description' => 'Kaart A4 in kleur',
                'Quantity' => $card->getQuantity(),
                'Price' => $card->getPriceIncl(),
                '_FullName' => $order->getDeliveryAddressAttn(),
                '_Organisation' => (string)$order->getDeliveryAddressCompanyName(),
                '_Address' => $order->getDeliveryAddressStreetAndNumber(),
                '_PostalCode' => $order->getDeliveryAddressPostcode(),
                '_City' => $order->getDeliveryAddressCity(),
                '_Country' => $order->getDeliveryAddressCountry()->getCode(),
            ];

            $pdf = $this->packingSlip->setItem($order);

            /** @var $file */
            $file = tmpfile();

            fwrite($file, $pdf->getContents());

            if (!$this->upload($supplierOrder, $file, $lineNumber)) {
                throw new \RuntimeException(sprintf('Uploading packingslip failed (%s)', 'Kaart A4 in kleur'));
            }

            $lineNumber++;
        }

        if ($letter) {
            $orderLines[] = [
                'OrderNumber' => $orderNumber,
                'LineNumber' => $lineNumber,
                'ItemCode' => 'BRIEF',
                'Description' => 'Brief A4 in kleur',
                'Quantity' => $letter->getQuantity(),
                'Price' => $letter->getPriceIncl(),
                '_FullName' => $order->getDeliveryAddressAttn(),
                '_Organisation' => (string)$order->getDeliveryAddressCompanyName(),
                '_Address' => $order->getDeliveryAddressStreet() . ' ' . $order->getDeliveryAddressNumber(),
                '_PostalCode' => $order->getDeliveryAddressPostcode(),
                '_City' => $order->getDeliveryAddressCity(),
                '_Country' => $order->getDeliveryAddressCountry()->getCode(),
            ];

            if (!$this->upload($supplierOrder, $letter->getOrderLine()->getMetadata()['letter_url'], $lineNumber)) {
                throw new \RuntimeException(sprintf('Uploading letter failed (%s)',
                    $letter->getOrderLine()->getMetadata()['letter_url']));
            }
        }

        if (empty($orderLines)) {
            throw new \RuntimeException('Order kan niet worden doorgestuurd, order kan niet worden omgezet.');
        }

        $data = [
            'Orders' => [
                'OrderNumber' => $orderNumber,
                'CustomerCode' => $this->getParameter('customer_number'),
                '_ShippingDate' => substr($this->calculateShippingDate($supplierOrder)->format('c'), 0, 19),
                array_map(function ($orderLine) {
                    return [
                        'name' => 'OrderLines',
                        'value' => $orderLine,
                    ];
                }, $orderLines),
            ],
        ];

        $xmlService = new Service();

        return $xmlService->write('recordset', $data);
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return null|SupplierOrderLine
     */
    private function findTelegram(SupplierOrder $supplierOrder)
    {
        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $orderLine = $supplierOrderLine->getOrderLine();

            if ($orderLine->getMetadata() && array_key_exists('telegram_text', $orderLine->getMetadata())) {
                return $supplierOrderLine;
            }
        }

        return null;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return null|SupplierOrderLine
     * @throws CardTextExceedsMaximumLengthException
     */
    private function findCard(SupplierOrder $supplierOrder)
    {
        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $orderLine = $supplierOrderLine->getOrderLine();

            if ($orderLine->getMetadata() && array_key_exists('text', $orderLine->getMetadata())) {
                if (\strlen($orderLine->getMetadata()['text']) > 250) {
                    $exception = new CardTextExceedsMaximumLengthException();
                    $exception->setMaximumLength(250);
                    $exception->setText($orderLine->getMetadata()['text']);

                    throw $exception;
                }

                return $supplierOrderLine;
            }
        }

        return null;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return null|SupplierOrderLine
     */
    private function findDesign(SupplierOrder $supplierOrder)
    {
        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $orderLine = $supplierOrderLine->getOrderLine();

            if ($orderLine->getMetadata() && array_key_exists('is_design',
                    $orderLine->getMetadata()) && array_key_exists('design_url', $orderLine->getMetadata())) {
                return $supplierOrderLine;
            }
        }

        return null;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return null|SupplierOrderLine
     */
    private function findLetter(SupplierOrder $supplierOrder)
    {
        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $orderLine = $supplierOrderLine->getOrderLine();

            if ($orderLine->getMetadata() && array_key_exists('is_letter',
                    $orderLine->getMetadata()) && !array_key_exists('letter_url', $orderLine->getMetadata())) {
                return $supplierOrderLine;
            }
        }

        return null;
    }

    /**
     * @return Filesystem
     */
    private function getFtp()
    {
        if (!$this->ftp) {
            $this->ftp = new Filesystem(new Ftp([
                'host' => $this->getParameter('ftp_hostname'),
                'username' => $this->getParameter('ftp_username'),
                'password' => $this->getParameter('ftp_password'),
                'root' => '/' . $this->getParameter('ftp_path'),
            ]));
        }

        return $this->ftp;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string|resource $source
     * @param int $lineNumber
     *
     * @return bool
     * @throws GuzzleException
     */
    private function upload(SupplierOrder $supplierOrder, $source, int $lineNumber)
    {
        $tmp = null;
        if (\is_string($source)) {
            $tmp = tempnam(sys_get_temp_dir(), 'tasty');

            $this->defaultClient->request('GET', $source, [
                'sink' => $tmp,
            ]);

            $filename = basename($source);

            $resource = fopen($tmp, 'rb');

            if ($resource === false) {
                unlink($tmp);
                throw new \RuntimeException('Unable to open tmp file');
            }
        } else {
            $filename = basename(stream_get_meta_data($source)['uri']) . '.pdf';

            $resource = $source;
        }

        $ftp = $this->getFtp();

        if (!$ftp->has('/' . $supplierOrder->getOrder()->getOrderNumber())) {
            $ftp->createDir('/' . $supplierOrder->getOrder()->getOrderNumber());
        }

        $remotePath = '/' . $supplierOrder->getOrder()->getOrderNumber() . '/' . $lineNumber . '-' . $filename;

        $result = $ftp->putStream($remotePath, $resource);

        if (\is_string($source)) {
            fclose($resource);
            unlink($tmp);
        }

        return $result;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return \DateTime
     * @throws \Exception
     */
    private function calculateShippingDate(SupplierOrder $supplierOrder)
    {
        $deliveryDate = $supplierOrder->getOrder()->getDeliveryDate();

        if (!$deliveryDate) {
            $shippingDate = new \DateTime();
            $shippingDate->setTime(0, 0, 0);
        } else {
            $shippingDate = clone $deliveryDate;
            $shippingDate->setTime(0, 0, 0);
            $shippingDate->sub(new \DateInterval('P1D'));
        }

        switch ($shippingDate->format('l')) {
            case 'Saturday':
                $shippingDate->sub(new \DateInterval('P1D'));
                break;
            case 'Sunday':
                $shippingDate->sub(new \DateInterval('P2D'));
                break;
        }

        return $shippingDate;
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        $form->add('webservice_wsdl', TextType::class, [
            'label' => 'Webservice WSDL',
            'data' => 'https://orders.chocotelegram.nl/api/inbound.asmx?WSDL',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('customer_number', TextType::class, [
            'label' => 'Klantnummer',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('email', EmailType::class, [
            'label' => 'Notificatie emailadres',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('phone', TextType::class, [
            'label' => 'Notificatie telefoonnummer',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('ftp_hostname', TextType::class, [
            'label' => 'FTP server',
            'data' => '145.131.209.119',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('ftp_username', TextType::class, [
            'label' => 'FTP gebruikersnaam',
            'data' => 'Topgeschenken',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('ftp_password', TextType::class, [
            'label' => 'FTP wachtwoord',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('ftp_path', TextType::class, [
            'label' => 'FTP map',
            'data' => 'TG Images',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);
    }
}
