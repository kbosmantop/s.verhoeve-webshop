<?php

namespace AppBundle\Connector;

use AppBundle\Entity\Relation\Company as Supplier;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Interfaces\ConnectorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Workflow\Registry;

/**
 * Class AbstractConnector
 * @package AppBundle\Connector
 */
abstract class AbstractConnector implements ConnectorInterface
{
    /**
     * @var string
     */
    protected $color = '#000000';

    /**
     * @var Client
     */
    protected $guzzleClient;

    /**
     * @var bool
     */
    protected $requireForwardingPrices = false;

    /**
     * @var bool
     */
    protected $alwaysShowForwardingPrices = true;

    /**
     * @var ArrayCollection
     */
    private $parameters;

    /**
     * @var bool
     */
    protected $commissionable = false;

    /**
     * @var string
     */
    protected $shortName;

    /**
     * @var string
     */
    protected $packingSlipPaperSize = 'A4';

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Registry
     */
    private $workflow;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $parameterBag
     * @param Registry $workflow
     *
     * @param LoggerInterface $logger
     *
     * @required
     */
    public function setDependencies(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        Registry $workflow,
        LoggerInterface $logger
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->workflow = $workflow;
        $this->logger = $logger;
    }

    /**
     * @param Supplier $supplier
     * @throws \ReflectionException
     */
    public function setSupplier(Supplier $supplier){
        $this->fetchParameters($supplier);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getName()
    {
        $reflectionClass = new \ReflectionClass($this);

        return $reflectionClass->getShortName();
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getShortName()
    {
        if (empty($this->shortName)) {
            return substr($this->getName(), 0, 16) . '…';
        }

        return $this->shortName;
    }

    /**
     * @return bool
     */
    public function requireForwardingPrices()
    {
        return $this->requireForwardingPrices;
    }

    /**
     * @return bool
     */
    public function alwaysShowForwardingPrices()
    {
        return $this->alwaysShowForwardingPrices;
    }

    /**
     * @return string
     */
    public function getPackingSlipPaperSize(): string
    {
        return $this->packingSlipPaperSize;
    }

    /**
     * @return FormBuilderInterface|null
     * @throws \ReflectionException
     */
    public function getConnectorParametersFormBuilder()
    {
        $name = (new \ReflectionClass($this))->getShortName();

        $form = $this->formFactory->createNamedBuilder($name, FormType::class, null, [
            'mapped' => false,
            'label' => false,
        ]);

        $this->addFormBuilderParameters($form);

        if (\count($form->all()) === 0) {
            return null;
        }

        return $form;
    }

    /**
     * @param $name string
     * @return mixed
     */
    final public function getParameter($name)
    {
        return $this->parameters->get($name);
    }

    /**
     * @param Supplier $supplier
     * @throws \ReflectionException
     */
    private function fetchParameters(Supplier $supplier)
    {
        $reflectionClass = new \ReflectionClass($this);

        /** @var CompanyConnectorParameter[] $results */
        $results = $this->entityManager->getRepository(CompanyConnectorParameter::class)->findBy([
            'company' => $supplier,
            'connector' => $reflectionClass->getShortName(),
        ]);

        $this->parameters = new ArrayCollection();

        foreach ($results as $result) {
            $this->parameters->set($result->getName(), $result->getValue());
        }
    }

    /**
     * @param Supplier $supplier
     * @return null
     */
    public function login(Supplier $supplier)
    {
        return null;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getColor()
    {
        if (null === $this->color) {
            throw new \RuntimeException('Color not set');
        }

        return $this->color;
    }

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();
        $workflow = $this->workflow->get($order);

        if($workflow->can($order, 'forward_to_supplier')) {
            $workflow->apply($order, 'forward_to_supplier');
        } else {
            // Shouldn't happen. Log and ignore for now.
            // TODO workflow refactor
            $this->logger->error(sprintf('Unable to use transition "forward_to_supplier" for order %s ', $order->getId()));
        }

        $supplierOrder->getOrder()->setSupplierOrder($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function cancel(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();
        $workflow = $this->workflow->get($order);

        if ($workflow->can($order, 'recall')) {
            $workflow->apply($order, 'recall');
        }

        $supplierOrder->getOrder()->setSupplierOrder(null);
    }

    /**
     * @param null $url
     * @return Client
     * @throws \Exception
     */
    public function getGuzzleClient($url = null)
    {
        if (null === $url) {
            throw new \RuntimeException('Connector url not set');
        }

        $authToken = $this->parameterBag->get('temp_api_authorization_token');

        if (!$this->guzzleClient) {
            $this->guzzleClient = new Client([
                'base_uri' => $url,
                'headers' => [
                    'Authorization' => $authToken,
                ],
            ]);
        }

        return $this->guzzleClient;
    }

    /**
     * @return bool
     */
    final public function getCommissionable()
    {
        return $this->commissionable;
    }
}
