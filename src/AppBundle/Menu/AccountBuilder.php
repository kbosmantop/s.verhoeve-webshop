<?php

namespace AppBundle\Menu;

use AppBundle\Entity\Security\Customer\Customer;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Translation\Common\Model\Message;

/**
 * Class AccountBuilder
 * @package AppBundle\Menu
 */
class AccountBuilder
{
    use ContainerAwareTrait;

    private $factory;

    public static function getTranslationMessages()
    {
        $messages = [];

        array_push($messages, new Message("customer.account.page_title", "messages"));
        array_push($messages, new Message("customer.account.orders.page_title", "messages"));
        array_push($messages, new Message("customer.account.settings.heading", "messages"));
        array_push($messages, new Message("customer.account.settings.page_title", "messages"));
        array_push($messages, new Message("customer.account.addressbook.page_title", "messages"));
        array_push($messages, new Message("customer.account.change_password.page_title", "messages"));

        array_push($messages, new Message("customer.account.company.page_title", "messages"));
        array_push($messages, new Message("customer.account.orders.page_title", "messages"));
        array_push($messages, new Message("customer.account.invoices.page_title", "messages"));
        array_push($messages, new Message("customer.account.users.page_title", "messages"));
        array_push($messages, new Message("customer.account.company.settings.heading", "messages"));
        array_push($messages, new Message("customer.account.company.settings.page_title", "messages"));
        array_push($messages, new Message("customer.account.newsletter.page_title", "messages"));

        return $messages;
    }

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createCustomerMenu(RequestStack $requestStack)
    {
        void($requestStack);

        $menu = $this->factory->createItem('root', [
            'childrenAttributes' => [],
        ]);

        $menu->addChild('customer.account.page_title', ['route' => 'app_customer_account_index']);
        $menu->addChild('customer.account.orders.page_title', ['route' => 'app_customer_account_order_index']);
//        $menu->addChild('customer.account.invoices.page_title', array('route' => 'app_customer_account_invoice_index'));

        $menu->addChild('customer.account.settings.heading',
            ['route' => 'app_customer_account_index'])->setAttributes([
            'navigation_header' => true,
        ]);

        $menu->addChild('customer.account.settings.page_title',
            ['route' => 'app_customer_account_settings_index']);
        $menu->addChild('customer.account.addressbook.page_title',
            ['route' => 'app_customer_account_addressbook_index']);
        $menu->addChild('customer.account.change_password.page_title',
            ['route' => 'app_customer_account_changepassword_index']);
//        $menu->addChild('customer.account.newsletter.page_title', array('route' => 'app_customer_account_newsletter_index'));
//
//        if ($this->container->get('security.token_storage')->getToken()->getUser()->getFacebookId()) {
//            $menu->addChild('customer.account.facebook.page_title', array('route' => 'app_customer_account_facebook_index'));
//        }

        return $menu;
    }

    public function createCompanyMenu(RequestStack $requestStack)
    {
        /** @var Customer $customer */
        $customer = $this->container->get('security.token_storage')->getToken()->getUser();
        $company = $customer->getCompany();

        $menu = $this->factory->createItem('root', [
            'childrenAttributes' => [],
        ]);

        $menu->addChild('customer.account.company.page_title', ['route' => 'app_customer_account_index']);
        $menu->addChild('customer.account.orders.page_title', ['route' => 'app_customer_account_order_index']);
        $menu->addChild('customer.account.report.page_title', ['route' => 'app_customer_account_report_index']);
//        $menu->addChild('customer.account.invoices.page_title', array('route' => 'app_customer_account_invoice_index'));

        $menu->addChild('customer.account.company.settings.heading',
            ['route' => 'app_customer_account_index'])->setAttributes([
            'navigation_header' => true,
        ]);

        if (null !== $company && !$customer->isMigrated() && $company->getCustomers()->count() <= 1) {
            $menu->addChild('customer.account.company.settings.page_title',
                ['route' => 'app_customer_account_settings_index']);
        }

        $menu->addChild('customer.account.users.page_title', ['route' => 'app_customer_account_user_index']);


        $menu->addChild('customer.account.addressbook.page_title',
            ['route' => 'app_customer_account_addressbook_index']);
        $menu->addChild('customer.account.change_password.page_title',
            ['route' => 'app_customer_account_changepassword_index']);
//        $menu->addChild('customer.account.newsletter.page_title', array('route' => 'topbloemen_site_customer_account_newsletter_index'));

        return $menu;
    }
}
