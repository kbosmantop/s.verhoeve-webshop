<?php

namespace AppBundle\Security;

use AppBundle\Services\CartService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * Class LogoutSuccessHandler
 * @package AppBundle\Security
 */
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * AuthenticationHandler constructor.
     *
     * @param Session     $session
     * @param CartService $cartService
     *
     */
    public function __construct(Session $session, CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->session = $session;
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function onLogoutSuccess(Request $request)
    {
        void($request);

        $this->session->invalidate();

        // Force creating a new anonymous cart
        $cart = $this->cartService->createCart(true);
        $this->cartService->setCart($cart);

        $cookie = new Cookie('cart', $cart->getUuid(), new \DateTime('+5 days'));

        $response = new RedirectResponse('/');
        $response->headers->setCookie($cookie);

        return $response;
    }
}
