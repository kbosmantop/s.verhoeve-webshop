<?php

namespace AppBundle\Security\Customer;

use AppBundle\Services\Customer\AuthenticationLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;

/**
 * Class AuthenticationLoginFailureHandler
 * @package AppBundle\Security\Customer
 */
class AuthenticationLoginFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @var AuthenticationLogger
     */
    private $authenticationLogHandler;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param AuthenticationLogger $authenticationLogHandler
     */
    public function setAuthenticationLogger(AuthenticationLogger $authenticationLogHandler)
    {
        $this->authenticationLogHandler = $authenticationLogHandler;
    }

    /**
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response never null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($exception instanceof BadCredentialsException) {
            $reason = AuthenticationLogger::REASON_INVALID_CREDENTIALS;
        } else {
            $reason = AuthenticationLogger::REASON_UNKNOWN;
        }

        $this->authenticationLogHandler->failure($request->request->get('_username'), $reason);

        return parent::onAuthenticationFailure($request, $exception);
    }
}
