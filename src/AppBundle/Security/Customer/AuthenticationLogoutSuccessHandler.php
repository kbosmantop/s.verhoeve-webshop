<?php


namespace AppBundle\Security\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Services\CartService;
use AppBundle\Services\Customer\AuthenticationLogger;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

/**
 * Class AuthenticationLogoutSuccessHandler
 * @package AppBundle\Security\Customer
 */
class AuthenticationLogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    use ContainerAwareTrait;

    /**
     * @var AuthenticationLogger
     */
    private $authenticationLogHandler;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var CartService $cartService
     */
    private $cartService;

    /**
     * @var Session $session
     */
    private $session;

    /**
     * init method
     */
    public function init()
    {
        $this->authenticationLogHandler = $this->container->get('app.customer.auth_logger');
        $this->tokenStorage = $this->container->get('security.token_storage');
        $this->cartService = $this->container->get(CartService::class);
        $this->session = $this->container->get('session');
    }

    /**
     *
     * @param Request $request
     *
     * @return Response never null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function onLogoutSuccess(Request $request)
    {
        $username = null;

        /** @var Customer $customer */
        $customer = null;

        if ($this->tokenStorage->getToken()) {
            if ($this->tokenStorage->getToken()->getUser() instanceof Customer) {
                $customer = $this->tokenStorage->getToken()->getUser();
            }

            if ($this->tokenStorage->getToken()->getUsername()) {
                $username = $this->tokenStorage->getToken()->getUsername();
            }

            $type = 'logout';
            $employee = null;
            $session = $request->getSession();

            if ($session !== null && $customer !== null) {
                if ($session->has('employee_login_as_customer')) {
                    $employee = $this->container->get('doctrine')->getRepository(User::class)->find($session->get('employee_id'));

                    $type = 'employee_logout';
                }

                $this->authenticationLogHandler->success($type, $username, $customer, null, null, $employee);
            }
        }

        $this->invalidateCart();

        return parent::onLogoutSuccess($request);
    }

    /**
     * Invalidate the current cart and start a new Anonymous one.
     *
     * @return RedirectResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function invalidateCart()
    {
        $this->session->invalidate();

        // Force creating a new anonymous cart
        $cart = $this->cartService->createCart(true);
        $this->cartService->setCart($cart);

        $cookie = new Cookie('cart', $cart->getUuid(), new \DateTime('+5 days'));

        $response = new RedirectResponse('/');
        $response->headers->setCookie($cookie);

        return $response;
    }
}