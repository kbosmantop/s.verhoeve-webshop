<?php

namespace AppBundle\Security;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Services\CartService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * TODO this belongs to HTTP CACHING, not used, may be removed
 *
 * Class CustomerProvider
 * @package AppBundle\Security
 */
class CustomerProvider implements UserProviderInterface
{
    use ContainerAwareTrait;

    protected $user;

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @throws UsernameNotFoundException if the user is not found
     * @param string $username The username
     *
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        $user = User::find(['username' => $username]);

        if (empty($user)) {
            throw new UsernameNotFoundException('Could not find user. Sorry!');
        }

        $this->user = $user;

        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation if it decides to reload the user data
     * from the database, or if it simply merges the passed User into the
     * identity map of an entity manager.
     *
     * @throws UnsupportedUserException if the account is not supported
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function loginUser($username, $password)
    {
        $loginManager = $this->container->get('fos_user.security.login_manager');
        $user_manager = $this->container->get('fos_user.user_manager');

        $factory = $this->container->get('security.encoder_factory');

        /** @var Customer $user */
        $user = $user_manager->findUserByUsername($username);

        if (!$user) {
            return false;
        }

        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        if ($encoder->isPasswordValid($user->getPassword(), $password, $salt)) {
            try {
                $loginManager->logInUser('main', $user);
            } catch (\Exception $e) {
                return false;
            }

            if (!$this->container->get('session')->has('employee_login_as_customer')) {
                $this->container->get(CartService::class)->migrate($user);
            }

            return true;
        }

        return false;
    }

    /**
     * Whether this provider supports the given user class
     *
     * @param string $class
     *
     * @return Boolean
     */
    public function supportsClass($class)
    {
        return $class === 'AppBundle\Entity\Security\Customer\Customer';
    }
}
