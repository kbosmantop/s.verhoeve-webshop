<?php

namespace AppBundle\Model\Product;

use AppBundle\Model\BaseModel;

/**
 * Class CombinationModel
 * @package AppBundle\Model\Product
 */
class CombinationModel extends BaseModel {

    /**
     * @return int
     * @throws \Exception
     */
    public function getMaxQuantity() {
        if($this->getMetadata() && isset($this->getMetadata()['max_quantity'])) {
            return $this->getMetadata()['max_quantity'];
        }

        return $this->container->get('app.parameter')->setEntity()->getValue('combination_max_quantity');
    }

    /**
     * @param $quantity
     */
    public function setMaxQuantity($quantity) {
        $metadata = $this->getMetadata();
        $metadata['max_quantity'] = $quantity;

        $this->setMetadata($metadata);
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getMinQuantity() {
        if($this->getMetadata() && isset($this->getMetadata()['min_quantity'])) {
            return $this->getMetadata()['min_quantity'];
        }

        return $this->container->get('app.parameter')->setEntity()->getValue('combination_min_quantity');
    }

    /**
     * @param $quantity
     */
    public function setMinQuantity($quantity) {
        $metadata = $this->getMetadata();
        $metadata['min_quantity'] = $quantity;

        $this->setMetadata($metadata);
    }

}