<?php

namespace AppBundle\Model;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Abstract Class BaseModel
 * @package AppBundle\Model
 */
abstract class BaseModel implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function init()
    {

    }
}
