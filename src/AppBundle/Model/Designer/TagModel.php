<?php

namespace AppBundle\Model\Designer;

use AppBundle\Model\BaseModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class DesignerTagModel
 * @package AppBundle\Model
 */
class TagModel extends BaseModel implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Init model
     */
    public function init()
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
    }

}
