<?php

namespace AppBundle\Model\Designer;

use AppBundle\Model\BaseModel;
use AppBundle\Services\Designer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class DesignerTemplateModel
 * @package AppBundle\Model
 */
class TemplateModel extends BaseModel implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Designer
     */
    protected $designerService;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Init model
     */
    public function init()
    {
        $designerService = $this->container->get('app.designer');
        $this->setDesignerService($designerService);

        $this->em = $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param Designer $designerService
     */
    private function setDesignerService(Designer $designerService)
    {
        $designerService->setUuid($this->uuid);
        $designerService->setIsPreDesign(true);

        $this->designerService = $designerService;
    }

    /**
     * @return null|string
     */
    public function getPreviewUrl()
    {
        return $this->designerService->getFileUrl(Designer::FILE_PREVIEW);
    }
}
