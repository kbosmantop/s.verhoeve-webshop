<?php

namespace AppBundle\Model;

use AppBundle\Entity\Order\Collo;
use AppBundle\Entity\Order\Order;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Services\SiteService;

/**
 * Class OrderModel
 * @package AppBundle\Model
 */
class OrderModel extends BaseModel
{
    /**
     * @todo: determine if display is excl or incl vat
     * @return string
     */
    public function getDisplayTotalPrice()
    {
        $site = null;

        if ($this->container->get(SiteService::class)->determineSite()) {
            $site = $this->container->get(SiteService::class)->determineSite();
        }

        $siteDefaultDisplayPrice = ($site) ? $site->getDefaultDisplayPrice() : 'inclusive';
        if($siteDefaultDisplayPrice === 'exclusive') {
            $price = $this->getTotalPrice();
        } else {
            $price = $this->getTotalPriceIncl();
        }

        return number_format($price, 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getDisplayTotalPriceExcl()
    {
        return number_format($this->getTotalPrice(), 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getDisplayTotalPriceIncl()
    {
        return number_format($this->getTotalPriceIncl(), 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getDisplayTotalTax()
    {
        return number_format($this->getTotalPriceIncl() - $this->getTotalPrice(), 2, ',', '.');
    }

    /**
     * @return float
     */
    public function getTotalDisplayPrice()
    {
        switch ($this->getOrderCollection()->getSite()->getDefaultDisplayPrice()) {
            case 'exclusive':
                return $this->getTotalPrice();
            default:
                return $this->getTotalPriceIncl();
        }
    }

    /**
     * @return float
     */
    public function getTotalDiscountAmount() {
        $total = 0;

        foreach($this->getLines() as $line) {
            $total += $line->getDiscountAmount();
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getTotalDiscountAmountExcl() {
        $total = 0;

        foreach($this->getLines() as $line) {
            $total += $line->getDiscountAmountExcl();
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getTotalDiscountAmountIncl() {
        $total = 0;

        foreach($this->getLines() as $line) {
            $total += $line->getDiscountAmountIncl();
        }

        return $total;
    }

    /**
     * @return string
     */
    public function getCompositeNumber()
    {
        return $this->getOrder()->getNumber() . '-' . $this->getNumber();
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        /**
         * @var Order $this
         */
        return $this->getStatus() === 'cancelled';
    }

    /**
     * @return bool
     */
    public function isProcessed()
    {
        /**
         * @var Order $this
         */
        return $this->getStatus() === 'processed';
    }

    /**
     * @param int $number
     *
     * @return string
     */
    public static function parseNumber(int $number)
    {
        return str_pad($number, 4, '0', STR_PAD_LEFT);
    }

    /**
     * @return Collo[]|ArrayCollection
     */
    public function getSentOrderCollos(): ArrayCollection
    {
        /** @var Order $order */
        $order = $this;

        return $order->getCollos()->filter(function(Collo $collo) {
            $activities = $collo->getOrder()->getActivities();

            foreach ($activities as $activity) {
                if($activity->getActivity() === 'order_sent') {
                    return true;
                }
            }

            return false;
        });
    }
}
