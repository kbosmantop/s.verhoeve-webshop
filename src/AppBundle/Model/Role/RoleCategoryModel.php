<?php

namespace AppBundle\Model\Role;

use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Model\BaseModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class RoleCategoryModel
 * @package AppBundle\Model\Role
 */
class RoleCategoryModel extends BaseModel
{
    protected static $reservedWords = ['CREATE', 'READ', 'WRITE', 'COLUMN'];

    /**
     * @return array|ObjectRepository
     */
    public function getUsers()
    {
        /** @var EntityRepository $users */
        $users = $this->container->get('doctrine')->getRepository(User::class);

        $qb = $users->createQueryBuilder('u');
        $qb->leftJoin('u.roles', 'r');
        $qb->andWhere('r.id IN(:ids)');
        $qb->setParameter('ids', $this->getRoleIds());

        $users = $qb->getQuery()->getResult();

        return $users;
    }

    /**
     * @return array|ObjectRepository
     */
    public function getGroups()
    {
        /** @var EntityRepository $groups */
        $groups = $this->container->get('doctrine')->getRepository(Group::class);

        $qb = $groups->createQueryBuilder('g');
        $qb->leftJoin('g.roles', 'r');
        $qb->andWhere('r.id IN(:ids)');
        $qb->setParameter('ids', $this->getRoleIds());

        $groups = $qb->getQuery()->getResult();

        return $groups;
    }

    /**
     * @return mixed
     */
    public function getRoleIds()
    {
        $roles = $this->getRoles(false)->toArray();
        array_map(function ($role) {
            $categoryRoles[] = $role->getId();
        }, $roles);

        return $roles;
    }

    /**
     * @return bool
     */
    public function hasFieldsSpecified()
    {
        return !$this->getColumnRoles()->isEmpty();
    }

    /**
     * @return ArrayCollection
     */
    public function getColumnRoles()
    {
        $columnRoles = new ArrayCollection();

        /** @var Role $role */
        foreach ($this->getRoles() as $role) {
            if (strpos($role->getName(), 'COLUMN')) {
                $columnRoles->add($role);
            }
        }

        return $columnRoles;
    }

    /**
     * @return ArrayCollection
     */
    public function getRootRoles()
    {
        $rootRoles = new ArrayCollection();
        $columnRoles = $this->getColumnRoles();
        $specialRoles = $this->getSpecialRoles();

        foreach ($this->getRoles() as $role) {
            if (!$columnRoles->contains($role) && !$specialRoles->contains($role)) {
                $rootRoles->add($role);
            }
        }

        return $rootRoles->toArray();
    }

    /**
     * @return array
     */
    public function getShortRootRoles()
    {
        $roles = [];

        /** @var Role $role */
        foreach ($this->getRootRoles() as $role) {
            $roles[] = [
                'id' => $role->getId(),
                'name' => $role->getDisplayName(),
            ];
        }

        return $roles;
    }

    /**
     * @return ArrayCollection
     */
    public function getSpecialRoles()
    {
        $roles = new ArrayCollection();

        foreach ($this->getRoles() as $role) {
            $addRole = true;
            foreach (self::$reservedWords as $word) {
                if (stripos($role->getName(), $word)) {
                    $addRole = false;
                }
            }

            if ($addRole) {
                $roles->add($role);
            }
        }

        return $roles;
    }

    /**
     * @return string
     */
    public function guessRolePrefix()
    {
        $role = $this->getRoles()->current();
        $role = trim(str_replace(self::$reservedWords, null, $role), '_');

        return $role;
    }
}
