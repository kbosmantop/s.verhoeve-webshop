<?php

namespace AppBundle\Model\Role;

use AppBundle\Model\BaseModel;

class RoleModel extends BaseModel
{

    /**
     * return string
     */
    public function getDisplayName()
    {
        return $this->getDescription() ? $this->getDescription() : $this->getName();
    }

}