<?php

namespace AppBundle\Model\Order;

use AppBundle\Entity\Order\Cart;

/**
 * Class CartLineModel
 * @package AppBundle\Model\Order
 */
class CartLineModel extends LineModel {

    /**
     * @var Cart $cart
     */
    protected $cart;

    /**
     * @param Cart $cart
     * @return $this
     */
    public function setCart(Cart $cart) {
        $this->cart = $cart;

        return $this;
    }

    /**
     * @return Cart
     */
    public function getCart() {
        return $this->cart;
    }

}