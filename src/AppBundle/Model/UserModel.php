<?php

namespace AppBundle\Model;

use AppBundle\Entity\Security\Employee\Role;
use function is_object;

/**
 * Class UserModel
 * @package AppBundle\Model
 */
class UserModel extends BaseModel
{
    /**
     * @param      $role
     * @param bool $withGroups
     *
     * @return bool
     */
    public function hasRole($role, $withGroups = true): bool
    {
        if(is_object($role) && $role instanceof Role) {
            return $this->getRoles($withGroups, true)->contains($role);
        }

        return \in_array($role, $this->getRoles($withGroups), true);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

}
