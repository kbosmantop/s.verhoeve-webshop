<?php

namespace AppBundle\Model;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Services\Designer;
use AppBundle\Services\Letter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class OrderLineModel
 * @package AppBundle\Model
 */
class OrderLineModel extends BaseModel
{
    /**
     * @return string
     */
    public function getDisplayPrice()
    {
        $i = $this->getOrder()->getOrderCollection()->getSite()->getDefaultDisplayPrice();
        if ($i === 'exclusive') {
            return number_format($this->getPrice(), 2, ',', '.');
        }

        return number_format($this->getPriceIncl(), 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getDisplayAmount()
    {
        $i = $this->getOrder()->getOrderCollection()->getSite()->getDefaultDisplayPrice();
        if ($i === 'exclusive') {
            return number_format($this->getTotalPrice(), 2, ',', '.');
        }

        return number_format($this->getTotalPriceIncl(), 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getDisplayDiscountAmount()
    {
        $i = $this->getOrder()->getOrderCollection()->getSite()->getDefaultDisplayPrice();
        if ($i === 'exclusive') {
            return number_format($this->getDiscountAmount(), 2, ',', '.');
        }

        return number_format($this->getDiscountAmountIncl(), 2, ',', '.');
    }

    /**
     * Get UUID of the design.
     *
     * @return null|string
     */
    public function getDesignUuid()
    {
        return ($this->getMetadata() && isset($this->getMetadata()['designer']) ? $this->getMetadata()['designer'] : null);
    }

    /**
     * @return null|\object
     * @throws \Exception
     */
    public function getProcess()
    {
        /** @var Order $orderOrder */
        $orderOrder = $this->getOrder();
        $orderCollection = $orderOrder->getOrderCollection();
        $referenceNumber = $orderCollection->getNumber() . $orderOrder->getNumber();
        $key = $orderOrder->getNumber() - 1;
        $url = $this->container->getParameter('connector_bakker_api_url');

        try {
            $process = @file_get_contents("{$url}/tgapi/export/delivery-status/{$referenceNumber}");
            if ($process === false) {
                return (object)['processed' => '0', 'status' => 'Error'];
            }
            $process = json_decode($process);
            if (array_key_exists($key, $process)) {
                return $process[$key];
            }
            return null;
        } catch (\Exception $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }



    /**
     * @param Product $product
     * @return int
     */
    public function getConfigurationQuantity(Product $product)
    {
        if ($this->getMetadata() && isset($this->getMetadata()['configuration']) && \is_array($this->getMetadata()['configuration'])) {
            foreach ($this->getMetadata()['configuration'] as $configuration) {
                if ($product->getId() !== $configuration['productId']) {
                    continue;
                }
                return $configuration['quantity'];
            }
        }

        return 1;
    }

    /**
     * @throws \Exception
     * @throws NotFoundHttpException
     * @return StreamedResponse
     */
    public function downloadLetter()
    {
        $metaData = $this->getMetadata();

        if (!isset($metaData['letter'])) {
            throw new \RuntimeException('No letter uploaded');
        }

        /** @var Letter $Letter */
        $Letter = $this->container->get('app.letter');
        $Letter->setUuid($metaData['letter']['uuid']);

        if (null === ($downloadUrl = $Letter->getUrl())) {
            throw new \RuntimeException('No letter uploaded');
        }

        $orderNumber = $this->getOrder()->getOrderNumber();

        $ext = pathinfo(strstr($downloadUrl, '?', true), PATHINFO_EXTENSION);

        $response = $this->container
            ->get('topgeschenken.proxy')
            ->process($downloadUrl);

        $response->headers->set('Content-Disposition', 'attachment; filename="Brief-' . $metaData['letter']['uuid'] . '-'.$orderNumber.'.' . $ext . '";');

        return $response;
    }

    /**
     * @return bool
     */
    public function hasRequiredPersonalization()
    {
        /** @var Product $product */
        $product = $this->getProduct();

        return $product->isRequiredPersonalization() || $this->getParent()->getProduct()->hasRequiredPersonalization();
    }

    /**
     * @return float|int
     * @deprecated Use OrderLineManager::getDiscountAmountExcl instead
     */
    public function getDiscountAmountExcl() {
        return ($this->getDiscountAmount() / ($this->getProduct()->getVat()->getPercentage() + 100) * 100);
    }

    /**
     * Checks whether this line has cards for the product
     *
     * @return boolean
     */
    public function hasProductCard()
    {
        $hasProductPersonalization = $this->getChildren()->filter(function (OrderLine $orderLine) {
            /** @var $cartOrderLine OrderLine */
            return ($orderLine->getMetadata() && !empty($orderLine->getMetadata()['is_card']));
        });

        return !$hasProductPersonalization->isEmpty();
    }

    /**
     * Checks whether this line has cards for the product
     *
     * @return boolean
     */
    public function getProductCard()
    {
        $productCard = $this->getChildren()->filter(function (OrderLine $orderLine) {
            /** @var $cartOrderLine $orderLine */
            return ($orderLine->getMetadata() && !empty($orderLine->getMetadata()['is_card']));
        });

        if ($productCard) {
            return $productCard->first();
        }

        return $productCard;
    }

    /**
     * Checks whether this line has personalization for the product
     *
     * @return boolean
     */
    public function hasProductPersonalization()
    {
        return (bool)$this->getPersonalizationLine();
    }

    /**
     * Get personalization child line
     *
     * @return OrderLine|null
     */
    public function getPersonalizationLine()
    {
        /** @var ArrayCollection $productPersonalizationLine */
        $productPersonalizationLine = $this->getChildren()->filter(function (OrderLine $orderLine) {
            /** @var $orderLine OrderLine */
            return (null === $orderLine->getDeletedAt() && $orderLine->getMetadata() && !empty($orderLine->getMetadata()['designer']));
        });

        if (!$productPersonalizationLine->isEmpty()) {
            return $productPersonalizationLine->first();
        }

        return null;
    }

    /**
     * @return Product
     */
    public function getMainProduct(): Product
    {
        /** @var OrderLine $orderLine */
        $orderLine = $this;

        $mainProduct = $orderLine->getProduct();

        if($orderLine->getParent()) {
            $mainProduct = $orderLine->getParent()->getProduct();
        }

        if($orderLine->getProduct()->isCombination() && (null !== ($mainCombination = $orderLine->getProduct()->getMainCombination()))) {
            $mainProduct = $mainCombination->getProduct();
        }

        return $mainProduct;
    }
}

