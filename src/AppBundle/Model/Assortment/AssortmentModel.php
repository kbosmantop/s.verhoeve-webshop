<?php

namespace AppBundle\Model\Assortment;

use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Site\Site;
use AppBundle\Model\BaseModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

/**
 * Class AssortmentModel
 * @package AppBundle\Model\Assortment
 */
class AssortmentModel extends BaseModel
{

    public function clearSites()
    {
        $this->sites->clear();
    }

    /**
     * @return string
     */
    public function getMinimumDisplayPrice()
    {
        return number_format($this->getMinimumPrice(), 2, ',', '.');
    }

    /**
     * @return float|int
     */
    public function getMinimumPrice()
    {
        $minPrice = 0;

        foreach ($this->getProducts() as $product) {
            if ($minPrice === 0 || $product->getPrice() < $minPrice) {
                $minPrice = $product->getPrice();
            }
        }

        return $minPrice;
    }

    /**
     * @return float|int
     */
    public function getMaximumPrice()
    {
        $maxPrice = 0;

        foreach ($this->getProducts() as $product) {
            if ($product->getPrice() > $maxPrice) {
                $maxPrice = $product->getPrice();
            }
        }

        return $maxPrice;
    }

    /**
     * Get products
     *
     * @return ArrayCollection|Product[]
     */
    public function getProducts()
    {
        $products = new ArrayCollection();

        /** @var AssortmentProduct $assortmentProduct */
        foreach ($this->getAssortmentProducts() as $assortmentProduct) {
            try {
                if (method_exists($assortmentProduct->getProduct(),
                        'isPublished') && !$assortmentProduct->getProduct()->isPublished()) {
                    continue;
                }

                $product = $assortmentProduct->getProduct();
                if ($product) {
                    if (!$product->hasVariations() && !$product->isCombination()) {
                        $product = $product->getVariations()->current();
                    }

                    $products[] = $product;
                }
            } catch (EntityNotFoundException $e) {
                continue;
            }
        }

        return $products;
    }

    /**
     * @return string
     */
    public function getMinimumDisplayPriceIncl()
    {
        $price = $this->getMinimumPriceIncl();
        //check if number is already formatted
        if (!is_numeric($price)) {
            return $price;
        }

        return number_format($price, 2, ',', '.');
    }

    /**
     * @return mixed
     */
    public function getMinimumPriceIncl()
    {
        $prices = [];

        foreach ($this->getProducts() as $product) {
            if (null === $product) {
                continue;
            }

            $prices[] = $product->getPriceIncl();
        }

        asort($prices);

        return array_shift($prices);
    }

    /**
     * @param null      $language
     * @param Site|null $site
     * @param null      $referenceType
     * @return null|string
     */
    public function getUrl($language = null, Site $site = null, $referenceType = null)
    {
        return $this->container->get('app.slug')->getUrl($this, $language, $site, $referenceType);
    }
}
