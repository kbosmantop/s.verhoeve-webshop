<?php

namespace AppBundle\Gateway;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Interfaces\GatewayInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Topgeschenken extends AbstractGateway implements GatewayInterface
{
    use ContainerAwareTrait;

    /**
     * @param Payment $payment
     *
     * @return mixed
     * @throws PaymentException
     */
    public function processPayment(Payment $payment)
    {
        $paymentMethod = $this->getPaymentMethod($payment);

        if ($paymentMethod instanceof ProcessableMethodInterface) {
            $result = $paymentMethod->processPayment($payment, $this->getRequest());

            return $result;
        }

        throw new PaymentException("Logic error");
    }
}
