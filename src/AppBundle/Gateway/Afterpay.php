<?php

namespace AppBundle\Gateway;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Interfaces\GatewayInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;
use AppBundle\Services\JobManager;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @link https://bitbucket.org/afterpay-plugins/afterpay-composer-package/src/b32404e9f29e4322c58232c75cf49c704b4fb3e6/lib/Afterpay.php?at=master&fileviewer=file-view-default
 */
class Afterpay extends AbstractGateway implements GatewayInterface
{
    use ContainerAwareTrait;

    const ENDPOINT_RM_NL_TEST = "https://test.acceptgirodienst.nl/soapservices/rm/AfterPaycheck?wsdl";
    const ENDPOINT_RM_NL_LIVE = "https://www.acceptgirodienst.nl/soapservices/rm/AfterPaycheck?wsdl";

    const ENDPOINT_OM_NL_TEST = "https://test.acceptgirodienst.nl/soapservices/om/OrderManagement?wsdl";
    const ENDPOINT_OM_NL_LIVE = "https://www.acceptgirodienst.nl/soapservices/om/OrderManagement?wsdl";

    const ENDPOINT_RM_BE_TEST = "https://test.afterpay.be/soapservices/rm/AfterPaycheck?wsdl";
    const ENDPOINT_RM_BE_LIVE = "https://api.afterpay.be/soapservices/rm/AfterPaycheck?wsdl";

    const ENDPOINT_OM_BE_TEST = "https://test.afterpay.be/soapservices/om/OrderManagement?wsdl";
    const ENDPOINT_OM_BE_LIVE = "https://mijn.afterpay.be/soapservices/om/OrderManagement?wsdl";

    const ORDER_MANAGEMENT = "om";

    /**
     * @var \SoapClient[]
     */
    private $clients = [];

    /**
     * @param Payment $payment
     *
     * @return mixed
     * @throws PaymentException
     * @throws \Exception
     */
    public function processPayment(Payment $payment)
    {
        if ($payment->getAmount() == 0) {
            throw new PaymentException("Couldn't process a payment with a zero amount");
        }

        $paymentMethod = $this->getPaymentMethod($payment);

        if ($paymentMethod instanceof ProcessableMethodInterface) {
            $result = $paymentMethod->processPayment($payment, $this->getRequest());

            return $result;
        }

        throw new PaymentException("Logic error");
    }

    /**
     * @param string $type
     *
     * @return string
     * @throws PaymentException
     */
    private function getEndpoint($type = "rm")
    {
        switch ($this->getContainer()->get("kernel")->getEnvironment()) {
            case "dev":
            case "test":
            case "staging":
                switch ($type) {
                    case "rm":
                        return self::ENDPOINT_RM_NL_TEST;
                    case self::ORDER_MANAGEMENT:
                        return self::ENDPOINT_OM_NL_TEST;
                }
                break;
            case "prod":
                switch ($type) {
                    case "rm":
                        return self::ENDPOINT_RM_NL_LIVE;
                    case self::ORDER_MANAGEMENT:
                        return self::ENDPOINT_OM_NL_LIVE;
                }
                break;
        }

        throw new PaymentException("Invalid environment '" . $this->getContainer()->get("kernel")->getEnvironment() . "'");
    }

    /**
     * @param Payment $payment
     *
     * @return bool
     * @throws \Exception
     *
     * @link https://mip.afterpay.nl/en/api-documentation/#full-capture
     * @link https://mip.afterpay.nl/en/api-documentation/#order-maintenance-objects
     */
    public function capture(Payment $payment)
    {
        $em = $this->container->get("doctrine")->getManager();

        if ($payment->getPaymentmethod()->getCode() != "afterpay") {
            throw new \Exception("Only AfterPay payments can be captured");
        }

        if ($payment->getStatus()->getId() != "authorized") {
            throw new \Exception("Only authorized payments can be captured");
        }

        try {
            $orderStatus = $this->getOrderStatus($payment);

            if ($orderStatus->totalReservedAmount > 0) {
                $data = [
                    "validateAndCheckB2COrder" => [
                        "authorization" => $this->generateAuthorization($payment),
                        "captureobject" => (object)[
                            "invoicenumber" => "F" . $payment->getOrderCollection()->getNumber(),
                            'transactionkey' => (object)[
                                'ordernumber' => $payment->getOrderCollection()->getNumber(),
                            ],
                            "capturedelaydays" => 0,
                            "shippingCompany" => '',
                        ],
                    ],
                ];

                $result = $this->getClient(self::ORDER_MANAGEMENT)->__soapCall("captureFull", $data);

                if (array_key_exists("failures", $result->return)) {
                    throw new \Exception($result->return->failures->failure);
                }

                $orderStatus = $result->return;
            }

            if ($orderStatus->totalReservedAmount == 0) {
                $metadata = $payment->getMetadata();
                $metadata['transactionId'] = $orderStatus->transactionId;

                $payment->setMetadata($metadata);
                $payment->setStatus($em->getRepository(PaymentStatus::class)->find("captured"));

                $em->flush();

                return true;
            }

            return false;
        } catch (\SoapFault $e) {
            print $e->getMessage() . PHP_EOL;

            return false;
        }
    }

    /**
     * @param Payment $payment
     *
     * @return mixed
     * @throws \Exception
     */
    public function getOrderStatus(Payment $payment)
    {
        $data = [
            "requestOrderStatus" => [
                "authorization" => $this->generateAuthorization($payment),
                "ordermanagementobject" => (object)[
                    'transactionkey' => (object)[
                        'ordernumber' => $payment->getOrderCollection()->getNumber(),
                    ],
                ],
            ],
        ];

        $result = $this->getClient(self::ORDER_MANAGEMENT)->__soapCall("requestOrderStatus", $data);

        if (array_key_exists("failures", $result->return)) {
            throw new \Exception($result->return->failures->failure);
        }

        return $result->return;
    }

    /**
     * @param string $type
     *
     * @return \SoapClient
     */
    public function getClient($type = "rm")
    {
        $endpoint = $this->getEndpoint($type);

        if (!array_key_exists($type, $this->clients)) {
            $this->clients[$endpoint] = new \SoapClient($endpoint, [
                'cache_wsdl' => WSDL_CACHE_BOTH,
                'exceptions' => true,
            ]);
        }

        return $this->clients[$endpoint];
    }

    /**
     * @param Payment $payment
     *
     * @return \stdClass
     */
    public function generateAuthorization(Payment $payment)
    {
        $authorization = new \stdClass();
        $authorization->merchantId = $payment->getOrderCollection()->getSite()->getAfterpayMerchantId();
        $authorization->portfolioId = $payment->getOrderCollection()->getSite()->getAfterpayPortfolioId();
        $authorization->password = $payment->getOrderCollection()->getSite()->getAfterpayPassword();

        return $authorization;
    }
}
