<?php

namespace AppBundle\Gateway;

use Adyen\AdyenException;
use Adyen\Client;
use Adyen\Service;
use Adyen\Util;
use AppBundle\DBAL\Types\GenderType;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Interfaces\GatewayInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;
use AppBundle\Interfaces\RedirectableMethodInterface;
use AppBundle\Services\SiteService;
use AppBundle\Utils\AmountConverter;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use libphonenumber\PhoneNumberFormat;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class Adyen
 * @package AppBundle\Gateway
 */
class Adyen extends AbstractGateway implements GatewayInterface
{
    public const ENDPOINT_TEST = 'https://test.adyen.com';
    public const ENDPOINT_LIVE = 'https://live.adyen.com';

    use ContainerAwareTrait;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param Payment $payment
     *
     * @return Response|bool
     * @throws AdyenException
     * @throws PaymentException
     * @throws \Twig_Error
     */
    public function processPayment(Payment $payment)
    {
        if ($payment->getAmount() == 0) {
            throw new PaymentException("Couldn't process a payment with a zero amount");
        }

        $paymentMethod = $this->getPaymentMethod($payment);

        if ($paymentMethod instanceof ProcessableMethodInterface) {
            return $this->authorise($payment);
        } elseif ($paymentMethod instanceof RedirectableMethodInterface) {
            $params = [
                'merchantReference' => $payment->getOrderCollection()->getPaymentDescription(),
                'paymentAmount' => AmountConverter::convert($payment->getAmount()),
                'currencyCode' => 'EUR',
                'skinCode' => $this->getSkinCode(),
                'merchantAccount' => $this->getMerchantAccount(),
                'countryCode' => $payment->getOrderCollection()->getInvoiceAddressCountry()->getCode(),
                'merchantReturnData' => (string) $payment->getUuid(),
                'shopperLocale' => $this->getRequest()->getLocale() ?: $this->getRequest()->getDefaultLocale(),
                'sessionValidity' => $this->calculateSessionValidity(),
                'shopperEmail' => $payment->getOrderCollection()->getCustomer()->getEmail(),
                'resURL' => $this->container->get('router')->generate('app_payment_index', [
                    'uuid' => $payment->getUuid(),
                ], UrlGeneratorInterface::ABSOLUTE_URL),
            ];

            $params += $paymentMethod->generateParams($payment);


            $params['merchantSig'] = Util\Util::calculateSha256Signature($this->getHmacKey(), $params);

            $url = $paymentMethod->generateUrl($this->getEndpoint(), $params);

            return new RedirectResponse($url);
        }

        throw new PaymentException('Logic error');
    }

    /**
     * @param Payment $payment
     *
     * @return bool|Response
     * @throws AdyenException
     * @throws \Twig_Error
     */
    public function authorise(Payment $payment)
    {
        try {
            $service = new Service\Payment($this->getClient());
            $result = $service->authorise($this->generatePaymentRequest($payment));

            return $this->handlePaymentResult($payment, $result);
        } catch (PaymentException $e) {
            $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'failed'));
            $payment->setLastErrors([$e->getMessage()]);

            $this->getEntityManager()->flush();

            return false;
        }
    }

    /**
     * @param Payment $payment
     * @param Request $request
     *
     * @return bool
     * @throws AdyenException
     * @throws \Twig_Error
     */
    public function authorise3D(Payment $payment, Request $request)
    {
        try {
            $service = new Service\Payment($this->getClient());

            $result = $service->authorise3D([
                'md' => $request->get('MD'),
                'merchantAccount' => $this->getMerchantAccount(),
                'paResponse' => $request->get('PaRes'),
                'shopperIP' => $request->getClientIp(),
                'browserInfo' => $this->generateBrowserInfo(),
            ]);

            return $this->handlePaymentResult($payment, $result);
        } catch (PaymentException $e) {
            $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'failed'));
            $payment->setLastErrors([$e->getMessage()]);

            $this->getEntityManager()->flush();

            return false;
        }
    }

    /**
     * @link https://docs.adyen.com/developers/api-reference/payments-api/paymentresult
     *
     * @param Payment $payment
     * @param array   $result
     *
     * @return Response|bool
     * @throws PaymentException
     * @throws \Twig_Error
     * @throws \Exception
     */
    public function handlePaymentResult(Payment $payment, array $result)
    {
        $payment->setMetadata($result);

        if (array_key_exists('pspReference', $result)) {
            $payment->setReference($result['pspReference']);
        }

        if (array_key_exists('fraudResult', $result)) {
            $payment->setFraudScore($result['fraudResult']['accountScore']);
        }

        switch ($result['resultCode']) {
            case 'Authorised':
                $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'authorized'));
                $payment->setLastErrors(null);
                break;
            case 'Refused':
                $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'refused'));
                $payment->setLastErrors(array_key_exists('refusalReason',
                    $result) ? [$result['refusalReason']] : null);
                break;
            case 'Error':
                $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'failed'));
                $payment->setLastErrors(array_key_exists('refusalReason',
                    $result) ? [$result['refusalReason']] : null);
                break;
            case 'Cancelled':
                $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'cancelled'));
                $payment->setLastErrors(array_key_exists('refusalReason',
                    $result) ? [$result['refusalReason']] : null);
                break;
            case 'Received':
                /**
                 * @link https://docs.adyen.com/developers/api-manual#sepadirectdebit
                 */
                throw new PaymentException('Not implemented yet');
                break;
            case 'RedirectShopper':
                /**
                 * RedirectShopper correctly redirects the shopper to the card issuer site to carry out 3D Secure verification.
                 *
                 * @link https://docs.adyen.com/developers/api-manual#security3davs
                 * @link https://docs.adyen.com/developers/risk-management/3d-secure#redirecttothecardissuer
                 */

                return new Response($this->getContainer()->get('templating')->render('@App/Payment/3d-secure-payment.html.twig',
                    [
                        'issuerUrl' => $result['issuerUrl'],
                        'paRequest' => $result['paRequest'],
                        'md' => $result['md'],
                        'returnUrl' => $this->getContainer()->get('router')->generate('app_payment_authorise3d', [
                            'uuid' => $payment->getUuid(),
                        ], UrlGeneratorInterface::ABSOLUTE_URL),
                    ]));
            default:
                throw new PaymentException('Not implemented yet');
        }

        $this->container->get('doctrine')->getManager()->flush();

        return $payment->isPaid();
    }

    /**
     * @param $data
     */
    public function saveNotifications($data)
    {
        $this->getDoctrine()->getConnection()->beginTransaction();

        $notificationService = $this->container->get('adyen_notification');

        if ($data && array_key_exists('notificationItems', $data)) {
            foreach ($data->notificationItems as $notificationItem) {
                $notificationService->save($notificationItem->NotificationRequestItem);
            }
        }

        $this->getDoctrine()->getConnection()->commit();
    }

    /**
     * @return string
     */
    public function getMerchantAccount()
    {
        return $this->getSite()->getAdyenMerchantAccount();
    }

    /**
     * @return string
     */
    public function getSkinCode()
    {
        return $this->getSite()->getAdyenSkinCode();
    }

    /**
     * @return string
     */
    public function getHmacKey()
    {
        return $this->getSite()->getAdyenHmacKey();
    }

    /**
     * @return string
     * @throws PaymentException
     */
    public function getCseJs()
    {
        switch ($this->getContainer()->get('kernel')->getEnvironment()) {
            case 'dev':
            case 'test':
            case 'staging':
                return self::ENDPOINT_TEST . '/hpp/cse/js/' . $this->getSite()->getAdyenCseToken() . '.shtml';
            case 'prod':
                return self::ENDPOINT_LIVE . '/hpp/cse/js/' . $this->getSite()->getAdyenCseToken() . '.shtml';
            default:
                throw new PaymentException('Invalid environment "' . $this->getContainer()->get('kernel')->getEnvironment() . '"');
        }
    }

    /**
     * @return string
     * @throws PaymentException
     */
    private function getEndpoint()
    {
        switch ($this->getContainer()->get('kernel')->getEnvironment()) {
            case 'dev':
            case 'test':
            case 'staging':
                return self::ENDPOINT_TEST;
            case 'prod':
                return self::ENDPOINT_LIVE;
            default:
                throw new PaymentException('Invalid environment "' . $this->getContainer()->get('kernel')->getEnvironment() . '"');
        }
    }

    /**
     * @return Client
     * @throws PaymentException
     * @throws AdyenException
     */
    public function getClient()
    {
        if (!$this->client) {
            $this->client = new Client();
            $this->client->setApplicationName('Topbloemen');
            $this->client->setUsername($this->getSite()->getAdyenUsername());
            $this->client->setPassword($this->getSite()->getAdyenPassword());

            switch ($this->getContainer()->get('kernel')->getEnvironment()) {
                case 'dev':
                case 'test':
                case 'staging':
                    $this->client->setEnvironment('test');
                    break;
                case 'prod':
                    $this->client->setEnvironment('live');
                    break;
                default:
                    throw new PaymentException('Invalid environment "' . $this->getContainer()->get('kernel')->getEnvironment() . '"');
            }
        }

        return $this->client;
    }

    /**
     * get ideal issuers
     *
     * @return array
     */
    public function getIdealIssuers()
    {
        $redis = $this->container->get('snc_redis.default');

        if ($redis->exists('payment.gateway.adyen.issuers')) {
            $issuers = unserialize($redis->get('payment.gateway.adyen.issuers'), []);
        } else {
            try {
                $service = new Service\DirectoryLookup($this->getClient());

                $params = [
                    'paymentAmount' => '1000',
                    'currencyCode' => 'EUR',
                    'merchantReference' => 'get iDEAL issuers',
                    'skinCode' => $this->getSkinCode(),
                    'merchantAccount' => $this->getMerchantAccount(),
                    'sessionValidity' => $this->calculateSessionValidity(),
                    'countryCode' => 'NL',
                    'shopperLocale' => 'nl_NL',
                ];

                // calculate signature
                $params['merchantSig'] = Util\Util::calculateSha256Signature($this->getHmacKey(), $params);

                // convert the result into an array
                $result = new ArrayCollection($service->directoryLookup($params)['paymentMethods']);

                $paymentMethod = $result->filter(function ($paymentMethod) {
                    return ($paymentMethod['brandCode'] === 'ideal');
                })->current();

                if ($paymentMethod) {
                    $issuers = [];

                    foreach ($paymentMethod['issuers'] as $issuer) {
                        $issuers[$issuer['issuerId']] = $issuer['name'];
                    }
                }

                natsort($issuers);

                $redis->set('payment.gateway.adyen.issuers', serialize($issuers), 86400);
            } catch (\Exception $e) {
                return [];
            }
        }

        return $issuers;
    }

    /**
     * @return string
     */
    private function calculateSessionValidity()
    {
        return date(DATE_ATOM, mktime(date('H') + 1, date('i'), date('s'), date('m'), date('j'), date('Y')));
    }

    /**
     * @param Payment $payment
     * @param Request $request
     *
     * @throws PaymentException
     * @throws AdyenException
     */
    public function processResponse(Payment $payment, Request $request)
    {
        $this->validateResponse($request);

        if ($request->get('merchantReturnData') !== (string) $payment->getUuid()) {
            throw new PaymentException('Wrong payment entity!');
        }

        switch ($request->get('authResult')) {
            case 'AUTHORISED':
                $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('authorized'));
                break;
            case 'REFUSED':
                $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('refused'));
                $payment->setLastErrors(['payment.ideal.refused']);
                break;
            case 'CANCELLED':
                $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('cancelled'));
                break;
            case 'ERROR':
                $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('failed'));
                break;
            case 'PENDING':
                ## Do nothing
                break;
        }

        $this->getContainer()->get('doctrine')->getManager()->flush();
    }

    /**
     * @param Request $request
     * @throws AdyenException
     * @throws PaymentException
     */
    public function validateResponse(Request $request): void
    {
        $params = $this->getRequest()->query->all();

        unset($params['merchantSig']);

        $params = array_filter($params, function ($value) {
            return null !== $value;
        });

        $merchantSig = Util\Util::calculateSha256Signature($this->getHmacKey(), $params);

        if ($merchantSig !== $request->get('merchantSig')) {
            throw new PaymentException(vsprintf("Signature mismatch '%s' (local) vs '%s' (adyen)", [
                $merchantSig,
                $request->get('merchantSig'),
            ]));
        }
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->container->get('doctrine');
    }

    /**
     * @return Site
     */
    private function getSite()
    {
        return $this->container->get(SiteService::class)->determineSite();
    }

    /**
     * @param Payment $payment
     *
     * @return array
     */
    private function generatePaymentRequest(Payment $payment)
    {
        $orderCollection = $payment->getOrderCollection();
        $order = $orderCollection->getOrders()[0];
        $customer = $orderCollection->getCustomer();

        return [
            'amount' => [
                'value' => AmountConverter::convert($payment->getAmount()),
                'currency' => 'EUR',
            ],
            'browserInfo' => $this->generateBrowserInfo(),
            'reference' => $orderCollection->getPaymentDescription(),
            'merchantAccount' => $this->getMerchantAccount(),
            'additionalData' => $this->generateAdditionalData(),
            'billingAddress' => $this->generateBillingAddress($payment),
            'deliveryAddress' => $this->generateDeliveryAddress($payment),
            'sessionId' => $this->container->get('session') ? $this->container->get('session')->getId() : null,
            'dateOfBirth' => $customer->getBirthday() ? $customer->getBirthday()->format('Y-m-d') : null,
            'deliveryDate' => $order->getDeliveryDate() ? $order->getDeliveryDate()->format('c') : null,
            'merchantOrderReference' => $orderCollection->getNumber(),
            'shopperEmail' => $customer->getEmail(),
            'shopperIp' => $this->getRequest()->getClientIp(),
            'shopperName' => $this->generateShopperName($payment),
            'telephoneNumber' => $orderCollection->getInvoiceAddressFormattedPhoneNumber(PhoneNumberFormat::E164),
            'shopperInteraction' => 'Ecommerce',
            'shopperLocale' => 'nl_NL',
        ];
    }

    /**
     * @return array
     *
     * @link https://docs.adyen.com/developers/api-reference/payments-api#paymentrequestadditionaldata
     */
    private function generateAdditionalData()
    {
        return [
            'card.encrypted.json' => $this->getRequest()->get('adyen-encrypted-data'),
        ];
    }

    /**
     * @return array
     *
     * @link https://docs.adyen.com/developers/api-reference/common-api#browserinfo
     */
    private function generateBrowserInfo()
    {
        return [
            'acceptHeader' => $this->getRequest()->headers->get('Accept'),
            'userAgent' => $this->getRequest()->headers->get('User-Agent'),
        ];
    }

    /**
     * @param Payment $payment
     *
     * @return array
     *
     * @link https://docs.adyen.com/developers/api-reference/common-api#address
     */
    private function generateBillingAddress(Payment $payment)
    {
        $orderCollection = $payment->getOrderCollection();

        return [
            'street' => $orderCollection->getInvoiceAddressStreet(),
            'houseNumberOrName' => trim($orderCollection->getInvoiceAddressNumber() .
                '-' . $orderCollection->getInvoiceAddressNumberAddition(), '-'),
            'postalCode' => $orderCollection->getInvoiceAddressPostcode(),
            'city' => $orderCollection->getInvoiceAddressCity(),
            'country' => $orderCollection->getInvoiceAddressCountry()->getCode(),
        ];
    }

    /**
     * @param Payment $payment
     *
     * @return array
     *
     * @link https://docs.adyen.com/developers/api-reference/common-api#address
     */
    private function generateDeliveryAddress(Payment $payment)
    {
        // Adyen doesn't support multiple delivery addresses
        $order = $payment->getOrderCollection()->getOrders()[0];

        return [
            'street' => $order->getDeliveryAddressStreet(),
            'houseNumberOrName' => trim($order->getDeliveryAddressNumber() . '-' .
                $order->getDeliveryAddressNumberAddition(), '-'),
            'postalCode' => $order->getDeliveryAddressPostcode(),
            'city' => $order->getDeliveryAddressCity(),
            'country' => $order->getDeliveryAddressCountry()->getCode(),
        ];
    }

    /**
     * @param Payment $payment
     *
     * @return array
     *
     * @link https://docs.adyen.com/developers/api-reference/common-api#name
     */
    private function generateShopperName(Payment $payment)
    {
        $orderCollection = $payment->getOrderCollection();

        if (!$orderCollection->getCustomer()) {
            return null;
        }

        switch ($orderCollection->getCustomer()->getGender()) {
            case GenderType::MALE:
                $gender = 'MALE';
                break;
            case GenderType::FEMALE:
                $gender = 'FEMALE';
                break;
            default:
                $gender = 'UNKNOWN';
                break;
        }

        return [
            'gender' => $gender,
            'firstName' => $orderCollection->getCustomer()->getFirstname(),
            'infix' => substr($orderCollection->getCustomer()->getLastnamePrefix(), 0, 20),
            'lastName' => $orderCollection->getCustomer()->getLastname(),
        ];
    }
}
