<?php

namespace AppBundle\Gateway;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractGateway
{
    use ContainerAwareTrait;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param Payment $payment
     * @return Paymentmethod
     */
    protected function getPaymentMethod(Payment $payment)
    {
        return $this->container->get("app.payment_service")->getMethod($payment->getOrderCollection(),
            $payment->getPaymentmethod()->getCode());
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->container->get("request_stack")->getCurrentRequest();
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->container->get("doctrine")->getManager();
    }
}