<?php

namespace AppBundle\Form\Company;

use AdminBundle\Form\Address\CompanyEstablishmentAddressType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyEstablishmentType
 * @package AppBundle\Form\Company
 */
class CompanyEstablishmentType extends AbstractType
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_establishment_container', ContainerType::class, []);

        $column = $builder->create('container_establishment_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Vestiging',
            'icon' => 'key',
        ]);

        $column
            ->add('establishmentNumber', TextType::class, [
                'label' => 'KVK Vestiging nummer',
                'attr' => [
                    'class' => 'establishmentNumberInput',
                    'data-establishment-url' => $this->container->get('router')->generate("topgeschenken.company.establishment.details"),
                ],
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'label' => 'Naam',
            ])
            ->add('company', EntityType::class, [
                'label' => 'Bovenliggend bedrijf',
                'class' => Company::class,
                'placeholder' => '',
                'choice_label' => function ($company) {
                    return $company->getName() . " (" . $company->getId() . ")";
                },
                'select2' => [
                    'ajax' => true,
                    'findBy' => [
                        'name',
                    ],
                ],
                'required' => true,
            ]);

        $container->add($column);

        $column = $builder->create('container_establishment_address_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Adresgegevens',
            'icon' => 'key',
        ]);

        $column->add('address', CompanyEstablishmentAddressType::class, [
            'label' => false,
        ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyEstablishment::class,
        ]);
    }
}
