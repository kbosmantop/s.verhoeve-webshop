<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class BooleanTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        return (int)$value;
    }

    public function reverseTransform($value)
    {
        return (bool)$value;
    }
}