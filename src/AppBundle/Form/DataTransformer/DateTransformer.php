<?php

namespace AppBundle\Form\DataTransformer;

use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

class DateTransformer implements DataTransformerInterface
{
    public function transform($date)
    {
        if ($date === null) {
            return null;
        }

        return $date->format("Y-m-d");
    }

    public function reverseTransform($date)
    {
        return new DateTime($date);
    }
}