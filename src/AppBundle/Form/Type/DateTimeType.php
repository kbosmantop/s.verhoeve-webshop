<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'datetimepicker',
        ]);

        $resolver->setAllowedTypes('datetimepicker', ['array']);

        $resolver->setDefaults([
            'icon' => null,
            'disabled' => false,
            'widget' => 'single_text',
            'attr' => [
                'max_length' => 17,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new DateTimeTransformer());

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
        $view->vars['widget'] = $options['widget'];
        $view->vars['widget_form_control_feedback'] = $options['widget_form_control_feedback'];

        $view->vars['date_value'] = null;
        $view->vars['time_value'] = null;

        if (isset($options['datetimepicker'])) {
            $view->vars['datetimepicker'] = $options['datetimepicker'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datetime';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * {@inherticdoc}
     */
    public function getExtendedType()
    {
        return TextType::class;
    }
}
