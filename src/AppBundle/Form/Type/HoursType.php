<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HoursType
 * @package AppBundle\Form\Type
 */
class HoursType extends AbstractType
{
    /**
     * @param int  $start
     * @param int  $end
     * @param int  $minutes
     * @param bool $appendLastHour
     * @return array
     */
    public function getHours(int $start = 8, int $end = 22, int $minutes = 30, bool $appendLastHour = true) {
        $appendMinutes = ($minutes >= 0 && $minutes < 60);
        $hours = [];

        foreach (range($start, $end) as $hour) {
            $hours[$hour . ':00'] = $hour . ':00';

            if ($appendMinutes) {
                $hours[$hour . ':30'] = $hour . ':30';
            }
        }

        if ($appendLastHour) {
            $hours[$end.':00'] = $end.':00';
        }

        return $hours;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefined([
            'minutes',
        ]);

        $resolver->setDefaults([
            'choices' => [],
            'reverse' => false,
            'startHour' => 8,
            'endHour' => 22,
            'minutes' => 30,
            'appendLastHour' => true,
        ]);

        $resolver->addAllowedTypes('reverse', ['bool', 'null']);
        $resolver->addAllowedTypes('startHour', ['int', 'null']);
        $resolver->addAllowedTypes('endHour', ['int', 'null']);
        $resolver->addAllowedTypes('appendLastHour', ['bool', 'null']);

        $resolver->setNormalizer('choices', function (Options $options) {
            $choices = $this->getHours($options['startHour'], $options['endHour'], $options['minutes'], $options['appendLastHour']);

            if ($options['reverse']) {
                $choices = array_reverse($choices);
            }

            return $choices;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
