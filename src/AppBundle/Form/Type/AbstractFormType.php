<?php

namespace AppBundle\Form\Type;

use AppBundle\Annotation\Type\YesNoType;
use AppBundle\Traits\PublishableEntity;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractFormType
 * @package AppBundle\Form\Type
 */
abstract class AbstractFormType extends AbstractType
{
    protected static $translatableData = [];
    protected $translatableProperties = [];

    /**
     * Add publishable fields to given builder
     *
     * @param FormBuilderInterface|null $container
     * @param FormBuilderInterface      $builder
     * @param array                     $options
     *
     * @return FormBuilderInterface $builder
     */
    public function addPublishableFields(
        FormBuilderInterface $container = null,
        FormBuilderInterface $builder,
        array $options
    ) {
        void($options);

        if ($builder->getDataClass() && \in_array(PublishableEntity::class, class_uses($builder->getDataClass()),
                true)) {
            $rootContainer = null;

            if (!$container) {
                $rootContainer = $builder->create('container_publishable', ContainerType::class);
                $container = $builder->create('container_publishable', ColumnType::class, [
                    'column_layout' => ColumnType::COLUMN_WIDTH_12,
                    'label' => 'Publicatie',
                    'icon' => 'traffic-lights',
                ]);
            }

            $yesNoChoices = [];

            foreach (YesNoType::$choices as $value => $key) {
                $yesNoChoices[$key] = $value;
            }

            $container->add('publish', BooleanType::class, [
                'label' => 'Publiceren',
                'choices' => $yesNoChoices,
                'expanded' => true,
                'multiple' => false,
                'translation_domain' => false,
            ]);

            $container->add('publishStart', DateTimeType::class, [
                'label' => 'Publiceren vanaf',
                'required' => false,
                'translation_domain' => false,
            ]);

            $container->add('publishEnd', DateTimeType::class, [
                'label' => 'Publiceren t/m',
                'required' => false,
                'translation_domain' => false,
            ]);

            if ($rootContainer !== null) {
                $rootContainer->add($container);

                $container = $rootContainer;
            }
        }

        return $container;
    }

    /**
     * @param Translatable $entity
     * @param              $request
     */
    public function hydrateTranslations(&$entity, $request)
    {
        $post = $request->request->all();

        self::getTranslationDataFromPost($post);

        if (!empty(self::$translatableData)) {

            foreach (self::$translatableData as $locale => $data) {
                $translationClass = $entity->translate($locale, false);

                foreach ($data as $property => $value) {

                    $method = 'set' . ucfirst($property);

                    if (method_exists($translationClass, $method)) {
                        $translationClass->$method($value);
                    }
                }
            }

            $entity->mergeNewTranslations();
        }
    }

    /**
     * @param $post
     */
    private static function getTranslationDataFromPost($post)
    {
        foreach ($post as $key => $data) {
            if (preg_match('#translations[0-9]*#', $key)) {
                foreach ($data as $locale => $translations) {
                    if (\is_array($translations)) {
                        foreach ($translations as $label => $value) {
                            self::$translatableData[$locale][$label] = $value;
                        }
                    }
                }
            }

            if (\is_array($data)) {
                self::getTranslationDataFromPost($data);
            }
        }
    }

    /**
     * @param array $fieldsParameters
     * @return array
     */
    protected function getA2lixTranslatableFieldsOptions(array $fieldsParameters = [])
    {
        return [
            'property_path' => 'translations',
            'label' => false,
            'excluded_fields' => array_diff($this->translatableProperties, array_keys($fieldsParameters)),
            'fields' => $fieldsParameters,
        ];
    }

    /**
     * @param FormInterface $form
     * @return FormInterface
     */
    protected function getRootForm(FormInterface $form)
    {
        if ($form->getParent()) {
            return $this->getRootForm($form->getParent());
        }
        return $form;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('translation_domain', false);

        parent::configureOptions($resolver);
    }
}
