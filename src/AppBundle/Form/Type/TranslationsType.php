<?php

namespace AppBundle\Form\Type;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType as A2lixTranslationsType;
use A2lix\TranslationFormBundle\Locale\LocaleProviderInterface;
use AppBundle\EventListener\TranslationsListener;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TranslationsType
 * @package AppBundle\Form\Type
 */
class TranslationsType extends A2lixTranslationsType
{
    /** @var TranslationsListener */
    private $translationsListener;

    /** @var LocaleProviderInterface */
    private $localeProvider;

    /**
     * @param TranslationsListener    $translationsListener
     * @param LocaleProviderInterface $localeProvider
     */
    public function __construct(TranslationsListener $translationsListener, LocaleProviderInterface $localeProvider)
    {
        parent::__construct($translationsListener, $localeProvider);
        $this->translationsListener = $translationsListener;
        $this->localeProvider = $localeProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $builder->addEventSubscriber($this->translationsListener);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'by_reference' => false,
            'empty_data' => function (FormInterface $form) {
                void($form);

                return new ArrayCollection();
            },
            'locales' => $this->localeProvider->getLocales(),
            'default_locale' => $this->localeProvider->getDefaultLocale(),
            'required_locales' => $this->localeProvider->getRequiredLocales(),
            'fields' => [],
            'excluded_fields' => [],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'a2lix_translationsForms';
    }
}
