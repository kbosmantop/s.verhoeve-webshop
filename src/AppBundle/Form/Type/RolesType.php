<?php

namespace AppBundle\Form\Type;

use AdminBundle\Services\ControllerRole;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RolesType extends AbstractType
{
    /**
     * @var ControllerRole
     */
    private $controllerRole;

    /**
     * RolesType constructor.
     * @param ControllerRole $controllerRole
     */
    public function __construct(ControllerRole $controllerRole)
    {
        $this->controllerRole = $controllerRole;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $rolesList = $this->getAvailableRoles(true);

        $roleChoices = [];

        foreach ($this->getAvailableRoles(true) as $key => $value) {
            $roleChoices[$key] = $value;
        }

        $resolver->setDefaults([
            'icon' => null,
            'disabled' => false,
            'multiple' => true,
            'choices' => $roleChoices,
            'label' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
        $view->vars['availableRoles'] = $this->getAvailableRoles();
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'roles';
    }

    /**
     * @param bool $choiceListResource
     * @return array
     */
    private function getAvailableRoles($choiceListResource = false)
    {
        return $this->controllerRole->getRoles($choiceListResource);
    }
}
