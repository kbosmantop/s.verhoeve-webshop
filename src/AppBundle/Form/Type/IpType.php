<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\IpTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class IpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ipTransformer = new IPTransformer();

        $builder->addModelTransformer($ipTransformer);

        parent::buildForm($builder, $options);
    }

    public function getBlockPrefix()
    {
        return 'ip';
    }

    public function getParent()
    {
        return TextType::class;
    }
}