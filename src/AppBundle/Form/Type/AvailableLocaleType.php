<?php

namespace AppBundle\Form\Type;

use AppBundle\Services\Domain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AvailableLocaleType
 * @package AppBundle\Form\Type
 */
class AvailableLocaleType extends AbstractType
{
    /**
     * @var Domain $domain
     */
    private $domain;

    /**
     * AvailableLocaleType constructor.
     * @param Domain $domain
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $locales = $this->domain->getDomain()->getSite()->getAvailableLocales();
        $defaultLocale = $this->domain->getDomain()->getDefaultLocale();

        $choices = [];

        foreach ($locales as $locale) {
            $codes = explode('_', $locale);

            if ($codes[0] === 'en') {
                unset($codes[1]);
            }


            $name = Intl::getLanguageBundle()->getLanguageName($codes[0], $codes[1] ?? null,
                $defaultLocale);

            $choices[$locale] = $name;
        }


        if(null === $choices) {
            $choices = [];
        }

        $choices = \array_flip($choices);

        $resolver->setDefaults([
            'choices' => $choices,
        ]);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
