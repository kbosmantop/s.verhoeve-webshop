<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableType extends CollectionType
{
    public function getBlockPrefix()
    {
        return 'table';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //disabled here is important to prevent that the data in the table is deleted
        $resolver->setDefaults([
            'disabled' => true,
            'allow_add' => true,
        ]);

        parent::configureOptions($resolver);
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}
