<?php

namespace AppBundle\Form\Type;

use AppBundle\DBAL\Types\YesNoType as YesNoEnumType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class YesNoType
 * @package AppBundle\Form\Type
 */
class YesNoType extends BooleanType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->getChoices(),
            'placeholder' => 'Maak een keuze',
        ]);
    }

    /**
     * @return array
     */
    private function getChoices(): array
    {
        $choices = [];
        foreach (YesNoEnumType::getChoices() as $key => $value) {
            $choices[$key] = $value;
        }

        return array_reverse($choices);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return BooleanType::class;
    }
}