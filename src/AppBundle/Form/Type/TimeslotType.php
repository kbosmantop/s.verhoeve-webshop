<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Common\TimeslotEntityInterface;
use AppBundle\Form\DataTransformer\RruleWeeklyTransformer;
use AppBundle\Form\DataTransformer\TimeTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class TimeslotType
 * @package AppBundle\Form\Type
 */
class TimeslotType extends AbstractType
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RruleWeeklyTransformer
     */
    private $transformer;

    /**
     * RruleWeeklyType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RruleWeeklyTransformer $transformer
     */
    public function __construct(EntityManagerInterface $entityManager, RruleWeeklyTransformer $transformer)
    {
        $this->entityManager = $entityManager;
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $builder->create('timeslot_container', ContainerType::class, []);

        $generalColumn = $builder->create('timeslot_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Tijden',
            'icon' => 'alarm',
        ]);

        $generalColumn->add('from', ChoiceType::class, [
            'choices' => $this->timeslotHours('Van'),
            'data_class' => null,
        ]);
        $generalColumn->add('till', ChoiceType::class, [
            'choices' => $this->timeslotHours('Tot'),
        ]);
        $generalColumn->add('excludeCompanyClosingDates', CheckboxType::class, [
            'label' => 'Gesloten op bedrijfsvakantiedagen',
            'data' => true,
        ]);
        $generalColumn->add('excludeHolidays', CheckboxType::class, [
            'label' => 'Gesloten op feestdagen',
            'data' => true,
        ]);

        $generalColumn->get('from')->addModelTransformer(new TimeTransformer());
        $generalColumn->get('till')->addModelTransformer(new TimeTransformer());

        $rruleColumn = $builder->create('timeslot_rrule_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Dagen',
            'icon' => 'calendar',
        ]);

        $rruleColumn->add('rrule', RruleWeeklyType::class, [
            'label' => false,
        ]);
        $rruleColumn->get('rrule')->addModelTransformer($this->transformer);

        $container->add($generalColumn);
        $container->add($rruleColumn);
        $builder->add($container);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $general = $event->getData()['timeslot_container']['timeslot_general_column'];
            $rrule = $event->getData()['timeslot_container']['timeslot_rrule_column'];
            $rruleString = $this->transformer->reverseTransform($rrule['rrule']);
            $timeslotRepository = $this->entityManager->getRepository(Timeslot::class);
            $timeslot = $timeslotRepository->findOneOrCreate([
                'from' => new \DateTime($general['from']),
                'till' => new \DateTime($general['till']),
                'excludeCompanyClosingDates' => $general['excludeCompanyClosingDates'] === '1',
                'excludeHolidays' => $general['excludeHolidays'] === '1',
                'rruleString' => $rruleString !== '' ? $rruleString: null,
            ]);
            $event->getForm()->setData($timeslot);
            $event->getForm()->remove('timeslot_container');
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Timeslot::class,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @param $placeHolder
     * @return array
     */
    public function timeslotHours($placeHolder): array
    {
        return [
            $placeHolder => '',
            '00:00' => '00:00',
            '01:00' => '01:00',
            '02:00' => '02:00',
            '03:00' => '03:00',
            '04:00' => '04:00',
            '05:00' => '05:00',
            '06:00' => '06:00',
            '07:00' => '07:00',
            '08:00' => '08:00',
            '09:00' => '09:00',
            '10:00' => '10:00',
            '11:00' => '11:00',
            '12:00' => '12:00',
            '13:00' => '13:00',
            '14:00' => '14:00',
            '15:00' => '15:00',
            '16:00' => '16:00',
            '17:00' => '17:00',
            '18:00' => '18:00',
            '19:00' => '19:00',
            '20:00' => '20:00',
            '21:00' => '21:00',
            '22:00' => '22:00',
            '23:00' => '23:00',
        ];
    }
}
