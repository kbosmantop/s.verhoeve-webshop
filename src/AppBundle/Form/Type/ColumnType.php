<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ColumnType extends AbstractType
{
    const COLUMN_WIDTH_1 = "col-md-1";
    const COLUMN_WIDTH_2 = "col-md-2";
    const COLUMN_WIDTH_3 = "col-md-3";
    const COLUMN_WIDTH_4 = "col-md-4";
    const COLUMN_WIDTH_5 = "col-md-5";
    const COLUMN_WIDTH_6 = "col-md-6";
    const COLUMN_WIDTH_7 = "col-md-7";
    const COLUMN_WIDTH_8 = "col-md-8";
    const COLUMN_WIDTH_9 = "col-md-9";
    const COLUMN_WIDTH_10 = "col-md-10";
    const COLUMN_WIDTH_11 = "col-md-11";
    const COLUMN_WIDTH_12 = "col-md-12";

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'column_layout' => self::COLUMN_WIDTH_12,
            'is_column' => true,
            'label' => false,
            'label_icon' => false,
            'inherit_data' => true,
            'hidden' => false,
            'id' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
        $view->vars['label'] = $options['label'];
        $view->vars['label_icon'] = $options['label_icon'];
        $view->vars['is_column'] = $options['is_column'];
        $view->vars['column_layout'] = $options['column_layout'];
        $view->vars['hidden'] = $options['hidden'];
        $view->vars['id'] = $options['id'];

        $view->parent->vars['has_columns'] = true;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'column';
    }
}
