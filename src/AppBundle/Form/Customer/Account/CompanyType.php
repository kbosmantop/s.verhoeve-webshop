<?php

namespace AppBundle\Form\Customer\Account;

use AppBundle\Entity\Relation\Company;
use AppBundle\Validator\Constraints\VatNumber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyType
 * @package AppBundle\Form\Customer
 *
 */
class CompanyType extends AbstractType
{
    use ContainerAwareTrait;

    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextType::class, [
                'label' => 'label.company_name',
                'required' => true,
            ])
            ->add('chamberOfCommerceNumber', TextType::class, [
                'label' => 'customer.company.chamber_of_commerce_number',
                'attr' => [
                    'readonly' => (($builder->getData()->getMainAddress() && $builder->getData()->getMainAddress()->getCountry()->getId() == 'BE') ? false : true),
                ],
                'required' => (($builder->getData()->getMainAddress() && $builder->getData()->getMainAddress()->getCountry()->getId() == 'BE') ? false : true),
            ]);

        $invalidVatNumber = new VatNumber($this->container->get('ec_europa_eu.vies'), [
            'groups' => ['company_profile'],
        ]);
        $invalidVatNumber->message = 'company.vat_number.not_valid';

        $builder
            ->add('vatNumber', TextType::class, [
                'label' => 'customer.company.vat_number',
                'constraints' => [
                    $invalidVatNumber,
                ],
                'attr' => [
                    'readonly' => (($builder->getData()->getMainAddress() && $builder->getData()->getMainAddress()->getCountry()->getId() == 'BE') ? true : false),
                ],
                'required' => (($builder->getData()->getMainAddress() && $builder->getData()->getMainAddress()->getCountry()->getId() == 'BE') ? true : false),
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['request']);

        $validationGroups = ['company_profile'];

        $customer = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($customer->getCompany()->getMainAddress()) {
            switch ($customer->getCompany()->getMainAddress()->getCountry()->getId()) {
                case 'NL':
                    $validationGroups[] = 'company_profile_nl';
                    break;
                case 'BE':
                    $validationGroups[] = 'company_profile_be';
                    break;
            }
        }

        $resolver->setDefaults([
            'data_class' => Company::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'validation_groups' => $validationGroups,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_customer_company';
    }
}
