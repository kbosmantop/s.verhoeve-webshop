<?php

namespace AppBundle\Form\Customer\Account;

use AppBundle\Entity\Relation\Address;
use AppBundle\Form\DataTransformer\CountryCodeToCountryTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddressType extends AbstractType
{
    private $em;

    /**
     * @param $em EntityManagerInterface
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // todo: (($builder->getData()->getPostcode() && $builder->getData()->getCity()) ? $builder->getData()->getPostcode() . ' ' . $builder->getData()->getCity() in placeholder
        $builder
            ->add('postcode_and_city', ChoiceType::class, [
                'label' => 'label.postcode_and_city',
                'translation_domain' => 'messages',
                'mapped' => false,
                'required' => false,
                'placeholder' => 'label.postcode_and_city.placeholder',
            ])
            ->add('companyName', TextType::class, [
                'label' => 'label.company_name',
                'required' => false,
            ]);

        $builder
            ->add('country_choice', ChoiceType::class, [
                'label' => 'label.country',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    Intl::getRegionBundle()->getCountryName('NL') => 'NL',
                    Intl::getRegionBundle()->getCountryName('BE') => 'BE',
                    'customer.address.country.other' => 'other',
                ],
                'choice_translation_domain' => false,
                'data' => ((!$builder->getData()->getCountry()) ? 'NL' : ((in_array($builder->getData()->getCountry()->getId(),
                    ['NL', 'BE'])) ? $builder->getData()->getCountry()->getId() : 'other')),
                'mapped' => false,
                'attr' => [
                    'class' => 'country-choice',
                ],
            ])
            ->add('country_list', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => array_flip(array_filter(Intl::getRegionBundle()->getCountryNames(), function ($code) {
                    return !in_array(strtolower($code), ['nl', 'be']);
                }, ARRAY_FILTER_USE_KEY)),
                'choice_translation_domain' => false,
                'placeholder' => 'label.country.choose',
                'translation_domain' => 'messages',
                'mapped' => false,
                'attr' => [
                    'class' => 'country-list-other',
                ],
            ])
            ->add('country', CountryType::class, [
                'label' => false,
                'required' => true,
                'choices' => array_flip(Intl::getRegionBundle()->getCountryNames()),
                //'choices_as_values' => false,
                'choice_translation_domain' => false,
                'attr' => [
                    'class' => 'country-list',
                ],
            ])
            ->add('postcode', TextType::class, [
                'label' => 'label.postcode',
                'required' => true,
            ])
            ->add('street', TextType::class, [
                'label' => 'label.street',
                'required' => true,
            ])
            ->add('number', TextType::class, [
                'label' => 'label.number',
            ])
            ->add('numberAddition', TextType::class, [
                'label' => 'label.numberAddition',
                'required' => false
            ])
            ->add('city', TextType::class, [
                'label' => 'label.city',
            ]);

        $request = isset($options['request']) ? $options['request'] : null;
        $isMainAddress = isset($options['is_main_address']) ? $options['is_main_address'] : null;
        $isInvoiceAddress = isset($options['is_invoice_address']) ? $options['is_invoice_address'] : null;
        $isCompany = isset($options['is_company']) ? $options['is_company'] : null;

        if (isset($request) && $request->getSession()->get('incomplete_facebook_registration')) {
            $isMainAddress = true;
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($request, $isMainAddress, $isInvoiceAddress, $isCompany) {
                $form = $event->getForm();

                $attn_options = [
                    'label' => 'label.attn',
                    'required' => false,
                ];

                if (!$isInvoiceAddress) {
                    $attn_options['required'] = true;
                    $attn_options['constraints'] = [
                        new NotBlank([
                            'message' => 'address.attn.not_blank',
                        ]),
                    ];
                }

                if (!$isMainAddress) {
                    $form->add('attn', TextType::class, $attn_options);
                }

                if ($isCompany && $isInvoiceAddress) {
                    $form->add('email', EmailType::class, [
                        'label' => 'label.email',
                        'required' => true,
                        'constraints' => [
                            new NotBlank([
                                'message' => 'address.invoice_email.not_blank',
                            ]),
                            new Email([
                                'message' => 'address.invoice_email.not_valid',
                            ]),
                        ],
                    ]);
                }
            });

        $builder->get('country')
            ->addModelTransformer(new CountryCodeToCountryTransformer($this->em));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['request', 'is_main_address', 'is_invoice_address', 'is_company']);

        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_customer_address';
    }
}
