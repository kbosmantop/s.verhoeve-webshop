<?php

namespace AppBundle\Form\Customer\Account;

use AppBundle\DBAL\Types\SalutationType;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AvailableLocaleType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserType
 * @package AppBundle\Form\Customer\Account
 */
class UserType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $genderChoices = [];

        foreach (SalutationType::getChoices() as $key => $value) {
            $genderChoices[$key] = $value;
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'label' => 'label.salutation',
                'required' => true,
                'choices' => $genderChoices,
                'expanded' => true,
                'multiple' => false,
                'invalid_message' => 'customer.account.invalid_message.gender',
            ])
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'required' => true,
                'invalid_message' => 'customer.account.invalid_message.firstname',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'required' => true,
                'invalid_message' => 'customer.account.invalid_message.lastname',
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'label.phonenumber',
                'required' => true,
                'invalid_message' => 'customer.phonenumber.invalid_message',
            ]);

        if ($options['intention'] === 'create') {
            $builder
                ->add('username', EmailType::class, [
                    'label' => 'label.email',
                    'required' => true,
                    'invalid_message' => 'customer.account.invalid_message.email',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'customer.email.not_blank',
                        ]),
                    ],
                ]);
        }

        $builder
            ->add('locale', AvailableLocaleType::class, [
                'label' => 'label.preferred_language',
                'required' => true,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['request']);

        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'validation_groups' => [
                'Default',
                'user_registration',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'site_customer_customer';
    }
}
