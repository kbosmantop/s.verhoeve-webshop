<?php

namespace AppBundle\Form\Customer\Flow;

use AppBundle\Form\Customer\CompanyType;
use Craue\FormFlowBundle\Form\FormFlow;

/**
 * Class CompanyFlow
 * @package AppBundle\Form\Customer\Flow
 */
class CompanyFlow extends FormFlow
{
    protected $maxSteps = 3;
    protected $revalidatePreviousSteps = false;
    protected $allowDynamicStepNavigation = true;

    private $em;
    private $registrationFromCheckout = false;
//
//    /**
//     * @param EntityManagerInterface $em
//     * @param RequestStack $requestStack
//     */
//    public function __construct(EntityManagerInterface $em, RequestStack $requestStack)
//    {
//        $this->em = $em;
//
//        $request = $requestStack->getCurrentRequest();
//
//        $this->setRequestStack($requestStack);
//
//        $this->registrationFromCheckout = ($request->query->has('country') && $request->query->has('company_identification'));
//    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return 'customer_company_create';
    }

    /**
     * @return array
     */
    protected function loadStepsConfig()
    {
        return [
            [
                'label' => 'label.country',
                'form_type' => CompanyType::class,
            ],
            [
                'label' => 'company.step.organization_identifier',
                'form_type' => CompanyType::class,
            ],
            [
                'label' => 'label.customer.data',
                'form_type' => CompanyType::class,
                'form_options' => [
                    'validation_groups' => ['company_registration'],
                ],
            ],
            [
                'label' => 'label.confirm',
            ],
        ];
    }
}
