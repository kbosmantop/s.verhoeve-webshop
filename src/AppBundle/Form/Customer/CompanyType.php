<?php

namespace AppBundle\Form\Customer;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\DataTransformer\CountryCodeToCountryTransformer;
use AppBundle\Services\Company\CompanyRegistrationService;
use AppBundle\Validator\Constraints\ChamberOfCommerce;
use AppBundle\Validator\Constraints\CustomerEmail;
use AppBundle\Validator\Constraints\VatNumber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Translation\Common\Model\Message;


/**
 * Class CompanyType
 * @package AppBundle\Form\Customer
 */
class CompanyType extends AbstractType
{
    use ContainerAwareTrait;

    private $em;

    public static function getTranslationMessages()
    {
        $messages = [];

        array_push($messages, new Message("company.vat_number.not_valid", "validators"));
        array_push($messages, new Message("company.chamber_of_commerce.not_valid", "validators"));

        return $messages;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     * @throws \SoapFault
     * @throws \libphonenumber\NumberParseException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $country = strtolower($request->get($this->getBlockPrefix())['country_choice']);

        if ($this->container->get('session')->get('registration_from_checkout')) {
            $country = $this->container->get('session')->get('registration_from_checkout_country');
        }

        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('country_choice', ChoiceType::class, [
                        'label' => 'customer.company.country',
                        'required' => true,
                        'expanded' => true,
                        'multiple' => false,
                        'choices' => [
                            Intl::getRegionBundle()->getCountryName('NL') => 'nl',
                            Intl::getRegionBundle()->getCountryName('BE') => 'be',
                        ],
                        'choice_translation_domain' => false,
                        'data' => 'nl',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'country-choice',
                        ],
                    ]);

                break;
            case 2:
                switch ($country) {
                    case 'nl':

                        $invalidChamberOfCommerce = new ChamberOfCommerce($this->container->get('webservices_nl'));
                        $invalidChamberOfCommerce->message = 'company.chamber_of_commerce.not_valid';

                        $builder
                            ->add('chamberOfCommerceNumber', TextType::class, [
                                'label' => 'customer.company.chamber_of_commerce_number',
                                'required' => true,
                                'constraints' => [
                                    new NotBlank([
                                        'message' => 'company.chamber_of_commerce.not_blank',
                                    ]),
                                    $invalidChamberOfCommerce,
                                ],
                                'invalid_message' => 'company.chamber_of_commerce.not_blank',
                                'client_validation' => [
                                    'validation' => 'required',
                                ],
                                'data' => (($this->container->get('session')->get('registration_from_checkout_company_identification')) ? $this->container->get('session')->get('registration_from_checkout_company_identification') : null),
                            ]);

                        break;
                    case 'be':
                        $invalidVatNumber = new VatNumber($this->container->get('ec_europa_eu.vies'));
                        $invalidVatNumber->message = 'company.vat_number.not_valid';

                        $builder
                            ->add('vatNumber', TextType::class, [
                                'label' => 'customer.company.vat_number',
                                'required' => true,
                                'constraints' => [
                                    new NotBlank([
                                        'message' => 'company.vat_number.not_blank',
                                    ]),
                                    $invalidVatNumber,
                                ],
                                'invalid_message' => 'company.vat_number.not_blank',
                                'client_validation' => [
                                    'validation' => 'required',
                                ],
                            ]);

                        break;
                }

                break;
            case 3:
                /** @var Company $company */
                $company = $options['data'];
                $companyIdentificationNumber = $this->container->get('session')->get('registration_from_checkout_company_identification');

                if ($country == 'nl') {
                    $companyRegistrationData = $this->container->get('webservices_nl')->getCompanyDataByChamberOfCommerceNumber($company->getChamberOfCommerceNumber() ?? $companyIdentificationNumber);

                    $builder
                        ->add('name', TextType::class, [
                            'label' => 'label.company_name',
                            'required' => true,
                            'disabled' => true,
                            'data' => $companyRegistrationData->name,
                            'invalid_message' => 'company.name.not_blank',
                            'client_validation' => [
                                'validation' => 'required',
                            ],
                        ]);
                } else {
                    if ($country == "be") {
                        $companyRegistrationData = $this->container->get('ec_europa_eu.vies')->getCompanyDataByVatNumber($company->getVatNumber() ?? $companyIdentificationNumber);

                        $builder
                            ->add('name', TextType::class, [
                                'label' => 'label.company',
                                'required' => true,
                                'disabled' => true,
                                'data' => $companyRegistrationData->name,
                                'invalid_message' => 'company.name.not_blank',
                                'client_validation' => [
                                    'validation' => 'required',
                                ],
                            ]);
                    }
                }


                $request = $this->container->get('request_stack')->getCurrentRequest();

                if ($country == "be") {
                    $builder
                        ->add('postcode_and_city', ChoiceType::class, [
                            'label' => 'label.postcode_and_city',
                            'translation_domain' => 'messages',
                            'choice_translation_domain' => false,
                            'mapped' => false,
                            'required' => true,
                            'placeholder' => 'label.postcode_and_city.placeholder',
//                            'constraints' => array(
//                                new NotBlank(array(
//                                    'message' => 'address.postcode_and_city.not_blank',
//                                    'groups' => array('company_registration')
//                                ))
//                            ),
                            'choices' => ((!empty($request->get($this->getBlockPrefix())['postcode_and_city'])) ? [$request->get($this->getBlockPrefix())['postcode_and_city'] => $request->get($this->getBlockPrefix())['postcode_and_city']] : null),
                            'invalid_message' => 'address.postcode_and_city.not_valid',
                            'client_validation' => [
                                'validation' => 'required',
                            ],
                        ]);
                }

                $builder
                    ->add('addresses', CollectionType::class, [
                        'entry_type' => AddressType::class,
                        'entry_options' => [
                            'required' => true,
                        ],
                        'validation_groups' => ['company_registration'],
                    ])
                    ->add('customers', CollectionType::class, [
                        'entry_type' => CompanyCustomerType::class,
                        'entry_options' => [
                            'required' => true,
                        ],
                    ]);

                $builder->addEventListener(FormEvents::PRE_SET_DATA,
                    function (FormEvent $event) use ($options, $request, $country, $companyRegistrationData) {

                        $address = new Address();
                        $address->setMainAddress(true);

                        $invoiceAddress = new Address();
                        $invoiceAddress->setInvoiceAddress(true);

                        $countryCodeDataTransformer = new CountryCodeToCountryTransformer($this->em);

                        if ($country == 'nl') {
                            $address->setPostcode($companyRegistrationData->address->postcode);
                            $address->setCity($companyRegistrationData->address->city);
                            $address->setStreet($companyRegistrationData->address->street);
                            $address->setNumber($companyRegistrationData->address->houseNumber);
                            $address->setNumberAddition($companyRegistrationData->address->houseNumberAddition);
                            $address->setCountry($countryCodeDataTransformer->reverseTransform($companyRegistrationData->address->countryCode));

                            $invoiceAddress->setPostcode($companyRegistrationData->postaddress->postcode);
                            $invoiceAddress->setCity($companyRegistrationData->postaddress->city);
                            $invoiceAddress->setStreet($companyRegistrationData->postaddress->street);
                            $invoiceAddress->setNumber($companyRegistrationData->postaddress->houseNumber);
                            $invoiceAddress->setNumberAddition($companyRegistrationData->postaddress->houseNumberAddition);
                            $invoiceAddress->setCountry($countryCodeDataTransformer->reverseTransform($companyRegistrationData->postaddress->countryCode));
                        } else {
                            if ($country == 'be') {
                                $address->setPostcode($companyRegistrationData->address->postcode);
                                $address->setCity($companyRegistrationData->address->city);
                                $address->setStreet($companyRegistrationData->address->street);
                                $address->setNumber($companyRegistrationData->address->houseNumber);
                                $address->setNumberAddition($companyRegistrationData->address->houseNumberAddition);
                                $address->setCountry($countryCodeDataTransformer->reverseTransform($companyRegistrationData->address->countryCode));

                                $invoiceAddress->setPostcode($companyRegistrationData->address->postcode);
                                $invoiceAddress->setCity($companyRegistrationData->address->city);
                                $invoiceAddress->setStreet($companyRegistrationData->address->street);
                                $invoiceAddress->setNumber($companyRegistrationData->address->houseNumber);
                                $invoiceAddress->setNumberAddition($companyRegistrationData->address->houseNumberAddition);
                                $invoiceAddress->setCountry($countryCodeDataTransformer->reverseTransform($companyRegistrationData->address->countryCode));
                            }
                        }

                        $companyRegistrationService = $this->container->get(CompanyRegistrationService::class);
                        $companyRegistration = $companyRegistrationService->createCompanyRegistration($country,
                            $companyRegistrationData);

                        $data = $event->getData();
                        $data->addAddress($address);
                        $data->addAddress($invoiceAddress);
                        $data->setCompanyRegistration($companyRegistration);
                        $data->setName($companyRegistration->getName());
                        $data->addCustomer(new Customer());

                        $event->setData($data);
                    });

                $builder->addEventListener(FormEvents::POST_SET_DATA,
                    function (FormEvent $event) use ($options, $request) {
                        $customerEmailValidator = new CustomerEmail($this->container->get('doctrine.orm.entity_manager'));
                        $customerEmailValidator->message = 'address.invoice_email.not_allowed';
                        $customerEmailValidator->groups = ['company_registration'];

                        $form = $event->getForm();

                        foreach ($form->get('addresses') as $address) {
                            $address->remove('country_list');
                            $address->remove('companyName');
                        }

                        $form->get('addresses')[1]->add('attn', TextType::class, [
                            'label' => 'label.invoice_attn',
                            'required' => false,
                        ]);

                        $form->get('addresses')[1]->add('email', EmailType::class, [
                            'label' => 'label.invoice_email',
                            'required' => true,
                            'constraints' => [
                                new NotBlank([
                                    'message' => 'address.invoice_email.not_blank',
                                    'groups' => ['company_registration'],
                                ]),
                                new Email([
                                    'message' => 'address.invoice_email.not_valid',
                                    'groups' => ['company_registration'],
                                ]),
                                $customerEmailValidator,
                            ],
                            'invalid_message' => 'address.invoice_email.not_valid',
                            'client_validation' => [
                                'validation' => 'email',
                            ],
                        ]);

                        $form->get('customers')[0]->add('phonenumber', TextType::class, [
                            'label' => 'label.phonenumber',
                            'required' => true,
                            'invalid_message' => 'customer.account.phonenumber.not_valid',
                            'constraints' => [
                                new NotBlank([
                                    'message' => 'customer.account.phonenumber.not_blank',
                                    'groups' => ['company_registration'],
                                ]),
                            ],
                            'client_validation' => [
                                'validation' => 'required',
                            ],
                        ]);
                    });

                break;
        }

        if ($options['flow_step'] != 1) {
            $builder->add('country_choice', HiddenType::class, [
                'data' => $country,
                'mapped' => false,
                'attr' => [
                    'class' => 'country-choice',
                ],
            ]);
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['request']);

        $resolver->setDefaults([
            'data_class' => Company::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
            ],
        ]);
    }

    public function getBlockPrefix()
    {
        return 'company';
    }
}
