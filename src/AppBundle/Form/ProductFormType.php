<?php

namespace AppBundle\Form;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Form\Common\EntityHiddenTransformer;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\StockManager;
use AppBundle\Services\ProductFormService;
use AppBundle\Services\SupplierManagerService;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class ProductFormType
 * @package AppBundle\Form
 */
class ProductFormType extends AbstractType
{
    /**
     * @var ProductFormService
     */
    private $productFormService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var StockManager
     */
    private $stockManager;

    /**
     * @var SupplierManagerService
     */
    private $supplierManagerService;

    /**
     * ProductFormType constructor.
     * @param ProductFormService     $productFormService
     * @param EntityManagerInterface $entityManager
     * @param ProductManager         $productManager
     * @param TranslatorInterface    $translator
     * @param StockManager           $stockManager
     * @param SupplierManagerService $supplierManagerService
     */
    public function __construct(
        ProductFormService $productFormService,
        EntityManagerInterface $entityManager,
        ProductManager $productManager,
        TranslatorInterface $translator,
        StockManager $stockManager,
        SupplierManagerService $supplierManagerService
    ) {
        $this->productFormService = $productFormService;
        $this->entityManager = $entityManager;
        $this->productManager = $productManager;
        $this->translator = $translator;
        $this->stockManager = $stockManager;
        $this->supplierManagerService = $supplierManagerService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws DBALException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Product $product */
        $product = $options['product'];

        $builder
            ->add('product', HiddenType::class)
            ->add('metadata', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('quantity', ChoiceType::class, [
                'choices' => $this->productFormService->getOrderableQuantities($options['product']),
                'choice_translation_domain' => false,
            ])
            ->add('letter_file', FileType::class, array_merge([
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => ini_get('upload_max_filesize'),
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'letter_file.invalid_message.mime_type',
                    ]),
                ],
                'attr' => [
                    'class' => 'hidden',
                    'accept' => 'application/pdf',
                    'id' => 'file-upload',
                ],
                'required' => false,
            ], []))
            ->add('submit', SubmitType::class, [
                'label' => 'label.add_to_cart',
                'translation_domain' => 'messages',
                'attr' => [
                    'disabled' => !$this->stockManager->isOrderable($product) || !$this->supplierManagerService->hasVariationWithSupplier($product),
                    'data-label-default' => $this->translator->trans('label.add_to_cart'),
                    'data-label-create-design' => $this->translator->trans('label.create_design'),
                ],
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($product) {
            if (!$this->productFormService->showLetterForm($product)) {
                $event->getForm()->remove('letter_file');
            }

            if (!$product->isConfigurable()) {
                $event->getForm()->remove('metadata');
            }
        });

        $this->addVariationFormField($product, $builder);

        /** @var Personalization $personalization */
        if (null !== $personalization = $product->getVariations()->current()->getFirstPersonalization()) {
            $builder->add('personalization_product_id', HiddenType::class, [
                'data' => $personalization->getId(),
            ]);
        }
    }

    /**
     * @param ProductInterface $product
     * @param FormBuilderInterface $form
     *
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    private function addVariationFormField(ProductInterface $product, FormBuilderInterface $form): void
    {
        if ($product->hasVariations()) {
            $form->add('variation', ChoiceType::class, [
                'attr' => [
                    'data-property-select' => null !== $product->getProductgroup() && $product->getProductgroup()->getProductPropertySelect() ? 'true' : 'false',
                    'data-properties' => json_encode($this->productFormService->getProperties($this->productManager->decorateProduct($product->getVariations()->current()))),
                    'data-label-available' => $this->translator->trans('label.available'),
                    'data-label-out-of-stock' => $this->translator->trans('label.out-of-stock'),
                ],
                'placeholder' => 'label.choice',
                'translation_domain' => 'messages',
                'choices' => $this->productFormService->getVariationChoices($product),
                'choice_translation_domain' => false,
                'data' => $this->productFormService->getDefaultVariationId($product),
                'choice_attr' => function ($pos, $key, $value) {
                    return $this->productFormService->getVariationChoicesAttributes((int)$value);
                },
            ]);
        } else {
            $variation = $product->getVariations()->first();

            $form->add('variation', HiddenType::class, []);
            $form->get('variation')->addModelTransformer(new EntityHiddenTransformer(
                $this->entityManager,
                Product::class,
                'id'
            ));
            $form->get('variation')->setData($variation);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined([
            'product',
            'selectedVariation',
        ]);
    }
}