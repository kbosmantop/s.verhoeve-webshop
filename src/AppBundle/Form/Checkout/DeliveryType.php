<?php

namespace AppBundle\Form\Checkout;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryType
 * @package AppBundle\Form\Checkout
 */
class DeliveryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
//        $resolver->setDefined(array('request'));

        $resolver->setDefaults([//            'data_class' => 'AppBundle\Entity\Relation\Address'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_checkout_delivery';
    }
}
