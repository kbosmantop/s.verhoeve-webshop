<?php

namespace AppBundle\Form\Cart;

use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Geography\DeliveryAddressTypeTranslation;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Form\DataTransformer\DateTimeTransformer;
use AppBundle\Services\DeliveryAddress;
use AppBundle\Services\SiteService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Geography\Country;


/**
 * Class CartOrderType
 * @package AppBundle\Form\Cart
 */
class CartOrderType extends AbstractType
{
    /**
     * @var DeliveryAddress $deliveryAddress
     */
    private $deliveryAddress;

    /** @var array */
    private $data;

    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * CartOrderType constructor.
     *
     * @param DeliveryAddress $deliveryAddress
     * @param SiteService     $siteService
     */
    public function __construct(DeliveryAddress $deliveryAddress, SiteService $siteService)
    {
        $this->deliveryAddress = $deliveryAddress;
        $this->siteService = $siteService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lines', CollectionType::class, [
                'by_reference' => false,
                'entry_type' => CartOrderLineType::class,
                'delete_empty' => true,
            ]);

        /**
         * Alleen purchasable lines worden toegevoegd in de POST, onderstaande code voegt de ontbrekende lijnen toe.
         */

        $this->data = null;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $this->data = $event->getForm()->get('lines')->getData();

        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();

            $data['lines'] = [];

            /** @var OrderLine $cartOrderLine */
            foreach ($event->getForm()->get('lines')->getData() as $i => $cartOrderLine) {
                if (!array_key_exists($i, $data['lines'])) {
                    $data['lines'][$i] = [
                        'quantity' => (string)$cartOrderLine->getQuantity(),
                    ];
                }
            }

            ksort($data['lines']);

            $event->setData($data);
        });

        if ($options['step'] === 2) {

            $deliveryChoices = [];

            foreach ($this->deliveryAddress->getTypes() as $key => $value) {
                $deliveryChoices[$key] = $value;
            }

            /**
             * @todo: Temp hide all other countries besides the Netherlands
             */
            $builder
                ->add('deliveryAddressCountry', EntityType::class, [
                    'class' => Country::class,
                    'label' => 'label.delivery_country',
                    'query_builder' => function (EntityRepository $er) {
                        $country = $this->siteService->determineSite()->getCountry();

                        if(null !== $country) {
                            $country = $country->getId();
                        } else {
                            $country = 'NL';
                        }

                        return $er->createQueryBuilder('c')
                            ->where('c.id = :code')
                            ->setParameter('code', $country);
                    },
                    'required' => true,
                ])
                ->add('deliveryAddressType', EntityType::class, [
                    'label' => 'label.delivery_address_type',
                    'class' => DeliveryAddressType::class,
                    'placeholder' => 'label.choice',
                    'translation_domain' => 'messages',
                    'choices' => $deliveryChoices,
                    'choice_label' => function (DeliveryAddressType $deliveryAddressType) {
                        /** @var DeliveryAddressTypeTranslation $translate */
                        $translate = $deliveryAddressType->translate();
                        return $translate->getName();
                    },
                    'choice_attr' => function (DeliveryAddressType $deliveryAddressType) {
                        /** @var DeliveryAddressTypeTranslation $translate */
                        $translate = $deliveryAddressType->translate();

                        $attr = [
                            'data-container' => 'delivery_address_type_fields_' . $deliveryAddressType->getId(),
                            'data-recipient-label' => $translate->getRecipientLabel(),
                        ];

                        if ($deliveryAddressType->getHideCompanyField()) {
                            $attr['data-hide-company'] = 'true';
                        } else {
                            $attr['data-company-label'] = $translate->getCompanyLabel();
                        }

                        return $attr;
                    },
                    'required' => true,
                ])
                ->add('deliveryAddressCompanyName', TextType::class, [
                    'label' => 'label.company_name',
                    'required' => true,
                ])
                ->add('deliveryAddressAttn', TextType::class, [
                    'label' => 'label.attn',
                    'required' => true,
                ])
                ->add('deliveryAddressPostcode', TextType::class, [
                    'label' => 'label.postcode',
                    'required' => true,
                ])
                ->add('deliveryAddressStreet', TextType::class, [
                    'label' => 'label.street',
                    'required' => true,
                ])
                ->add('deliveryAddressNumber', TextType::class, [
                    'label' => 'label.housenumber',
                    'required' => true,
                ])
                ->add('deliveryAddressNumberAddition', TextType::class, [
                    'label' => 'label.housenumber_addition',
                    'required' => false,
                ])
                ->add('deliveryAddressCity', TextType::class, [
                    'label' => 'label.city',
                    'required' => true,
                ])
                ->add('deliveryAddressPhoneNumber', TextType::class, [
                    'label' => 'label.phonenumber',
                    'required' => false,
                    'invalid_message' => 'address.phonenumber.not_valid',
                ])
                ->add('deliveryRemark', TextareaType::class, [
                    'label' => 'label.delivery_remark',
                    'required' => false,
                ])
                ->add('deliveryDate', HiddenType::class, [
                    'label' => false,
                ]);

            $builder->get('deliveryDate')->addModelTransformer(new DateTimeTransformer());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        void($form, $options);

        $view->vars['deliveryAddressTypes'] = $this->deliveryAddress->getTypes();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'app_cart_order';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CartOrder::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
                'autocomplete' => 'off',
            ],
            'validation_groups' => [
                'Default',
            ],
//            'allow_extra_fields' => true
        ]);
    }
}
