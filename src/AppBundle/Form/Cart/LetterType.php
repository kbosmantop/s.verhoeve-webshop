<?php

namespace AppBundle\Form\Cart;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class LetterType
 * @package AppBundle\Form\Cat
 */
class LetterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('file', FileType::class, [
                'mapped' => false,
                'label' => 'Brief uploaden',
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'Upload een geldig PDF bestand',
                    ]),
                ],
                'attr' => [
                    'accept' => 'application/pdf',
                    'id' => 'file-upload',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'hidden',
                ],
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'letter';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
        ]);
    }
}
