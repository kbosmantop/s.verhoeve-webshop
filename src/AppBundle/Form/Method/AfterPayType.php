<?php

namespace AppBundle\Form\Method;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AfterPayType
 * @package AppBundle\Form\Method
 */
class AfterPayType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * AfterPayType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('birthday', BirthdayType::class, [
                'label' => 'label.birthdate',
                'format' => 'd MMMM yyyy',
                'placeholder' => '',
                'years' => array_reverse(range(date('Y') - 100, date('Y') - 13)),
                'translation_domain' => 'messages'
            ])
            ->add('conditions', CheckboxType::class, [
                'label' => 'label.afterpay.conditions',
                'value' => 1,
                'invalid_message' => 'label.afterpay.conditions.invalid',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'payment_method_afterpay';
    }
}
