<?php

namespace AppBundle\Utils\Date;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class Date
 * @package AppBundle\Utils
 */
class DayUtil
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * Date constructor.
     * @param $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param int  $dayIndex
     * @param bool $short
     * @return null|string
     */
    public function getDayName(int $dayIndex, bool $short = false): ?string
    {
        $days = $this->getDayNames();

        if (isset($days[$dayIndex])) {
            return $days[$dayIndex];
        }

        return null;
    }

    /**
     * Retrieve names of the week, supports for date('N') and date('w')
     *
     * @param bool $short
     * @return array
     */
    public function getDayNames(bool $short = false): array
    {
        return [
            0 => $this->translateDayName( 'sunday', $short),
            1 => $this->translateDayName( 'monday', $short),
            2 => $this->translateDayName( 'tuesday', $short),
            3 => $this->translateDayName( 'wednesday', $short),
            4 => $this->translateDayName( 'thursday', $short),
            5 => $this->translateDayName( 'friday', $short),
            6 => $this->translateDayName( 'saturday', $short),
            7 => $this->translateDayName( 'sunday', $short)
        ];
    }

    /**
     * @param bool   $short
     * @param string $key
     * @return string
     */
    private function translateDayName(string $key, bool $short = false)
    {
        $key = sprintf('day.%s.%s', $short ? 'short' : 'long', $key);

        return $this->translator->trans($key);
    }
}