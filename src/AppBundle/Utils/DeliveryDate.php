<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Geography\Holiday;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Site\Site;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class DeliveryDate
 * @package AppBundle\Utils
 */
class DeliveryDate extends \DateTime
{
    /**
     * @var Site
     */
    private $site;

    /**
     * @var DeliveryDates
     */
    private $iterator;

    /**
     * @var Holiday
     */
    private $holiday;

    /**
     * @var null|bool
     */
    private $available;

    /**
     * @var TransportType
     */
    private $transportType;

    /**
     * @var ProductPriceManager
     */
    protected $productPriceManager;

    /**
     * DeliveryDate constructor.
     * @param string             $time
     * @param \DateTimeZone|null $timezone
     * @throws \Exception
     */
    public function __construct($time = 'now', \DateTimeZone $timezone = null)
    {
        parent::__construct($time, $timezone);

        $this->setTime(0, 0, 0);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $formatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);

        return $formatter->format($this) . ($this->getDescription() ? ' (' . $this->getDescription() . ')' : null);
    }

    /**
     * @param \IteratorAggregate $iterator
     */
    public function setIterator(\IteratorAggregate $iterator): void
    {
        $this->iterator = $iterator;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        if (!$this->holiday) {
            return '';
        }

        return $this->holiday->translate()->getName();
    }

    /**
     * @param Site $site
     */
    public function setSite(Site $site): void
    {
        $this->site = $site;
    }

    /**
     * @return float
     * @throws \Exception
     */
    public function getPrice()
    {
        return $this->productPriceManager->getPrice($this->transportType, null, false);
    }

    /**
     * Get price including vat
     *
     * @return float
     * @throws \Exception
     */
    public function getPriceIncl()
    {
        return $this->productPriceManager->getPrice($this->transportType, null, true);
    }


    /**
     * @return float
     * @throws \Exception
     */
    public function getDisplayPrice(?CartOrder $cartOrder = null)
    {
        return $this->productPriceManager->getPrice($this->transportType, null, null, $cartOrder);
    }

    /**
     * @return bool
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function isAvailable(): bool
    {
        if (null === $this->available) {
            if ($this->holiday || $this->format('N') === '7') {
                return $this->available = false;
            }

            $product = $this->iterator->getProduct();

            if ($product === null || $product->getProductgroup() === null) {
                return false;
            }

            $maxTime = $this->getMaxTime($product->getProductgroup());
            $isSameDay = $this->isSameDay($product->getProductgroup());

            $nowTime = date('H:i');

            $previousBusinessDay = null;

            $possiblePreviousBusinessDay = $this;

            while ($possiblePreviousBusinessDay = $this->iterator->getPrevious($possiblePreviousBusinessDay)) {
                if (!$possiblePreviousBusinessDay->getHoliday() && $possiblePreviousBusinessDay->format('N') !== '7') {
                    $previousBusinessDay = $possiblePreviousBusinessDay;
                    break;
                }
            }

            if ($product && $product->getProductgroup() !== null) {
                if (date('N') < 5 && $product->getProductgroup()->getSkuPrefix() === 'FRT') {
                    $maxTime = '20:00';
                }

                if ($product->getProductgroup()->getSkuPrefix() === 'PIE') {
                    $maxTime = '16:50';
                }
            }

            if ($isSameDay) {
                if ($nowTime >= $maxTime && $this->isToday()) {
                    return $this->available = false;
                }
            } else {
                //if today is sunday or holiday, exclude next day
                if (null === $previousBusinessDay) {
                    return $this->available = false;
                }

                if ($this->isToday()) {
                    return $this->available = false;
                }

                if ($nowTime >= $maxTime && null !== $previousBusinessDay && $previousBusinessDay->isToday()) {
                    return $this->available = false;
                }

                if ($product && $product->getProductgroup() !== null && $product->getProductgroup()->getSkuPrefix() === 'FRT') {
                    $previous = $this->iterator->getPrevious($this);

                    if ($previous->format('N') === '7') {
                        $previous = $this->iterator->getPrevious($previous);
                    }

                    if ($previous && $previous->getHoliday()) {
                        return $this->available = false;
                    }
                }

                if (!$this->checkForWeekends($maxTime)) {
                    return $this->available = false;
                }
            }

            return $this->available = true;
        }

        return $this->available;
    }

    /**
     * @param Productgroup $productgroup
     * @return string
     */
    private function getMaxTime(Productgroup $productgroup): ?string
    {
        switch ($productgroup->getSkuPrefix()) {
            case 'FLO':
                return '14:00';
            case 'PIE':
                return '16:50';
            case 'FRT':
                return (date('N') < 5) ? '20:00' : '17:00';
            default:
                return '17:00';
        }
    }

    /**
     * @param Productgroup $productgroup
     * @return string
     */
    private function isSameDay(Productgroup $productgroup): ?string
    {
        return $productgroup->getSkuPrefix() === 'FLO';
    }

    /**
     * @param $maxTime
     * @return bool.
     */
    private function checkForWeekends($maxTime): bool
    {
        $nowTime = date('H:i');

        $current = $this;

        while ($current = $this->iterator->getPrevious($current)) {
            if ($current->format('N') === 0 || $current->getHoliday()) {
                continue;
            }

            return !($nowTime >= $maxTime && $current->format('N') === 6);
        }

        if ($this->iterator->first()) {
            return false;
        }

        return true;
    }

    /**
     * @param Holiday $holiday
     * @return $this
     */
    public function setHoliday(Holiday $holiday): self
    {
        $this->holiday = $holiday;

        return $this;
    }

    /**
     * @return Holiday
     */
    public function getHoliday(): ?Holiday
    {
        return $this->holiday;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isToday(): bool
    {
        $currentDate = new \DateTime();
        $currentDate->setTime(0, 0, 0);

        return ($this == $currentDate);
    }

    /**
     * @param TransportType $transportType
     *
     * @return DeliveryDate
     */
    public function setTransportType(TransportType $transportType): DeliveryDate
    {
        $this->transportType = $transportType;

        return $this;
    }

    /**
     * @param ProductPriceManager $priceManager
     */
    public function setProductPriceManager(ProductPriceManager $priceManager) {
        $this->productPriceManager = $priceManager;
    }

    /**
     * @return TransportType
     */
    public function getTransportType(): TransportType
    {
        return $this->transportType;
    }
}
