<?php

namespace AppBundle\Utils\Auto;

/**
 * Class RefusalReason
 * @package AppBundle\Utils\Auto
 */
class RefusalReason
{
    /**
     * @var string
     */
    protected $description;

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}