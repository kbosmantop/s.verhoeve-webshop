<?php

namespace AppBundle\Utils\Designer;

use SimpleXMLElement;

/**
 * Class SVG
 * @package AppBundle\Utils\Designer
 */
class SVG
{
    /**
     * @var SimpleXMLElement $svg
     */
    private $svg;

    /**
     * @var int $width
     */
    private $width;

    /**
     * @var int $height
     */
    private $height;

    /**
     * SVG constructor.
     * @param string|null $data
     */
    public function __construct(?string $data)
    {
        if ($data) {
            $data = str_replace('&nbsp;', ' ', $data);

            $this->svg = simplexml_load_string($data, null, LIBXML_PARSEHUGE);
        }
    }

    /**
     * @param string $data
     * @return $this
     */
    public static function createFromData(?string $data)
    {
        return new self($data);
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        if (isset($this->svg->attributes()['width'])) {
            return $this->width = (int)$this->svg->attributes()['width'];
        }

        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        if (isset($this->svg->attributes()['height'])) {
            return $this->height = (int)$this->svg->attributes()['height'];
        }
        return $this->height;
    }

    /**
     * @return SimpleXMLElement|null
     */
    public function getSvg()
    {
        return $this->svg;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->svg->__toString();
    }
}