<?php
namespace AppBundle\Utils\Designer;

/**
 * Class DesignDimensions
 * @package AppBundle\Utils\Designer
 */
class DesignDimensions
{
    /**
     * @var float
     */
    public $width = 0;

    /**
     * @var float
     */
    public $height = 0;

    /**
     * @var float
     */
    public $percWidth = 0;

    /**
     * @var float
     */
    public $percHeight = 0;

    /**
     * DesignDimensions constructor.
     *
     * @param $width
     * @param $height
     */
    public function __construct($width, $height)
    {
        $ratio = (float)$width / $height;

        $this->width = $width;
        $this->height = $height;

        if ($ratio >= 1) {
            $this->percWidth = 1.00;
            $this->percHeight = round($height / $width, 3);
        } else {
            $this->percWidth = round($width / $height, 3);
            $this->percHeight = 1.00;
        }
    }
}