<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Gateway\Adyen;
use AppBundle\Manager\Order\OrderCollectionManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AdyenNotification
 * @package AppBundle\Utils
 */
class AdyenNotification
{
    use ContainerAwareTrait;

    private $gateway;

    /**
     * AdyenNotification constructor.
     * @param Adyen $gateway
     */
    public function __construct(Adyen $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function process($data)
    {
        switch ($data->eventCode) {
            case 'NOTIFICATIONTEST':
                return true;
        }

        $this->getDoctrine()->getConnection()->beginTransaction();

        $orderCollection = $this->findOrder($data);

        $adyenNotification = new \AppBundle\ThirdParty\Adyen\Entity\AdyenNotification();
        $adyenNotification->setOrder($orderCollection);
        $adyenNotification->setDatetime(new \DateTime());
        $adyenNotification->setData($data);

        $this->container->get('doctrine')->getManager()->persist($adyenNotification);

        switch ($data->eventCode) {
            case 'AUTHORISATION':
                $this->authorization($data, $orderCollection);
                break;
            case 'CAPTURE':
                $this->capture($data);
                break;
            case 'CANCELLATION':
                $this->cancellation($data);
                break;
            case 'REFUND':
                $this->refund($data);
                break;
            case 'CHARGEBACK':
                $this->chargeback($data, $orderCollection);
                break;

        }

        $this->container->get('doctrine')->getManager()->flush();

        $this->getDoctrine()->getConnection()->commit();

        return true;
    }

    /**
     * @param                 $data
     * @param OrderCollection $orderCollection
     * @throws \Exception
     */
    private function authorization($data, OrderCollection $orderCollection = null)
    {
        $payment = $this->findPayment($data);

        if (!$payment || !$orderCollection) {
            return;
        }

        if ($data->success === 'true') {
            $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('authorized'));

            if ($this->container->get(OrderCollectionManager::class)->calculateOutstanding($orderCollection) === 0) {
                foreach ($orderCollection->getOrders() as $order) {
                    if ($order->getStatus() === 'payment_pending') { // @todo: workflow guard refactor
                        $order->setStatus('new'); // @todo: workflow refactor
                    }
                }
            }
        } else {
            $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('failed'));
        }
    }

    /**
     * @param $data
     */
    private function capture($data)
    {
        $originalPayment = $this->findPaymentByReference($data->originalReference);

        if (!$originalPayment) {
            return;
        }

        $originalPayment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('captured'));
    }

    /**
     * @param $data
     */
    private function cancellation($data)
    {
        $payment = $this->findPayment($data);

        if (!$payment) {
            return;
        }

        $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('cancelled'));
    }

    /**
     * @param $data
     */
    private function refund($data)
    {
        $originalPayment = $this->findPaymentByReference($data->originalReference);

        if (!$originalPayment) {
            return;
        }

        $payment = $this->findPaymentByReference($data->pspReference);

        // Already processed
        if ($payment) {
            return;
        }

        $payment = new Payment();
        $payment->setRelated($originalPayment);
        $payment->setOrder($originalPayment->getOrderCollection());
        $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('refunded'));
        $payment->setAmount(0 - ($data->amount->value / 100));
        $payment->setReference($data->pspReference);
        $payment->setDatetime(new \DateTime());

        $this->getDoctrine()->getManager()->persist($payment);

        $orderActivity = new OrderActivity();
        $orderActivity->setOrderCollection($originalPayment->getOrderCollection());
        $orderActivity->setActivity('payment_refund');
        $orderActivity->setDatetime(new \DateTime());

        $this->getDoctrine()->getManager()->persist($orderActivity);
    }

    /**
     * @param                 $data
     * @param OrderCollection $orderCollection
     */
    private function chargeback($data, OrderCollection $orderCollection = null)
    {
        if (!$orderCollection) {
            return;
        }

        preg_match('/[0-9a-z]*$/', $data->originalReference, $result);

        $originalReference = $result[0];

        $relatedPayment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy([
            'reference' => $originalReference,
        ]);

        $payment = new Payment();
        $payment->setRelated($relatedPayment);
        $payment->setOrder($orderCollection);
        $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find('reversed'));
        $payment->setAmount(0 - ($data->amount->value / 100));
        $payment->setReference($data->pspReference);
        $payment->setDatetime(new \DateTime());
        $payment->setMetadata([
            'reason' => $data->reason,
        ]);

        $this->getDoctrine()->getManager()->persist($payment);
    }

    /**
     * @param $data
     * @return OrderCollection|null
     */
    private function findOrder($data)
    {
        if (!preg_match('/\d{8}/', $data->merchantReference, $match)) {
            return null;
        }

        return $this->getDoctrine()->getRepository(OrderCollection::class)->findOneBy([
            'number' => $match[0],
        ]);
    }

    /**
     * @param $data
     * @return Payment|null
     */
    private function findPayment($data)
    {
        $payment = $this->findPaymentByReference($data->pspReference);

        if ($payment) {
            return $payment;
        }

        $orderCollection = $this->findOrder($data);

        if (!$orderCollection) {
            return null;
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getManager()->getRepository(Payment::class);

        $qb = $entityRepository->createQueryBuilder('payment');
        $qb->leftJoin('payment.orderCollection', 'order_collection');
        $qb->andWhere('payment.reference IS NULL');
        $qb->andWhere('payment.amount = :amount');
        $qb->andWhere('order_collection.id = :orderCollectionId');
        $qb->addOrderBy('payment.datetime', 'DESC');
        $qb->setParameter('amount', $data->amount->value / 100);
        $qb->setParameter('orderCollectionId', $orderCollection->getId());

        $results = new ArrayCollection($qb->getQuery()->getResult());

        $payment = $results->current();

        if ($payment) {
            $payment->setReference($data->pspReference);

            $this->getDoctrine()->getManager()->flush();

            return $payment;
        }

        return null;
    }

    /**
     * @param $reference
     * @return null|Payment
     */
    private function findPaymentByReference($reference)
    {
        $payment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy([
            'reference' => $reference,
        ]);

        return $payment;
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->container->get('doctrine');
    }
}
