<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Geography\CityTranslation;
use AppBundle\Entity\Geography\CountryTranslation;
use GuzzleHttp\Exception\GuzzleException;
use Locale;
use const PHP_EOL;
use function strpos;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Address
 * @package AppBundle\Utils
 */
class Address
{
    use ContainerAwareTrait;

    /**
     * @var string
     */
    public $street;

    /**
     * @var string
     */
    public $houseNumber;

    /**
     * @var string
     */
    public $houseNumberAddition;

    /**
     * @var string
     */
    public $postcode;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format(PHP_EOL);
    }

    /**
     * @param string $separator
     *
     * @return string
     */
    public function format($separator = PHP_EOL)
    {
        return $this->getStreet() . ' ' . $this->getHouseNumber() . $this->getHouseNumberAddition() . $separator . $this->getPostcode() . ' ' . $this->getCity() . $separator . $this->getCountry();
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @return string
     */
    public function getHouseNumberAddition()
    {
        return $this->houseNumberAddition;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param null $locale
     *
     * @return string
     */
    public function getCountry($locale = null)
    {
        if (!$locale) {
            $locale = $this->countryCode;
        }

        return Locale::getDisplayRegion('-' . $locale, $this->countryCode);
    }

    /**
     * @param string $address
     * @throws \Exception
     */
    public function parseAddress($address, $import = false)
    {
        if (substr_count(trim($address), "\n") === 0 && substr_count(trim($address), ',') > 0) {
            $address = str_replace(',', "\n", $address);
        }

        if(strpos($address, '\n') === false && !$import) {
            $regex = "/(\d*[\w\p{L}\d '\-\.\(\)\/]+)\s(\d+)(?:[\s\-\/]?([0-9A-Z\.\-]+))?\s(\d{4,5})\s+([\w\p{L}\d '\-\.\(\)\/]+)$/ui";

            preg_match($regex, trim($address), $parts);

            $this->street = $parts[1];
            $this->houseNumber = $parts[2];
            $this->houseNumberAddition = !empty($parts[3]) ? $parts[3] : null;
            $this->postcode = $parts[4];
            $this->city = $parts[5];
        } else {
            $parts = explode("\n", trim($address));
            $parts = array_map('trim', $parts);

            /**
             * Modified regex from devotis/splitsAdres.js to support unicode addresses
             *
             * @link https://gist.github.com/devotis/c574beaf73adcfd74997
             */
            $regex = "/^(\d*[\w\p{L}\d '\-\.\(\)\/]+)[,\s]+(\d+)\s*([\p{L}\s\d\-\/]*)$/iu";

            preg_match($regex, trim($parts[0]), $parts2);

            if (isset($parts2[1])) {
                $this->street = trim($parts2[1]);
            }

            if (isset($parts2[2])) {
                $this->houseNumber = (int)trim($parts2[2]);
            }

            if (isset($parts2[3])) {
                $this->houseNumberAddition = trim($parts2[3]);
            }

            if (array_key_exists(1, $parts)) {
                $this->postcode = substr($parts[1], 0, strpos($parts[1], ' '));
                $this->city = substr($parts[1], strpos($parts[1], ' ') + 1);
            }

            if (array_key_exists(2, $parts)) {
                if (\strlen($parts[2]) === 2) {
                    $this->countryCode = $parts[2];
                } else {
                    $this->countryCode = $this->detectCountryCode($parts[2]);
                }
            }
        }
    }

    /**
     * @param string $country
     *
     * @return string
     * @throws \Exception
     */
    private function detectCountryCode($country)
    {
        /** @var CountryTranslation $result */
        $result = $this->container->get('doctrine.orm.entity_manager')->getRepository(CountryTranslation::class)->findOneBy([
            'name' => $country,
        ]);

        if (!$result) {
            throw new \RuntimeException("Can't find country code for '" . $country . "'");
        }

        return $result->getTranslatable()->getCode();
    }

    /**
     * @return bool
     * @throws GuzzleException
     */
    public function normalize()
    {
        switch ($this->countryCode) {
            case 'NL':
                if ($this->normalizeThroughPostcodeNL()) {
                    return true;
                }
                break;
            case 'BE':
                if ($this->normalizeThroughDatabase()) {
                    $this->street = ucfirst(strtolower($this->street));

                    return true;
                }
                break;
        }

        $this->street = ucfirst(strtolower($this->street));
        $this->city = ucfirst(strtolower($this->city));

        return true;
    }

    /**
     * @return bool
     * @throws GuzzleException
     */
    private function normalizeThroughPostcodeNL(): bool
    {
        try {
            $response = $this->container->get('postcode_nl')->getAddress($this->postcode, $this->houseNumber,
                $this->houseNumberAddition);

            $this->street = $response->street;
            $this->city = $response->city;
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function normalizeThroughDatabase()
    {
        /** @var CityTranslation $city */
        $city = $this->container->get('doctrine.orm.entity_manager')->getRepository(CityTranslation::class)->findOneBy([
            'name' => $this->city,
        ]);

        if (!$city) {
            return false;
        }

        $this->city = $city->getName();

        return true;
    }
}
