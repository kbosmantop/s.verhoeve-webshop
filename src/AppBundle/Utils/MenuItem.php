<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem as EntityMenuItem;
use AppBundle\Services\Slug;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MenuItem
 * @package AppBundle\Utils
 */
class MenuItem extends MenuStructure
{
    /**
     * @var string
     */
    protected $displayTitle;

    /**
     * @var string
     */
    protected $displayContent;

    /**
     * @var null|string
     */
    protected $absoluteUrl;

    /**
     * @var null|object
     */
    protected $entity;

    /**
     * @var null|bool
     */
    protected $available;

    /** @var array */
    private $doctrineFilters = ['publishable', 'softdeleteable'];

    /**
     * MenuStructure constructor.
     *
     * @param EntityManagerInterface       $entityManager
     * @param Slug                $slugService
     * @param TokenStorage        $tokenStorage
     * @param Menu                $menu
     * @param TranslatorInterface $translator
     * @param EntityMenuItem|null $menuItem
     * @throws Exception
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Slug $slugService,
        TokenStorage $tokenStorage,
        Menu $menu,
        TranslatorInterface $translator,
        EntityMenuItem $menuItem = null
    ) {
        parent::__construct($entityManager, $slugService, $tokenStorage, $menu, $translator, $menuItem);

        $this->toggleDoctrineFilter('disable');

        $locale = $this->translator->getLocale();

        $this->menuItem->setCurrentLocale($locale);

        $this->displayTitle = $this->generateDisplayTitle();

        $this->displayContent = $this->generateDisplayContent();

        $this->absoluteUrl = $this->generateAbsoluteUrl();

        if ($this->hasEntity()) {
            $this->entity = $this->getEntity();
        }

        $this->toggleDoctrineFilter('enable');
    }

    /**
     * Serialize the data for caching purposes
     *
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'displayTitle' => $this->displayTitle,
            'displayContent' => $this->displayContent,
            'absoluteUrl' => $this->absoluteUrl,
            'menuItem' => $this->menuItem,
            'entity' => $this->entity,
            'items' => $this->items,
            'position' => $this->position,
            'available' => $this->isAvailable(),
        ]);
    }

    /**
     * Unserialize the data string in order to use the saved objects
     *
     * @param string $data
     */
    public function unserialize($data)
    {
        $data = unserialize($data, []);

        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Check if an entity is set in the MenuItem
     *
     * @return boolean|\stdClass
     */
    public function hasEntity()
    {
        return ($this->menuItem->getEntityName() && $this->menuItem->getEntityId());
    }

    /**
     *
     * Get the connected entity
     *
     * @return null|object
     */
    public function getEntity()
    {
        $entity = null;

        if(null !== $this->menuItem->getEntityName() && null !== $this->menuItem->getEntityId()) {
            $entity = $this->entityManager->getRepository($this->menuItem->getEntityName())->find($this->menuItem->getEntityId());
        }

        return $entity;
    }

    /**
     * Check whether the entity is available for use
     *
     * return boolean
     */
    public function isEntityAvailable()
    {
        if (!$this->entity) {
            return false;
        }

        if (method_exists($this->entity, 'isPublished') && !$this->entity->isPublished()) {
            return false;
        }

        if (method_exists($this->entity, 'isDeleted') && $this->entity->isDeleted()) {
            return false;
        }

        if (method_exists($this->entity, 'isLoginRequired') && $this->entity->isLoginRequired()) {
            if (!$this->tokenStorage || !$this->tokenStorage->getToken() || !($this->tokenStorage->getToken()->getUser() instanceof Customer)) {
                return false;
            } elseif ($this->tokenStorage && $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof Customer && !$this->entity->getAccessibleCustomerGroups()->isEmpty()) {
                return $this->checkCustomerGroupAccess($this->tokenStorage->getToken()->getUser()->getCustomerGroup(),
                    $this->entity->getAccessibleCustomerGroupIds());
            }
        }

        return (bool)$this->entity;
    }

    /**
     * Method to check if the item is available for displaying in the front-end
     *
     * @return bool
     */
    public function isAvailable()
    {
        if (is_bool($this->available)) {
            return $this->available;
        }

        if (($this->hasEntity() && !$this->isEntityAvailable()) || !$this->menuItem->isPublished()) {
            return false;
        }

        if ($this->menuItem->isLoginRequired()) {
            if (!$this->tokenStorage || !($this->tokenStorage->getToken()->getUser() instanceof Customer)) {
                return false;
            } elseif ($this->tokenStorage && $this->tokenStorage->getToken()->getUser() instanceof Customer && !$this->menuItem->getAccessibleCustomerGroups()->isEmpty()) {
                return $this->checkCustomerGroupAccess($this->tokenStorage->getToken()->getUser()->getCustomerGroup(),
                    $this->menuItem->getAccessibleCustomerGroupIds());
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getDisplayTitle()
    {
        return $this->displayTitle;
    }

    /**
     * @return string
     */
    public function getDisplayContent()
    {
        return $this->displayContent;
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl()
    {
        return $this->absoluteUrl;
    }

    /**
     * Generated the display title
     */
    public function generateDisplayTitle()
    {
        $title = $this->menuItem->translate()->getTitle();

        if (!$title && $this->hasEntity() && $this->getEntity() !== null) {
            $title = $this->getEntity()->getMenuTitle();
        }

        return $title;
    }

    /**
     * Generated the display content
     */
    public function generateDisplayContent()
    {
        return $this->menuItem->translate()->getContent();
    }

    /**
     * Generated the display title
     */
    public function generateAbsoluteUrl()
    {
        $url = $this->menuItem->translate()->getUrl();

        if (!$url && $this->hasEntity() && $this->getEntity() !== null) {
            $url = $this->slugService->getUrl($this->getEntity(), null, $this->menu->getSite(),
                UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return $url;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return \call_user_func_array([$this->menuItem, $name], $arguments);
    }

    /**
     * @param CustomerGroup $customerGroup
     * @param array         $customerGroupIds
     * @return bool
     */
    private function checkCustomerGroupAccess(CustomerGroup $customerGroup, array $customerGroupIds)
    {
        return \in_array($customerGroup->getId(), $customerGroupIds);
    }

    /**
     * @param string $toggle
     *
     * @throws Exception
     */
    private function toggleDoctrineFilter($toggle)
    {
        $allowedToggles = ['enable', 'disable'];

        if (!\in_array($toggle, $allowedToggles)) {
            throw new \RuntimeException(sprintf('Invalid toggle, %s is not in %s', $toggle,
                implode(', ', $allowedToggles)));
        }

        $filterCollection = $this->entityManager->getFilters();

        $checkMethod = !($toggle === 'enable');

        foreach ($this->doctrineFilters as $filter) {
            if (!$filterCollection->has($filter)) {
                continue;
            }

            $filterStatus = $filterCollection->isEnabled($filter);

            // Check if the method has the inversed status of $toggle
            if ($filterStatus == $checkMethod) {
                call_user_func([$filterCollection, $toggle], $filter);
            }
        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'MenuItem';
    }
}
