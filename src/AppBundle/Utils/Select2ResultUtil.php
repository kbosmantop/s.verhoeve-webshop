<?php


namespace AppBundle\Utils;


use Symfony\Component\OptionsResolver\OptionsResolver;

class Select2ResultUtil
{
    /**
     * @param array $items
     * @param array $mappedFields
     * @param bool $groupByParent
     *
     * @return array
     */
    public static function generateList(array $items = [], array $mappedFields = [], $groupByParent = false)
    {
        $mappedFields = self::resolve($mappedFields, $groupByParent);

        $items = array_map(function ($object) use ($mappedFields) {
            return [
                'id' => $object[$mappedFields['id']] ?? null,
                'text' => $object[$mappedFields['text']] ?? null,
                'parent_id' => $object[$mappedFields['parent_id']] ?? null,
                'parent_text' => $object[$mappedFields['parent_text']] ?? null,
            ];
        }, $items);

        if ($groupByParent) {
            $restructuredItems = [];
            foreach ($items as $item) {
                if (!isset($restructuredItems[$item['parent_id']])) {
                    $restructuredItems[$item['parent_id']] = [
                        'id' => $item['parent_id'],
                        'text' => $item['parent_text'],
                        'children' => [],
                    ];
                }

                $parentId = $item['parent_id'];

                unset($item['parent_id']);
                unset($item['parent_text']);

                $restructuredItems[$parentId]['children'][] = $item;
            }

            $items = array_values($restructuredItems);
        }

        return ['items' => $items];
    }

    /**
     * @param array $data
     *
     * @param bool $groupByParent
     *
     * @return array
     */
    private static function resolve(array $data = [], bool $groupByParent = false)
    {
        $optionResolver = new OptionsResolver();

        $required = [];

        $required[] = 'id';
        $required[] = 'text';

        if ($groupByParent) {
            $required[] = 'parent_id';
            $required[] = 'parent_text';
        }

        $optionResolver->setRequired($required);

        return $optionResolver->resolve($data);
    }
}