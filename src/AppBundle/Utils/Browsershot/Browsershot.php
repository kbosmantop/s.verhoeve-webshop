<?php

namespace AppBundle\Utils\Browsershot;

/**
 * Class Browsershot
 * @package AppBundle\Utils\Browsershot
 */
class Browsershot extends \Spatie\Browsershot\Browsershot
{
    /**
     * @var string
     */
    protected $userDataDir = '/tmp/chrome-data';

    /**
     * @return mixed
     */
    private function getEnvironment() {
        global $kernel;

        if(null !== $kernel->getContainer()) {
            return $kernel->getContainer()->getParameter('kernel.environment');
        }

        return 'prod';
    }

    /**
     * @param string $workingDirectory
     * @return array
     */
    public function createScreenshotCommand($workingDirectory = null): array
    {
        $command = parent::createScreenshotCommand($workingDirectory);

        if($this->getEnvironment() === 'dev') {
            $command['options']['args'][] = '--no-sandbox';
        }

        return $command;
    }

    /**
     * @param null $targetPath
     * @return array
     */
    public function createPdfCommand($targetPath = null): array
    {
        $command = parent::createPdfCommand($targetPath);

        if($this->getEnvironment() === 'dev') {
            $command['options']['args'][] = '--no-sandbox';
        }

        return $command;
    }

    public function __destruct()
    {
        $this->cleanupTemporaryHtmlFile();
    }
}