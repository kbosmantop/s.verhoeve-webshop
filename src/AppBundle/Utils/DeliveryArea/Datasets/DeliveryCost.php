<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

use Doctrine\DBAL\DBALException;

/**
 * Class DeliveryCost
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
class DeliveryCost extends AbstractDataset
{
    /**
     * @var string
     */
    protected $label = 'Bezorgtarieven';

    /**
     * @var string
     */
    protected $event = 'getRegions';

    /**
     * @var bool
     */
    public $dateSelect;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    protected $supplierGroups = [
        'Bakkers',
    ];

    /**
     * @var int
     */
    protected $order = 4;

    /**
     * @var array
     */
    protected $supplierConnectors = [
        'Bakker',
        'BakkerMail',
    ];

    /**
     * @param \stdClass|null $filter
     *
     * @return array
     * @throws DBALException
     */
    public function getRegions(\stdClass $filter = null)
    {
        $rawDeliveryAreas = $this->getDeliveryAreas($filter);

        $data = [];
        $data['regions'] = [];
        $data['suppliers'] = [];

        foreach ($rawDeliveryAreas as $rawDeliveryArea) {

            $country = $rawDeliveryArea->country_code;
            $postcode = $rawDeliveryArea->postcode;
            $postcode_id = $rawDeliveryArea->postcode_id;
            $supplierId = $rawDeliveryArea->supplier_id;
            $supplierName = $rawDeliveryArea->supplier_name;
            $price = (float)$rawDeliveryArea->delivery_price;

            if ($price <= 2) {
                $color = $this->getLegendColor(1);
            } elseif ($price <= 3) {
                $color = $this->getLegendColor(2);
            } elseif ($price <= 4) {
                $color = $this->getLegendColor(3);
            } elseif ($price <= 5) {
                $color = $this->getLegendColor(4);
            } elseif ($price <= 6) {
                $color = $this->getLegendColor(5);
            } else {
                $color = $this->getLegendColor(6);
            }

            if (isset($data['regions'][$postcode_id])
                && $data['regions'][$postcode_id]['price'] > $price) {
                $data['regions'][$postcode_id]['price'] = $price;
                $data['regions'][$postcode_id]['color'] = $color;
            } else {
                $data['regions'][$postcode_id] = [
                    'country' => $country,
                    'price' => $price,
                    'postcode' => $postcode,
                    'color' => $color,
                    'premium' => false,
                    'overlap' => false,
                ];
            }

            if ($supplierId && $supplierName) {
                $data['suppliers'][(int)$supplierId] = $supplierName;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getLegend()
    {
        return [
            [
                'color' => '#26AE00',
                'label' => '<= 2,00',
            ],
            [
                'color' => '#4CFF00',
                'label' => '<= 3,00',
            ],
            [
                'color' => '#FFFF00',
                'label' => '<= 4,00',
            ],
            [
                'color' => '#FF6A00',
                'label' => '<= 5,00',
            ],
            [
                'color' => '#FF0000',
                'label' => '<= 6,00',
            ],
            [
                'color' => '#000000',
                'label' => '<span>6,00 en hoger</span>',
            ],
        ];
    }
}