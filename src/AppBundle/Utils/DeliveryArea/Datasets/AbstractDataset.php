<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Interfaces\DeliveryArea\DatasetInterface;
use AppBundle\Services\DeliveryArea\DeliveryAreaService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\DBALException;
use PDO;
use stdClass;

/**
 * Class AbstractDataset
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
abstract class AbstractDataset implements DatasetInterface
{
    /**
     * @var string
     */
    public const DEFAULT_DELIVERY_GROUP = 'Bakkers';

    /**
     * @var string
     */
    protected $label = '';

    /**
     * @var string
     */
    protected $event = 'getData';

    /**
     * @var bool
     */
    public $dateSelect = false;

    /**
     * @var array
     */
    protected $suppliers = [];

    /**
     * @var array
     */
    protected $supplierGroups = [];

    /**
     * @var array
     */
    protected $supplierConnectors = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $legend = [];

    /**
     * @var EntityManagerInterface|null $em
     */
    protected $em;

    /**
     * @var DeliveryAreaService
     */
    protected $deliveryAreaService;

    /**
     * @var DeliveryArea[]|array
     */
    protected $deliveryAreas = [];

    /**
     * @var int
     */
    protected $order = 0;

    /**
     * AbstractDataset constructor.
     *
     * @param DeliveryAreaService $deliveryAreaService
     * @param EntityManagerInterface       $em
     */
    public function __construct(DeliveryAreaService $deliveryAreaService, EntityManagerInterface $em)
    {
        $this->deliveryAreaService = $deliveryAreaService;
        $this->em = $em;
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        $path = explode('\\', get_called_class());
        return array_pop($path);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getLegend()
    {
        return $this->legend;
    }

    /**
     * @return array|int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param stdClass|null $filter
     * @return array|object[]
     * @throws DBALException
     */
    public function getDeliveryAreas(stdClass $filter = null): array
    {
        if (!$this->deliveryAreas) {
            $connection = $this->em->getConnection();

            $sql = '
                SELECT
                    `sda`.`id` `id`
                    ,   `sda`.`postcode_id`
                    ,   `sda`.`delivery_interval`
                    ,   `sda`.`delivery_price`
                    ,   `c`.`id` `supplier_id`
                    ,   `c`.`name` `supplier_name`
                    ,   `c`.`supplier_connector`
                    ,   `sg`.`name`
                    ,   `p`.`postcode`
                    ,   `pro`.`country` `country_code`
                    ,	`cps`.`position` `position`
                FROM `supplier_delivery_area` `sda`
                LEFT JOIN `postcode` `p` ON `sda`.`postcode_id` = `p`.`id`
                LEFT JOIN `province` `pro` ON `pro`.`id` = `p`.`province_id`
                LEFT JOIN `company` `c` ON `sda`.`company_id` = `c`.`id`
                LEFT JOIN `company_supplier_group` `csg` ON `csg`.`company_id` = `c`.`id`
                LEFT JOIN `supplier_group` `sg` ON `csg`.`supplier_group_id` = `sg`.`id`
                LEFT JOIN `company_preferred_supplier` `cps` ON `cps`.`company_id` = `c`.`id` AND `cps`.`delivery_area_id` = `sda`.`id`
                WHERE 1=1
            ';

            $parameters = [];

            if ($filter !== null) {
                if (!empty($filter->suppliers)) {
                    $sql .= ' AND `c`.`id` IN(:suppliers)';
                    $parameters['suppliers'] = implode(',', $filter->suppliers);
                }

                if (!empty($filter->supplierGroup)) {
                    $sql .= ' AND `sg`.`name` = :supplierGroup';
                    $parameters['supplierGroup'] = $filter->supplierGroup;
                }

                if (!empty($filter->supplierConnectors)) {
                    $sql .= ' AND `c`.`supplier_connector` IN(:supplierConnectors)';
                    $parameters['supplierConnectors'] = implode(',', $filter->supplierConnectors);
                }

                if (!empty($filter->intervals)) {
                    $sql .= ' AND `sda`.`delivery_interval` IN(:intervals)';
                    $parameters['intervals'] = implode(',', $filter->intervals);
                }
            }

            $sql .= ' ORDER BY `postcode_id`, IFNULL(`cps`.`position`, 999)';

            $stm = $connection->prepare($sql);
            $stm->execute($parameters);

            $results = $stm->fetchAll(PDO::FETCH_OBJ);

            if($results) {
                $this->deliveryAreas = $results;
            }
        }

        if(null === $this->deliveryAreas) {
            return [];
        }

        return $this->deliveryAreas;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getSuppliers()
    {
        if (!$this->suppliers) {
            foreach ($this->getDeliveryAreas() as $deliveryArea) {
                $supplierId = $deliveryArea->supplier_id;
                $supplierName = $deliveryArea->supplier_name;
                if ($supplierId && $supplierName) {
                    $data['suppliers'][(int)$supplierId] = $supplierName;
                }
            }
        }

        return $this->suppliers;
    }

    /**
     * @return array
     */
    public function getSupplierGroups()
    {
        return $this->supplierGroups;
    }

    /**
     * @return array
     */
    public function getSupplierConnectors()
    {
        return $this->supplierConnectors;
    }

    /**
     * @param $index
     *
     * @return string|null
     */
    public function getLegendColor($index)
    {
        if (isset($this->getLegend()[$index - 1]['color'])) {
            return $this->getLegend()[$index - 1]['color'];
        }
        return null;
    }

}