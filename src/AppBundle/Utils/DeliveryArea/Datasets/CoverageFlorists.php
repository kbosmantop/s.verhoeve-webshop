<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

use Doctrine\DBAL\DBALException;
use stdClass;

/**
 * Class CoverageBakkers
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
class CoverageFlorists extends AbstractDataset
{
    /**
     * @var string
     */
    public const DEFAULT_DELIVERY_GROUP = 'Bloemisten';

    /**
     * @var string
     */
    protected $label = 'Dekkingskaart - Bloemisten';

    /**
     * @var string
     */
    protected $event = 'getRegions';

    /**
     * @var bool
     */
    public $dateSelect;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    protected $supplierGroups = [
        'Bloemisten',
    ];

    /**
     * @var int
     */
    protected $order = 2;

    /**
     * @var array
     */
    protected $supplierConnectors = [
        'Topbloemen'
    ];

    /**
     * @param stdClass $filter
     * @return array
     * @throws DBALException
     */
    public function getRegions(?stdClass $filter = null)
    {
        $rawDeliveryAreas = $this->getDeliveryAreas($filter);

        $data = [];
        $data['regions'] = [];
        $data['suppliers'] = [];

        /** @var object $rawDeliveryArea */
        foreach ($rawDeliveryAreas as $rawDeliveryArea) {
            $country = $rawDeliveryArea->country_code;
            $postcode_id = $rawDeliveryArea->postcode_id;
            $postcode = $rawDeliveryArea->postcode;
            $supplierId = $rawDeliveryArea->supplier_id;
            $supplierName = $rawDeliveryArea->supplier_name;
            $position = (int)$rawDeliveryArea->position;

            $isPreferred = null !== $position;

            // Default non premium;
            $color = ($isPreferred ? $this->getLegendColor(1) : $this->getLegendColor(2));

            if (isset($data['regions'][$postcode_id])) {
                if ($isPreferred) {
                    $data['regions'][$postcode_id]['color'] = $color;
                    $data['regions'][$postcode_id]['premium'] = $isPreferred;
                    $data['regions'][$postcode_id]['overlap'] = true;
                }
            } else {
                $data['regions'][$postcode_id] = [
                    'country' => $country,
                    'postcode' => $postcode,
                    'color' => $color,
                    'premium' => $isPreferred,
                    'overlap' => false,
                ];
            }

            if ($supplierId && $supplierName) {
                $data['suppliers'][(int)$supplierId] = $supplierName;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getLegend()
    {
        return [
            [
                'color' => '#26AE00',
                'label' => 'Premium Dekking',
            ],
            [
                'color' => '#F44336',
                'label' => 'Dekking',
            ]
        ];
    }
}
