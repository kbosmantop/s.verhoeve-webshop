<?php

namespace AppBundle\Utils;

/**
 * Class Translation
 * @package AppBundle\Translation
 */
class Translation
{
    /**
     * @param string $input
     * @param bool   $htmlSpecialChars
     *
     * @return string
     */
    public function sanitizeTranslationString(string $input, bool $htmlSpecialChars = false): string
    {
        if ($htmlSpecialChars) {
            $input = htmlspecialchars($input);
        }

        $patterns = ["/(?:\r\n|\r|\n)/", "/[\s\t]+/"];
        $replace = ['<br>', ' '];

        return preg_replace($patterns, $replace, $input);
    }
}
