<?php

namespace AppBundle\Utils;

use function abs;
use function floor;

/**
 * TODO SF4 move to Static namespace
 * Class NumberUtil
 * @package AppBundle\Utils
 */
class NumberUtil
{
    /**
     * This method truncates all decimals after the given precision without rounding the number. eg: 2.576 will become 2.57 instead of 2.58
     *
     * @param $number
     * @param int $precision
     *
     * @return float|int
     */
    public static function truncateNumber($number, $precision = 2)
    {
        /** @noinspection TypeUnsafeComparisonInspection */
        if (0 == $number) {
            return $number;
        }

        $negative = $number / abs($number);
        $number = abs($number);
        $precision = 10 ** $precision;

        return floor($number * $precision) / $precision * $negative;
    }
}