<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\CartService;
use AppBundle\Services\PriceService;
use AppBundle\Services\SiteService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeliveryDates
 * @package AppBundle\Utils
 * @method DeliveryDate[] getIterator()
 */
class DeliveryDates extends ArrayCollection
{
    use ContainerAwareTrait;

    /**
     * @var Site
     */
    private $site;

    /**
     * DeliveryDates constructor.
     * @param ContainerInterface $container
     * @param array              $elements
     */
    public function __construct(array $elements = [], ContainerInterface $container = null)
    {
        parent::__construct($elements);

        $this->container = $container;

        if($container) {
            $this->site = $container->get(SiteService::class)->determineSite();
        }
    }

    /**
     * @return null|ProductInterface
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getProduct(): ?ProductInterface
    {
        try {
            $product = $this->container->get(CartService::class)->getCurrentOrder()->getLines()[0]->getProduct();
        } catch (NonUniqueResultException | OptimisticLockException $e) {
            return null;
        }

        if ($product->getParent()) {
            $product = $product->getParent();
        }

        if($product instanceof CompanyProduct) {
            $product = $this->container->get(ProductManager::class)->decorateProduct($product);
        }

        return $product;
    }

    /**
     * @param DeliveryDate $value
     * @return bool
     */
    public function add($value): bool
    {
        if ($this->site) {
            $value->setSite($this->site);
        }

        $value->setIterator($this);

        return parent::add($value);
    }

    /**
     * @param $element
     *
     * @return DeliveryDate|null
     */
    public function getPrevious($element): ?DeliveryDate
    {
        $key = $this->indexOf($element);

        if ($key === false) {
            throw new \UnexpectedValueException("Result shouldn't empty");
        }

        if ($key === 0) {
            return null;
        }

        return $this->get($key - 1);
    }

    /**
     * @return Collection
     */
    public function getAvailable(): Collection
    {
        return $this->filter(function (DeliveryDate $deliveryDate) {
            return $deliveryDate->isAvailable();
        });
    }

    /**
     * @return Collection
     */
    public function getHolidays(): Collection
    {
        return $this->filter(function (DeliveryDate $deliveryDate) {
            return $deliveryDate->getHoliday();
        });
    }

    /**
     * @param Datetime $date
     *
     * @return bool
     */
    public function isValid(DateTime $date): bool
    {
        $clone = clone $date;
        $clone->setTime(0, 0, 0);

        $matchedDeliveryDates = $this->filter(function (DeliveryDate $deliveryDate) use ($date) {
            return $deliveryDate == $date;
        });

        if ($matchedDeliveryDates->isEmpty()) {
            return false;
        }

        return $matchedDeliveryDates->current()->isAvailable();
    }

    /**
     * @param Datetime $date
     *
     * @return DeliveryDate
     * @throws \RuntimeException
     */
    public function getDeliveryDate(DateTime $date): DeliveryDate
    {
        if (!$this->isValid($date)) {
            throw new \RuntimeException('Invalid delivery date requested');
        }

        $matchedDeliveryDates = $this->filter(function (DeliveryDate $deliveryDate) use ($date) {
            return $deliveryDate == $date;
        });

        return $matchedDeliveryDates->current();
    }
}
