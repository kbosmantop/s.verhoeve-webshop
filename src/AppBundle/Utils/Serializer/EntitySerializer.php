<?php

namespace AppBundle\Utils\Serializer;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class EntitySerializer
 * @package AppBundle\Utils\Serializer
 */
class EntitySerializer
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var EntityNormalizer
     */
    private $normalizer;

    /**
     * EntitySerializer constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->normalizer = new EntityNormalizer();
        $this->normalizer->setAttributes($attributes);
        $this->serializer = new Serializer([$this->normalizer], [new JsonEncoder()]);
        $this->normalizer->setSerializer($this->serializer);
    }

    /**
     * @param $object
     * @return array
     */
    public function normalize($object): array
    {
        if (is_iterable($object)) {
            return $this->normalizeMultiple($object);
        }
        return $this->normalizeSingle($object);
    }

    /**
     * @param $object
     * @return array
     */
    public function normalizeSingle($object): array
    {
        return $this->serializer->normalize($object, 'json', []);
    }

    /**
     * @param $objects
     * @return array
     */
    public function normalizeMultiple($objects): array
    {
        $normalizedObject = [];
        if(is_iterable($objects)) {
            foreach ($objects as $object) {
                $normalizedObject[] = $this->serializer->normalize($object, 'json', []);
            }
        }
        return $normalizedObject;
    }

    /**
     * @param $data
     * @param $object
     * @return object
     * @throws \Exception
     */
    public function updateObject($data, $object)
    {
        if (is_iterable($object)) {
            return $this->updateObjectMultiple($data, $object);
        }
        return $this->updateObjectSingle($data, $object);
    }

    /**
     * @param $data
     * @param $object
     * @return object
     * @throws \Exception
     */
    public function updateObjectSingle($data, $object)
    {
        return $this->normalizer->updateObject($data, $object);
    }

    /**
     * @param $data
     * @param $objects
     * @return object
     * @throws \Exception
     */
    public function updateObjectMultiple($data, $objects)
    {
        if(is_iterable($objects)){
            foreach($objects as $index => $object) {
                $objects[$index] = $this->normalizer->updateObject($data, $object);
            }
        }
        return $objects;
    }
}