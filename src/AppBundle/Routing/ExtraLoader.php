<?php

namespace AppBundle\Routing;

use AppBundle\Services\DynamicRoutesService;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class ExtraLoader
 * @package AppBundle\Routing
 */
class ExtraLoader extends Loader
{
    use ContainerAwareTrait;

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * @var DynamicRoutesService
     */
    private $dynamicRoutes;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExtraLoader constructor.
     * @param EntityManagerInterface $entityManager
     * @param DynamicRoutesService   $dynamicRoutes
     */
    public function __construct(EntityManagerInterface $entityManager, DynamicRoutesService $dynamicRoutes)
    {
        $this->dynamicRoutes = $dynamicRoutes;
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $resource
     * @param null  $type
     * @return RouteCollection
     * @throws InvalidArgumentException
     */
    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $routeCollection = $this->dynamicRoutes->getRouteCollection();

        $filter->reenableFilter();

        return $routeCollection;
    }

    /**
     * @param mixed $resource
     * @param string|null $type
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }
}
