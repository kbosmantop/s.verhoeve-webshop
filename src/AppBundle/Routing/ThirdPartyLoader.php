<?php

namespace AppBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class ThirdPartyLoader
 * @package AppBundle\Routing
 */
class ThirdPartyLoader extends Loader
{
    /**
     * Loads a resource.
     *
     * @param mixed $resource The resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return RouteCollection
     * @throws \Exception If something went wrong
     */
    public function load($resource, $type = null)
    {
        $fs = new Filesystem();

        $routes = new RouteCollection;

        /** @var SplFileInfo $directory */
        foreach ((new Finder())->directories()->in(__DIR__ . '/../ThirdParty')->depth(0) as $directory) {
            $path = sprintf('%s/Resources/config', $directory->getRealPath());

            if ($fs->exists($path . '/routing.yml')) {
                $routes->addCollection($this->import($path . '/routing.yml', 'yaml'));
            }
        }

        return $routes;
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed $resource A resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return 'third_party' === $type;
    }
}
