<?php

namespace AppBundle\Profiler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

/**
 * Class AmpMatcher
 * @package AppBundle\Profiler
 */
class AmpMatcher implements RequestMatcherInterface
{
    /**
     * @param Request $request
     * @return bool
     */
    public function matches(Request $request)
    {
        return !$request->query->has('amp');
    }
}