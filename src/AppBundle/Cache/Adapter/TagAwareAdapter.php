<?php

namespace AppBundle\Cache\Adapter;

use Symfony\Component\Cache\Adapter\TagAwareAdapter as SymfonyTagAwareAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TagAwareAdapter
 * @package AppBundle\Cache\Adapter
 */
class TagAwareAdapter extends SymfonyTagAwareAdapter
{
    public const CLEAR_ON_RELEASE = 'clear.on_release';

    /**
     * TagAwareAdapter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    ) {
        /** @noinspection MissingService PhpParamsInspection This is defined in the config.yml */
        parent::__construct($container->get('redis_cache'));
    }

    /**
     * @param EntityInterface $item
     * @return string
     * @throws \ReflectionException
     */
    public function generateTagKey($item)
    {
        $reflection = new \ReflectionClass($item);

        $name = str_replace('\\', '_', $reflection->getName());

        if ($reflection->hasMethod('getId')) {
            $name .= '.' . $item->getId();
        }

        return $name;
    }
}