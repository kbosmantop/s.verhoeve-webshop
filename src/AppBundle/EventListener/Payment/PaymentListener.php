<?php

namespace AppBundle\EventListener\Payment;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Manager\Order\BillingItemGroupManager;
use AppBundle\Services\JobManager;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;
use Hoa\Event\Event;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class PaymentListener
 * @package AppBundle\EventListener\Payment
 */
class PaymentListener
{
    /** @var JobManager */
    protected $jobManager;

    /**
     * @var BillingItemGroupManager
     */
    protected $billingItemGroupManager;

    protected $changeSet;

    /**
     * PaymentListener constructor.
     * @param JobManager              $jobManager
     * @param BillingItemGroupManager $billingItemGroupManager
     */
    public function __construct(JobManager $jobManager, BillingItemGroupManager $billingItemGroupManager)
    {
        $this->jobManager = $jobManager;
        $this->billingItemGroupManager = $billingItemGroupManager;
    }

    /**
     * @param Payment $payment
     * @param EventArgs $eventArgs
     *
     * @throws \Exception
     */
    public function prePersist(Payment $payment, EventArgs $eventArgs)
    {
        if($payment->getStatus()->getId() === 'authorized') {
            if($payment->getPaymentmethod()->getCode() !== 'invoice') {
                $orderCollection = $payment->getOrderCollection();
                $this->billingItemGroupManager->createByOrderCollection($orderCollection);
            }

            $this->createPaymentJobs($payment, $eventArgs);
        }
    }

    /**
     * @param Payment $payment
     * @param EventArgs $eventArgs
     *
     * @throws \Exception
     */
    public function preUpdate(Payment $payment, EventArgs $eventArgs)
    {
        /** @var UnitOfWork $uow */
        $uow = $eventArgs->getEntityManager()->getUnitOfWork();
        $changeSet = $uow->getEntityChangeSet($payment);

        $this->changeSet = $changeSet;

        if (isset($this->changeSet['status'])) {
            /** @var PaymentStatus $newStatus */
            $newStatus = array_pop($this->changeSet['status']);

            if ($newStatus->getId() === 'authorized') {
                if($payment->getPaymentmethod()->getCode() !== 'invoice') {
                    $orderCollection = $payment->getOrderCollection();
                    $this->billingItemGroupManager->createByOrderCollection($orderCollection);
                }

                $this->createPaymentJobs($payment, $eventArgs);
            }
        }
    }

    /**
     * @param Payment   $payment
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    private function createPaymentJobs(Payment $payment, EventArgs $eventArgs)
    {
        $orderCollection = $payment->getOrderCollection();
        $paymentAmount = $orderCollection->getPaymentAmount();

        if ($paymentAmount <= 0) {
            $fraudJob = new Job('topgeschenken:fraude:check', [$orderCollection->getId()]);
            $fraudJob->addRelatedEntity($orderCollection);
            $this->jobManager->addJob($fraudJob);

            foreach ($orderCollection->getOrders() as $order) {
                if ($order->getId()) {
                    $job = new Job('auto:process-order', [$order->getId(), '--no-processing'], true, 'auto');
                    $job->addRelatedEntity($order);
                    $this->jobManager->addJob($job, 60);
                }
            }

            $confirmationJob = new Job('topbloemen:shop:send-confirmation', [
                $orderCollection->getId(),
            ], true, 'order_confirmation');
            $confirmationJob->addRelatedEntity($orderCollection);

            // Added delay to reduce chance of double sending the confirmation
            $this->jobManager->addJob($confirmationJob, 60);
        }
    }
}
