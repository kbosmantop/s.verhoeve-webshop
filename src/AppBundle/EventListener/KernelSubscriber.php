<?php

namespace AppBundle\EventListener;

use AppBundle\Services\Domain;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class KernelSubscriber
 * @package AppBundle\EventListener
 */
class KernelSubscriber implements EventSubscriberInterface
{
    use ContainerAwareTrait;

    /**
     * @var Domain
     */
    private $domain;

    /**
     * KernelSubscriber constructor.
     *
     * @param Domain $domain
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * getSubscribedEvents
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $requestUri = $event->getRequest()->getRequestUri();

        switch (explode('/', trim($requestUri, '/'))[0]) {
            case 'voicedata':
            case 'temp-api':
            case 'admin':
                return;
        }

        $domain = $domain = $this->domain->getDomain();

        if ($domain) {
            $siteTheme = $domain->getSite()->getTheme();

            if ($siteTheme !== null) {
                $activeTheme = $this->container->get('liip_theme.active_theme');
                $activeTheme->setName($siteTheme);
            }
        }
    }
}
