<?php

namespace AppBundle\EventListener\Relation;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use function array_merge;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class AddressListener
 * @package AppBundle\EventListener\Relation
 */
class AddressListener
{
    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preRemove',
        ];
    }

    /**
     * @param Address $address
     * @param LifecycleEventArgs $eventArgs
     */
    public function preRemove(Address $address, LifecycleEventArgs $eventArgs)
    {
        /** @var EntityManager $em */
        $em = $eventArgs->getEntityManager();

        //select all companies and customers which have this address as default delivery address and null them
        $companies = $em->getRepository(Company::class)->findBy(['default_delivery_address' => $address]);
        $customers = $em->getRepository(Customer::class)->findBy(['default_delivery_address' => $address]);
        $entities = array_merge($companies, $customers);

        /** @var Customer|Company $entity */
        foreach ($entities as $entity) {
            $entity->setDefaultDeliveryAddress(null);
        }
    }
}