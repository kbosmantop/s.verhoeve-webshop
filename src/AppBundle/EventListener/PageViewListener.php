<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Security\Employee\User;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use DataDog\AuditBundle\Entity\Association;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PageViewListener
 * @package AppBundle\EventListener
 */
class PageViewListener
{
    use ContainerAwareTrait;

    /** @var TokenStorage */
    private $tokenStorage;

    /** @var EntityManagerInterface */
    private $em;

    /** @var string */
    private $adminHost;

    /**
     * PageViewListener constructor.
     * @param TokenStorage  $tokenStorage
     * @param EntityManagerInterface $em
     * @param string        $adminHost
     */
    public function __construct(TokenStorage $tokenStorage, EntityManagerInterface $em, string $adminHost)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->adminHost = $adminHost;
    }

    /**
     * @param FilterControllerEvent $event
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        if ($this->tokenStorage->getToken() !== null &&
            $event->getRequest()->getHost() === $this->adminHost &&
            $this->tokenStorage->getToken()->isAuthenticated() &&
            $this->tokenStorage->getToken()->getUser() !== 'anon.' &&
            $event->getRequest()->attributes->get('_route')) {
            $requestAttributes = $event->getRequest()->attributes;
            [$controller] = $event->getController();

            if ($controller instanceof PageViewControllerInterface) {
                $pageviewSettings = $controller->getPageviewSettings();

                if ($pageviewSettings['route'] === $requestAttributes->get('_route') &&
                    isset($requestAttributes->get('_route_params')[$pageviewSettings['param']])) {

                    $tableName = $this->em->getClassMetadata($pageviewSettings['entity'])->getTableName();
                    $entityId = $requestAttributes->get('_route_params')[$pageviewSettings['param']];

                    /**
                     * @var QueryBuilder $qb
                     */
                    $qb = $this->em->getRepository(AuditLog::class)->createQueryBuilder('log')
                        ->leftJoin('log.source', 's')
                        ->leftJoin('log.blame', 'b')
                        ->andWhere('log.tbl IN (:tbl)')
                        ->andWhere('log.action IN (:action)')
                        ->andWhere('s.class IN (:class)')
                        ->andWhere('s.fk = :entityId')
                        ->andWhere('b.fk IN (:blame)')
                        ->andWhere('log.loggedAt >= :timeAgo')
                        ->setParameter('tbl', $tableName)
                        ->setParameter('class', $pageviewSettings['entity'])
                        ->setParameter('action', 'view')
                        ->setParameter('blame', $this->tokenStorage->getToken()->getUser()->getId())
                        ->setParameter('timeAgo', new \DateTime('-1 minute'))
                        ->setParameter('entityId', $entityId)
                        ->setMaxResults(1);

                    /** @var AuditLog $log */
                    $log = $qb->getQuery()->getOneOrNullResult();

                    if ($log) {
                        $this->em->createQueryBuilder()->update(AuditLog::class, 'log')
                            ->set('log.loggedAt', $qb->expr()->literal(date('Y-m-d H:i:s')))
                            ->where('log.id = ?1')
                            ->setParameter(1, $log->getId())
                            ->getQuery()
                            ->execute();

                        /** @var Association $source */
                        $source = $log->getSource();

                        $this->em->createQueryBuilder()->update(Association::class, 'a')
                            ->set('a.fk', $entityId)
                            ->where('a.id = :id')
                            ->setParameter('id', $source->getId())
                            ->getQuery()
                            ->execute();

                    } else {
                        /** @var Connection $connection */
                        $connection = $this->em->getConnection();

                        $connection
                            ->createQueryBuilder()
                            ->insert('audit_associations')
                            ->values([
                                'typ' => $connection->quote(''),
                                'tbl' => $connection->quote($tableName),
                                'fk' => $entityId,
                                'class' => $connection->quote($pageviewSettings['entity']),
                            ])
                            ->execute();
                        $associationId = $connection->lastInsertId();

                        $connection
                            ->createQueryBuilder()
                            ->insert('audit_associations')
                            ->values([
                                'typ' => $connection->quote('app.security.employee.user'),
                                'tbl' => $connection->quote('adiuvo_user'),
                                'fk' => $this->tokenStorage->getToken()->getUser()->getId(),
                                'label' => $connection->quote($this->tokenStorage->getToken()->getUser()->getUsername()),
                                'class' => $connection->quote(User::class),
                            ])
                            ->execute();
                        $blameId = $connection->lastInsertId();

                        $connection
                            ->createQueryBuilder()
                            ->insert('audit_logs')
                            ->values([
                                'action' => $connection->quote('view'),
                                'tbl' => $connection->quote($tableName),
                                'source_id' => $associationId,
                                'blame_id' => $blameId,
                                'logged_at' => $connection->quote(date('Y-m-d H:i:s')),
                            ])
                            ->execute();
                    }
                }
            }
        }
    }

}
