<?php

namespace AppBundle\EventListener\Order;

use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Manager\Order\CartManager;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Services\Discount\VoucherService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class CartListener
 */
class CartListener
{
    /**
     * @var CartManager
     */
    protected $cartManager;

    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * OrderCollectionListener constructor.
     *
     * @param CartManager $cartManager
     * @param VoucherService $voucherService
     */
    public function __construct(CartManager $cartManager, VoucherService $voucherService)
    {
        $this->cartManager = $cartManager;
        $this->voucherService = $voucherService;
    }

    /**
     * @var array
     */
    protected $orders = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'prePersist',
        ];
    }

    /**
     * @param Cart               $cart
     * @param LifecycleEventArgs $event
     * @throws \Exception
     */
    public function preUpdate(Cart $cart, LifecycleEventArgs $event)
    {
        foreach($cart->getVouchers() as $voucher) {
            $this->voucherService->applyVoucherForOrderCollectionInterface($voucher->getCode(), $cart, true);
        }

        $this->calculateTotals($cart, $event);
    }

    /**
     * @param Cart               $cart
     * @param LifecycleEventArgs $event
     * @throws \Exception
     */
    public function prePersist(Cart $cart, LifecycleEventArgs $event)
    {
        $this->calculateTotals($cart, $event);
    }

    /**
     * @param OrderCollection    $cart
     * @param LifecycleEventArgs $event
     * @throws \Exception
     */
    public function calculateTotals(Cart $cart, LifecycleEventArgs $event)
    {
        void($event);
        
        $this->cartManager->calculateTotals($cart);
    }
}
