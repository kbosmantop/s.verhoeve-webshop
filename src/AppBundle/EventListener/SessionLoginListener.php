<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class SessionLoginListener
{
    use ContainerAwareTrait;

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequest()->getHost() == $this->container->getParameter("admin_host")) {
            return;
        }

        if ($this->checkSessionLogin() && $event->isMasterRequest()) {
            $route = $this->container->get("request_stack")->getMasterRequest()->get("_route");

            try {
                $customer = $this->container->get('doctrine')->getManager()->find(Customer::class,
                    $this->getSession()->get('login_user_after_returning'));

                $this->container->get('fos_user.security.login_manager')->logInUser('main', $customer);

                $this->getSession()->remove('login_user_after_returning');

                $response = new RedirectResponse($this->container->get("router")->generate($route));
                $event->setResponse($response);
            } catch (\Exception $e) {
            }
        }
    }

    private function checkSessionLogin()
    {
        return ($this->getSession()->has('login_user_after_returning') && $this->getSession()->get('login_user_after_returning') !== false);
    }

    private function getSession()
    {
        return $this->container->get('session');
    }
}