<?php

namespace AppBundle\EventListener\Authentication\Employee;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class LoginListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param RequestStack $requestStack
     * @param TokenStorage $tokenStorage
     */
    public function __construct(RequestStack $requestStack, TokenStorage $tokenStorage)
    {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        $payload = $event->getData();

        if ($request->getSession()->has('employee_login_jwt')) {
            $userId = null;

            $user = $this->tokenStorage->getToken()->getUser();

            if ($user) {
                $userId = $user->getId();
            }

            $payload['employee_login_as_customer'] = true;
            $payload['employee_id'] = $userId;

            $request->getSession()->remove('employee_login_jwt');
        }

        $event->setData($payload);
    }

    /**
     * @param JWTDecodedEvent $event
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        $payload = $event->getPayload();

        // Check for required fields in JWT payload and register session variables for further use
        if (array_key_exists('employee_login_as_customer', $payload) && (array_key_exists('employee_id',
                    $payload) && !is_null($payload['employee_id']))) {
            $request->getSession()->set('employee_login_as_customer', true);
            $request->getSession()->set('employee_id', $payload['employee_id']);
        }
    }
}