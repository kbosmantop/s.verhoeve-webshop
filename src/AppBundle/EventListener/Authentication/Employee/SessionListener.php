<?php

namespace AppBundle\EventListener\Authentication\Employee;

use Symfony\Bundle\SecurityBundle\Security\FirewallMap;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SessionListener
{
    use ContainerAwareTrait;

    /**
     * @var int
     */
    private $expirationTime;

    /**
     * SessionListener constructor.
     *
     * @param int $expirationTime
     */
    public function __construct(int $expirationTime)
    {
        $this->expirationTime = $expirationTime;
    }

    /**
     * Check if session has inactivity for too long,
     * and invalidate it if that's the case
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $request = $event->getRequest();

        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->container->get('security.token_storage');

        /** @var FirewallMap $firewallMap */
        $firewallMap = $this->container->get('security.firewall.map');

        $firewallName = $firewallMap->getFirewallConfig($request)->getName();

        if (!is_null($tokenStorage->getToken()) && !is_null($tokenStorage->getToken()) && $firewallName == "main") {
            $session = $request->getSession();

            if ($session->has('employee_login_as_customer')) {
                $session->start();

                $metadata = $session->getMetadataBag();

                $timeDifference = time() - $metadata->getLastUsed();

                if ($timeDifference > $this->expirationTime) {
                    $event->setResponse(new RedirectResponse("/account/logout"));
                }
            }
        }
    }
}
