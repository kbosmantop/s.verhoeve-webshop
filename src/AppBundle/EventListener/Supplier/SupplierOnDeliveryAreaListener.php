<?php

namespace AppBundle\EventListener\Supplier;

use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Services\Parameter\ParameterService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;

/**
 * Class SupplierOnDeliveryAreaListener
 */
class SupplierOnDeliveryAreaListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * DeliveryAreaListener constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterService       $parameterService
     */
    public function __construct(EntityManagerInterface $entityManager, ParameterService $parameterService)
    {
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
    }

    /**
     * @var array
     */
    protected $orders = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preRemove',
        ];
    }

    /**
     * @param DeliveryArea    $deliveryArea
     * @param LifecycleEventArgs $event
     */
    public function preRemove(DeliveryArea $deliveryArea, LifecycleEventArgs $event)
    {
        $supplier = $deliveryArea->getCompany();
        $deliveryAreaRepository = $this->entityManager->getRepository(DeliveryArea::class);
        $deliveryAreas = $deliveryAreaRepository->findBy([
            'company' => $supplier,
        ]);
        if(\count($deliveryAreas) === 1){
            //Count 1 because this last one is about to be deleted
            $carrierRepository = $this->entityManager->getRepository(Carrier::class);
            /** @var Carrier $carrier */
            $carrier = $carrierRepository->findOneBy([
                'company' => $supplier,
            ]);
            if($carrier !== null){
                $this->entityManager->remove($carrier);
            }
        }
    }

    /**
     * @param DeliveryArea $deliveryArea
     * @param LifecycleEventArgs $event
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function prePersist(DeliveryArea $deliveryArea, LifecycleEventArgs $event)
    {
        $supplier = $deliveryArea->getCompany();
        $carrierRepository = $this->entityManager->getRepository(Carrier::class);

        /** @var UnitOfWork $uow */
        $uow = $event->getEntityManager()->getUnitOfWork();
        $insertions = array_filter($uow->getScheduledEntityInsertions(), function($object) use($supplier) {
            return $object instanceof Carrier && $object->getCompany() === $supplier;
        });

        if(empty($insertions)) {
            /** @var Carrier $carrier */
            $carrier = $carrierRepository->findOneOrCreate([
                'company' => $supplier,
            ]);

            if($carrier->getId() === null){
                if(($deliveryMethod = $this->parameterService->setEntity()->getValue('delivery_method_by_supplier')) !== null){
                    $deliveryMethod = $this->entityManager->getRepository(DeliveryMethod::class)->find($deliveryMethod);
                    if($deliveryMethod !== null) {
                        $carrier->addDeliveryMethod($deliveryMethod);
                    }
                }
                $carrier->setShortName(substr($supplier->getName(), 0, 20));

                $class = $event->getEntityManager()->getClassMetadata(Carrier::class);
                /** @var UnitOfWork $uow */
                $uow = $event->getEntityManager()->getUnitOfWork();
                $uow->computeChangeSet($class, $carrier);
            }
        }
    }
}
