<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\TransportType;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use function get_class;

/**
 * Class SkuListener
 * @package AppBundle\EventListener
 */
class SkuListener
{
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     *
     * @param Product            $product
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */

    public function post(Product $product, LifecycleEventArgs $args)
    {
        if ($product->getSku()) {
            return;
        }

        //only give sku's to child products
        if (in_array(get_class($product), [ProductCard::class, TransportType::class], true) || null !== $product->getParent()) {
            $parent = $product->getParent();
            $productGroup = null !== $parent ? $parent->getProductgroup() : $product->getProductgroup();

            if (!$productGroup) {
                return;
            }

            $skuPrefix = $productGroup->getSkuPrefix();

            if (!$skuPrefix) {
                return;
            }

            $sql = '
                SELECT MAX(SUBSTR(sku, 4)) 
                FROM product
                WHERE LEFT(sku, 3) = :skuPrefix
                GROUP BY LEFT(sku, 3)
            ';

            $em = $args->getEntityManager();

            $stmt = $em->getConnection()->prepare($sql);
            $stmt->bindValue(':skuPrefix', $skuPrefix);
            $stmt->execute();

            $skuNumber = max([(int)$stmt->fetchColumn() + 1, 10000]);
            $skuNumber = str_pad($skuNumber, 5, '0', STR_PAD_LEFT);

            $product->setSku($skuPrefix . $skuNumber);

            $em = $args->getObjectManager();
            $uow = $args->getEntityManager()->getUnitOfWork();

            $uow->computeChangeSet($em->getClassMetadata(ClassUtils::getRealClass(\get_class($product))), $product);
        }
    }
}
