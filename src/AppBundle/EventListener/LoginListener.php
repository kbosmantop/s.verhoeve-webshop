<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\CartService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener
{
    use ContainerAwareTrait;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * LoginListener constructor.
     *
     * @param AuthorizationChecker $authorizationChecker
     * @param CartService          $cartService
     */
    public function __construct(AuthorizationChecker $authorizationChecker, CartService $cartService)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->cartService = $cartService;
    }

    /**
     * Do the magic.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        /** @var Customer $user */
        $user = $event->getAuthenticationToken()->getUser();


        if ($user instanceof Customer && $this->authorizationChecker->isGranted('ROLE_USER') && !$event->getRequest()->getSession()->has('employee_login_as_customer')) {
            /** @var Cart $cart */
            $this->cartService->migrate($user);
        }
    }
}
