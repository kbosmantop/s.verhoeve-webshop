<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Relation\Company;
use AppBundle\Manager\Relation\CompanyManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class CompanyListener
 * @package AppBundle\EventListener
 */
class CompanyListener implements EventSubscriber
{
    /** @var CompanyManager $companyManager */
    private $companyManager;

    /**
     * CompanyListener constructor.
     * @param CompanyManager $companyManager
     */
    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
        ];
    }

    /**
     * @param Company $company
     */
    public function prePersist(Company $company)
    {
        $this->companyManager->getMainEstablishment($company);
    }
}