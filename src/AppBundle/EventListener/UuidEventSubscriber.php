<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ramsey\Uuid\Uuid;

/**
 * Class UuidEventSubscriber
 * @package AppBundle\EventListener
 */
class UuidEventSubscriber implements EventSubscriber
{
    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (method_exists($entity, 'getUuid') && method_exists($entity, 'setUuid')) {
            if ($entity->getUuid() === null) {
                $entity->setUuid(Uuid::uuid4());
            }
        }
    }

}
