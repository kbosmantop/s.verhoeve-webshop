<?php


namespace AppBundle\EventListener;

use AppBundle\Entity\Site\MenuItem;
use AppBundle\Entity\Site\MenuSourceInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

class MenuSourceSubscriber implements EventSubscriber
{
    /**
     * @var array
     */
    private $menuItemsToUpdate = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'postFlush',
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $this->check($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    private function check(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (in_array(MenuSourceInterface::class, class_implements($entity))) {
            if ($event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity)) {
                $menuItems = $this->getMenuItems($event->getEntityManager(), $entity);

                foreach ($menuItems as $menuItem) {
                    $this->menuItemsToUpdate[] = $menuItem;
                }
            }
        }
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param               $entity
     *
     * @return boolean|array|MenuItem[]
     */
    private function getMenuItems(EntityManagerInterface $entityManager, $entity)
    {
        $entityName = get_class($entity);

        if (!method_exists($entity, 'getId')) {
            return false;
        }

        $entityId = $entity->getId();

        return $entityManager->getRepository(MenuItem::class)->findBy([
            'entityName' => $entityName,
            'entityId' => $entityId,
        ]);
    }

    /**
     * @param PostFlushEventArgs $event
     */
    public function postFlush(PostFlushEventArgs $event)
    {
        if ($this->menuItemsToUpdate) {
            /**
             * @var MenuItem $menuItem
             */
            foreach ($this->menuItemsToUpdate as $menuItem) {
                $menuItem->setUpdatedAt(new \DateTime());
            }

            $this->menuItemsToUpdate = [];

            $event->getEntityManager()->flush();
        }
    }
}
