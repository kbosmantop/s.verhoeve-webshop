<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;

class TranslationListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::POST_SET_DATA => 'postSetData',
            FormEvents::SUBMIT => 'submit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $formOptions = $form->getConfig()->getOptions();

        foreach ($formOptions['locales'] as $locale) {
            $form->add($locale, $formOptions['field_type'], [
                'required' => in_array($locale, $formOptions['required_locales'], true),
            ]);
        }
    }

    /**
     * @param FormEvent $event
     *
     * @throws \Exception
     */
    public function postSetData(FormEvent $event)
    {
        if (!$event->getForm()->getParent()->getData()) {
            $closure = $event->getform()->getParent()->getConfig()->getEmptyData();

            $data = $closure($event->getForm());

            if (!$data) {
                throw new \Exception("Closure shouldn't return null, required for this element must be set to true");
            }

            $event->getForm()->getParent()->setData($data);
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($event->getForm()->all() as $field) {
            $translation = $event->getForm()->getParent()->getData()->translate($field->getName(), false);

            $field->setData($accessor->getValue($translation, $this->getFieldName($event->getForm()->getConfig())));
        }
    }

    /**
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        $entity = $event->getForm()->getParent()->getData();

        if ($entity != null) {
            $data = $event->getData();

            $accessor = PropertyAccess::createPropertyAccessor();

            foreach ($data as $locale => $value) {
                $translation = $entity->translate($locale, false);

                $accessor->setValue($translation, $this->getFieldName($event->getForm()->getConfig()), $value);
            }

            $entity->mergeNewTranslations();
        }
    }

    /**
     * @param FormBuilder $form
     *
     * @return mixed
     */
    private function getFieldName(FormBuilder $form)
    {
        $name = $form->getPropertyPath();

        if (!$name) {
            $name = $form->getName();
        }

        return $name;
    }
}
