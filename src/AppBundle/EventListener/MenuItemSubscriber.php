<?php


namespace AppBundle\EventListener;

use AppBundle\Entity\Site\MenuItem;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

class MenuItemSubscriber implements EventSubscriber
{
    /**
     * @var array
     */
    private $menuItems = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'postFlush',
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $this->check($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $this->check($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    private function check(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($event->getEntity() instanceof MenuItem) {
            if ($event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity)) {
                $this->menuItems[] = $entity;
            }
        }
    }

    /**
     * @param PostFlushEventArgs $event
     */
    public function postFlush(PostFlushEventArgs $event)
    {
        if ($this->menuItems) {

            /**
             * @var MenuItem $menuItem
             */
            foreach ($this->menuItems as $menuItem) {
                $menuItem->getMenu()->setUpdatedAt(new \DateTime());
            }

            $this->menuItems = [];

            $event->getEntityManager()->flush();
        }
    }
}
