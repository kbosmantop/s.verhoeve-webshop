<?php

namespace AppBundle\EventListener;

use AppBundle\Annotation\GeneralListener;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\ORMInvalidArgumentException;

/**
 * Class GeneralEventSubscriber
 * @package AppBundle\EventListener
 */
class GeneralEventSubscriber implements EventSubscriber
{
    /**
     * @var AnnotationReader $annotationReader
     */
    protected $annotationReader;

    /**
     * @var array $scheduledForUpdate
     */
    protected $scheduledForUpdate;

    /**
     * GeneralEventSubscriber constructor.
     * @throws AnnotationException
     */
    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'onFlush',
            'preUpdate',
        ];
    }

    /**
     * @param PreUpdateEventArgs $args
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        $uow = $args->getEntityManager()->getUnitOfWork();

        //convert class name to the real classname instead of proxy name
        $class = ClassUtils::getRealClass(\get_class($entity));
        $customListenerAnnotation = $this->annotationReader->getClassAnnotation(new \ReflectionClass($class),
            GeneralListener::class);

        if (null !== $customListenerAnnotation) {
            $parentGetter = $customListenerAnnotation->getParentGetter();

            //check if method exists for entity
            if (!method_exists($entity, $parentGetter)) {
                throw new \RuntimeException(sprintf('The method %s does not exists for class %s. Are your annotations set up correctly?',
                    $parentGetter, $class));
            }

            //set parent and schedule for update
            $parentEntity = $entity->$parentGetter();
            if (null !== $parentEntity) {
                $uow->computeChangeSet($args->getEntityManager()->getClassMetadata(ClassUtils::getRealClass(\get_class($parentEntity))),
                    $parentEntity);
            }
        }
    }

    /**
     * @param OnFlushEventArgs $args
     * @throws \Exception
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $this->scheduledForUpdate = [];
        $this->scheduleEvents($args);
    }

    /**
     * @param OnFlushEventArgs $args
     * @throws \Exception
     */
    protected function scheduleEvents(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $scheduledAllEntities = true;

        foreach ($uow->getScheduledEntityUpdates() as $updated) {
            //get the real name of the class
            $class = ClassUtils::getRealClass(\get_class($updated));

            //if the class is already mentioned in the array there is no need for scheduling
            if (\in_array($class, $this->scheduledForUpdate, true)) {
                continue;
            }

            $this->scheduledForUpdate[] = $class;
            $scheduledAllEntities = false;

            $customListenerAnnotation = $this->annotationReader->getClassAnnotation(new \ReflectionClass($class),
                GeneralListener::class);
            if (null !== $customListenerAnnotation) {
                //get the annotation properties
                $parentGetter = $customListenerAnnotation->getParentGetter();
                $getter = $customListenerAnnotation->getGet();
                $setter = $customListenerAnnotation->getSet();

                //check if method exists on entity
                if (!method_exists($updated, $parentGetter)) {
                    throw new \RuntimeException(sprintf('The method %s does not exists for class %s. Are your annotations set up correctly?',
                        $parentGetter, $class));
                }

                //set parent and schedule for update
                $entity = $updated->$parentGetter();
                if (null !== $entity) {
                    $entityName = ClassUtils::getRealClass(\get_class($entity));
                    //check if methods exists for entities
                    if (!method_exists($entity, $setter)) {
                        throw new \RuntimeException(sprintf('The method %s does not exists for class %s. Are your annotations set up correctly?',
                            $setter, $entityName));
                    }

                    if (!method_exists($entity, $getter)) {
                        throw new \RuntimeException(sprintf('The method %s does not exists for class %s. Are your annotations set up correctly?',
                            $getter, $entityName));
                    }

                    // handling for soft-deletes
                    try {
                        $entity->$setter($updated->$getter());
                        $uow->scheduleForUpdate($entity);
                    } catch (EntityNotFoundException $e) {
                        continue;
                    } catch (ORMInvalidArgumentException $e) {
                        continue;
                    }
                }
            }
        }

        //if not all entities were scheduled reiterate the scheduled entities to potentially schedule their parents
        if (!$scheduledAllEntities) {
            $this->scheduleEvents($args);
        }
    }
}
