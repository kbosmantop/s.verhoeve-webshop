<?php

namespace AppBundle\EventListener\Report;

use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Manager\Report\CompanyReportCustomerManager;
use AppBundle\Services\JobManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CompanyReportCustomerListener
 * @package AppBundle\EventListener\Report
 */
class CompanyReportCustomerListener
{

    /**
     * @var JobManager
     */
    protected $jobmanager;

    /**
     * @var CompanyReportCustomerManager
     */
    protected $companyReportCustomerManager;

    /**
     * @var string
     */
    protected $companyReportCustomerCommand = 'app:company-report-customer:send-report';

    /**
     * @param JobManager                   $jobManager
     * @param CompanyReportCustomerManager $companyReportCustomerManager
     */
    public function __construct(JobManager $jobManager, CompanyReportCustomerManager $companyReportCustomerManager)
    {
        $this->jobmanager = $jobManager;
        $this->companyReportCustomerManager = $companyReportCustomerManager;
    }

    /**
     * @ORM\PostPersist()
     * @param CompanyReportCustomer $companyReportCustomer
     * @param LifecycleEventArgs    $args
     * @throws \Exception
     */
    public function postPersist(CompanyReportCustomer $companyReportCustomer, LifecycleEventArgs $args)
    {
        $this->companyReportCustomerManager->schedule($companyReportCustomer);
    }

    /**
     * @ORM\PreRemove()
     * @param CompanyReportCustomer $companyReportCustomer
     * @param LifecycleEventArgs    $args
     */
    public function preRemove(CompanyReportCustomer $companyReportCustomer, LifecycleEventArgs $args)
    {
        $relatedJobs = $this->jobmanager->getStartableJobs($this->companyReportCustomerCommand, $companyReportCustomer);

        $em = $args->getEntityManager();
        foreach ($relatedJobs as $relatedJob) {
            $em->remove($relatedJob);
        }
    }

}