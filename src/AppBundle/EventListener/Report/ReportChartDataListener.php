<?php

namespace AppBundle\EventListener\Report;

use AppBundle\Entity\Report\ReportChartData;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ReportChartListener
 * @package AppBundle\EventListener\Report
 */
class ReportChartDataListener
{

    use ContainerAwareTrait;

    /**
     * @ORM\PostPersist()
     * @param ReportChartData    $reportChartData
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function postPersist(ReportChartData $reportChartData, LifecycleEventArgs $args)
    {
        $this->createJob($reportChartData);
    }

    /**
     * @ORM\PostUpdate()
     * @param ReportChartData    $reportChartData
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function postUpdate(ReportChartData $reportChartData,LifecycleEventArgs $args)
    {
        $this->createJob($reportChartData);
    }

    /**
     * @param ReportChartData $reportChartData
     * @throws \Exception
     */
    private function createJob(ReportChartData $reportChartData)
    {
        $job = new Job("app:generate:chart", [$reportChartData->getReportChart()->getId()]);
        $job->addRelatedEntity($reportChartData->getReportChart());

        $this->container->get("job.manager")->addJob($job);
        $this->container->get('doctrine')->getManager()->flush();
    }

}