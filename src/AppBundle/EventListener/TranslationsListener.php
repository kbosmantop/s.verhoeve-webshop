<?php

namespace AppBundle\EventListener;

use A2lix\AutoFormBundle\Form\Type\AutoFormType;
use A2lix\TranslationFormBundle\Form\EventListener\TranslationsListener as A2lixTranslationsListener;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;

/**
 * Class TranslationsListener
 * @package AppBundle\EventListener
 */
class TranslationsListener extends A2lixTranslationsListener
{
    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event) :void
    {
        $form = $event->getForm();

        if (null === $formParent = $form->getParent()) {
            throw new \RuntimeException('Parent form missing');
        }

        $formOptions = $form->getConfig()->getOptions();

        $translationClass = $this->getTranslationClass($formParent);

        $fieldsOptions = $this->getFieldsOptions($form, $formOptions);

        foreach ($formOptions['locales'] as $locale) {
            if (!isset($fieldsOptions[$locale])) {
                continue;
            }

            $form->add($locale, AutoFormType::class, [
                'data_class' => $translationClass,
                'required' => \in_array($locale, $formOptions['required_locales'], true),
                'block_name' => (isset($formOptions['theming_granularity']) && 'field' === $formOptions['theming_granularity']) ? 'locale' : null,
                'fields' => $fieldsOptions[$locale],
                'excluded_fields' => $formOptions['excluded_fields'],
            ]);
        }
    }

    /**
     * @param FormInterface $form
     * @return string
     */
    private function getTranslationClass(FormInterface $form): string
    {
        do {
            $translatableClass = $form->getConfig()->getDataClass();
        } while ((null === $translatableClass) && $form->getConfig()->getInheritData() && (null !== $form = $form->getParent()));

        // Knp
        if (method_exists($translatableClass, 'getTranslationEntityClass')) {
            return $translatableClass::getTranslationEntityClass();
        }

        // Gedmo
        if (method_exists($translatableClass, 'getTranslationClass')) {
            return $translatableClass::getTranslationClass();
        }

        return $translatableClass.'Translation';
    }
}
