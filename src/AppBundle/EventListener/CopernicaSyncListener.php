<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JMS\JobQueueBundle\Entity\Job;

class CopernicaSyncListener
{
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'postDelete',
        ];
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @param Customer           $customer
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(Customer $customer, LifecycleEventArgs $args)
    {
        void($customer);

        $em = $args->getObjectManager();
        $entity = $args->getEntity();

        $job = new Job('topbloemen:customer:copernica:sync', [$entity->getId()]);
        $job->addRelatedEntity($entity);

        $em->persist($job);
        $em->flush();
    }
}
