<?php

namespace AppBundle\ThirdParty\Csv\Services;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\Designer\ProcessDesignException;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Services\CartService;
use AppBundle\Services\Designer\CsvDesignGeneratorService;
use AppBundle\ThirdParty\Csv\Entity\CsvOrder;
use AppBundle\ThirdParty\Csv\Exception\CsvImportException;
use DateTime;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\DriverException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use http\Exception\RuntimeException;
use PhpOffice\PhpSpreadsheet\Exception as SpreadsheetException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use function preg_match;
use Swift_Attachment;
use Swift_Mailer;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;

/**
 * Class CsvService
 * @package AppBundle\ThirdParty\Csv\Services
 */
class CsvService
{
    public const STATUS_ERROR = 'error';
    public const STATUS_ARCHIVE = 'archive';

    // Designer constants
    public const MAX_SENTENCE_LENGTH = 45;

    public const PIXELS_Y = 293.8;
    public const PIXELS_X = 0.754;

    /**
     * @var boolean $dryRun
     */
    private $dryRun = false;

    /**
     * @var EntityManagerInterface $entityManager
     */
    public $entityManager;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var Company $company
     */
    private $company;

    /**
     * @var string $fileName
     */
    private $fileName;

    /**
     * @var string $defaultLocation
     */
    private $defaultLocation;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CsvDesignGeneratorService
     */
    private $designGenerator;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * CsvService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param CartService $cartService
     * @param CsvDesignGeneratorService $designGenerator
     * @param Swift_Mailer $mailer
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CartService $cartService,
        CsvDesignGeneratorService $designGenerator,
        Swift_Mailer $mailer,
        ParameterBagInterface $parameterBag
    ) {
        $this->entityManager = $entityManager;
        $this->cartService = $cartService;
        $this->designGenerator = $designGenerator;
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
    }

    /**
     * Read all available CSV files from specified location
     *
     * @param ConsoleLogger $logger
     * @param string $csv
     *
     * @return array
     * @throws Exception
     * @throws OptimisticLockException
     * @throws SpreadsheetException
     */
    public function readCsv(ConsoleLogger $logger, $csv = null): array
    {
        $config = $this->getConfig();

        if ($this->defaultLocation === null) {
            throw new CsvImportException('The default location for csv import was not found (\'csv_location\' in \'parameters.yml\')');
        }

        $csvArray = [];

        if (isset($config['companies'])) {
            foreach ($config['companies'] as $companyId => $information) {
                /**
                 * @var Company $company
                 */
                $company = $this->entityManager->getRepository(Company::class)->find((int)$companyId);

                if (null === $company) {
                    throw new \RuntimeException(sprintf('Company with ID: %s not found', $companyId));
                }

                $finder = new Finder();

                if (empty($information['file_location'])) {
                    throw new \RuntimeException(sprintf('Path location for company %s not found', $companyId));
                }

                $path = $this->defaultLocation . '/' . $information['file_location'];

                $filesInPath = null;
                try {
                    $filesInPath = $finder->files()->in($path);
                } catch (\InvalidArgumentException $e) {
                    $logger->error($e->getMessage());
                    continue;
                }

                /**
                 * @var SplFileInfo $file
                 */
                foreach ($filesInPath->ignoreDotFiles(true)->ignoreUnreadableDirs()->name('*.csv')->depth('== 0') as $file) {
                    if (null !== $csv && $csv !== $file->getFilename()) {
                        continue;
                    }

                    $this->registerCsvOrder($company, $file);

                    $delimiter = $config['fallback_delimiter'];

                    if (!empty($information['delimiter'])) {
                        $delimiter = $information['delimiter'];
                    }

                    $reader = IOFactory::createReader('Csv');

                    if ($reader instanceof Csv) {
                        $reader->setDelimiter($delimiter);
                    }

                    $sheet = $reader->load($file->getRealPath());
                    $lines = $sheet->getActiveSheet()->toArray();
                    $keys = $lines[0];

                    foreach ($lines as $key => $line) {

                        if ($key === 0) {
                            foreach ($line as $num => $value) {
                                $keys[$num] = trim($value);
                            }

                            continue;
                        }

                        $csvArray[$companyId][$file->getFilename()][] = array_combine($keys, $line);
                    }
                }

                $finder = null;
            }
        }

        return $csvArray;

    }

    /**
     * @param Company $company
     * @param SplFileInfo $file
     *
     * @throws OptimisticLockException
     * @throws \RuntimeException
     */
    private function registerCsvOrder(Company $company, SplFileInfo $file)
    {
        $csvOrder = new CsvOrder();
        $csvOrder->setFileName($file->getFilename());
        $csvOrder->setCompany($company);

        if (!$this->getDryRun()) {
            $this->entityManager->persist($csvOrder);
            $this->entityManager->flush();
        }
    }

    /**
     * @param array $csv
     * @param int $companyId
     * @param string $fileName
     *
     * @return Cart
     * @throws \Exception
     */
    public function handleCsv($csv, int $companyId, $fileName): Cart
    {
        $this->fileName = $fileName;
        $this->company = $this->entityManager->getRepository(Company::class)->find($companyId);
        $errors = [];

        $this->cartService->setAutoFlush(false);
        $this->entityManager->getConnection()->beginTransaction();
        try {
            if (null === $this->company) {
                throw new CsvImportException(sprintf('Company with ID: %s not found', $companyId));
            }

            $cart = $this->createCart($this->company);
            $this->cartService->setCart($cart);

            foreach ($csv as $csvRow => $orderData) {
                try {
                    if (null === current($orderData)) {
                        continue;
                    }

                    $cartOrder = $this->createCartOrder($orderData);
                    $this->cartService->setCurrentOrder($cartOrder);

                    $product = null;
                    $card = null;
                    $personalization = null;
                    $productOptions = [];

                    if(!preg_match('/\d{2}-\d{2}-\d{4}/', $orderData['Bezorgdatum'])) {
                        throw new CsvImportException('Invalid deliverydate, use format: dd-mm-YYYY');
                    }

                    $date = DateTime::createFromFormat('d-m-Y', $orderData['Bezorgdatum']);
                    $dateErrors = DateTime::getLastErrors();
                    if ($dateErrors['warning_count'] + $dateErrors['error_count'] > 0) {
                        throw new CsvImportException('Invalid delivery date given');
                    }

                    if (!empty($orderData['ProductSKU'])) {
                        $product = $this->findPurchasableProduct(Product::class, $orderData['ProductSKU']);
                        $productOptions['quantity'] = $orderData['ProductAmount'];
                    } else {
                        throw new CsvImportException('Missing product in CSV');
                    }

                    if (!empty($orderData['KaartjeSKU'])) {
                        $card = $this->findPurchasableProduct(ProductCard::class, $orderData['KaartjeSKU']);
                        $productOptions['card_text'] = stripcslashes($orderData['KaartTekst']);
                        $productOptions['card_quantity'] = $orderData['KaartjeAmount'];
                    }

                    if (!empty($orderData['PersonalisatieSKU'])) {
                        $personalization = $this->findPurchasableProduct(Personalization::class,
                            $orderData['PersonalisatieSKU']);

//                        $personalization = $this->entityManager->getRepository(Personalization::class)->findOneBy(['sku' => $orderData['PersonalisatieSKU']]);

                        //TODO NOT FOUND CHECK ERROR

                        $metadata = [];
                        if (!empty($orderData['PersonalisatieTekst'])) {
                            $text = stripcslashes($orderData['PersonalisatieTekst']);

                            $image = null;
                            if (!empty($orderData['PersonalisatieBeeld'])) {
                                $image = $orderData['PersonalisatieBeeld'];
                            }

                            $metadata = $this->generatePersonalization($text, $image);
                        } elseif (!empty($orderData['PersonalisatieBeeld'])
                            && strpos($orderData['PersonalisatieBeeld'], 'http') !== false) {
                            $metadata = $this->designGenerator->generateImagePersonalization($orderData['PersonalisatieBeeld']);
                        }

                        $productOptions['personalization_complete_meta'] = $metadata;
                    }

                    $this->cartService->addProduct($product, $card, $personalization, $productOptions);

                } catch (CsvImportException | ProcessDesignException | CartInvalidSupplierCombinationException $e) {
                    $errors[$csvRow] = $e->getMessage();
                }
            }

            if (!empty($errors)) {
                $errorString = '';
                foreach ($errors as $errorNumber => $error) {
                    $errorString .= sprintf("Fout op regel %d: \t\t\t %s", $errorNumber + 2, $error) . PHP_EOL;
                }
                throw new CsvImportException($errorString);
            }

            /** @var Order $order */
            foreach ($cart->getOrders() as $order) {
                if ($order->getLines()->count() === 0) {
                    throw new \RuntimeException('Cart order cannot be empty');
                }
            }

            if (!$this->getDryRun()) {
                $this->cartService->finalize();
                /** @var Order $order */
                foreach ($cart->getOrderCollection()->getOrders() as $index => $order) {
                    $order->setInvoiceReference($csv[$index]['ReferentieNummer'] ?: '');
                }
                $this->addPayment($cart->getCompany(), $cart->getOrder());
                $this->entityManager->flush();
            }

            $this->moveFileTo(self::STATUS_ARCHIVE, $cart);
            $this->entityManager->getConnection()->commit();
        } catch (CsvImportException | PDOException | DriverException $e) {
            $this->entityManager->getConnection()->rollBack();
            if ($e instanceof DriverException) {
                $e = $e->getPrevious();
            }
            $this->sendMail(self::STATUS_ERROR, $e->getMessage());

            if (!$this->getDryRun()) {
                $this->moveFileTo(self::STATUS_ERROR, $cart);
            }

            throw new \RuntimeException('Process of CSV file failed: ' . $e->getMessage());
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            throw $e;
        }

        return $cart;
    }


    /**
     * @param string $productClass
     * @param string $sku
     *
     * @return Product|null
     */
    private function findPurchasableProduct(string $productClass, string $sku): ?Product
    {
        $product = null;
        if ($productClass === Product::class || is_subclass_of($productClass, Product::class)) {
            $product = $this->entityManager->getRepository(Product::class)->findOneBy(['sku' => $sku]);
        }

        if (null === $product) {
            throw new CsvImportException(sprintf('Product with SKU: %s not found', $sku));
        }

        if ($productClass instanceof Product && !$product->getPurchasable()) {
            throw new CsvImportException(sprintf('Product %s is not purchasable', $sku));
        }

        return $product;
    }

    /**
     * @param Company $company
     *
     * @return Cart
     */
    private function createCart(Company $company): Cart
    {
        $cart = new Cart();

        $cart->setCompany($company);
        $cart->setInvoiceAddress($company->getInvoiceAddress());
        $cart->setSite($this->entityManager->getRepository(Site::class)->findOneBy(['theme' => 'toptaarten']));
        $cart->setCustomer($company->getCustomers()->first());

        return $cart;
    }

    /**
     * @param array $orderData
     *
     * @return CartOrder
     * @throws \Exception
     */
    private function createCartOrder(array $orderData): CartOrder
    {
        $timestamp = strtotime(str_replace('/', '-', $orderData['Bezorgdatum']));
        $datetime = new \DateTime(date('Y-m-d H:i:s', $timestamp));

        if (new \Datetime() > $datetime) {
            throw new CsvImportException('Delivery date has already passed');
        }

        $cartOrder = $this->cartService->createOrder();
        $cartOrder->setDeliveryDate($datetime);
        $cartOrder->setDeliveryRemark($orderData['Opmerking'] ?? '');
        $cartOrder->setDeliveryAddressAttn($orderData['NaamOntvanger']);
        $cartOrder->setDeliveryAddressStreet($orderData['StraatOntvanger']);

        $addressNumberParts = [];
        if (preg_match('/([0-9]+)[ -\/]?(.*)/', $orderData['HuisnummerOntvanger'], $addressNumberParts,
            PREG_UNMATCHED_AS_NULL)) {
            $cartOrder->setDeliveryAddressNumber($addressNumberParts[1]);
            $cartOrder->setDeliveryAddressNumberAddition($addressNumberParts[2]);
        } else {
            // fallback
            $cartOrder->setDeliveryAddressNumber($orderData['HuisnummerOntvanger']);
        }

        $cartOrder->setDeliveryAddressPostcode($orderData['PostcodeOntvanger']);
        $cartOrder->setDeliveryAddressCity($orderData['PlaatsOntvanger']);
        $cartOrder->setDeliveryAddressCountry($this->entityManager->getRepository(Country::class)->find('NL'));

        return $cartOrder;
    }

    /**
     * @param $type
     * @param $message
     *
     * @return int
     */
    private function sendMail($type, $message): int
    {
        if ($type === self::STATUS_ERROR) {
            foreach ($this->parameterBag->get('csv_error_emails') as $emailAddress) {
                $message = $this->generateErrorMessage($emailAddress, $message);

                $this->mailer->send($message);
            }
        }
        return 0;
    }

    /**
     * @param $emailAddress
     * @param $message
     *
     * @return \Swift_Message
     */
    private function generateErrorMessage($emailAddress, $message): \Swift_Message
    {
        $path = $this->getPath();
        $config = $this->getConfig();

        return (new \Swift_Message(sprintf('Foutieve CSV bestelling van %s', $this->company->getName())))
            ->setFrom($config['mail_from'])
            ->setTo($emailAddress)->attach(Swift_Attachment::fromPath(sprintf('%s/%s/%s',
                $this->parameterBag->get('csv_location'),
                $path, $this->fileName)))
            ->setBody(sprintf(
                    'Er is een fout opgetreden bij het inladen van nieuwe bestelling via de CSV import.' . PHP_EOL .
                    'Graag contact opnemen met de klant.' . PHP_EOL . PHP_EOL .
                    'Import bestand: %s' . PHP_EOL .
                    'Bedrijf: %s' . PHP_EOL .
                    'Bericht: ' . PHP_EOL . '%s' . PHP_EOL,
                    $this->fileName,
                    $this->company->getName(),
                    $message)
            );
    }

    /**
     * @return null|string
     */
    private function getPath(): ?string
    {
        $config = $this->getConfig();

        if (!empty($config['companies'][$this->company->getId()]['file_location'])) {
            return $config['companies'][$this->company->getId()]['file_location'];
        }

        return null;
    }

    /**
     * @param      $type
     * @param Cart $cartCollection
     *
     * @return bool|string
     */
    private function moveFileTo($type, Cart $cartCollection)
    {
        if (!$this->getDryRun()) {
            $csvOrder = $this->entityManager->getRepository(CsvOrder::class)->findOneBy(['fileName' => $this->fileName]);

            if (null === $csvOrder) {
                throw new \RuntimeException('No csv order found');
            }

            $csvOrder->setStatus(self::STATUS_ARCHIVE);

            $csvOrder->addCart($cartCollection);
            $fileSystem = new Filesystem();

            $defaultPath = $this->parameterBag->get('csv_location');

            $path = sprintf('%s/%s/%s', $defaultPath, $this->getPath(), $this->fileName);

            $newPath = sprintf('%s/%s/%s/%s', $defaultPath, $this->getPath(), ucfirst($type), $this->fileName);

            try {
                $fileSystem->rename($path, $newPath, true);

                return true;
            } catch (\RuntimeException $e) {
                return $e->getMessage();
            }
        }

        return true;
    }


    /**
     * Gets the config from config.yml at the csv location
     * @return array
     */
    private function getConfig(): array
    {
        if (null === $this->config) {
            $this->defaultLocation = $this->parameterBag->get('csv_location');

            if (null !== $this->defaultLocation) {

                $finder = new Finder();

                /**
                 * @var \SplFileObject $file
                 */
                foreach ($finder->in($this->defaultLocation)->name('config.yml')->files() as $file) {
                    $this->config = Yaml::parse(file_get_contents($file->getRealPath()));
                }
            }
            if(null === $this->config){
                throw new \RuntimeException('Csv import can not find / read the config.yml');
            }
        }

        return $this->config;
    }

    /**
     * @param $dryRun
     *
     * @return CsvService
     */
    public function setDryRun($dryRun): CsvService
    {
        $this->dryRun = $dryRun;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDryRun(): bool
    {
        return $this->dryRun;
    }

    /**
     * @param Company $company
     * @param OrderCollection $orderCollection
     *
     * @throws OptimisticLockException
     * @throws \Exception
     */
    private function addPayment(Company $company, OrderCollection $orderCollection)
    {
        $paymentMethod = $this->entityManager->getRepository(Paymentmethod::class)->findOneBy(['code' => 'invoice']);
        $config = $this->getConfig();

        if (isset($config['companies'][$company->getId()]['payment_method'])) {
            $paymentMethod = $this->entityManager->getRepository(Paymentmethod::class)->findOneBy(['code' => $config['companies'][$company->getId()]['payment_method']]);
        }

        if (false === $company->getVerified()) {
            throw new CsvImportException('Credit invoicing is not allowed for this customer');
        }

        if ($orderCollection->getPayments()->isEmpty()) {
            $payment = new Payment();
            $payment->setPaymentmethod($paymentMethod);
            $payment->setOrder($orderCollection);
            $payment->setDatetime(new \Datetime());
            $payment->setAmount($orderCollection->getPaymentAmount());
            //TODO change when workflow is implemented
            $payment->setStatus($this->entityManager->getRepository(PaymentStatus::class)->findOneBy(['id' => 'pending']));

            $this->entityManager->persist($payment);
            $this->entityManager->flush();

            //TODO change when workflow is implemented
            $payment->setStatus($this->entityManager->getRepository(PaymentStatus::class)->findOneBy(['id' => 'authorized']));
        }
    }

    /**
     * @param      $text
     * @param null $imageUrl
     *
     * @return array
     * @throws ProcessDesignException
     */
    private function generatePersonalization($text, $imageUrl = null): array
    {
        $config = $this->getConfig();

        $companyConfig = $config['companies'][$this->company->getId()];

        $font = 'Jura-Regular';
        if (isset($companyConfig['designer']['font'])) {
            $font = $companyConfig['designer']['font'];
        }

        $fontPath = $this->parameterBag->get('csv_location') . '/fonts/' . $font . '.ttf';

        if (!file_exists($fontPath)) {
            throw new CsvImportException('Personalisation font not found.');
        }

        $imageWidth = isset($companyConfig['designer']['image_width']) ? (float)$companyConfig['designer']['image_width'] : 1;
        $imageHeight = isset($companyConfig['designer']['image_height']) ? (float)$companyConfig['designer']['image_height'] : 1;

        $sourceFilesPath = $this->parameterBag->get('csv_location') . '/' . $this->getPath();

        return $this->designGenerator->generatePersonalization($text, $sourceFilesPath, $fontPath, $imageWidth,
            $imageHeight, $imageUrl);
    }

}
