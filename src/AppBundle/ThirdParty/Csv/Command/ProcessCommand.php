<?php

namespace AppBundle\ThirdParty\Csv\Command;

use AppBundle\ThirdParty\Csv\Services\CsvService;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Class ProcessCommand
 * @Job(minute="21,51", hour="8-19")
 */
class ProcessCommand extends Command
{
    /**
     * @var CsvService
     */
    private $csvService;

    /**
     * @param CsvService  $csvService
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     */
    public function __construct(CsvService $csvService, ?string $name = null)
    {
        parent::__construct($name);
        $this->csvService = $csvService;
    }

    /**
     * Sets the command name to use in the console
     */
    protected function configure()
    {
        $this->setName('csv:process')
            ->addArgument('csv', InputArgument::OPTIONAL, 'Find a single CSV file by name and process it')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);
        $this->csvService->setDryRun($input->getOption('dry-run'));
        $csvArray = $this->csvService->readCsv($logger, $input->getArgument('csv'));
        $errors = false;

        foreach ($csvArray as $companyId => $file) {
            foreach ($file as $fileName => $csv) {
                try {
                    $this->csvService->handleCsv($csv, $companyId, $fileName);
                    $logger->info('File (' . $fileName . '): Processed successfully');
                } catch (\Exception $e) {
                    $logger->error('File (' . $fileName . '): ' . $e->getMessage());
                    $errors = true;
                }
            }
        }

        if($input->getOption('dry-run')){
            $logger->warning('Dry run enabled');
        }

        if ($errors === false) {
            $logger->info('CSVs processed');
            return 0;
        }

        $logger->info('CSVs processed with errors');
        return 1;
    }
}
