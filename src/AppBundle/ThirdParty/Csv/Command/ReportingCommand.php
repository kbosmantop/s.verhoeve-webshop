<?php

namespace AppBundle\ThirdParty\Csv\Command;

use AppBundle\ThirdParty\Csv\Entity\CsvOrder;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class ReportingCommand
 */
class ReportingCommand extends Command
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * CsvService constructor.
     * @param EntityManagerInterface $entityManager
     * @param Swift_Mailer           $mailer
     * @param ParameterBagInterface  $parameterBag
     * @param string|null            $name
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Swift_Mailer $mailer,
        ParameterBagInterface $parameterBag,
        ?string $name = null
    ) {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        parent::__construct($name);

    }

    /**
     * Sets the command name to use in the console
     */
    protected function configure()
    {
        $this
            ->setName('csv:report')
            ->addArgument('date', InputArgument::OPTIONAL, 'Use a specific date to generate a report');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);
        $logger->info('Reporting triggered');

        $dateStart = date('Y-m-d 00:00:00');
        $dateEnd = date('Y-m-d 23:59:59');
        
        if (null !== $input->getArgument('date')) {
            $dateStart = new \DateTime(date('Y-m-d 00:00:00', strtotime($input->getArgument('date'))));
            $dateEnd = new \DateTime(date('Y-m-d 23:59:59', strtotime($input->getArgument('date'))));
        }

        $logger->info('Gathering data');

        $csvOrderRepository = $this->entityManager->getRepository(CsvOrder::class);

        $queryBuilder = $csvOrderRepository->createQueryBuilder('co');
        $queryBuilder->where($queryBuilder->expr()->between('co.createdAt', ':date_start', ':date_end'));
        $queryBuilder->andWhere('co.status = :status');
        $queryBuilder->setParameters([
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'status' => 'archive',
        ]);

        /** @var CsvOrder[] $csvOrders */
        $csvOrders = $queryBuilder->getQuery()->getResult();

        $text = null;

        $files = [];

        foreach ($csvOrders as $csvOrder) {
            if (!in_array($csvOrder->getFileName(), $files, true)) {
                $text .= sprintf('CSV Bestand: %s', $csvOrder->getFileName() . PHP_EOL);

                $text .= sprintf('Met de volgende bestelling(en): %s', PHP_EOL . PHP_EOL);

                foreach ($csvOrder->getCarts() as $cart) {
                    foreach ($cart->getOrders() as $cartOrder) {
                        $text .= sprintf('Referentie number: %s',
                            $cartOrder->getMetaData()['ReferenceNumber'] . PHP_EOL);
                    }

                    $text .= sprintf('Bestelnummer: %s', $cart->getOrder()->getNumber() . PHP_EOL . PHP_EOL);
                }
            }
        }

        if ($text !== null) {
            $logger->info('sending emails');

            foreach ($this->parameterBag->get('csv_report_emails', []) as $emailAddress) {
                $message = (new \Swift_Message(sprintf('Dag rapportage %s', $dateStart)))
                    ->setFrom('csv-import@topgeschenken.nl')
                    ->setTo($emailAddress)
                    ->setBody($text);

                $this->mailer->send($message);
            }

            return 0;
        }

        $logger->info('No mails pending');

        return 1;
    }
}
