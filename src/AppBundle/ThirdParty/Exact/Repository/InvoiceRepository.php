<?php

namespace AppBundle\ThirdParty\Exact\Repository;

use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class InvoiceRepository
 */
class InvoiceRepository extends EntityRepository
{
    /**
     * @param Company $company
     * @return int
     */
    public function getExportedInvoiceCountForCompany(?Company $company): int
    {
        if($company === null){
            return 0;
        }
        $qb = $this->createQueryBuilder('invo');
        $qb->select($qb->expr()->count('invo.id'));
        $qb->where('invo.company = :company');
        $qb->setParameter('company', $company);
        try {
            $count = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            return 0;
        }
        return (int)$count;
    }

    /**
     * @param InvoiceExport $invoiceExport
     * @return array
     */
    public function findByInvoiceExport(InvoiceExport $invoiceExport)
    {
        $qb = $this->createQueryBuilder('invo');
        $qb->select('invo');
        $qb->where('invo.invoiceExport = :export');
        $qb->setParameter('export', $invoiceExport);
        return $qb->getQuery()->getResult();
    }
}