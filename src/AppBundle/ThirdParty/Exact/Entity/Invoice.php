<?php

namespace AppBundle\ThirdParty\Exact\Entity;

use AppBundle\Entity\Order\BillingItemGroup;
use AppBundle\Entity\Relation\Company;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\ThirdParty\Exact\Repository\InvoiceRepository")
 * @ORM\Table("exact_invoice")
 */
class Invoice
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private $company;

    /**
     * @var InvoiceExport
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\ThirdParty\Exact\Entity\InvoiceExport")
     * @ORM\JoinColumn(nullable=false)
     */
    private $invoiceExport;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    protected $invoiceDate;


    /**
     * @var string|null
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $externalRef;

    /**
     * @var Collection|InvoiceLine[]
     */
    private $lines;

    /**
     * @var Collection|BillingItemGroup[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\BillingItemGroup", mappedBy="invoice", cascade={"remove"})
     */
    private $billingItemGroups;

    public function __construct()
    {
        $this->lines = new ArrayCollection();
        $this->billingItemGroups = new ArrayCollection();
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Invoice
     */
    public function setCompany(Company $company): Invoice
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @return InvoiceExport
     */
    public function getInvoiceExport(): InvoiceExport
    {
        return $this->invoiceExport;
    }

    /**
     * @param InvoiceExport $invoiceExport
     */
    public function setInvoiceExport(InvoiceExport $invoiceExport): void
    {
        $this->invoiceExport = $invoiceExport;
    }

    /**
     * @return DateTime
     */
    public function getInvoiceDate(): DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @param DateTime $invoiceDate
     */
    public function setInvoiceDate(DateTime $invoiceDate): void
    {
        $this->invoiceDate = $invoiceDate;
    }

    /**
     * @return string
     */
    public function getExternalRef(): string
    {
        return $this->externalRef ?: '';
    }

    /**
     * @param string $externalRef
     */
    public function setExternalRef(string $externalRef): void
    {
        $this->externalRef = $externalRef;
    }

    /**
     * @return InvoiceLine[]|Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param InvoiceLine $invoiceLine
     */
    public function addLine(InvoiceLine $invoiceLine): void
    {
        if (!$this->lines->contains($invoiceLine)) {
            $this->lines->add($invoiceLine);
        }
    }

    /**
     * @return float
     */
    public function getTotalPriceValue(): float
    {
        $totalPriceValue = 0;

        foreach ($this->lines as $line) {
            $totalPriceValue += $line->getPriceValue() * $line->getQuantity();
        }

        return $totalPriceValue;
    }

    /**
     * @return BillingItemGroup[]|Collection
     */
    public function getBillingItemGroups()
    {
        return $this->billingItemGroups;
    }

    /**
     * @param BillingItemGroup $billingItemGroup
     */
    public function addBillingItemGroup(BillingItemGroup $billingItemGroup)
    {
        if(!$this->billingItemGroups->contains($billingItemGroup)) {
            $billingItemGroup->setInvoice($this);
            $this->billingItemGroups->add($billingItemGroup);
        }
    }

    /**
     * @param BillingItemGroup $billingItemGroup
     */
    public function removeBillingItemGroup(BillingItemGroup $billingItemGroup)
    {
        if($this->billingItemGroups->contains($billingItemGroup)) {
            $this->billingItemGroups->removeElement($billingItemGroup);
        }
    }
}