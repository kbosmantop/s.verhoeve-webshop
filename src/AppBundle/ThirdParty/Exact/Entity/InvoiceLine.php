<?php

namespace AppBundle\ThirdParty\Exact\Entity;

use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Order\OrderLine;

/**
 * Class InvoiceLine
 * @package AppBundle\ThirdParty\Exact\Entity
 */
class InvoiceLine
{
    /**
     * @var string
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $itemCode;

    /**
     * @var integer
     */
    protected $quantity;

    /**
     * @var float
     */
    protected $priceValue;

    /**
     * @var string
     */
    protected $vatCode;

    /**
     * @var OrderCollectionLine
     */
    private $orderCollectionLine;

    /**
     * @var OrderLine
     */
    private $orderLine;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getItemCode(): ?string
    {
        return $this->itemCode;
    }

    /**
     * @param string|null $itemCode
     */
    public function setItemCode(?string $itemCode): void
    {
        $this->itemCode = $itemCode;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float|null
     */
    public function getPriceValue(): ?float
    {
        return $this->priceValue;
    }

    /**
     * @param float $priceValue
     */
    public function setPriceValue(float $priceValue): void
    {
        $this->priceValue = $priceValue;
    }

    /**
     * @return string
     */
    public function getVatCode(): string
    {
        return $this->vatCode;
    }

    /**
     * @param string $vatCode
     */
    public function setVatCode(string $vatCode): void
    {
        $this->vatCode = $vatCode;
    }

    /**
     * @param OrderCollectionLine|null $orderCollectionLine
     * @return InvoiceLine
     */
    public function setOrderCollectionLine(?OrderCollectionLine $orderCollectionLine): InvoiceLine
    {
        $this->orderCollectionLine = $orderCollectionLine;
        $this->orderLine = null;
        return $this;
    }

    /**
     * @return OrderCollectionLine|null
     */
    public function getOrderCollectionLine(): ?OrderCollectionLine
    {
        return $this->orderCollectionLine;
    }

    /**
     * @param OrderLine|null $orderLine
     * @return InvoiceLine
     */
    public function setOrderLine(?OrderLine $orderLine): InvoiceLine
    {
        $this->orderCollectionLine = null;
        $this->orderLine = $orderLine;
        return $this;
    }

    /**
     * @return OrderLine|null
     */
    public function getOrderLine(): ?OrderLine
    {
        return $this->orderLine;
    }
}