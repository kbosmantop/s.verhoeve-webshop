<?php

namespace AppBundle\ThirdParty\Exact\Xml;

use AppBundle\Interfaces\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sabre\Xml as Sabre;

/**
 * Class AbstractExactAdapter
 */
abstract class ExactAbstractAdapter implements ExactXmlInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Sabre\Service $sabre
     */
    protected $sabre;

    /**
     * @var EntityInterface
     */
    protected $object;

    /**
     * AbstractAdapter constructor.
     */
    public function __construct()
    {
        $this->sabre = new Sabre\Service();
    }

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $object
     */
    public function setObject(object $object)
    {
        $this->object = $object;
    }

    /**
     * @param Sabre\Writer $writer
     */
    public function generateXmlHeader(Sabre\Writer $writer)
    {
        $writer->writeAttributes([
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:noNamespaceSchemaLocation' => 'eExact-Schema.xsd',
        ]);
    }
}
