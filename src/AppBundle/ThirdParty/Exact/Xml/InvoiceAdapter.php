<?php

namespace AppBundle\ThirdParty\Exact\Xml;

use AppBundle\ThirdParty\Exact\Entity\Invoice;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use Exception;
use LogicException;
use Sabre\Xml as Sabre;


/**
 * Class InvoiceAdapter
 * @package AppBundle\ThirdParty\Exact\Xml
 */
class InvoiceAdapter extends ExactAbstractAdapter
{
    /**
     * @var InvoiceExport $object
     */
    protected $object;

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return InvoiceExport::class;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateXml()
    {
        $invoiceRepository = $this->entityManager->getRepository(Invoice::class);
        $invoices = $invoiceRepository->findByInvoiceExport($this->object);

        return $this->_generateXml($invoices);
    }

    /**
     * @internal
     * @param array $invoices
     * @return string
     */
    public function _generateXml(array $invoices){
        return $this->sabre->write('eExact', function (Sabre\Writer $writer) use ($invoices) {
            $this->generateXmlHeader($writer);
            $writer->writeElement('Invoices', static function() use ($writer, $invoices){

                /** @var Invoice $invoice */
                foreach($invoices as $invoice){
                    $company = $invoice->getCompany();
                    if($company === null){
                        throw new LogicException(sprintf('Company is null on Invoice %d', $invoice->getId()));
                    }
                    $companyInvoiceSettings = $company->getCompanyInvoiceSettings();
                    if($companyInvoiceSettings === null){
                        throw new LogicException(sprintf('CompanyInvoiceSettings is null on Invoice %d and Company %d', $invoice->getId(), $company->getId()));
                    }

                    $writer->writeElement('Invoice', static function() use ($writer, $invoice, $company, $companyInvoiceSettings){
                        // UITLEG V=Verkoopfactuur 12=herkenning voor Geschenkbezorgen
                        $writer->writeAttributes([
                            'code' => '20',
                            'type' => 'V'
                        ]);

                        $writer->writeElement('Description', 'Topgeschenken Factuur');

                        $writer->writeElement('YourRef', $invoice->getExternalRef());

                        $writer->writeElement('Currency', static function() use ($writer){
                            $writer->writeAttributes([
                                'code' => 'EUR',
                            ]);
                        });

                        $writer->writeElement('OrderedBy', static function() use ($writer, $company){

                            $writer->writeElement('Debtor', static function() use ($writer, $company){
                                $writer->writeAttributes([
                                    'code' => $company->getExactRelationNumber(),
                                ]);
                            });
                        });

                        $writer->writeElement('DeliveredTo', static function() use ($writer, $company){

                            $writer->writeElement('Debtor', static function() use ($writer, $company){
                                $writer->writeAttributes([
                                    'code' => $company->getExactRelationNumber(),
                                ]);
                            });
                        });

                        $writer->writeElement('InvoicedTo', static function() use ($writer, $company){

                            $writer->writeElement('Debtor', static function() use ($writer, $company){
                                $writer->writeAttributes([
                                    'code' => $company->getExactRelationNumber(),
                                ]);
                            });

                            $writer->writeElement('Address', static function() use ($writer, $company){
                                $address = $company->getInvoiceAddress();
                                $streetAndNumber = '';
                                if($address !== null){
                                    $streetAndNumber = $address->getStreet() . ' ' . $address->getNumber() . (!empty($address->getNumberAddition())? $address->getNumberAddition(): '' );
                                }

                                $writer->writeElement('Addressee', static function() use ($writer, $address){

                                    $writer->writeElement('Name', ($address !== null)? $address->getAttn(): '');
                                });

                                $writer->writeElement('AddressLine1', ($address !== null)? $streetAndNumber: '');

                                $writer->writeElement('PostalCode', ($address !== null)? $address->getPostcode(): '');

                                $writer->writeElement('City', ($address !== null)? $address->getCity(): '');

                                $writer->writeElement('Country', ($address !== null)? $address->getCountry()->getCode(): '');
                            });
                        });

                        $writer->writeElement('PaymentCondition', $companyInvoiceSettings->getPaymentTerm());

                        $writer->writeElement('Selection', static function() use ($writer){
                            //UITLEG gebruikt om type facturen te definiëren voor verwerking in Exact.
                            $writer->writeAttributes([
                                'code' => '20',
                            ]);
                        });

                        $writer->writeElement('InvoiceGroup', static function() use ($writer){
                            //UITLEG net als invoice type code.
                            $writer->writeAttributes([
                                'code' => '12',
                            ]);
                        });

                        foreach($invoice->getLines() as $lineNo => $invoiceLine) {
                            $writer->writeElement('InvoiceLine', static function () use ($writer, $invoiceLine, $lineNo) {
                                $writer->writeAttributes([
                                    'lineNo' => (string)($lineNo + 1),
                                ]);

                                $writer->writeElement('Description', $invoiceLine->getDescription());

                                $writer->writeElement('Text', $invoiceLine->getDescription());

                                //Text line
                                if($invoiceLine->getPriceValue() === null){
                                    return;
                                }

                                $writer->writeElement('Item', static function () use ($writer, $invoiceLine) {
                                    $writer->writeAttributes([
                                        'code' => (string)$invoiceLine->getItemCode(),
                                    ]);
                                });

                                $writer->writeElement('Quantity', $invoiceLine->getQuantity());

                                $writer->writeElement('Price', static function () use ($writer, $invoiceLine) {
                                    //UITLEG S=standard (vaste waarde)
                                    $writer->writeAttributes([
                                        'type' => 'S',
                                    ]);

                                    $writer->writeElement('Currency', static function () use ($writer) {
                                        $writer->writeAttributes([
                                            'code' => 'EUR',
                                        ]);
                                    });

                                    $writer->writeElement('Value', $invoiceLine->getPriceValue());

                                    $writer->writeElement('VAT', static function () use ($writer, $invoiceLine) {
                                        $writer->writeAttributes([
                                            'code' => $invoiceLine->getVatCode(),
                                        ]);
                                    });
                                });

                                $writer->writeElement('Delivery', static function () use ($writer, $invoiceLine) {
                                    $deliveryDate = '';
                                    if(($invoiceLine->getOrderLine() !== null) && $invoiceLine->getOrderLine()->getOrder()->getDeliveryDate() !== null) {
                                        $deliveryDate = $invoiceLine->getOrderLine()->getOrder()->getDeliveryDate()->format('Y-m-d');
                                    }
                                    $writer->writeElement('Date', $deliveryDate);
                                });

                                $writer->writeElement('Project', static function () use ($writer, $invoiceLine) {
                                    $orderNumber = '';
                                    if($invoiceLine->getOrderCollectionLine() !== null){
                                        $orderNumber = $invoiceLine->getOrderCollectionLine()->getOrderCollection()->getNumber();
                                    }
                                    if($invoiceLine->getOrderLine() !== null){
                                        $orderNumber = $invoiceLine->getOrderLine()->getOrder()->getOrderNumber();
                                    }
                                    $writer->writeAttributes([
                                        'code' => $orderNumber,
                                    ]);
                                });
                            });
                        }
                    });
                }
            });
        });
    }
}
