<?php

namespace AppBundle\ThirdParty\Copernica\Adapters;

/**
 * Class AbstractCopernicaAdapter
 * @package AppBundle\ThirdParty\Copernica\Adapters
 */
abstract class AbstractCopernicaAdapter
{
    /**
     * @var string $identifier
     */
    protected $identifier;

    /**
     * @var string $database
     */
    protected $database;

    /**
     * @var mixed Entity id
     */
    protected $id;

    /**
     * AbstractCopernicaAdapter constructor.
     * @param string $database
     * @param mixed    $id
     * @param string $identifier
     */
    public function __construct(string $database, $id, string $identifier)
    {
        $this->database = $database;
        $this->id = $id;
        $this->identifier = $identifier;
    }

    /**
     * @return array
     */
    abstract protected function createRemoteObject();

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }
}