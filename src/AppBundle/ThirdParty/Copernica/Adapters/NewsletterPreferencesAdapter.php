<?php

namespace AppBundle\ThirdParty\Copernica\Adapters;

use AppBundle\Entity\Security\Customer\NewsletterPreferences;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;

/**
 * Class NewsletterPreferencesAdapter
 * @package AppBundle\ThirdParty\Copernica\Adapters
 */
class NewsletterPreferencesAdapter extends AbstractCopernicaAdapter
{
    /**
     * @var NewsletterPreferences $newsletterPreferences
     */
    private $newsletterPreferences;

    /**
     * NewsletterPreferencesAdapter constructor.
     * @param NewsletterPreferences $newsletterPreferences
     */
    public function __construct(NewsletterPreferences $newsletterPreferences)
    {
        $this->newsletterPreferences = $newsletterPreferences;
        parent::__construct(CopernicaService::DB_NEWSLETTER_PREFRENCES, $newsletterPreferences->getId(),
            'newsletter_id');
    }

    /**
     * @return array
     */
    public function createRemoteObject()
    {
        $profile = [];
        $profile['newsletter_id'] = $this->newsletterPreferences->getId();
        if ($this->newsletterPreferences->getCustomer()) {
            $profile['customer_id'] = $this->newsletterPreferences->getCustomer()->getId() ?? null;
            $profile['contact'] = $this->newsletterPreferences->getCustomer()->getMetadata()['externalCopernicaCustomerId'] ?? null;
        } else {
            $profile['customer_id'] = null;
            $profile['contact'] = null;
        }
        $profile['name'] = $this->newsletterPreferences->getName();
        $profile['email'] = $this->newsletterPreferences->getEmail();

        return $profile;
    }

    /**
     * @return array
     */
    public function createPreferences()
    {
        $preferences = [];
        $preferences['no_mail'] = $this->newsletterPreferences->getTypeNoMail();
        $preferences['newsletters'] = $this->newsletterPreferences->getTypeNewsletters();
        $preferences['discount_actions'] = $this->newsletterPreferences->getTypeDiscountActions();
        $preferences['research'] = $this->newsletterPreferences->getTypeResearch();
        $preferences['topbloemen_nl'] = $this->newsletterPreferences->getSiteTopbloemen();
        $preferences['topgeschenken_nl'] = $this->newsletterPreferences->getSiteTopgeschenken();
        $preferences['toptaart_nl'] = $this->newsletterPreferences->getSiteToptaart();
        $preferences['topfruit_nl'] = $this->newsletterPreferences->getSiteTopfruit();
        $preferences['suppliers_nl'] = $this->newsletterPreferences->getSiteSuppliers();
        return $preferences;
    }
}