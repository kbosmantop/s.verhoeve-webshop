<?php

namespace AppBundle\ThirdParty\Copernica\Adapters;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use libphonenumber\PhoneNumberFormat;

/**
 * Class CustomerAdapter
 * @package AppBundle\ThirdParty\Copernica\Adapters
 */
class CustomerAdapter extends AbstractCopernicaAdapter
{
    /**
     * @var Customer $customer
     */
    private $customer;

    /**
     * CustomerAdapter constructor.
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
        parent::__construct(CopernicaService::DB_CONTACTS, $customer->getUuid(), 'customer_id');
    }

    /**
     * @return array
     */
    public function createRemoteObject()
    {
        $profile = [];
        $profile['customer_id'] = $this->customer->getUuid();
        $profile['gender'] = $this->customer->getGender();
        $profile['name'] = $this->customer->getFullname();
        $profile['dateOfBirth'] = (null !== $this->customer->getBirthday()) ? $this->customer->getBirthday()->format('Y-m-d') : '';
        $profile['firstname'] = $this->customer->getFirstname();
        $profile['prepposition'] = $this->customer->getLastnamePrefix();
        $profile['lastname'] = $this->customer->getLastname();
        $profile['email'] = $this->customer->getEmail();
        $profile['phone'] = $this->customer->getFormattedPhoneNumber(PhoneNumberFormat::E164);
        $profile['mobile'] = $this->customer->getFormattedMobileNumber(PhoneNumberFormat::E164);

        if (!$this->customer->getCompany()) {
            $profile['company_id'] = null;
            $profile['company'] = null;
        } else {
            $profile['company_id'] = $this->customer->getCompany()->getUuid();
            $profile['company'] = $this->customer->getCompany()->getMetadata()['externalCopernicaCompanyId'] ?? null;
        }

        $profile['facebook_id'] = $this->customer->getFacebookId();
        $profile['customer_group'] = $this->customer->getCustomerGroup() !== null ? $this->customer->getCustomerGroup()->getDescription() : '';

        $profile['last_login'] = $this->customer->getLastLogin() ? $this->customer->getLastLogin()->format('Y-m-d H:i:s') : null;
        $profile['created_at'] = $this->customer->getCreatedAt()->format('Y-m-d H:i:s');
        $profile['updated_at'] = $this->customer->getUpdatedAt()->format('Y-m-d H:i:s');
        $profile['deleted_at'] = $this->customer->getDeletedAt() ? $this->customer->getDeletedAt()->format('Y-m-d H:i:s') : null;

        return $profile;
    }
}