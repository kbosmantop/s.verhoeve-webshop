<?php

namespace AppBundle\ThirdParty\Copernica\Adapters;

use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;

/**
 * Class CompanyAdapter
 * @package AppBundle\ThirdParty\Copernica\Adapters
 */
class CompanyAdapter extends AbstractCopernicaAdapter
{
    /**
     * @var Company $company
     */
    private $company;

    /**
     * CompanyAdapter constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
        parent::__construct(CopernicaService::DB_COMPANIES, $company->getUuid(), 'company_id');
    }

    /**
     * @return array
     */
    public function createRemoteObject()
    {
        $profile = [];
        $profile['company_id'] = $this->company->getUuid();
        $profile['company_name'] = $this->company->getName();
        $profile['chamber_of_commerce'] = $this->company->getChamberOfCommerceNumber();

        return $profile;
    }
}