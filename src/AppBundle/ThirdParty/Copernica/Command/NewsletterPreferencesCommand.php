<?php

namespace AppBundle\ThirdParty\Copernica\Command;

use AppBundle\Entity\Security\Customer\NewsletterPreferences;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class NewsletterPreferencesCommand
 * @package AppBundle\ThirdParty\Copernica\Command
 */
class NewsletterPreferencesCommand extends Command
{

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var CopernicaService $copernicaService
     */
    private $copernicaService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('copernica:update:newsletterPreferences')
            ->setDescription('Command to send new or existing newsletterPreferences to copernica')
            ->addArgument('id', InputArgument::REQUIRED, 'NewsletterPreferences identifier');
    }

    /**
     * NewsletterPreferencesCommand constructor.
     * @param EntityManager    $entityManager
     * @param CopernicaService $copernicaService
     */
    public function __construct(EntityManager $entityManager, CopernicaService $copernicaService)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->copernicaService = $copernicaService;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
            $newsletterPreferencesId = (int)$input->getArgument('id');
            $newsletterPreferences = $this->entityManager->getRepository(NewsletterPreferences::class)->find($newsletterPreferencesId);

            if (null === $newsletterPreferences) {
                throw new CopernicaNotFoundException(sprintf('Cannot find newsletterPreferences with id %s',
                    $newsletterPreferencesId));
            }

            $externalRef = $this->copernicaService->updateNewsletterPreferences($newsletterPreferences);
            $output->write(sprintf('Copernica id %s (internal id %s) has been inserted or updated', $externalRef,
                $newsletterPreferencesId));
            return 0;
    }
}
