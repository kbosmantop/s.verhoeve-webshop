<?php

namespace AppBundle\ThirdParty\Copernica\Command;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class OrderLineCommand
 * @package AppBundle\ThirdParty\Copernica\Command
 */
class OrderLineCommand extends Command
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var CopernicaService $copernicaService
     */
    private $copernicaService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('copernica:update:orderline')
            ->setDescription('Command to send new or existing order line to copernica')
            ->addArgument('id', InputArgument::REQUIRED, 'OrderLine identifier');
    }

    /**
     * OrderLineCommand constructor.
     * @param EntityManagerInterface    $entityManager
     * @param CopernicaService $copernicaService
     */
    public function __construct(EntityManagerInterface $entityManager, CopernicaService $copernicaService)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->copernicaService = $copernicaService;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $orderLineId = (int)$input->getArgument('id');
        $orderLine = $this->entityManager->getRepository(OrderLine::class)->find($orderLineId);

        if (null === $orderLine) {
            throw new CopernicaNotFoundException(sprintf('Cannot find orderLine with id %s', $orderLineId));
        }

        $externalRef = $this->copernicaService->updateCopernicaOrderLine($orderLine);
        $output->write(sprintf('Copernica id %s (internal id %s) has been inserted or updated', $externalRef,
            $orderLineId));

        $filter->reenableFilter();
        return 0;
    }
}
