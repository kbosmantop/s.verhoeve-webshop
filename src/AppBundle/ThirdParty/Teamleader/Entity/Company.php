<?php

namespace AppBundle\ThirdParty\Teamleader\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("teamleader_company")
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $company;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $data;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $syncedAt;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Company
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return Company
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Relation\Company $company
     *
     * @return Company
     */
    public function setCompany(\AppBundle\Entity\Relation\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Relation\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return Company
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }
}
