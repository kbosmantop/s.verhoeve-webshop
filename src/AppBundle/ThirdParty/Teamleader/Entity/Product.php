<?php

namespace AppBundle\ThirdParty\Teamleader\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("teamleader_product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    protected $product;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $data;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $syncedAt;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Catalog\Product\Product $product
     *
     * @return Product
     */
    public function setProduct(\AppBundle\Entity\Catalog\Product\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Catalog\Product\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return Product
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return Product
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }
}
