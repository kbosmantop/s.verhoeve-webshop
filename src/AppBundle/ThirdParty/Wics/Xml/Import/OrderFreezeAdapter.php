<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;

/**
 * Class OrderFreezeAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Import
 */
class OrderFreezeAdapter extends AbstractAdapter implements ImportXmlInterface
{
    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return (string)$this->xml->xpath('//OrderFreeze/ExternalReference')[0];
    }
}
