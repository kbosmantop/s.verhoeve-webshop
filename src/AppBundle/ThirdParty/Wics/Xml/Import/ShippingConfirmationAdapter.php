<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;

/**
 * Class ShippingConfirmationAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Import
 */
class ShippingConfirmationAdapter extends AbstractAdapter implements ImportXmlInterface
{
    /**
     * @return \SimpleXMLElement[]
     */
    public function getOrderStatuses() : array
    {
        return $this->xml->xpath('//orderStatus');
    }
}
