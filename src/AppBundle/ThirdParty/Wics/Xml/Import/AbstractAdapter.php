<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

/**
 * Class AbstractAdapter
 */
abstract class AbstractAdapter
{
    /**
     * @var \SimpleXMLElement
     */
    protected $xml;

    /**
     * AbstractAdapter constructor.
     * @param string $xml
     */
    public function __construct(string $xml)
    {
        $this->xml = new \SimpleXMLElement($xml);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return (string)$this->xml->xpath("//Type")[0];
    }

    /**
     * @return int
     */
    public function getMessageNumber()
    {
        return (int)$this->xml->xpath("//MessageNo")[0]->__toString();
    }

    /**
     * @return \DateTime
     */
    public function getDateTime()
    {
        return new \DateTime((string)$this->xml->xpath("//Date")[0] . "  " . (string)$this->xml->xpath("//Time")[0]);
    }
}
