<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;

/**
 * Class StoreConfirmationAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Import
 */
class StoreConfirmationAdapter extends AbstractAdapter implements ImportXmlInterface
{
    /**
     * @return string
     */
    public function getPurchaseReceipt()
    {
        return $this->xml->xpath("//PurchaseReceipt");
    }

    /**
     * @return string
     */
    public function getCustomerReference()
    {
        return (string)$this->xml->xpath("//CustReference")[0];
    }
}
