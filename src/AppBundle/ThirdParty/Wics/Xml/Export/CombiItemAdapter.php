<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sabre\Xml as Sabre;

/**
 * Class CombiItemAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Export
 */
class CombiItemAdapter extends AbstractAdapter implements ExportXmlInterface
{
    /**
     * @var string
     */
    protected $type = 'CombiItem';

    /**
     * @var Product
     */
    protected $product;

    /**
     * ItemAdapter constructor.
     * @param $object
     * @throws \Exception
     */
    public function __construct($object)
    {
        parent::__construct();

        if (!$object instanceof Product) {
            throw new \InvalidArgumentException('Object must be an instance of Product');
        }


        $this->product = $object;
    }

    /**
     * @return string
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function generateXml(): string
    {
        $xml = $this->sabre->write('Message', function (Sabre\Writer $writer) {
            $this->generateXmlHeader($writer);

            $writer->writeElement('Item', function () use ($writer) {
                $title = $this->product->translate()->getTitle();
                if ($title === '') {
                    $title = $this->product->getParent()->translate()->getTitle();
                }

                $writer->writeElement('ItemNum', $this->product->getSku());
                $writer->writeElement('ItemDesc', $title);
                $writer->writeElement('SupplierCode', $this->product->getSku());
                $writer->writeElement('ProductGroup', $this->product->getProductgroup()->getSkuPrefix());

                $writer->writeElement('Combination', function () use ($writer) {

                    $items = [];
                    /** @var Combination $combination */
                    foreach ($this->product->getCombinations() as $combination) {
                        if (isset($items[$combination->getProduct()->getSku()])) {
                            $items[$combination->getProduct()->getSku()] += $combination->getQuantity();
                        } else {
                            $items[$combination->getProduct()->getSku()] = $combination->getQuantity();
                        }
                    }

                    foreach ($items as $sku => $amount) {
                        $writer->writeElement('CombinationItem', function () use ($writer, $sku, $amount) {
                            $writer->writeElement('CombinationItemNum', $sku);
                            $writer->writeElement('CombinationItemQuantity', $amount);
                        });
                    }
                });

            });
        });

        $this->setMessageXml($xml);

        return $xml;
    }
}
