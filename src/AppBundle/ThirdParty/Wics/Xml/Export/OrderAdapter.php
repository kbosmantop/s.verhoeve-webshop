<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\ProductPackagingUnit;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use Sabre\Xml as Sabre;

/**
 * Class OrderAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Export
 */
class OrderAdapter extends AbstractAdapter implements ExportXmlInterface
{
    public const INSTRUCTION_PICKUP = 'Pickup';
    public const INSTRUCTION_CARD = 'HasCard';
    public const INSTRUCTION_LETTER = 'HasLetter';
    public const INSTRUCTION_DESIGN = 'HasDesign';

    /**
     * @var SupplierOrder $supplierOrder
     */
    private $supplierOrder;

    /**
     * @var WicsService $wics
     */
    private $wics;

    /**
     * @var string $type
     */
    protected $type = 'Order';

    /**
     * OrderAdapter constructor.
     * @param $object
     * @throws \Exception
     */
    public function __construct($object)
    {
        parent::__construct();

        if (!$object instanceof SupplierOrder) {
            throw new \RuntimeException('Invalid request');
        }

        $this->supplierOrder = $object;
    }

    /**
     * @param WicsService $wics
     * @return $this
     */
    public function setWics(WicsService $wics)
    {
        $this->wics = $wics;

        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateXml()
    {
        $wicsService = $this->wics;

        return $this->sabre->write('Message', function (Sabre\Writer $writer) use ($wicsService) {
            $this->generateXmlHeader($writer);

            $writer->writeElement('orders', function () use ($writer, $wicsService) {
                $writer->writeElement('order', function () use ($writer, $wicsService) {
                    $deliveryDate = $this->supplierOrder->getOrder()->getDeliveryDate();

                    if (null === $deliveryDate) {
                        $deliveryDate = new \DateTime('tomorrow');
                        if ($deliveryDate->format('N') === '7') {
                            $deliveryDate->add(new \DateInterval('P1D'));
                        }
                    }

                    $deliveryDateFormatted = $deliveryDate->format('Y-m-d');

                    $writer->writeElement('OrderType', $wicsService->getOrderTypeCode($this->supplierOrder));
                    $writer->writeElement('DeliveryDate', $deliveryDateFormatted);
                    $writer->writeElement('ReqShipDate', $deliveryDateFormatted);
                    $writer->writeElement('ExternalReference', $this->supplierOrder->getOrder()->getOrderNumber());
                    $writer->writeElement('ExternalReference2', '');

                    if (null !== $this->supplierOrder->getOrder()->getPickupAddress()) {
                        $writer->writeElement('CustomerName', $this->supplierOrder->getOrder()->getOrderCollection()->getInvoiceAddressAttn() ?? '');
                        $writer->writeElement('CustomerContactName', $this->supplierOrder->getOrder()->getOrderCollection()->getInvoiceAddressCompanyName() ?? '');
                    } else {
                        $name = $this->supplierOrder->getOrder()->getDeliveryAddressAttn() ?? '';
                        foreach($this->supplierOrder->getOrder()->getMetadata() as $meta){
                            if(isset($meta['label'], $meta['id'], $meta['value'])){
                                $name .= sprintf(', %s: %s', $meta['label'], $meta['value']);
                            }
                        }
                        $writer->writeElement('CustomerName', $name);
                        $writer->writeElement('CustomerContactName', $this->supplierOrder->getOrder()->getDeliveryAddressCompanyName() ?? '');
                    }

                    $writer->writeElement('Notes', $this->supplierOrder->getOrder()->getDeliveryRemark());
                    if (null !== $this->supplierOrder->getOrder()->getPickupAddress()) {
                        $writer->writeElement('Instructions', self::INSTRUCTION_PICKUP);
                    }
                });

                $writer->writeElement('Address', function () use ($writer) {

                    $writer->writeElement('AddressType', 'Delivery');

                    if (null !== ($pickupAddress = $this->supplierOrder->getOrder()->getPickupAddress())) {
                        $writer->writeElement('Address', $pickupAddress->getStreet());
                        $writer->writeElement('HouseNumber', $pickupAddress->getNumber());
                        $writer->writeElement('HouseNumberExt', $pickupAddress->getNumberAddition());
                        $writer->writeElement('Address2', '');
                        $writer->writeElement('Zipcode', $pickupAddress->getPostcode());
                        $writer->writeElement('City', $pickupAddress->getCity());
                        $writer->writeElement('State', '');
                        $writer->writeElement('Country', $pickupAddress->getCountry()->getCode());
                    } else {
                        $writer->writeElement('Address', $this->supplierOrder->getOrder()->getDeliveryAddressStreet());
                        $writer->writeElement('HouseNumber', $this->supplierOrder->getOrder()->getDeliveryAddressNumber());
                        $writer->writeElement('HouseNumberExt', $this->supplierOrder->getOrder()->getDeliveryAddressNumberAddition());
                        $writer->writeElement('Address2', '');
                        $writer->writeElement('Zipcode', $this->supplierOrder->getOrder()->getDeliveryAddressPostcode());
                        $writer->writeElement('City', $this->supplierOrder->getOrder()->getDeliveryAddressCity());
                        $writer->writeElement('State', '');
                        $writer->writeElement('Country', $this->supplierOrder->getOrder()->getDeliveryAddressCountry()->getCode());
                    }

                    $writer->writeElement('Email',
                        ($this->supplierOrder->getOrder()->getOrderCollection()->getCustomer() ? $this->supplierOrder->getOrder()->getOrderCollection()->getCustomer()->getEmail() : ''));
                    $writer->writeElement('PhoneNumber', $this->supplierOrder->getOrder()->getDeliveryAddressPhoneNumber());
                });

                $writer->writeElement('OrderLines', function () use ($writer) {
                    $i = 1;

                    /** @var SupplierOrderLine $line */
                    foreach ($this->supplierOrder->getLines() as $line) {
                        if (null === $line->getConcludedSupplierProduct()->getProduct()->getSku()) {
                            throw new \RuntimeException('Undefined SKU for product: ' . $line->getConcludedSupplierProduct()->getProduct()->translate()->getTitle());
                        }

                        if ($line->getConcludedSupplierProduct()->getProduct() instanceof GenericProduct) {
                            if ($line->getConcludedSupplierProduct()->getProduct()->getParent() === null) {
                                continue;
                            }

                            $writer->writeElement('OrderLine', function () use ($line, $writer, $i) {
                                $writer->writeElement('LineNumber', $i);
                                $writer->writeElement('ItemNum',
                                    $line->getConcludedSupplierProduct()->getProduct()->getSku());
                                $writer->writeElement('QtyOrdered', $line->getQuantity());

                                if (!$line->getConcludedSupplierProduct()->getProduct()->isCombination()) {
                                    /** @var ProductPackagingUnit[] $packagingUnits */
                                    $packagingUnits = $line->getConcludedSupplierProduct()->getProduct()->getProductPackagingUnits()->toArray();

                                    if (empty($packagingUnits)) {
                                        throw new \RuntimeException(sprintf('Geen verpakkingseenheid gevonden voor product %s',
                                            $line->getConcludedSupplierProduct()->getProduct()->getSku()));
                                    }

                                    if (\count($packagingUnits) > 1) {
                                        usort($packagingUnits,
                                            function (ProductPackagingUnit $a, ProductPackagingUnit $b) {
                                                return $a->getQuantity() <=> $b->getQuantity();
                                            });
                                    }

                                    /** @var ProductPackagingUnit $packageingUnit */
                                    if (null !== ($packageingUnit = $packagingUnits[0])) {
                                        $writer->writeElement('StockUnit',
                                            $packageingUnit->getPackagingUnit()->getCode());
                                    }
                                }

                                if (null !== $line->getOrderLine()->getChildren()) {
                                    $attachments = [];
                                    foreach ($line->getOrderLine()->getChildren() as $childLine) {
                                        $concludedProduct = $childLine->getProduct();

                                        if($concludedProduct->isCard()){
                                            $attachments[] = 'Kaartje';
                                        }
                                        if($concludedProduct->isPersonalization()){
                                            $attachments[] = 'Personalisatie';
                                        }
                                        if($concludedProduct->isLetter()){
                                            $attachments[] = 'Brief';
                                        }
                                    }
                                    if(!empty($attachments)){
                                        $instruction = sprintf('Let op: dit product bevat de volgende bijlagen: %s.', implode(', ', $attachments));
                                        $writer->writeElement('Instructions', $instruction);
                                    }
                                }

                                $writer->writeElement('Notes', '');
                            });

                            $i++;
                        }
                    }
                });

                $writer->writeElement('transport', function () use ($writer) {
                    if ($this->supplierOrder->getOrder()->getPickupAddress() === null) {
                        $carrier = $this->wics->getCarrierFromSupplierOrder($this->supplierOrder);
                        $deliveryMethod = $this->wics->getDeliveryMethodFromSupplierOrder($this->supplierOrder);
                        $writer->writeElement('carrier', $this->wics->getWicsCarrierId($carrier));
                        $writer->writeElement('carrierOptions', $this->wics->getWicsCarrierOption($deliveryMethod));
                        $writer->writeElement('carrierDeliveryOptions', '');
                    } else {
                        $writer->writeElement('carrier', $this->wics->getWicsPickupCarrierId());
                        $writer->writeElement('carrierOptions', $this->wics->getWicsPickupCarrierOption());
                        $writer->writeElement('carrierDeliveryOptions', '');
                    }
                });
            });
        });
    }
}
