<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use Sabre\Xml as Sabre;

/**
 * Class PreAlertAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Export
 */
class PreAlertAdapter extends AbstractAdapter implements ExportXmlInterface
{
    /**
     * @var array $preAlerts
     */
    private $preAlerts;

    /**
     * @var string $type
     */
    protected $type = 'PreAlert';

    /**
     * ExportXmlInterface constructor.
     * @param $object
     * @throws \Exception
     */
    public function __construct($object)
    {
        throw new \Exception('Not implemented');
//        if ($object instanceof PreAlert) {
//            throw new \Exception('Invalid request');
//        }

        $this->preAlerts = $object;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function generateXml()
    {
        return $this->sabre->write('PreAlerts', function (Sabre\Writer $writer) {
            $this->generateXmlHeader($writer);

            foreach ($this->preAlerts as $preAlert) {
                $writer->writeElement('PreAlert', function () use ($writer, $preAlert) {
                    $writer->writeElement('ReceiveDate', '');
                    $writer->writeElement('CustReference', '');
                    $writer->writeElement('PreAlertLines', function () use ($writer, $preAlert) {
                        /**
                         * @var OrderLine $line
                         */
                        foreach ($preAlert->getOrder()->getLines() as $line) {
                            $writer->writeElement('PreAlertLine', function () use ($writer, $line) {
                                $writer->writeElement('LineNum', $line->getId());
                                $writer->writeElement('ItemNum', $line->getProduct()->getId());
                                $writer->writeElement('Quantity', $line->getQuantity());
                                # Todo: get the StockUnit
                                $writer->writeElement('StockUnit', 'ST');
                            });
                        }
                    });
                });
            }
        });
    }
}
