<?php

namespace AppBundle\ThirdParty\Wics\Interfaces;

/**
 * Interface ImportXmlInterface
 * @package AppBundle\ThirdParty\Wics\Interfaces
 */
interface ImportXmlInterface
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @return int
     */
    public function getMessageNumber();

    /**
     * @return \DateTime
     */
    public function getDateTime();
}
