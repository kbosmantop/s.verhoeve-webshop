<?php

namespace AppBundle\ThirdParty\Wics\Command\Import;

use AppBundle\ThirdParty\Wics\Factory\ImportFactory;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StockLevelCommand
 * @package AppBundle\ThirdParty\Wics\Command
 */
class StockLevelCommand extends Command
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var WicsService $wicsService
     */
    private $wicsService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('wics:import:stockLevel')
            ->setDescription('Command to process stock level imports from WICS')
            ->addArgument('filePath', InputArgument::REQUIRED, 'File path of XML to process');
    }

    /**
     * StockLevelCommand constructor.
     * @param EntityManagerInterface $em
     * @param WicsService   $wicsService
     */
    public function __construct(EntityManagerInterface $em, WicsService $wicsService)
    {
        parent::__construct();

        $this->em = $em;
        $this->wicsService = $wicsService;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return integer
     * @throws \Exception
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $path = $input->getArgument('filePath');
        $xml = $this->wicsService->read($path);

        $hasError = false;

        try {
            $this->wicsService->handleStockLevel(ImportFactory::createStockLevel($xml));
        } catch (\Exception $e) {
            $hasError = true;
            throw $e;
        } finally {
            $this->wicsService->move($path, !$hasError);
        }

        $filter->reenableFilter();
        return 0;
    }
}
