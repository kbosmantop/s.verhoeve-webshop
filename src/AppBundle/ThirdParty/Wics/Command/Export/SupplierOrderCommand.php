<?php

namespace AppBundle\ThirdParty\Wics\Command\Export;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use AppBundle\Traits\CommandContextTrait;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SupplierOrderCommand
 * @package AppBundle\ThirdParty\Wics\Command
 */
class SupplierOrderCommand extends ContainerAwareCommand
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var WicsService $wicsService
     */
    private $wicsService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('wics:export:supplier-order')
            ->setDescription('Export supplierOrder to WICS WMS')
            ->addArgument('supplierOrderId', InputArgument::REQUIRED, 'SupplierOrder identifier');
    }

    /**
     * SupplierOrderCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     * @param WicsService            $wicsService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        WicsService $wicsService
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->wicsService = $wicsService;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     * @throws OptimisticLockException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * USAGE
         * while read ORDER ; do php bin/console wics:export:supplierOrder "$((ORDER))" --no-debug; done < <((echo {5..12} | xargs -n1) 2>/dev/null)
         * WHERE {5..12} is the range
         */
        throw new \RuntimeException('HARDCODE DISABLE');

        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $supplierOrderId = $input->getArgument('supplierOrderId');

        $supplierOrderId = (int)$supplierOrderId;

        /**
         * @var SupplierOrder $supplierOrder
         */
        $supplierOrder = current($this->entityManager->getRepository(SupplierOrder::class)->findBy([
            'id' => $supplierOrderId,
            'connector' => 'Wics',
        ]));

        if (!$supplierOrder) {
            throw new \RuntimeException(sprintf('Cannot find supplierOrder with id %s', $supplierOrderId));
        }

        $this->defineAdminContext();

        try {
            $this->wicsService->exportOrder($supplierOrder);
            $this->logger->info(sprintf('Synchronized supplierOrder to WICS: %d', $supplierOrder->getId()));
        } catch (\RuntimeException $e) {
            $this->logger->error($e->getMessage(), [
                'exception' => $e,
            ]);
            return 1;
        } finally {
            $filter->reenableFilter();
        }

        return 0;
    }
}
