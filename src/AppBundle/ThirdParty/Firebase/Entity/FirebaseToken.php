<?php

namespace AppBundle\ThirdParty\Firebase\Entity;

use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class FirebaseToken
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $token;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     */
    protected $user;

    /**
     * Set token
     *
     * @param string $token
     *
     * @return FirebaseToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return FirebaseToken
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Security\Employee\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
