<?php

namespace AppBundle\Event;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CustomerEvent
 *
 * @package App\Event
 */
class CustomerEvent extends UserEvent
{
    /** @var array */
    private $payload;

    /**
     * CustomerEvent constructor.
     *
     * @param UserInterface $user
     * @param null|Request  $request
     * @param array         $payload
     */
    public function __construct(UserInterface $user, ?Request $request = null, array $payload = [])
    {
        $this->payload = $payload;

        parent::__construct($user, $request);
    }

    /**
     * @return UserInterface
     */
    public function getCustomer()
    {
        return $this->getUser();
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }
}