<?php

namespace AppBundle\Command\Finance;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AdminBundle\Services\Finance\InvoiceExportService;
use AppBundle\DBAL\Types\ExactInvoiceExportStatusType;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use Recurr\Exception;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InvoiceExportGeneratorCommand
 */
class InvoiceExportGeneratorCommand extends Command
{
    use LockableTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var InvoiceExportService
     */
    private $invoiceExportGenerator;

    /**
     * InvoiceExportCommand constructor.
     * @param EntityManagerInterface        $entityManager
     * @param InvoiceExportGeneratorService $invoiceExportGenerator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        InvoiceExportGeneratorService $invoiceExportGenerator
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->invoiceExportGenerator = $invoiceExportGenerator;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:finance:invoice-export-generator')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addOption('rerun', '', InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->lock(null, true);

        void($output);

        $invoiceExport = $this->entityManager->getRepository(InvoiceExport::class)->find($input->getArgument('id'));

        if (!$invoiceExport) {
            throw new RuntimeException(sprintf('No invoice export entity found with id %d',
                $input->getArgument('id')));
        }

        if ($invoiceExport->getProcessedAt() === null || $input->hasOption('rerun')) {
            try {
                $this->invoiceExportGenerator->generate($invoiceExport);
                $invoiceExport->setExportStatus(ExactInvoiceExportStatusType::EXPORTED);
                $invoiceExport->setProcessedAt(new DateTime());
                $this->entityManager->flush();
            } catch (Exception $e) {
                $invoiceExport->setExportStatus(ExactInvoiceExportStatusType::FAILED);
                $this->entityManager->flush();
                throw $e;
            }
        } else {
            throw new RuntimeException('Invoice export has already been executed');
        }

        return 0;
    }
}