<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Mailer\OrderCancellationMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendOrderCancellationCommand
 * @package AppBundle\Command
 */
class SendOrderCancellationCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderCancellationMailer
     */
    private $orderCancellationMailer;

    /**
     * SendConfirmationCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param OrderCancellationMailer $orderCancellationMailer
     * @param null|string $name
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        OrderCancellationMailer $orderCancellationMailer,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->orderCancellationMailer = $orderCancellationMailer;
    }

    protected function configure()
    {
        $this
            ->setName('app:send-order-cancellation-mail')
            ->setDescription('Send order cancelled mail')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $order = $this->entityManager->find(Order::class, $input->getArgument('id'));

        if (null === $order) {
            return 1;
        }

        $orderCollection = $order->getOrderCollection();

        $this->defineContext($orderCollection->getSite());

        $this->orderCancellationMailer->send(['orderCollection' => $orderCollection]);

        return 0;
    }
}
