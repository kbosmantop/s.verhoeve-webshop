<?php

namespace AppBundle\Command\Sync;

use AppBundle\Services\Sync\Florists;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FloristsCommand
 * @package AppBundle\Command\Sync
 *
 * @Job(minute="14", hour="5")
 */
class FloristsCommand extends Command
{
    /**
     * @var Florists
     */
    private $floristSyncService;

    protected function configure()
    {
        $this->setName('sync:florists');
    }

    /**
     * FloristsCommand constructor.
     * @param Florists $floristSyncService
     * @param null     $name
     */
    public function __construct(Florists $floristSyncService, $name = null)
    {
        parent::__construct($name);

        $this->floristSyncService = $floristSyncService;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->floristSyncService->setOutput($output)->import();

        return 0;
    }
}
