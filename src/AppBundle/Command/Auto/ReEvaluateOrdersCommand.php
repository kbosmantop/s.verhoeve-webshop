<?php

namespace AppBundle\Command\Auto;

use AppBundle\Entity\Order\Order;
use AppBundle\Services\Auto\OrderEvaluatorService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProcessOrderCommand
 * @package AppBundle\Command\Auto
 */
class ReEvaluateOrdersCommand extends Command
{

    /**
     * @var OrderEvaluatorService
     */
    private $orderEvaluatorService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ReEvaluateOrdersCommand constructor.
     * @param OrderEvaluatorService  $orderEvaluatorService
     * @param EntityManagerInterface $entityManager
     * @param string|null            $name
     */
    public function __construct(OrderEvaluatorService $orderEvaluatorService, EntityManagerInterface $entityManager, ?string $name = null)
    {
        parent::__construct($name);
        $this->orderEvaluatorService = $orderEvaluatorService;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('auto:reevaluate-orders')
            ->setDescription('Re-evaluate orders from a certain date for auto order')
            ->addArgument('fromDate', InputArgument::REQUIRED)
            ->addArgument('tillDate', InputArgument::OPTIONAL)
            ->addOption('dry-run', null, InputOption::VALUE_NONE)
            ->addOption('no-processing', null, InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fromDate = $input->getArgument('fromDate');
        $tillDate = $input->getArgument('tillDate');

        $dryRun = $input->getOption('dry-run');

        if ($dryRun && $output->getVerbosity() < OutputInterface::VERBOSITY_VERBOSE) {
            $output->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
        }

        $process = !$input->getOption('no-processing');

        try {
            $fromDate =  new DateTime($fromDate);

            if($tillDate) {
                $tillDate = new DateTime($tillDate);
            }
        } catch(\Exception $e) {
            throw new \InvalidArgumentException('De opgegeven datum(s) zijn niet geldig.');
        }

        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o')
            ->leftJoin('o.status', 'os')
            ->andWhere('o.deliveryDate >= :fromDate')
            ->andWhere('o.forceManual = false')
            ->andWhere('os.processable = true')
            ->setParameter('fromDate', $fromDate);

        if ($tillDate) {
            $qb->andWhere('o.deliveryDate <= :tillDate')
                ->setParameter('tillDate', $tillDate);
        }

        $orders = $qb->getQuery()->getResult();

        foreach ($orders as $order) {
            $this->orderEvaluatorService
                ->setOutput($output)
                ->process($order, $dryRun, $process, false);
        }

        return 0;
    }

}