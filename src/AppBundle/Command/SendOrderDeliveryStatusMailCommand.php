<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\Order;
use AppBundle\Services\Mailer\OrderDeliveryStatusMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendDeliveryStatusMailCommand
 * @package AppBundle\Command
 */
class SendOrderDeliveryStatusMailCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var OrderDeliveryStatusMailer
     */
    private $orderDeliveryStatusMailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SendConfirmationCommand constructor.
     * @param OrderDeliveryStatusMailer $orderDeliveryStatusMailer
     * @param EntityManagerInterface    $entityManager
     * @param null|string               $name
     */
    public function __construct(
        OrderDeliveryStatusMailer $orderDeliveryStatusMailer,
        EntityManagerInterface $entityManager,
        ?string $name = null
    ) {
        parent::__construct($name);

        $this->orderDeliveryStatusMailer = $orderDeliveryStatusMailer;

        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('app:mailer:send-delivery-status')
            ->setDescription('Send delivery status for order')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $order = $this->entityManager->find(Order::class, $input->getArgument('id'));

        $orderCollection = $order->getOrderCollection();

        $this->defineContext($orderCollection->getSite());

        $this->orderDeliveryStatusMailer->send([
            'order' => $order
        ]);
    }
}