<?php

namespace AppBundle\Command\Report;

use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Manager\Report\CompanyReportCustomerManager;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CompanyReportCustomerSendReportCommand
 * @package AppBundle\Command\Report
 */
class CompanyReportCustomerSendReportCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var CompanyReportCustomerManager
     */
    protected $companyReportCustomerManager;

    /**
     * CompanyReportCustomerSendReportCommand constructor.
     * @param EntityManagerInterface       $entityManager
     * @param CompanyReportCustomerManager $companyReportCustomerManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CompanyReportCustomerManager $companyReportCustomerManager
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->companyReportCustomerManager = $companyReportCustomerManager;
    }

    protected function configure()
    {
        $this
            ->setName('app:company-report-customer:send-report')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $companyReportCustomerId = $input->getArgument('id');

        /** @var CompanyReportCustomer|null $companyReportCustomer */
        $companyReportCustomer = $this->entityManager
            ->getRepository(CompanyReportCustomer::class)
            ->find($companyReportCustomerId);

        if (
            null !== $companyReportCustomer
            && null !== $site = $companyReportCustomer->getCustomer()->getRegisteredOnSite()
        ) {
            $this->defineContext($site);
            $this->companyReportCustomerManager->send($companyReportCustomer);
        }
    }
}
