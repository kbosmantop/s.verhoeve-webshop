<?php

namespace AppBundle\Command\Afterpay;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Manager\Order\OrderManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Padam87\CronBundle\Annotation\Job as Cron;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AutoCaptureCommand
 * @package AppBundle\Command\Afterpay
 *
 * @Cron(minute="0", hour="6")
 */
class AutoCaptureCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    /**
     * @var \DateTime
     */
    private $today;

    /**
     * AutoCaptureCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->today = new \DateTime();
        $this->today->setTime(0, 0, 0);
    }

    protected function configure()
    {
        $this
            ->setName('afterpay:auto-capture')
            ->addOption('dryrun', null, InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $formatter = $this->getHelper('formatter');

        /** @var Paymentmethod $afterpayPaymentMethod */
        $afterpayPaymentMethod = $this->getDoctrine()->getManager()->getRepository(Paymentmethod::class)->findOneBy([
            'code' => 'afterpay',
        ]);

        /** @var Payment[] $payments */
        $payments = $this->getDoctrine()->getManager()->getRepository(Payment::class)->findBy([
            'paymentmethod' => $afterpayPaymentMethod,
            'status' => 'authorized',
        ]);

        $afterpay = $this->getContainer()->get('app.gateway.afterpay');

        foreach ($payments as $payment) {
            $orderCollection = $payment->getOrderCollection();

            if (!$this->isInvoicable($orderCollection)) {
                continue;
            }

            if ($output->isVerbose()) {
                $output->writeln('Order ' . $orderCollection->getNumber());
            }

            try {
                $orderStatus = $afterpay->getOrderStatus($payment);

                if ($output->isVerbose()) {
                    if ($orderStatus->totalReservedAmount > 0) {
                        $output->writeln(' - nog te capturen');
                    } else {
                        $output->writeln(' - Reeds gecaptured');
                    }
                }

                if (!$input->getOption('dryrun')) {
                    $afterpay->capture($payment);
                }
            } catch (\Exception $e) {
                $output->writeln($formatter->formatBlock($e->getMessage(), 'error'));
            }
        }
    }

    /**
     * @param OrderCollection $orderCollection
     * @return bool
     * @throws \Exception
     */
    public function isInvoicable(OrderCollection $orderCollection)
    {
        foreach ($orderCollection->getOrders() as $order) {
            //@todo SF4
            if (!$this->getContainer()->get(OrderManager::class)->isInvoiceable($order)) {
                continue;
            }

            $delay = new \DateInterval('P' . $order->getOrderCollection()->getSite()->getAfterpayCaptureDelay() . 'D');

            if (!$order->getDeliveryDate()) {
                $datetime = (clone $order->getCreatedAt());
            } else {
                $datetime = (clone $order->getDeliveryDate());
            }

            if ($datetime->add($delay) < $this->today) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
