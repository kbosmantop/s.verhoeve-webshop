<?php

namespace AppBundle\Command\Fraud;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Fraud\OrderFraudCheckService;
use AppBundle\Services\JobManager;
use Doctrine\ORM\EntityManagerInterface;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FraudCheckCommand
 * @package AppBundle\Command\Fraud
 */
class FraudCheckCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var OrderFraudCheckService
     */
    private $orderFraudCheckService;

    /**
     * FraudCheckCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param JobManager             $jobManager
     * @param OrderFraudCheckService $orderFraudCheckService
     * @param string|null            $name
     */
    public function __construct(EntityManagerInterface $entityManager, JobManager $jobManager, OrderFraudCheckService $orderFraudCheckService, ?string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->jobManager = $jobManager;
        $this->orderFraudCheckService = $orderFraudCheckService;
    }

    protected function configure(): void
    {
        $this
            ->setName('topgeschenken:fraude:check')
            ->addArgument('id', InputArgument::OPTIONAL);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        //check if orderCollectionId is available, if not, create jobs for all new orders.
        $orderCollectionId = $input->getArgument('id');

        if (null !== $orderCollectionId) {
            /** @var OrderCollection $orderCollection */
            $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find($orderCollectionId);

            if (null === $orderCollection) {
                throw new \RuntimeException(sprintf('The orderCollection with ID "%s" does not exist',
                    $orderCollectionId));
            }

            $this->orderFraudCheckService->checkOrder($orderCollection);

            return 0;
        }

        //schedule all new orderCollections for fraud check
        $qb = $this->entityManager->getRepository(OrderCollection::class)->createQueryBuilder('oc')
            ->innerJoin('oc.orders', 'o')
            ->innerJoin('o.status', 'os')
            ->andWhere('oc.fraudScore is null')
            ->addGroupBy('oc.id')
            ->andHaving('MAX(os.fraudCheckable) = true');

        /** @var OrderCollection[] $orderCollections */
        $orderCollections = $qb->getQuery()->getResult();

        foreach ($orderCollections as $orderCollection) {
            $command = 'topgeschenken:fraude:check';

            $fraudJob = new Job($command, [$orderCollection->getId()]);
            $fraudJob->addRelatedEntity($orderCollection);

            $this->jobManager->addJob($fraudJob);
        }

        $this->entityManager->flush();

        return 0;
    }

}
