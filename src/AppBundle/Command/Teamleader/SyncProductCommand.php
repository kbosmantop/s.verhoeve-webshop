<?php

namespace AppBundle\Command\Teamleader;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityNotFoundException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncProductCommand
 * @package AppBundle\Command\Teamleader
 */
class SyncProductCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('teamleader:sync-product')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            try {
                $product = $this->getProduct((int)$input->getArgument('id'));
            } catch (EntityNotFoundException $e) {
                return 1;
            }

            $this->getContainer()->get('teamleader')->syncProduct($product);
        } else {
            $products = $this->getProducts();

            $progress = new ProgressBar($output, \count($products));
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < \count($products)) {
                try {
                    $this->getContainer()->get('teamleader')->syncProduct($products[$progress->getProgress()]);
                } catch (ClientException $e) {
                    if ($e->getCode() === 429) {
                        /** You can call the Teamleader API a maximum of 25 times every 5 seconds. */

                        sleep(5);
                        continue;
                    }

                    if ($output->isVerbose() && $e->getResponse() !== null) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$products[$progress->getProgress()]->getId()] = $result->reason;
                    }
                } catch (\Exception $e) {
                    if ($output->isVerbose()) {
                        $errors[$products[$progress->getProgress()]->getId()] = $e->getMessage();
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if ($errors &&  $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }

        return 0;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        /** @var ArrayCollection $products */
        $products = new ArrayCollection($this->getContainer()->get('doctrine')->getManager()->getRepository(Product::class)->findAll());
        $products = $products->filter(function (Product $product) {
            if (!$product->getVariations()->isEmpty()) {
                return false;
            }

            return true;
        });

        return array_values($products->toArray());
    }

    /**
     * @param int $id
     * @return Product
     * @throws EntityNotFoundException
     */
    public function getProduct(int $id) {
        $product = $this->getContainer()->get('doctrine')->getManager()->find(Product::class, $id);

        if (!$product) {
            throw new EntityNotFoundException(sprintf('Product with id %s', $id));
        }

        return $product;
    }
}