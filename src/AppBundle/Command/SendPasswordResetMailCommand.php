<?php

namespace AppBundle\Command;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Domain;
use AppBundle\Services\Mailer\PasswordResetMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SendPasswordResetMailCommand
 * @package AppBundle\Command
 */
class SendPasswordResetMailCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PasswordResetMailer
     */
    private $passwordResetMailer;

    /**
     * SendPasswordResetMailCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param PasswordResetMailer    $passwordResetMailer
     * @param RouterInterface        $router
     * @param null|string            $name
     */
    public function __construct(EntityManagerInterface $entityManager, PasswordResetMailer $passwordResetMailer, RouterInterface $router, ?string $name = null)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->passwordResetMailer = $passwordResetMailer;
    }

    protected function configure()
    {
        $this
            ->setName('topbloemen:site:send-password-reset-mail')
            ->setDescription('Send password reset email')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('domain', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        /** @var Customer $customer */
        $customer = $this->entityManager->find(Customer::class, $input->getArgument('id'));

        /** @var Domain $domain */
        $domain = $this->entityManager->find(Domain::class, $input->getArgument('domain'));

        $this->defineContext($domain->getSite());

        $this->passwordResetMailer->send([
            'customer' => $customer,
            'site' => $domain->getSite(),
        ]);
    }
}