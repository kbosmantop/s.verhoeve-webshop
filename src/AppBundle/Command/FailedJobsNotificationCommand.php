<?php

namespace AppBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Driver\PDOConnection;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Padam87\CronBundle\Annotation\Job as Cron;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class FailedJobsNotificationCommand
 * @package AppBundle\Command
 *
 * @Cron(minute="0", hour="6")
 */
class FailedJobsNotificationCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this->setName('app:failed-jobs-notification');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval('P1D'));

        $message = new \Swift_Message();
        $message->setSubject('Overview of failed jobs on ' . $date->format('l j F Y'));

        $body = '';

        $urlGenerator = $this->getUrlGenerator();

        foreach ($this->getFailedJobs($date) as $failedJob) {
            $url = $urlGenerator->generate('jms_jobs_overview', [
                'state' => 'failed',
                'command' => $failedJob['command']
            ], RouterInterface::ABSOLUTE_URL);

            $body .= $failedJob['count'] .' x <a href="'. $url .'">'. $failedJob['command'] .'</a> <br />';
        }

        $message->setBody($body, 'text/html');

        // TODO IN refactor aanpassen naar parameters
        $message->setFrom('noreply@topgeschenken.nl');
        $message->addTo('it@topgeschenken.nl');

        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @param \DateTime $date
     * @return Job[]
     */
    private function getFailedJobs(\DateTime $date)
    {
        /** @var PDOConnection $connection */
        $connection = $this->getDoctrine()->getConnection();

        $sql = " 
            SELECT command, COUNT(*) AS count
            FROM jms_jobs 
            WHERE state = 'failed'
              AND DATE(closedAt) = :date        
            GROUP BY command
        ";

        $stmt = $connection->prepare($sql);
        $stmt->execute([
            'date' => $date->format('Y-m-d')
        ]);

        return $stmt->fetchAll();
    }

    /**
     * @return Registry
     */
    private function getDoctrine() {
        return $this->container->get('doctrine');
    }

    /**
     * @return UrlGeneratorInterface
     */
    private function getUrlGenerator()
    {
        $context = $this->container->get('router')->getContext();
        $context->setScheme($this->getContainer()->getParameter('admin_scheme'));
        $context->setHost($this->getContainer()->getParameter('admin_host'));

        if ($context->getScheme() === 'https') {
            $context->setHttpsPort($this->getContainer()->getParameter('admin_port'));
        } else {
            $context->setHttpPort($this->getContainer()->getParameter('admin_port'));
        }

        return $this->getContainer()->get('router')->getGenerator();
    }
}