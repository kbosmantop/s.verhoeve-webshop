<?php

namespace AppBundle\Command\Discount;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\VoucherBatch;
use AppBundle\Services\Company\CompanyRegistrationService;
use AppBundle\Services\Discount\VoucherService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateVoucherBatchCommand
 * @package AppBundle\Command\Discount
 */
class GenerateVoucherBatchCommand extends Command
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GenerateVoucherBatchCommand constructor.
     * @param string|null            $name
     * @param VoucherService         $voucherService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(?string $name = null, VoucherService $voucherService, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);


        $this->voucherService = $voucherService;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('app:voucher:generate-batch')
            ->setDescription('Generate vouchers for the given batch')
            ->addArgument('id', InputArgument::REQUIRED, 'The ID of the batch');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchId = $input->getArgument('id');

        $batch = $this->entityManager->getRepository(VoucherBatch::class)->find($batchId);
        if(null !== $batch) {
            $this->voucherService->generateBatch($batch);
        }
    }
}
