<?php

namespace AppBundle\Command\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Services\Supplier\Bakery;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncBakeryCommand
 * @package AppBundle\Command\Supplier
 */
class SyncBakeryCommand extends Command
{
    /**
     * @var Bakery
     */
    private $bakery;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SyncBakeryCommand constructor.
     * @param Bakery                 $bakery
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Bakery $bakery, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->bakery = $bakery;
        $this->entityManager = $entityManager;

    }

    protected function configure()
    {
        $this
            ->setName('supplier:bakery:sync')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            $company = $this->getCompany((int)$input->getArgument('id'));

            $syncData = $this->bakery->sync($company);

            $output->writeln($syncData);

        } else {
            $companies = $this->getBakeries();

            $progress = new ProgressBar($output, \count($companies));
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < \count($companies)) {
                try {
                    $this->bakery->sync($companies[$progress->getProgress()]);
                } catch (ClientException $e) {
                    if ($output->isVerbose() && $e->getResponse() !== null) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$companies[$progress->getProgress()]->getId()] = $result->reason;
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if ($errors && $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }
    }

    /**
     * @return Company[]
     */
    public function getBakeries()
    {
        /** @var SupplierGroup $bakeryGroup */
        $bakeryGroup = $this->entityManager->getRepository(SupplierGroup::class)->findOneBy([
            'name' => 'Bakkers',
        ]);

        $bakeries = $this->entityManager->createQueryBuilder()->select('c')
            ->from(Company::class, 'c')
            ->where('c.isSupplier = 1')
            ->andWhere(':supplierGroupBakery MEMBER OF c.supplierGroups')
            ->setParameter('supplierGroupBakery', $bakeryGroup->getId())
            ->getQuery();

        /** @var ArrayCollection $companies */
        $companies = new ArrayCollection($bakeries->getResult());

        return array_values($companies->toArray());
    }

    /**
     * @param int $id
     * @return Company
     */
    public function getCompany($id)
    {
        return $this->entityManager->find(Company::class, $id);
    }
}
