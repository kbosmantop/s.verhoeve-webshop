<?php

namespace AppBundle\Command\Doctrine\Schema;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DoctrineCommandHelper;
use Doctrine\DBAL\DBALException as DBALExceptionAlias;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand as DoctrineSchemaCreateCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command to execute the SQL needed to generate the database schema for
 * a given entity manager.
 */
class CreateCommand extends DoctrineSchemaCreateCommand
{
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('doctrine:schema:create')
            ->setDescription('Executes (or dumps) the SQL needed to generate the database schema')
            ->addOption('em', null, InputOption::VALUE_OPTIONAL, 'The entity manager to use for this command');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     * @throws DBALExceptionAlias
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        DoctrineCommandHelper::setApplicationEntityManager($this->getApplication(), $input->getOption('em'));

        /** @var EntityManagerInterface $em */
        $em = $this->getHelper('em')->getEntityManager();
        $em->getConnection()->query('SET foreign_key_checks=0;');

        $result = parent::execute($input, $output);

        $em->getConnection()->query('SET foreign_key_checks=1;');

        return $result;
    }
}
