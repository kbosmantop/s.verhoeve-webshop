<?php

namespace AppBundle\Command\Bakery;

use AppBundle\Connector\Bakker;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class SendBakeryMailCommand
 * @package AppBundle\Command\Bakery
 */
class SendBakeryMailCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderActivityService
     */
    private $orderActivityService;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('bakery:send-new-order-mail')
            ->setDescription('Send new order available mail to bakery')
            ->addArgument('supplierOrderId', InputArgument::REQUIRED, 'Supplier order identifier');
    }

    /**
     * ProductCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param Swift_Mailer           $mailer
     * @param LoggerInterface        $logger
     * @param OrderActivityService   $orderActivityService
     * @param TwigEngine             $twig
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Swift_Mailer $mailer,
        LoggerInterface $logger,
        OrderActivityService $orderActivityService,
        TwigEngine $twig
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->orderActivityService = $orderActivityService;
        $this->twig = $twig;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return bool
     * @throws OptimisticLockException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierOrderId = $input->getArgument('supplierOrderId');

        if (null !== $supplierOrderId) {
            $supplierOrderId = (int)$supplierOrderId;

            /**
             * @var SupplierOrder $supplierOrder
             */
            $supplierOrder = $this->entityManager->getRepository(SupplierOrder::class)->find($supplierOrderId);

            if (null === $supplierOrder) {
                throw new \RuntimeException(sprintf('Cannot find supplier order with id %s', $supplierOrderId));
            }

            $order = $supplierOrder->getOrder();

            try {
                $reflectionClass = new \ReflectionClass(Bakker::class);

                /** @var CompanyConnectorParameter[] $connectorParameters */
                $connectorParameters = $this->entityManager->getRepository(CompanyConnectorParameter::class)->findBy([
                    'company' => $supplierOrder->getSupplier(),
                    'connector' => $reflectionClass->getShortName(),
                ]);
                $parameters = new ArrayCollection();
                foreach ($connectorParameters as $cp) {
                    $parameters->set($cp->getName(), $cp->getValue());
                }
                $to = ($parameters->get('email') ?: $supplierOrder->getSupplier()->getEmail());

                if ($to) {
                    $subject = sprintf('Nieuwe opdracht (onze referentie: %s', $order->getOrderNumber());

                    $body = $this->twig->render('@App/bakery-order.html.twig');

                    $message = (new \Swift_Message())
                        ->setFrom('noreply@toptaarten.nl', 'Toptaarten.nl')
                        ->setSubject($subject)
                        ->addTo($to, $supplierOrder->getSupplier()->getName())
                        ->setBody($body, 'text/html');

                    $this->mailer->send($message);
                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage() . PHP_EOL . $e->getTraceAsString());

                $this->orderActivityService->add('order_notification_failed', $order, null, [
                    'text' => sprintf('%s notificatie versturen is mislukt', 'Email'),
                ]);
            }
        }

        return 0;
    }
}
