<?php

namespace AppBundle\Command;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Domain;
use AppBundle\Services\Mailer\RegistrationMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendRegistrationMailCommand
 * @package AppBundle\Command
 */
class SendRegistrationMailCommand extends Command
{
    use CommandContextTrait;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @var RegistrationMailer
     */
    private $registrationMailer;

    /**
     * SendRegistrationMailCommand constructor.
     * @param EntityManagerInterface      $entityManager
     * @param RegistrationMailer $registrationMailer
     * @param null|string        $name
     */
    public function __construct(EntityManagerInterface $entityManager, RegistrationMailer $registrationMailer, ?string $name = null)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->registrationMailer = $registrationMailer;
    }

    protected function configure()
    {
        $this
            ->setName('topbloemen:site:send-registration-mail')
            ->setDescription('Send registration reset email')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('domain', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        /** @var Customer $customer */
        $customer = $this->entityManager->getRepository(Customer::class)->find($input->getArgument('id'));

        /** @var Domain $domain */
        $domain = $this->entityManager->getRepository(Domain::class)->find($input->getArgument('domain'));

        $this->defineContext($domain->getSite());

        $parameters = [
            'customer' => $customer,
            'site' => $domain->getSite(),
        ];

        if ($customer->getCompany()) {
            $parameters['company'] = $customer->getCompany();
        }

        $this->registrationMailer->send($parameters);
    }
}
