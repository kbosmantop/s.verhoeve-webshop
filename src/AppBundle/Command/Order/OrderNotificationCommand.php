<?php

namespace AppBundle\Command\Order;

use AppBundle\Connector\BakkerMail;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Services\ConnectorHelper;

/**
 * Class OrderNotificationCommand
 * @package AppBundle\Command\Order
 * @Job(commandLine="order:notify pending", hour="7", minute="30")
 * @Job(commandLine="order:notify processed", hour="18", minute="0")
 */
class OrderNotificationCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('order:notify')
            ->addArgument('status', InputArgument::REQUIRED, 'Available: pending, processed');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Twig_Error
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $status = strtolower($input->getArgument('status'));

        $availableStatuses = ['pending', 'processed'];
        $connectorsToBeNotified = ['bakker', 'bakkermail'];

        //make sure the input is correct
        if (!\in_array($status, $availableStatuses, true)) {
            $output->writeln(sprintf("'%s' is not a valid status", $status));
            exit;
        }

        $today = new \DateTime();

        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $entityRepository */
        $entityRepository = $em->getRepository(Order::class);

        $qb = $entityRepository->createQueryBuilder('order')
            ->join('order.supplier_order', 'supplier_order')
            ->andWhere('supplier_order.supplier is not null')
            ->andWhere('order.status = :status')
            ->andWhere('order.deliveryDate = :date')
            ->andWhere('supplier_order.connector IN (:connectors)')
            ->setParameters([
                'status' => $status,
                'date' => $today->format('Y-m-d'),
                'connectors' => $connectorsToBeNotified,
            ]);

        $orders = $qb->getQuery()->execute();

        /**
         * @var Order[] $orders
         */
        //create array to check if a bakker connector supplier has already been notified
        //bakker connector suppliers get only notified once
        $bakkerSuppliers = [];
        $suppliersNotifiedBySMS = [];

        foreach ($orders as $order) {
            $supplierOrder = $order->getSupplierOrder();

            if ($supplierOrder === null) {
                continue;
            }

            $supplier = $supplierOrder->getSupplier();
            $connector = $supplierOrder->getConnector();

            $supplier = $order->getSupplier();

            if (!$order->hasActivity('order_notified_' . $status)) {
                //TODO set activity to order notified to prevent that the same order is notified twice
                // TODO SF4
                $activityService = $this->getContainer()->get('order.activity');
                $activityService->add('order_notified_' . $status, $order);

                //check if bakker connector is already notified
                if (strtolower($connector) === 'bakker' && \in_array($supplier->getId(),
                        $bakkerSuppliers, true)) {
                    continue;
                }

                $bakkerSuppliers[] = $supplier->getId();

                $output->writeln('order: ' . $order->getId() . ' notified - ' . $connector);

                $results = $this->getDoctrine()->getRepository(CompanyConnectorParameter::class)->findBy([
                    'company' => $order->getSupplier(),
                    'connector' => $connector,
                ]);

                $parameters = new ArrayCollection();

                /** @var CompanyConnectorParameter[] $results */
                foreach ($results as $result) {
                    $parameters->set($result->getName(), $result->getValue());
                }

                switch (strtolower($connector)) {
                    case 'bakkermail':
                        // // TODO SF4
                        /** @var BakkerMail $bakkerMailConnector */
                        $bakkerMailConnector = $this->container->get(ConnectorHelper::class)->getConnector($supplierOrder);
                        $bakkerMailConnector->notify($supplierOrder, $status);
                        break;
                    case 'bakker':
                        //check status to set the correct content
                        if ($status === 'pending') {
                            $subject = 'Onverwerkte orders voor vandaag';
                            $content = 'U heeft nog overwerkte orders.';
                        } else {
                            $subject = 'Order zonder bezorgstatus, bezorging voor vandaag';
                            $content = 'U heeft nog orders zonder bezorgstatus.';
                        }

                        $message = new \Swift_Message();
                        $message
                            ->setSubject($subject)
                            ->setFrom('noreply@topgeschenken.nl', 'Topgeschenken.nl')
                            ->addTo($parameters->get('email'), $supplier->getName())
                            ->setBody(
                                // // TODO SF4
                                $this->container->get('twig')->render('@Supplier/bakery-notify.html.twig', [
                                    'content' => $content,
                                ]), 'text/html'
                            );

                        // TODO SF4
                        $this->container->get('mailer')->send($message);
                        break;
                }

                //only send SMS when connector is bakker or bakkerMail
                if (strtolower($connector) === 'bakker' || (strtolower($connector) === 'bakkermail' && $status !== 'processed')) {
                    if (!\in_array($supplier->getId(), $suppliersNotifiedBySMS, true)) {
                        $suppliersNotifiedBySMS[] = $supplier->getId();

                        //send sms if phone is available
                        if ($parameters->get('phone')) {
                            // // TODO SF4
                            $sms = $this->getContainer()->get('sms');

                            //check status to set the correct content
                            if ($status === 'pending') {
                                $content = 'U heeft nog onverwerkte orders in het orderprogramma, we verzoeken u de status te updaten. Bedankt, Toptaarten.nl';
                            } else {
                                $content = 'U heeft nog orders zonder bezorgstatus in het orderprogramma, we verzoeken u de status te updaten. Bedankt, Toptaarten.nl';
                            }

                            try {
                                $sms->send([$this->getParameter('phone')], $content, 'Toptaarten');
                            } catch (\Exception $e) {
                                // TODO SF4
                                $this->container->get('order.activity')->add('order_notification_failed', $order, null,
                                    ['text' => 'SMS notificatie versturen is mislukt']);
                            }
                        }
                    }
                }
            }
        }

        $em->flush();
    }

    /**
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
