<?php

namespace AppBundle\Command\Resources;

use AppBundle\Entity\Geography\Country;
use AppBundle\Services\Resources\ShapefileReader;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

use Symfony\Component\Finder\Finder;

/**
 * Class ShapefileCommand
 * @package AppBundle\Command\Resources
 */
class ShapefileCommand extends Command
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var ShapefileReader
     */
    private $shapefileReader;

    protected function configure()
    {
        $this
            ->setName('resources:shapefile')
            ->addArgument('country', InputArgument::OPTIONAL)
            ->addArgument('name', InputArgument::OPTIONAL, 'Filename of shapefile (without extension)');
    }

    /**
     * ShapefileCommand constructor.
     * @param Registry        $doctrine
     * @param ShapefileReader $shapefileReader
     * @param null|string     $name
     */
    public function __construct(
        Registry $doctrine,
        ShapefileReader $shapefileReader,
        ?string $name = null
    ) {
        parent::__construct($name);

        $this->doctrine = $doctrine;
        $this->shapefileReader = $shapefileReader;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws \Exception
     * @return integer
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getArgument('country') && $input->getArgument('name')) {
            $shapefile = $input->getArgument('name');
            $country = $this->getCountry($input->getArgument('country'));
        } else {
            $helper = $this->getHelper('question');

            $question = new ChoiceQuestion('Please select a shapefile', $this->getShapefiles());
            $question->setErrorMessage('Shapefile %s is invalid.');

            $shapefile = $helper->ask($input, $output, $question);

            $question = new ChoiceQuestion('Please select a country', [
                'NL' => 'Nederland',
                'BE' => 'België',
                'LU' => 'Luxemburg',
                'DE' => 'Duitsland'
            ]);
            $question->setErrorMessage('Country %s is invalid.');

            $country = $this->getCountry($helper->ask($input, $output, $question));
        }

        $this->shapefileReader->read($shapefile, $country);

        return 0;
    }

    /**
     * @param $country
     * @return Country
     */
    private function getCountry($country): Country
    {
        return $this->doctrine->getRepository(Country::class)->find($country);
    }

    /**
     * @return array
     */
    private function getShapefiles(): array
    {
        $finder = new Finder();

        $shapefiles = [];

        foreach ($finder->in('var/resources/shapefiles/')->name('*.shp')->files()->getIterator() as $file) {
            $shapefiles[] = substr($file->getBasename(), 0, -4);
        }

        return $shapefiles;
    }
}
