<?php

namespace AppBundle\Command;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Domain;
use AppBundle\Services\Mailer\CompanyChoosePasswordMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendCompanyChoosePasswordMailCommand
 * @package AppBundle\Command
 */
class SendCompanyChoosePasswordMailCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CompanyChoosePasswordMailer
     */
    private $companyChoosePasswordMailer;

    /**
     * SendCompanyChoosePasswordMailCommand constructor.
     * @param EntityManagerInterface                      $entityManager
     * @param CompanyChoosePasswordMailer $companyChoosePasswordMailer
     * @param null|string                        $name
     */
    public function __construct(EntityManagerInterface $entityManager, CompanyChoosePasswordMailer $companyChoosePasswordMailer, ?string $name = null)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->companyChoosePasswordMailer = $companyChoosePasswordMailer;
    }

    protected function configure()
    {
        $this
            ->setName('topbloemen:site:send-company-choose-password-mail')
            ->setDescription('Send password reset email')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('domain', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        /** @var Customer $customer */
        $customer = $this->entityManager->find(Customer::class, $input->getArgument('id'));

        /** @var Domain $domain */
        $domain = $this->entityManager->find(Domain::class, $input->getArgument('domain'));

        $parameters = [
            'customer' => $customer,
            'site' => $domain->getSite(),
        ];

        if ($customer->getCompany()) {
            $parameters['company'] = $customer->getCompany();
        }

        $this->defineContext($domain->getSite());

        $this->companyChoosePasswordMailer->send($parameters);
    }
}