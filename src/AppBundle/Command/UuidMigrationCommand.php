<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UuidMigrationCommand
 * @package AppBundle\Command
 */
class UuidMigrationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('topgeschenken:migrate:uuid')
            ->addArgument('entityClass', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $entityClass = $input->getArgument('entityClass');

        $this->getContainer()->get('topgeschenken.uuid_generator')->migrate($entityClass);
    }
}
