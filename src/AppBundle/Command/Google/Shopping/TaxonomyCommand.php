<?php

namespace AppBundle\Command\Google\Shopping;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TaxonomyCommand
 * @package AppBundle\Command\Google\Shopping
 */
class TaxonomyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('google:shopping:taxonomy');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        void($input, $output);

        $this->getContainer()->get('google.shopping.taxonomy-importer')->import();
    }
}