<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Mailer\OrderConfirmationMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendConfirmationCommand
 * @package AppBundle\Command
 */
class SendConfirmationCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderConfirmationMailer
     */
    private $orderConfirmationMailer;

    /**
     * SendConfirmationCommand constructor.
     * @param EntityManagerInterface  $entityManager
     * @param OrderConfirmationMailer $orderConfirmationMailer
     * @param null|string             $name
     */
    public function __construct(EntityManagerInterface $entityManager, OrderConfirmationMailer $orderConfirmationMailer, ?string $name = null)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;

        $this->orderConfirmationMailer = $orderConfirmationMailer;
    }

    protected function configure()
    {
        $this
            ->setName('topbloemen:shop:send-confirmation')
            ->setDescription('Send order confirmation')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $orderCollection = $this->entityManager->find(OrderCollection::class, $input->getArgument('id'));

        $this->defineContext($orderCollection->getSite());

        $this->orderConfirmationMailer->send(['orderCollection' => $orderCollection]);
    }
}
