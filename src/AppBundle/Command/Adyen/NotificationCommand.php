<?php

namespace AppBundle\Command\Adyen;

use Adyen\AdyenException;
use AppBundle\ThirdParty\Adyen\Entity\AdyenNotification;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class NotificationCommand
 * @package AppBundle\Command\Adyen
 */
class NotificationCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('adyen:notification')
            ->addArgument('id', null, InputArgument::REQUIRED)
            ->addOption('process', null, InputOption::VALUE_NONE)
            ->addOption('force', null, InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws AdyenException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        $notification = $this->getDoctrine()->getRepository(AdyenNotification::class)->find($id);

        if ($notification === null) {
            throw new \RuntimeException(sprintf("Notification with id '%d' not found", $id));
        }

        if ($input->getOption('process')) {
            if (!$notification->getProcessed() || $input->getArgument('force')) {
                $this->getContainer()->get('adyen_notification')->process($notification);
            }
        }
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}