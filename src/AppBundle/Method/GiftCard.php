<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Form\Method\GiftCardType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\RedirectableMethodInterface;

/**
 * Class GiftCard
 * @package AppBundle\Method
 */
class GiftCard extends AbstractMethod implements MethodInterface, RedirectableMethodInterface
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'giftcard';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Cadeaubon';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return GiftCardType::class;
    }

    /**
     * @param $host
     * @param $params
     * @return string
     */
    public function generateUrl($host, $params)
    {
        return $host . '/hpp/details.shtml?' . http_build_query($params);
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function generateParams(Payment $payment)
    {
        return [
            'brandCode' => 'bcb',
        ];
    }
}
