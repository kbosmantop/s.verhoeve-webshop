<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Form\Method\BancontactType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\RedirectableMethodInterface;

/**
 * Class Bancontact
 * @package AppBundle\Method
 */
class Bancontact extends AbstractMethod implements MethodInterface, RedirectableMethodInterface
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'bancontact';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Bancontact';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return BancontactType::class;
    }

    /**
     * @param $host
     * @param $params
     * @return string
     */
    public function generateUrl($host, $params)
    {
        return $host . '/hpp/details.shtml?' . http_build_query($params);
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function generateParams(Payment $payment)
    {
        return [
            'brandCode' => 'bcmc',
        ];
    }
}
