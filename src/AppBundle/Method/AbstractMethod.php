<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Interfaces\GatewayInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class AbstractMethod
 * @package AppBundle\Method
 */
abstract class AbstractMethod
{
    /**
     * @var GatewayInterface
     */
    protected $gateway;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @return string
     */
    abstract public function getCode();

    /**
     * AbstractMethod constructor.
     * @param null $gateway
     */
    public function __construct($gateway = null)
    {
        $this->gateway = $gateway;
    }

    /**
     * @param Registry $doctrine
     */
    public function setDoctrine(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return GatewayInterface
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @return null
     */
    public function getFormClass()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getEntityManager()
    {
        if ($this->doctrine) {
            return $this->doctrine->getManager();
        }

        return $this->gateway->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public function getEntity()
    {
        $entity = $this->getEntityManager()->getRepository(Paymentmethod::class)->findOneBy([
            'code' => $this->getCode(),
            'gateway' => $this->gateway ? strtolower((new \ReflectionClass($this->gateway))->getShortName()) : null,
        ]);

        return $entity;
    }
}
