<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Form\Method\InvoiceType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;
use AppBundle\Services\JobManager;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Dump\Container;

/**
 * Class AfterPay
 * @package AppBundle\Method
 *
 * @link    http://integration.afterpay.nl/documentation/api/webservice-objects/authorization-objects/
 * @link    http://integration.afterpay.nl/nl/documentation/api/result-object/
 * @link    http://integration.afterpay.nl/testing/test-data/
 * @link    http://integration.afterpay.nl/nl/go-live-checklist/
 */
class Invoice extends AbstractMethod implements MethodInterface, ProcessableMethodInterface
{
    /**
     * @var JobManager $jobManager
     */
    private $jobManager;

    /**
     * @return string
     */
    public function getCode()
    {
        return 'invoice';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Op rekening';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return InvoiceType::class;
    }

    /**
     * @param JobManager $jobManager
     */
    public function setJobManager(JobManager $jobManager) {
        $this->jobManager = $jobManager;
    }

    /**
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    public function processPayment(Payment $payment)
    {
        $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'authorized'));

        $this->getEntityManager()->flush($payment);

        return true;
    }
}
