<?php

namespace AppBundle\Method;

use AppBundle\Entity\Order\OrderLineVat;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Form\Method\AfterPayType;
use AppBundle\Gateway\Afterpay as AfterpayGateway;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Utils\AmountConverter;
use Translation\Common\Model\Message;
use libphonenumber\PhoneNumberFormat;
use Symfony\Component\HttpFoundation\Request;

/**
 * @link http://integration.afterpay.nl/documentation/api/webservice-objects/authorization-objects/
 * @link http://integration.afterpay.nl/nl/documentation/api/result-object/
 * @link http://integration.afterpay.nl/testing/test-data/
 * @link http://integration.afterpay.nl/nl/go-live-checklist/
 *
 * @method AfterpayGateway getGateway
 *
 */
class AfterPay extends AbstractMethod implements MethodInterface, ProcessableMethodInterface
{
    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * AfterPay constructor.
     * @param null             $gateway
     * @param OrderLineManager $orderLineManager
     */
    public function __construct($gateway = null, OrderLineManager $orderLineManager)
    {
        $this->orderLineManager = $orderLineManager;

        parent::__construct($gateway);
    }

    /**
     * @var AfterpayGateway
     */
    protected $gateway;

    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        $messages = [
            new Message('payment.afterpay.refused', 'validators'),
        ];

        return $messages;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return 'afterpay';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'AfterPay';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'veilig achteraf betalen';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return AfterPayType::class;
    }

    /**
     * @param Payment $payment
     * @param Request $request
     *
     * @return bool
     * @throws PaymentException
     * @throws \Exception
     */
    public function processPayment(Payment $payment, Request $request)
    {
        $orderCollection = $payment->getOrderCollection();

        $this->handleCustomerBirthday($orderCollection->getCustomer(), $request);

        try {
            $data = [
                'validateAndCheckB2COrder' => [
                    'authorization' => $this->getGateway()->generateAuthorization($payment),
                    'b2corder' => $this->generateOrder($payment, $request),
                ],
            ];

            $result = $this->getGateway()->getClient()->__soapCall('validateAndCheckB2COrder', $data);

            switch ($result->return->resultId) {
                case '0':
                    /* Het betaalverzoek is geaccepteerd en de order is bij AfterPay geregistreerd. */
                    if ($result->return->statusCode === 'A') {
                        $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'authorized'));
                        $payment->setReference($result->return->afterPayOrderReference);
                    } else {
                        throw new PaymentException('Unknown status code: '.$result->return->statusCode);
                    }

                    break;
                case '1':
                    /* Er is een technische fout opgetreden in de aanvraag */
                    throw new PaymentException('Unknown afterpay error: '.$result->return->statusCode);

                    break;
                case '2':
                    /* Een of meerdere velden voldoen niet aan het juiste formaat */
                    $errors = $this->getErrors($result->return->failures);

                    $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'failed'));
                    $payment->setLastErrors($errors);

                    break;
                case '3':
                    /* Het verzoek was valide maar de consument komt niet in aanmerking voor achteraf betalen */
                    $errors = [];
                    $errors[$result->return->rejectCode] = 'payment.afterpay.refused'; //$result->return->rejectDescription;

                    $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'refused'));
                    $payment->setLastErrors($errors);

                    break;
                case '4':
                    /** Pending */
                    if ($result->return->statusCode === 'P') {
                        $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'pending'));
                        $payment->setReference($result['pspReference']);
                    } else {
                        throw new PaymentException('Unknown status code: '.$result->return->statusCode);
                    }
                    break;
            }
        } catch (\SoapFault $e) {
            $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'failed'));
            $payment->setLastErrors([$e->getMessage()]);
        }

        $this->getEntityManager()->flush($payment);

        return $payment->isPaid();
    }

    /**
     * @param Payment $payment
     * @param Request $request
     *
     * @return \stdClass
     * @throws \Exception
     */
    private function generateOrder(Payment $payment, Request $request)
    {
        $orderCollection = $payment->getOrderCollection();

        $order = new \stdClass();

        $order->shopper = new \stdClass();
        $order->shopper->profilecreated = substr($orderCollection->getCustomer()->getCreatedAt()->format('c'), 0, -6);

        $order->b2cbilltoAddress = new \stdClass();
        $order->b2cbilltoAddress->referencePerson = new \stdClass();
        $order->b2cbilltoAddress->referencePerson->dateofbirth = substr($orderCollection->getCustomer()->getBirthday()->format('c'),
            0, -6);
        $order->b2cbilltoAddress->referencePerson->emailaddress = $orderCollection->getCustomer()->getEmail();
        $order->b2cbilltoAddress->referencePerson->gender = $this->getGender($orderCollection->getCustomer());
        $order->b2cbilltoAddress->referencePerson->initials = $orderCollection->getCustomer()->getInitials();
        $order->b2cbilltoAddress->referencePerson->isoLanguage = 'NL';
        $order->b2cbilltoAddress->referencePerson->lastname = $orderCollection->getCustomer()->getLastname();
        $order->b2cbilltoAddress->referencePerson->phonenumber1 = $orderCollection->getCustomer()->getFormattedPhoneNumber(PhoneNumberFormat::E164);

        $order->b2cbilltoAddress->city = $orderCollection->getInvoiceAddressCity();
        $order->b2cbilltoAddress->housenumber = $orderCollection->getInvoiceAddressNumber();
        $order->b2cbilltoAddress->housenumberAddition = $orderCollection->getInvoiceAddressNumberAddition();
        $order->b2cbilltoAddress->isoCountryCode = $orderCollection->getInvoiceAddressCountry()->getCode();
        $order->b2cbilltoAddress->postalcode = $orderCollection->getInvoiceAddressPostcode();
        $order->b2cbilltoAddress->streetname = $orderCollection->getInvoiceAddressStreet();

        /**
         * @var Order $orderOrder ;
         */
        $orderOrder = $orderCollection->getOrders()->current();

        $order->b2cshiptoAddress = new \stdClass();
        $order->b2cshiptoAddress->referencePerson = new \stdClass();
        $order->b2cshiptoAddress->referencePerson->dateofbirth = substr($orderCollection->getCustomer()->getBirthday()->format('c'),
            0, -6);
        $order->b2cshiptoAddress->referencePerson->emailaddress = $orderCollection->getCustomer()->getEmail();
        $order->b2cshiptoAddress->referencePerson->gender = $this->getGender($orderCollection->getCustomer());
        $order->b2cshiptoAddress->referencePerson->initials = $orderCollection->getCustomer()->getInitials();
        $order->b2cshiptoAddress->referencePerson->isoLanguage = 'NL';
        $order->b2cshiptoAddress->referencePerson->lastname = $orderCollection->getCustomer()->getLastname();
        $order->b2cshiptoAddress->referencePerson->phonenumber1 = $orderCollection->getCustomer()->getFormattedPhoneNumber(PhoneNumberFormat::E164);

        $order->b2cshiptoAddress->city = $orderOrder->getDeliveryAddressCity();
        $order->b2cshiptoAddress->housenumber = $orderOrder->getDeliveryAddressNumber();
        $order->b2cshiptoAddress->housenumberAddition = $orderOrder->getDeliveryAddressNumberAddition();
        $order->b2cshiptoAddress->isoCountryCode = $orderOrder->getDeliveryAddressCountry()->getCode();
        $order->b2cshiptoAddress->postalcode = $orderOrder->getDeliveryAddressPostcode();
        $order->b2cshiptoAddress->streetname = $orderOrder->getDeliveryAddressStreet();
        $order->ordernumber = $orderCollection->getNumber();
        $order->bankaccountNumber = null;
        $order->currency = 'EUR';
        $order->ipAddress = $request->getClientIp();
        $order->parentTransactionreference = null;
        $order->orderlines = [];

        $totalOrderAmount = 0;

        foreach ($orderCollection->getOrders() as $orderOrder) {
            foreach ($orderOrder->getLines() as $line) {
                if(null === $line) {
                    continue;
                }

                /**
                 * @var OrderLine $line ;
                 */
                $orderLine = new \stdClass();
                $orderLine->articleId = 'Product ' . $line->getProduct()->getId();
                $orderLine->articleDescription = $line->getTitle();
                $orderLine->quantity = $line->getQuantity();

                $totalLinePice = $this->orderLineManager->getAmountIncl($line);
                $convertedTotalLinePrice = AmountConverter::convert($totalLinePice);
                $unitPrice = $convertedTotalLinePrice / $orderLine->quantity;

                $orderLine->unitprice = $unitPrice;
                $totalOrderAmount += $convertedTotalLinePrice;
                $orderLine->vatcategory = $this->getVatCategory($line->getVatLines()->current());

                $order->orderlines[] = $orderLine;

                if ($line->getDiscountAmount()) {
                    $orderLine = new \stdClass();
                    $orderLine->articleDescription = 'Korting';
                    $orderLine->articleId = 0;
                    $orderLine->quantity = 1;
                    $orderLine->unitprice = AmountConverter::convert(-$line->getDiscountAmount());
                    $orderLine->vatcategory = $this->getVatCategory($line->getVatLines()->current());
                    $totalOrderAmount += $orderLine->unitprice;

                    $order->orderlines[] = $orderLine;
                }
            }
        }

        $order->totalOrderAmount = $totalOrderAmount;

        return $order;
    }

    /**
     * @param OrderLineVat $orderLineVat
     * @return int
     */
    private function getVatCategory(OrderLineVat $orderLineVat)
    {
        $percentage = $orderLineVat->getVatRate()->getFactor() * 100;
        switch ($percentage) {
            case 21: // high
                return 1;
            case 6: // low
            case 9:
                return 2;
            case 0: // zero
                return 3;
            case null: // none
                return 4;
        }

        throw new \RuntimeException("VAT percentage '%d' not implemented", $percentage);
    }

    /**
     * @param Customer $customer
     *
     * @return string
     * @throws \RuntimeException
     */
    private function getGender(Customer $customer)
    {
        switch ($customer->getGender()) {
            case 'male':
                return 'M';
            case 'female':
                return 'V';
        }

        throw new \RuntimeException(sprintf("Gender '%s' not implemented", $customer->getGender()));
    }

    /**
     * @param $failures
     *
     * @return array
     */
    private function getErrors($failures)
    {
        if (\is_object($failures)) {
            $failures = $failures = [$failures];
        }

        $errors = [];

        foreach ($failures as $failure) {
            $errors[$failure->failure] = $failure->suggestedvalue;
        }

        return $errors;
    }

    /**
     * @param Customer $customer
     * @param Request  $request
     *
     * @return \DateTime
     */
    private function handleCustomerBirthday(Customer $customer, Request $request)
    {
        if (!$customer->getBirthday()) {
            $datetime = new \DateTime(implode('-',
                $request->get('cart')['site_checkout_payment']['afterpay']['birthday']));
            $datetime->setTime(0, 0, 0);

            $customer->setBirthday($datetime);

            $this->getEntityManager()->flush($customer);
        }

        return $customer->getBirthday();
    }
}
