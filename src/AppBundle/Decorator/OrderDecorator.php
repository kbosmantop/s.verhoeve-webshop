<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Order\OrderCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderDecorator
 * @package AppBundle\Decorator
 */
class OrderDecorator
{
    use ContainerAwareTrait;

    /**
     * @var OrderCollection $orderCollection
     */
    private $orderCollection;

    /**
     * @var ArrayCollection $orders
     */
    private $orders;

    /**
     * @param OrderCollection $orderCollection
     * @return $this
     */
    public function setOrder(OrderCollection $orderCollection)
    {
        $this->orderCollection = $orderCollection;
        $this->orders = $orderCollection->getOrders();

        return $this;
    }

    /**
     * Get the OrderCollection entity
     *
     * @return OrderCollection
     */
    public function get()
    {
        return $this->orderCollection;
    }

    /**
     * @deprecated
     * Get the total items
     *
     * @return int $count
     */
    public function count()
    {
        return $this->orderCollection->count();
    }

    /**
     * Check if there are any underlying orders
     *
     * @return bool
     */
    public function hasOrders()
    {
        return (bool)$this->orders;
    }

    /**
     * Get underlying orders
     *
     * @return ArrayCollection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Return the invoice address
     *
     * @return Address|null $invoiceAddress
     */
    public function getInvoiceAddress()
    {
        $invoiceAddress = $this->orderCollection->getInvoiceAddress();

        if (!$invoiceAddress) {
            $invoiceAddress = $this->orderCollection->getCustomer()->getInvoiceAddress();
        }

        return $invoiceAddress;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayTotalPrice()
    {
        return $this->orderCollection->getTotalDisplayPrice();
    }

    /**
     * @deprecated
     * @return float
     */
    public function getTotalDisplayPrice()
    {
        return $this->orderCollection->getTotalDisplayPrice();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return \call_user_func_array([$this->orderCollection, $name], $arguments);
    }
}
