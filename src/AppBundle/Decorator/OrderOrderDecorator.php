<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Order\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderOrderDecorator
 * @package AppBundle\Decorator
 */
class OrderOrderDecorator
{
    use ContainerAwareTrait;

    /**
     * @var OrderDecorator $order
     */
    private $order;

    /**
     * @var Order $order
     */
    private $orderOrder;

    /**
     * @var ArrayCollection $lines
     */
    private $lines;

    /**
     * @param Order $orderOrder
     *
     * @return $this
     */
    public function setOrderOrder(Order $orderOrder)
    {
        $this->orderOrder = $orderOrder;

        $this->lines = new ArrayCollection();

        foreach ($orderOrder->getLines() as $orderLine) {
            $orderLineDecorator = new OrderLineDecorator();
            $orderLineDecorator->setContainer($this->container);
            $orderLineDecorator->setOrderOrder($this);
            $orderLineDecorator->setOrderLine($orderLine);

            if ($orderLine->getParent()) {
                $parentOrderLineDecorator = new OrderLineDecorator();
                $parentOrderLineDecorator->setContainer($this->container);
                $parentOrderLineDecorator->setOrderOrder($this);
                $parentOrderLineDecorator->setOrderLine($orderLine->getParent());

                $orderLineDecorator->setParent($parentOrderLineDecorator);
            }

            $this->lines->add($orderLineDecorator);
        }

        return $this;
    }

    /**
     *
     * @return Order
     */
    public function get()
    {
        return $this->orderOrder;
    }

    /**
     * @param OrderDecorator $orderDecorator
     *
     * @return OrderOrderDecorator
     */
    public function setOrder(OrderDecorator $orderDecorator)
    {
        $this->order = $orderDecorator;

        return $this;
    }

    /**
     *
     * @return OrderDecorator
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     *
     * @return ArrayCollection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @deprecated
     * @todo: determine if display is excl or incl vat
     * @return string
     */
    public function getDisplayTotalPrice()
    {
        return $this->orderOrder->getDisplayTotalPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayTotalPriceExcl()
    {
        return $this->orderOrder->getDisplayTotalPriceExcl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayTotalPriceIncl()
    {
        return $this->orderOrder->getDisplayTotalPriceIncl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayTotalTax()
    {
        return $this->orderOrder->getDisplayTotalTax();
    }

    /**
     * @deprecated
     * @return float
     */
    public function getTotalDisplayPrice()
    {
        return $this->orderOrder->getTotalDisplayPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getCompositeNumber()
    {
        return $this->orderOrder->getCompositeNumber();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->orderOrder, $name], $arguments);
    }
}