<?php

namespace AppBundle\Decorator;

use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductPropertyDecorator
 * @package AppBundle\Decorator
 */
class ProductPropertyDecorator
{
    use ContainerAwareTrait;

    /**
     * @var ProductProperty
     */
    private $productProperty;

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return \call_user_func_array([$this->productProperty, $name], $arguments);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return (string)$this->productProperty;
        } catch (\Exception $e) {
        }

        return '';
    }

    /**
     * @param ProductProperty $productProperty
     */
    public function setProductProperty(ProductProperty $productProperty)
    {
        $this->productProperty = $productProperty;
    }

    /**
     * @return ProductProperty
     */
    public function getProductProperty()
    {
        return $this->productProperty;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->productProperty->getProductgroupProperty()->getFormType();
    }

    /**
     * @todo
     * @throws \RuntimeException
     */
    public function getDecoratorService()
    {
        throw new \RuntimeException('Not implemented');
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function getText()
    {
        if ($this->getType() !== ProductgroupPropertyTypeType::TEXT) {
            throw new \RuntimeException(sprintf("Type '%s' not equal to '%s'", $this->getType(),
                ProductgroupPropertyTypeType::TEXT));
        }

        return $this->productProperty->getValue();
    }

    /**
     * Alias for getText
     *
     * @return string
     * @throws \Exception
     */
    public function getValue()
    {
        return $this->getText();
    }

    /**
     * @return null
     * @throws \RuntimeException
     */
    public function getChoice()
    {
        if ($this->getType() !== ProductgroupPropertyTypeType::CHOICE) {
            throw new \RuntimeException(sprintf("Type '%s' not equal to '%s'", $this->getType(),
                ProductgroupPropertyTypeType::CHOICE));
        }

        throw new \RuntimeException('Not implemented');
    }

    /**
     * @return ProductDecorator|null
     * @throws \RuntimeException
     */
    public function getEntity()
    {
        if ($this->getType() !== ProductgroupPropertyTypeType::ENTITY) {
            throw new \RuntimeException(sprintf("Type '%s' not equal to '%s'", $this->getType(),
                ProductgroupPropertyTypeType::ENTITY));
        }

        if (!$this->productProperty->getProductVariation()) {
            return null;
        }

        return $this->container->get('app.product.factory')->get($this->productProperty->getProductVariation());
    }

    /**
     * Alias for getEntity
     *
     * @return ProductDecorator
     * @throws \Exception
     */
    public function getProduct()
    {
        return $this->getEntity();
    }

    /**
     * @return void
     * @throws \RuntimeException
     */
    public function getAssortment()
    {
        if ($this->getType() !== ProductgroupPropertyTypeType::ASSORTMENT) {
            throw new \RuntimeException(sprintf("Type '%s' not equal to '%s'", $this->getType(),
                ProductgroupPropertyTypeType::ASSORTMENT));
        }

        throw new \RuntimeException('Not implemented');
    }
}
