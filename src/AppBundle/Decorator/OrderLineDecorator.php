<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class OrderLineDecorator
{
    use ContainerAwareTrait;

    /**
     * @var OrderDecorator $orderDecorator
     */
    private $orderDecorator;

    /**
     * @var OrderLine $order
     */
    private $orderLine;

    /**
     * @var OrderLineDecorator $parent
     */
    private $parent;

    /**
     * @var ArrayCollection $lines
     */
    private $children;

    /**
     * @param OrderLine $orderLine
     *
     * @return $this
     */
    public function setOrderLine(OrderLine $orderLine)
    {
        $this->orderLine = $orderLine;

        $this->children = new ArrayCollection();

        foreach ($orderLine->getChildren() as $orderLine) {
            $orderLineDecorator = new OrderLineDecorator();
            $orderLineDecorator->setParent($this);
            $orderLineDecorator->setOrderLine($orderLine);
            $orderLineDecorator->setOrderOrder($this->getOrderOrder());

            $this->children->add($orderLineDecorator);
        }

        return $this;
    }

    /**
     * @param OrderDecorator $order
     *
     * @return $this
     */
    public function setOrderOrder(OrderDecorator $order)
    {
        $this->orderDecorator = $order;

        return $this;
    }

    /**
     *
     * @return OrderLine
     */
    public function get()
    {
        return $this->orderLine;
    }

    /**
     *
     * @return Order|OrderDecorator
     */
    public function getOrderOrder()
    {
        return $this->orderDecorator;
    }

    /**
     * @param OrderLineDecorator $parent
     *
     * @return OrderLineDecorator
     */
    public function setParent(OrderLineDecorator $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayPrice()
    {
        return $this->orderLine->getDisplayPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayAmount()
    {
        return $this->orderLine->getDisplayAmount();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayDiscountAmount()
    {
        return $this->orderLine->getDisplayDiscountAmount();
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->orderLine, $name], $arguments);
    }
}
