<?php

namespace AppBundle\Decorator\Factory;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProductDecoratorFactory
 * @package AppBundle\Decorator\Factory
 */
class ProductDecoratorFactory
{
    use ContainerAwareTrait;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Product $product
     * @return ProductDecorator|Product
     */
    public function get(Product $product)
    {
        $decoratedProduct = new ProductDecorator();
        $decoratedProduct->setContainer($this->container);
        $decoratedProduct->setProduct($product);

        return $decoratedProduct;
    }
}
