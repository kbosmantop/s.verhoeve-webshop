<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SubscriptionDecorator
{
    use ContainerAwareTrait;

    private $assortiment;

    public function setAssortiment(Assortment $assortment)
    {
        $this->assortiment = $assortment;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->assortiment, $name], $arguments);
    }
}
