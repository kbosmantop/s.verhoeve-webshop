<?php

namespace AppBundle\Decorator;

use AppBundle\DBAL\Types\ProductTypeType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Site\Site;
use AppBundle\Manager\StockManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\PersistentCollection;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;

/**
 * Class ProductDecorator
 * @package AppBundle\Decorator
 */
class ProductDecorator
{
    use ContainerAwareTrait;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var ProductDecorator
     */
    private $parent;

    /**
     * @var ArrayCollection|ProductPropertyDecorator[]
     */
    private $productProperties;

    /**
     * @var ArrayCollection|ProductDecorator[]
     */
    private $variations;

    /**
     * ProductDecorator constructor.
     */
    public function __construct()
    {
        $this->productProperties = new ArrayCollection();
        $this->variations = new ArrayCollection();
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        // If product is of type variation, convert variation products to product decorator
        if ($product->getType() === ProductTypeType::PRODUCT_TYPE_VARIATION) {
            foreach ($product->getVariations() as $variation) {
                $decoratedProduct = new ProductDecorator();
                $decoratedProduct->setContainer($this->container);
                $decoratedProduct->setProduct($variation);
                $decoratedProduct->setParentProduct($this);

                $this->variations->add($decoratedProduct);
            }
        } elseif ($product->getParent() && $product->getParent()->getType() === ProductTypeType::PRODUCT_TYPE_VARIATION) {
            $this->parent = $product->getParent();
        }

        foreach ($this->product->getProductProperties() as $productProperty) {
            $decoratedProductProperty = $this->container->get('app.product_property_decorator');
            $decoratedProductProperty->setProductProperty($productProperty);

            $this->productProperties->add($decoratedProductProperty);

//
//            if ($productProperty->getProductgroupProperty()->getDecoratorService() && $this->container->has($productProperty->getProductgroupProperty()->getDecoratorService()) && (bool)($productPropertyDecorator = $this->container->get($productProperty->getProductgroupProperty()->getDecoratorService())) === false) {
//                $productPropertyDecorator = $this->container->get("app.product_property_decorator");
//            }
//
////            if (isset($productPropertyDecorator)) {
//                print "x";
//                $productPropertyDecorator->setProductProperty($productProperty);
//
////                array_push($this->productProperties, $productPropertyDecorator);
//  //          }
        }

        return $this;
    }

    /**
     * @param ProductDecorator $decoratedProduct
     */
    public function setParentProduct(ProductDecorator $decoratedProduct)
    {
        $this->parent = $decoratedProduct;
    }

    /**
     * @return ProductDecorator|Product
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @deprecated
     *
     * @param null      $language
     * @param Site|null $site
     * @param null      $referenceType
     *
     * @return null|string
     */
    public function getUrl($language = null, Site $site = null, $referenceType = null)
    {
        return $this->product->getUrl($language, $site, $referenceType);
    }

    /**
     * @deprecated
     *
     * @param null      $language
     * @param Site|null $site
     *
     * @return null|string
     */
    public function getAbsoluteUrl($language = null, Site $site = null)
    {
        return $this->product->getAbsoluteUrl($language, $site);
    }

    /**
     * @deprecated
     */
    public function getPrice()
    {
        return $this->product->getPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getPriceIncl()
    {
        return $this->product->getPriceIncl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayPrice()
    {
        return $this->product->getDisplayPrice();
    }

    /**
     * @deprecated
     */
    public function getMinPriceIncl()
    {
        return $this->product->getMinPriceIncl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayMinPrice()
    {
        return $this->product->getDisplayMinPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getMaxPriceIncl()
    {
        return $this->product->getMaxPriceIncl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayMaxPrice()
    {
        return $this->product->getDisplayMaxPrice();
    }

    /**
     * @deprecated
     * @return null|string
     */
    public function getStepMinMaxPriceIncl()
    {
        return $this->product->getStepMinMaxPriceIncl();
    }

    /**
     * @deprecated
     */
    public function getDisplayStepMinMaxPrice()
    {
        return $this->product->getDisplayStepMinMaxPrice();
    }

    /**
     * @deprecated
     *
     * @param $price
     *
     * @return array
     */
    public function checkMinMaxPrice($price)
    {
        return $this->product->checkMinMaxPrice($price);
    }

    /**
     * @deprecated
     * @return ProductImage|ProductImage[]|PersistentCollection
     */
    public function getImages()
    {
        return $this->product->getImages();
    }

    /**
     * @deprecated
     * @return null
     */
    public function generateUrl()
    {
        return $this->product->generateUrl();
    }

    /**
     * @deprecated
     *
     * @param $fallback
     *
     * @return ProductImage || null
     */
    public function getCategoryImage($fallback = false)
    {
        return $this->product->getCategoryImage($fallback);
    }

    /**
     * @deprecated
     * @return null|static
     */
    public function getUntaggedImages()
    {
        return $this->product->getUntaggedImages();
    }

    /**
     * @deprecated
     */
    public function getPersonalizationDecorator()
    {
        return $this->product->getProductgroupProperty('personalization');
    }

    /**
     * @deprecated
     * @return bool
     */
    public function hasVariations()
    {
        return $this->product->hasVariations();
    }

    /**
     * @deprecated
     */
    public function getVariations()
    {
        return $this->product->getVariations();
    }

    /**
     * @param string $serviceName
     * @return bool
     */
    public function hasProductPropertyDecoratorService(string $serviceName)
    {
        foreach ($this->productProperties as $productProperty) {
            if (null !== ($decoratorService = $productProperty->getProductgroupProperty()->getDecoratorService()) && $decoratorService === $serviceName) {
                return true;
            }
        }

        return false;
    }

    /**
     * @deprecated
     * @return bool
     * @throws \Exception
     */
    public function isProductPersonalizationAvailable()
    {
        return $this->product->isProductPersonalizationAvailable();
    }

    /**
     * @return ProductCardDecorator|ArrayCollection|null
     */
    public function getCards()
    {
        if (null === $this->getCardsDecorator()) {
            return null;
        }

        return $this->getCardsDecorator()->getCards();
    }

    /**
     * @return ProductCardDecorator|null
     */
    public function getCardsDecorator()
    {
        if (!$this->product->getCards()) {
            return null;
        }

        if (!isset($this->cardDecorator)) {
            $cardsAssortment = $this->container->get('app.assortment.factory')->get($this->product->getCards());

            $this->cardDecorator = $this->container->get('app.product.card_decorator');
            $this->cardDecorator->setCards($cardsAssortment);
        }

        return $this->cardDecorator;
    }

    /**
     * @deprecated
     * @return Product
     */
    public function get()
    {
        return $this->product;
    }

    /**
     * @param        $property
     * @param string $language
     * @return bool
     */
    public function hasProductProperty($property, $language = 'nl')
    {
        if (!$this->productProperties) {
            return false;
        }

        foreach ($this->productProperties as $productProperty) {
            if ($productProperty->getProductgroupProperty()->translate($language, false)->getSlug() === $property) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param        $property
     * @param string $language
     * @return ProductPropertyDecorator|bool|mixed
     */
    public function getProductProperty($property, $language = 'nl')
    {
        if (!$this->productProperties) {
            return false;
        }

        foreach ($this->productProperties as $productProperty) {
            if ($productProperty->getProductgroupProperty()->translate($language, false)->getSlug() === $property) {
                return $productProperty;
            }
        }

        return false;
    }

    /**
     * @param $serviceName
     * @return ProductPropertyDecorator|mixed
     * @throws \RuntimeException
     */
    public function getProductPropertyDecoratorService($serviceName)
    {
        foreach ($this->productProperties as $productProperty) {
            if (null !== ($decoratorService = $productProperty->getProductgroupProperty()->getDecoratorService()) && $decoratorService === $serviceName) {
                return $productProperty;
            }
        }

        throw new \RuntimeException(sprintf('Decorator Service %s not found', $serviceName));
    }

    /**
     * @return ArrayCollection|ProductPropertyDecorator[]
     */
    public function getProductProperties()
    {
        return $this->productProperties;
    }

    /**
     * @deprecated
     *
     * @param $key string
     *
     * @return ProductProperty
     * @throws \Exception
     */
    public function findProductProperty($key)
    {
        return $this->product->findProductProperty($key);
    }

    /**
     * @return bool
     */
    public function getPurchasable()
    {
        if ($this->product->getParent()) {
            return $this->product->getParent()->getPurchasable();
        }

        return $this->product->getPurchasable();
    }

    /**
     * Creates a form to post a product to the checkout process
     *
     * @param $options array
     *
     * @return Form The form
     */
    public function createForm($options = [])
    {
        $defaultOptions = [
            'method' => 'POST',
            'attr' => [
                'class' => 'form',
                'novalidate' => 'novalidate',
            ],
        ];

        $options = $defaultOptions + $options;

        $form = $this->container->get('form.factory')->create(FormType::class, [],
            $options);

        if ($this->hasVariations()) {
            $choices = [];

            foreach ($this->getVariations() as $variation) {
                // @TODO FIX / REMOVE THE FOLLOWING CODE
                $choices[$variation->getId()] = $variation->getDisplayName() . ': ' . $variation->getDisplayPrice();
            }

            $form->add('variation', ChoiceType::class, [
                'choices' => array_flip($choices),
                'choice_translation_domain' => false,
            ]);
        }

        $form->add('quantity', ChoiceType::class, [
            'choices' => array_combine(range(1, 99), range(1, 99)),
            'choice_translation_domain' => false,
        ]);

        $form->add('submit', SubmitType::class,
            [
                'label' => 'label.add_to_cart',
            ]
        );

        return $form;
    }

    /**
     * @deprecated
     * @return array
     * @throws DBALException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    public function getOrderableQuantities()
    {
        return $this->product->getOrderableQuantities();
    }

    /**
     * @deprecated
     *
     * @return integer
     *
     * @throws OptimisticLockException
     */
    public function getPhysicalStock()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getPhysicalStock($this->product);
    }

    /**
     * @deprecated
     *
     * @return integer
     *
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getVirtualStock()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getVirtualStock($this->product);
    }

    /**
     * @deprecated
     * @return integer
     * @throws OptimisticLockException
     */
    public function getStockThreshold()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getThreshold($this->product);
    }

    /**
     * @param $fallback
     *
     * @deprecated
     * @return ProductImage || null
     */
    public function getMainImage($fallback = false)
    {
        return $this->product->getMainImage();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return \call_user_func_array([$this->product, $name], $arguments);
    }
}
