<?php

namespace AppBundle\Decorator\ProductProperty;

use AppBundle\Decorator\ProductPropertyDecorator;

/**
 * Class PersonalizationDecorator
 *
 * @package AppBundle\Decorator\ProductProperty
 */
class PersonalizationDecorator extends ProductPropertyDecorator
{
    public function getPersonalizationProduct()
    {
//        $cardProduct = $this->container->get('doctrine')->getRepository(Product::class)->findOneBySku('CARD');
//
//        if (!$cardProduct) {
//            return false;
//        }
//
//        $cartProductDecorator = $this->container->get('app.product.factory')->get($cardProduct);
//
//        return $cartProductDecorator;
    }

//    public function getMinimumPrice()
//    {
//        $prices = [];
//
//        foreach ($this->getCardProduct()->getVariations() as $product) {
//            array_push($prices, $product->getPrice());
//        }
//
//        asort($prices);
//
//        return array_shift($prices);
//    }
//
//    public function getMinimumDisplayPrice()
//    {
//        return number_format($this->getMinimumPrice(), 2, ",", ".");
//    }
//
//    public function getMinimumPriceIncl()
//    {
//        $prices = [];
//
//        foreach ($this->getCardProduct()->getVariations() as $product) {
//            array_push($prices, $product->getPriceIncl());
//        }
//
//        asort($prices);
//
//        return array_shift($prices);
//    }
//
//    public function getMinimumDisplayPriceIncl()
//    {
//        return number_format($this->getMinimumPriceIncl(), 2, ",", ".");
//    }

    public function getTwigFile()
    {
        return 'AppBundle:blocks/Product/options:personalization.html.twig';
    }
}