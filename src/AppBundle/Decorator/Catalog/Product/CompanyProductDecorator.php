<?php

namespace AppBundle\Decorator\Catalog\Product;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Geography\Country;
use AppBundle\Interfaces\Catalog\Product\ProductDecoratorInterface;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class CompanyProductDecorator
 * @package AppBundle\Decorator
 */
class CompanyProductDecorator implements ProductInterface, ProductDecoratorInterface
{
    /**
     * @var CompanyProduct
     */
    private $companyProduct;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * CompanyProductDecorator constructor.
     * @param CompanyProduct $companyProduct
     */
    public function __construct(CompanyProduct $companyProduct)
    {
        $this->companyProduct = $companyProduct;
        $this->propertyAccessor = new PropertyAccessor();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $method = $name;
        $relProduct = $this->companyProduct->getRelatedProduct();

        if (method_exists($relProduct, $method)) {
            $method = $name;
        } elseif (method_exists($relProduct, 'get' . ucfirst($method))) {
            $method = 'get' . ucfirst($method);
        } elseif (method_exists($relProduct, 'is' . ucfirst($method))) {
            $method = 'is' . ucfirst($method);
        } elseif (method_exists($relProduct, 'has' . ucfirst($method))) {
            $method = 'has' . ucfirst($method);
        }

        $value = \call_user_func_array([$this->companyProduct, $method], $arguments);

        if (null !== $value) {
            return $value;
        }

        return \call_user_func_array([$this->companyProduct->getRelatedProduct(), $method], $arguments);
    }

    /**
     * @return CompanyProduct|ProductInterface
     */
    public function getEntity()
    {
        return $this->companyProduct;
    }

    public function getVatGroups()
    {
        return $this->companyProduct->getVatGroups()->isEmpty() ? $this->companyProduct->getRelatedProduct()->getVatGroups() : $this->companyProduct->getVatGroups();
    }

    /**
     * @param Country $country
     * @return \AppBundle\Entity\Finance\Tax\VatGroup|null
     */
    public function getVatGroupForCountry(Country $country)
    {
        return $this->companyProduct->getVatGroupForCountry($country) ?? $this->companyProduct->getRelatedProduct()->getVatGroupForCountry($country);
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->companyProduct->getPrice() ?? $this->companyProduct->getRelatedProduct()->getPrice();
    }

    /**
     * @return CompanyProductDecorator|CompanyProduct
     */
    public function getParent(): ?ProductInterface
    {
        return $this->companyProduct->getParent();
    }

    /**
     * @param null $locale
     * @return object
     */
    public function translate($locale = null)
    {
        $translation = $this->companyProduct->translate($locale);
        $relatedtranslation = $this->companyProduct->getRelatedProduct()->translate($locale);

        return new class($translation, $relatedtranslation) extends ProductTranslation {
            /** @var ProductTranslation $translation */
            protected $translation;
            /** @var ProductTranslation $relatedTranslation */
            protected $relatedTranslation;

            /**
             *  constructor.
             * @param $translation
             * @param $relatedTranslation
             */
            public function __construct($translation, $relatedTranslation)
            {
                $this->translation = $translation;
                $this->relatedTranslation = $relatedTranslation;
            }

            /**
             * @return string
             */
            public function getTitle()
            {
                return !empty($this->translation->getTitle()) ? $this->translation->getTitle() : $this->relatedTranslation->getTitle();
            }

            /**
             * @return string
             */
            public function getDescription()
            {
                return !empty($this->translation->getDescription()) ? $this->translation->getDescription() : $this->relatedTranslation->getDescription();
            }

            /**
             * @return string
             */
            public function getShortDescription()
            {
                return !empty($this->translation->getShortDescription()) ? $this->translation->getShortDescription() : $this->relatedTranslation->getShortDescription();
            }

            /**
             * @return string
             */
            public function getSlug()
            {
                return !empty($this->translation->getSlug()) ? $this->translation->getSlug() : $this->relatedTranslation->getSlug();
            }
        };
    }
}
