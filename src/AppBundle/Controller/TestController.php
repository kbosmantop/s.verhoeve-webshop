<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package AppBundle\Controller
 *
 * @Route("/test")
 */
class TestController extends Controller
{
    /**
     * @Route("/", condition="'%kernel.environment%' === 'dev'")
     */
    public function index(): Response
    {
        exit;
    }
}
