<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Catalog\Product\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SiteController
 * @package AppBundle\Controller\Company
 */
class SiteController extends Controller
{

    /**
     * @Template()
     * @param Company $company
     *
     * @return array
     */
    public function indexAction(Company $company)
    {
        return [
            'company' => $company,
        ];
    }

    /**
     * @Template("@App/Assortment/index.html.twig")
     * @param Company $company
     * @param Assortment $assortment
     *
     * @return array
     */
    public function assortmentAction(Company $company, Assortment $assortment)
    {
        $siteLogo = null;
        $siteColor = null;

        if($company->getSite()->getCustomStyle() && $company->getSite()->getCustomStyle()->getLogo()) {
            $siteLogo = $company->getSite()->getCustomStyle()->getLogo()->getPath();
        }

        if($company->getSite()->getCustomStyle()) {
            $siteColor = $company->getSite()->getCustomStyle()->getBodyBackgroundColor();
        }

        return [
            'maxPrice' => $assortment->getMinimumPrice(),
            'minPrice' => $assortment->getMaximumPrice(),
            'company' => $company,
            'assortment' => $assortment,
            'site' => $company->getSite()->getDefaultSite(),
            'site_slogan' => '',
            'site_logo' => $siteLogo,
            'site_color' => $siteColor,
        ];
    }

    /**
     * @Template("@App/Product/index.html.twig")
     * @param Company $company
     * @param Product $product
     *
     * @return array
     */
    public function productAction(Company $company, Product $product) {
        $siteLogo = null;
        $siteColor = null;

        if($company->getSite()->getCustomStyle() && $company->getSite()->getCustomStyle()->getLogo()) {
            $siteLogo = $company->getSite()->getCustomStyle()->getLogo()->getPath();
        }

        if($company->getSite()->getCustomStyle()) {
            $siteColor = $company->getSite()->getCustomStyle()->getBodyBackgroundColor();
        }

        return [
            'company' => $company,
            'product' => $product,
            'site' => $company->getSite()->getDefaultSite(),
            'site_slogan' => 'Custom site',
            'site_logo' => $siteLogo,
            'site_color' => $siteColor,
        ];
    }

}