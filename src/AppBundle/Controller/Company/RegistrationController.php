<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Company\CustomCustomerFormType;
use AppBundle\Services\SiteService;
use JMS\JobQueueBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegistrationController
 * @package AppBundle\Controller\Company
 */
class RegistrationController extends Controller
{

    /**
     * @Template()
     * @param Company $company
     * @param Request $request
     *
     * @return array
     */
    public function indexAction(Company $company, Request $request)
    {
        $customer = new Customer();
        $customer->setCompany($company);

        $form = $this->createCustomerForm($customer, $request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->container->get('doctrine')->getManager();

            $customer = $form->getData();
            $em->persist($customer);
            $em->flush();

            $this->createRegistrationMailJob($customer);
            $this->get('session')->getFlashBag()->add('frontend.success', 'customer.registration.success');
        }

        return [
            'company' => $company,
            'entity' => $customer,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Custom Custom entity.
     *
     * @param Customer $entity The entity
     * @param Request $request
     *
     * @return Form The form
     */
    private function createCustomerForm(Customer $entity, Request $request)
    {
        $form = $this->createForm(CustomCustomerFormType::class, $entity, [
            'action' => $request->getRequestUri(),
            'method' => 'POST',
            'request' => $request,
        ]);

        $customRegistrationFields = $entity->getCompany()->getSite()->getCustomFields();

        if ($customRegistrationFields) {
            /** @var Company\CompanySiteCustomField $field */
            foreach ($customRegistrationFields as $field) {
                $fieldOptions = [];

                switch ($field->getType()) {
                    case 'choice':
                        $fieldValues = explode(PHP_EOL, $field->translate(false)->getValues());
                        $fieldChoices = array_combine($fieldValues, $fieldValues);

                        $fieldType = ChoiceType::class;
                        $fieldOptions = [
                            'placeholder' => 'label.choice',
                            'translation_domain' => 'messages',
                            'choices' => $fieldChoices,
                            'multiple' => $field->getMultiple(),
                        ];

                        break;
                    default:
                        $fieldType = TextType::class;
                        break;
                }

                $fieldOptions = array_merge([
                    'label' => $field->translate()->getLabel(),
                    'mapped' => false,
                    'required' => $field->getRequired(),
                ], $fieldOptions);

                $form->add('custom_reg_field_' . $field->getId(), $fieldType, $fieldOptions);
            }
        }

        $form->add('submit', SubmitType::class, ['label' => $this->get('translator')->trans('customer.register.submit')]);

        return $form;
    }

    /**
     * @param object $entity
     * @throws \Exception
     */
    private function createRegistrationMailJob($entity)
    {
        $job = new Job('topbloemen:site:send-registration-mail', [
            $entity->getId(),
            $this->container->get(SiteService::class)->determineSite()->getId(),
        ], true, 'registration_mail');

        $job->addRelatedEntity($entity);
        $job->addRelatedEntity($this->get('app.domain')->getDomain());

        $this->container->get('job.manager')->addJob($job);
        $this->container->get('doctrine')->getManager()->flush();
    }

}
