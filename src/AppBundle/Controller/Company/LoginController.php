<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 * @package AppBundle\Controller\Company
 */
class LoginController extends Controller
{
    /**
     * @Template()
     * @param Company $company
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function indexAction(Company $company, Request $request)
    {
        $authError = null;

        if ($request->getMethod() == 'POST') {
            $username = $request->request->get('_username');
            $password = $request->request->get('_password');

            $userExists = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
                'username' => $username,
                'company' => $company,
            ]);

            // Login
            if ($userExists && $this->get('app.user_provider')->loginUser($username, $password)) {
                return $this->redirectToRoute('app_home_index');
            }

            $authError = 'customer.login.invalid_combination';
        }

        return [
            'company' => $company,
            'authError' => $authError,
        ];
    }

}