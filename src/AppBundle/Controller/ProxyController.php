<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Common\Parameter\ParameterEntityFile;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Interfaces\DesignerControllerInterface;
use AppBundle\Traits\DesignerTrait;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProxyController
 * @package AppBundle\Controller
 */
class ProxyController extends Controller
{
    /**
     * @Route("/common/files/{prefix}/{parameterEntityFile}/{filename}", name="proxy_common_files", methods={"GET"})
     * @ParamConverter("parameterEntityFile", options={"mapping": { "parameterEntityFile" = "uuid"}})
     * @param Request             $request
     * @param ParameterEntityFile $parameterEntityFile
     * @return StreamedResponse
     */
    public function proxyAction(Request $request, ParameterEntityFile $parameterEntityFile)
    {
        $objectStoreUrl = $this->getObjectStoreUrl();
        $proxyUrl = $objectStoreUrl . $request->getPathInfo();

        if($request->getPathInfo() !== $parameterEntityFile->getPath()) {
            throw new NotFoundHttpException('Common file not found.');
        }

        return $this->get('topgeschenken.proxy')->process($proxyUrl);
    }

    /**
     * @param bool $private
     * @return string
     */
    private function getObjectStoreUrl($private = false): string
    {
        $url = $this->getParameter('openstack_object_store');
        $url .= $private === true ? '/private' : '/public';

        return $url;
    }
}
