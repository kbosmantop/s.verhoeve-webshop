<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Form\Checkout\PaymentType;
use AppBundle\Manager\Order\OrderCollectionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaymentController
 * @package AppBundle\Controller
 */
class PaymentController extends Controller
{
    /**
     * @Route("/betaling/{uuid}", methods={"GET"})
     * @Template()
     * @param Request $request
     * @param         $uuid
     *
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function indexAction(Request $request, $uuid)
    {
        $payment = $this->getPayment($uuid);

        $this->get('app.gateway.adyen')->processResponse($payment, $request);

        $route = $this->getRoute($payment);

        if ($route) {
            return $this->redirectToRoute($route, ['payment' => $uuid]);
        }

        return [];
    }

    /**
     * @Route("/betaling/{uuid}", methods={"POST"})
     *
     * @param $uuid
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function pendingAction($uuid)
    {
        $payment = $this->getPayment($uuid);

        $route = $this->getRoute($payment);

        if ($route) {
            return new JsonResponse([
                'redirect' => $this->get('router')->generate($route),
            ]);
        } else {
            return new JsonResponse([
                'redirect' => null,
            ]);
        }
    }

    /**
     * @Route("/betaling/{uuid}/authorise3d", methods={"POST"})
     *
     * @param Request $request
     * @param         $uuid
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function authorise3DAction(Request $request, $uuid)
    {
        $payment = $this->getPayment($uuid);

        $this->container->get('app.payment.gateway.adyen')->authorise3D($payment, $request);

        return $this->redirectToRoute($this->getRoute($payment), [
            'payment' => (string)$payment->getUuid(),
        ]);
    }

    /**
     * @Route("/betaallink/{payment}", methods={"GET"})
     * @ParamConverter("payment", options={"mapping": { "payment" = "uuid"}})
     * @Template()
     * @param Payment $payment
     * @return array
     * @throws PaymentException
     * @throws \ReflectionException
     */
    public function paymentLinkAction(Payment $payment)
    {
        $form = $this->createForm(PaymentType::class);

        $methods = $this->get('app.payment')->getMethods($payment->getOrderCollection());
        $availableMethodsForPayment = $payment->getMetadata()['request']['methods'];
        foreach ($methods as $method) {
            if(!\in_array($method->getCode(), $availableMethodsForPayment, true)) {
                continue;
            }

            $attr = [];
            $paymentProductDecorator = null;

            /** @var Paymentmethod $paymentMethod */
            $paymentMethod = $method->getEntity();

            if ($paymentMethod && $paymentMethod->getProduct()) {
                $paymentProductDecorator = $this->get('app.product.factory')->get($paymentMethod->getProduct());

                $attr['data-payment-costs-name'] = $paymentProductDecorator->translate()->getTitle();
                $attr['data-payment-costs-amount'] = $paymentProductDecorator->getDisplayPrice();
            }

            if ($method->getDescription()) {
                $attr['data-has-description'] = true;
            }

            $form->add($method->getCode(), $method->getFormClass(), [
                'label' => $method->getName() . ($paymentProductDecorator ? ' <span> - &euro; ' . $paymentProductDecorator->getDisplayPrice() . '</span>' : null) . ($method->getDescription() ? '<br /> <span style="font-style: italic; font-size: 11px;">' . $method->getDescription() . '</span>' : null),
                'attr' => $attr,
            ]);
        }

        $form->add('cart_submit', SubmitType::class, [
            'label' => 'Betalen'
        ]);

        return array_merge($this->container->get('app.payment')->getAdyenCseParams(), [
            'metadata' => $payment->getMetadata(),
            'payment' => $payment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/betaallink/{payment}", methods={"POST"})
     * @ParamConverter("payment", options={"mapping": { "payment" = "uuid"}})
     * @param Request $request
     * @param Payment $payment
     * @return bool|RedirectResponse|Response
     * @throws \Exception
     */
    public function process(Request $request, Payment $payment)
    {
        $paymentMethodCode = !empty($request->get('cart')['paymentMethod']) ? $request->get('cart')['paymentMethod'] : null;
        $paymentMethod = $this->getDoctrine()->getRepository(Paymentmethod::class)->findOneBy(['code' => $paymentMethodCode]);

        $payment->setPaymentmethod($paymentMethod);

        $metadata = $payment->getMetadata();

        foreach ($request->get('site_checkout_payment') as $paymentMethodData) {
            if(\is_array($paymentMethodData)) {
                foreach($paymentMethodData as $key => $value) {
                    if($key) {
                        $metadata[$key] = $value;
                    }
                }
            }
        }

        $payment->setMetadata($metadata);

        $paymentMethodClass = $this->get('app.payment_service')->getMethod($payment->getOrderCollection(), $paymentMethodCode);
        if($paymentMethodClass->getGateway()) {
            $this->getDoctrine()->getManager()->flush();
            $result = $paymentMethodClass->getGateway()->processPayment($payment);

            if ($result instanceof Response) {
                return $result;
            }

            if (\is_bool($result) && $result) {
                return $this->redirectToRoute('app_checkout_success');
            }
        }

        return false;
    }

    /**
     * @param $uuid
     *
     * @return Payment
     * @throws \Exception
     */
    private function getPayment($uuid)
    {
        $payment = $this->getDoctrine()->getManager()->getRepository(Payment::class)->findOneBy([
            'uuid' => $uuid,
        ]);

        if (!$payment) {
            throw new \Exception("Payment '" . $uuid . "' not found");
        }

        return $payment;
    }

    /**
     * @param Payment $payment
     *
     * @return null|string
     * @throws \Exception
     */
    private function getRoute(Payment $payment)
    {
        switch ($payment->getStatus()->getId()) {
            case 'cancelled':
            case 'failed':
            case 'refused';
                return 'app_checkout_sender';
            case 'authorized':
                if ($this->get(OrderCollectionManager::class)->calculateOutstanding($payment->getOrderCollection()) == 0) {
                    return 'app_checkout_success';
                }

                return 'app_checkout_sender';
            case 'pending':
                return null;
                break;
            default:
                return false;
        }
    }
}
