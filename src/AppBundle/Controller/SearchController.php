<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package AppBundle\Controller
 * @Route("/zoeken")
 */
class SearchController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     * @Template()
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function index(Request $request): array
    {
        $searchQuery = $request->get('q');

        $products = [];
        if(!empty(trim($searchQuery))) {
            $products = $this->get('app.search.product')->getProductsByTitle($request->get('q'));
        }

        return [
            'products' => $products,
            'query' => $request->get('q')
        ];
    }
}
