<?php

namespace AppBundle\Controller\Seo;

use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Redirect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class RedirectController extends Controller
{
    public function indexAction(Redirect $redirect)
    {
        $twig = $this->container->get("twig");

        switch ($redirect->getStatus()) {
            case 410:
            case 451:
                return new Response($twig->render($this->getTemplate($redirect->getStatus()), [
                    'page' => $this->getPage(),
                ]), $redirect->getStatus());
            case 301:
            case 302:
            case 307:
                return new RedirectResponse($redirect->getNewUrl(), $redirect->getStatus());
        }

        throw new \Exception(sprintf("Unexpected response code '%d' ", $redirect->getStatus()));
    }

    /**
     * @param $statusCode
     *
     * @return string
     */
    private function getTemplate($statusCode)
    {
        $template = 'AppBundle:Exception:error' . $statusCode . '.html.twig';

        if (!$this->get('twig')->getLoader()->exists($template)) {
            $template = 'AppBundle:Exception:error.html.twig';
        }

        return $template;
    }

    /**
     * @return Page
     */
    private function getPage()
    {
        $qb = $this->getDoctrine()->getManager()->getRepository(Page::class)->createQueryBuilder("page");
        $qb->select(["page", "page_translation"]);
        $qb->leftJoin("page.translations", "page_translation");
        $qb->andWhere("page_translation.slug = :slug");
        $qb->setParameters([
            "slug" => "foutmelding",
        ]);

        $page = $qb->getQuery()->getOneOrNullResult();

        if (!$page) {
            $page = new Page();
        }

        return $page;
    }
}
