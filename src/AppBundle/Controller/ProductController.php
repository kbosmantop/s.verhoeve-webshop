<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Form\ProductFormType;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use AppBundle\Manager\StockManager;
use AppBundle\Services\AccessibilityService;
use AppBundle\Services\CartService;
use AppBundle\Services\ProductFormHandlerService;
use AppBundle\Services\ProductFormValidationService;
use AppBundle\Services\ProductPriceDifferencesService;
use AppBundle\Services\SupplierManagerService;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Translation\Common\Model\Message;

/**
 * Class ProductController
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{
    /**
     * @var StockManager
     */
    private $stockManager;

    /**
     * @var SupplierManagerService
     */
    private $supplierManager;

    /**
     * @var ProductManager
     */
    protected $productManager;

    /**
     * @var ProductPriceManager
     */
    protected $productPriceManager;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ProductFormHandlerService
     */
    private $productFormHandlerService;

    /**
     * @var ProductFormValidationService
     */
    private $productFormValidationService;

    /**
     * ProductController constructor.
     *
     * @param StockManager $stockManager
     * @param SupplierManagerService $supplierManager
     * @param ProductManager $productManager
     * @param ProductPriceManager $productPriceManager
     * @param CartService $cartService
     * @param RouterInterface $router
     * @param ProductFormHandlerService $productFormHandlerService
     * @param ProductFormValidationService $productFormValidationService
     */
    public function __construct(
        StockManager $stockManager,
        SupplierManagerService $supplierManager,
        ProductManager $productManager,
        ProductPriceManager $productPriceManager,
        CartService $cartService,
        RouterInterface $router,
        ProductFormHandlerService $productFormHandlerService,
        ProductFormValidationService $productFormValidationService
    ) {
        $this->stockManager = $stockManager;
        $this->supplierManager = $supplierManager;
        $this->productManager = $productManager;
        $this->productPriceManager = $productPriceManager;
        $this->cartService = $cartService;
        $this->router = $router;
        $this->productFormHandlerService = $productFormHandlerService;
        $this->productFormValidationService = $productFormValidationService;
    }

    /**
     * @return array
     */
    public static function getTranslationMessages(): array
    {
        return [
            new Message('product.card.not_blank', 'validators'),
            new Message('product.card_text.not_blank', 'validators'),
            new Message('product.card.not_valid', 'validators'),
            new Message('product.designer.not_valid', 'validators'),
            new Message('product.manual_price.not_blank', 'validators'),
            new Message('product.manual_price.not_valid', 'validators'),
            new Message('product.manual_price.step_not_valid', 'validators'),
            new Message('product.manual_price.step_not_valid', 'validators'),
            new Message('product.out-of-stock', 'validators'),
            new Message('product.not-available-at-the-moment', 'validators'),
            new Message('cart.product_successfully_added'),
        ];
    }

    /**
     * @param Product|ProductInterface $product
     * @param Request $request
     *
     * @return Response
     * @throws InvalidArgumentException
     * @throws DBALException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function indexAction(ProductInterface $product, Request $request): Response
    {
        $cartPersonalizationProduct = null;
        $cartCardProduct = null;
        $variationId = null;
        $invalidProductCombination = false;
        $redirectUrl = null;
        $selectedProduct = null;
        $template = 'AppBundle:Product:index.html.twig';

        if (!$this->get(AccessibilityService::class)->isProductAccessibleByUser($product)) {
            throw new AccessDeniedException();
        }

        if ($product->hasVariations() && $request->query->get('productId')) {
            $variationId = $request->query->get('productId');
        }

        if (null !== ($parent = $product->getParent())) {
            $product = $parent;
        }

        if (
            null !== ($productGroup = $product->getProductgroup())
            && null !== ($productTemplate = $productGroup->getProductTemplate())
        ) {
            $template = $productTemplate;
        }

        if (null !== ($productTemplate = $product->getTemplate())) {
            $template = $productTemplate;
        }

        //if entrance point is base product and only has one variation, set selected variation to that one
        if (null === $product->getParent() && !$product->getVariations()->isEmpty()) {
            $selectedProduct = $this->productManager->decorateProduct($product->getVariations()->current());
        } else {
            $selectedProduct = $product;
        }

        //make sure were working with the entity and not a decorated product
        $productEntity = $this->productManager->getEntity($product);
        $selectedProductEntity = $this->productManager->getEntity($selectedProduct);

        $letterSettings = ['active' => false, 'show' => true];
        $productLetter = null;

        if (null !== $product->getLetter()) {
            $productLetter = $product->getLetter();

            /** @var ProductTranslation $productTranslate */
            $productTranslate = $productLetter->translate();
            $letterSettings = [
                'active' => true,
                'show' => false,
                'title' => $productTranslate->getTitle(),
                'description' => $productTranslate->getDescription(),
                'price' => $this->productPriceManager->getPrice($productLetter),
            ];
        }

        $isOrderable = $this->stockManager->isOrderable($product);
        $hasVariationWithSupplier = $this->supplierManager->hasVariationWithSupplier($product);

        /** @var Form $form */
        $form = $this->createForm(ProductFormType::class, null, [
            'product' => $product,
            'attr' => [
                'id' => 'product_form'
            ]
        ]);

        if (false === $hasVariationWithSupplier) {
            $form->addError(new FormError($this->get('translator')->trans('label.not-available-at-the-moment')));
        } elseif (false === $isOrderable) {
            $form->addError(new FormError($this->get('translator')->trans('label.out-of-stock')));
        }

        if ($request->isMethod('POST')) {
            if (null !== ($letter = $request->request->get('letter'))) {
                $letterSettings['show'] = $letter;
            }

            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                $this->productFormValidationService->validate($form, $request);

                if ($form->isValid()) {
                    $currentOrder = $this->cartService->createOrder();
                    $this->cartService->setCurrentOrder($currentOrder);

                    // Als er gekozen is om de huidige winkelwagen te overschrijven met het nieuwe product.
                    if (!empty($request->request->get('form')['replace_cart'])) {
                        $currentOrder->removeAllLines();
                    }

                    try {
                        return $this->redirect($this->productFormHandlerService->process($form, $request));
                    } catch (Exception $e) {
                        $this->addFlash('warning', 'product.failed_to_add_product_to_cart');

                        $this->get('logger')->error($e->getMessage());

                        throw $e;
                    }
                }
            }
        }

        $cardActive = false;
        $cardId = null;
        $cardText = null;
        $cardQuantity = null;

        if ($request->isMethod('POST') && $request->request->all()) {
            $cardActive = (bool)$request->request->get('add_card');
            $cardId = $request->request->get('card');
            $cardText = $request->request->get('card_text');
            $cardQuantity = $request->request->get('card_quantity');
        }

        $this->get('app.google_analytics')->addToCartJs($product, 1);

        $ampTemplate = str_replace('.html', '.amp-html', $template);

        if ($this->get('templating')->exists($ampTemplate)) {
            $this->get('twig')->addGlobal('amphtml', $product->getAbsoluteUrl() . '?amp');

            if ($request->query->has('amp')) {
                if (\extension_loaded('newrelic')) { // Ensure PHP agent is available
                    newrelic_disable_autorum();
                }

                $template = $ampTemplate;
            }
        }

        if ($variationId && $form->has('variation')) {
            $variation = $product->getVariations()->filter(function (Product $variation) use ($variationId) {
                return $variation->getId() === (int)$variationId;
            })->current();

            $form->get('variation')->setData($variation);
        }

        $show_add_to_cart_modal = false;
        if ($this->get('session')->has('show_add_to_cart_modal')) {
            $show_add_to_cart_modal = true;
            $this->get('session')->remove('show_add_to_cart_modal');
        }

        $cardMaxQuantity = 99;
        if ($product->getMinQuantity() > 1 && $form->has('quantity') && $form->get('quantity')->getConfig()->hasOption('choices')) {
            $cardMaxQuantity = max($form->get('quantity')->getConfig()->getOption('choices'));

            if (!$cardQuantity) {
                $cardQuantity = min($form->get('quantity')->getConfig()->getOption('choices'));
            }
        }

        $priceDifferences = [];
        if ($product->hasVariations()) {
            $priceDifferences = $this->getVariationPriceDifferences($product->getVariations(),
                $product->isCombination());
        }

        $listedCards = $this->productManager->listCards($product);
        $cheapestCard = $this->productManager->getCheapestCard($product);

        return $this->render($template, [
            'form' => $form->createView(),
            'hasErrors' => $form->getErrors()->count() > 0,
            'product' => $product,
            'cheapestVariation' => $product->getCheapestVariation(),
            'isOrderable' => $this->stockManager->isOrderable($selectedProduct ?? $product),
            'hasSupplier' => !$this->supplierManager->getAvailableSuppliersForProduct($selectedProductEntity ?? $productEntity)->isEmpty(),
            'card' => [
                'active' => $cardActive,
                'id' => $cardId,
                'text' => $cardText,
                'quantitySelector' => $product->getMinQuantity() > 1,
                'maxQuantity' => $cardMaxQuantity,
                'selected' => $cardQuantity,
            ],
            'maxQuantity' => $cardMaxQuantity,
            'cards' => $listedCards,
            'minimumCardPrice' => null !== $cheapestCard ? $this->productPriceManager->getPrice($cheapestCard) : null,
            'personalization' => [
                'active' => true,
            ],
            'letter' => $letterSettings,
            'showAddToCartModal' => $show_add_to_cart_modal,
            'addToCartModalRedirect' => $this->router->generate('app_checkout_sender'),
            'invalidProductCombination' => $invalidProductCombination,
            'selectedProduct' => $selectedProduct,
            'priceDifferences' => $priceDifferences,
            'use_combination' => [
                'active' => (bool)$request->get('use_combination', false) ? 1 : 0,
            ],
            'upsells' => $this->productManager->getUpsellProducts($product),
        ]);
    }

    /**
     * @Route("/product-informatie", methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function productInfo(Request $request)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($request->get('product'));

        if ($product) {
            /** @var ProductTranslation $productTranslate */
            $productTranslate = $product->translate();

            return new JsonResponse([
                'product' => [
                    'id' => $product->getId(),
                    'title' => $productTranslate->getTitle(),
                    'displayPrice' => $product->getDisplayPrice(),
                    'description' => $productTranslate->getDescription(),
                ],
            ]);
        }

        return new JsonResponse(['message' => 'Product not found'], 404);
    }

    /**
     * @Route("/combinatie-informatie", methods={"POST"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function combinationInfoAction(Request $request)
    {
        $products = [];

        $productId = $request->get('product');
        $product = $this->getDoctrine()->getRepository(GenericProduct::class)->find($productId);

        if (null !== $product && $product->isCombination()) {
            /** @var Combination $combination */
            foreach ($product->getCombinations() as $combination) {
                $products[] = $combination->getProduct();
            }
        }

        return [
            'products' => $products,
        ];
    }

    /**
     * @param Collection $collection
     * @param bool       $useCombinationMainProduct
     * @return array
     * @throws Exception
     */
    private function getVariationPriceDifferences(Collection $collection, bool $useCombinationMainProduct = false): array
    {
        $diffServices = $this->get(ProductPriceDifferencesService::class);

        return $diffServices->getDifferences($collection, $useCombinationMainProduct);
    }
}
