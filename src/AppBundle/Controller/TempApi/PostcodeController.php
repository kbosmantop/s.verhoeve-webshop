<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use PHPExcel;
use PHPExcel_Cell_DataType;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @Route("/temp-api")
 */
class PostcodeController extends BaseController
{
    /**
     * @Route("/get-regions")
     *
     * @param Request $request
     * @return JsonResponse|StreamedResponse
     */
    public function getRegions(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        ob_end_clean();
        ob_implicit_flush();

        ini_set('memory_limit', '1G');

        /*
         * @TODO Alleen NL + BE postcodes
         * @TODO sorteren op postcode nummer (per land)
         */

        /** @var ArrayCollection|Postcode[] $postcodes */
        $postcodes = new ArrayCollection($this->getDoctrine()->getRepository(Postcode::class)->findAll());
        $postcodes = $postcodes->filter(function (Postcode $postcode) {
            return $postcode->getGeometry();
        });

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setCallback(function () use ($postcodes) {
            print '[';

            $i = 0;
            foreach ($postcodes as $postcode) {

                if ($i > 0) {
                    print ',';
                }

                print json_encode([
                    'country' => $postcode->getCountry()->getCode(),
                    'postcode' => (int)$postcode->getPostcode(),
                    'geometry' => 'POLYGON' . $postcode->getGeometry()->getMultipolygon(),
                    'lat' => $postcode->getGeometry()->getPoint()->getLatitude(),
                    'lng' => $postcode->getGeometry()->getPoint()->getLongitude(),
                ]);

                flush();

                $i++;

            }

            print ']';
        });

        return $response;
    }

    /**
     * @Route("/get-pointers")
     *
     * @param Request $request
     * @return JsonResponse|StreamedResponse
     */
    public function getPointersAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        ob_end_clean();
        ob_implicit_flush();

        /** @var ArrayCollection|Company[] $companies */

        $suppliers = $this->getSuppliers();

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setCallback(function () use ($suppliers) {
            print '[';

            foreach ($suppliers as $i => $supplier) {
                if ($i > 0) {
                    print ',';
                }

                $connector = $supplier->getSupplierConnector();

                $icon = 'topbloemen.png';

                if ($connector === 'Bakker') {
                    $icon = 'topbloemen-premium.png';

                    if ($supplier->getParent()) {
                        $icon = 'topbloemen-premium.png';
                    }
                }

                $result = [
                    'icon' => $icon,
                    'id' => $supplier->getId(),
                    'name' => $supplier->getName(),
                    'address' => null,
                    'postcode' => null,
                    'city' => null,
                    'phone' => null,
                    'country' => 'NL',
                    'lat' => null,
                    'lng' => null,
                    'connector' => $connector,
                ];

                if (null !== ($mainAddress = $supplier->getMainAddress())) {
                    $result['address'] = $mainAddress->getStreet() . ' ' . $mainAddress->getNumber();
                    $result['postcode'] = $mainAddress->getPostcode();
                    $result['city'] = $mainAddress->getCity();
                    $result['phone'] = $mainAddress->getFormattedPhoneNumber();

                    if (null !== ($point = $mainAddress->getPoint())) {
                        $result['lat'] = $point->getLatitude();
                        $result['lng'] = $point->getLongitude();
                    }
                }

                print json_encode($result);

                flush();
            }

            print ']';
        });

        return $response;
    }

    /**
     * @Route("/dataset-init")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function datasetInitAction(Request $request)
    {
        $data['regions'] = [];

        if (
            null !== ($dataSet = $request->get('dataset'))
            && \in_array($dataSet, ['CoverageMap', 'CoverageMapSecondary', 'CoverageMapAll'], true)
        ) {
            /** @var EntityRepository $entityRepository */
            $entityRepository = $this->getDoctrine()->getRepository(DeliveryArea::class);

            $qb = $entityRepository->createQueryBuilder('delivery_area');
            $qb->select('delivery_area');
            $qb->leftJoin('delivery_area.company', 'company');

            if ($dataSet !== 'CoverageMapAll') {
                $supplierConnector = ($dataSet === 'CoverageMap' ? 'Bakker' : 'BakkerMail');
                $qb->andWhere('company.supplierConnector = :supplierConnector');
                $qb->setParameter('supplierConnector', $supplierConnector);
            }

            $deliveryAreas = $qb->getQuery()->getResult();

            foreach ($deliveryAreas as $deliveryArea) {
                /** @var $deliveryArea DeliveryArea */
                $pc = $deliveryArea->getPostcode()->getPostcode();
                $supplierConnector = $deliveryArea->getCompany()->getSupplierConnector();
                $isPremium = ($supplierConnector === 'Bakker');

                // Default non premium;
                $color = ($isPremium ? $this->getLegendColor(1) : $this->getLegendColor(5));

                if (isset($data['regions'][$pc])) {
                    if ($isPremium) {
                        $data['regions'][$pc]['color'] = $color;
                        $data['regions'][$pc]['premium'] = $isPremium;
                        $data['regions'][$pc]['overlap'] = true;
                    }

                } else {
                    $data['regions'][$pc] = [
                        'country' => 'NL',
                        'postcode' => $pc,
                        'color' => $color,
                        'premium' => $isPremium,
                        'overlap' => false,
                    ];
                }
            }
        }

        $response = new JsonResponse($data);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/get-postcodes-within-bounds")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPostcodesWithinBoundsAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $postcodes = [];

        $sql = "
            SELECT postcode.postcode 
            FROM postcode_geometry 
            LEFT JOIN postcode ON postcode_geometry.id = postcode.geometry_id            
            WHERE MBRIntersects(GeomFromText('Polygon((
              :NorthEastLng :NorthEastLat,
              :SouthWestLng :NorthEastLat,
              :SouthWestLng :SouthWestLat,
              :NorthEastLng :SouthWestLat,
              :NorthEastLng :NorthEastLat    
            ))'), multipolygon);    
        ";

        $sql = str_replace([':NorthEastLat', ':NorthEastLng', ':SouthWestLat', ':SouthWestLng'], [
            (float)$request->get('NorthEast')['lat'],
            (float)$request->get('NorthEast')['lng'],
            (float)$request->get('SouthWest')['lat'],
            (float)$request->get('SouthWest')['lng'],
        ], $sql);

        /** @var \PDOStatement $stmt */
        $stmt = $this->getDoctrine()->getConnection()->prepare($sql);

        $stmt->execute();

        foreach ($stmt->fetchAll(\PDO::FETCH_COLUMN) as $postcode) {
            $postcodes[] = [
                'country' => 'NL',
                'postcode' => $postcode,
            ];
        }

        $response = new JsonResponse($postcodes);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/partner")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws \Exception
     */
    public function partnerAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $company = $this->getDoctrine()->getManager()->find(Company::class, $request->get('id'));

        $deliveryAreas = $this->getDoctrine()->getRepository(DeliveryArea::class)->findBy([
            'company' => $company,
        ]);

        $response = $this->render('@App/TempApi/Postcode/partner.html.twig', [
            'company' => $company,
            'deliveryAreas' => $deliveryAreas,
            'deliveryIntervals' => $this->getDeliveryIntervals(),
        ]);

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/get-region")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws \Exception
     */
    public function getRegionAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $postcode = $this->getPostcode($request->get('country'), $request->get('postcode'));

        $deliveryAreas = $this->getDoctrine()->getRepository(DeliveryArea::class)->findBy([
            'postcode' => $postcode,
        ]);

        $response = $this->render('@App/TempApi/Postcode/getRegion.html.twig', [
            'postcode' => $postcode,
            'deliveryAreas' => $deliveryAreas,
            'deliveryIntervals' => $this->getDeliveryIntervals(),
        ]);

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }


    /**
     * @Route("/pointer")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws \Exception
     */
    public function pointerAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $company = $this->getCompany($request->get('id'));

        /** @var DeliveryArea[] $deliveryAreas */
        $deliveryAreas = $this->getDoctrine()->getRepository(DeliveryArea::class)->findBy([
            'company' => $company,
        ]);

        $data['regions'] = [];

        foreach ($deliveryAreas as $deliveryArea) {
            $data['regions'][] = [
                'country' => 'NL',
                'postcode' => $deliveryArea->getPostcode()->getPostcode(),
                'color' => '#26AE00',
                'premium' => false,
            ];
        }

        $response = new JsonResponse($data);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/get-coverage")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws \Exception
     */
    public function getCoverangeAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        /** @var DeliveryArea[] $deliveryAreas */
        /** @var EntityRepository $entityRepository */
        $supplierConnector = ($request->get('supplierConnector') ?: 'Bakker');

        $entityRepository = $this->getDoctrine()->getRepository(DeliveryArea::class);

        $qb = $entityRepository->createQueryBuilder('delivery_area');
        $qb->select('delivery_area');
        $qb->leftJoin('delivery_area.company', 'company');
        $qb->andWhere('company.supplierConnector = :supplierConnector');

        $qb->setParameter('supplierConnector', $supplierConnector);

        $deliveryAreas = $qb->getQuery()->getResult();

        $data['regions'] = [];
        foreach ($deliveryAreas as $deliveryArea) {
            $data['regions'][] = [
                'country' => 'NL',
                'postcode' => $deliveryArea->getPostcode()->getPostcode(),
                'color' => '#26AE00',
                'premium' => false,
            ];
        }

        $response = new JsonResponse($data);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/toggle-region")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws \Exception
     */
    public function toggleRegionAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $postcode = $this->getPostcode($request->get('country'), $request->get('postcode'));
        $company = $this->getCompany($request->get('partnerId'));

        $result = $this->getDoctrine()->getRepository(DeliveryArea::class)->findOneBy([
            'postcode' => $postcode,
            'company' => $company,
        ]);

        if (!$result) {
            $deliveryArea = new DeliveryArea();
            $deliveryArea->setPostcode($postcode);
            $deliveryArea->setCompany($company);
            $deliveryArea->setDeliveryPrice((float)$this->container->getParameter('temp_api_default_delivery_cost'));
            $deliveryArea->setDeliveryInterval('P1DT7H');

            $this->getDoctrine()->getManager()->persist($deliveryArea);
            $this->getDoctrine()->getManager()->flush();
        } else {
            $this->getDoctrine()->getManager()->remove($result);
            $this->getDoctrine()->getManager()->flush();
        }

        $response = new Response('OK');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/set-partner-remark")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function setPartnerRemark(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $remark = $request->get('remark');

        $company = $this->getCompany($request->get('id'));

        if (!$company) {
            $response = new Response('ERROR', 404);
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;
        }

        $company->setInternalRemark($remark);

        $this->getDoctrine()->getManager()->flush();

        $response = new Response('OK');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/inline-edit", methods={"POST"})
     *
     * @param Request $request
     * @return Response|JsonResponse
     */
    public function inlineEditAction(Request $request)
    {
        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $result = $this->getDoctrine()->getRepository(DeliveryArea::class)->find($request->get('id'));

        if (!$result) {
            $response = new Response('ERROR', 404);
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;
        }

        switch ($request->get('handle')) {
            case 'delivery-costs':
                $result->setDeliveryPrice(round($request->get('price') / 1.06, 3));
                break;
            case 'delivery-latest-time':
                $result->setDeliveryInterval($request->get('latestTime'));
                break;
        }

        $this->getDoctrine()->getManager()->flush();

        $response = new Response('OK');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/export-postcodes")
     *
     * @param Request $request
     * @return Response|JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws \PHPExcel_Exception
     */
    public function exportPostcodesAction(Request $request)
    {

        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        $excel = new PHPExcel();

        $company = $this->getCompany($request->get('partner-id'));

        $excel->getProperties()->setCreator('Topgeschenken Nederland')
            ->setLastModifiedBy('Topbloemen Nederland')
            ->setTitle($company->getName() . ' bezorggebieden')
            ->setSubject($company->getName() . ' bezorggebieden');

        $row = 2;

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Bezorgkosten');
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Aannametijd');
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

        $excel->getActiveSheet()->getStyle('A1:E' . 1)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 8,
            ],
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('A1:E' . 1)->getFill()->applyFromArray([
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => [
                'rgb' => 'FCFCFC',
            ],
        ]);

        $excel->getActiveSheet()->getRowDimension(1)->setRowHeight(18);

        $deliveryAreas = $this->getDoctrine()->getRepository(DeliveryArea::class)->findBy([
            'company' => $company,
        ]);

        $deliveryIntervals = $this->getDeliveryIntervals();

        foreach ($deliveryAreas as $deliveryArea) {
            $excel->getActiveSheet()->getCellByColumnAndRow(1,
                $row)->setValueExplicit($deliveryArea->getPostcode()->getPostcode(),
                PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $excel->getActiveSheet()->getCellByColumnAndRow(2,
                $row)->setValueExplicit($deliveryArea->getPostcode()->getCities()[0]->translate()->getName());
            $excel->getActiveSheet()->getCellByColumnAndRow(3,
                $row)->setValueExplicit(number_format($deliveryArea->getDeliveryPrice() * 1.06, 2, '.', ','),
                PHPExcel_Cell_DataType::TYPE_NUMERIC);

            $description = $deliveryArea->getDeliveryInterval();

            foreach ($deliveryIntervals as $deliveryInterval) {
                if ($deliveryInterval['interval'] === $deliveryArea->getDeliveryInterval()) {
                    $description = $deliveryInterval['description'];

                    break;
                }
            }

            $excel->getActiveSheet()->getCellByColumnAndRow(4, $row)->setValue($description);

            $excel->getActiveSheet()->getStyle('A' . $row . ':E' . $row)->getFill()->applyFromArray([
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => [
                    'rgb' => 'B4CC8D',
                ],
            ]);

            $row++;
        }

        $excel->getActiveSheet()->getStyle('A2:C' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('D1:D' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('D2:D' . $row)->getNumberFormat()->setFormatCode('0.00');

        $excel->getActiveSheet()->getStyle('A2:E' . $row)->applyFromArray([
            'font' => [
                'size' => 9,
            ],
        ]);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($excel) {
            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save('php://output');
        });
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition',
            'attachment;filename=' . $company->getName() . ' bezorggebieden ' . date('d-m-Y') . '.xlsx');

        return $response;
    }

    /**
     * @param $country  string
     * @param $postcode integer
     * @return Postcode
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function getPostcode($country, $postcode)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getRepository(Postcode::class);

        $qb = $entityRepository->createQueryBuilder('postcode');
        $qb->leftJoin('postcode.province', 'province');
        $qb->leftJoin('province.country', 'country');
        $qb->andWhere('postcode.postcode = :postcode');
        $qb->setParameter('postcode', $postcode);
        $qb->andWhere('country.id = :country');
        $qb->setParameter('country', $country);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param $company integer
     * @return Company
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function getCompany($company)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getRepository(Company::class);

        $qb = $entityRepository->createQueryBuilder('company');
        $qb->andWhere('company.id = :company');
        $qb->setParameter('company', $company);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @return array|Company[]
     *
     * @Route("/get-suppliers")
     */
    public function getSuppliers()
    {
        $supplierGroup = $this->get('doctrine')->getRepository(SupplierGroup::class)->findOneBy(['name' => 'Bakkers']);
        if (!$supplierGroup) {
            return [];
        }

        $suppliers = [];
        /** @var Company $supplier */
        foreach ($supplierGroup->getSuppliers() as $supplier) {
            if (null !== $supplier->getMainAddress() && $supplier->getMainAddress()->getPoint()) {
                $suppliers[] = $supplier;
            }
        }
        return $suppliers;
    }

    /**
     * @return array
     */
    private function getDeliveryIntervals()
    {
        return [
            [
                'interval' => 'P1DT7H',
                'description' => '17:00 (Volgende werkdag)',
            ],
            [
                'interval' => 'PT15H',
                'description' => '09:00',
            ],
            [
                'interval' => 'PT14H',
                'description' => '10:00',
            ],
            [
                'interval' => 'PT13H',
                'description' => '11:00',
            ],
            [
                'interval' => 'PT12H',
                'description' => '12:00',
            ],
            [
                'interval' => 'PT11H',
                'description' => '13:00',
            ],
            [
                'interval' => 'PT10H',
                'description' => '14:00',
            ],
            [
                'interval' => 'PT9H',
                'description' => '15:00',
            ],
            [
                'interval' => 'PT8H',
                'description' => '16:00',
            ],
            [
                'interval' => 'PT7H',
                'description' => '17:00',
            ],
        ];
    }

    /**
     * @param null $index
     * @return string
     */
    private function getLegendColor($index = null)
    {
        switch ($index) {
            case 1:
                return '#26AE00';
                break;
            case 2:
                return '#4CFF00';
                break;
            case 3:
                return '#FFFF00';
                break;
            case 4:
                return '#FF6A00';
                break;
            case 5:
                return '#FF0000';
                break;
            case 6:
            default:
                return '#000000';
        }
    }


}
