<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Convert\AddressService;
use AppBundle\Services\Convert\CompanyService;
use AppBundle\Services\Convert\CustomerService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/temp-api")
 */
class CompanyController extends BaseController
{
    /** @var Request $request */
    private $request;

    /**
     * @Route("/company/convert", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {

        $this->request = $request;

        $this->container->get("doctrine")->getConnection()->beginTransaction();

        try {

            if ($this->validateRequest($request)) {
                throw new \Exception('Invalid request');
            }

            $company = $this->convert();

            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getManager()->commit();

            // Fix for event listener (onFlush).
            $company->setUpdatedAt(new \DateTime('now'));
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                'status' => 'success',
                'data' => $this->getData($company),
            ]);
        } catch (\Exception $e) {

            $this->container->get("doctrine")->getConnection()->rollBack();

            if (strpos($e->getMessage(), "SQL") !== false) {
                $data = [
                    'status' => 'failed',
                    'message' => "Database error occurred",
                ];
            } else {
                $data = [
                    'status' => 'failed',
                    'message' => (string)$e->getMessage(),
                ];
            }

            if (!empty($e->type) && $e->type == 'username_exist') {
                $data['type'] = $e->type;
                $data['customer'] = $e->customer;
            }

            $code = ($e->getCode() ? $e->getCode() : 500);

            return new JsonResponse($data, $code);
        }
    }

    /**
     * @Route("/company/{company}", methods={"GET"})
     * @param Company $company
     * @return JsonResponse
     */
    public function getAction(Company $company)
    {

        try {
            $json = $this->get(CompanyService::class)
                ->getJsonData($company, true);
        } catch (\Exception $e) {
            return new JsonResponse('Failed', 500);
        }

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/company/validate/chamber-of-commerceNumber/{chamberOfCommerceNumber}", methods={"GET"})
     * @param $chamberOfCommerceNumber
     * @return JsonResponse
     */
    public function validateChamberOfCommerceNumber($chamberOfCommerceNumber)
    {

        $json = new \stdClass();

        $json->status = 'invalid';
        $json->message = 'Request failed';

        try {

            if (!$this->container->get('webservices_nl')
                ->isValidChamberOfCommerceNumber($chamberOfCommerceNumber)) {
                $json->message = 'Invalid `chamber of commerce number`.';
            } else {
                $json->status = 'valid';
                $json->message = 'Valid `chamber of commerce number`';
            }

            return new JsonResponse($json, 200);
        } catch (\Exception $e) {
            $json->message = 'Request can not be processed';
            return new JsonResponse($json, 422);
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getRawData()
    {

        $rawJson = file_get_contents('php://input');
        $rawData = json_decode($rawJson);

        if (empty($rawData)) {
            throw new \Exception('Invalid data posted', 402);
        }

        return $rawData;
    }

    /**
     * @return Company
     * @throws \Exception
     */
    private function convert()
    {
        $rawData = $this->getRawData();

        if (isset($rawData->customers)) {
            $customers = $this->convertCustomers($rawData->customers);
            unset($rawData->customers);
        }

        $addresses = null;
        if (isset($rawData->addresses)) {
            $addresses = $this->convertAddresses($rawData->addresses);
            unset($rawData->addresses);
        }

        $company = $this->convertCompany($rawData);

        /** @var Customer[] $customers */
        foreach ($customers as $customer) {

            // Set relation to the company.
            $company->addCustomer($customer);
        }

        if ($addresses) {
            $company->setAddresses($addresses);

            $invoiceAddress = $addresses->filter(function (Address $address) {
                return $address->isInvoiceAddress();
            })->current();

            foreach ($company->getCustomers() as $customer) {
                $customer->setDefaultInvoiceAddress($invoiceAddress);
            }
        }

        if ($company->getCustomers()->isEmpty()) {
            throw new \Exception("No customers created");
        }

        if ($company->getAddresses()->isEmpty()) {
            throw new \Exception("No addresses created");
        }

        return $company;
    }

    /**
     * @param null|\stdClass $rawData
     * @return Company
     * @throws \Exception
     */
    private function convertCompany($rawData = null)
    {

        $convert = $this->get(CompanyService::class);
        $rawData = $convert->validateRawData($rawData);

        try {
            $company = $convert->createOrUpdate((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Can\'t convert company data', 422);
        }

        return $company;
    }

    /**
     * @param array|null $rawAddresses
     * @return ArrayCollection|null
     */
    private function convertAddresses(array $rawAddresses)
    {

        $addresses = new ArrayCollection();

        foreach ($rawAddresses as $rawAddress) {
            $address = $this->convertAddress((array)$rawAddress);
            $addresses->add($address);
        }

        return $addresses;
    }

    /**
     * @param array $rawData
     * @return Address
     * @throws \Exception
     */
    private function convertAddress(array $rawData)
    {

        $convert = $this->get(AddressService::class);
        $rawData = $convert->validateRawData($rawData);

        try {
            $address = $convert->createOrUpdate((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Can\'t convert address data', 422);
        }

        return $address;
    }

    /**
     * @param array|null $rawCustomers
     * @return ArrayCollection|null
     */

    private function convertCustomers(array $rawCustomers)
    {
        $customers = new ArrayCollection();

        foreach ($rawCustomers as $rawCustomer) {
            $customer = $this->convertCustomer($rawCustomer);
            $customers->add($customer);
        }

        return $customers;
    }

    /**
     * @param null|\stdClass $rawData
     * @return Customer
     * @throws \Exception
     */
    private function convertCustomer(\stdClass $rawData = null)
    {

        $convert = $this->get(CustomerService::class);
        $rawData = $convert->validateRawData($rawData);

        try {
            $customer = $convert->createOrUpdate((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Can\'t convert customer data', 422);
        }

        return $customer;
    }

    /**
     * @param Company $company
     * @return array
     */
    private function getData(Company $company)
    {

        $customerList = [];
        if ($company->getCustomers()) {

            $customers = $company->getCustomers();

            $customerList = $customers->map(function (Customer $customer) {

                $obj = new \stdClass();
                $obj->id = $customer->getId();
                $obj->username = $customer->getUsername();

                return $obj;
            })->toArray();
        }

        return [
            'company' => $company->getId(),
            'customers' => $customerList,
        ];
    }
}

