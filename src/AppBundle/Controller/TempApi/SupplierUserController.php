<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/temp-api")
 */
class SupplierUserController extends BaseController
{

    /**
     * @Route("/supplier/user/update-password", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePasswordAction(Request $request)
    {

        $username = $request->get("username");
        $hash = $request->get("hash");

        if (empty($username) || empty($hash)) {
            return new JsonResponse(['status' => 'failed'], 500);
        }

        $customer = $this->getDoctrine()
            ->getRepository(Customer::class)
            ->findOneBy([
                'username' => $username,
            ]);

        if (!$customer) {
            return new JsonResponse(['status' => 'failed'], 500);
        }

        $customer->setPassword($hash);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse(['status' => 'success']);

    }

}
