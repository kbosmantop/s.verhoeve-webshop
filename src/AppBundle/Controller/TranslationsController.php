<?php

namespace AppBundle\Controller;

use AppBundle\Utils\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Translator;

/**
 * Class TranslationsController
 * @package AppBundle\Controller
 * @Route("/public/translations")
 */
class TranslationsController extends Controller
{
    /**
     * @var Translator $translator;
     */
    private $translator;

    /**
     * @var Translation
     */
    private $translationUtil;

    /**
     * TranslatorExtension constructor.
     * @param Translator  $translator
     * @param Translation $translationUtil
     */
    public function __construct(Translator $translator, Translation $translationUtil)
    {
        $this->translator = $translator;
        $this->translationUtil = $translationUtil;
    }

    /**
     * @Route(".{domain}.{locale}.js", methods={"GET"})
     * @param string      $domain
     * @param null|string $locale
     *
     * @return Response
     */
    public function jsOutput(string $domain = 'messages', ?string $locale = null)
    {
        return $this->getTranslationsJS($locale, $domain);
    }

    /**
     * @Route(".{domain}.{locale}.json", methods={"GET"})
     * @param string      $domain
     * @param null|string $locale
     *
     * @return Response
     */
    public function jsonOutput(string $domain = 'messages', ?string $locale = null)
    {
        $response = new JsonResponse($this->getTranslations($locale, $domain));
        $response->setCache([
            'max_age' => 3600,
            'private' => false,
        ]);
        $response->sendHeaders();
        return $response;
    }

    /**
     * @param string|null $locale
     * @param string      $domain
     *
     * @return Response
     */
    private function getTranslationsJS(string $locale = null, string $domain = 'messages')
    {
        $content = 'function trans(k){var t = ' . json_encode($this->getTranslations($locale,
                $domain)) . '; return t.hasOwnProperty(k)? t[k]: k;}';
        if($locale !== null){
            $content .= ' var currentLocale = \'' . $locale . '\';';
        }
        $response = new Response($content,
            200, [
                'Content-Type' => 'application/javascript',
            ]);
        $response->setCache([
            'max_age' => 3600,
            'private' => true,
        ]);
        $response->sendHeaders();

        return $response;
    }

    /**
     * @param string|null $locale
     * @param string      $domain
     *
     * @return array
     */
    private function getTranslations(string $locale = null, string $domain = 'messages'): array
    {
        if ($locale !== null) {
            $this->translator->setLocale($locale);
        }

        $translations = [];
        foreach ($this->translator->getCatalogue()->all($domain) as $tag => $translation) {
            $translations[$tag] = $this->translationUtil
                ->sanitizeTranslationString($translation, true);
        }

        return $translations;
    }
}
