<?php

namespace AppBundle\Controller\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserInterface as ModelUserInterface;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Translation\Common\Model\Message;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * @Route("/wachtwoord-vergeten")
 */
class ResettingController extends Controller
{
    public const SESSION_EMAIL = 'fos_user_send_resetting_email/email';
    public const SESSION_RETURN_URL = 'return_url';

    /**
     * @return array
     */
    public static function getTranslationMessages(): array
    {
        $messages = [
            new Message('account.resetting.success', 'messages'),
            new Message('resetting.return_message', 'messages'),
        ];

        return $messages;
    }

    /**
     * @Route("/")
     * @Template("@App/Customer/Resetting/request.html.twig")
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function requestAction(Request $request)
    {
        $returnUrl = $request->get('return_url');

        if (null !== $returnUrl) {
            $this->get('session')->set(self::SESSION_RETURN_URL, $returnUrl);
        }

        if ($this->get('app.domain')->getDomain()->getSite()->getDisableCustomerLogin()) {
            return new RedirectResponse($this->generateUrl('app_home_index'));
        }

        return [];
    }

    /**
     * @Route("/verstuur-email")
     * @param Request $request
     *
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->get('username');

        $user = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
            'username' => $username,
        ]);

        if (null === $user) {
            return $this->render('AppBundle:Customer\Resetting:request.html.twig', [
                'invalid_username' => $username,
            ]);
        }

        if (!$user->isEnabled()) {
            return $this->render('AppBundle:Customer\Resetting:request.html.twig', [
                'account_disabled' => $username,
            ]);
        }

        if ($user->isPasswordRequestNonExpired($this->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->render('AppBundle:Customer\Resetting:passwordAlreadyRequested.html.twig');
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');

            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->get('session')->set(self::SESSION_EMAIL, $user->getEmail());

        $job = new Job('topbloemen:site:send-password-reset-mail', [
            $user->getId(),
            $this->get('app.domain')->getDomain()->getId(),
        ], true, 'reset_password');

        $job->addRelatedEntity($user);
        $job->addRelatedEntity($this->get('app.domain')->getDomain());

        $this->container->get('job.manager')->addJob($job);

        $user->setPasswordRequestedAt(new \DateTime());

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse($this->get('router')->generate('app_customer_resetting_checkemail'));
    }

    /**
     * @Route("/controleer-email")
     */
    public function checkEmailAction()
    {
        $session = $this->get('session');
        $email = $session->get(self::SESSION_EMAIL);

        $session->remove(self::SESSION_EMAIL);

        if (empty($email)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->get('router')->generate('app_customer_resetting_request'));
        }

        $returnUrl = $session->get(self::SESSION_RETURN_URL);

        $response = $this->render('AppBundle:Customer\Resetting:checkEmail.html.twig', [
            'email' => $email,
            'hasReturnUrl' => (bool)$returnUrl,
        ]);

        if ($returnUrl) {
            $response->headers->set('Refresh', '3; url=' . $returnUrl);
            $session->remove(self::SESSION_RETURN_URL);
        }

        return $response;
    }

    /**
     * @Route("/instellen/{token}")
     * @param Request $request
     * @param         $token
     *
     * @return RedirectResponse|Response
     */
    public function resetAction(Request $request, $token)
    {
        $user = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
            'confirmationToken' => $token,
        ]);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"',
                $token));
        }

        if (!$user->isPasswordRequestNonExpired($this->getParameter('fos_user.resetting.token_ttl'))) {
            return new RedirectResponse($this->get('router')->generate('app_customer_resetting_request'));
        }

        $form = $this->get('fos_user.resetting.form.factory')->createForm();

        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken(null);
            $user->setPasswordRequestedAt(null);

            $this->get('fos_user.user_manager')->updateUser($user);

            $this->setFlash('frontend.success', 'account.resetting.success');

            $response = new RedirectResponse($this->getRedirectionUrl($user));

            $this->authenticateUser($user, $response);

            return $response;
        }

        return $this->render('AppBundle:Customer\Resetting:reset.html.twig', [
            'token' => $token,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param ModelUserInterface     $user
     * @param HttpFoundationResponse $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->logInUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * Generate the redirection url when the resetting is completed.
     *
     * @param ModelUserInterface $user
     *
     * @return string
     */
    protected function getRedirectionUrl(UserInterface $user)
    {
        void($user);

        return $this->get('router')->generate('app_customer_account_index');
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}
