<?php

namespace AppBundle\Controller\Customer;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Customer\CustomerType;
use AppBundle\Services\SiteService;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\Form\FormInterface;
use Translation\Common\Model\Message;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/account/registreren")
 */
class RegisterController extends Controller
{
    public static function getTranslationMessages()
    {
        $messages = [];

        array_push($messages, new Message("customer.company.registration.success", "messages"));
        array_push($messages, new Message("customer.registration.success", "messages"));
        array_push($messages, new Message("address.invoice_email.not_allowed", "validators"));
        array_push($messages, new Message("customer.account.email.not_allowed", "validators"));
        array_push($messages, new Message("customer.email.not_unique", "validators"));

        return $messages;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("app_customer_account_index");
        }

        if ($this->get(SiteService::class)->determineSite()->getDisableCustomerRegistration()) {
            return new RedirectResponse($this->generateUrl("app_home_index"));
        }

        $requestEmail = $request->get('email');
        $registerAs = $request->get('register_as');

        if ($requestEmail) {

            $emailAlreadyExists = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
                'username' => $requestEmail,
            ]);

            if ($emailAlreadyExists) {
                return $this->render('AppBundle:Customer\Account:index.html.twig', [
                    'error' => 'customer.email.not_unique',
                    'email' => $requestEmail,
                    'registerAs' => $registerAs,
                    'targetPath' => '',
                ]);
            }
        }

        if ($registerAs === 'company') {
            return $this->redirect($this->generateUrl('app_customer_register_company', [
                'email' => $requestEmail,
            ]));
        }

        $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->findOneBy([
            "description" => "Consument",
        ]);

        $customer = new Customer();

        if ($customerGroup) {
            $customer->setCustomerGroup($customerGroup);
        }

        $form = $this->createCustomerForm($customer, $request);

        return [
            'entity' => $customer,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Template()
     * @Route("/zakelijk")
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function companyAction(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("app_customer_account_index");
        }

        if ($this->get(SiteService::class)->determineSite()->getDisableCustomerRegistration()) {
            return new RedirectResponse($this->generateUrl("app_home_index"));
        }

        $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->findOneBy([
            'description' => 'Zakelijk',
        ]);

        $company = new Company();
        $company->setIsCustomer(true);

        if ($customerGroup) {
            $company->addCustomerGroup($customerGroup);
        }

        $flow = $this->get('app.form_flow_company');

        if ($this->container->get('session')->get('registration_from_checkout')) {
            $company->setChamberOfCommerceNumber($this->container->get('session')->get('registration_from_checkout_company_identification'));
        }

        $requestEmail = $request->query->get('email', null);
        if ($requestEmail) {
            $this->container->get('session')->set('start_registration_email', $requestEmail);
        }

        $formOptions = [
            'action' => $this->generateUrl('app_customer_register_company'),
            'validation_groups' => 'Default',
        ];

        $flow->setGenericFormOptions($formOptions);

        $flow->bind($company);

        $form = $flow->createForm();

        if ($form->has('country_choice')) {
            $form->get('country_choice')->setData($this->container->get('session')->get('registration_from_checkout_country'));
        }

        if ($this->container->get('session')->get('registration_from_checkout')) {
            if ($this->container->get('session')->get('registration_from_checkout_country') === 'nl') {
                $form->getData()->setChamberOfCommerceNumber($this->container->get('session')->get('registration_from_checkout_company_identification'));
            } else {
                if ($this->container->get('session')->get('registration_from_checkout_country') === 'be') {
                    $form->getData()->setVatNumber($this->container->get('session')->get('registration_from_checkout_company_identification'));
                }
            }

            $flow->setAllowDynamicStepNavigation(false);
        }

        if ($flow->getCurrentStep() === 1 && $this->container->get('session')->get('registration_from_checkout')) {
            $flow->saveCurrentStepData($form);

            $form->get('country_choice')->setData($this->container->get('session')->get('registration_from_checkout_country'));

            $flow->nextStep();
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                $form = $flow->createForm();
            }
        }

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

                if ($form->has('customers') && $form->get('customers')->has(0) && $form->get('customers')->get(0)->has('username')) {

                    $session = $this->container->get('session');

                    $email = $session->get('registration_from_checkout_email');
                    if (empty($email)) {
                        $email = $session->get('start_registration_email');
                    }
                    $form->get('customers')->get(0)->get('username')->setData($email);
                }

            } else {
                // flow finished
                $em = $this->getDoctrine()->getManager();

                $mainAddress = $company->getAddresses()[0];
                $mainAddress->setCompany($company);
                $mainAddress->setCompanyName($company->getName());
                $mainAddress->setMainAddress(true);
                $mainAddress->setDeliveryAddress(true);

                $invoiceAddress = $company->getAddresses()[1];
                $invoiceAddress->setCompany($company);
                $invoiceAddress->setCompanyName($company->getName());
                $invoiceAddress->setInvoiceAddress(true);
                $company->setInvoiceAddress($invoiceAddress);

                /** @var Customer $defaultCustomer */
                $defaultCustomer = $company->getCustomers()[0];
                $defaultCustomer->setDefaultInvoiceAddress($invoiceAddress);
                $defaultCustomer->setCompany($company);
                $defaultCustomer->setEnabled(true);
                $defaultCustomer->setRegisteredOnSite($this->container->get(SiteService::class)->determineSite());
                $defaultCustomer->setLocale($this->get('app.domain')->getDomain()->getDefaultLocale());

                if ($customerGroup) {
                    $defaultCustomer->setCustomerGroup($customerGroup);
                }

                $company->setDefaultDeliveryAddress($mainAddress);

                $username = $defaultCustomer->getUsername();
                $password = $defaultCustomer->getPlainPassword();

                $company->setVerified(true);

                // Set also registration email addresses directly on the company.
                $company->setEmail($defaultCustomer->getEmail());
                $company->setInvoiceEmail($invoiceAddress->getEmail());

                $em->persist($company);
                $em->flush();

                $flow->reset(); // remove step data from the session

                $this->createRegistrationMailJob($defaultCustomer);

                $this->get('session')->getFlashBag()->add('frontend.success', 'customer.company.registration.success');

                $this->get('app.user_provider')->loginUser($username, $password);

                /**
                 * In case of registering during checkout, login user and redirect to checkout page
                 */
                if ($this->container->get('session')->get('registration_from_checkout')) {
                    $this->container->get('session')->remove('registration_from_checkout');
                    $this->container->get('session')->remove('registration_from_checkout_country');
                    $this->container->get('session')->remove('registration_from_checkout_company_identification');
                    $this->container->get('session')->save();

                    return $this->redirectToRoute('app_checkout_sender'); // redirect when done
                }

                return $this->redirect($this->generateUrl('app_customer_account_index')); // redirect when done
            }
        }

        return [
            'entity' => $company,
            'form' => $form->createView(),
            'flow' => $flow,
            'registrationFromCheckout' => $this->container->get('session')->get('registration_from_checkout'),
        ];
    }

    /**
     * Creates a new Company entity.
     *
     * @Route("/", methods={"POST"})
     * @Template("AppBundle:Customer/Register:index.html.twig")
     *
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAction(Request $request)
    {
        if ($this->get(SiteService::class)->determineSite()->getDisableCustomerRegistration()) {
            return new RedirectResponse($this->generateUrl("app_home_index"));
        }

        $entity = new Customer();
        $form = $this->createCustomerForm($entity, $request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$request->request->get('customer')['birthday']) {
                $entity->setBirthday(null);
            }

            $em = $this->getDoctrine()->getManager();

            $customerGroupName = 'Consumenten';

            foreach ($entity->getAddresses() as $address) {
                $address->setCustomer($entity);
                $address->setMainAddress(true);
                $address->setInvoiceAddress(true);
            }

            $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->findOneBy([
                'description' => $customerGroupName,
            ]);

            $entity->setDefaultDeliveryAddress($entity->getAddresses()->first());
            $entity->setDefaultInvoiceAddress($entity->getAddresses()->first());

            $entity->setCustomerGroup($customerGroup);
            $entity->setRegisteredOnSite($this->container->get(SiteService::class)->determineSite());
            $entity->setLocale($this->get('app.domain')->getDomain()->getDefaultLocale());

            $username = $entity->getUsername();
            $password = $entity->getPlainPassword();

            $entity->setEnabled(true);

            // Save Customer
            $em->persist($entity);

            $em->flush();

            $this->createRegistrationMailJob($entity);

            $this->get('app.user_provider')->loginUser($username, $password);

            $this->get('session')->getFlashBag()->add('frontend.success', 'customer.registration.success');

            return $this->redirect($this->generateUrl('app_customer_account_index'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Customer $entity The entity
     * @param Request  $request
     *
     * @return FormInterface The form
     */
    private function createCustomerForm(Customer $entity, Request $request)
    {
        $form = $this->createForm(CustomerType::class, $entity, [
            'action' => $this->generateUrl('app_customer_register_create'),
            'method' => 'POST',
            'request' => $request,
            'intention' => 'create',
        ]);

        $form->add('submit', SubmitType::class, ['label' => $this->get('translator')->trans('customer.register.submit')]);

        return $form;
    }

    /**
     * @param Customer $entity
     * @throws \Exception
     */
    private function createRegistrationMailJob(Customer $entity)
    {
        $job = new Job('topbloemen:site:send-registration-mail', [
            $entity->getId(),
            $this->get('app.domain')->getDomain()->getId(),
        ], true, 'registration_mail');

        $job->addRelatedEntity($entity);
        $job->addRelatedEntity($this->get('app.domain')->getDomain());

        $this->container->get('job.manager')->addJob($job);
        $this->container->get('doctrine')->getManager()->flush();
    }
}
