<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Customer\Account\CompanyType;
use AppBundle\Form\Customer\CustomerType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Translation\Common\Model\Message;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/gegevens")
 * @Security("has_role('ROLE_USER')")
 */
class SettingsController extends Controller
{
    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('account.company.settings.success', 'messages'),
            new Message('account.settings.success', 'messages'),
            new Message('account.settings.submit', 'messages'),
        ];
    }

    /**
     * @Route("/", methods={"GET"})
     * @Template()
     * @param Request $request
     * @return array|Response
     */
    public function indexAction(Request $request)
    {
        if (
            $this->getUser()->getCompany()
            && (
                $this->getUser()->getCompany()->getChamberofCommerceNumber()
                || $this->getUser()->getCompany()->getVatNumber()
            )
        ) {
            if (!$this->userCanEditCompany()) {
                throw new AccessDeniedHttpException();
            }

            $entity = $this->getUser()->getCompany();
            $form = $this->createCompanyForm($entity, $request);

            return $this->render('AppBundle:Customer/Account/Settings:company.html.twig', [
                'entity' => $entity,
                'form' => $form->createView(),
            ]);
        }

        $entity = $this->getUser();
        $form = $this->createCustomerForm($entity, $request);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/", methods={"PUT"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editPersistAction(Request $request)
    {
        if (
            $this->getUser()->getCompany()
            && (
                $this->getUser()->getCompany()->getChamberofCommerceNumber()
                || $this->getUser()->getCompany()->getVatNumber()
            )
        ) {
            if (!$this->userCanEditCompany()) {
                throw new AccessDeniedHttpException();
            }

            $entity = $this->getUser()->getCompany();
            $form = $this->createCompanyForm($entity, $request);
            $form->handleRequest($request);
            $template = 'AppBundle:Customer/Account/Settings:company.html.twig';

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var EntityManagerInterface $em */
                $em = $this->getDoctrine()->getManager();

                $em->getConnection()->beginTransaction();

                try {
                    $em->flush();

                    $em->getConnection()->commit();

                    $session = $request->getSession();
                    if (null !== $session) {
                        $session->set('_locale', $request->getLocale());
                    }
                } catch (Exception $e) {
                    print $e->getMessage();
                }

                $this->addFlash('frontend.success', 'account.company.settings.success');

                return $this->redirectToRoute('app_customer_account_index');
            }
        } else {
            $entity = $this->getUser();
            $form = $this->createCustomerForm($entity, $request);
            $form->handleRequest($request);
            $template = 'AppBundle:Customer/Account/Settings:index.html.twig';

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var EntityManagerInterface $em */
                $em = $this->getDoctrine()->getManager();

                $em->getConnection()->beginTransaction();

                try {
                    // Save Customer
                    $em->persist($entity);

                    $em->flush();

                    $em->getConnection()->commit();

                    $session = $request->getSession();
                    if (null !== $session) {
                        $session->set('_locale', $entity->getLocale());
                    }
                } catch (Exception $e) {
                    print $e->getMessage();
                }

                $this->addFlash('frontend.success', 'account.settings.success');

                return $this->redirectToRoute('app_customer_account_index');
            }
        }

        return $this->render($template, [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to create a Customer entity.
     *
     * @param Customer $entity The entity
     *
     * @param Request  $request
     * @return FormInterface
     */
    private function createCustomerForm(Customer $entity, Request $request)
    {
        $form = $this->createForm(CustomerType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_settings_index'),
            'method' => 'PUT',
            'request' => $request,
            'intention' => 'edit',
        ]);

        $form->add('submit', SubmitType::class,
            ['label' => $this->get('translator')->trans('account.settings.submit')]);

        return $form;
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Company $entity The entity
     *
     * @param Request $request
     * @return FormInterface
     */
    private function createCompanyForm(Company $entity, Request $request)
    {
        $form = $this->createForm(CompanyType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_settings_index'),
            'method' => 'PUT',
            'request' => $request,
            'intention' => 'edit',
        ]);

        $form->add('submit', SubmitType::class,
            ['label' => $this->get('translator')->trans('account.settings.submit')]);

        return $form;
    }

    /**
     * @return bool
     */
    private function userCanEditCompany()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();
        $company = $customer->getCompany();

        return (!$customer->isMigrated() && $company->getCustomers()->count() <= 1);
    }
}
