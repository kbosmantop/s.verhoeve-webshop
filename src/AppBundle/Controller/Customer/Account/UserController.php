<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Customer\Account\UserType;
use JMS\JobQueueBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Translation\Common\Model\Message;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/gebruikers")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends AbstractAccountController
{
    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('account.user.edit.success', 'messages'),
            new Message('account.user.new.success', 'messages'),
        ];
    }

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'edit' => $this->userCanEditAccounts(),
        ];
    }

    /**
     * @Route("/nieuw", methods={"GET"})
     * @Template()
     * @return array
     */
    public function newAction()
    {
        $entity = new Customer();
        $form = $this->createCreateForm($entity);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @Template("@App/Customer/Account/User/edit.html.twig")
     * @param Customer $entity
     * @return array
     */
    public function editAction(Customer $entity)
    {
        $this->checkAccessRights($entity);

        $editForm = $this->createEditForm($entity);

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"PUT"})
     * @Template("AppBundle:Customer/Account/User:edit.html.twig")
     * @param Customer $entity
     * @param Request  $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Customer $entity, Request $request)
    {
        $this->checkAccessRights($entity);

        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('frontend.success', 'account.user.edit.success');

            return $this->redirect($this->generateUrl('app_customer_account_user_index'));
        }

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];
    }

    /**
     * @Route("/nieuw", methods={"POST"})
     * @Template("AppBundle:Customer/Account/User:new.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $entity = new Customer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setCompany($this->getUser()->getCompany());
            $this->getUser()->getCompany()->addCustomer($entity);

            $entity->doFullname();
            $entity->doUsernameToEmail();

            if (null === $entity->getConfirmationToken()) {
                $tokenGenerator = $this->get('fos_user.util.token_generator');

                $entity->setConfirmationToken($tokenGenerator->generateToken());
            }

            $em->persist($entity);
            $em->flush();

            $job = new Job('topbloemen:site:send-company-choose-password-mail', [
                $entity->getId(),
                $this->get('app.domain')->getDomain()->getId(),
            ], true, 'company_choose_password_mail');

            $job->addRelatedEntity($entity);
            $job->addRelatedEntity($this->get('app.domain')->getDomain());

            $this->container->get('job.manager')->addJob($job);
            $em->flush();

            $entity->setPasswordRequestedAt(new \DateTime());

            $this->get('session')->getFlashBag()->add('frontend.success', 'account.user.new.success');

            return $this->redirect($this->generateUrl('app_customer_account_user_index'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }


    /**
     * Creates a form to create a Company entity.
     *
     * @param Customer $entity The entity
     *
     * @return FormInterface
     */
    private function createCreateForm(Customer $entity)
    {
        $form = $this->createForm(UserType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_user_create'),
            'method' => 'POST',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'validation_groups' => ['Default', 'user_registration'],
            'intention' => 'create',
        ]);

        $form->add('submit', SubmitType::class,
            ['label' => $this->container->get('translator')->trans('account.user.new.submit')]);

        return $form;
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Customer $entity The entity
     *
     * @return FormInterface
     */
    private function createEditForm(Customer $entity)
    {
        $form = $this->createForm(UserType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_user_edit', [
                'id' => $entity->getId(),
            ]),
            'method' => 'PUT',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'validation_groups' => ['Default', 'user_registration'],
            'intention' => 'edit',
        ]);

        $form->add('submit', SubmitType::class,
            ['label' => $this->container->get('translator')->trans('account.user.edit.submit')]);

        return $form;
    }

    /**
     * @return bool
     */
    private function userCanEditAccounts()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();
        $company = $customer->getCompany();

        return (!$customer->isMigrated() && $company !== null && $company->getCustomers()->count() <= 1);
    }
}
