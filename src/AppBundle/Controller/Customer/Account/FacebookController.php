<?php

namespace AppBundle\Controller\Customer\Account;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/facebook")
 * @Security("has_role('ROLE_USER')")
 */
class FacebookController extends Controller
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()->getCompany() || !$this->getUser()->getFacebookId()) {
            return $this->redirectToRoute('app_customer_account_index');
        }

        return [];
    }

    /**
     * @Route("/loskoppelen")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function disconnectAction()
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $user->setFacebookId(null);
            $user->setFacebookAccessToken(null);

            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {
        }

        return $this->redirectToRoute("app_customer_account_facebook_index");
    }
}
