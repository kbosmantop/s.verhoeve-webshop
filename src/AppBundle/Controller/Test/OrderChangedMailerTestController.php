<?php

namespace AppBundle\Controller\Test;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Services\EnvironmentalService;
use AppBundle\Services\Mailer\OrderChangedMailer;
use AppBundle\Services\Mailer\OrderConfirmationMailer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderChangedMailerTestController
 * @package AppBundle\Controller\Test
 */
class OrderChangedMailerTestController extends Controller
{
    /**
     * @var OrderConfirmationMailer
     */
    private $orderChangedMailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderConfirmationMailerTestController constructor.
     *
     * @param OrderChangedMailer $orderChangedMailer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        OrderChangedMailer $orderChangedMailer,
        EntityManagerInterface $entityManager
    ) {
        $this->orderChangedMailer = $orderChangedMailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/test/order-changed/{orderCollection}/{payment}/send", methods={"GET"})
     *
     * @param OrderCollection $orderCollection
     * @param Payment $payment
     *
     * @return Response
     * @throws Exception
     */
    public function sendTestMail(OrderCollection $orderCollection = null, Payment $payment = null): Response
    {
        $parameters = [
            'orderCollection' => $orderCollection,
            'payment' => $payment,
            'preview' => true
        ];

        return new Response($this->orderChangedMailer->send($parameters) ? 'Ok' : 'Failed');
    }

    /**
     * @Route("/test/order-changed/{orderCollection}/{payment}/preview", condition="'%kernel.environment%' === 'dev'") methods={"GET"})
     *
     * @param OrderCollection $orderCollection
     * @param Payment $payment
     *
     * @return Response
     * @throws Exception
     */
    public function preview(OrderCollection $orderCollection, Payment $payment): Response
    {
        $data = null;
        $this->orderChangedMailer->send(
            [
                'orderCollection' => $orderCollection,
                'payment' => $payment,
                'preview' => true
            ],
            static function (Swift_Message $message) use (&$data) {
                $data = $message->getBody();
                return false;
            }
        );

        return new Response($data);
    }
}
