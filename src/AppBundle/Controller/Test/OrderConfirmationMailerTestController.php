<?php

namespace AppBundle\Controller\Test;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\EnvironmentalService;
use AppBundle\Services\Mailer\OrderConfirmationMailer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderConfirmationMailerTestController
 * @package AppBundle\Controller\Test
 */
class OrderConfirmationMailerTestController extends Controller
{
    /**
     * @var OrderConfirmationMailer
     */
    private $orderConfirmationMailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderConfirmationMailerTestController constructor.
     * @param OrderConfirmationMailer $orderConfirmationMailer
     * @param EntityManagerInterface  $entityManager
     */
    public function __construct(
        OrderConfirmationMailer $orderConfirmationMailer,
        EntityManagerInterface $entityManager
    ) {
        $this->orderConfirmationMailer = $orderConfirmationMailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/test/order-confirmation/{orderCollection}/send", condition="'%kernel.environment%' === 'test'", methods={"GET"})
     * @param OrderCollection|null $orderCollection
     * @return Response
     * @throws Exception
     */
    public function sendTestMail(?OrderCollection $orderCollection = null): Response
    {
        if($orderCollection === null) {
            $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find(1);
        }

        $isSend = $this->orderConfirmationMailer->send([
            'orderCollection' => $orderCollection
        ]);

        if($isSend) {
            return new Response('Ok');
        }

        return new Response('Failed');
    }

    /**
     * @Route("/test/order-confirmation/{orderCollection}/preview", condition="'%kernel.environment%' === 'dev'") methods={"GET"})
     *
     * @param OrderCollection|null $orderCollection
     * @return Response
     * @throws \Exception
     */
    public function preview(?OrderCollection $orderCollection = null): Response
    {
        if($orderCollection === null) {
            $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find(1);
        }

        $data = null;
        $this->orderConfirmationMailer->send(
            [
                'orderCollection' => $orderCollection,
                'preview' => true
            ],
            function (\Swift_Message $message) use (&$data) {
                $data = $message->getBody();
                return false;
            }
        );

        return new Response($data);
    }
}