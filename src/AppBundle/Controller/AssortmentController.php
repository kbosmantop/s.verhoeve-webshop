<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Manager\Catalog\Assortment\AssortmentManager;
use AppBundle\Services\AccessibilityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * Class AssortmentController
 * @package AppBundle\Controller
 */
class AssortmentController extends Controller
{
    /**
     * @param Assortment $assortment
     *
     * @return array
     *
     * @Template
     * @Cache(expires="+1 days", public=true)
     * @throws \Exception
     */
    public function indexAction(Assortment $assortment)
    {
        $assortmentManager = $this->container->get(AssortmentManager::class);

        //check if user has access to assortment
        if(!$this->get(AccessibilityService::class)->isAssortmentAccessibleByUser($assortment)) {
            throw new AccessDeniedHttpException();
        }

        foreach ($assortment->getProducts() as $product) {

            if(null === $product) {
                continue;
            }

            $this->get("app.google_analytics")->productImpression($assortment, $product);
        }

        $parameters = [];
        $parameters['assortment'] = $assortment;
        $parameters['maxPrice'] = $assortmentManager->getMaxPrice($assortment);
        $parameters['minPrice'] = $assortmentManager->getMinPrice($assortment);
        $parameters['filters'] = $assortmentManager->getFilters($assortment);

        if ($assortment->getContent()) {
            $parameters['content'] = $assortment->getContent();
            $parameters['contentPosition'] = $assortment->getContentPosition();
            $parameters['contentHeight'] = explode('x', $assortment->getContentSize())[0];
            $parameters['contentWidth'] = explode('x', $assortment->getContentSize())[1];
        }

        return $parameters;
    }
}
