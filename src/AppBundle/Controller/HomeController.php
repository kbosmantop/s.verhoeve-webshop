<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Page;
use ArrayIterator;
use AppBundle\Services\SiteService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/")
 */
class HomeController extends Controller
{
    /**
     * @Template()
     * @Route("")
     * @Cache(expires="+1 hour")
     */
    public function indexAction()
    {
        /** @var Customer $user */
        $user = $this->getUser();

        if (
            null !== $user
            && null !== $this->getParameter('bakker_homepage_id')
            && null !== $user->getCustomerGroup()
            && $user->getCustomerGroup()->getDescription() === 'Bakkers'
        ) {
            $page = $this->getDoctrine()->getRepository(Page::class)->find($this->getParameter('bakker_homepage_id'));

            if ($page) {
                $page = $this->get('app.page_decorator')->setPage($page);
            }

            return [
                'page' => $page,
            ];
        }

        $appSite = $this->get(SiteService::class);

        if (null === ($site = $appSite->determineSite())) {
            throw new \RuntimeException('No site received');
        }

        //set main menu and site id in session to determine menu's
        $session = $this->container->get('session');
        $session->set('site_id', $site->getId());

        if(false === $site->getMainMenu()->isEmpty()) {
            $session->set('menu_id', $site->getMainMenu()->current()->getId());
        }

        $page = $site->getHomepage();

        if ($page) {
            $page = $this->get('app.page_decorator')->setPage($page);
        }

        $sliderProducts = null;
        $assortments = $appSite->determineAssortments();
        $aboutPage = $site->getHomepageAbout();
        $homepageAssortment = $site->getHomepageAssortment();
        $homepageAssortmentsBlocks = $site->getHomepageAssortments();
        $companyAssortments = new ArrayCollection();

        if (null !== $homepageAssortment) {
            $sliderProducts = $this->getDoctrine()->getRepository(Product::class)->getProductsByAssortment($homepageAssortment);
        }

        $homepageAssortment = null;

        //get company assortments instead of site assortments
        if (null !== $user && null !== $user->getCompany() && false === $user->getCompany()->getAssortments()->isEmpty()) {
            $assortments = $user->getCompany()->getAssortments();

            $assortments = $assortments->filter(function (Assortment $assortment) {
                $assortmentKey = $assortment->getAssortmentType() ? $assortment->getAssortmentType()->getKey() : false;
                return !\in_array($assortmentKey,
                    [AssortmentType::TYPE_ADDITIONAL_PRODUCTS, AssortmentType::TYPE_CARDS], true);
            });

            if (false === $assortments->isEmpty()) {
                $companyAssortments = $assortments;

                /** @var ArrayIterator $iterator */
                $iterator = $companyAssortments->getIterator();
                $iterator->uasort(function (Assortment $a, Assortment $b) {
                    return $a->getPosition() <=> $b->getPosition();
                });
                $companyAssortments = new ArrayCollection(iterator_to_array($iterator));
            }
        }

        //set main menu and site id in session to determine menu's
        $session = $this->container->get('session');
        $session->set('site_id', $site->getId());

        if (false === $site->getMainMenu()->isEmpty()) {
            $session->set('menu_id', $site->getMainMenu()->current()->getId());
        }

        if ($assortments->count() === 1) {
            $homepageAssortment = $assortments->first();
        }

        return [
            'page' => $page,
            'about' => $aboutPage,
            'assortments' => $assortments,
            'sliderProducts' => $sliderProducts,
            'homepageAssortmentsBlocks' => $homepageAssortmentsBlocks,
            'homepageAssortment' => $homepageAssortment,
            'companyAssortments' => $companyAssortments,
        ];
    }
}
