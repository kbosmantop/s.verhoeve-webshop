<?php

namespace AppBundle\Controller;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Services\SiteService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/")
 */
class SitemapController extends Controller
{
    /**
     * @Cache(expires="+1 hours")
     * @Route("/sitemap.xml", defaults={"_format": "xml"}, methods={"GET"})
     * @Template("@App/sitemapindex.xml.twig")
     */
    public function indexAction()
    {
        $sitemaps = [];

        array_push($sitemaps, (object)[
            'url' => $this->generateUrl("app_sitemap_pages", [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        array_push($sitemaps, (object)[
            'url' => $this->generateUrl("app_sitemap_categories", [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        array_push($sitemaps, (object)[
            'url' => $this->generateUrl("app_sitemap_products", [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return [
            'sitemaps' => $sitemaps,
        ];
    }

    /**
     * @Cache(expires="+1 hours")
     * @Route("/sitemap-pages.xml", defaults={"_format": "xml"}, methods={"GET"})
     * @Template("@App/sitemap.xml.twig")
     */
    public function pagesAction()
    {
        $urls = new ArrayCollection();

        $pages = $this->container->get(SiteService::class)->determineSite()->getPages();

        foreach ($pages as $page) {
            $page = $this->container->get("app.page_decorator")->setPage($page);

            if (!$this->evaluate($page)) {
                continue;
            }

            $urls->add((object)[
                'loc' => $page->getAbsoluteUrl(),
                'lastmod' => $page->getUpdatedAt(),
            ]);
        }

        $this->sort($urls);

        return [
            'urls' => $urls,
        ];
    }

    /**
     * @Cache(expires="+1 hours")
     * @Route("/sitemap-categories.xml", defaults={"_format": "xml"}, methods={"GET"})
     * @Template("@App/sitemap.xml.twig")
     */
    public function categoriesAction()
    {
        $urls = new ArrayCollection();

        $assortments = $this->container->get(SiteService::class)->determineSite()->getAssortments();

        foreach ($assortments as $assortment) {
            $assortment = $this->get("app.assortment.factory")->get($assortment);

            if (!$this->evaluate($assortment)) {
                continue;
            }

            if (!$assortment->getProducts()) {
                continue;
            }

            $urls->add((object)[
                'loc' => $assortment->getAbsoluteUrl(),
                'lastmod' => $assortment->getUpdatedAt(),
            ]);
        }

        $this->sort($urls);

        return [
            'urls' => $urls,
        ];
    }

    /**
     * @Cache(expires="+1 hours")
     * @Route("/sitemap-products.xml", defaults={"_format": "xml"}, methods={"GET"})
     * @Template("@App/sitemap.xml.twig")
     */
    public function productsAction()
    {
        $urls = new ArrayCollection();

        $assortments = $this->container->get(SiteService::class)->determineSite()->getAssortments();
        $products = new ArrayCollection();

        foreach ($assortments as $assortment) {
            $assortment = $this->get("app.assortment.factory")->get($assortment);

            if (!$this->evaluate($assortment)) {
                continue;
            }

            foreach ($assortment->getProducts() as $product) {
                if (!$this->evaluate($product)) {
                    continue;
                }

                $result = $products->filter(function (ProductDecorator $decoratedProduct) use ($product) {
                    return $decoratedProduct->get() == $product->get();
                });

                if (!$result->count()) {
                    $products->add($product);
                }
            }
        }

        foreach ($products as $product) {
            $urls->add((object)[
                'loc' => $product->getAbsoluteUrl(),
                'lastmod' => $product->getUpdatedAt(),
            ]);
        }

        $this->sort($urls);

        return [
            'urls' => $urls,
        ];
    }

    /**
     * @Cache(expires="+1 hours")
     * @Route("/sitemap.xsl", defaults={"_format": "xsl"}, methods={"GET"})
     * @Template("@App/sitemap.xsl.twig")
     */
    public function xslAction()
    {
        return [];
    }

    /**
     * @param $object
     * @return bool
     */
    private function evaluate($object)
    {
        if (substr(get_class($object), -9) == "Decorator") {
            $object = $object->get();
        }

        if (!$object->isPublished()) {
            return false;
        }

        if (method_exists($object, "getMetaRobotsIndex") && !$object->getMetaRobotsIndex()) {
            return false;
        }

        if (method_exists($object, "translate") && method_exists($object->translate(),
                "getMetaRobotsIndex") && !$object->translate()->getMetaRobotsIndex()) {
            return false;
        }

        return true;
    }

    private function sort(ArrayCollection &$collection)
    {
        $iterator = $collection->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->loc < $b->loc) ? -1 : 1;
        });

        $collection = new ArrayCollection(iterator_to_array($iterator));
    }
}
