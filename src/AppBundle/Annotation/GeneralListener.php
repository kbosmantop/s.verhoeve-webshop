<?php

namespace AppBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"CLASS"})
 * Class GeneralListener
 * @package AppBundle\Annotation
 */
class GeneralListener
{
    /**
     * @var string $parent
     */
    public $parent;

    /**
     * @var string $parentGetter
     */
    public $parentGetter;

    /**
     * @var string $get
     */
    public $get;

    /**
     * @var string $set
     */
    public $set;

    /**
     * @return string
     */
    public function getParent(): string
    {
        return ucfirst($this->parent);
    }

    /**
     * @return string
     */
    public function getParentGetter(): string
    {
        return $this->parentGetter;
    }

    /**
     * @return string
     */
    public function getGet(): string
    {
        return 'get' . ucfirst($this->get);
    }

    /**
     * @return string
     */
    public function getSet(): string
    {
        return 'set' . ucfirst($this->set);
    }
}