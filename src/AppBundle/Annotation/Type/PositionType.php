<?php

namespace AppBundle\Annotation\Type;

/**
 * Class PositionType
 * @package AppBundle\Annotation\Type
 */
class PositionType
{
    private const TOP = 'top';
    private const TOP_LEFT = 'top-left';
    private const TOP_RIGHT = 'top-right';
    private const LEFT = 'left';
    private const CENTER = 'center';
    private const RIGHT = 'right';
    private const BOTTOM = 'bottom';
    private const BOTTOM_LEFT = 'bottom-left';
    private const BOTTOM_RIGHT = 'bottom-right';

    /**
     * @var array
     */
    public static $choices = [
        self::TOP => 'Boven',
        self::TOP_LEFT => 'Linksboven',
        self::TOP_RIGHT => 'Rechtsboven',
        self::LEFT => 'Links',
        self::CENTER => 'Midden',
        self::RIGHT => 'Rechts',
        self::BOTTOM => 'Onder',
        self::BOTTOM_LEFT => 'Linksonder',
        self::BOTTOM_RIGHT => 'Rechtsonder',
    ];
}