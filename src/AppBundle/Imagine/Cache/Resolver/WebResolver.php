<?php

namespace AppBundle\Imagine\Cache\Resolver;

use AppBundle\Traits\Imagine\WebDataTrait;
use League\Flysystem\Filesystem;
use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Exception\Imagine\Cache\Resolver\NotResolvableException;
use Liip\ImagineBundle\Imagine\Cache\Resolver\ResolverInterface;
use Liip\ImagineBundle\Imagine\Cache\Resolver\WebPathResolver;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class WebResolver
 * @package AppBundle\Imagine\Cache\Resolver
 */
class WebResolver implements ResolverInterface
{
    use ContainerAwareTrait;
    use WebDataTrait;

    /**
     * @var WebPathResolver
     */
    private $webPath;

    /**
     * @var Filesystem
     */
    private $publicFilesystem;

    /**
     * @var Filesystem
     */
    private $privateFilesystem;

    /**
     * @var string
     */
    private $objectStoreUrl;

    /**
     * WebResolver constructor.
     * @param WebPathResolver $webPath
     * @param Filesystem      $publicFilesystem
     * @param Filesystem      $privateFilesystem
     * @param                 $cache
     */
    public function __construct(
        WebPathResolver $webPath,
        Filesystem $publicFilesystem,
        Filesystem $privateFilesystem,
        $cache
    ) {
        $this->webPath = $webPath;
        $this->publicFilesystem = $publicFilesystem;
        $this->privateFilesystem = $privateFilesystem;

        $this->cache = $cache;
    }

    /**
     * @param string $path
     * @param string $filter
     *
     * @return bool
     */
    public function isStored($path, $filter)
    {
        try {
            if (($remoteUrl = $this->getRemoteUrl($path))) {
                return $this->checkIfUrlRemoteExists($remoteUrl);
            } else {
                return $this->webPath->isStored($path, $filter);
            }
        } catch (NotResolvableException $e) {
            return false;
        }
    }

    /**
     * @param string $path
     * @param string $filter
     *
     * @return string
     */
    public function resolve($path, $filter)
    {
        // Strip url params
        $path = explode('?', $path)[0];

        if (!$this->objectStoreUrl) {
            $this->objectStoreUrl = (string)$this->container->getParameter('openstack_object_store');
        }

        if (strstr($path, $this->objectStoreUrl . '/private/')) {
            if (($remoteFile = $this->getRemoteContent($path))) {
                return 'data:image/png;base64, ' . $remoteFile;
            } else {
                throw new NotResolvableException('Image not resolvable');
            }
        } elseif (($remoteFile = $this->getRemoteUrl($path))) {
            return $remoteFile;
        } else {
            return $this->webPath->resolve($path, $filter);
        }
    }

    /**
     * @param BinaryInterface $binary
     * @param string          $path
     * @param string          $filter
     */
    public function store(BinaryInterface $binary, $path, $filter)
    {
        // Strip url params
        $path = explode('?', $path)[0];

        $this->webPath->store($binary, $path, $filter);
    }

    /**
     * @param string[] $paths
     * @param string[] $filters
     */
    public function remove(array $paths, array $filters)
    {
        if (empty($paths) && empty($filters)) {
            return;
        }

        foreach ($paths as $pathKey => $path) {
            // Strip url params
            $paths[$pathKey] = explode('?', $path)[0];
        }

        $this->webPath->remove($paths, $filters);
    }
}
