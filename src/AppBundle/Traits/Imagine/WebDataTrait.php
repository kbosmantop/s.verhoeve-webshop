<?php

namespace AppBundle\Traits\Imagine;

use Liip\ImagineBundle\Exception\Imagine\Cache\Resolver\NotResolvableException;

/**
 * Trait WebDataTrait
 * @package AppBundle\Traits\Imagine
 */
trait WebDataTrait
{
    /**
     * @var resource
     */
    private $ch;

    private $cache;

    /**
     * @param string $remoteUrl
     * @return bool|string
     */
    private function getRemoteUrl(string $remoteUrl)
    {
        if (strpos($remoteUrl, '://') !== false) {
            $splitUrl = explode('://', $remoteUrl, 2);
            $urlProtocol = explode('/', $splitUrl[0]);

            return $urlProtocol[\count($urlProtocol) - 1] . '://' . $splitUrl[1];
        }

        return false;
    }

    private function getCurl() {
        if (!$this->ch) {
            $this->ch = curl_init();
            curl_setopt($this->ch, CURLOPT_HEADER, 0);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

            if ($this->container->get('kernel')->getEnvironment() !== 'prod') {
                // TODO Code inspector: Exposes a connection to MITM attacks
                curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
            }
        }
    }

    /**
     * @param string $remoteUrl
     * @return bool
     */
    public function checkIfUrlRemoteExists(string $remoteUrl)
    {
        $cachedRemoteUrlstatus = $this->cache->getItem('url_status'.md5($remoteUrl));

        if (!$cachedRemoteUrlstatus->isHit()) {
            $this->getCurl();

            curl_setopt($this->ch, CURLOPT_URL, $remoteUrl);
            curl_exec($this->ch);

            if (curl_errno($this->ch)) {
                throw new NotResolvableException('File not found');
            }

            $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

            if (!$httpCode || $httpCode === 400) {

                $cachedRemoteUrlstatus->set(0);

                $this->cache->save($cachedRemoteUrlstatus);
                return false;
            }

            $cachedRemoteUrlstatus->set(1);

            $this->cache->save($cachedRemoteUrlstatus);
            return true;
        }

        return $cachedRemoteUrlstatus->get();
    }

    /**
     * @param string $path
     * @return bool|string
     */
    public function getRemoteContent(string $path)
    {
        if ($remoteUrl = $this->getRemoteUrl($path)) {

            // Check if url from object store
            if (0 === strpos($path, $this->objectStoreUrl)) {
                $filePath = str_replace($this->objectStoreUrl, '', $path);

                // Check if url is from private
                if (0 === strpos($filePath, '/private/')) {
                    $filePath = str_replace('/private/', '', $filePath);
                    $fileContent = $this->privateFilesystem->read($filePath);

                } else {
                    $filePath = str_replace('/public/', '', $filePath);
                    $fileContent = $this->publicFilesystem->read($filePath);
                }
            } else {

                $this->getCurl();

                curl_setopt($this->ch, CURLOPT_URL, $remoteUrl);

                $fileContent = curl_exec($this->ch);

                if (curl_errno($this->ch)) {
                    throw new NotResolvableException('File not found');
                }
            }

            return base64_encode($fileContent);
        }

        return false;
    }
}
