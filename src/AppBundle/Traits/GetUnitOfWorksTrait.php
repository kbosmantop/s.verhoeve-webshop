<?php

namespace AppBundle\Traits;

use Doctrine\Common\EventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait GetUnitOfWorksTrait
 * @package AppBundle\Traits
 */
trait GetUnitOfWorksTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param LifecycleEventArgs|EventArgs $eventArgs
     * @param string $className
     * @param string $jobName
     * @param string $validateMethod
     * @param string $queue
     *
     * @return void
     * @throws \Exception
     */
    public function addJobOnChange(EventArgs $eventArgs, string $className, string $jobName, $validateMethod = '', $queue = 'default')
    {
        $entityManager = $this->container->get('doctrine')->getManager();
        $jobManager = $this->container->get('job.manager');
        $objectEntity = $eventArgs->getObject();

        if ($objectEntity instanceof $className || \get_class($objectEntity) === $className) {
            /** @var UnitOfWork $uow */
            $uow = $entityManager->getUnitOfWork();

            /** @var array $changeSet */
            $changeSet = $uow->getEntityChangeSet($objectEntity);

            if ($validateMethod !== '' && !$this->{$validateMethod}($objectEntity, $changeSet)) {
                return;
            }

            $job = new Job($jobName, [$objectEntity->getId()], true, $queue);
            $job->addRelatedEntity($objectEntity);

            $jobManager->addJob($job);
        }
    }

    /**
     * @required
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }
}
