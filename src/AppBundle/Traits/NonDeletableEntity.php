<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait NonDeletableEntity
 * @package AppBundle\Traits
 */
trait NonDeletableEntity
{
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $deletable = true;

    /**
     * Sets deletable.
     *
     * @param boolean $deletable
     *
     * @return $this
     */
    public function setDeletable($deletable = null)
    {
        $this->deletable = $deletable;

        return $this;
    }

    /**
     * Returns deletable.
     *
     * @return boolean
     */
    public function getDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * Is deletable?
     *
     * @return bool
     */
    public function isDeletable(): bool
    {
        return ($this->deletable === true);
    }
}