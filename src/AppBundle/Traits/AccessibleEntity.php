<?php

namespace AppBundle\Traits;

use AppBundle\Entity\Security\Customer\CustomerGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Class AccessibleEntity
 * @package AppBundle\Traits
 */
trait AccessibleEntity
{
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $loginRequired;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $accessibleCustomerGroups;

    /**
     * @return bool
     */
    public function getLoginRequired()
    {
        return $this->loginRequired;
    }

    /**
     * Get loginRequired
     *
     * @return bool
     */
    public function isLoginRequired()
    {
        return $this->loginRequired;
    }

    /**
     * @param bool $loginRequired
     *
     * @return AccessibleEntity
     */
    public function setLoginRequired($loginRequired)
    {
        $this->loginRequired = $loginRequired;

        return $this;
    }

    /**
     * Get accessibleCustomerGroups
     *
     * @return \Doctrine\Common\Collections\Collection|CustomerGroup[]
     */
    public function getAccessibleCustomerGroups()
    {
        return $this->accessibleCustomerGroups;
    }

    /**
     * Add customer
     *
     * @param CustomerGroup $customerGroup
     *
     * @return $this
     */
    public function addAccessibleCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->accessibleCustomerGroups[] = $customerGroup;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param CustomerGroup $customerGroup
     */
    public function removeAccessibleCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->accessibleCustomerGroups->removeElement($customerGroup);
    }

    /**
     * Get
     *
     * @return array
     */
    public function getAccessibleCustomerGroupIds()
    {
        $ids = [];

        foreach ($this->accessibleCustomerGroups as $customerGroup) {
            $ids[] = $customerGroup->getId();
        }

        return $ids;
    }
}
