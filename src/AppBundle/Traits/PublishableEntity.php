<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait PublishableEntity
 * @package AppBundle\Traits
 */
Trait PublishableEntity
{
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $publish;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $publishStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $publishEnd;

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return $this
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Sets publishStart.
     *
     * @param \Datetime|null $publishStart
     *
     * @return $this
     */
    public function setPublishStart(\DateTime $publishStart = null)
    {
        $this->publishStart = $publishStart;

        return $this;
    }

    /**
     * Returns archivedAt.
     *
     * @return \DateTime
     */
    public function getPublishStart()
    {
        return $this->publishStart;
    }

    /**
     * Sets publishEnd.
     *
     * @param \Datetime|null $publishEnd
     *
     * @return $this
     */
    public function setPublishEnd(\DateTime $publishEnd = null)
    {
        $this->publishEnd = $publishEnd;

        return $this;
    }

    /**
     * Returns publishEnd.
     *
     * @return \DateTime
     */
    public function getPublishEnd()
    {
        return $this->publishEnd;
    }

    /**
     * Is published?
     *
     * @return bool
     */
    public function isPublished()
    {
        $datetime = new \DateTime();

        if (method_exists($this, 'getParent') && $this->getParent() && !$this->getParent()->isPublished()) {
            return false;
        }

        return (
            $this->publish === true
            && (null === $this->getPublishStart() || ($this->getPublishStart()->getTimestamp() <= $datetime->getTimestamp()))
            && (null === $this->getPublishEnd() || ($this->getPublishEnd()->getTimestamp() >= $datetime->getTimestamp()))
        );
    }

    /**
     * Has schedule?
     *
     * @return bool
     */
    public function isScheduled()
    {
        $datetime = new \DateTime();

        return (
            $this->publish === true
            &&
            ($this->getPublishStart() && $this->getPublishStart()->getTimestamp() >= $datetime->getTimestamp())
        );
    }

    /**
     * Has schedule?
     *
     * @param boolean $publish
     *
     * @return bool
     */
    public function hasSchedule($publish = true)
    {
        return (
            ((!$publish) ? true : $this->publish === $publish)
            && (($this->getPublishStart() && $this->getPublishStart()->getTimestamp()) || ($this->getPublishEnd() && $this->getPublishEnd()->getTimestamp()))
        );
    }

    /**
     * Publish entity
     *
     * @param \DateTime|null $publishStart
     * @param \DateTime|null $publishEnd
     *
     */
    public function publish(\DateTime $publishStart = null, \DateTime $publishEnd = null)
    {
        $this->publish = true;
        $this->publishStart = $publishStart;
        $this->publishEnd = $publishEnd;
    }
}
