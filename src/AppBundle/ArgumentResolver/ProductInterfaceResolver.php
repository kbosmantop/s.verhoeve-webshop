<?php

namespace AppBundle\ArgumentResolver;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * Class ProductInterfaceResolver
 * @package AppBundle\ArgumentResolver
 */
class ProductInterfaceResolver implements ArgumentValueResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProductInterfaceResolver constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request          $request
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getType() === ProductInterface::class;
    }

    /**
     * @param Request          $request
     * @param ArgumentMetadata $argument
     * @return \Generator
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        yield $this->entityManager->getRepository(Product::class)->find($request->get('product') ?? $request->get('id'));
    }


}