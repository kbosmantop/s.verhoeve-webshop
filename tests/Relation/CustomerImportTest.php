<?php

namespace Tests\Relation;

use AppBundle\Entity\Relation\Company;
use AppBundle\Exceptions\Common\SpreadsheetExtensionException;
use AppBundle\Exceptions\Customer\BatchCreate\CompanyNotAllowedException;
use AppBundle\Exceptions\Customer\BatchCreate\InvalidMimeTypeException;
use AppBundle\Exceptions\Customer\EmailAddressUsedException;
use AppBundle\Exceptions\Customer\UsernameUsedException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Exception as WriterException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CustomerImportTest
 * @package Tests\Relation
 */
class CustomerImportTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * Check if a valid file will succesfully processed.
     *
     * @throws ConnectionException
     */
    public function testValidFile()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        $file = $this->getFileByName('valid_file.xlsx');

        /** @var Company $company */
        $company = $doctrine->getRepository(Company::class)->find(1);

        $uploadedFile = new UploadedFile(
            $file->getRealPath(),
            $file->getBasename(),
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $file->getSize()
        );

        $customerImport = $this->container->get('customer.import');

        try {
            $collection = $customerImport->processFile($uploadedFile, $company);

            self::assertEquals(1, $collection->count());
        } catch(\Exception $exception) {
            self::fail($exception->getMessage());
        }

        $connection->rollBack();
    }

    /**
     * Test if invalid files will handled.
     *
     * @throws ConnectionException
     */
    public function testInvalidFileExtension()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        $file = $this->getFileByName('invalid_ext_file.txt');

        /** @var Company $company */
        $company = $doctrine->getRepository(Company::class)->find(1);

        $uploadedFile = new UploadedFile(
            $file->getRealPath(),
            $file->getBasename(),
            'text/plain',
            $file->getSize()
        );

        $customerImport = $this->container->get('customer.import');

        try {
            $customerImport->processFile($uploadedFile, $company);
        } catch (\Exception $exception) {
            self::assertInstanceOf(SpreadsheetExtensionException::class, $exception);
        }

        $connection->rollBack();
    }

    /**
     * @throws ConnectionException
     * @throws Exception
     * @throws SpreadsheetExtensionException
     * @throws \AppBundle\Exceptions\Common\SpreadsheetReaderException
     * @throws \AppBundle\Exceptions\Customer\BatchCreateException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testInvalidMimeType()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        $file = $this->getFileByName('invalid_mime_type.xlsx');

        /** @var Company $company */
        $company = $doctrine->getRepository(Company::class)->find(1);

        $uploadedFile = new UploadedFile(
            $file->getRealPath(),
            $file->getBasename(),
            'text/plain',
            $file->getSize()
        );

        $customerImport = $this->container->get('customer.import');

        try {
            $customerImport->processFile($uploadedFile, $company);
        } catch (\Exception $exception) {
            self::assertInstanceOf(InvalidMimeTypeException::class, $exception);
        }

        $connection->rollBack();
    }

    /**
     * Test if invalid user can import customers to a invalid company.
     *
     * @throws ConnectionException
     */
    public function testImportWithInvalidCompany()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        $file = $this->getFileByName('valid_file_all_columns.xlsx');

        /** @var Company $company */
        $company = $doctrine->getRepository(Company::class)->find(2);

        $uploadedFile = new UploadedFile(
            $file->getRealPath(),
            $file->getBasename(),
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $file->getSize()
        );

        $customerImport = $this->container->get('customer.import');

        try {
            $customerImport->processFile($uploadedFile, $company);
        } catch (\Exception $exception) {
            self::assertInstanceOf(CompanyNotAllowedException::class, $exception);
        }

        $connection->rollBack();
    }

    /**
     * Test if duplicated rows in import will give an exception.
     *
     * @throws ConnectionException
     */
    public function testDuplicatedRowInImport()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        $file = $this->getFileByName('valid_file_duplicated_row.xlsx');

        /** @var Company $company */
        $company = $doctrine->getRepository(Company::class)->find(1);

        $uploadedFile = new UploadedFile(
            $file->getRealPath(),
            $file->getBasename(),
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $file->getSize()
        );

        $customerImport = $this->container->get('customer.import');

        try {
            $customerImport->processFile($uploadedFile, $company);
        } catch (\Exception $exception) {
            if($exception instanceof EmailAddressUsedException) {
                self::assertInstanceOf(EmailAddressUsedException::class, $exception);
            } else {
                self::assertInstanceOf(UsernameUsedException::class, $exception);
            }
        }

        $connection->rollBack();
    }

    /**
     * @return BinaryFileResponse
     * @throws Exception
     * @throws WriterException
     */
    public function testDownloadImportTemplate()
    {
        $customerImportTemplate = $this->container->get('customer.import.template');

        $template = $customerImportTemplate->downloadCustomerImportTemplate();

        self::assertInstanceOf(BinaryFileResponse::class, $template);
    }

    /**
     * @param string $name
     * @return SplFileInfo
     */
    private function getFileByName($name = 'valid_file.xlsx')
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/uploads')->name($name);

        $iterator = $finder->getIterator();
        $iterator->rewind();

        return $iterator->current();
    }

}