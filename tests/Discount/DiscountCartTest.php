<?php

namespace Test\Discount;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Order\Cart;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Services\Discount\DiscountService;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Tests\Holidays\Traits\MocksTrait;

/**
 * Class DiscountCartTest
 * @package Test\Discount
 */
class DiscountCartTest extends KernelTestCase
{
    use ContainerAwareTrait;
    use MocksTrait;

    /**
     * @var RuleTransformerService $ruleTransformer
     */
    protected $ruleTransformer;

    /**
     * @var ResolverService $ruleResolver
     */
    protected $ruleResolver;

    /**
     * @var Rule $rule
     */
    protected $rule;

    /**
     * @var Cart $cart
     */
    protected $cart;

    /**
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \TypeError
     */
    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $kernel = self::bootKernel();
        $this->setContainer($kernel->getContainer());

        $fakeRequest = Request::create('/', 'GET', [], [], [], [
            'HTTP_HOST' => 'dev.toptaarten.nl',
        ]);

        $requestStack = $this->container->get('request_stack');
        $requestStack->push($fakeRequest);

        $this->cart = $this->mockCart();

        $this->ruleTransformer = $this->container->get('rule.transformer');
        $this->ruleResolver = $this->container->get('rule.resolver');

        $inputs = [
            ResolverService::QB_ALIAS => $this->container->get('doctrine')->getManager()->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode(OrderLineInterface::class),
            ]),
        ];

        $this->rule = $this->ruleTransformer->reverseTransformData([
            'condition' => 'AND',
            'rules' => [
                [
                    'operator' => 'greater',
                    'id' => '__result.order.collection.totalPrice',
                    'field' => '__result.order.collection.totalPrice',
                    'value' => '10',
                    'input' => 'text',
                    'type' => 'string',
                ],
            ],
        ], $inputs);

        $this->rule->setDescription('Kortingsregel');
    }

    /**
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function testFixedDiscount()
    {
        $discount = new Discount();
        $discount->setValue(10);
        $discount->setRule($this->rule);
        $discount->setType('amount');

        $cart = $this->mockCart();

        /** @var Cart $discountedCart */
        $discountedCart = $this->container->get(DiscountService::class)->applyLocalDiscount($discount, $cart);

        static::assertEquals(10, $discountedCart->getDiscountAmount());
    }

    /**
     * @throws \Exception
     */
    public function testPercentageDiscount()
    {
        $discount = new Discount();
        $discount->setValue(10);
        $discount->setRule($this->rule);
        $discount->setType('percentage');

        $cart = $this->mockCart();

        /** @var Cart $discountedCart */
        $discountedCart = $this->container->get(DiscountService::class)->applyLocalDiscount($discount, $cart);

        static::assertEquals(2.8, $discountedCart->getDiscountAmount());
    }

}