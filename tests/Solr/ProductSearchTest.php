<?php

namespace App\Tests\Solr;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\Catalog\Product\Product;

/**
 * Class ProductSearchTest
 * @package Tests\Search
 */
class ProductSearchTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws \Exception
     */
    public function testSearchWithResults(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue AP-31'); // TODO

        $search = $this->container->get('app.search.product');
        $search->searchByTitle($this->q());

        static::assertNotEmpty($search->getResult());
    }

    /**
     * @throws \Exception
     */
    public function testLimitedResults(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue AP-31'); // TODO

        $search = $this->container->get('app.search.product');
        $search->searchByTitle($this->q(), 3);

        static::assertLessThanOrEqual(3, \count($search->getResult()));
    }

    /**
     * @throws \Exception
     */
    public function testSearchWithoutResults(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue AP-31'); // TODO

        $search = $this->container->get('app.search.product');
        $search->searchByTitle('SHOULD_NOT_RETURN_ANY_RESULT');

        static::assertEmpty($search->getResult());
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    private function q(): ?string
    {
        /** @var Product $product */
        $product = $this->container->get('doctrine')->getRepository(Product::class)->findOneBy([]);

        if (!$product) {
            throw new \RuntimeException('At least one product should exist when running this test');
        }

        return explode(' ', $product->translate()->getTitle())[0];
    }
}
