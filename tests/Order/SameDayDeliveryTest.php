<?php

namespace Tests\Order;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Services\CartService;
use AppBundle\Utils\DeliveryDate;
use AppBundle\Utils\DeliveryDates;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class SameDayDeliveryTest
 * @package Tests\Order
 */
class SameDayDeliveryTest extends KernelTestCase
{
    public function setUp()
    {
        self::bootKernel();
    }

    public function testSameDayBefore1400(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        self::$kernel->getContainer()->get(CartService::class)->setCart($this->mockCartOrder());

        timecop_freeze(new \DateTime('2018-08-01 13:55:00'));

        $deliveryDates = new DeliveryDates([], self::$kernel->getContainer());
        $deliveryDates->add(new DeliveryDate());

        $deliveryDate = $deliveryDates[0];

        $this->assertTrue($deliveryDate->isAvailable());
    }

    public function testSameDayAfter1400(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        self::$kernel->getContainer()->get(CartService::class)->setCart($this->mockCartOrder());

        timecop_freeze(new \DateTime('2018-08-01 14:05:00'));

        $deliveryDates = new DeliveryDates([], self::$kernel->getContainer());
        $deliveryDates->add(new DeliveryDate());

        $deliveryDate = $deliveryDates[0];

        $this->assertFalse($deliveryDate->isAvailable());
    }

    /**
     * @return Cart
     */
    private function mockCartOrder(): Cart
    {
        $cart = new Cart();

        $cartOrder = new CartOrder();
        $cartOrder->setCart($cart);

        $cartOrderLine = new CartOrderLine();
        $cartOrderLine->setProduct($this->getSameDayProduct());

        $cartOrder->addLine($cartOrderLine);

        $cart->addOrder($cartOrder);

        return $cart;
    }

    /**
     * @return Product
     */
    private function getSameDayProduct(): Product
    {
        $productGroup = $this->getDoctrine()->getRepository(Productgroup::class)->findOneBy([
            'skuPrefix' => 'FLO'
        ]);

        self::assertNotNull($productGroup);

        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy([
            'productgroup' => $productGroup,
            'parent' => null,
            'publish' => true
        ], [
            'id' => 'desc'
        ]);

        self::assertNotNull($product);
        self::assertFalse($product->getVariations()->isEmpty());

        return $product->getVariations()[0];
    }

    /**
     * @return Registry
     */
    private function getDoctrine(): Registry
    {
        return self::$kernel->getContainer()->get('doctrine');
    }

    public function tearDown(): void
    {
        timecop_return();
    }
}