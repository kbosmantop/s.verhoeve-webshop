<?php

namespace Tests\ExactGlobe;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;
use AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType;

/**
 * Class PersonalDataTest
 */
class PersonalDataTest extends AbstractTest
{
    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers CompanyInvoiceSettings::setInvoiceInclPersonalData
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testInclPersonalDataNone()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceInclPersonalData(CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_NONE);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(3, $invoice->getLines());
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers CompanyInvoiceSettings::setInvoiceInclPersonalData
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testInclPersonalDataCustomer()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceInclPersonalData(CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_CUSTOMER);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(4, $invoice->getLines());
        static::assertNotContains('Ontvanger: ', $invoice->getLines()[3]->getDescription());
        static::assertContains('Afzender: ', $invoice->getLines()[3]->getDescription());
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers CompanyInvoiceSettings::setInvoiceInclPersonalData
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testInclPersonalDataRecipient()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceInclPersonalData(CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_RECIPIENT);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(4, $invoice->getLines());
        static::assertContains('Ontvanger: ', $invoice->getLines()[3]->getDescription());
        static::assertNotContains('Afzender: ', $invoice->getLines()[3]->getDescription());
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers CompanyInvoiceSettings::setInvoiceInclPersonalData
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testInclPersonalDataAll()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceInclPersonalData(CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_ALL);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(4, $invoice->getLines());
        static::assertContains('Ontvanger: ', $invoice->getLines()[3]->getDescription());
        static::assertContains('Afzender: ', $invoice->getLines()[3]->getDescription());
    }
}