<?php

namespace Tests\ExactGlobe;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyInvoiceSettings;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use UnexpectedValueException;

/**
 * Class AbstractTest
 */
abstract class AbstractTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @return Order
     */
    protected function fetchOrder(): Order
    {
        $order = $this->container->get('doctrine')->getManager()->find(Order::class, 1);

        if ($order->getTotalPrice() !== 38.497 && $order->getTotalPriceIncl() !== 41.97) {
            // fixture data seems to be changed, so we can't run this test
            throw new UnexpectedValueException('Fixture data seems to be changed');
        }

        return $order;
    }

    /**
     * @return Company
     */
    protected function mockCompany(): Company
    {
        $companyInvoiceSettings = new CompanyInvoiceSettings();

        $company = new Company();
        $company->setCompanyInvoiceSettings($companyInvoiceSettings);

        return $company;
    }

    /**
     * @return Invoice
     */
    protected function mockInvoice(): Invoice
    {
        $invoice = new Invoice();
        $invoice->setCompany($this->mockCompany());

        return $invoice;
    }
}