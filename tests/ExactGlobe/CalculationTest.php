<?php

namespace Tests\ExactGlobe;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;

/**
 * Class CalculationTest
 */
class CalculationTest extends AbstractTest
{
    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::setDecimalPrecision
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testDiscountSeperateDecimals2()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_SEPARATE);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->setDecimalPrecision(2);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(6, $invoice->getLines());
        static::assertEquals(18.77, $invoice->getLines()[0]->getPriceValue());
        static::assertEquals(-3.44, $invoice->getLines()[1]->getPriceValue());
        static::assertEquals(1.00, $invoice->getLines()[2]->getPriceValue());
        static::assertEquals(-1.00, $invoice->getLines()[3]->getPriceValue());
        static::assertEquals(6.56, $invoice->getLines()[4]->getPriceValue());
        static::assertEquals(-2.16, $invoice->getLines()[5]->getPriceValue());
        static::assertEquals(round($order->getTotalPrice(), 2), round($invoice->getTotalPriceValue(), 2));
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::setDecimalPrecision
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testDiscountSeperateDecimals3()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_SEPARATE);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->setDecimalPrecision(3);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(6, $invoice->getLines());
        static::assertEquals(18.774, $invoice->getLines()[0]->getPriceValue());
        static::assertEquals(-3.44, $invoice->getLines()[1]->getPriceValue());
        static::assertEquals(1, $invoice->getLines()[2]->getPriceValue());
        static::assertEquals(-1, $invoice->getLines()[3]->getPriceValue());
        static::assertEquals(6.557, $invoice->getLines()[4]->getPriceValue());
        static::assertEquals(-2.163, $invoice->getLines()[5]->getPriceValue());
        static::assertEquals(round($order->getTotalPrice(), 2), round($invoice->getTotalPriceValue(), 2));
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::setDecimalPrecision
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testDiscountAppliedDecimals2()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->setDecimalPrecision(2);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(3, $invoice->getLines());
        static::assertEquals(17.05, $invoice->getLines()[0]->getPriceValue());
        static::assertEquals(0.00, $invoice->getLines()[1]->getPriceValue());
        static::assertEquals(4.39, $invoice->getLines()[2]->getPriceValue());

        // Special case here, using decimalPrecision of 2 resuls in wrong outcome
        // static::assertEquals(round($order->getTotalPrice(), 2), round($invoice->getTotalPriceValue(), 2));
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::setDecimalPrecision
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testDiscountAppliedDecimals3()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->setDecimalPrecision(3);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(3, $invoice->getLines());
        static::assertEquals(17.054, $invoice->getLines()[0]->getPriceValue());
        static::assertEquals(0.00, $invoice->getLines()[1]->getPriceValue());
        static::assertEquals(4.394, $invoice->getLines()[2]->getPriceValue());

        static::assertEquals(round($order->getTotalPrice(), 2), round($invoice->getTotalPriceValue(), 2));
    }
}