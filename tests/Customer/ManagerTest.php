<?php

namespace App\Tests\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\CustomerNotFoundException;
use AppBundle\Manager\CustomerManager;
use AppBundle\Services\CustomerManagerService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use FOS\UserBundle\Model\UserInterface;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class CustomerManagerTest
 *
 * @package Tests\App
 */
class ManagerTest extends WebTestCase
{
    use ContainerAwareTrait;

    /**
     * @var CustomerManagerService
     */
    private $customerManager;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private static $idsMarkedForDeletion = [];

    /**
     * @var Registry
     */
    private static $doctrineRegistry;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->customerManager = $this->container->get(CustomerManager::class);

        self::$doctrineRegistry = $this->container->get('doctrine');
    }

    /**
     * Test creating a customer without an account
     *
     * @throws \Exception
     */
    public function testCreateCustomerWithoutAccount(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customerPropertyAccessor = PropertyAccess::createPropertyAccessor();

        $properties = [
            'email' => 'unit_test@test.nl',
            'firstname' => 'Voornaam',
            'lastname' => 'Achternaam',
            'gender' => 'male',
            'enabled' => false,
        ];

        $customer = $this->customerManager->create($properties);

        static::assertInternalType('int', $customer->getId());

        foreach ($properties as $property => $value) {
            static::assertEquals($customerPropertyAccessor->getValue($customer, $property), $value);
        }

        self::$idsMarkedForDeletion[] = $customer->getId();
    }

    /**
     * Test creating a customer with an account
     * @throws \Exception
     */
    public function testCreateCustomersWithAccount(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customerPropertyAccessor = PropertyAccess::createPropertyAccessor();

        $registeredOnSite = self::$doctrineRegistry->getRepository(Site::class)->find(2);

        $customers = [
            [
                'email' => 'unit_test_account@test.nl',
                'firstname' => 'Voornaam',
                'lastname' => 'Achternaam',
                'gender' => 'male',
                'enabled' => true,
                'username' => 'unit_test_account@test.nl',
                'plainPassword' => '12345',
            ],
            [
                'email' => 'unit_test_account_two@test.nl',
                'firstname' => 'Voornaam 2',
                'lastname' => 'Achternaam 2',
                'gender' => 'female',
                'enabled' => true,
                'username' => 'unit_test_account_two@test.nl',
                'plainPassword' => '123456',
                'registeredOnSite' => $registeredOnSite,
            ],
        ];

        foreach ($customers as $properties) {
            $customer = $this->customerManager->create($properties);

            static::assertInternalType('int', $customer->getId());

            foreach ($properties as $property => $value) {
                if ($property === 'plainPassword') {
                    static::assertNull($customerPropertyAccessor->getValue($customer, $property));
                    static::assertTrue(password_verify($value,
                        $customerPropertyAccessor->getValue($customer, 'password')));

                    continue;
                }

                static::assertEquals($customerPropertyAccessor->getValue($customer, $property), $value);
            }

            self::$idsMarkedForDeletion[] = $customer->getId();
        }
    }

    /**
     * Test logging in on a website
     */
    public function testCustomerLogin(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $this->client = static::createClient();

        /** @var Customer $customer */
        $customer = $this->customerManager->findByUsername('robbert@topbloemen.nl');

        $this->authenticateCustomer($customer);

        $crawler = $this->client->request('GET', 'http://dev.topfruit.nl:8080/account/');

        static::assertSame('(' . $customer->getFullname() . ')', $crawler->filter('.logged-in-as')->text());
    }

    /**
     * Test logging in on a website
     */
    public function testFailedCustomerLogin(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $this->expectException(\TypeError::class);

        $this->client = static::createClient();

        /** @var Customer $customer */
        $customer = $this->customerManager->findByUsername('unit_test@test.nl');

        $this->authenticateCustomer($customer);
    }

    /**
     * Remove a customer with an account
     */
    public function testRemoveCustomerWithAccount(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $removal = $this->customerManager->remove('unit_test_account@test.nl');

        static::assertTrue($removal);
    }

    /**
     * Test deactivating a customer account
     *
     * @throws \TypeError
     */
    public function testDeactivateCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->deactivate('robbert@topbloemen.nl');

        static::assertFalse($customer->isEnabled());
    }

    /**
     * Test activating a customer account
     * @throws \TypeError
     */
    public function testActivateCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->activate('robbert@topbloemen.nl');

        static::assertTrue($customer->isEnabled());
    }

    /**
     * Test if the proper Exception is thrown when performing an action on a non-existing customer
     */
    public function testActivatingNonExistingCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $this->expectException(InvalidArgumentException::class);

        $this->customerManager->activate('ikbestaniet@mooidomeinnaam.nl');
    }

    /**
     * Add new role to customer
     */
    public function testAddRoleToCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->addRole('robbert@topbloemen.nl', 'ROLE_PHPUNIT_TEST');

        static::assertTrue($customer->hasRole('ROLE_PHPUNIT_TEST'));
    }

    /**
     * Remove newly added role from customer
     */
    public function testRemoveRoleToCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->removeRole('robbert@topbloemen.nl', 'ROLE_PHPUNIT_TEST');

        static::assertFalse($customer->hasRole('ROLE_PHPUNIT_TEST'));
    }

    /**
     * Add a role that already exists for a customer
     */
    public function testAddExistingRoleToCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->addRole('robbert@topbloemen.nl', 'ROLE_USER');

        static::assertFalse($customer);
    }

    /**
     * Remove a non existing role for a customer
     */
    public function testRemoveNotExistingRoleToCustomer(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->removeRole('robbert@topbloemen.nl', 'ROLE_PHPUNUT_TEST_NOT_EXISTING');

        static::assertFalse($customer);
    }

    /**
     * Convert a customer without an account to a customer with an account
     */
    public function testConvertToCustomerWithAccount(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $email = 'unit_test@test.nl';
        $customer = $this->customerManager->convertToAccount($email);

        static::assertTrue($customer->isEnabled());
        static::assertEquals($customer->getUsername(), $email);
    }

    /**
     * Try to login with a converted customer account
     */
    public function testConvertedCustomerLogin(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $this->client = static::createClient();
        $this->client->followRedirects();

        /** @var Customer $customer */
        $customer = $this->customerManager->findByUsername('unit_test@test.nl');

        $this->authenticateCustomer($customer);

        $crawler = $this->client->request('GET', 'http://dev.topfruit.nl:8080/account/');

        static::assertSame('(' . $customer->getFullname() . ')', $crawler->filter('.logged-in-as')->text());
    }

    /**
     * Request a password reset for a customer account
     */
    public function testRequestResetPassword(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->findByUsername('unit_test_account_two@test.nl');

        try {
            $customer = $this->customerManager->requestResetPassword($customer);

            static::assertNotNull($customer->getConfirmationToken());
            static::assertNotNull($customer->getPasswordRequestedAt());
        } catch (\Exception $e) {
        }
    }

    /**
     * Reset a password for a customer account with an active request
     */
    public function testResetPassword(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $customer = $this->customerManager->findByUsername('unit_test_account_two@test.nl');

        try {
            $password = '456789';

            $customer = $this->customerManager->resetPassword($customer->getConfirmationToken(), $password);

            static::assertNull($customer->getConfirmationToken());
            static::assertNull($customer->getPasswordRequestedAt());
            static::assertTrue(password_verify($password, $customer->getPassword()));
        } catch (\Exception $e) {
        }
    }

    /**
     * Reset a password for a customer account with no active request
     * @throws CustomerNotFoundException
     */
    public function testResetPasswordWithoutRequest(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-2766'); // TODO

        $this->expectException(CustomerNotFoundException::class);

        $this->customerManager->resetPassword('nonexistingconfirmationtoken', '456789');
    }

    /**
     * @param UserInterface $customer
     */
    private function authenticateCustomer(UserInterface $customer): void
    {
        $session = $this->client->getContainer()->get('session');

        $firewallContext = 'frontend';

        $token = new UsernamePasswordToken($customer, null, $firewallContext, $customer->getRoles());

        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());

        $this->client->getCookieJar()->set($cookie);
    }

    /**
     * Clean up all persisted customer entities
     *
     * @throws DBALException
     */
    private static function cleanupDatabase(): void
    {
        /** @var Connection $connection */
        $connection = self::$doctrineRegistry->getConnection();

        if (!empty(self::$idsMarkedForDeletion)) {
            $connection->executeQuery('DELETE FROM customer WHERE id IN(' . implode(',',
                    self::$idsMarkedForDeletion) . ') ');
        }
    }

    /**
     * Tear down after all tests
     *
     * @throws DBALException
     */
    public static function tearDownAfterClass()
    {
        self::cleanupDatabase();
    }
}
