<?php

namespace Tests\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class MappingInfoTest
 * @package Tests\Doctrine
 */
class MappingInfoTest extends KernelTestCase
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->entityManager = $this->container->get('doctrine')->getManager();
    }

    /**
     * @see php bin/console doctrine:mapping:info
     * @throws ORMException
     */
    public function testMappings(): void
    {
        $entityClassNames = $this->entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $mappingExceptions = [];

        foreach ($entityClassNames as $entityClassName) {
            try {
                $this->entityManager->getClassMetadata($entityClassName);
            } catch (MappingException $e) {
                $mappingExceptions[] = $e;
            }
        }

        static::assertEmpty($mappingExceptions);
    }
}
