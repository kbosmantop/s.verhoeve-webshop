<?php

namespace Tests\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaValidator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SchemaValidateTest
 * @package Tests\Doctrine
 */
class SchemaValidateTest extends KernelTestCase
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->entityManager = $this->container->get('doctrine')->getManager();
    }

    /**
     * @see php bin/console doctrine:schema:validate
     */
    public function testSchema(): void
    {
        static::markTestSkipped('Skipped test, see Jira issue WEB-268'); // TODO

        $validator = new SchemaValidator($this->entityManager);

        static::assertEmpty($validator->validateMapping());
    }
}
