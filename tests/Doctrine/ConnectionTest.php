<?php

namespace Tests\Doctrine;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ConnectionTest
 * @package Tests\Doctrine
 */
class ConnectionTest extends KernelTestCase
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->entityManager = $this->container->get('doctrine')->getManager();
    }

    public function testConnection(): void
    {
        $connection = $this->entityManager->getConnection();

        static::assertTrue($connection->isConnected());
    }
}
