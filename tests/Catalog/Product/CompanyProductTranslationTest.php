<?php

namespace Tests\Catalog\Product;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Manager\Catalog\Product\ProductManager;
use function array_diff;
use function array_intersect;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CompanyProductTranslationTest
 * @package Tests\Catalog\Product
 */
class CompanyProductTranslationTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws ReflectionException
     */
    public function testOverriddenTranslations()
    {
        $product = new GenericProduct();
        $companyProduct = new CompanyProduct();
        $translation = new ProductTranslation();

        $companyProduct->setRelatedProduct($product);
        $companyProduct->addTranslation($translation);
        $product->addTranslation($translation);

        $companyProductDecorator = $this->container->get(ProductManager::class)->decorateProduct($companyProduct);

        $anonymousReflection = new ReflectionClass($companyProductDecorator->translate());
        $translationReflection = new ReflectionClass($translation);

        $translationMethods = $this->filterArray($this->mapArray($translationReflection));
        $anonymousMethods = $this->filterArray($this->mapArray($anonymousReflection));

        self::assertEmpty(array_diff($anonymousMethods, $translationMethods));
    }

    /**
     * @param $array
     * @return array
     */
    private function filterArray($array) {
        return array_filter($array, function($name) {
            return strpos($name, 'get') === 0;
        });
    }

    /**
     * @param ReflectionClass $class
     * @return array
     */
    private function mapArray(ReflectionClass $class) {
        return array_map(function(ReflectionMethod $method) {
            return $method->getName();
        }, $class->getMethods());
    }
}