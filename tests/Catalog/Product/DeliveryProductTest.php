<?php

namespace Tests\Catalog\Product;

use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductTransportType;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Manager\Catalog\Product\ProductTransportTypeManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CompanyProductTranslationTest
 * @package Tests\Catalog\Product
 */
class DeliveryProductTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    public function testProductTransportTypes()
    {
        $parent = new GenericProduct();
        $product = new GenericProduct();
        $productGroup = new Productgroup();

        $transportTypeFree = new TransportType();
        $transportTypeFree->setPrice(0);

        $product->setParent($parent);
        $parent->setProductgroup($productGroup);

        $productTransportTypeFree = new ProductTransportType();
        $productTransportTypeFree->setTransportType($transportTypeFree);
        $productTransportTypeFree->setQuantity(1);

        $productGroup->addProductTransportType($productTransportTypeFree);

        $productTwo = new GenericProduct();
        $productTwo->setParent($parent);

        $transportTypeTwo = new TransportType();
        $transportTypeTwo->setPrice(2);

        $productTransportTypeTwo = new ProductTransportType();
        $productTransportTypeTwo->setTransportType($transportTypeTwo);
        $productTransportTypeTwo->setQuantity(1);

        $productTwo->addProductTransportType($productTransportTypeTwo);

        //selects transportType from productgroup
        $transportTypesFree = $this->container->get(ProductTransportTypeManager::class)->getProductTransportTypesForProduct($product);

        //selects transportType from product
        $transportTypesTwo = $this->container->get(ProductTransportTypeManager::class)->getProductTransportTypesForProduct($productTwo);

        self::assertContains($productTransportTypeFree, $transportTypesFree);
        self::assertContains($productTransportTypeTwo, $transportTypesTwo);
    }
}