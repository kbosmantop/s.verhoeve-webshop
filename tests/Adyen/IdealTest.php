<?php

namespace Tests\Adyen;

use Adyen\AdyenException;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Ledger;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Services\CartService;
use AppBundle\Services\SiteService;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IdealTest
 * @package Tests\Adyen
 */
class IdealTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp(): void
    {
        self::bootKernel();

        $this->container = static::$kernel->getContainer();

        $requestStack = $this->container->get('request_stack');
        $requestStack->push($this->getRequest());
    }

    /**
     * @return Site|null
     */
    private function getSite(): ?Site
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->container->get('doctrine')->getRepository(Site::class);

        $qb = $entityRepository->createQueryBuilder('site');
        $qb->andWhere('site.adyenUsername IS NOT NULL');
        $qb->andWhere('site.adyenPassword IS NOT NULL');
        $qb->andWhere('site.adyenMerchantAccount IS NOT NULL');
        $qb->andWhere('site.adyenSkinCode IS NOT NULL');
        $qb->setMaxResults(1);

        try {
            /** @var Site $site */
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // won't happen
            return null;
        }
    }

    /**
     * @return Request
     */
    private function getRequest(): Request
    {
        $site = $this->getSite();

        if ($site === null) {
            return Request::create('/', 'GET', [], [], [], []);
        }

        return Request::create('/', 'GET', [], [], [], ['HTTP_HOST' => $site->getPrimaryDomain()]);
    }

    public function testIssuers(): void
    {
        $adyenGateway = $this->container->get('app.gateway.adyen');

        $idealIssuers = $adyenGateway->getIdealIssuers();

        static::assertInternalType('array', $idealIssuers);

        switch ($this->container->get('kernel')->getEnvironment()) {
            case 'dev':
            case 'test':
            case 'staging':
                static::assertArrayHasKey(1121, $idealIssuers);
                break;
            case 'prod':
                static::assertArrayHasKey(0031, $idealIssuers);
                break;
        }
    }

    /**
     * @throws NonUniqueResultException
     * @throws AdyenException
     * @throws PaymentException
     * @throws ConnectionException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Twig_Error
     */
    public function testRedirect(): void
    {
        $cart = $this->mockCart();
        $cart->setSite($this->container->get(SiteService::class)->determineSite());

        $orderCollection = $this->container->get(CartService::class)->finalize($cart, false);
        $orderCollection->setTotalPriceIncl(99.99);
        $orderCollection->getOrders()[0]->setTotalPriceIncl(99.99);

        $payment = new Payment();
        $payment->setAmount(99.99);
        $payment->setOrderCollection($orderCollection);

        $paymentmethod = new Paymentmethod();
        $paymentmethod->setCode('ideal');

        $payment->setPaymentmethod($paymentmethod);

        $response = $this->container->get('app.payment.gateway.adyen')->processPayment($payment);

        static::assertInstanceOf(RedirectResponse::class, $response);

        $location = $response->headers->get('Location');

        static::assertRegExp('|https:\/\/[a-z]+.adyen.com/hpp/redirectIdeal.shtml|', $location);
    }

    public function tearDown(): void
    {
        $requestStack = $this->container->get('request_stack');
        $requestStack->pop();

        parent::tearDown();
    }

    /**
     * @return Cart
     */
    private function mockCart(): Cart
    {
        /** @var Country $country */
        $country = $this->container->get('doctrine')->getRepository(Country::class)->find('NL');

        $customer = new Customer();
        $customer->setEmail('test@topgeschenken.nl');

        $address = new Address();
        $address->setCountry($country);
        $address->setMainAddress(true);
        $address->setInvoiceAddress(true);

        $customer->addAddress($address);

        $mainAddress = new Address();
        $mainAddress->setCountry($country);

        $customer->addAddress($mainAddress);

        $cart = new Cart();
        $cart->setCustomer($customer);
        $cart->setInvoiceAddressCountry($customer->getMainAddress()->getCountry());
        $cart->setTotalPriceIncl(99.99);

        $vat = $this->container->get('doctrine')->getRepository(Vat::class)->find(2);

        $ledger = new Ledger();
        $ledger->setNumber(8100);

        $productGroup = new Productgroup();
        $productGroup->setSkuPrefix('FRT');
        $productGroup->setLedger($ledger);

        $product = new GenericProduct();
        $product->setVat($vat);
        $product->setProductgroup($productGroup);

        $cartOrder = new CartOrder();
        $cartOrder->setTotalPriceIncl(99.99);
        $cartOrder->setCart($cart);

        $cartOrder->setDeliveryAddressCountry($country);

        $cartOrderLine = new CartOrderLine();
        $cartOrderLine->setProduct($product);
        $cartOrderLine->setCartOrder($cartOrder);

        $cartOrder->addLine($cartOrderLine);

        $cart->addOrder($cartOrder);

        return $cart;
    }
}
