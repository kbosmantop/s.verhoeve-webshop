<?php

namespace Tests\Controller;

use AppBundle\Services\Admin;
use GuzzleHttp\Client as Guzzle;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @covers TranslationsController
 *
 * Class TranslationsControllerTest
 */
class TranslationsControllerTest extends WebTestCase
{
    use ContainerAwareTrait;

    /** @var $client */
    private $client;

    /** @var $adminLogin */
    private $adminLogin;

    /** @var string */
    private $adminPassword;

    /** @var RouterInterface */
    private $router;

    /** @var Admin */
    private $admin;

    /** @var $locales */
    private $locales;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->adminLogin = getenv('ADMIN_LOGIN');

        $this->adminPassword = getenv('ADMIN_PASS');

        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => $this->adminLogin,
            'PHP_AUTH_PW'   => $this->adminPassword,
        ]);

        $this->logIn();

        $this->admin = $this->container->get('admin');

        $this->router = $this->client->getContainer()->get('router');

        $this->router->setContext($this->admin->getRequestContext());

        $this->locales = $this->container->getParameter('a2lix_translation_form.locales');
    }

    /**
     * Functional Index test
     */
    public function testIndex()
    {
        $uri = $this->router->generate('translation_index', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $this->client->request('GET', $uri);

        static::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Functional Index test Crawler
     */
    public function testIndexCrawler()
    {
        $uri = $this->router->generate('translation_index', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $crawler = $this->client->request('GET', $uri);

        static::assertSame('Dutch (Netherlands)', $crawler->filter('.container > h3')->text());
    }

    /**
     * @covers \AppBundle\Controller\TranslationsController::jsOutput
     */
    public function testJsOutput()
    {
        $client = new Guzzle();

        foreach ($this->locales as $locale) {
            $routeParameters = [
                'domain' => 'messages',
                'locale' => $locale,
            ];

            $uri = $this->router->generate(
                'app_translations_jsoutput',
                $routeParameters,
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $response = $client->get($uri);

            static::assertContains('application/javascript', $response->getHeaderLine('Content-Type'));
        }
    }
    /**
     * @covers \AppBundle\Controller\TranslationsController::jsonOutput
     */
    public function testJsonOutput()
    {
        $client = new Guzzle();

        foreach ($this->locales as $locale) {
            $routeParameters = [
                'domain' => 'messages',
                'locale' => $locale,
            ];

            $uri = $this->router->generate(
                'app_translations_jsonoutput',
                $routeParameters,
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $response = $client->get($uri);

            static::assertContains('application/json', $response->getHeaderLine('Content-Type'));
        }
    }

    private function logIn() {
        $session = $this->container->get('session');

        $token = new UsernamePasswordToken($this->adminLogin, null, $this->adminLogin, ['ROLE_ADMIN']);
        $session->set('_security_'. 'backend', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
