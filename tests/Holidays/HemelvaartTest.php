<?php

namespace Tests\Holidays;

/**
 * Hemelvaartsdag valt altijd op een donderdag, tien dagen vóór Pinksteren.
 */
class HemelvaartTest extends AbstractHoliday
{
    /**
     * @throws \Exception
     */
    public function testDayBeforeAt1600(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('ascensionDay');
        $datetime = (clone $holiday)->modify('-8 hour');

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual($holiday->modify('+1 day')->format('Y-m-d'), $firstDeliveryDate);
    }

    /**
     * @throws \Exception
     */
    public function testDayBeforeAt1800(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('ascensionDay');
        $datetime = (clone $holiday)->modify('-6 hour');

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual($holiday->modify('+2 day')->format('Y-m-d'), $firstDeliveryDate);
    }

    /**
     * @throws \Exception
     */
    public function test(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('ascensionDay');
        $datetime = (clone $holiday)->setTime(random_int(0, 23), random_int(0, 59));

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual($holiday->modify('+2 day')->format('Y-m-d'), $firstDeliveryDate);
    }
}
