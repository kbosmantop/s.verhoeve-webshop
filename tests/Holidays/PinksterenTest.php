<?php

namespace Tests\Holidays;

/**
 * Class PinksterenTest
 * @package Tests\Holidays
 */
class PinksterenTest extends AbstractHoliday
{
    /**
     * @throws \Exception
     */
    public function testDayBeforeAt1600(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('pentecost');
        $datetime = (clone $holiday)->modify('-8 hour');

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual((clone $holiday)->modify('+2 days')->format('Y-m-d'), $firstDeliveryDate);
    }

    /**
     * @throws \Exception
     */
    public function testDayBeforeAt1800(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('pentecost');
        $datetime = (clone $holiday)->modify('-6 hour');

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual((clone $holiday)->modify('+3 days')->format('Y-m-d'), $firstDeliveryDate);
    }

    /**
     * @throws \Exception
     */
    public function test(): void
    {
        static::assertTrue(\extension_loaded('timecop'));

        $holiday = clone $this->getHoliday('pentecost');
        $datetime = (clone $holiday)->setTime(random_int(0, 23), random_int(0, 59));

        timecop_freeze($datetime);

        $deliveryDates = $this->getDeliveryDates()->getAvailable();

        $firstDeliveryDate = $deliveryDates->first()->format('Y-m-d');

        static::assertGreaterThanOrEqual($holiday->modify('+3 day')->format('Y-m-d'), $firstDeliveryDate);
    }
}
