<?php

namespace Tests\Holidays;

use AppBundle\Services\CartService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Geography\Holiday;
use AppBundle\Utils\DeliveryDate;
use AppBundle\Utils\DeliveryDates;
use Tests\Holidays\Traits\MocksTrait;
use Yasumi\Holiday as YasumiHoliday;
use Yasumi\Yasumi;

/**
 * Class AbstractHoliday
 * @package Tests\Holidays
 */
abstract class AbstractHoliday extends KernelTestCase
{
    use ContainerAwareTrait;
    use MocksTrait;

    public function setUp()
    {
        global $kernel;

        $kernel = self::bootKernel();

        $this->setContainer($kernel->getContainer());

        $fakeRequest = Request::create('/', 'GET', [], [], [], [
            'HTTP_HOST' => 'dev.toptaarten.nl',
        ]);

        $requestStack = $this->container->get('request_stack');
        $requestStack->push($fakeRequest);

        $cart = $this->mockCart();

        $this->container->get(CartService::class)->setCart($cart);
    }

    /**
     * @param string $holiday
     * @param string $region
     * @return YasumiHoliday
     * @throws \ReflectionException
     */
    protected function getHoliday(string $holiday, string $region = 'Netherlands'): YasumiHoliday
    {
        return Yasumi::create($region, date('Y'))->getHoliday($holiday);
    }

    /**
     * @todo move internal logic to seperate service
     * @return DeliveryDates
     * @throws \Exception
     */
    protected function getDeliveryDates(): DeliveryDates
    {
        $begin = new \DateTime();
        $begin->setTime(0, 0, 0);

        $end = clone $begin;
        $end = $end->modify('+365 day');

        $interval = new \DateInterval('P1D');

        /** @var $dates \DateTime[] */
        $dates = new \DatePeriod($begin, $interval, $end);

        $deliveryDates = new DeliveryDates([], $this->container);

        foreach ($dates as $date) {
            $deliveryDates->add(new DeliveryDate($date->format('r')));
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->container->get('doctrine')->getManager()->getRepository(Holiday::class);

        $qb = $entityRepository->createQueryBuilder('h');
        $qb->leftJoin('h.countries', 'hc');
        $qb->andWhere('hc.country = :country');

        $qb->setParameters([
            'country' => 'NL',
        ]);

        $holidays = new ArrayCollection($qb->getQuery()->getResult());

        foreach ($deliveryDates as &$deliveryDate) {
            $deliveryDate->setIterator($deliveryDates);

            $matchedHolidays = $holidays->filter(function (Holiday $holiday) use ($deliveryDate) {
                return $holiday->getDate()->format('Y-m-d') === $deliveryDate->format('Y-m-d');
            });

            if (!$matchedHolidays->isEmpty()) {
                $deliveryDate->setHoliday($matchedHolidays->current());
            }
        }

        return $deliveryDates;
    }

    public function tearDown(): void
    {
        timecop_return();

        $requestStack = $this->container->get('request_stack');
        $requestStack->pop();

        parent::tearDown();
    }
}