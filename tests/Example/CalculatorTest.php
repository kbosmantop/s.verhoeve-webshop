<?php

namespace Tests\Example;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CalculatorTest extends KernelTestCase
{
    public function testOnePlusOneIsTwo()
    {
        $two = 1.01 + 0.99;

        self::assertInternalType('int', $two);
        self::assertEquals(2, $two);
    }
}