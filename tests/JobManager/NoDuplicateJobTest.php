<?php

namespace Tests\JobManager;

use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class NoDuplicateJobTest
 * @package Tests\JobManager
 */
class NoDuplicateJobTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp(): void
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws \Exception
     */
    public function testNewJob(): void
    {
        $job = new Job('about');

        $this->container->get('job.manager')->addJob($job, 2, false);
        $this->container->get('doctrine')->getManager()->flush();

        static::assertInternalType('numeric', $job->getId());

        /** @var \DateTimeInterface $executeAfter */
        $executeAfter = $job->getExecuteAfter();

        static::assertEquals(2, $job->getCreatedAt()->diff($executeAfter)->format('%s'));
    }

    /**
     * @throws \Exception
     */
    public function testExistingJob(): void
    {
        $job = new Job('about');

        $this->container->get('job.manager')->addJob($job, 2, true);
        $this->container->get('doctrine')->getManager()->flush();

        static::assertNull($job->getId());
    }
}
