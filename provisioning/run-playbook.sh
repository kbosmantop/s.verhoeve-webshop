#!/bin/bash

playbook_name=main
playbook_file=site.yml
limit=""

if [ $(id -u) -eq 0 ]; then
	su -lc "$0 $*" ansible
elif [ $(id -un) == 'ansible' ]; then
	cd /provisioning/playbooks;

	if [ -n "$1" ]; then
		if [[ "$1" == *\/* ]]; then
			echo "Invalid argument: $1" >&2
			exit 1
		elif [ -d "$1" ]; then
			playbook_name="$1"
			shift
		else
			echo "No such playbook: $1" >&2
			exit 1
		fi
	fi

	if [ -n "$1" ]; then
		if [[ "$1" != *\/* ]]; then
			playbook_file="$1"
			shift
		else
			echo "Invalid argument: $1" >&2
			exit 1
		fi
	fi

	if ! [ -f "$playbook_name/$playbook_file" ]; then
		echo "File not found: " $(realpath "$playbook_file") >&2
		exit 1
	fi

	cd "$playbook_name" || exit 1

	if [ -f requirements.yml ]; then
		echo "Installing dependencies..."
		ansible-galaxy install -r requirements.yml || exit 1
	fi

	echo "Run Ansible playbook..."
	ansible-playbook --inventory=/provisioning/inventory.ini "$playbook_file" $limit || exit 1
else
	echo "Need to be root to do this."; >&2
	exit 1
fi
