# Upgrade from 1.14 to 1.15

#Klantenservice popup (alle pagina's)
Content is verplaatst vanuit twig template naar een webpagina('s).

1. Pagina's aanmaken per site met juiste content zie:
    https://github.com/topgeschenken/webshop/issues/1892
    
   In template kunnen twee variabelen gebruikt worden.
    - `%siteName%`
    - `%customerServiceImage%`
    
    Pagina html sjabloon:
    
    ```
    <div class="row">
    <div class="col-lg-6 col-md-6">
    <div class="left">
    <div class="foto"><img alt="%siteName%" src="%customerServiceImage%" /></div>    
    <div class="info">
    <div class="rounded">Telefoon: 088 110 8060</div>    
    <div class="rounded">Email: <a href="mailto:klantenservice@toptaarten.nl">klantenservice@toptaarten.nl</a></div>    
    <div class="regel">
    <div class="day">Ma t/m Do</div>    
    <div class="time">8.30 - 20.00 uur</div>
    </div>    
    <div class="regel">
    <div class="day">Vrij t/m Za</div>    
    <div class="time">8.30 - 17.30 uur</div>
    </div>
    </div>
    </div>
    </div>    
    <div class="col-lg-6 col-md-6">
    <div class="right">
    <h3>Kan ik u helpen?</h3>    
    <p><b>Hoe snel kunt u leveren?</b> Binnen Nederland kunt u tot 17.00 uur bestellen en nog de volgende dag laten bezorgen! De website geeft u automatisch de eerst mogelijke bezorgdatum.<br />
    <br />
    <b>Wat zijn de bezorgkosten?</b> Voor heel Nederland &euro; 5.95. Voor bezorging in het buitenland geld een ander tarief.<br />
    <br />
    <b>Wat als de ontvanger niet thuis is?</b> Indien de ontvanger niet thuis is bij aflevering, zijn er een aantal mogelijkheden die de koerier dan kan ondernemen:</p>    
    <ul>
    	<li>De koerier neemt het pakket mee terug en zal de eerstvolgende bezorg dag een nieuwe bezorgpoging ondernemen.</li>
    	<li>De koerier geeft het pakket af, bij &eacute;&eacute;n van de naastgelegen buren.</li>
    	<li>De koerier geeft het pakket af bij het dichtstbijzijnde afhaalpunt. Hier kan het pakket de eerstvolgende werkdag worden afgehaald door de ontvanger.</li>
    </ul>
    <a href="/faq">Meer veelgestelde vragen</a></div>
    </div>
    </div>

    ```
    
2. Koppel de diverse popup pagina's via;
    - Instellingen > Websites > Toptaarten.nl > Tab: Content > Veld: Klantenservice Popup