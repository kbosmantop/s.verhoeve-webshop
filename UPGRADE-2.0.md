# Upgrade from 1.15 tot 2.0

# Installatie puppeteer

Voor browsershot versie 3.0 is puppeteer nodig, de installatie instructies hieronder;
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
sudo npm install --global --unsafe-perm puppeteer
sudo chmod -R o+rx /usr/lib/node_modules/puppeteer/.local-chromium
```

bron: https://github.com/spatie/browsershot

# Regels fraude check

Omdat het regelsysteem nu samenwerkt met de auto order zal er een regel moeten worden aangemaakt die de Payment fraud score in de gaten houdt.

Regel startpunt vanaf OrderCollection

# install image compressors
`sudo apt install jpegoptim optipng`
