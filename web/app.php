<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

/** @var Composer\Autoload\ClassLoader */
$loader = require __DIR__ . '/../app/autoload.php';

//Config environment vars
$dotEnv = new Dotenv();
$dotEnv->load(__DIR__ . '/../.env');

$env = (getenv('SYMFONY_ENV') !== false ? getenv('SYMFONY_ENV') : 'dev');
$debug = getenv('SYMFONY_DEBUG') !== '0' && $env !== 'prod';

if ($env === 'dev' && strpos($_SERVER['HTTP_HOST'], 'ngrok.io') !== false) {
    $_SERVER['HTTP_HOST'] = 'dev.topfruit.nl';
}

$kernel = new AppKernel($env, $debug);

//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
if ($debug) {
    Debug::enable();
}
$request = Request::createFromGlobals();

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
