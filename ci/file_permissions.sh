#!/usr/bin/env bash

set -eu

echo "#################################"
echo "## running file-permissions.sh ##"
echo "#################################"

docker cp bin/file_permissions.sh tg_php:/bin

## composer
docker-compose exec -T php sh -c 'cd /app && bash bin/file_permissions.sh'
