#!/usr/bin/env bash

set -eu

echo "###################################"
echo "## running php bin/console d:s:u ##"
echo "###################################"

## database
docker-compose exec -T php sh -c 'cd /app && php bin/console d:s:u --force'