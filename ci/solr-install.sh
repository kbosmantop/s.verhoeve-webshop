#!/bin/bash
i=20
while [[ ${i} -gt 0 ]] ; do
    status_code=$(curl --write-out %{http_code} --silent --output /dev/null http://localhost:8983/solr/admin/cores)
    echo ${status_code}
    if [[ "$status_code" -ne 200 ]] ; then
        i=${i}-1
    else
        sleep 3
        break
    fi
    sleep 1
done

echo "Available cores in project:"
echo "$(docker-compose exec -T php sh -c 'php bin/console topbloemen:solr-cores')"

cores=$(docker-compose exec -T php sh -c 'php bin/console topbloemen:solr-cores --missing --no-debug 2>/dev/null')

echo ""
echo "Missing cores:"
echo ${cores}

for core in ${cores} ; do
    echo "Creating SOLR core: ${core}"
    docker-compose exec -T solr sh -c "/opt/solr/bin/solr create -c ${core} -n data_driven_schema_configs"
done