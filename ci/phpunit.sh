#!/usr/bin/env bash

set -eu

echo "########################"
echo "## running phpunit.sh ##"
echo "########################"

## currently in the project building cache from a clean cache crashes, forcing result of first run to || true to prevent dockers quits the process
docker-compose exec -T php sh -c 'cd /app && php bin/console cache:clear --env=test || true'
docker-compose exec -T php sh -c 'cd /app && php bin/console cache:clear --env=test'

#docker-compose exec -T php sh -c 'cd /app && php bin/phpunit'
docker-compose exec -T php sh -c 'cd /app && php bin/console reset'