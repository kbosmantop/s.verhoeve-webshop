#!/usr/bin/env bash

set -eu

echo "#################################"
echo "## running composer-install.sh ##"
echo "#################################"

## composer
docker-compose exec -T php sh -c 'cd /app && composer install --no-progress'

# copy composer files
echo "COPY composer files to local"
rm -dfR ./vendor
docker cp tg_php:/app/vendor ./vendor